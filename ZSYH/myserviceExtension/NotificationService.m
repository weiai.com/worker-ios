//
//  NotificationService.m
//  myserviceExtension
//
//  Created by 主事丫环 on 2019/5/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "NotificationService.h"
#import "VoiceManager.h"
#import <AVFoundation/AVFoundation.h>
#import <UserNotifications/UserNotifications.h>

#define kFileManager [NSFileManager defaultManager]

typedef void(^PlayVoiceBlock)();

@interface NotificationService ()<AVAudioPlayerDelegate>
@property (nonatomic, retain)AVAudioPlayer *player;
@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
// 语音合成完毕之后，使用 AVAudioPlayer 播放
@property (nonatomic, copy)PlayVoiceBlock aVAudioPlayerFinshBlock;
@property (nonatomic, strong) AVSpeechSynthesizer *speechSynthesizer;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    /**
     *  备注: 后台推送过来的数据要协商好格式，只要格式协商好，这部分就不会有问题
     */
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    self.speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    
    //UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    //UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    //content.title = [NSString localizedUserNotificationStringForKey:@"Hello" arguments:nil];
    //content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"Agent-%d",arc4random()%100] arguments:nil];
    //content.sound = [UNNotificationSound defaultSound];
    //content.sound = [UNNotificationSound soundNamed:[NSString stringWithFormat:@"sound02.wav"]];

    //UNNotificationRequest *requester = [UNNotificationRequest requestWithIdentifier:@"OXNotification" content:content trigger:nil];
    //[center addNotificationRequest:requester withCompletionHandler:^(NSError *_Nullable error) {
    //    NSLog(@"成功添加推送");
    //}];
    
    NSString *stere = @"";
    if ([[request.content.userInfo allKeys] containsObject:@"subscribeId"]) {
        stere = @"yuyuedan";
    }else  if ([[request.content.userInfo allKeys] containsObject:@"userPayToRepairUser"]) {
        stere = @"";
    }else  if ([[request.content.userInfo allKeys] containsObject:@"afterOrderToRepairUser"]) {
        stere = @"shouhouweiixui";
    }
    else if ([[request.content.userInfo allKeys] containsObject:@"repairUserInsurancePremium"]) {
        //您的订单已支付 //userPayToRepairUser
        stere = @"dingdanyizhifu";
    }
    else if ([[request.content.userInfo allKeys] containsObject:@"accessoriesArrived"]) {
        //您的配件已到货
        stere = @"peijianyidaohuo";
    }
    
    NSDictionary *mydic =request.content.userInfo;
    
    //    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    //    content.title = [mydic objectForKey:@"alert"];
    //    content.subtitle = @"dddddddd";;
    //    content.body = @"";
    //    content.sound = [UNNotificationSound soundNamed:[NSString stringWithFormat:@"%@.wav",stere]];
    //
    //    content.categoryIdentifier = @"dsaf";
    //
    //    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:0.01 repeats:NO];
    //
    //    UNNotificationRequest *reque = [UNNotificationRequest requestWithIdentifier:@"dsaf" content:content trigger:trigger];
    //    [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:reque withCompletionHandler:^(NSError * _Nullable error) {
    //        if (error == nil) {
    //
    //        }
    //    }];
    //    self.bestAttemptContent.title = @"我是大标题";
    //            self.bestAttemptContent.subtitle = @"我是小标题";
    //            self.bestAttemptContent.body =@"我是内容";
    //        self.bestAttemptContent.sound = [UNNotificationSound alloc];
    //     self.bestAttemptContent.sound = [UNNotificationSound soundNamed:[NSString stringWithFormat:@"%@.wav",stere]];
    
    self.bestAttemptContent.sound = [UNNotificationSound soundNamed:[NSString stringWithFormat:@"%@.m4a",stere]];
    
    //自定义铃声
    //self.bestAttemptContent.sound = [UNNotificationSound soundNamed:@"sound.wav"];
    
    //AVSpeechSynthesizer *voice= [[AVSpeechSynthesizer alloc]init];
    //[self startBroadcastVoice:self speed:0.1 volume:1 tone:1 voice:voice LanguageType:@"zh-CN" content:@"您有一条新的预约单"];
    //self.bestAttemptContent.title = @"我是大标题";
    //self.bestAttemptContent.subtitle = @"我是小标题";
    //self.bestAttemptContent.body =@"我是内容";
    //self.contentHandler(self.bestAttemptContent);
    //self.bestAttemptContent.body =@"我是内容";
    
    //UNMutableNotificationContent 这个类你用过吗,
    
    //self.bestAttemptContent.sound = [UNNotificationSound  soundNamed:@"2"] ;
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"servicejspath" object:nil userInfo:nil];
    
    //NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"music" withExtension:@"mp3"];
    //self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileUrl error:nil];
    //if (self.player) {
    //    [self.player prepareToPlay];
    //}
    //self.player.volume = 0.5;
    //self.player.pan = -1;
    //self.player.numberOfLoops = -1;
    //self.player.rate = 0.5;
    //[self.player play];
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end
