//
//  BasePresentView.m
//  Orchard
//
//  Created by 李英杰 on 2019/5/13.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import "BasePresentView.h"
@interface BasePresentView ()

@end
@implementation BasePresentView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
- (void)buildSubview {
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    self.alpha = 0;
    
    UIControl *control = [[UIControl alloc] initWithFrame:self.bounds];
    if (self.width == 0 || self.height == 0) {
        control.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
    }
    control.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    [control addTarget:self action:@selector(dismissPresentView) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:control];
    self.control = control;
    
    UIView *presentView = [[UIView alloc] init];
    presentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:presentView];
    self.presentView = presentView;
}

- (void)showPresentView {
    self.alpha = 1;
    [UIView animateWithDuration:0.3 animations:^{
        self.control.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        self.presentView.transform = CGAffineTransformMakeTranslation(0, - self.presentView.height);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissPresentView {
    [UIView animateWithDuration:0.3 animations:^{
        self.control.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.presentView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self.alpha = 0;
    }];
    
}

@end
