//
//  BaseView.h
//  ShiYun
//
//  Created by 李英杰 on 2018/8/23.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView

@property (nonatomic, strong) id data;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView *bottomLine;

- (void)buildSubview;

- (void)loadContent;

@end
