//
//  BaseLeftImageLabelRightImageLabelCell.m
//  ShiYun
//
//  Created by 李英杰 on 2018/9/6.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"
@interface BaseLeftImageLabelRightImageLabelCell ()


@end

@implementation BaseLeftImageLabelRightImageLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+ (instancetype)cellViewWithTableView:(UITableView *)tableView {
    
    BaseLeftImageLabelRightImageLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([self class])];
    if (!cell) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([self class])];
    }
    return cell;
}

- (void)buildSubview {
    self.contentView.backgroundColor = [UIColor whiteColor];
    //背景View
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:backView];
    self.backView = backView;
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        //标题
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = regular16Font;
        titleLabel.textColor = kMainTextColor33;
        titleLabel.numberOfLines = 1;
        titleLabel.text = @"手机号码：";
        [self.backView addSubview:titleLabel];
        _titleLabel = titleLabel;
        
    }
    return _titleLabel;
}

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _leftImageView = titleImageView;
        
    }
    return _leftImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        //内容
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.backgroundColor = [UIColor clearColor];
        tipLabel.textAlignment = NSTextAlignmentLeft;
        tipLabel.font = regular15Font;
        tipLabel.textColor = kMainTextColor66;
        tipLabel.numberOfLines = 0;
        tipLabel.text = @"";
        _contentLabel = tipLabel;
        [self.backView addSubview:tipLabel];
        
    }
    return _contentLabel;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _rightImageView = titleImageView;
        
    }
    return _rightImageView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self.backView addSubview:bottomLine];
        _bottomLine = bottomLine;
    }
    return _bottomLine;
}

- (UIView *)alphaView {
    if (!_alphaView) {
        UIView *alphaView = [[UIView alloc] init];
        alphaView.backgroundColor = [UIColor lightGrayColor];
        alphaView.alpha = 0;
        [self.contentView addSubview:alphaView];
        [self.contentView sendSubviewToBack:alphaView];
        _alphaView = alphaView;
        [alphaView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return _alphaView;
}

- (void)showSelectedAnimation {
    self.alphaView.alpha = 0;
    [UIView animateWithDuration:0.1 animations:^{
        self.alphaView.alpha = 0.3;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.alphaView.alpha = 0;
        } completion:^(BOOL finished) {
            
        }];
    }];
}


@end
