//
//  BaseLeftImageLabelRightImageLabelCell.h
//  ShiYun
//
//  Created by 李英杰 on 2018/9/6.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface BaseLeftImageLabelRightImageLabelCell : BaseTableViewCell

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) UIView *alphaView;//选中效果

+ (instancetype)cellViewWithTableView:(UITableView *)tableView;
/* 显示选中效果 */
- (void)showSelectedAnimation;
@end
