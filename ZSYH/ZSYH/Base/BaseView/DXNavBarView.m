//
//  DXNavBarView.m
//  DaXueZhang
//
//  Created by qiaoxuekui on 2018/7/13.
//  Copyright © 2018年 qiaoxuekui. All rights reserved.
//

#import "DXNavBarView.h"

@implementation DXNavBarView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self initialize];
    }
    return self;
}
//
//progress
- (void)initialize{
    
    [self addSubview:self.backView];
    
    [self addSubview:self.backgroundImageView];
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self addSubview:self.backButton];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kNaviHeight-44);
        make.left.equalTo(self).offset(0);
//        make.width.mas_equalTo(kScreen_Width*0.12);
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(44);
    }];
    
    [self addSubview:self.leftButton];
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kNaviHeight-44);
        make.left.equalTo(self).offset(0);
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(44);
    }];
    self.leftButton.hidden=YES;
    
    [self addSubview:self.rightButton];
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kNaviHeight-44);
        make.right.equalTo(self).offset(0);
        make.width.mas_equalTo(56);
        make.height.mas_equalTo(44);
    }];
    self.rightButton.hidden=YES;
    
    [self addSubview:self.titleView];
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kNaviHeight-44);
        make.centerX.equalTo(self).offset(0);
        make.height.mas_equalTo(44);
        make.width.mas_equalTo(kScreen_Width-112);
    }];
    
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kNaviHeight-44);
        make.centerX.equalTo(self).offset(0);
        make.width.mas_equalTo(kScreen_Width-112);
        make.height.mas_equalTo(44);
    }];
    [self addSubview:self.seperateLine];
    [self.seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self) ;
        make.height.mas_equalTo(kScaleNum(0.5));
    }];
    
//    [self addSubview:self.shadowView];
//    [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self);
//        make.right.equalTo(self);
//        make.bottom.equalTo(self).offset(-DXNUM(2)) ;
//        make.height.mas_equalTo(DXNUM(1));
//    }];
    
}

-(UIView *)backView{
    if (_backView==nil) {
        _backView=[[UIView alloc] initWithFrame:self.bounds];
        _backView.backgroundColor= [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        _backgroundImageView.backgroundColor = [UIColor clearColor];
    }
    return _backgroundImageView;
}

-(UILabel *)titleLabel{
    if (_titleLabel==nil) {
        _titleLabel=[[UILabel alloc] init];
        _titleLabel.backgroundColor=[UIColor clearColor];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font=[UIFont boldSystemFontOfSize:18];
        _titleLabel.textAlignment=NSTextAlignmentCenter;
    }
    return _titleLabel;
}

-(UIView *)titleView{
    if (_titleView==nil) {
        _titleView=[[UIView alloc] init];
        _titleView.backgroundColor=[UIColor clearColor];
    }
    return _titleView;
}

-(UIButton *)backButton{
    if (_backButton==nil) {
        _backButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton setImage:[UIImage imageNamed:@"left_icon"] forState:UIControlStateNormal];
        [_backButton setImage:[UIImage imageNamed:@"left_icon_white"]  forState:UIControlStateSelected];
        [_backButton setBackgroundColor:[UIColor clearColor]];
        _backButton.adjustsImageWhenDisabled = NO;
        _backButton.adjustsImageWhenHighlighted = NO;
    }
    return _backButton;
}

-(UIButton *)leftButton{
    if (_leftButton==nil) {
        _leftButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [_leftButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_leftButton setBackgroundColor:[UIColor clearColor]];
        _leftButton.adjustsImageWhenDisabled = NO;
        _leftButton.adjustsImageWhenHighlighted = NO;
     }
    return _leftButton;
}

-(UIButton *)rightButton{
    if (_rightButton==nil) {
        _rightButton=[UIButton buttonWithType:UIButtonTypeCustom];
        self.rightButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_rightButton setBackgroundColor:[UIColor clearColor]];
        _rightButton.adjustsImageWhenDisabled = NO;
        _rightButton.adjustsImageWhenHighlighted = NO;
    }
    return _rightButton;
}
-(UILabel *)seperateLine{
    if (_seperateLine == nil) {
        _seperateLine = [[UILabel alloc] init];
        _seperateLine.hidden = YES;
        _seperateLine.backgroundColor = [UIColor lightGrayColor];
    }
    return _seperateLine;
}

-(UIView *)shadowView{
    if (_shadowView==nil) {
        _shadowView=[[UIView alloc] init];
        _shadowView.backgroundColor = [UIColor grayColor];
        _shadowView.layer.shadowColor = [UIColor grayColor].CGColor;//设置阴影的颜色
        //_shadowView.layer.shadowOpacity = 0.2f;
        //_shadowView.layer.shadowRadius = 2.f;
        _shadowView.layer.shadowOffset = CGSizeMake(kScreen_Width,4);
    }
    return _shadowView;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
