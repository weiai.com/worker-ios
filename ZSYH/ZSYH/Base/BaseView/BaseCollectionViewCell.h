//
//  BaseCollectionViewCell.h
//  Orchard
//
//  Created by 李英杰 on 2019/5/7.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionViewCell : UICollectionViewCell
/**
 * 数据源
 */
@property (nonatomic, weak) id data;

/**
 * 记录 cell 的 indexPath
 */
@property (nonatomic, weak) NSIndexPath *indexPath;

/**
 * tableView
 */
@property (nonatomic, weak) UICollectionView *collectionView;

@property (nonatomic, weak) UIViewController *viewController;

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath;
/**
 * 配置 cell
 */
- (void)setupCell;

/**
 * 创建并布局子视图
 */
- (void)buildSubview;

/**
 * 加载数据
 */
- (void)loadContent;

/**
 *  Calculate the cell's from data, override by subclass.
 *
 *  @param data Data.
 *
 *  @return Cell's height.
 */
+ (CGFloat)cellHeightWithData:(id)data;

/**
 *  Selected event, you should use this method in 'tableView:didSelectRowAtIndexPath:' to make it effective.
 */
- (void)selectedEvent;

#pragma mark - Constructor method.

/**
 *  Register to collectionView with the reuseIdentifier you specified.
 *
 *  @param collectionView       CollectionView.
 *  @param reuseIdentifier The cell reuseIdentifier.
 */
+ (void)registerToCollectionView:(UICollectionView *)collectionView reuseIdentifier:(NSString *)reuseIdentifier;

/**
 *  Register to collectionView with the The class name.
 *
 *  @param collectionView       CollectionView.
 */
+ (void)registerToCollectionView:(UICollectionView *)collectionView;
@end
