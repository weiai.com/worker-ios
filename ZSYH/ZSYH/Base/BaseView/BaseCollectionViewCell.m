//
//  BaseCollectionViewCell.m
//  Orchard
//
//  Created by 李英杰 on 2019/5/7.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface BaseCollectionViewCell ()
@property (nonatomic, assign) BOOL hasInit;//是否已经初始化.默认为NO
@end

@implementation BaseCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView forIndexPath:(NSIndexPath *)indexPath {
    BaseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([self class]) forIndexPath:indexPath];
    if (!cell.hasInit) {
        //如果未加载过
        [cell setupCell];
        [cell buildSubview];
        cell.hasInit = YES;
    }
    return cell;
}

- (void)setupCell {
    NSLog(@"setupCell方法");
}

- (void)buildSubview {
    NSLog(@"buildSubview方法");
}

- (void)loadContent {
    
}

- (void)selectedEvent {
    
}

+ (CGFloat)cellHeightWithData:(id)data {
    return 0.f;
}

/**
 *  Register to collectionView with the reuseIdentifier you specified.
 *
 *  @param collectionView       CollectionView.
 *  @param reuseIdentifier The cell reuseIdentifier.
 */
+ (void)registerToCollectionView:(UICollectionView *)collectionView reuseIdentifier:(NSString *)reuseIdentifier {
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:reuseIdentifier];
}

/**
 *  Register to collectionView with the The class name.
 *
 *  @param collectionView       CollectionView.
 */
+ (void)registerToCollectionView:(UICollectionView *)collectionView {
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:NSStringFromClass([self class])];
}

@end
