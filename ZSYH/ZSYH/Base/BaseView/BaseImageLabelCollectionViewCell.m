//
//  BaseImageLabelCollectionViewCell.m
//  Orchard
//
//  Created by 李英杰 on 2019/5/7.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import "BaseImageLabelCollectionViewCell.h"

@implementation BaseImageLabelCollectionViewCell

- (void)buildSubview {
    
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (UIView *)backView {
    if (!_backView) {
        UIView *backView = [[UIView alloc] init];
        backView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:backView];
        _backView = backView;
    }
    return _backView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        //标题
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = regular16Font;
        titleLabel.textColor = kMainTextColor33;
        titleLabel.numberOfLines = 1;
        titleLabel.text = @"";
        [self.backView addSubview:titleLabel];
        _titleLabel = titleLabel;
        
    }
    return _titleLabel;
}

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _leftImageView = titleImageView;
        
    }
    return _leftImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        //内容
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.backgroundColor = [UIColor clearColor];
        tipLabel.textAlignment = NSTextAlignmentLeft;
        tipLabel.font = regular15Font;
        tipLabel.textColor = kMainTextColor66;
        tipLabel.numberOfLines = 0;
        tipLabel.text = @"";
        _contentLabel = tipLabel;
        [self.backView addSubview:tipLabel];
        
    }
    return _contentLabel;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _rightImageView = titleImageView;
        
    }
    return _rightImageView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self.backView addSubview:bottomLine];
        _bottomLine = bottomLine;
    }
    return _bottomLine;
}

@end
