//
//  BaseImageLabelCollectionReusableView.h
//  Orchard
//
//  Created by 李英杰 on 2019/5/20.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseImageLabelCollectionReusableView : UICollectionReusableView
@property (nonatomic, weak) id data;
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView *bottomLine;
@end
