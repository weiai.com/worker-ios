//
//  BaseLeftLabelRightButtonHeaderFooterView.h
//  ShiYun
//
//  Created by 李英杰 on 2018/9/6.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import "BaseTableViewHeaderFooterView.h"

@interface BaseLeftLabelRightButtonHeaderFooterView : BaseTableViewHeaderFooterView

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UILabel *rightLabel;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIView *bottomLine;

+ (instancetype)sectionViewWithTableView:(UITableView *)tableView;

@end
