//
//  BasePresentView.h
//  Orchard
//
//  Created by 李英杰 on 2019/5/13.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import "BaseView.h"

@interface BasePresentView : BaseView
@property (nonatomic, strong) id data;
//显示分享面板,整个灰色透明背景
@property (nonatomic, strong) UIControl *control;
//栈顶显示的白色背景
@property (nonatomic, strong) UIView *presentView;

/**
 * 显示面板--外部调用
 */
- (void)showPresentView;
/**
 * 消失面板--外部调用
 */
- (void)dismissPresentView;
@end
