//
//  BaseLeftLabelRightButtonHeaderFooterView.m
//  ShiYun
//
//  Created by 李英杰 on 2018/9/6.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import "BaseLeftLabelRightButtonHeaderFooterView.h"

@implementation BaseLeftLabelRightButtonHeaderFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (instancetype)sectionViewWithTableView:(UITableView *)tableView {
    
    BaseLeftLabelRightButtonHeaderFooterView *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([self class])];
    if (!sectionView) {
        sectionView = [[self alloc] initWithReuseIdentifier:NSStringFromClass([self class])];
    }
    return sectionView;
}

- (void)buildSubview {
    UIView *backView = [UIView new];
    backView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:backView];
    self.backView = backView;
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    
    //底部触发点击事件的 button
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.adjustsImageWhenDisabled = NO;
    backButton.adjustsImageWhenHighlighted = NO;
    backButton.backgroundColor = [UIColor clearColor];
    backButton.userInteractionEnabled = YES;
    [backButton addTarget:self action:@selector(backButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:backButton];
    [backButton mas_makeConstraints:^(MASConstraintMaker *make) {
       make.edges.equalTo(self.backView);
    }];
    
}

- (void)rightButtonEvent:(UIButton *)sender {
    
}

- (void)backButtonEvent:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(baseTableViewHeaderFooterView:event:)]) {
        [self.delegate baseTableViewHeaderFooterView:self event:nil];
    }
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.font = regular15Font;
        titleLabel.textColor = kMainTextColor00;
        titleLabel.text = @"选择支付方式";
        [self.backView addSubview:titleLabel];
        _titleLabel = titleLabel;
    }
    return _titleLabel;
}

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _leftImageView = titleImageView;
    }
    return _leftImageView;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        //右侧按钮
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        rightButton.backgroundColor = [UIColor clearColor];
        [rightButton addTarget:self action:@selector(rightButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:rightButton];
        _rightButton = rightButton;
    }
    return _rightButton;
}

- (UILabel *)rightLabel {
    if (!_rightLabel) {
        
        UILabel *rightLabel = [[UILabel alloc] init];
        rightLabel.font = regular15Font;
        rightLabel.textColor = kMainTextColor00;
        rightLabel.text = @"联系客服";
        [self.backView addSubview:rightLabel];
        _rightLabel = rightLabel;
    }
    return _rightLabel;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self.backView addSubview:titleImageView];
        _rightImageView = titleImageView;
    }
    return _rightImageView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        //分割线
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self.backView addSubview:bottomLine];
        _bottomLine = bottomLine;
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kScaleNum(1));
            make.left.right.bottom.equalTo(self.backView);
        }];
    }
    return _bottomLine;
}

@end
