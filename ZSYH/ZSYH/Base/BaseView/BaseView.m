//
//  BaseView.m
//  ShiYun
//
//  Created by 李英杰 on 2018/8/23.
//  Copyright © 2018年 李英杰. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self buildSubview];
    }
    return self;
}

- (void)buildSubview {
    
}

- (void)loadContent {
    
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        //标题
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.font = regular16Font;
        titleLabel.textColor = kMainTextColor33;
        titleLabel.numberOfLines = 1;
        titleLabel.text = @"手机号码：";
        [self addSubview:titleLabel];
        _titleLabel = titleLabel;
        
    }
    return _titleLabel;
}

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:titleImageView];
        _leftImageView = titleImageView;
        
    }
    return _leftImageView;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        //内容
        UILabel *tipLabel = [[UILabel alloc] init];
        tipLabel.backgroundColor = [UIColor clearColor];
        tipLabel.textAlignment = NSTextAlignmentLeft;
        tipLabel.font = regular15Font;
        tipLabel.textColor = kMainTextColor66;
        tipLabel.numberOfLines = 0;
        tipLabel.text = @"";
        _contentLabel = tipLabel;
        [self addSubview:tipLabel];
        
    }
    return _contentLabel;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeCenter;
        titleImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:titleImageView];
        _rightImageView = titleImageView;
        
    }
    return _rightImageView;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self addSubview:bottomLine];
        _bottomLine = bottomLine;
    }
    return _bottomLine;
}

@end
