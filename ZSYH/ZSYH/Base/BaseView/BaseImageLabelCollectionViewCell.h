//
//  BaseImageLabelCollectionViewCell.h
//  Orchard
//
//  Created by 李英杰 on 2019/5/7.
//  Copyright © 2019年 李英杰. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@interface BaseImageLabelCollectionViewCell : BaseCollectionViewCell
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView *bottomLine;
@end
