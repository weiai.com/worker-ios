//
//  TabBarViewController.m
//  ZSYH
//
//  Created by 魏堰青 on 2019/2/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "TabBarViewController.h"
#import "BaseNavViewController.h"
#import "ListenListViewController.h"
#import "MallHomeViewController.h"
#import "MyCenterViewController.h"
#import "CSnewCenterViewController.h"
#import "CSnewMallViewController.h"
#import "CSnewMainMallViewController.h"
#import "HouBaoMainViewController.h"
#import "CSMyorderViewController.h"
#import "OrderViewController.h"
#import "CSnewForumViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

-(void)viewWillAppear:(BOOL)animated
{
    // 适配iOS13导致的bug
    if (@available(iOS 13.0, *)) {
        self.tabBar.tintColor = [UIColor colorWithHexString:@"#70BE68"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CSnewMainMallViewController *vc1 = [[CSnewMainMallViewController alloc] init];
    [self addOneChildVc:vc1 title:@"产品展示" imageName:@"mallImage" selectedImageName:@"mallImage1"];
    
    OrderViewController *vc2 = [[OrderViewController alloc] init];
    [self addOneChildVc:vc2 title:@"我的订单" imageName:@"组 2769" selectedImageName:@"组 2771"];
    
    ListenListViewController *vc3 = [[ListenListViewController alloc] init];
    [self addOneChildVc:vc3 title:@"听单" imageName:@"listingimage" selectedImageName:@"listingimage1"];
    
    CSnewCenterViewController *vc4 = [[CSnewCenterViewController alloc] init];
    [self addOneChildVc:vc4 title:@"个人中心" imageName:@"touxiangImage" selectedImageName:@"touxiangImage1"];
    
    //HouBaoMainViewController *vc4 = [[HouBaoMainViewController alloc] init]; //候保配件安装 新版
    //[self addOneChildVc:vc4 title:@"扫码安装" imageName:@"houbao" selectedImageName:@"组 3138"];
    
}

- (void)addOneChildVc:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName {
    
    UIImage *normalImage = [UIImage imageNamed:imageName];
    UIImage *selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    childVc.tabBarItem = [[UITabBarItem alloc]initWithTitle:title image:normalImage selectedImage:selectedImage];
    [childVc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#70BE68"]} forState:UIControlStateSelected];
   
    //设置tabbar白色不透明背景
    //UIView *bgView = [[UIView alloc] initWithFrame:self.tabBar.bounds];
    //bgView.backgroundColor = [UIColor whiteColor];
    //[self.tabBar insertSubview:bgView atIndex:0];
    
    BaseNavViewController *nav = [[BaseNavViewController alloc] initWithRootViewController:childVc];
    //底部标题
    nav.tabBarItem.title = title;
    [self addChildViewController:nav];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
   [[NSNotificationCenter defaultCenter] postNotificationName:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil userInfo:nil];
    if ([item.title isEqualToString:@"我的订单"]) {
        if (KUID == nil || [KUID isEqualToString:@""]) {
              [self clickLogin];
          }
       
    } else if ([item.title isEqualToString:@"听单"]) {
         if (KUID == nil || [KUID isEqualToString:@""]) {
                     [self clickLogin];
                 }
    } else if ([item.title isEqualToString:@"个人中心"]) {
        if (KUID == nil || [KUID isEqualToString:@""]) {
                     [self clickLogin];
                 }
    }
}

-(void)clickLogin {
    CSLoginViewController *vc = [[CSLoginViewController alloc]init];
    BaseNavViewController *nav = [[BaseNavViewController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
    
    
    
}

@end
