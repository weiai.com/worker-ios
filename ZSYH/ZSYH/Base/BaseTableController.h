//
//  BaseTableController.h
//  NanYangParty
//
//  Created by wanshangwl on 2018/5/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "BaseViewController.h"
#import "MJRefresh.h"

@interface BaseTableController : BaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;

//tableView plain & group
@property (nonatomic,assign) BOOL style;


@property (nonatomic,strong) UIView *emptyView;


//- (void)headBlock:(MJRefreshComponentRefreshingBlock)headBlock footBlock:(MJRefreshComponentRefreshingBlock)footBlock;

@end
