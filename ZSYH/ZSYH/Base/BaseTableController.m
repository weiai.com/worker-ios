//
//  BaseTableController.m
//  NanYangParty
//
//  Created by wanshangwl on 2018/5/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "BaseTableController.h"
//#import "GoodsSearchController.h"


@interface BaseTableController ()

@end

@implementation BaseTableController

- (void)viewDidLoad {
    [super viewDidLoad];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, ScreenH-kNaviHeight-kTabbarHeight) style:_style?UITableViewStyleGrouped:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];

    adjustInset(_tableView);
    [self leftSeparator:0];
}



- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [super textFieldShouldReturn:textField];
//    GoodsSearchController *vc = [GoodsSearchController new];
//    //vc.types = 0;
//    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

- (void)searchAction {
    UITextField *search = [[UITextField alloc]init];
    [search createLeftViewImage:@"搜索1"];
    search.delegate = self;
    self.navigationItem.titleView = search;
}

- (UIView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIView alloc]initWithFrame:self.tableView.bounds];
        
        UIView *back = [[UIView alloc]initWithFrame:CGRectMake(KWIDTH/2-150, self.tableView.height/2-120, 300, 240)];

        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(30, 30, back.width-60, back.width*0.67-40)];
        img.image = imgname(@"order0");
        [back addSubview:img];
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, img.bottom+10, back.width, 20)];
        lab.textColor = [UIColor lightGrayColor];
        lab.font = [UIFont systemFontOfSize:20];
        lab.textAlignment = NSTextAlignmentCenter;
        lab.text = @"暂无列表信息";
        [back addSubview:lab];
        
        [_emptyView addSubview:back];
    }
    return _emptyView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
