//
//  BaseViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.titleTextAttributes=
    @{NSForegroundColorAttributeName:[UIColor blackColor],
      NSFontAttributeName:[UIFont systemFontOfSize:16]};
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.view endEditing:YES];
}

- (void)showOnleText:(NSString *)text delay:(NSTimeInterval)delay {
    //    MBProgressHUD *ghud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //
    //    // Set the text mode to show only text.
    //    ghud.mode = MBProgressHUDModeText;
    //    ghud.label.text = text;
    //    // Move to bottm center.
    //    ghud.center = self.view.center;//屏幕正中心
    //
    //
    //    [ghud hideAnimated:YES afterDelay:delay];
}

-(NSString *)StringAndData:(NSString *) str{
    
    long long time = [str longLongValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    //1:创建日期转换器  (实例化一个NSDateFormatter对象)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //2:设置日期转换的时区
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    //3:设置日期转换的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *temDateStr = [dateFormatter stringFromDate:date];
    return temDateStr;
}

- (void)leftNavItem:(NSString *)img action:(SEL)action {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:img] style:UIBarButtonItemStyleDone target:self action:action];
}

- (void)rightNavItem:(NSString *)img action:(SEL)action {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:img] style:UIBarButtonItemStyleDone target:self action:action];
}

- (UIButton *)rightbutton {
    if (!_rightbutton) {
        _rightbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
        _rightbutton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_rightbutton setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, -10)];
        [_rightbutton setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
        [_rightbutton addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_rightbutton];
    }
    return _rightbutton;
}

- (UIButton *)leftbutton {
    if (!_leftbutton) {
        _leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        _leftbutton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_leftbutton setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
        [_leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
        [_leftbutton setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
        [_leftbutton addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_leftbutton];
    }
    return _leftbutton;
}


- (void)leftSeparator:(CGFloat)length {
    [[UITableView appearance] setSeparatorInset:UIEdgeInsetsMake(0, length, 0, 0)];
    if ([UITableView instancesRespondToSelector:@selector(setLayoutMargins:)]) {
        [[UITableView appearance] setLayoutMargins:UIEdgeInsetsMake(0, length, 0, 0)];
    }
}

- (NSMutableDictionary *)heightAtIndexPath {
    if (!_heightAtIndexPath) {
        _heightAtIndexPath = [NSMutableDictionary dictionary];
    }
    return _heightAtIndexPath;
}

//if (!_titleLabel) {
//_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
//_titleLabel.backgroundColor = [UIColor clearColor];
//_titleLabel.text = title;
//_titleLabel.textColor = [UIColor blackColor];
//_titleLabel.font = [UIFont boldSystemFontOfSize:17];
//_titleLabel.textAlignment = NSTextAlignmentCenter;
//_titleLabel.userInteractionEnabled  = YES;
//}
//self.navigationItem.titleView =_titleLabel;

#pragma mark --------------键盘下去的代理-------------
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    //return 键判断
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)leftClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightClick {
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (UIColor *) colorWithHexString: (NSString *)color {
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // 判断前缀
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

- (CGFloat)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(CGFloat)width{
    if (text && font) {
        CGRect rect  = [text boundingRectWithSize:CGSizeMake(width, 1000) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil];
        return rect.size.height;
    }
    return 0;
}

- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = YES;
}

- (void)showTabBar{
    if (self.tabBarController.tabBar.hidden == NO){
        return;
    }
    UIView *contentView;
    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]])
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = NO;
}

-(NSString *)strwithdic:(NSDictionary *)dic{
    NSString *str = @"";
    str = [NSString stringWithFormat:@"%@%@",[dic valueForKey:@"text"],[dic valueForKey:@"value"]];
    return str;
}

@end



