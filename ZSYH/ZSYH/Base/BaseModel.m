//
//  BaseModel.m
//  ZSYH
//
//  Created by 魏堰青 on 2019/2/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.Id = value;
    }
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Id": @"id"};
}

@end
