//
//  BaseNavViewController.m
//  ZSYH
//
//  Created by 魏堰青 on 2019/2/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseNavViewController.h"
@interface BaseNavViewController ()

@end

@implementation BaseNavViewController



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barTintColor = KBColor;
    self.navigationBar.tintColor = WHITECOLOR;
    [self.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:WHITECOLOR}];
    self.interactivePopGestureRecognizer.delegate = self;
    
    
    
}





- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"qwrewqr"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(popAction)];
    }
    [super pushViewController:viewController animated:animated];
}

-(void)popAction {
    [self popViewControllerAnimated:YES];
}

#pragma mark - UIGestureRecognizerDelegate
//这个方法在视图控制器完成push的时候调用
/*
 -(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
 if (navigationController.viewControllers.count == 1){
 //如果堆栈内的视图控制器数量为1 说明只有根控制器，将currentShowVC 清空，为了下面的方法禁用侧滑手势
 _currentvc = Nil;
 }
 else{
 //将push进来的视图控制器赋值给currentShowVC
 _currentvc = viewController;
 }
 }*/

//这个方法是在手势将要激活前调用：返回YES允许侧滑手势的激活，返回NO不允许侧滑手势的激活
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    //首先在这确定是不是我们需要管理的侧滑返回手势
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        if (self.viewControllers.count > 1) {
            //如果堆栈内的控制器数量大于 1 ，允许激活侧滑手势
            return YES;
        }
        //禁用侧滑手势
        return NO;
    }
    //这里就是非侧滑手势调用的方法.统一允许激活
    return YES;
}

//获取侧滑返回手势
- (UIScreenEdgePanGestureRecognizer *)panGesture {
    UIScreenEdgePanGestureRecognizer *panGesture = nil;
    if (self.view.gestureRecognizers.count > 0) {
        for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
            if ([recognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]]) {
                panGesture = (UIScreenEdgePanGestureRecognizer *)recognizer;
                break;
            }
        }
    }
    return panGesture;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
