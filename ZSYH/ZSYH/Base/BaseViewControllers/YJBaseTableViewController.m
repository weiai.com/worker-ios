//
//  YJBaseTableViewController.m
//  Courier
//
//  Created by lyj on 2018/5/8.
//  Copyright © 2018年 SIMPLE PLAN. All rights reserved.
//

#import "YJBaseTableViewController.h"

@interface YJBaseTableViewController ()

@property (nonatomic, strong) UIView *noDataView;
@property (nonatomic, strong) UIImageView *noDataImageView;
@property (nonatomic, strong) UILabel *noDataLabel;
@end

@implementation YJBaseTableViewController

- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataSourceArray;
}

- (NSMutableDictionary *)dataSourceDictionary {
    if (!_dataSourceDictionary) {
        _dataSourceDictionary = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return _dataSourceDictionary;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildSubviews {
    
}

- (void)setUpNavigationBar {
    [super setUpNavigationBar];
    //阴影
//    self.navBar.backView.layer.shadowColor = RGBColor_HEX(0xbbbbbb, 1).CGColor;
//    self.navBar.backView.layer.shadowOffset = CGSizeMake(0, 2);
//    self.navBar.backView.layer.shadowOpacity = 0.5;
    self.navBar.titleLabel.textColor = self.navBar.backgroundImageView.hidden ? [UIColor blackColor] : [UIColor whiteColor];
    self.navBar.backButton.selected = self.navBar.backgroundImageView.hidden ? NO : YES;
}

- (void)setShowNoDataView:(BOOL)showNoDataView {
    if (showNoDataView) {
        self.noDataView.alpha = 1.f;
    } else {
        self.noDataView.alpha = 0.f;
    }
}

- (UIView *)noDataView {
    if (!_noDataView) {
        _noDataView = [[UIView alloc] init];
        _noDataView.alpha = 0.f;
        _noDataView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_noDataView];
        [_noDataView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(kNaviHeight, 0, 0, 0));
        }];
        
        UIImageView *noDataImageView = [[UIImageView alloc] init];
        noDataImageView.backgroundColor = [UIColor clearColor];
        [_noDataView addSubview:noDataImageView];
        _noDataImageView = noDataImageView;
        _noDataImageView.image = _noDataImage;
        [noDataImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_centerY).mas_offset(kScaleNum(-5));
            make.centerX.equalTo(self.view.mas_centerX);
        }];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor lightGrayColor];
        titleLabel.font = kFont(14);
        _noDataLabel = titleLabel;
        _noDataLabel.text = _noDataString;
        [_noDataView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view.mas_centerY).mas_offset(kScaleNum(5));
            make.centerX.equalTo(self.view.mas_centerX).mas_offset(0);
            make.height.mas_equalTo(kScaleNum(20));
        }];
        
    }
    return _noDataView;
}

- (UITableView *)listTableView {
    if (!_listTableView) {
        UITableView *listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT) style:UITableViewStyleGrouped];
        listTableView.backgroundColor = [UIColor clearColor];
        listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        listTableView.showsVerticalScrollIndicator = NO;
        //自适应
        listTableView.estimatedRowHeight = 44;
        listTableView.estimatedSectionHeaderHeight = 44;
        listTableView.estimatedSectionFooterHeight = 44;
        listTableView.rowHeight = UITableViewAutomaticDimension;
        listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
        listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
        listTableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, CGFLOAT_MIN)];
        listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, YY_bottomViewHeight)];
        //缺省图
        listTableView.showNoData = YES;
        listTableView.customImg = [UIImage imageNamed:@""];
        listTableView.customMsg = @"暂无数据";
        listTableView.isShowBtn = NO;
        //listTableView.offsetY = CGFLOAT_MIN;
        _listTableView = listTableView;
        [self.view addSubview:_listTableView];
    }
    return _listTableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
