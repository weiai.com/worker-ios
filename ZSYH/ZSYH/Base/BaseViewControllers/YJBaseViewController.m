//
//  YJBaseViewController
//  DaXueZhang
//
//  Created by qiaoxuekui on 2018/7/11.
//  Copyright © 2018年 qiaoxuekui. All rights reserved.
//

#import "YJBaseViewController.h"


@interface YJBaseViewController ()

@end

@implementation YJBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.navigationController.childViewControllers.count != 1) {
        self.navBar.backButton.hidden=NO;
    }
    else{
        self.navBar.backButton.hidden=YES;
    }
    [self.navBar.backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    //    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    if (iOS11) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
    else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //子类继承
    [self buildSubviews];
    [self setUpNavigationBar];
    [self bindViewModel];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view bringSubviewToFront:self.navBar];
}

- (void)buildSubviews {
    
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = YES;
    [self.view addSubview:self.navBar];
    //主事丫环项目中需要原生导航栏
    self.navigationController.navigationBar.hidden = NO;
    self.navBar.hidden = YES;
}

- (void)bindViewModel {
    
}


- (void)goToLoginViewController {
    
}


-(DXNavBarView *)navBar{
    if (_navBar==nil) {
        _navBar=[[DXNavBarView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, kNaviHeight)];
    }
    return _navBar;
}

#pragma mark - table.set 可修改

-(void)viewDidLayoutSubviews
{
//    __block UITableView* table;
//    [self.view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//        if ([obj isKindOfClass:[UITableView class]]) {
//            table=obj;
//        }
//    }];
//    if ([table respondsToSelector:@selector(setSeparatorInset:)]) {
//        [table setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
//    }
//    
//    if ([table respondsToSelector:@selector(setLayoutMargins:)]) {
//        [table setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
//    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
//    {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
//    {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    SDWebImageManager *mgr = [SDWebImageManager sharedManager];
    // 1.取消正在下载的操作
    [mgr cancelAll];
    // 2.清除内存缓存
    [mgr.imageCache clearWithCacheType:SDImageCacheTypeAll completion:^{
        NSLog(@"清理缓存");
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
