//
//  YJBaseTableViewController.h
//  Courier
//
//  Created by lyj on 2018/5/8.
//  Copyright © 2018年 SIMPLE PLAN. All rights reserved.
//

#import "YJBaseViewController.h"
#import "UITableView+NoData.h"

@interface YJBaseTableViewController : YJBaseViewController

@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, strong) NSMutableArray *dataSourceArray;
@property (nonatomic, strong) NSMutableDictionary *dataSourceDictionary;
@property (nonatomic, assign) BOOL showNoDataView;
@property (nonatomic, strong) UIImage *noDataImage;
@property (nonatomic, copy) NSString *noDataString;

@end
