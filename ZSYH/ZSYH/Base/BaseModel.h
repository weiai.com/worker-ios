//
//  BaseModel.h
//  ZSYH
//
//  Created by 魏堰青 on 2019/2/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : NSObject
@property (nonatomic, copy) NSString *Id;

@end

NS_ASSUME_NONNULL_END
