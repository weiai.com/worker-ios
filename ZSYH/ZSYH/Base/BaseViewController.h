//
//  BaseViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate>

@property(nonatomic,strong) UIButton *rightbutton;
@property(nonatomic,strong) UIButton *leftbutton;

//cell缓存高度所用字典
@property(nonatomic,strong) NSMutableDictionary *heightAtIndexPath;


//设置tableview分割线距离左边的间距
- (void)leftSeparator:(CGFloat)length;

- (void)rightClick;

- (void)leftClick;

- (void)leftNavItem:(NSString *)img action:(SEL)action;

- (void)rightNavItem:(NSString *)img action:(SEL)action;
- (void)showOnleText:(NSString *)text delay:(NSTimeInterval)delay;

-(NSString *)StringAndData:(NSString *) str;//字符串转日期
- (UIColor *) colorWithHexString: (NSString *)color;//颜色转换
- (CGFloat)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(CGFloat)width;//计算lable高度
- (void)showTabBar;//显示tabbar
- (void)hideTabBar;//隐藏
-(NSString *)strwithdic:(NSDictionary *)dic;

@end

NS_ASSUME_NONNULL_END
