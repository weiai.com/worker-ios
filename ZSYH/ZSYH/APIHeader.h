//
//  APIHeader.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#ifndef APIHeader_h
#define APIHeader_h

//1 运行正式环境  0 运行测试环境
#if 1
/**
 修改证件照
 */
#define updateCertificateImgs @"/cs_user/updateCertificateImgs"

/**
 查看证件照
 */
#define getCertificateImgs @"/cs_user/getCertificateImgs"

/**
 维修工用户注册第一步接口（验证手机号是否注册以及邀请码是否正确）
 */
#define checkPhone @"/cs_user/checkPhone"

/**
维修工用户注册接口
 */
#define LRegin @"/cs_user/register"

/**
 登录接口
 */
#define Login @"/cs_user/dologin"

/**
 结束/开始听单
 */
#define LendBegin @"/cs_user/changeWorkState"
/**
 查询预约单信息
 */
#define LlistenList @"/cs_order/getAppOrdersInfo"

/**
 根据订单id获取订单详情
 */
#define LlistenDetaile @"/cs_shop/getOrdInfoById"

/**
 电器厂修改预约日期和时间
 */
#define dqcUpdateOrderTime @"/cs_order/dqcUpdateOrderTime"

/**
 发送抢预约单消息(抢单接口)
 */
#define LlistGrab @"/cs_order/sendQdGoodsMessage"
/**
获取产品类型信息
 */
#define MmallList @"/cs_shop/getProType"
/**
 获取banner
 */
#define Mbannerapi @"/cs_shop/getAllAdBanners"
/**
根据产品类型id找出相关产品
 */
#define MmallDetaileID @"/cs_shop/getProByType"

/**
 根据产品id获取产品相关信息
 */
#define MmallgoodsbuyID @"/cs_shop/getProInfoById"

/**
商城首页列表接口
 */
#define MmallHomeAD @"/cs_shop/showProduct"
/**
商铺首页接口
 */
#define shopHome @"/cs_shop/getAgentShopById"

/**
 添加购物车
 */
#define Maddcur @"/cs_shop/addCar"

/**
 查看个人信息
 */
#define CcentenInfo @"/cs_user/getMyInfo"

/**
我的订单（全部未分类）接口
 */
#define CmyOrderAll @"/cs_shop/getOrdInfoByUserId"

/**
 查询预约单信息
 */
#define CmyYuYueOrderAll @"/cs_order/getAppOrdersInfo"

/**
 我的购物车
 */
#define CmycurList @"/cs_shop/myShopCar"

/**
 我的客户
 */
#define Cmyperson @"/cs_user/getMypriLci"

/**
 获取所有维修订单的评价和评分
 */
#define CmyCommitList @"/cs_order/getAllrepEva"

/**
 我的银行卡列表
 */
#define CmyBankidList @"/cs_user/myBankCards"

/**
 维修工添加银行卡接口
 */
#define CmyAddBankCard @"/cs_user/AddBankCard"

/**
 维修工用户添加地址接口
 */
#define CmyAddaddress @"/cs_user/addAddress"

/**
维修工App修改地址接口
 */
#define cmyeditaddress @"/cs_user/modifyAddress"

/**
维修工App删除地址接口
 */
#define Cmyadeleaddress @"/cs_user/deleteAddress"

/**
 维修工我的地址
 */
#define CmyaddressList @"/cs_user/myAddress"

/**
用户注销
 */
#define CUserZhuXiao @"/cs_user/logout"

/**
 生成订单接口
 */
#define MmakeOrder @"/cs_shop/submitOrder"

/**
商城模糊查询接口
 */
#define MSearch @"/cs_shop/proSearch"

/**W
 获取所有的维修订单
 */
#define ClistW @"/cs_order/getAllrepOrders"

/**W
维修工拒绝指派给自己的预约单
 */
#define Listjujue @"/cs_order/refuseAppOrder"

/**W
 查看产品分类列表
 */
#define threeProductList @"/cs_order/getRepProTypes"

/**W
根据一级类型id获取子级配件类型
 */
#define erJIProductList @"/cs_order/getRepProTypesById"

/**W
维修工商城购物订单支付接口（支付宝）
 */
#define alipay @"/cs_pay/orderPay"

/**W
维修工商城购物订单支付接口（微信）
 */
#define wechat @"/cs_pay/wxRepOrderPay"

/**W
维修工我的账户
 */
#define myAccont @"/cs_user/myBalance"

/**W
获取银行卡类型
 */
#define getCardtype @"/cs_user/getBanks"

/**W
 更改维修工用户信息接口
 */
#define setUserInfo @"/cs_user/updateRepUser"

/**W
 维修工app获取头像接口
 */
#define setUserInfoIMge @"/cs_user/myImageUrl"

/**W
 维修工app获取头像接口
 */
#define setUserInfoIMge @"/cs_user/myImageUrl"

/**W
维修工app上传头像接口
 */
#define setUpUserImage @"/cs_user/uploadImage"

/**W
确定申请结算接口
 */
#define tixianAccont @"/cs_user/appSettlement"

/**W
 申请结算接口
 */
#define getCanAccont @"/cs_user/appSetMoney"

/**W
查看订单收益
 */
#define lookMyInco @"/cs_user/getMyEarnings"

/**W
生成检测报告
 */
#define sehncgehngList @"/cs_order/createTestReport"

/**W
 维修工订单收益统计接口
 */
#define myAccTongjiSY @"/cs_order/createTestReport"

/**W
 扫描二维码接口
 */
#define Saoyisao @"/cs_order/ScanQrCode"

/**W
维修工添加配件接口
 */
#define addPeiJian @"/cs_order/addRepProduct"

/**W
维修工订单收益统计接口
 */
#define shouyiTongji @"/cs_user/getCountMyEarnings"

/**W
维修工我的结算历史明细
 */
#define jieSuanHos @"/cs_user/mySettlement"

/**W
 维修工我的结算历史明细
 */
#define jieSuanHos @"/cs_user/mySettlement"

/**W
维修工我的结算统计
 */
#define jieSuantongji @"/cs_user/getCountSettlement"

/**W
维修工App查看账单接口

 */
#define lookListacc @"/cs_order/getBillById"

/**W
维修工App查看检测报告接口
 */
#define lookbaogao @"/cs_order/getTestReportById"

/**W
 发送短信验证码接口
 */
#define sendMsg @"/cs_user/sendTextMsg"

/**W
 
 */
#define lookshopOrderDet @"/cs_shop/getOrdInfoById"

/**W
更改手机号接口
 */
#define changePhone @"/cs_user/updateRepUserPhone"

/**W
 获取维修售后列表信息
 */
#define shouHouList @"/cs_order/getAllAfterOrder"

/**W
获取产品分类
 */
#define getfenlei @"/cs_shop/getProType"

/**W
 根据产品类型id找出相关产品
 */
#define getfenleiWithID @"/cs_shop/getProTypeByPid"

/**W
 上传身份证照片接口
 */
#define upphoneIdracd @"/cs_user/uploadIdCard"

/**W
 根据订单id获取订单详情
 */
#define weixuidContent @"/cs_order/getOrderInfoById"

/**W
确认和处理维修售后单状态
 */
#define changeErci @"/cs_order/confirmAndHandle"

/**W
维修售后更换配件接口
 */
#define weixuishouhouchenpru @"/cs_order/updateRepProduct"

/**W
获取帖子类型
 */
#define gettypetie @"/cs_user/getBbsTypes"

/**W
购买产品评价接口
 */
#define upcommit @"/cs_shop/evaluate"

/**W
 查看维修单评价接口
 */
#define lookcimmMail @"/cs_shop/getEvaluateByOrderId"

/**W
查看订单评价
 */
#define lookcimmMail @"/cs_shop/getEvaluateByOrderId"

/**W
 查看维修单评价接口
 */
#define lookcimmweixiu @"/cs_order/getRepOrderEvlById"

/**W
根据筛选条件获取帖子列表
 */
#define lookForumList @"/cs_user/getBbsList"

/**W
购买产品确认已收货接口
 */
#define querenShouHuo @"/cs_shop/hasGetPro"

/**W
 购买产品确认已收货接口
 */
#define fabuforum @"/cs_user/releaseBbs"

/**W
论坛消息接口
 */
#define foeNewsList @"/cs_user/getBbsMsg"

/**W
根据帖子id获取帖子详情
 */
#define forumContentApi @"/cs_user/getBbsInfoById"

/**W
 商铺产品分类排序
 */
#define mailHomeseleapi @"/cs_shop/getShopPro"

/**W
商城订单取消接口
 */
#define mailquxiaoOrder @"/cs_shop/cancelOrder"

/**W
维修工投诉列表接口
 */
#define getTousu @"/cs_user/myComplaint"

/**W
维修工推送账单接口
 */
#define tuisongzhanbgd @"/cs_pay/sendBill"

/**W
置换token接口
 */
#define changeToken @"/cs_user/replaceToken"

/**W
根据id获取投诉详细信息
 */
#define cousuContentlist @"/cs_user/complaintInfo"

/**W
轮播帖子接口
 */
#define lunboBanner @"/cs_user/getBbslb"

/**W
回复帖子接口
 */
#define huifuForum @"/cs_user/replyBbs"

/**W
已读消息处理接口
 */
#define isreadNewsapi @"/cs_order/readShMsg"

/**W
 已读消息处理接口
 */
#define isreadNewsapi @"/cs_order/readShMsg"

/**W
 已读消息处理接口
 */
#define isreadNewsapi @"/cs_order/readShMsg"

/**W
获取服务类型范围接口
 */
#define getsServiceFW @"/cs_user/showFaultType"

/**W
更改购物车中的物品状态
 */
#define genggaiCur @"/cs_shop/updateProState"

/**W
 购物车去结算接口
 */
#define getseetShopCar @"/cs_shop/seetShopCar"

/***
 微信绑定账户调用接口
 */
#define wxAppLogin @"/cs_pay/repairWxAppLogin"

/***
 支付宝绑定账户调用接口
 */
#define aliPayAppLogin @"/cs_pay/repairAliPayAppLogin"

/**W
论坛未读数量接口
 */
#define WgetIsReadCount @"/cs_user/getIsReadCount"

/**W
论坛消息已读接口
 */
#define WreadBBsReply @"/cs_user/readBBsReply"

/**W
个人维修生成账单接口
 */
#define OrderCreateBill @"/cs_order/createBill"

/**W
 候保产品订单列表接口
 */
#define HbRepOrderList @"/cs_order/getHbRepOrderList"

/**W
 候保产品订单处理接口
 */
#define delRepProduct @"/cs_order/delRepProduct"

/**W
 查看物流信息接口
 */
#define LOOKLogisticsInfo @"http://v.juhe.cn/exp/index"

/**W
 验证身份证信息接口
 */
#define checkIdCard @"/cs_user/checkIdCard"

/**W
 生成二维码接口
 */
#define myCodeImage @"/index.html#/Ewm_binding?repUserId="

/**W
 上传相关证件接口
 */
#define uploadLicense @"/cs_user/uploadLicense"

/**
 * 生成扫描安装单
 */
#define createScanOrder @"/cs_order/createScanOrder"
/**
 * 检测扫描配件是否已经安装过
 */
#define cs_orderCheckPartsAz @"/cs_order/checkPartsAz"

/**
 * 扫描安装单申请质保
 */
#define appGuaranteed @"/cs_order/appGuaranteed"

/**
 * 查看产品详情
 */
#define getPartsInfo @"/cs_user/getPartsInfo"

/**
 * 获取扫描安装单列表
 */
#define getScanOrders @"/cs_order/getScanOrders"

/**
 * 售后单配件申请质保
 */
#define afterAppGuar @"/cs_order/afterAppGuar"

/**
 * 售后单获取配件列表
 */
#define getRepPros @"/cs_order/getRepPros"
/**
 * 售后维修详情
 */
#define getAfterOrderById @"/cs_order/afterOrder/getAfterOrderById"

/**
 * 获取省
 */
#define getProvince @"/cs_user/getProvince"

/**
 * 获取市
 */
#define getSysDtAreasByParId @"/cs_user/getSysDtAreasByParId"

/**
 * 获取候保配件列表(立即订购,好物优选,新品推荐)
 */
#define getPartsList @"/cs_user/getPartsList"

/**
 *提交留言
 */
#define levelPartsMessage @"/cs_user/levelPartsMessage"

/**
 *我的留言列表
 */
#define myPartsMessage @"/cs_user/myPartsMessage"

/**
 * 获取购买产品接口
 */
#define getProductList @"/cs_shop/getProductList"

/**
 * 我的预订
 */
#define myOrderCar @"/cs_user/myOrderCar"

/***
 * 候保预订单 (我的订货单列表)
 */
#define myPartsOrders @"/cs_user/myPartsOrders"

/***
 * 取消预订
 */
#define cancelOrders @"/cs_user/cancelOrders"

/***
 * 修改数量
 */
#define updatePartsOrder @"/cs_user/updatePartsOrder"

/***
 * 获取订单详情
 */
#define getPartsOrderInfo @"/cs_user/getPartsOrderInfo"

/***
 * 补助金列表
 */
#define myGrantsList @"/cs_user/myGrantsList"

/***
 候保配件详情界面 图文详情 web地址
 */
#define partDetailImageTextWebUrl ([HOST_LINK isEqualToString:@"https://www.zzzsyh.com"])? @"https://www.zzzsyh.com/image_text.html?id=" :@"http://192.168.2.188/image_text.html?id="

/***
 配件商城 新
 */
#define GETPRODUCTLIST @"/cs_shop/getProductList"

/***
 检测费列表
 */
#define getTestOrders @"/cs_order/getTestOrders"

/***
 检测单详情
 */
#define getTestOrderById @"/cs_order/getTestOrderById"

/***
 获取所有的维修单列表
 */
#define getRepOrdersList @"/cs_order/getRepOrdersList"

/***
 配件绑定维修单
 */
#define bindingOrder @"/cs_order/bindingOrder"

/***
 扫码安装单 订单详情
 */
#define getScanOrderById @"/cs_order/getScanOrderById"

/***
 取消预订单
 */
#define cancelOrders @"/cs_user/cancelOrders"

/***
 加入预订车
 */
#define addOrder @"/cs_user/addOrder"

/***
 配件商城 搜索产品
 */
#define getPartsListSearch @"/cs_user/getPartsList"

/***
 生成检测单报告
 */
#define createTestOrderReport @"/cs_order/createTestOrderReport"

/***
 我的预订 - 立即订购提交/删除
 */
#define USER_DEL_ORDER_CAR @"/cs_user/delOrderCar"

/***
 查看检测单报告
 */
#define getTestReportByTestId @"/cs_order/getTestReportByTestId"

/***
 检测单查看账单
 */
#define getTestOrderById @"/cs_order/getTestOrderById"

/*
提交订购单数据接口
 */
#define USER_SUBMIT_ORDER_CAR @"/cs_user/submitOrderCar"

/*
 查看平台结算政策接口
 */
#define cs_userGetPolicy @"/cs_user/getPolicy"

/***
 维修工验证身份证接口
 */
#define checkIdCard @"/cs_user/checkIdCard"

/**W
 售后维修添加新配件接口
 */
#define updateRepProduct @"/cs_order/updateRepProduct"

/***
 配件已更换结束
 */
#define endAfterOrder @"/cs_order/endAfterOrder"

/***
 查询需上传的服务范围的选项
 */
#define getCertificateListByFaultId @"/cs_user/repUser/getCertificateListByFaultId"

/***
 支付宝,微信切换默认账户接口
 */
#define setDefaultTX @"/cs_user/setDefaultTX"

/***
 用户协议
 */
#define sifu_agreement @"/index.html#/sifu_agreement"

/***
 轮播图
 */
#define bannerImage ([HOST_LINK isEqualToString:@"https://www.zzzsyh.com"])? @"https://www.zzzsyh.com/image_view.html?id=" :@"http://192.168.2.188/image_view.html?id="

/***
 操作指南
 */
#define instructions @"/index.html#/instructions"

/***
 获取扫描安装单列表
 */
#define getScanOrders @"/cs_order/getScanOrders"

/***
 补助金申请列表
 */
#define getApplyGrantsListApp @"/cs_order/repairApplyGrants/getApplyGrantsListApp"

/***
根据补助金申请单id查询申请详情
 */
#define repairApplyGrantsGetApplyGrantsById @"/cs_order/repairApplyGrants/getApplyGrantsById"
/***
 获取扫描安装详情
 */
#define newGetScanOrderById @"/cs_order/getScanOrderById"

/***
 根据二维码qrcode查询配件是否可以申请补助金
 */
#define judgePartsStatus @"/cs_order/repairApplyGrants/judgePartsStatus"

/***
 扫描二维码获取配件详情
 */
#define getScanQrCode @"/cs_platform/factoryLogin/scanQrCode"

/***
 扫码申请补助金 提交申请 （JSON格式传参）
 */
#define applyGrantsApp @"/cs_order/repairApplyGrants/applyGrantsAppIos"

/***
 生成扫描安装单(JSON格式上传)
 */
#define createScanOrderIos @"/cs_order/createScanOrderIos"

/***
 上传图片 - 单图上传
 */
#define invoiceUploadImg @"/cs_order/invoice/uploadImg"

/***
 获取扫描安装详情
 */
#define getScanOrderById @"/cs_order/getScanOrderById"
/***
取消回寄配件
 */
#define repairApplyGrantsCancelReturn @"/cs_order/repairApplyGrants/cancelReturn"
/***
确认回寄配件
 */
#define repairApplyGrantsPartsReturn @"/cs_order/repairApplyGrants/partsReturn"

/***
 根据申请单id查询申请详情
 */
#define getApplyGrantsById @"/cs_order/repairApplyGrants/getApplyGrantsById"

/***
 根据申请单id获取扫描安装详情
 */
#define getScanOrderById @"/cs_order/getScanOrderById"

#else
/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - 测试接口

/***
 微信绑定账户调用接口
 */
#define wxAppLogin @":8088/cs_pay/repairWxAppLogin"

/***
 支付宝绑定账户调用接口
 */
#define aliPayAppLogin @":8088/cs_pay/repairAliPayAppLogin"

/**W
 论坛未读数量接口
 */
#define WgetIsReadCount @":8096/cs_user/getIsReadCount"

/**W
 论坛消息已读接口
 */
#define WreadBBsReply @":8096/cs_user/readBBsReply"

/**W
 个人维修生成账单接口
 */
#define OrderCreateBill @"/cs_order/createBill"

/**
 修改证件照
 */
#define updateCertificateImgs @"/cs_user/updateCertificateImgs"

/**
 查看证件照
 */
#define getCertificateImgs @"/cs_user/getCertificateImgs"

/**
 维修工用户注册第一步接口（验证手机号是否注册以及邀请码是否正确）
 */
#define checkPhone @"/cs_user/checkPhone"

/**
 维修工用户注册接口
 */
#define LRegin @":8096/cs_user/register"

/**
 登录接口
 */
#define Login @":8096/cs_user/dologin"

/**
 结束/开始听单
 */
#define LendBegin @":8096/cs_user/changeWorkState"
/**
 查询预约单信息
 */
#define LlistenList @":8094/cs_order/getAppOrdersInfo"

/**
 根据订单id获取订单详情
 */
#define LlistenDetaile @":8095/cs_shop/getOrdInfoById"

/**
 电器厂修改预约日期和时间
 */
#define dqcUpdateOrderTime @"/cs_order/dqcUpdateOrderTime"

/**
 发送抢预约单消息(抢单接口)
 */
#define LlistGrab @":8094/cs_order/sendQdGoodsMessage"

/**
 获取产品类型信息
 */
#define MmallList @":8095/cs_shop/getProType"

/**
 获取banner
 */
#define Mbannerapi @":8095/cs_shop/getAllAdBanners"

/**
 根据产品类型id找出相关产品
 */
#define MmallDetaileID @":8095/cs_shop/getProByType"

/**
 根据产品id获取产品相关信息
 */
#define MmallgoodsbuyID @":8095/cs_shop/getProInfoById"

/**
 商城首页列表接口
 */
#define MmallHomeAD @":8095/cs_shop/showProduct"

/**
 商铺首页接口
 */
#define shopHome @":8095/cs_shop/getAgentShopById"

/**
 添加购物车
 */
#define Maddcur @":8095/cs_shop/addCar"

/**
 查看个人信息
 */
#define CcentenInfo @":8096/cs_user/getMyInfo"

/**
 我的订单（全部未分类）接口
 */
#define CmyOrderAll @":8095/cs_shop/getOrdInfoByUserId"

/**
 查询预约单信息
 */
#define CmyYuYueOrderAll @":8094/cs_order/getAppOrdersInfo"

/**
 我的购物车
 */
#define CmycurList @":8095/cs_shop/myShopCar"

/**
 我的客户
 */
#define Cmyperson @":8096/cs_user/getMypriLci"

/**
 获取所有维修订单的评价和评分
 */
#define CmyCommitList @":8094/cs_order/getAllrepEva"

/**
 我的银行卡列表
 */
#define CmyBankidList @":8096/cs_user/myBankCards"

/**
 维修工添加银行卡接口
 */
#define CmyAddBankCard @":8096/cs_user/AddBankCard"

/**
 维修工用户添加地址接口
 */
#define CmyAddaddress @":8096/cs_user/addAddress"

/**
 维修工App修改地址接口
 */
#define cmyeditaddress @":8096/cs_user/modifyAddress"

/**
 维修工App删除地址接口
 */
#define Cmyadeleaddress @":8096/cs_user/deleteAddress"

/**
 维修工我的地址
 */
#define CmyaddressList @":8096/cs_user/myAddress"

/**
 用户注销
 */
#define CUserZhuXiao @":8096/cs_user/logout"

/**
 生成订单接口
 */
#define MmakeOrder @":8095/cs_shop/submitOrder"

/**
 商城模糊查询接口
 */
#define MSearch @":8095/cs_shop/proSearch"

/**W
 获取所有的维修订单
 */
#define ClistW @":8094/cs_order/getAllrepOrders"

/**W
 维修工拒绝指派给自己的预约单
 */
#define Listjujue @":8094/cs_order/refuseAppOrder"

/**W
 查看产品分类列表
 */
#define threeProductList @":8094/cs_order/getRepProTypes"

///**W
//获取配件类型（一级）
// */
//#define erJIProductList @":8094/cs_order/getRepProTypesById"

/**W
 根据一级类型id获取子级配件类型
 */
#define erJIProductList @":8094/cs_order/getRepProTypesById"

/**W
 维修工商城购物订单支付接口（支付宝）
 */
#define alipay @":8088/cs_pay/orderPay"

/**W
 维修工商城购物订单支付接口（微信）
 */
#define wechat @":8088/cs_pay/wxRepOrderPay"

/**W
 维修工我的账户
 */
#define myAccont @":8096/cs_user/myBalance"

/**W
 获取银行卡类型
 */
#define getCardtype @":8096/cs_user/getBanks"

/**W
 更改维修工用户信息接口
 */
#define setUserInfo @":8096/cs_user/updateRepUser"

/**W
 维修工app获取头像接口
 */
#define setUserInfoIMge @":8096/cs_user/myImageUrl"

/**W
 维修工app获取头像接口
 */
#define setUserInfoIMge @":8096/cs_user/myImageUrl"

/**W
 维修工app上传头像接口
 */
#define setUpUserImage @":8096/cs_user/uploadImage"

/**W
 确定申请结算接口
 */
#define tixianAccont @":8096/cs_user/appSettlement"

/**W
 申请结算接口
 */
#define getCanAccont @":8096/cs_user/appSetMoney"

/**W
 查看订单收益
 */
#define lookMyInco @":8096/cs_user/getMyEarnings"

/**W
 生成检测报告
 */
#define sehncgehngList @":8094/cs_order/createTestReport"

/**W
 维修工订单收益统计接口
 */
#define myAccTongjiSY @":8094/cs_order/createTestReport"

/**W
 扫描二维码接口
 */
#define Saoyisao @":8094/cs_order/ScanQrCode"

/**W
 维修工添加配件接口
 */
#define addPeiJian @":8094/cs_order/addRepProduct"

/**W
 维修工订单收益统计接口
 */
#define shouyiTongji @":8096/cs_user/getCountMyEarnings"

/**W
 维修工我的结算历史明细
 */
#define jieSuanHos @":8096/cs_user/mySettlement"

/**W
 维修工我的结算历史明细
 */
#define jieSuanHos @":8096/cs_user/mySettlement"

/**W
 维修工我的结算统计
 */
#define jieSuantongji @":8096/cs_user/getCountSettlement"

/**W
 维修工App查看账单接口
 */
#define lookListacc @":8094/cs_order/getBillById"

/**W
 维修工App查看检测报告接口
 */
#define lookbaogao @":8094/cs_order/getTestReportById"

/**W
 发送短信验证码接口
 */
#define sendMsg @":8096/cs_user/sendTextMsg"

/**W
 发送短信验证码接口
 */
#define lookshopOrderDet @":8095/cs_shop/getOrdInfoById"

/**W
 更改手机号接口
 */
#define changePhone @":8096/cs_user/updateRepUserPhone"

/**W
 获取维修售后列表信息
 */
#define shouHouList @":8094/cs_order/getAllAfterOrder"

/**W
 获取产品分类
 */
#define getfenlei @":8095/cs_shop/getProType"

/**W
 根据产品类型id找出相关产品
 */
#define getfenleiWithID @":8095/cs_shop/getProTypeByPid"

/**W
 上传身份证照片接口
 */
#define upphoneIdracd @":8096/cs_user/uploadIdCard"

/**W
 根据订单id获取订单详情
 */
#define weixuidContent @":8094/cs_order/getOrderInfoById"

/**W
 确认和处理维修售后单状态
 */
#define changeErci @":8094/cs_order/confirmAndHandle"

/**W
 维修售后更换配件接口
 */
#define weixuishouhouchenpru @":8094/cs_order/updateRepProduct"

/**W
 获取帖子类型
 */
#define gettypetie @":8096/cs_user/getBbsTypes"

/**W
 购买产品评价接口
 */
#define upcommit @":8095/cs_shop/evaluate"

/**W
 查看维修单评价接口
 */
#define lookcimmMail @":8095/cs_shop/getEvaluateByOrderId"

/**W
 查看订单评价
 */
#define lookcimmMail @":8095/cs_shop/getEvaluateByOrderId"

/**W
 查看维修单评价接口
 */
#define lookcimmweixiu @":8094/cs_order/getRepOrderEvlById"

/**W
 根据筛选条件获取帖子列表
 */
#define lookForumList @":8096/cs_user/getBbsList"

/**W
 购买产品确认已收货接口
 */
#define querenShouHuo @":8095/cs_shop/hasGetPro"

/**W
 购买产品确认已收货接口
 */
#define fabuforum @":8096/cs_user/releaseBbs"

/**W
 论坛消息接口
 */
#define foeNewsList @":8096/cs_user/getBbsMsg"

/**W
 根据帖子id获取帖子详情
 */
#define forumContentApi @":8096/cs_user/getBbsInfoById"

/**W
 商铺产品分类排序
 */
#define mailHomeseleapi @":8095/cs_shop/getShopPro"

/**W
 商城订单取消接口
 */
#define mailquxiaoOrder @":8095/cs_shop/cancelOrder"

/**W
 维修工投诉列表接口
 */
#define getTousu @":8096/cs_user/myComplaint"

/**W
 维修工推送账单接口
 */
#define tuisongzhanbgd @":8088/cs_pay/sendBill"

/**W
 置换token接口
 */
#define changeToken @":8096/cs_user/replaceToken"

/**W
 根据id获取投诉详细信息
 */
#define cousuContentlist @":8096/cs_user/complaintInfo"

/**W
 轮播帖子接口
 */
#define lunboBanner @":8096/cs_user/getBbslb"

/**W
 回复帖子接口
 */
#define huifuForum @":8096/cs_user/replyBbs"

/**W
 已读消息处理接口
 */
#define isreadNewsapi @":8094/cs_order/readShMsg"

/**W
 已读消息处理接口
 */
#define isreadNewsapi @":8094/cs_order/readShMsg"

/**W
 已读消息处理接口
 */
#define isreadNewsapi @":8094/cs_order/readShMsg"

/**W
 获取服务类型范围接口
 */
#define getsServiceFW @":8096/cs_user/showFaultType"

/**W
购物车去结算接口
 */
#define getseetShopCar @":8095/cs_shop/seetShopCar"

/**W
 购物车去结算接口
 */
#define getseetShopCar @":8095/cs_shop/seetShopCar"

/**W
更改购物车中的物品状态
*/
#define genggaiCur @":8095/cs_shop/updateProState"

/**W
 候保产品订单列表接口
 */
#define HbRepOrderList @":8094/cs_order/getHbRepOrderList"

/**W
 
 候保产品订单处理接口
 */
#define delRepProduct @":8094/cs_order/delRepProduct"

/**W
 查看物流信息接口
 */
#define LOOKLogisticsInfo @"http://v.juhe.cn/exp/index"

/**W
验证身份证信息接口
 */
#define checkIdCard @"/cs_user/checkIdCard"

/**W
 生成二维码接口
 */
#define myCodeImage @"/index.html#/Ewm_binding?repUserId="

/**
 上传相关证件接口
 */
#define uploadLicense @":8096/cs_user/uploadLicense"

/**
 * 生成扫描安装单
 */
#define createScanOrder @":8094/cs_order/createScanOrder"

/**
 * 扫描安装单申请质保
 */
#define appGuaranteed @":8096/cs_order/appGuaranteed"

/**
 * 查看配件详情
 */
#define getPartsInfo @":8096/cs_user/getPartsInfo"

/**
 * 获取扫描安装单列表
 */
#define getScanOrders @":8094/cs_order/getScanOrders"

/**
 * 售后单配件申请质保
 */
#define afterAppGuar @":8094/cs_order/afterAppGuar"

/**
 * 售后单获取配件列表
 */
#define getRepPros @":8094/cs_order/getRepPros"

/**
 * 获取省
 */
#define getProvince @":8080/cs_user/getProvince"

/**
 * 获取市
 */
#define getSysDtAreasByParId @":8080/cs_user/getSysDtAreasByParId"

/**
 * 获取候保配件列表(立即订购,好物优选,新品推荐)
 */
#define getPartsList @":8096/cs_user/getPartsList"

/**
 *提交留言
 */
#define levelPartsMessage @":8096/cs_user/levelPartsMessage"

/**
 *我的留言列表
 */
#define myPartsMessage @":8096/cs_user/myPartsMessage"

/**
 * 获取购买产品接口
 */
#define getProductList @":8095/cs_shop/getProductList"

/**
 * 我的预订
 */
#define myOrderCar @":8096/cs_user/myOrderCar"

/***
 * 候保预订单 (我的订货单列表)
 */
#define myPartsOrders @":8096/cs_user/myPartsOrders"

/***
 * 取消预订
 */
#define cancelOrders @":8096/cs_user/cancelOrders"

/***
 * 修改数量
 */
#define updatePartsOrder @":8096/cs_user/updatePartsOrder"

/***
 * 获取订单详情
 */
#define getPartsOrderInfo @":8096/cs_user/getPartsOrderInfo"

/***
 * 补助金列表
 */
#define myGrantsList @":8096/cs_user/myGrantsList"

/***
 配件商城 新
 */
#define GETPRODUCTLIST @":8095/cs_shop/getProductList"

/***
 检测费列表
 */
#define getTestOrders @":8094/cs_order/getTestOrders"

/***
 检测单详情
 */
#define getTestOrderById @":8094/cs_order/getTestOrderById"

/***
 获取所有的维修单列表
 */
#define getRepOrdersList @":8094/cs_order/getRepOrdersList"

/***
 配件绑定维修单
 */
#define bindingOrder @":8094/cs_order/bindingOrder"

/***
 扫码安装单 订单详情
 */
#define getScanOrderById @"cs_order/getScanOrderById"

/***
 取消预订单
 */
#define cancelOrders @":8096/cs_user/cancelOrders"

/***
 加入预订车
 */
#define addOrder @":8096/cs_user/addOrder"

/***
 配件商城 搜索产品
 */
#define getPartsListSearch @":8096/cs_user/getPartsList"

/***
 生成检测单报告
 */
#define createTestOrderReport @":8094/cs_order/createTestOrderReport"

/***
 我的预订 - 立即订购提交/删除
 */
#define USER_DEL_ORDER_CAR @"/cs_user/delOrderCar"

/***
 查看检测单报告
 */
#define getTestReportByTestId @":8094/cs_order/getTestReportByTestId"

/***
 检测单查看账单
 */
#define getTestOrderById @":8094/cs_order/getTestOrderById"

/****
 提交订购单数据接口
 */
#define USER_SUBMIT_ORDER_CAR @"/cs_user/submitOrderCar"

/***
 维修工验证身份证接口
 */
#define checkIdCard @"8096/cs_user/checkIdCard"

/**W
 售后维修添加新配件接口
 */
#define updateRepProduct @"8094/cs_order/updateRepProduct"

/***
 配件已更换结束
 */
#define endAfterOrder @"/cs_order/endAfterOrder"

/***
 查询需上传的服务范围的选项
 */
#define getCertificateListByFaultId @"/cs_user/repUser/getCertificateListByFaultId"

/***
 支付宝,微信切换默认账户接口
 */
#define setDefaultTX @"/cs_user/setDefaultTX"

/***
 用户协议
 */
#define sifu_agreement @"/index.html#/sifu_agreement"

/***
 轮播图
 */
#define bannerImage @"/image_view.html?id="

/***
 操作指南
 */
#define instructions @"/index.html#/instructions"

#endif

#endif /* APIHeader_h */
