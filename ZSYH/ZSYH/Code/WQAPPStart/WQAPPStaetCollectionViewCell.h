//
//  WQAPPStaetCollectionViewCell.h
//  APPStart
//
//  Created by 孙满 on 2017/7/28.
//  Copyright © 2017年 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WQAPPStaetCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIButton *finishBtn;
@end
