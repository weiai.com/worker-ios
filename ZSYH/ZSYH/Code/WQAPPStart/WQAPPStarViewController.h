//
//  WQAPPStarViewController.h
//  APPStart
//
//  Created by 孙满 on 2017/7/28.
//  Copyright © 2017年 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WQAPPStaetCollectionViewCell.h"
typedef void(^ZJLeadingPageSetupCellHandler)(WQAPPStaetCollectionViewCell *cell, NSIndexPath *indexPath);
typedef void(^ZJLeadingPageFinishHandler)(UIButton *finishBtn);


@interface WQAPPStarViewController : UIViewController
- (instancetype)initWithPagesCount:(NSInteger)count setupCellHandler:(ZJLeadingPageSetupCellHandler)setupCellHandler finishHandler:(ZJLeadingPageFinishHandler)finishHandler ;
@property (strong, nonatomic, readonly) UIPageControl *pageControl;
@property (strong, nonatomic, readonly) UICollectionView *collectionView;
@property (assign, nonatomic, readonly) NSInteger count;


@end
