//
//  CSBuyAndAddView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSBuyAndAddView.h"

@implementation CSBuyAndAddView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"CSBuyAndAddView" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;
        [self showUI];
    }
    return self;
}

-(void)showUI{

    self.bgview.layer.cornerRadius = 2;
    self.bgview.layer.masksToBounds = YES;
    self.bgview.layer.borderWidth= 1;
    self.bgview.layer.borderColor = [UIColor colorWithHexString:@"#707070"].CGColor;
    UILabel *leftlb = [[UILabel alloc]initWithFrame:CGRectMake(self.bgview.width/3-0.5, 0, 1, self.bgview.height )];
    leftlb.backgroundColor = [UIColor colorWithHexString:@"#707070"];
    [self.bgview addSubview:leftlb];
    UILabel *rightlb = [[UILabel alloc]initWithFrame:CGRectMake(self.bgview.width/3*2-0.5, 0, 1, self.bgview.height )];
    rightlb.backgroundColor = [UIColor colorWithHexString:@"#707070"];
    [self.bgview addSubview:rightlb];
    
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)quxiao:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(0);
    }
}


- (IBAction)minumAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(1);
    }
}
- (IBAction)addAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(2);
    }
}
- (IBAction)bottomAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(3);
    }
}
- (IBAction)phoneAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(4);
    }
}


@end
