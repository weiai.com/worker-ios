//
//  ZYBuyNowShopHeaderView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ZYBuyNowShopHeaderViewCategoryType) {
    ZYBuyNowShopHeaderViewCategoryTypeNew = 0,      //最新
    ZYBuyNowShopHeaderViewCategoryTypeBest          //优选
};
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZYBuyNowShopHeaderViewDeletate <NSObject>
- (void)buyNowShopHeaderViewClick:(ZYBuyNowShopHeaderViewCategoryType)type;
@end

@interface ZYBuyNowShopHeaderView : UIView
@property (nonatomic, weak) id <ZYBuyNowShopHeaderViewDeletate> delegate;

@end

NS_ASSUME_NONNULL_END
