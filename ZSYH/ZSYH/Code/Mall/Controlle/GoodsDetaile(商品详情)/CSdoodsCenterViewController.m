//
//  CSdoodsCenterViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSdoodsCenterViewController.h"
#import "CSdoodsCommitView.h"
#import "CSgoodsShopView.h"
#import "CSGoodsModel.h"
#import "CSgoodscommitModel.h"
#import "CSBannerModel.h"
#import "CSShopViewController.h"
#import "CSshopModel.h"

@interface CSdoodsCenterViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong) XSQBanner *banner;
@property (nonatomic,assign) BOOL isrefa;
@property(nonatomic,strong)CSGoodsModel *modlel;
@property(nonatomic,strong)NSMutableArray *commitArr;
@property(nonatomic,strong)NSMutableArray *imageArr;
@property(nonatomic,strong)CSshopModel *shopModel;
@property(nonatomic,strong)NSMutableArray *shopProductArr;

@end

@implementation CSdoodsCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.isrefa  = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.commitArr = [NSMutableArray arrayWithCapacity:1];
    self.imageArr = [NSMutableArray arrayWithCapacity:1];
    self.shopProductArr = [NSMutableArray arrayWithCapacity:1];
    
    [self requestDetaile];
}

-(void)requestDetaile{
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = NOTNIL(_goodsID);
    mudic[@"type"] = @"0";
    
    kWeakSelf;
    [NetWorkTool POST:MmallgoodsbuyID param:mudic success:^(id dic) {
       
        weakSelf.modlel = [CSGoodsModel mj_objectWithKeyValues:dic[@"data"][@"product"]];
       
        self.commitArr = [CSgoodscommitModel mj_keyValuesArrayWithObjectArray:dic[@"data"][@"evaluates"]];
        self.imageArr = [CSBannerModel mj_keyValuesArrayWithObjectArray:dic[@"data"][@"imgs"]];
        self.shopProductArr = [CSGoodsModel mj_keyValuesArrayWithObjectArray:dic[@"data"][@"proList"]];
        
        self.shopModel = [CSshopModel mj_objectWithKeyValues:dic[@"data"][@"shop"]];
        [self showdetaile];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)showdetaile{
    [self.view addSubview:self.scrollView];
    
    __block CGFloat heig = 0;
    CGFloat hei = [NSString heightWithWidth:KWIDTH-32 font:14 text:_modlel.productInfo];
    UILabel *miaoshulb = [[UILabel alloc]initWithFrame:CGRectMake(16, heig+20, KWIDTH-32, hei)];
    miaoshulb.backgroundColor = [UIColor whiteColor];
    miaoshulb.textAlignment = NSTextAlignmentCenter;
    miaoshulb.font = FontSize(14);
    miaoshulb.text =_modlel.productInfo;
    miaoshulb.numberOfLines = 0;
    miaoshulb.textColor = [UIColor colorWithHexString:@"#0D0E10"];
    [self.scrollView addSubview:miaoshulb];
    
    //    heig = miaoshulb.bottom+9;
    //
    //    if (_imageArr.count >0) {
    //
    //        //        for (CSBannerModel *model in _imageArr) {
    //        for (int i = 0; i < _imageArr.count; i++) {
    //            CSBannerModel *model =_imageArr[i];
    //
    //            UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, KWIDTH)];
    //            mysdimg(image, model.product_img_url);
    //
    //            [self.scrollView addSubview:image];
    //            heig =image.bottom;
    //        }
    //    }
    //    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);
    
    heig = miaoshulb.bottom+9;
    KMyLog(@"height0 = %f",heig);
    if (_imageArr.count >0) {
        
        for (int i = 0; i < _imageArr.count; i++) {
            CSBannerModel *model =_imageArr[i];
            UIImageView *imgv = [[UIImageView alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, KHEIGHT)];
            
            [imgv sd_setImageWithURL:[NSURL URLWithString:model.product_img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
                UIImage *newImage = [self imageCompressForWidth:imgv.image targetWidth:ScreenW];
                imgv.frame = CGRectMake(0, heig, newImage.size.width, newImage.size.height);
                
                [self.scrollView addSubview:imgv];
                heig = imgv.bottom;
                KMyLog(@"height0++%d = %f",i,heig);
                
                if (i == self.imageArr.count-1) {
                    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);
                }
            }];
        }
    }
    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);

    //    //创建产品详情
    //    [self.scrollView addSubview:self.banner];
    //    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    //    [muarr addObject:_modlel.productPic];
    //    for (CSBannerModel *model in _imageArr) {
    //        [muarr addObject:model.product_img_url];
    //    }
    //    if (muarr.count <1) {
    //        [muarr addObject:_modlel.productPic];
    //    }
    //    [_banner updateImageArr:muarr AndImageClickBlock:^(NSInteger index) {
    //    }];
    //    CGFloat hei =_banner.bottom;
    //
    //    if (hei == 0) {
    //        hei = kNaviHeight;
    //    }
    //    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(10, hei+10, KWIDTH-20, 40)];
    //    name.textColor = KTEXTCOLOR;
    //    name.font = FontSize(18);
    //    name.text = _modlel.productName;
    //    name.numberOfLines = 2;
    //    [name sizeToFit];
    //    [self.scrollView addSubview:name];
    //    UILabel *price = [[UILabel alloc]initWithFrame:CGRectMake(10, name.bottom+10, KWIDTH-160, 20)];
    //    price.textColor = RGBA(252, 23, 39,1);
    //    price.font = FontSize(18);
    //    price.text =[NSString stringWithFormat:@"¥%@",_modlel.productPrice];
    //    [self.scrollView addSubview:price];
    //    self.scrollView.contentSize = CGSizeMake(KWIDTH,price.bottom+20);
    //
    //    UILabel *pingtaiLB = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-140, name.bottom+10, 120, 20)];
    //    pingtaiLB.textColor = [UIColor colorWithHexString:@"#FEC351"];
    //    pingtaiLB.textAlignment = NSTextAlignmentCenter;
    //    pingtaiLB.font = FontSize(13);
    //    pingtaiLB.text =[NSString stringWithFormat:@"平台直接提供保障"];
    //    pingtaiLB.backgroundColor = [UIColor colorWithHexString:@"#F2E8D5"];
    //
    //    //    if ([_modlel.productBelType integerValue] == 1) {
    //    [self.scrollView addSubview:pingtaiLB];
    //
    //    //    }
    //    UILabel *con = [[UILabel alloc]initWithFrame:CGRectMake(0, pingtaiLB.bottom+10, KWIDTH, 30)];
    //    con.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //    [self.scrollView addSubview:con];
    //    self.scrollView.contentSize = CGSizeMake(KWIDTH,con.bottom+20);
    //
    //    CGFloat ffhei = 0;
    //    for (int i = 0; i<_imageArr.count; i++) {
    //        CSBannerModel *mode = _imageArr[i];
    //
    //        NSString *strurl=mode.product_img_url;
    //        UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(0, pingtaiLB.bottom+10+KWIDTH*i, KWIDTH, KWIDTH)];
    //        [self.scrollView addSubview:imagev];
    //
    //        if ([strurl hasPrefix:@"http"]) {
    //            [imagev sd_setImageWithURL:[NSURL URLWithString:strurl] placeholderImage:defaultImg];
    //        }else{
    //            sdimg(imagev, strurl);
    //
    //        }
    //        ffhei =imagev.bottom;
    //    }
    //    NSString *tit = _modlel.productInfo;
    //    CGFloat sdffs = [NSString heightWithWidth:KWIDTH-32 font:14 text:tit];
    //    UILabel *conttitle = [[UILabel alloc]initWithFrame:CGRectMake(16, ffhei+16, KWIDTH-32, sdffs)];
    //    conttitle.font = FontSize(14);
    //    conttitle.text = tit;
    //    conttitle.numberOfLines = 0;
    //    [self.scrollView addSubview:conttitle];
    //
    //    self.scrollView.contentSize = CGSizeMake(KWIDTH,conttitle.bottom+20);
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark +++获取图片高度
- (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth{
    
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = height / (width / targetWidth);
    CGSize size = CGSizeMake(targetWidth, targetHeight);
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    UIGraphicsEndImageContext();
    return newImage;
}

@end
