//
//  WQgoodsInfoViewController.m
//  ZengBei
//
//  Created by 魏堰青 on 2018/12/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "WQgoodsInfoViewController.h"
#import "WQgoodsInfoTableViewCell.h"
//#import "SpecPropModel.h"

#import "CSSpecListModel.h"

@interface WQgoodsInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;

@end


@implementation WQgoodsInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGBA(0, 0, 0, 0.5);
    // Do any additional setup after loading the view.
    CGFloat fff = 49;
    if (IPHONE_X) {
        fff = 83;
    }
    
    
    UIView *toughview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 150+kNaviHeight)];
    [self.view addSubview:toughview];
    toughview.backgroundColor = [UIColor clearColor];
    toughview.userInteractionEnabled = YES;
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gesAcction)];
    [toughview addGestureRecognizer:ges];
    
    self.myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 150+kNaviHeight, KWIDTH, KHEIGHT - 150-fff-kNaviHeight+10) style:(UITableViewStylePlain)];
    [self.view addSubview:self.myTableView];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 10)];;
    self.myTableView.estimatedRowHeight = 90;
    self.myTableView.rowHeight = UITableViewAutomaticDimension;
    self.myTableView.backgroundColor = [UIColor whiteColor];
    [self.myTableView registerNib:[UINib nibWithNibName:@"WQgoodsInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"WQgoodsInfoTableViewCell"];
//    self.myTableView.bounces = NO;

    UIView *canshuVI = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 50)];
    canshuVI.backgroundColor = [UIColor whiteColor];
    UILabel *contentLb = [[UILabel alloc]init];
    contentLb.frame =CGRectMake(10, 0, KWIDTH-20, 50 );
    contentLb.text = @"产品参数";
    contentLb.font = [UIFont systemFontOfSize:16];
    contentLb.textAlignment = NSTextAlignmentCenter;
    contentLb.textColor = KTEXTCOLOR;
    [canshuVI addSubview:contentLb];
    
  
         self.myTableView.layer.cornerRadius = 8;
         self.myTableView.layer.masksToBounds = YES;
    self.myTableView.tableHeaderView =canshuVI;
    
    
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut addTarget:self action:@selector(yesActon:) forControlEvents:UIControlEventTouchUpInside];
    [yesBut setBackgroundColor:RGBA(251, 89, 10, 1)];
    [yesBut setBackgroundColor:KBColor];

    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 15;
    UIView *myView = [[UIView alloc]initWithFrame:CGRectMake(0, KHEIGHT-fff, KWIDTH, fff)];
    myView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:myView];
    [myView addSubview:yesBut];

    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.right.offset(-10);
        make.bottom.offset(-fff+39);
        make.height.offset(30);
    }];
    
    
    
    
}

/**
 *  设置部分圆角(相对布局)
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect {

    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];

    self.view.layer.mask = shape;

}
-(void)yesActon:(UIButton *)but{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)gesAcction{
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
       NSInteger ind = 100;

    return ind;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WQgoodsInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WQgoodsInfoTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    SpecPropModel *mod = _model.goods_info.attr[indexPath.row];
    cell.myLeftLB.text = [NSString stringWithFormat:@"%@",mod.name];
    
    cell.myRightLB.text = [NSString stringWithFormat:@"%@",mod.value];
    
    
    
    
    
    
    return cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
