//
//  ZYBuyNowShopDetailStoreCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYShopPartDetailModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol ZYBuyNowShopDetailStoreCellDelegate <NSObject>
//点击某个配件
- (void)shopDetailStoreCellClickPart:(NSString *)partID;
@end

@interface ZYBuyNowShopDetailStoreCell : UITableViewCell
@property (nonatomic, strong) ZYShopPartDetailModel *partModel;
@property (nonatomic, weak) id <ZYBuyNowShopDetailStoreCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
