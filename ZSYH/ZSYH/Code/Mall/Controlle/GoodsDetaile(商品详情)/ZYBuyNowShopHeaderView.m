//
//  ZYBuyNowShopHeaderView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopHeaderView.h"

@implementation ZYBuyNowShopHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局方法
- (void)setupViewAction{
    self.backgroundColor = kColorWithHex(0xffffff);
    
    UIButton *btnNew = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNew.frame = CGRectMake((kScreen_Width - kScaleNum(360)) / 2.0, 10, kScaleNum(180), kScaleNum(180));
    btnNew.tag = 200;
    [btnNew setImage:imgname(@"shop_new_icon") forState:UIControlStateNormal];
    [btnNew addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnNew];
    
    UIButton *btnBest = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBest.frame = CGRectMake(CGRectGetMaxX(btnNew.frame), 10, kScaleNum(180), kScaleNum(180));
    btnBest.tag = 201;
    [btnBest setImage:imgname(@"shop_best_icon") forState:UIControlStateNormal];
    [btnBest addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnBest];
    
    UIImageView *ivHot = [[UIImageView alloc] initWithFrame:CGRectMake(btnNew.origin.x, CGRectGetMaxY(btnNew.frame) + 13, 20, 20)];
    ivHot.image = imgname(@"shop_hot_icon");
    [self addSubview:ivHot];
    
    UILabel *lblHot = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ivHot.frame) + 10, CGRectGetMaxX(btnNew.frame) + 12, 100, 22)];
    lblHot.text = @"热门推荐";
    lblHot.font = [UIFont systemFontOfSize:16];
    lblHot.textColor = kColorWithHex(0x333333);
    [self addSubview:lblHot];
}
#pragma mark - 交互事件
- (void)btnClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(buyNowShopHeaderViewClick:)]) {
        if (tag == 200) {
            //最新上架
            [self.delegate buyNowShopHeaderViewClick:ZYBuyNowShopHeaderViewCategoryTypeNew];
        }else if (tag == 201){
            //好物优选
            [self.delegate buyNowShopHeaderViewClick:ZYBuyNowShopHeaderViewCategoryTypeBest];
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
