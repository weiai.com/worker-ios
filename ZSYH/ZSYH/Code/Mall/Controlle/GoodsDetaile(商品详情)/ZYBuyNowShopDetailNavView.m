//
//  ZYBuyNowShopDetailNavView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopDetailNavView.h"
@interface ZYBuyNowShopDetailNavView ()
@property (nonatomic, strong) UIView *viewPage;//滑块
@property (nonatomic, assign) UIButton *btnSelect;//选中

@end

@implementation ZYBuyNowShopDetailNavView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局方法
- (void)setupViewAction{
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, kNaviHeight - 44, 44, 44);
    btnBack.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [btnBack setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBackClickAction:) forControlEvents:UIControlEventTouchUpInside];
    btnBack.tag = 202;
    [self addSubview:btnBack];
    
    _btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnLeft.frame = CGRectMake((self.width - 90) / 2.0, self.height - 44, 40, 44);
    [_btnLeft setTitle:@"产品" forState:UIControlStateNormal];
    [_btnLeft setTitleColor:kColorWithHex(0x333333) forState:UIControlStateNormal];
    [_btnLeft setTitleColor:kColorWithHex(0x70be68) forState:UIControlStateSelected];
    _btnLeft.titleLabel.font = [UIFont systemFontOfSize:18];
    _btnLeft.tag = 200;
    _btnLeft.selected = YES;
    _btnSelect = _btnLeft;
    [_btnLeft addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_btnLeft];
    
    _btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnRight.frame = CGRectMake(CGRectGetMaxX(_btnLeft.frame) + 18, self.height - 44, 40, 44);
    [_btnRight setTitle:@"详情" forState:UIControlStateNormal];
    [_btnRight setTitleColor:kColorWithHex(0x333333) forState:UIControlStateNormal];
    [_btnRight setTitleColor:kColorWithHex(0x70be68) forState:UIControlStateSelected];
    _btnRight.titleLabel.font = [UIFont systemFontOfSize:18];
    _btnRight.tag = 201;
    [_btnRight addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_btnRight];
    
    _viewPage = [[UIView alloc]initWithFrame:CGRectMake((self.width - 90) / 2.0, self.height - 1, 40, 1)];
    _viewPage.backgroundColor = kColorWithHex(0x70be68);
    [self addSubview:_viewPage];
    
    UIButton *btnOrderCar = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOrderCar.frame = CGRectMake(self.width - 48, self.height - 37, 30, 30);
    btnOrderCar.tag = 203;
    [btnOrderCar setImage:imgname(@"shop_car_icon") forState:UIControlStateNormal];
    [btnOrderCar addTarget:self action:@selector(btnOrderCarClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnOrderCar];
    
}
#pragma mark - 交互事件
//返回按钮点击
- (void)btnBackClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(navViewBtnClick:)]) {
        [self.delegate navViewBtnClick:tag];
    }
}
//标题点击
- (void)btnClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    NSInteger selectedTag = self.btnSelect.tag;
    if (selectedTag == tag) {
        //点击的是同一个按钮
    } else {
        self.btnSelect.selected = NO;
        sender.selected = YES;
        self.btnSelect = sender;
        kWeakSelf;
        if (tag == 200) {
            [UIView animateWithDuration:0.3 animations:^{
                weakSelf.viewPage.x = weakSelf.btnLeft.x;
            }];
        }else if (tag == 201){
            [UIView animateWithDuration:0.3 animations:^{
                weakSelf.viewPage.x = weakSelf.btnRight.x;
            }];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(navViewBtnClick:)]) {
            [self.delegate navViewBtnClick:tag];
        }
    }
}
- (void)btnOrderCarClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(navViewBtnClick:)]) {
        [self.delegate navViewBtnClick:tag];
    }
    KMyLog(@"您点击了 产品 详情 购物车按钮");
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
