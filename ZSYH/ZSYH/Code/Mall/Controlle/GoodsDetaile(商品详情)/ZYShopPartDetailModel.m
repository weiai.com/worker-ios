//
//  ZYShopPartDetailModel.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYShopPartDetailModel.h"

@implementation ZYShopPartDetailModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"attribute":@"ZYShopPartTypeModel",
             @"lbImg":[ZYShopPartCycleModel class]
             };
}
@end

@implementation ZYShopPartTypeModel

@end

@implementation ZYShopStoreInfoModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{@"parts":[ZYShopPartModel class]};
}
@end

@implementation ZYShopPartCycleModel

@end
