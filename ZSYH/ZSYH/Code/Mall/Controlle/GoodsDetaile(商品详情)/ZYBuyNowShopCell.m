//
//  ZYBuyNowShopCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopCell.h"
#import "ZYShopPartModel.h"

@interface ZYBuyNowShopCell ()
@property (nonatomic, strong) UIImageView *ivShop;//产品图片
@property (nonatomic, strong) UILabel *lblDetail;//产品说明
@property (nonatomic, strong) UILabel *lblBuyNumber;//购买量
@property (nonatomic, strong) UIImageView *ivShopType;//产品类型(候保产品订购)

@end

@implementation ZYBuyNowShopCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViewAction];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

#pragma mark - 界面布局方法
- (void)setupViewAction{
    UIImageView *ivBg = [UIImageView new];
    ivBg.image = imgname(@"shop_bg_icon");
    [self.contentView addSubview:ivBg];
    [ivBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
    
    _ivShop = [UIImageView new];
    _ivShop.backgroundColor = kColorWithHex(0xf2f2f2);
    _ivShop.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:_ivShop];
    [_ivShop mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(22);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-22);
        make.top.equalTo(self.contentView.mas_top).offset(22);
        make.height.mas_equalTo((kScreen_Width - 44) * 0.53);
    }];
    
    UIImageView *ivLine = [UIImageView new];
    ivLine.image = imgname(@"shop_line_icon");
    [self.contentView addSubview:ivLine];
    [ivLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(4);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-4);
        make.top.equalTo(self.ivShop.mas_bottom);
        make.height.mas_equalTo(42);
    }];
    
    _lblDetail = [UILabel new];
    _lblDetail.numberOfLines = 2;
    _lblDetail.font = [UIFont systemFontOfSize:16];
    _lblDetail.textColor = kColorWithHex(0x333333);
    [self.contentView addSubview:_lblDetail];
    [_lblDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(29);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-24);
        make.top.equalTo(ivLine.mas_bottom);
    }];
    
    _ivShopType = [UIImageView new];
    _ivShopType.image = imgname(@"shop_type_hb_icon");
    [self.contentView addSubview:_ivShopType];
    [_ivShopType mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(29);
        make.top.equalTo(self.lblDetail.mas_bottom).offset(16);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-27);
        make.size.mas_equalTo(CGSizeMake(93, 22));
    }];
    
    _lblBuyNumber = [UILabel new];
    _lblBuyNumber.textColor = kColorWithHex(0x777777);
    _lblBuyNumber.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:_lblBuyNumber];
    [_lblBuyNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-24);
        make.top.equalTo(self.ivShopType.mas_top);
    }];
}

#pragma mark - 数据更新
- (void)setModel:(ZYShopPartModel *)model{
    if (model) {
        _model = model;
        [HFTools imageViewUpdateWithUrl:model.image_url withImageView:_ivShop withPlaceholderImage:@""];
        _lblDetail.text = [NSString stringWithFormat:@"%@",model.parts_details];
        _lblBuyNumber.text = [NSString stringWithFormat:@"%ld人订购",model.cellCount];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
