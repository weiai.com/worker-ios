//
//  ZYGoodsDetailViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYGoodsDetailViewController.h"
#import "ZYBuyNowShopDetailNavView.h"//导航
#import "ZYBuyNowShopDetailImageCycleView.h"//轮播图
#import "ZYBuyNowShopDetailContentCell.h"//购买cell
#import "ZYBuyNowShopDetailStoreCell.h"//店铺cell
#import "ZYBuyNowShopDetailWebCell.h"//图文详情
#import "ZYBuyNowShopDetailBottomView.h"//底部按钮
#import "ZYShopPartDetailModel.h"
#import "ZYFeedbackViewController.h"
#import "ZYHouBaoOrderFormViewController.h"//订购车

@interface ZYGoodsDetailViewController () <UITableViewDelegate,UITableViewDataSource,ZYBuyNowShopDetailNavViewDelegate,ZYBuyNowShopDetailImageCycleViewDelegate,ZYBuyNowShopDetailImageCycleViewDataSource,ZYBuyNowShopDetailContentCellDelegate,ZYBuyNowShopDetailStoreCellDelegate,ZYBuyNowShopDetailWebCellDelegate,ZYBuyNowShopDetailBottomViewDelegate>
{
    BOOL _isClickNav;//是否是点击的导航栏
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZYBuyNowShopDetailNavView *viewNav;
@property (nonatomic, strong) ZYBuyNowShopDetailImageCycleView *viewCycleBanner;

@property (nonatomic, strong) ZYBuyNowShopDetailContentCell *contentCell;
@property (nonatomic, strong) ZYBuyNowShopDetailWebCell *webCell;
@property (nonatomic, strong) ZYShopPartDetailModel *shopModel;
@property (nonatomic, strong) ZYShopPartTypeModel *selectedTypeModel;//选中的型号
@property (nonatomic, assign) BOOL isSubmit;//是否在提交数据
@property (nonatomic, strong) UIView *bgView;

@end

@implementation ZYGoodsDetailViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavViewAction];
    
    [self setupViewAction];
    [self requestData];
    [self shwoBgview];
    // Do any additional setup after loading the view.
}
#pragma mark - 导航栏布局
- (void)setupNavViewAction{
    _viewNav = [[ZYBuyNowShopDetailNavView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, kNaviHeight)];
    _viewNav.delegate = self;
    [self.view addSubview:_viewNav];
    
}
#pragma mark - 界面布局
- (void)setupViewAction{
    
    ZYBuyNowShopDetailBottomView *viewBottom = [[ZYBuyNowShopDetailBottomView alloc] initWithFrame:CGRectMake(0, kScreen_Height - 53 - TabbarSafeBottomMargin, kScreen_Width, 53 + TabbarSafeBottomMargin)];
    viewBottom.delegate = self;
    [self.view addSubview:viewBottom];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight, kScreen_Width, kScreen_Height - kNaviHeight - viewBottom.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //_tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; //取消cell的下划线
    [self.view addSubview:_tableView];
    [_tableView registerClass:[ZYBuyNowShopDetailContentCell class] forCellReuseIdentifier:NSStringFromClass([ZYBuyNowShopDetailContentCell class])];
    [_tableView registerClass:[ZYBuyNowShopDetailStoreCell class] forCellReuseIdentifier:NSStringFromClass([ZYBuyNowShopDetailStoreCell class])];
    [_tableView registerClass:[ZYBuyNowShopDetailWebCell class] forCellReuseIdentifier:NSStringFromClass([ZYBuyNowShopDetailWebCell class])];
    
    _viewCycleBanner = [[ZYBuyNowShopDetailImageCycleView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, kScreen_Width) withTimerValue:3.0];
    _viewCycleBanner.delegate = self;
    _viewCycleBanner.dataSource = self;
    _tableView.tableHeaderView = _viewCycleBanner;
}
#pragma mark - 数据请求
- (void)requestData{
    [MBProgressHUD showHUDAddedTo:self.view msg:@"数据加载中" animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setObjectIfNotNil:self.shopID forKey:@"id"];
    [param setObjectIfNotNil:@"1" forKey:@"user_type"];//1所有经销商2维修工
    kWeakSelf;
    [NetWorkTool POST:getPartsInfo param:param success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        self.shopModel = [ZYShopPartDetailModel mj_objectWithKeyValues:[dic objectForKeyNotNil:@"data"]];
        [weakSelf.tableView reloadData];
        [weakSelf.viewCycleBanner reloadData];

    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}
#pragma mark - BBuyNowShopDetailNavViewDelegate
- (void)navViewBtnClick:(NSInteger)tag{
    if (tag == 202) {
        [self.navigationController popViewControllerAnimated:YES];
    }else if (tag == 201){
        //详情
        _isClickNav = YES;
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }else if (tag == 200){
        //产品
        _isClickNav = YES;
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }else if (tag == 203){
        //我的预订
        ZYHouBaoOrderFormViewController *vc = [[ZYHouBaoOrderFormViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if (!self.shopModel) {
            return [UITableViewCell new];
        }
        ZYBuyNowShopDetailContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZYBuyNowShopDetailContentCell class]) forIndexPath:indexPath];
        cell.delegate = self;
        self.contentCell = cell;
        cell.partModel = self.shopModel;
        return cell;
    } else if (indexPath.row == 1) {
//        ZYBuyNowShopDetailStoreCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZYBuyNowShopDetailStoreCell class]) forIndexPath:indexPath];
//        cell.partModel = self.shopModel;
//        cell.delegate = self;
//        return cell;
//    } else if (indexPath.row == 2) {
        ZYBuyNowShopDetailWebCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZYBuyNowShopDetailWebCell class]) forIndexPath:indexPath];
        cell.partID = self.shopID;
        cell.delegate = self;
        self.webCell = cell;
        return cell;
    }
    return [UITableViewCell new];
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if (self.shopModel) {
            return [self.contentCell cellHeight];            
        }else{
            return 0;
        }
    }else if (indexPath.row == 1){
//        return 202;
//    }else if (indexPath.row == 2){
        return [self.webCell cellHeight];
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    KMyLog(@"不可以点击哦");
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //解决点击事件和滑动事件的冲突
    if (_isClickNav) {
        return;
    }
    
    //获得可视范围内的第一个cell
    UITableViewCell *cell = [self.tableView visibleCells].firstObject;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.row >= 1) {
        //nav选中 详情
        [self.viewNav btnClickAction:self.viewNav.btnRight];
    }else{
        //nav选中 产品
        [self.viewNav btnClickAction:self.viewNav.btnLeft];
    }
}
//停止滚动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    _isClickNav = NO;
}
#pragma mark - ZYBuyNowShopDetailImageCycleViewDataSource - 轮播图
- (NSInteger)numberOfImagesForView:(ZYBuyNowShopDetailImageCycleView *)cycleView{
    return self.shopModel.lbImg.count;
}
//轮播图填充
- (UIView *)cycleView:(ZYBuyNowShopDetailImageCycleView *)cycleView imageAtIndex:(NSInteger)index{
    UIImageView *ivCycle = [[UIImageView alloc] initWithFrame:cycleView.bounds];
    ivCycle.contentMode = UIViewContentModeScaleToFill;
    ZYShopPartCycleModel *model = [self.shopModel.lbImg objectAtIndexSafe:index];
    [HFTools imageViewUpdateWithUrl:model.image_url withImageView:ivCycle withPlaceholderImage:@""];
    return ivCycle;
}
#pragma mark - ZYBuyNowShopDetailImageCycleViewDelegate
- (void)cycleView:(ZYBuyNowShopDetailImageCycleView *)cycleView didSelectedAtIndex:(NSInteger)index{
    KMyLog(@"点击了轮播图%ld",(long)index);
}
#pragma mark - BBuyNowShopDetailContentCellDelegate
- (void)buyNowShopDetailContentCellBtnArrowClick{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}
- (void)buyNowShopDetailContentCellSelectedType:(ZYShopPartTypeModel *)typeModel{
    self.selectedTypeModel = typeModel;
}
#pragma mark - BBuyNowShopDetailStoreCellDelegate
- (void)shopDetailStoreCellClickPart:(NSString *)partID{
    ZYGoodsDetailViewController *vc = [ZYGoodsDetailViewController new];
    vc.shopID = partID;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - ZYBuyNowShopDetailWebCellDelegate
- (void)buyNowShopDetailWebCellReload:(CGFloat)cellHeight{
    //刷新网页高度
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark - BBuyNowShopDetailBottomViewDelegate
- (void)detailBottomViewClick:(NSInteger)tag{
    if (tag == 200) {
        //咨询电话
        NSString *telephoneNumber = self.shopModel.contactPhone;
        if (telephoneNumber.length > 0) {
            NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",telephoneNumber];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }else if (tag == 201){
        //留言
        ZYFeedbackViewController *vc = [[ZYFeedbackViewController alloc] init];
        vc.orderId = self.shopID;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (tag == 202){
        //加入购物车
        KMyLog(@"self.selectedTypeModel.Id %@", self.selectedTypeModel.Id);
        
        if (self.shopModel && self.selectedTypeModel) {
            if (_isSubmit) {
                return;
            }
            kWeakSelf;
            _isSubmit = YES;
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:self.shopID forKey:@"id"];//配件ID
            [dict setObjectIfNotNil:self.selectedTypeModel.Id forKey:@"attribute"];//类型ID
            [dict setObjectIfNotNil:@(self.shopModel.buyNumber) forKey:@"count"];//个数
            [dict setObject:@"1" forKey:@"user_type"];//2经销商1师傅
            
            [NetWorkTool POST:addOrder param:dict success:^(id dic) {
                weakSelf.isSubmit = NO;
                [[UIApplication sharedApplication].keyWindow addSubview:self->_bgView];
                //更新我的预订->订购车数据
                [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_ADD_ORDER_SUCCESS" object:nil];
            } other:^(id dic) {
                weakSelf.isSubmit = NO;
            } fail:^(NSError *error) {
                weakSelf.isSubmit = NO;
                ShowToastWithText(@"加入预订车失败，请稍后再试");
            } needUser:YES];
            
        }else{
            ShowToastWithText(@"请选择型号");
        }
    }
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = self.view.bounds;
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 30, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 82, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"添加成功";
    //self.bgUPLable =UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 113, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"可在个人中心我的预订中查看";
    //self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-28);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    //if (self.myblock) {
        //self.myblock(@"");
    //}
    
    //[self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
