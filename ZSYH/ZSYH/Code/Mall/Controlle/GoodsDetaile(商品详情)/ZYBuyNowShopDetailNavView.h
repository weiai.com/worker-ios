//
//  ZYBuyNowShopDetailNavView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

@protocol ZYBuyNowShopDetailNavViewDelegate <NSObject>
//按钮点击代理方法
- (void)navViewBtnClick:(NSInteger)tag;
@end

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYBuyNowShopDetailNavView : UIView
@property (nonatomic, strong) UIButton *btnLeft;//产品
@property (nonatomic, strong) UIButton *btnRight;//详情
@property (nonatomic, weak) id <ZYBuyNowShopDetailNavViewDelegate> delegate;
- (void)btnClickAction:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
