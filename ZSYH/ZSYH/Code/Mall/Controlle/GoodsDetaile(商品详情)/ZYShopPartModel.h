//
//  ZYShopPartModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYShopPartModel : BaseModel
@property (nonatomic, copy) NSString *parts_details;
@property (nonatomic, copy) NSString *is_good_choice;
@property (nonatomic, copy) NSString *parts_remarks;
@property (nonatomic, copy) NSString *is_new_push;
@property (nonatomic, copy) NSString *parts_number;

@property (nonatomic, copy) NSString *lower_agentB;
@property (nonatomic, copy) NSString *lower_agentA;
@property (nonatomic, copy) NSString *agent_user_price;
@property (nonatomic, assign) NSInteger cellCount;//销量
@property (nonatomic, copy) NSString *parts_name;

//@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *is_hot_push;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *repair_user_price;
@property (nonatomic, assign) NSInteger soft;

@property (nonatomic, copy) NSString *parts_place;
@property (nonatomic, copy) NSString *modifyDate;
@property (nonatomic, copy) NSString *parts_price;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *parts_model;

@property (nonatomic, copy) NSString *lower_user;
@property (nonatomic, copy) NSString *parts_type;
@property (nonatomic, copy) NSString *parts_brand;
@property (nonatomic, copy) NSString *factory_id;
@property (nonatomic, copy) NSString *is_today_hot;

@property (nonatomic, copy) NSString *parts_quantity;

@end

NS_ASSUME_NONNULL_END
