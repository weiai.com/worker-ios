//
//  ZYBuyNowShopCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZYShopPartModel;

NS_ASSUME_NONNULL_BEGIN

@interface ZYBuyNowShopCell : UITableViewCell
@property (nonatomic, strong) ZYShopPartModel *model;

@end

NS_ASSUME_NONNULL_END
