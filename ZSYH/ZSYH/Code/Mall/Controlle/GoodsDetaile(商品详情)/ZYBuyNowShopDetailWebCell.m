//
//  ZYBuyNowShopDetailWebCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopDetailWebCell.h"
#import <WebKit/WebKit.h>

@interface ZYBuyNowShopDetailWebCell ()
@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, assign) CGFloat tempHeight;//临时变量
@property (nonatomic, assign) BOOL isRefresh;//是否已经刷新了界面
@end

@implementation ZYBuyNowShopDetailWebCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    self.contentView.backgroundColor = kColorWithHex(0xf2f2f2);
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = @"图文详情";
    lblTitle.font = [UIFont systemFontOfSize:12];
    lblTitle.textColor = kColorWithHex(0x5d5d5d);
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:lblTitle];
    [lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.contentView.mas_top).offset(15);
        make.height.mas_equalTo(17);
    }];
    
    UIView *viewLienLeft = [UIView new];
    viewLienLeft.backgroundColor = kColorWithHex(0xdddbd7);
    [self.contentView addSubview:viewLienLeft];
    [viewLienLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblTitle.mas_centerY);
        make.trailing.equalTo(lblTitle.mas_leading).offset(-7);
        make.size.mas_equalTo(CGSizeMake(42, 1));
    }];
    
    UIView *viewLienRight = [UIView new];
    viewLienRight.backgroundColor = kColorWithHex(0xdddbd7);
    [self.contentView addSubview:viewLienRight];
    [viewLienRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblTitle.mas_centerY);
        make.leading.equalTo(lblTitle.mas_trailing).offset(7);
        make.size.mas_equalTo(CGSizeMake(42, 1));
    }];
    
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    _webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    [self.contentView addSubview:_webView];
    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.contentView);
        make.top.equalTo(lblTitle.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}
#pragma mark - 更新界面
- (void)setPartID:(NSString *)partID{
    if (partID) {
        _partID = partID;
        //NSString *webUrl = [partDetailImageTextWebUrl stringByAppendingString:partID];
        NSString *webUrl = [NSString stringWithFormat:@"%@%@",partDetailImageTextWebUrl, partID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:webUrl]];
        [_webView loadRequest:request];
        KMyLog(@"产品详情 详情网页网络地址 %@", webUrl);
    }
}

//kvo
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    CGFloat webHeight = self.webView.scrollView.contentSize.height;
    //KMyLog(@"获得的网页的高度%f",webHeight);
    
    if (webHeight > 0) {
        if(self.tempHeight - webHeight != 0){
            //webHeight在变化
            self.tempHeight = webHeight;
            _isRefresh = NO;
        } else {
            //高度已经稳定了
            self.cellHeight = webHeight + 42;
            if (!_isRefresh) {
                _isRefresh = YES;//防止多次回调
                if (self.delegate && [self.delegate respondsToSelector:@selector(buyNowShopDetailWebCellReload:)]) {
                    [self.delegate buyNowShopDetailWebCellReload:self.cellHeight];
                }
            }
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
