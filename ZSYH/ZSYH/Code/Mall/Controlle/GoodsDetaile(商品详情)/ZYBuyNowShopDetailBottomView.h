//
//  ZYBuyNowShopDetailBottomView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ZYBuyNowShopDetailBottomViewDelegate <NSObject>
//点击事件
- (void)detailBottomViewClick:(NSInteger)tag;
@end

@interface ZYBuyNowShopDetailBottomView : UIView
@property (nonatomic, weak) id <ZYBuyNowShopDetailBottomViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
