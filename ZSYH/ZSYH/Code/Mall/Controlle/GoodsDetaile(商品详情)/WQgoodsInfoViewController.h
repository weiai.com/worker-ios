//
//  WQgoodsInfoViewController.h
//  ZengBei
//
//  Created by 魏堰青 on 2018/12/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsDetailModel.h"


@interface WQgoodsInfoViewController : UIViewController
@property(nonatomic,strong)GoodsDetailModel *model;

@end
