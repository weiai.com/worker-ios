//
//  ZYBuyNowShopDetailContentCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 立即订购 配件详情内容cell

#import <UIKit/UIKit.h>
#import "ZYShopPartDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZYBuyNowShopDetailContentCellDelegate <NSObject>
- (void)buyNowShopDetailContentCellBtnArrowClick;//展开按钮点击事件
- (void)buyNowShopDetailContentCellSelectedType:(ZYShopPartTypeModel *_Nullable)typeModel;//选中的类型
@end

@interface ZYBuyNowShopDetailContentCell : UITableViewCell
@property (nonatomic, weak) id <ZYBuyNowShopDetailContentCellDelegate> delegate;
@property (nonatomic, strong) ZYShopPartDetailModel *partModel;
- (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
