//
//  CSBuyAndAddView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSBuyAndAddView : UIView
@property (weak, nonatomic) IBOutlet UIView *bgview;
@property(nonatomic,copy)void (^myblock)(NSInteger ind0Retuen1minus2add3botBUt4phone);
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *goodsPrice;
@property (weak, nonatomic) IBOutlet UILabel *goodsName;
@property (weak, nonatomic) IBOutlet UILabel *goodsNumber;
@property (weak, nonatomic) IBOutlet UILabel *goodsSeleLB;
@property (weak, nonatomic) IBOutlet UIButton *buttomBut;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (nonatomic, strong) NSString *phoneNum;


@end

NS_ASSUME_NONNULL_END
