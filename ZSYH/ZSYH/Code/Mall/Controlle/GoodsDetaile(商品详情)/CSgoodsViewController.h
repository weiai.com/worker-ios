//
//  CSgoodsViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSgoodsViewController : BaseViewController
@property(nonatomic,strong)NSString *goodsID;
@property(nonatomic,strong)NSString *isshop;
@property(nonatomic,strong)NSString *iscur;

@end

NS_ASSUME_NONNULL_END
