//
//  CSgoodsViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSgoodsViewController.h"
#import "CSgoodsLeftViewController.h"
#import "CSdoodsCenterViewController.h"
#import "MyCommentsController.h"
#import "CSBuyAndAddView.h"
#import "CSGoodsModel.h"
#import "CSorderListViewController.h"
#import "HFShopCarGoodsModel.h"
static int const labelWith = 100;

@interface CSgoodsViewController ()<UIScrollViewDelegate>{
    UILabel *mylab;
}
@property(nonatomic,weak)UIScrollView *titleScrollView;
@property(nonatomic,weak)UIScrollView *contentScrollView;
@property(nonatomic,strong)UIButton *oldButton;
@property(nonatomic,strong)UILabel *seletLabel;
@property(nonatomic,strong)NSMutableArray *titleArray;
@property(nonatomic,strong)UIView *bgView;
@property(nonatomic,assign)BOOL isbutton;
@property(nonatomic,strong)CSBuyAndAddView *buyAudAddView;
@property(nonatomic,strong)CSGoodsModel *modlel;
@property(strong,nonatomic)NSMutableArray  *orderTotalGoods;
@property(strong,nonatomic)NSMutableArray  *chunzhiArr;
@property(nonatomic,strong)UIView *buyAudBgview;
@property(nonatomic,strong)NSString *shopPhone;
@property(nonatomic,strong)NSDictionary *shopdic;

@end

@implementation CSgoodsViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(NSMutableArray *)titleArray{
    
    if (_titleArray == nil) {
        _titleArray = [NSMutableArray array];
    }
    return _titleArray;
}

-(CSBuyAndAddView *)buyAudAddView{
    if (!_buyAudAddView) {
        _buyAudAddView = [[CSBuyAndAddView alloc]initWithFrame:CGRectMake(0, KHEIGHT-340, KWIDTH, 340)];
        self.buyAudBgview = [[UIView alloc]initWithFrame:self.contentScrollView.bounds];
        //[[UIApplication sharedApplication].keyWindow addSubview:_buyAudBgview];
    }
    return _buyAudAddView;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isbutton = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(0, kNaviHeight-40, ScreenW, 40)];
    [self.view addSubview:_bgView];
    _bgView.userInteractionEnabled = YES;
    [self setScrollViewTopandBottom];
    [self setupViewControllers];
    _titleArray = [@[@"产品",@"评价",@"详情"] mutableCopy];
    [self setTitlesButton];
    //    [self reqestConpon];
    
    // Do any additional setup after loading the view.
    UIButton * _leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, 40, 40)];
    _leftbutton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_leftbutton setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [_leftbutton setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [_leftbutton setTitleColor:[UIColor blueColor] forState:(UIControlStateNormal)];
    [_leftbutton addTarget:self action:@selector(leftClickaction) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:_leftbutton];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refashAction) name:@"refasth" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refashcommitAction) name:@"refasthcommit" object:nil];
    [Center addObserver:self selector:@selector(beginaction) name:@"beginScroll" object:nil];
    [Center addObserver:self selector:@selector(endaction) name:@"endScroll" object:nil];
    
    [self requestDetaile];
}

-(void)beginaction{
    self.contentScrollView.scrollEnabled = NO;
}

-(void)endaction{
    self.contentScrollView.scrollEnabled = YES;
}

- (void)leftClick{
    
    [super leftClick];
    [self.buyAudAddView removeFromSuperview];
}

-(void)requestDetaile{
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = NOTNIL(_goodsID);
    mudic[@"type"] = @"0";
    
    kWeakSelf;
    [NetWorkTool POST:MmallgoodsbuyID param:mudic success:^(id dic) {
        weakSelf.modlel = [[CSGoodsModel alloc]init];
        
        [weakSelf.modlel setValuesForKeysWithDictionary:[[dic objectForKey:@"data"] objectForKey:@"product"]];
        weakSelf.modlel.number = @"1";
        self.shopPhone =[[[dic objectForKey:@"data"] objectForKey:@"shop"] objectForKey:@"shop_phone"];
        weakSelf.shopdic =[[dic objectForKey:@"data"] objectForKey:@"shop"];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)refashAction{
    CGFloat offsetX = 1 * ScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

-(void)refashcommitAction{
    //查看全部评价
    CGFloat offsetX = 1 * ScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

-(void)leftClickaction{
    [self.buyAudAddView removeFromSuperview];
    self.buyAudAddView.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

//初始化标题ScrollView
-(void)setupScrollView{
    NSInteger count = self.childViewControllers.count;
    //设置标题滚动条
    self.titleScrollView.contentSize = CGSizeMake(count * labelWith, 0);
    self.titleScrollView.showsHorizontalScrollIndicator = NO;
    
    //设置内容滚动条
    self.contentScrollView.contentSize = CGSizeMake(ScreenW*count, 0);
    //开启分页
    self.contentScrollView.pagingEnabled = YES;
    //没有弹簧效果
    self.contentScrollView.bounces = NO;
    //隐藏水平滚动条
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //设置协议
    self.contentScrollView.delegate = self;
}

//设置页面scrollView
-(void)setScrollViewTopandBottom{
    CGFloat fff = 55;
    if ([_iscur isEqualToString:@"cur"]) {
        fff = 0;
    }
    
    UIScrollView *contentScroll = [[UIScrollView alloc]init];
    contentScroll.frame = CGRectMake(0, kNaviHeight, ScreenW, self.view.frame.size.height -40-fff);
    contentScroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentScroll];
    self.contentScrollView = contentScroll;
    self.contentScrollView.userInteractionEnabled = YES;
    //    self.contentScrollView.scrollEnabled = NO;
    self.contentScrollView.pagingEnabled = YES;
    //显示水平方向的滚动条(默认YES)
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //显示垂直方向的滚动条(默认YES)
    self.contentScrollView.showsVerticalScrollIndicator = NO;
    self.contentScrollView.bounces = NO;
    
    self.contentScrollView.contentSize = CGSizeMake(ScreenW*3, ScreenH-37);
    
    self.contentScrollView.delegate = self;
}

#pragma mark -- UISCrollview代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSInteger index = scrollView.contentOffset.x/scrollView.bounds.size.width;
    
    //  CGFloat offsetX = index * ScreenW;
    
    UIButton *but = (UIButton *)[self.view viewWithTag:100+index];
    [self.oldButton setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [but setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:UIControlStateNormal];
    self.oldButton = but;
    CGPoint CEN =  mylab.center;
    CEN.x = but.center.x;
    mylab.center = CEN;
    [self.buyAudAddView removeFromSuperview];
    
    for (UIView *vie in self.view.subviews) {
        if (vie.frame.size.height  == KHEIGHT-kNaviHeight) {
            [vie removeFromSuperview];
        }
    }
    NSLog(@"%ld", index);
}

-(void)setTitlesButton{
    
    [_bgView removeAllSubviews];
    NSArray *arr = _titleArray;
    CGFloat leftRight = 60;
    CGFloat wwww = (ScreenW-leftRight-leftRight)/arr.count;
    wwww = labelWith;
    CGFloat fengxi = (ScreenW-leftRight-leftRight)/(arr.count-1);
    fengxi = 0;
    CGFloat hhhhh = 16;
    CGFloat yyyy = +12 ;
    
    for (int i = 0; i<arr.count; i++) {
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(leftRight+(fengxi+wwww)*i, yyyy, wwww, hhhhh);
        myButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_bgView addSubview:myButton];
        myButton.tag = 100+i;
        [myButton setTitle:arr[i] forState:UIControlStateNormal];
        [myButton addTarget:self action:@selector(titleClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [myButton setTitleColor:[self colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        if (i == 0) {
            [myButton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:UIControlStateNormal];
            self.oldButton = myButton;
            [self titleClick:myButton];
            mylab = [[UILabel alloc]init];
            [_bgView addSubview:mylab];
            mylab.frame = CGRectMake(10, yyyy+hhhhh+4, 70, 2);
            CGPoint CEN =  mylab.center;
            CEN.x = myButton.center.x;
            mylab.center = CEN;
            mylab.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
        }
    }
}

-(void)titleClick:(UIButton *)but{
    //0取出label
    NSInteger index = but.tag-100;
    //2.1计算滚动位置
    CGFloat offsetX = index * ScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

//显示页面
-(void)showVC:(NSInteger)index{
    
    CGFloat offsetX = index * ScreenW;
    
    UIViewController *vc = self.childViewControllers[index];
    
    //判断当前控制器的View 有没有加载过 如果已经加载过 就不需要加载
    if (vc.isViewLoaded) return;
    vc.view.frame = CGRectMake(offsetX, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:vc.view];
}

- (void)setupViewControllers {
    
    CGFloat  buthei = 50;
    
    
    for (int i = 0; i < 3; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(KWIDTH/3.0*i, KHEIGHT-50, KWIDTH/3, buthei);
        btn.titleLabel.font = FontSize(14);
        btn.tag = 3333+i;
        //        btn.backgroundColor = i==0?[UIColor whiteColor]:zhutiColor;
        if (i == 0) {
            [btn setImage:imgname(@"newcallimg") forState:(UIControlStateNormal)];
            [btn setTitle:@"电话联系" forState:(UIControlStateNormal)];
            [btn setBackgroundColor:[UIColor whiteColor]];
            [btn setTitleColor:K333333 forState:UIControlStateNormal];
            
        }else if (i == 1){
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitle:@"加入购物车" forState:(UIControlStateNormal)];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"#9DD596"]];
        }else{
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btn setTitle:@"立即购买" forState:(UIControlStateNormal)];
            [btn setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        }
        //        [btn setTitle:i==0?@"加入购物车":@"立即购买" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(buyAddCart:) forControlEvents:UIControlEventTouchUpInside];
        if ([_iscur isEqualToString:@"cur"]) {
        }else{
            [self.view addSubview:btn];
            
        }
    }
    
    CSgoodsLeftViewController *seveVC = [[CSgoodsLeftViewController alloc]init];
    seveVC.goodsID = self.goodsID;
    seveVC.isshop = _isshop;
    seveVC.view.frame = CGRectMake(0, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:seveVC.view];
    [self addChildViewController:seveVC];
    
    CSdoodsCenterViewController *evaluate = [[CSdoodsCenterViewController alloc]init];
    evaluate.goodsID = self.goodsID;
    evaluate.view.frame = CGRectMake(ScreenW*2, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:evaluate.view];
    [self addChildViewController:evaluate];
    
    MyCommentsController *evaluate1 = [[MyCommentsController alloc]init];
    evaluate1.goodsId = self.goodsID;
    evaluate1.view.frame = CGRectMake(ScreenW*1, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:evaluate1.view];
    [self addChildViewController:evaluate1];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    UITouch *touch = [touches anyObject];
    // 获取当前的位置
//    CGPoint current = [touch locationInView:self.contentScrollView];
}

- (void)buyAddCart:(UIButton *)btn {
    NSInteger ind = btn.tag-3333;
    
    if (ind == 0) {
        
        [HFTools callMobilePhone:_shopPhone];
        return;
    }

    [[UIApplication sharedApplication].keyWindow addSubview:self.buyAudAddView];
    
    self.buyAudAddView.goodsSeleLB.text= @"1";
    self.modlel.number = @"1";
    self.buyAudAddView.goodsNumber.text =[NSString stringWithFormat:@"已选择：1件"];
    
    
    self.buyAudBgview.hidden = NO;
    if (ind == 1) {
        [self.buyAudAddView.phoneBtn setImage:imgname(@"newcallimg") forState:UIControlStateNormal];
        [self.buyAudAddView.phoneBtn setTitle:@"电话联系" forState:(UIControlStateNormal)];
        [self.buyAudAddView.phoneBtn setBackgroundColor:[UIColor whiteColor]];
        [self.buyAudAddView.phoneBtn setTitleColor:K333333 forState:UIControlStateNormal];
        [self.buyAudAddView.buttomBut setTitle:@"加入购物车" forState:UIControlStateNormal];
    }else if (ind == 2) {
        [self.buyAudAddView.phoneBtn setImage:imgname(@"newcallimg") forState:UIControlStateNormal];
        [self.buyAudAddView.phoneBtn setTitle:@"电话联系" forState:(UIControlStateNormal)];
        [self.buyAudAddView.phoneBtn setBackgroundColor:[UIColor whiteColor]];
        [self.buyAudAddView.phoneBtn setTitleColor:K333333 forState:UIControlStateNormal];
        [self.buyAudAddView.buttomBut setTitle:@"立即购买" forState:UIControlStateNormal];
    }
    kWeakSelf;
    self.buyAudAddView.myblock = ^(NSInteger ind0Retuen1minus2add3botBUt4phone) {
        switch (ind0Retuen1minus2add3botBUt4phone) {
            case 0:
            {
                [weakSelf.buyAudAddView removeFromSuperview];
                weakSelf.buyAudBgview.hidden = YES;
                
            }
                break;
            case 1:
            {
                KMyLog(@"减一");
                NSInteger ind =[weakSelf.buyAudAddView.goodsSeleLB.text integerValue];
                ind-=1;
                if (ind == 0) {
                    ind = 1;
                }
                weakSelf.modlel.number = [NSString stringWithFormat:@"%ld",ind];
                
                weakSelf.buyAudAddView.goodsNumber.text = [NSString stringWithFormat:@"已选择：%ld件",ind];
                
                weakSelf.buyAudAddView.goodsSeleLB.text =[NSString stringWithFormat:@"%ld",ind];;
                
                
            }
                break;
            case 2:
            {
                KMyLog(@"加一");
                NSInteger ind =[weakSelf.buyAudAddView.goodsSeleLB.text integerValue];
                ind+=1;
                //                if (weakSelf.modlel.) {
                //
                //                }
                
                weakSelf.modlel.number = [NSString stringWithFormat:@"%ld",ind];
                weakSelf.buyAudAddView.goodsNumber.text = [NSString stringWithFormat:@"已选择：%ld件",ind];
                weakSelf.buyAudAddView.goodsSeleLB.text =[NSString stringWithFormat:@"%ld",ind];;
            }
                break;
            case 3:
            {
                KMyLog(@"底部");
                if (ind == 1) {
                    [weakSelf addcar];
                    //[self.buyAudAddView.buttomBut setTitle:@"加入购物车" forState:UIControlStateNormal];
                }else if(ind == 2) {
                    //[self.buyAudAddView.buttomBut setTitle:@"立即购买" forState:UIControlStateNormal];
                    [weakSelf buy];
                }
            }
                break;
            case 4:
            {
                KMyLog(@"电话");
                [HFTools callMobilePhone:self->_shopPhone];
            }
                break;
            default:
                break;
        }
    };
    if ([_modlel.productPic hasPrefix:@"http"]) {
        [self.buyAudAddView.goodsImage sd_setImageWithURL:[NSURL URLWithString:_modlel.productPic] placeholderImage:defaultImg];
    }else{
        sdimg(self.buyAudAddView.goodsImage, _modlel.productPic);
        
    }
    self.buyAudAddView.goodsPrice.text = [NSString stringWithFormat:@"¥%@",_modlel.productPrice];
    self.buyAudAddView.goodsName.text = [NSString stringWithFormat:@"%@",_modlel.productName];
   
}

-(void)buy{
    
    self.orderTotalGoods = [NSMutableArray arrayWithCapacity:1];
    self.chunzhiArr = [NSMutableArray arrayWithCapacity:1];
    NSString *ste = [NSString stringWithFormat:@"合计:¥ %@",_modlel.productPrice];
    [self.chunzhiArr addObject:ste];
    
    HFShopCarGoodsModel *mymodle = [[HFShopCarGoodsModel alloc]init];
    mymodle.id = _modlel.Id;
    mymodle.product_name = _modlel.productName;
    
    mymodle.product_pic = _modlel.productPic;
    mymodle.product_price = _modlel.productPrice;
    mymodle.count = _modlel.number;
    mymodle.product_id = _modlel.Id;
    [self.orderTotalGoods addObject:mymodle];

    CSorderListViewController *vc = [[CSorderListViewController alloc]init];
    
    vc.mydateSource = self.orderTotalGoods;
    vc.dibuCZArr = self.chunzhiArr;
    vc.shopDic = self.shopdic;
    
    [singlTool shareSingTool].isLjiBuy = 0;
    [_buyAudAddView removeFromSuperview];
    _buyAudBgview.hidden = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)addcar{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"carId"] = @"";
    param[@"count"] = NOTNIL(_modlel.number);
    param[@"id"] = @"";
    param[@"payState"] = @"0";
    param[@"productId"] = NOTNIL(_modlel.Id);

    NSDictionary *dic = @{@"products":param};
    NSString *jssy = [HFTools toJSONString:dic];
    NSDictionary *chuDic = @{@"data":jssy};
    kWeakSelf;
    [NetWorkTool POST:Maddcur param:chuDic success:^(id dic) {
        ShowToastWithText(@"加入购物车成功");
        [weakSelf.buyAudAddView.superview removeFromSuperview];
        [weakSelf.buyAudAddView removeFromSuperview];
        
        for (UIView *one in self.view.allSubviews) {
            if (one.frame.size.height == KHEIGHT) {
                [one removeFromSuperview];
            }
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
