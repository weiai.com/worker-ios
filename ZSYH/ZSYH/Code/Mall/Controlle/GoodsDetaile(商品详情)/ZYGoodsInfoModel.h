//
//  ZYGoodsInfoModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ZYRepairUserModel;

NS_ASSUME_NONNULL_BEGIN

@interface ZYGoodsInfoModel : BaseModel
@property (nonatomic, copy) NSString *parts_name;//配件名称
@property (nonatomic, copy) NSString *type_name;//配件品类
@property (nonatomic, copy) NSString *qrcode;//二维码信息
@property (nonatomic, copy) NSString *productionDate;//出厂时间
@property (nonatomic, copy) NSString *termValidity;//有效期在productionDate 和 termValidity之间
@property (nonatomic, copy) NSString *install;//如果没有以上两个字段 install月 表示有效期
@property (nonatomic, copy) NSString *anTime;//安装时间
@property (nonatomic, copy) NSString *shTime;//损坏时间
@property (nonatomic, copy) NSString *warranty;//保质期
@property (nonatomic, copy) NSString *compensationPrice;//赔付金额
@property (nonatomic, strong) ZYRepairUserModel *repairUser;//维修师傅信息

@end

@interface ZYRepairUserModel : BaseModel
@property (nonatomic, copy) NSString *personName;//姓名
@property (nonatomic, copy) NSString *phone;//手机号
@property (nonatomic, copy) NSString *Id;

@end

NS_ASSUME_NONNULL_END
