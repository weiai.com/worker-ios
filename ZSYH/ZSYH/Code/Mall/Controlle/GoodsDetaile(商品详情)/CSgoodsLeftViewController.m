//
//  CSgoodsLeftViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSgoodsLeftViewController.h"
#import "CSdoodsCommitView.h"
#import "CSgoodsShopView.h"
#import "CSGoodsModel.h"
#import "CSgoodscommitModel.h"
#import "CSBannerModel.h"
#import "CSShopViewController.h"
#import "CSshopModel.h"
#import "CSgoodsViewController.h"
#import <AMapNaviKit/AMapNaviKit.h>
#import "QPLocationManager.h"

@interface CSgoodsLeftViewController ()<UIScrollViewDelegate,AMapNaviCompositeManagerDelegate>

@property (nonatomic, strong) AMapNaviCompositeManager *compositeManager;
@property(nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong) XSQBanner *banner;
//@property (nonatomic, strong) SPCarouselView *banner;
@property (nonatomic, assign) NSInteger lastcontentOffset; //添加此属性的作用，根据差值，判断ScrollView是上滑还是下拉
@property (nonatomic,assign) BOOL isrefa;
@property(nonatomic,strong)CSGoodsModel *modlel;
@property(nonatomic,strong)NSMutableArray *commitArr;
@property(nonatomic,strong)NSMutableArray *imageArr;
@property(nonatomic,strong)CSshopModel *shopModel;
@property(nonatomic,strong)NSMutableArray *shopProductArr;
@property(nonatomic,strong)NSMutableArray *LBimageArr;
@property(nonatomic,strong)NSString *numberStr;//多人已购买
@property(nonatomic,assign)CGFloat heig;//点击区域
@property(nonatomic,strong)UILabel *nameLB;

@end

@implementation CSgoodsLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品详情";
    self.isrefa  = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.commitArr = [NSMutableArray arrayWithCapacity:1];
    self.imageArr = [NSMutableArray arrayWithCapacity:1];
    self.shopProductArr = [NSMutableArray arrayWithCapacity:1];
    self.LBimageArr = [NSMutableArray arrayWithCapacity:1];

    [self requestDetaile];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

-(void)requestDetaile{
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = NOTNIL(_goodsID);
    mudic[@"type"] = @"0";

    kWeakSelf;
    [NetWorkTool POST:MmallgoodsbuyID param:mudic success:^(id dic) {
        weakSelf.modlel = [[CSGoodsModel alloc]init];

        weakSelf.numberStr =[NSString stringWithFormat:@"%@",[[dic objectForKey:@"data"] objectForKey:@"count"]];
        [weakSelf.modlel setValuesForKeysWithDictionary:[[dic objectForKey:@"data"] objectForKey:@"product"]];
        for (NSDictionary *evdic in [[dic objectForKey:@"data"] objectForKey:@"evaluates"]) {
            CSgoodscommitModel *model = [[CSgoodscommitModel alloc]init];
            [model setValuesForKeysWithDictionary:evdic];
            [self.commitArr addObject:model];
        }
        
        for (NSDictionary *evdic in [[dic objectForKey:@"data"] objectForKey:@"lbImgs"]) {
            CSBannerModel *lbmodel = [[CSBannerModel alloc]init];
            [lbmodel setValuesForKeysWithDictionary:evdic];
            [self.LBimageArr addObject:lbmodel];
        }
        
        for (NSDictionary *imdic in [[dic objectForKey:@"data"] objectForKey:@"imgs"]) {
            CSBannerModel *bmodel = [[CSBannerModel alloc]init];
            [bmodel setValuesForKeysWithDictionary:imdic];
            [self.imageArr addObject:bmodel];
        }
        
        for (NSDictionary *pudic in [[dic objectForKey:@"data"] objectForKey:@"proList"]) {
            CSGoodsModel *model = [[CSGoodsModel alloc]init];
            [model setValuesForKeysWithDictionary:pudic];
            [self.shopProductArr addObject:model];
        }
        self.shopModel = [[CSshopModel alloc]init];
        [ self.shopModel setValuesForKeysWithDictionary: [[dic objectForKey:@"data"] objectForKey:@"shop"]];
        [self showdetaile];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)showdetaile{
    [self.view addSubview:self.scrollView];
    //创建产品详情
    [self.scrollView addSubview:self.banner];
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    //[muarr addObject:_modlel.productPic];
    for (CSBannerModel *model in _LBimageArr) {
        [muarr addObject:model.product_img_url];

    }
    if (muarr.count <1) {
        [muarr addObject:_modlel.productPic];
    }
    [_banner updateImageArr:muarr AndImageClickBlock:^(NSInteger index) {

    }];
    
//    self.banner.urlImages = muarr;
//    self.banner.clickedImageBlock = ^(NSUInteger index) {
//        //轮播图点击事件
//    };

    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, _banner.bottom+10, KWIDTH-20, 40)];
    name.textColor = KTEXTCOLOR;
    name.font = FontSize(16);
    name.text = _modlel.productName;
    name.numberOfLines = 2;
    [name sizeToFit];
    [self.scrollView addSubview:name];
    UILabel *price = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+10, 10, 10)];
    price.textColor = RGBA(252, 23, 39,1);
    price.font = FontSize(18);
    price.text =[NSString stringWithFormat:@"¥%@",_modlel.productPrice];
    [self.scrollView addSubview:price];
    [price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(16);
        make.top.offset(name.bottom+10);
        make.height.offset(20);
    }];

    UILabel *priceleft = [[UILabel alloc]initWithFrame:CGRectMake(10, name.bottom+10, KWIDTH-160, 20)];
    priceleft.textColor = [UIColor colorWithHexString:@"#999999"];
    priceleft.font = FontSize(13);
    priceleft.text =[NSString stringWithFormat:@"%@人已付款", _numberStr];
    [self.scrollView addSubview:priceleft];
    [priceleft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(price.mas_right).offset(8);
        make.top.offset(name.bottom+10);
        make.height.offset(20);
    }];

    UILabel *pingtaiLB = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-140, name.bottom+10, 120, 20)];
    pingtaiLB.textColor = [UIColor colorWithHexString:@"#CA9F61"];
    pingtaiLB.textAlignment = NSTextAlignmentCenter;
    pingtaiLB.font = FontSize(12);
    pingtaiLB.text =[NSString stringWithFormat:@"候保产品质量保障"];
    pingtaiLB.backgroundColor = [UIColor colorWithHexString:@"#F2E8D5"];

//    if ([_modlel.productBelType integerValue] == 1) {
//        [self.scrollView addSubview:pingtaiLB];
//    }
    if ([_modlel.channel integerValue] == 1) {
        [self.scrollView addSubview:pingtaiLB];
    }

    __block CGFloat heig = pingtaiLB.bottom+19;


    UILabel *moreLB = [[UILabel alloc]initWithFrame:CGRectMake(0, heig+20, KWIDTH, 32)];
    moreLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    moreLB.textAlignment = NSTextAlignmentCenter;
    moreLB.font = FontSize(12);
    moreLB.text =[NSString stringWithFormat:@"评价"];
    moreLB.textColor = [UIColor colorWithHexString:@"#5D5D5D"];
    [self.scrollView addSubview:moreLB];
    UILabel *moreLB1 = [[UILabel alloc]initWithFrame:CGRectMake(0, heig+20, KWIDTH/2 - 28, 32)];
    moreLB1.textAlignment = NSTextAlignmentRight;
    moreLB1.text =[NSString stringWithFormat:@"—————"];
    moreLB1.textColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.scrollView addSubview:moreLB1];
    UILabel *moreLB2 = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH/2+28, heig+20, KWIDTH/2, 32)];
//    moreLB1.textAlignment = NSTextAlignmentRight;
    moreLB2.text =[NSString stringWithFormat:@"—————"];
    moreLB2.textColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.scrollView addSubview:moreLB2];

    heig =moreLB.bottom;
    if (self.commitArr.count >0) {

        CSgoodscommitModel *model = _commitArr[0];
        CGFloat  nbheigtf =  [NSString heightWithWidth:KWIDTH-32 font:14 text:model.evaluate_content];
        CSdoodsCommitView *commView = [[CSdoodsCommitView alloc]initWithFrame:CGRectMake(0, moreLB.bottom, KWIDTH, 120+nbheigtf)];
        commView.pingjiaLB.text = [NSString stringWithFormat:@"产品评价（%ld）",_commitArr.count];
        [commView.picImage sd_setImageWithURL:[NSURL URLWithString:model
                                               .image_url] placeholderImage:defaultImg];
        if (model.nickname.length == 1) {
            commView.nameLB.text = @"*";
        }else if (model.nickname.length == 2) {
            NSInteger length = 1;
            NSString *name = [HFTools replaceStringWithString:model.nickname Asterisk:1 length:length];
            commView.nameLB.text = name;
        }else if (model.nickname.length >= 3) {
            NSInteger length = model.nickname.length-2;
            NSString *name = [HFTools replaceStringWithString:model.nickname Asterisk:1 length:length];
            commView.nameLB.text = name;
        }
        commView.nameLB.font = [UIFont systemFontOfSize:14];
        commView.contentLB.text =  model.evaluate_content;
        [commView.lookAllButton addTarget:self action:@selector(lookMallAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:commView];
        heig = commView.bottom;

    }else{
        UILabel *nocimmit = [[UILabel alloc]initWithFrame:CGRectMake(16, heig+16, KWIDTH, 14)];
        nocimmit.font = FontSize(14);
        nocimmit.text =[NSString stringWithFormat:@"暂无评价"];
        nocimmit.textColor = K333333;
        [self.scrollView addSubview:nocimmit];
        heig = nocimmit.bottom+16;


    }
    UILabel *commViewDowBgLable = [[UILabel alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, 5)];
    commViewDowBgLable.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:commViewDowBgLable];
    
    UIImageView *shopImage = [[UIImageView alloc]initWithFrame:CGRectMake(16, commViewDowBgLable.bottom+16, 40, 40)];
    [self.scrollView addSubview:shopImage];
    
    mysdimg(shopImage, _shopModel.shop_image);
    heig = commViewDowBgLable.bottom;

    UILabel *shopName = [[UILabel alloc]initWithFrame:CGRectMake(64, heig+18, KWIDTH-150, 16)];
    shopName.font = FontSize(16);
    shopName.text =self.shopModel.shop_name;
    shopName.textColor = K333333;
    [self.scrollView addSubview:shopName];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jindian)];
    shopName.userInteractionEnabled = YES;
    [shopName addGestureRecognizer:ges];
//    [shopName mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(commViewDowBgLable.mas_bottom).offset(18);
//        make.left.offset(64);
//        make.height.offset(14);
//    }];
    UILabel *shopaddress = [[UILabel alloc]initWithFrame:CGRectMake(76, shopName.bottom+10, KWIDTH-90, 14)];
    shopaddress.font = FontSize(12);
    shopaddress.text =self.shopModel.shop_address;
    shopaddress.textColor = [UIColor colorWithHexString:@"#666666"];
    shopaddress.userInteractionEnabled = YES;
    UITapGestureRecognizer *gex = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapactiondddddd)];
    [shopaddress addGestureRecognizer:gex];
    [self.scrollView addSubview:shopaddress];
//    [shopaddress mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(shopName.mas_bottom).offset(10);
//        make.left.offset(76);
//        make.right.offset(-14);
//        make.height.offset(14);
//    }];
    UIImageView *locaimage = [[UIImageView alloc]initWithFrame:CGRectMake(64, shopName.bottom+11, 8, 11)];
    [self.scrollView addSubview:locaimage];
    locaimage.image = imgname(@"locaimage1152");
//    [locaimage mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(shopName.mas_bottom).offset(11);
//        make.left.offset(64);
//        make.height.offset(11);
//        make.width.offset(8);
//
//    }];
    UIButton *phonebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [phonebut setTitle:@"进店逛逛" forState:(UIControlStateNormal)];
    [phonebut setTitleColor:K333333 forState:(UIControlStateNormal)];
    [phonebut addTarget:self action:@selector(phgoneaction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.scrollView addSubview:phonebut];
    phonebut.titleLabel.font = FontSize(12);
    phonebut.frame = CGRectMake(KWIDTH-50-24-16-4, commViewDowBgLable.bottom+18, 70, 24);

    UIButton *phonebutimage = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [phonebutimage setImage:imgname(@"Blockkjintu") forState:(UIControlStateNormal)];
    [phonebutimage addTarget:self action:@selector(phgoneaction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.scrollView addSubview:phonebutimage];
    phonebutimage.titleLabel.font = FontSize(10);
    phonebutimage.frame = CGRectMake(KWIDTH-24-12, commViewDowBgLable.bottom+18, 24, 24);
    
    self.heig = shopImage.bottom+8;
    kWeakSelf;
    CSgoodsShopView *shopCollectView = [[CSgoodsShopView alloc]initWithFrame:CGRectMake(0, shopImage.bottom+8, KWIDTH, 120) modelArr:_shopProductArr block:^(NSInteger index) {
        KMyLog(@"点击了地%ld个",index);
        CSGoodsModel *model  =weakSelf.shopProductArr[index];
        CSgoodsViewController *vc = [CSgoodsViewController new];
        vc.goodsID = model.Id;
        vc.isshop =@"shop";
        [self.navigationController pushViewController:vc animated:YES];

    }];
    [self.scrollView addSubview:shopCollectView];

    if (85 * _shopProductArr.count > KWIDTH) {
        UIPanGestureRecognizer *uptap = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(cehushoushi)];
        shopCollectView.userInteractionEnabled = YES;
        [shopCollectView addGestureRecognizer:uptap];

    }
    heig = shopCollectView.bottom-20;
    UILabel *moreLB33 = [[UILabel alloc]initWithFrame:CGRectMake(0, heig+20, KWIDTH, 32)];
    moreLB33.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    moreLB33.textAlignment = NSTextAlignmentCenter;
    moreLB33.font = FontSize(12);
    moreLB33.text =[NSString stringWithFormat:@"图文详情"];
    moreLB33.textColor = [UIColor colorWithHexString:@"#5D5D5D"];
    [self.scrollView addSubview:moreLB33];
    UILabel *moreLB334 = [[UILabel alloc]initWithFrame:CGRectMake(0, heig+20, KWIDTH/2 - 30, 32)];
    moreLB334.textAlignment = NSTextAlignmentRight;
    moreLB334.text =[NSString stringWithFormat:@"—————"];
    moreLB334.textColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.scrollView addSubview:moreLB334];
    UILabel *moreLB3345 = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH/2+30, heig+20, KWIDTH/2, 32)];
    //    moreLB1.textAlignment = NSTextAlignmentRight;
    moreLB3345.text =[NSString stringWithFormat:@"—————"];
    moreLB3345.textColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.scrollView addSubview:moreLB3345];
    heig = moreLB3345.bottom-9;
    CGFloat hei = [NSString heightWithWidth:KWIDTH-32 font:14 text:_modlel.productInfo];
    UILabel *miaoshulb = [[UILabel alloc]initWithFrame:CGRectMake(16, heig+20, KWIDTH-32, hei)];
    miaoshulb.backgroundColor = [UIColor whiteColor];
    miaoshulb.textAlignment = NSTextAlignmentCenter;
    miaoshulb.font = FontSize(14);
    miaoshulb.text =_modlel.productInfo;
    miaoshulb.numberOfLines = 0;
    miaoshulb.textColor = [UIColor colorWithHexString:@"#0D0E10"];
    [self.scrollView addSubview:miaoshulb];
    
//    heig = miaoshulb.bottom+9;
//
//    KMyLog(@"height0 = %f",heig);
//    if (_imageArr.count >0) {
//
//            for (int i = 0; i < _imageArr.count; i++) {
//                CSBannerModel *model =_imageArr[i];
//                UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, KWIDTH)];
//                mysdimg(image, model.product_img_url);
//
//                [self.scrollView addSubview:image];
//                heig = image.bottom;
//                KMyLog(@"height0++%d = %f",i,heig);
//        }
//    }
//    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);
    
  
    heig = miaoshulb.bottom+9;
    KMyLog(@"height0 = %f",heig);
    if (_imageArr.count >0) {
        
        for (int i = 0; i < _imageArr.count; i++) {
            CSBannerModel *model =_imageArr[i];
            
            UIImageView *imgv = [[UIImageView alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, KHEIGHT)];

            [imgv sd_setImageWithURL:[NSURL URLWithString:model.product_img_url] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                
                UIImage *newImage = [self imageCompressForWidth:imgv.image targetWidth:ScreenW];
                imgv.frame = CGRectMake(0, heig, newImage.size.width, newImage.size.height);
                
                [self.scrollView addSubview:imgv];
                heig = imgv.bottom;
                KMyLog(@"height0++%d = %f",i,heig);
                if (i == self.imageArr.count-1) {
                    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);
                }
            }];
        }
    }
//    self.scrollView.contentSize = CGSizeMake(KWIDTH,heig+kTabbarHeight);

//    UILabel *bg = [[UILabel alloc]initWithFrame:CGRectMake(0,0, KWIDTH, 2000)];
//    [self.scrollView addSubview:bg];
//    self.scrollView.contentSize = CGSizeMake(KWIDTH,2000);
//    self.scrollView.contentSize = CGSizeMake(KWIDTH,2000);

    
//    UILabel *bg = [[UILabel alloc]initWithFrame:CGRectMake(0, shopCollectView.bottom, KWIDTH, 1000)];
//  bg.backgroundColor =   [UIColor colorWithHexString:@"#F2F2F2"];
//    [self.scrollView addSubview:bg];
    
//    UILabel *refaLB = [[UILabel alloc]initWithFrame:CGRectMake(0, shopCollectView.bottom+8, KWIDTH, 11)];
//    refaLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
//    refaLB.textAlignment = NSTextAlignmentCenter;
//    refaLB.font = FontSize(8);
//    refaLB.text =[NSString stringWithFormat:@"上拉查看图文详情"];
//    refaLB.textColor = [UIColor colorWithHexString:@"#666666"];
//        [self.scrollView addSubview:refaLB];
//
//    CGFloat fff = KHEIGHT-refaLB.bottom;
//    if (fff < 0) {
//        fff = 10;
//    }
//    UILabel *refaLBddfdf = [[UILabel alloc]initWithFrame:CGRectMake(0, refaLB.bottom+8, KWIDTH, fff)];
//    refaLBddfdf.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
//
//
//    [self.scrollView addSubview:refaLBddfdf];
//    self.scrollView.contentSize = CGSizeMake(KWIDTH,refaLBddfdf.bottom+20);
}

// init
- (AMapNaviCompositeManager *)compositeManager {
    if (!_compositeManager) {
        _compositeManager = [[AMapNaviCompositeManager alloc] init];  // 初始化
        _compositeManager.delegate = self;  // 如果需要使用AMapNaviCompositeManagerDelegate的相关回调（如自定义语音、获取实时位置等），需要设置delegate
    }
    return _compositeManager;
}

-(void)tapactiondddddd{
    [[QPLocationManager sharedManager] getGps:^(NSString *province, NSString *city, NSString *area, NSString *street) {
        NSLog(@"province=%@,street=%@",province,street);
        [self gaodeSricnd];
        
    }];
    
}

-(void)gaodeSricnd{
    CGFloat longituded = [[USER_DEFAULT objectForKey:@"longitude"] floatValue];
    CGFloat latitude = [[USER_DEFAULT objectForKey:@"latitude"] floatValue];
    
    AMapNaviCompositeUserConfig *config = [[AMapNaviCompositeUserConfig alloc] init];
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeStart location:[AMapNaviPoint locationWithLatitude:latitude longitude:longituded] name:@"当前位置" POIId:@""];     //传入起点，并且带高德POIId
    //    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeWay location:[AMapNaviPoint locationWithLatitude:39.941823 longitude:116.426319] name:@"北京大学" POIId:@"B000A816R6"];            //传入途径点，并且带高德POIId
    
    CGFloat longituded33 = [self.shopModel.lon floatValue];
    CGFloat latitude33 = [self.shopModel.lat floatValue];
    
    
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeEnd location:[AMapNaviPoint locationWithLatitude:latitude33 longitude:longituded33] name:self.shopModel.shop_name POIId:nil];
    
    //传入终点，并且带高德POIId
    [self.compositeManager presentRoutePlanViewControllerWithOptions:config];
}

-(void)cehushoushi{
    NSLog(@"滚动地图");
    [Center postNotificationName:@"beginScroll" object:nil];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"滚动结束");
    [Center postNotificationName:@"endScroll" object:nil];

}

-(void)phgoneaction:(UIButton *)but{
//    [HFTools callMobilePhone: _shopModel.shop_phone];
    [self jindian];
}
/**
 进店
 */
-(void)jindian{
    
    if ([_isshop isEqualToString:@"shop"]) {
        
    }else{
    CSShopViewController *jin = [[CSShopViewController alloc]init];
    jin.shopid = _shopModel.Id;
    [self.navigationController pushViewController:jin animated:YES];
    
    }
}
/**
 查看更多评价
 */
-(void)lookMallAction{
    KMyLog(@"查看更多评价");
    NSNotification *nof = [NSNotification notificationWithName:@"refasthcommit" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:nof];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

//    CGFloat hight = scrollView.frame.size.height;
//    CGFloat contentOffset = scrollView.contentOffset.y;
//    CGFloat distanceFromBottom = scrollView.contentSize.height - contentOffset;
//    CGFloat offset = contentOffset - self.lastcontentOffset;
//    self.lastcontentOffset = contentOffset;
//    
//    if (offset > 0 && contentOffset > 0) {
////        NSLog(@"上拉行为");
//    }
//    if (offset < 0 && distanceFromBottom > hight) {
////        NSLog(@"下拉行为");
//    }
//    if (contentOffset == 0) {
////        NSLog(@"滑动到顶部");
//    }
//    if (distanceFromBottom < hight) {
////        NSLog(@"滑动到底部");
//        self.isrefa = YES;
//            if (offset > 0 && contentOffset>360) {
//                NSLog(@"上拉行为");
//                KMyLog(@"________刷新%f",contentOffset);
//                NSNotification *nof = [NSNotification notificationWithName:@"refasth" object:nil userInfo:nil];
//                [[NSNotificationCenter defaultCenter] postNotification:nof];
//
//
//                
//            }
//        
//        
//    }
    
//}

- (XSQBanner *)banner {
    if (!_banner) {
        _banner = [[XSQBanner alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KWIDTH)];
        _banner.pageLocation = PageControlLocationCenter;
    }
    return _banner;
}

//- (SPCarouselView *)banner {
//    if (!_banner) {
//        _banner = [[SPCarouselView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KWIDTH)];
//        //_banner.pageLocation = PageControlLocationCenter;
//        _banner.pageControlPosition = SPPageContolPositionBottomCenter;
//    }
//    return _banner;
//}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

#pragma mark +++获取图片高度
- (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth{
    
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = height / (width / targetWidth);
    CGSize size = CGSizeMake(targetWidth, targetHeight);
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    UIGraphicsEndImageContext();
    return newImage;
}


@end
