//
//  ZYBuyNowShopDetailWebCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYShopPartDetailModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol ZYBuyNowShopDetailWebCellDelegate <NSObject>
//获得网页高度后刷新界面
- (void)buyNowShopDetailWebCellReload:(CGFloat)cellHeight;
@end
@interface ZYBuyNowShopDetailWebCell : UITableViewCell
@property (nonatomic, strong) ZYShopPartDetailModel *partModel;
@property (nonatomic, copy) NSString *partID;//配件ID
@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, weak) id <ZYBuyNowShopDetailWebCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
