//
//  WQgoodsInfoTableViewCell.h
//  ZengBei
//
//  Created by 魏堰青 on 2018/12/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WQgoodsInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *myLeftLB;
@property (weak, nonatomic) IBOutlet UILabel *myRightLB;

@end
