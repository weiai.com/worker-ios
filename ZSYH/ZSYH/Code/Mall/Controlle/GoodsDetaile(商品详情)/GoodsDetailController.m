//
//  GoodsDetailController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "GoodsDetailController.h"
#import "GoodsDetailModel.h"
//#import "ChooseAttrView.h"
#import "WQgoodsInfoViewController.h"
#import "CSGoodsModel.h"
#import "CSorderListViewController.h"

@interface GoodsDetailController ()<UIWebViewDelegate>
@property (nonatomic, strong) XSQBanner *banner;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CSGoodsModel *modlel;
@property (nonatomic, strong) CSGoodsModel *attView;
@property (strong, nonatomic) UIWebView *webView;
@property (assign, nonatomic) CGFloat flobot;
@property (strong, nonatomic) NSMutableArray *orderTotalGoods;
@property (strong, nonatomic) NSMutableArray *chunzhiArr;

@end

@implementation GoodsDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品详情";
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = NOTNIL(_goodsID);
    kWeakSelf;
    [NetWorkTool POST:@"product/getProInfoById" param:mudic success:^(id dic) {
       weakSelf.modlel = [[CSGoodsModel alloc]init];
        [weakSelf.modlel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        [self setGoodsUI];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
    
}

- (void)setGoodsUI {
    [self.view addSubview:self.scrollView];
    
    for (int i = 0; i < 2; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(KWIDTH/2.0*i, KHEIGHT-50, KWIDTH/2, 50);
        btn.titleLabel.font = FontSize(15);
        btn.tag = 3333+i;
        btn.backgroundColor = i==0?[UIColor lightGrayColor]:KBColor;
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setTitle:i==0?@"加入购物车":@"立即购买" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(buyAddCart:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
    //创建产品详情
    [self.scrollView addSubview:self.banner];
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    [muarr addObject:_modlel.productPic];
    [_banner updateImageArr:muarr AndImageClickBlock:^(NSInteger index) {
    }];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(10, _banner.bottom+10, KWIDTH-20, 30)];
    name.textColor = KTEXTCOLOR;
    name.font = FontSize(18);
    name.text = _modlel.productName;
    name.numberOfLines = 0;
    [name sizeToFit];
    [self.scrollView addSubview:name];
    //显示抢购
//    if (_xianShi) {
//        WQFlashsaleView *myFashSale = [[WQFlashsaleView alloc]init];
//        [self.scrollView addSubview:myFashSale];
//        myFashSale.backgroundColor = [UIColor redColor];
//        myFashSale.frame = CGRectMake(0, name.top-65, KWIDTH, 55);
//        [myFashSale showUIwithmodel:_model];
//    }
    UILabel *price = [[UILabel alloc]initWithFrame:CGRectMake(10, name.bottom+10, KWIDTH-20, 20)];
    price.textColor = RGBA(252, 23, 39,1);
    price.font = FontSize(18);
    price.text =[NSString stringWithFormat:@"¥%@",_modlel.productPrice];
    [self.scrollView addSubview:price];
    
    UILabel *sells = [[UILabel alloc]initWithFrame:CGRectMake(10, price.bottom+10, KWIDTH-20, 20)];
    sells.textColor = [UIColor colorWithHexString:@"#666666"];
    sells.font = FontSize(13);
    //NSString *sellstr = _model.sales_sum.intValue>10000?[NSString stringWithFormat:@"%.1f 万",_model.sales_sum.floatValue/10000.0]:_model.sales_sum;
    sells.text = [NSString stringWithFormat:@"销量:%@",_modlel.count];
    [self.scrollView addSubview:sells];
    
    UIButton *collect = [UIButton buttonWithType:UIButtonTypeCustom];
    collect.titleLabel.font = FontSize(14);
    collect.frame = CGRectMake(KWIDTH-70, sells.top, 65, 20);
    NSString *imgname = [_modlel.productState isEqualToString:@"1"]?@"已收藏":@"收藏";
    [collect setImage:imgname(imgname) forState:UIControlStateNormal];
    [collect setTitle:[_modlel.productState isEqualToString:@"1"]?@" 已收藏":@" 收藏" forState:UIControlStateNormal];
    [collect setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
//    [collect addTarget:self action:@selector(collectionGoods:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:collect];
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, sells.bottom+15, KWIDTH, 5)];
    line.backgroundColor = KRGB(233, 233, 233);
    [self.scrollView addSubview:line];
    
    UILabel *choose = [[UILabel alloc]initWithFrame:CGRectMake(10, line.bottom, KWIDTH-20, 40)];
    choose.textColor = [UIColor colorWithHexString:@"#666666"];
    choose.font = FontSize(14);
    choose.text = @"请选择产品规格属性";
    [choose whenTapped:^{
        [self.view addSubview:self.attView];
    }];
    [self.scrollView addSubview:choose];
    
    UIView *line2 = [[UIView alloc]initWithFrame:CGRectMake(0, choose.bottom, KWIDTH, 5)];
    line2.backgroundColor = KRGB(233, 233, 233);
    [self.scrollView addSubview:line2];
    //    UIImageView *arrow = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH-15, 17, 8, 15)];
    //    arrow.centerY = choose.centerY;
    //    arrow.image = Image(@"arrow1");
    //    [self.scrollView addSubview:arrow];
    //
    //    UIView *shopView = [[UIView alloc]initWithFrame:CGRectMake(0, line2.bottom, KWIDTH, 40)];
    //
    //    UIImageView *pic = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 15)];
    //    pic.image = imgname(@"评价");
    //    [shopView addSubview:pic];
    //
    //    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(pic.right+10, 10, KWIDTH-pic.right-20, 15)];
    //    lab.textColor = KTEXTCOLOR;
    //    lab.font = KTEXTFONT;
    //    NSString *str = [NSString stringWithFormat:@"评价：%ld",_model.evaluate.count];
    //    lab.text = str;
    //    [shopView addSubview:lab];
    //    UIButton *shop = [UIButton buttonWithType:UIButtonTypeCustom];
    //    shop.titleLabel.font = FontSize(13);
    //    shop.frame = CGRectMake(0, 0, 120, 40);
    //    shop.userInteractionEnabled = NO;
    //    [shop setImage:imgname(@"评价") forState:UIControlStateNormal];
    //    [shop setTitle:str forState:UIControlStateNormal];
    //    [shop setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    //    [shopView addSubview:shop];
    
    //    UIImageView *arrow2 = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH-15, 17, 8, 15)];
    //    arrow2.centerY = pic.centerY;
    //    arrow2.image = Image(@"arrow1");
    //    [shopView addSubview:arrow2];
    //    [self.scrollView addSubview:shopView];
    //    [shopView whenTapped:^{
    //        [self showComments];
    //    }];

    CGFloat ch = 0;
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0, line2.bottom, KWIDTH, 40)];
        image.image = imgname(@"——详情——.png");
        [self.scrollView addSubview:image];
        ch = image.bottom;
    
    if (_modlel.count > 0) {
        
        UILabel *censhuLB = [[UILabel alloc]initWithFrame:CGRectMake(10, ch, 60, 40)];
        censhuLB.textColor = [UIColor blackColor];
        censhuLB.text = @"参数";
        censhuLB.font = [UIFont systemFontOfSize:14];
        [self.scrollView addSubview:censhuLB];
        NSString *butStr = @"";
//        if (_model.goods_info.attr.count == 1) {
//            SpecPropModel *mod = _model.goods_info.attr[0];
//            butStr = [NSString stringWithFormat:@"%@",mod.name];
//        }else{
//            SpecPropModel *mod = _model.goods_info.attr[0];
//            SpecPropModel *mod1 = _model.goods_info.attr[1];
//            butStr = [NSString stringWithFormat:@"%@  %@",mod.name,mod1.name];
//        }
        UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [showButton setTitle:butStr forState:UIControlStateNormal];
        showButton.frame = CGRectMake(70, ch, KWIDTH-80, 40) ;
        showButton.titleLabel.font = [UIFont systemFontOfSize:14];
        showButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [showButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.scrollView addSubview:showButton];
        [showButton addTarget:self action:@selector(shougoodsInfo) forControlEvents:UIControlEventTouchUpInside];
        ch += 40;
    }
    
    self.flobot =ch;
    _scrollView.contentSize = CGSizeMake(KWIDTH, ch);
    
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, ch, KWIDTH, 0)];
    _webView.delegate = self;
    adjustInset(self.webView.scrollView);
    [self.scrollView addSubview:_webView];
//    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@api/goods/view.html?goods_id=%@",HOST_URL,@""]]]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.jianshu.com/p/5b2b6fa88349"]]];
    //添加监听
    [_webView.scrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if (object == _webView.scrollView && [keyPath isEqualToString:@"contentSize"]) {
        CGSize fir = [_webView sizeThatFits:CGSizeZero];
        _webView.height = fir.height;
        CGFloat dfdfdf = _flobot + fir.height;
        self.scrollView.contentSize = CGSizeMake(KWIDTH,dfdfdf);
    }
}

-(void)shougoodsInfo{
    WQgoodsInfoViewController  *info = [[WQgoodsInfoViewController alloc]init];
    info.providesPresentationContextTransitionStyle = YES;
    info.definesPresentationContext = YES;
    [info setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    info.model = _model;
    [self presentViewController:info animated:YES completion:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    float height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"]floatValue];
    _webView.height = height;
    self.scrollView.contentSize = CGSizeMake(KWIDTH, _webView.bottom);
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    self.scrollView.contentSize = CGSizeMake(KWIDTH, _webView.bottom);
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)buyAddCart:(UIButton *)btn {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"carId"] = @"";
    param[@"count"] = @"1";
    param[@"id"] = NOTNIL(_modlel.Id);
    param[@"payState"] = @"0";
    param[@"productId"] = NOTNIL(_modlel.productTypeId);
   
    NSDictionary *dic = @{@"products":param};
    NSString *jssy = [HFTools toJSONString:dic];
    NSDictionary *chuDic = @{@"data":jssy};
    [NetWorkTool POST:@"shop/addCar" param:chuDic success:^(id dic) {
        ShowToastWithText(@"添加购物车成功");
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
//    [self.view addSubview:self.attView];
//    [self.attView.buyBtn setBackgroundColor:KBColor];
//    if (btn.tag == 3333) {
//        [self.attView.buyBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
//    } else {
//        [self.attView.buyBtn setTitle:@"立即购买" forState:UIControlStateNormal];
//    }
}

- (XSQBanner *)banner {
    if (!_banner) {
        _banner = [[XSQBanner alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KWIDTH)];
        _banner.pageLocation = PageControlLocationCenter;
    }
    return _banner;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-50)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

//- (ChooseAttrView *)attView {
//    if (!_attView) {
//        _attView = [[NSBundle mainBundle]loadNibNamed:@"ChooseAttrView" owner:nil options:nil].firstObject;
//        _attView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
//        _attView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
//        _attView.goodPrice.text = _model.goods_info.price;
//        if ([_model.goods_info.goods_images count]>0) {
//
//            _attView.GooModel = _model;
//            [_attView.goodPic sd_setImageWithURL:[NSURL URLWithString:append(_model.goods_info.goods_images[0])]];
//        }
//        [_attView.buyBtn addTarget:self action:@selector(addCartClick) forControlEvents:UIControlEventTouchUpInside];
//        [[_attView.close rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
//            [_attView removeFromSuperview];
//        }];
//    }
//
//    if (_attView.modelArr == nil || _attView.modelArr.count == 0) {
//        [self requestAttri];
//    }
//    return _attView;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
