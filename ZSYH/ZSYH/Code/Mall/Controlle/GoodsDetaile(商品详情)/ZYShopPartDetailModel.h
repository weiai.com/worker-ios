//
//  ZYShopPartDetailModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"
#import "ZYShopPartModel.h"

@class ZYShopPartTypeModel,ZYShopStoreInfoModel,ZYShopPartCycleModel;

NS_ASSUME_NONNULL_BEGIN

@interface ZYShopPartDetailModel : BaseModel
@property (nonatomic, copy) NSString *parts_details;//产品介绍
@property (nonatomic, copy) NSString *parts_remarks;//图文详情

@property (nonatomic, strong) NSArray *attribute;//类型
@property (nonatomic, assign) BOOL isOpenAttribute;//用于标记是否展开了类型
@property (nonatomic, assign) CGFloat attributeHeight;//用于存储类型高度
@property (nonatomic, assign) CGFloat attributeSelectedHeight;//用于存储选中类型后的标签高度

@property (nonatomic, strong) ZYShopStoreInfoModel *factory;//店铺
@property (nonatomic, strong) NSArray <ZYShopPartCycleModel *> *lbImg;//轮播图

@property (nonatomic, copy) NSString *lower_agentA;//省级经销商最低购买数
@property (nonatomic, copy) NSString *lower_agentB;//市县级经销商最低购买数
@property (nonatomic, copy) NSString *lower_user;//用户最低购买量
@property (nonatomic, assign) NSInteger buyNumber;//用于存储加入购物车的数量
@property (nonatomic, copy) NSString *contactPhone;//产品详情咨询电话
@end

// 配件类型model
@interface ZYShopPartTypeModel : BaseModel
@property (nonatomic, copy) NSString *compensationPrice;//赔付金额
@property (nonatomic, copy) NSString *quantity;//
@property (nonatomic, copy) NSString *salePrice;//
@property (nonatomic, copy) NSString *install;//有效期
@property (nonatomic, copy) NSString *contractPrice;//
@property (nonatomic, copy) NSString *partsId;//类型ID
@property (nonatomic, copy) NSString *name;//型号名称
@property (nonatomic, copy) NSString *warranty;//保质期
@property (nonatomic, copy) NSString *createDate;//保质期
@property (nonatomic, copy) NSString *soft;//保质期
@property (nonatomic, copy) NSString *freeWarranty;//
@property (nonatomic, assign) BOOL isSelected;//用于记录选中的标签
@end

//店铺信息model
@interface ZYShopStoreInfoModel : BaseModel
@property (nonatomic, copy) NSString *factory_person;//店铺所有人
@property (nonatomic, copy) NSString *factory_phone;//电话
@property (nonatomic, copy) NSString *factory_address;//地址
@property (nonatomic, copy) NSString *modifyDate;//
@property (nonatomic, copy) NSString *factory_area;//
@property (nonatomic, copy) NSString *factory_code;//
@property (nonatomic, copy) NSString *factory_name;//店铺名称
@property (nonatomic, copy) NSString *factory_img;//店铺图片
@property (nonatomic, copy) NSString *factory_remarks;//店铺简介
@property (nonatomic, copy) NSString *factory_email;//店铺邮件
@property (nonatomic, copy) NSString *factory_password;//
@property (nonatomic, copy) NSString *createDate;//
@property (nonatomic, strong) NSArray <ZYShopPartModel *>*parts;//店铺产品列表
@end

//轮播图model
@interface ZYShopPartCycleModel : BaseModel
@property (nonatomic, copy) NSString *image_url;//图片地址
@property (nonatomic, copy) NSString *parts_id;
@property (nonatomic, copy) NSString *sort;
@property (nonatomic, copy) NSString *type;
@end

NS_ASSUME_NONNULL_END
