//
//  ZYBuyNowShopDetailBottomView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopDetailBottomView.h"

@implementation ZYBuyNowShopDetailBottomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH /4, self.height)];
    leftView.backgroundColor = kColorWithHex(0xffffff);
    [self addSubview:leftView];
    
    UILabel *line1 = [[UILabel alloc]init];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    line1.frame = CGRectMake(0, 0, leftView.width, 1);
    [leftView addSubview:line1];
    
    UIButton *btnPhone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPhone.frame = leftView.bounds;
    [btnPhone setImage:imgname(@"smallPhoneImg") forState:(UIControlStateNormal)];
    [btnPhone setTitle:@"咨询电话" forState:UIControlStateNormal];
    [btnPhone setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    btnPhone.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnPhone.titleLabel.font = FontSize(12);
    btnPhone.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;//使图片和文字水平居中显示
    [btnPhone setTitleEdgeInsets:UIEdgeInsetsMake(btnPhone.imageView.frame.size.height+10 ,-btnPhone.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
    [btnPhone setImageEdgeInsets:UIEdgeInsetsMake(-10, 0.0,0.0, -btnPhone.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
    btnPhone.tag = 200;
    [btnPhone addTarget:self action:@selector(btnBottomClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [leftView addSubview:btnPhone];
    
    UIView *centerView = [[UIView alloc] initWithFrame:CGRectMake(leftView.width, 0, leftView.width, leftView.height)];
    centerView.backgroundColor = kColorWithHex(0xffffff);
    [self addSubview:centerView];
    
    UILabel *line2 = [[UILabel alloc]init];
    line2.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    line2.frame = CGRectMake(0, 0, centerView.width, 1);
    [centerView addSubview:line2];
    
    UILabel *line3 = [[UILabel alloc]init];
    line3.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    line3.frame = CGRectMake(0, 0, 1, centerView.height);
    [centerView addSubview:line3];
    
    UIButton *btnMessage = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMessage.frame = leftView.bounds;
    [btnMessage setImage:imgname(@"liuyan") forState:(UIControlStateNormal)];
    [btnMessage setTitle:@"留言" forState:UIControlStateNormal];
    [btnMessage setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    btnMessage.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnMessage.titleLabel.font = FontSize(12);
    btnMessage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;//使图片和文字水平居中显示
    [btnMessage setTitleEdgeInsets:UIEdgeInsetsMake(btnMessage.imageView.frame.size.height+10 ,-btnMessage.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
    [btnMessage setImageEdgeInsets:UIEdgeInsetsMake(-10, 0.0,0.0, -btnMessage.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
    btnMessage.tag = 201;
    [btnMessage addTarget:self action:@selector(btnBottomClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:btnMessage];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(centerView.right, 0, KWIDTH /2, centerView.height)];
    rightView.backgroundColor = kColorWithHex(0x70be68);
    [self addSubview:rightView];
    
    UIButton *btnAddOrder = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddOrder.frame = rightView.bounds;
    [btnAddOrder setTitle:@"加入预订" forState:UIControlStateNormal];
    [btnAddOrder setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnAddOrder.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    btnAddOrder.titleLabel.textAlignment = NSTextAlignmentCenter;
    btnAddOrder.titleLabel.font = FontSize(16);
    btnAddOrder.tag = 202;
    [btnAddOrder addTarget:self action:@selector(btnBottomClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:btnAddOrder];
}
#pragma mark - 交互事件
- (void)btnBottomClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailBottomViewClick:)]) {
        [self.delegate detailBottomViewClick:tag];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
