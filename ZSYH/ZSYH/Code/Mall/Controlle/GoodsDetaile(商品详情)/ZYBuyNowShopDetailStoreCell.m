//
//  ZYBuyNowShopDetailStoreCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowShopDetailStoreCell.h"

@interface ZYBuyNowShopDetailStoreCell ()
@property (nonatomic, strong) UIImageView *ivStore;//店铺图片
@property (nonatomic, strong) UILabel *lblStore;//店铺名称
@property (nonatomic, strong) UILabel *lblDetailStore;//店铺简介
//展示的四个产品
@property (nonatomic, strong) UIImageView *ivShopOne;
@property (nonatomic, strong) UILabel *lblShopOne;
@property (nonatomic, strong) UIImageView *ivShopTwo;
@property (nonatomic, strong) UILabel *lblShopTwo;
@property (nonatomic, strong) UIImageView *ivShopThree;
@property (nonatomic, strong) UILabel *lblShopThree;
@property (nonatomic, strong) UIImageView *ivShopFour;
@property (nonatomic, strong) UILabel *lblShopFour;

@end

@implementation ZYBuyNowShopDetailStoreCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    _ivStore = [[UIImageView alloc] initWithFrame:CGRectMake(16, 16, 40, 40)];
    [self.contentView addSubview:_ivStore];
    
    _lblStore = [[UILabel alloc] initWithFrame:CGRectMake(69, 16, kScreen_Width - 160, 22)];
    _lblStore.font = [UIFont systemFontOfSize:16];
    _lblStore.textColor = kColorWithHex(0x333333);
    [self.contentView addSubview:_lblStore];
    
    _lblDetailStore = [[UILabel alloc] initWithFrame:CGRectMake(69, 39, kScreen_Width - 92, 17)];
    _lblDetailStore.font = [UIFont systemFontOfSize:12];
    _lblDetailStore.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:_lblDetailStore];
    
    UILabel *lblNotice = [[UILabel alloc] initWithFrame:CGRectMake(kScreen_Width - 80, 16, 50, 22)];
    lblNotice.text = @"进店逛逛";
    lblNotice.font = [UIFont systemFontOfSize:12];
    lblNotice.textColor = kColorWithHex(0x333333);
    [self.contentView addSubview:lblNotice];
    UIImageView *ivArrow = [[UIImageView alloc] initWithFrame:CGRectMake(kScreen_Width - 30, 15, 24, 24)];
    ivArrow.image = imgname(@"arrow_right_small_icon");
    [self.contentView addSubview:ivArrow];
    
    UITapGestureRecognizer *tapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClickAction:)];
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClickAction:)];
    UITapGestureRecognizer *tapThree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClickAction:)];
    UITapGestureRecognizer *tapFour = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClickAction:)];
    
    CGFloat ivWidth = (kScreen_Width - 32 - 56 * 4) / 3.0;
    _ivShopOne = [[UIImageView alloc] initWithFrame:CGRectMake(16, 78, 56, 64)];
    _ivShopOne.userInteractionEnabled = YES;
    [_ivShopOne addGestureRecognizer:tapOne];
    _ivShopOne.tag = 200;
    [self.contentView addSubview:_ivShopOne];
    
    _ivShopTwo = [[UIImageView alloc] initWithFrame:CGRectMake(72 + ivWidth, 78, 56, 64)];
    _ivShopTwo.userInteractionEnabled = YES;
    [_ivShopTwo addGestureRecognizer:tapTwo];
    _ivShopTwo.tag = 201;
    [self.contentView addSubview:_ivShopTwo];
    
    _ivShopThree = [[UIImageView alloc] initWithFrame:CGRectMake(128 + 2 * ivWidth, 78, 56, 64)];
    _ivShopThree.userInteractionEnabled = YES;
    [_ivShopThree addGestureRecognizer:tapThree];
    _ivShopThree.tag = 202;
    [self.contentView addSubview:_ivShopThree];
    
    _ivShopFour = [[UIImageView alloc] initWithFrame:CGRectMake(184 + 3 * ivWidth, 78, 56, 64)];
    _ivShopFour.userInteractionEnabled = YES;
    [_ivShopFour addGestureRecognizer:tapFour];
    _ivShopFour.tag = 203;
    [self.contentView addSubview:_ivShopFour];
    
    _lblShopOne = [UILabel new];
    _lblShopOne.font = [UIFont systemFontOfSize:10];
    _lblShopOne.textColor = kColorWithHex(0x333333);
    _lblShopOne.textAlignment = NSTextAlignmentCenter;
    _lblShopOne.numberOfLines = 2;
    [self.contentView addSubview:_lblShopOne];
    [_lblShopOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivShopOne.mas_centerX);
        make.top.equalTo(self.ivShopOne.mas_bottom).offset(5);
        make.width.mas_equalTo(70);
    }];
    
    _lblShopTwo = [UILabel new];
    _lblShopTwo.font = [UIFont systemFontOfSize:10];
    _lblShopTwo.textColor = kColorWithHex(0x333333);
    _lblShopTwo.textAlignment = NSTextAlignmentCenter;
    _lblShopTwo.numberOfLines = 2;
    [self.contentView addSubview:_lblShopTwo];
    [_lblShopTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivShopTwo.mas_centerX);
        make.top.equalTo(self.ivShopTwo.mas_bottom).offset(5);
        make.width.mas_equalTo(70);
    }];
    
    _lblShopThree = [UILabel new];
    _lblShopThree.font = [UIFont systemFontOfSize:10];
    _lblShopThree.textColor = kColorWithHex(0x333333);
    _lblShopThree.textAlignment = NSTextAlignmentCenter;
    _lblShopThree.numberOfLines = 2;
    [self.contentView addSubview:_lblShopThree];
    [_lblShopThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivShopThree.mas_centerX);
        make.top.equalTo(self.ivShopThree.mas_bottom).offset(5);
        make.width.mas_equalTo(70);
    }];
    
    _lblShopFour = [UILabel new];
    _lblShopFour.font = [UIFont systemFontOfSize:10];
    _lblShopFour.textColor = kColorWithHex(0x333333);
    _lblShopFour.textAlignment = NSTextAlignmentCenter;
    _lblShopFour.numberOfLines = 2;
    [self.contentView addSubview:_lblShopFour];
    [_lblShopFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivShopFour.mas_centerX);
        make.top.equalTo(self.ivShopFour.mas_bottom).offset(5);
        make.width.mas_equalTo(70);
    }];
    
}
#pragma mark - 界面更新
- (void)setPartModel:(ZYShopPartDetailModel *)partModel{
    if (partModel) {
        _partModel = partModel;
        [HFTools imageViewUpdateWithUrl:partModel.factory.factory_img withImageView:_ivStore withPlaceholderImage:@""];
        _lblStore.text = partModel.factory.factory_name;
        _lblDetailStore.text = partModel.factory.factory_remarks;
        
        NSInteger tagOne = _ivShopOne.tag;
        ZYShopPartModel *partModelOne = [self.partModel.factory.parts objectAtIndexSafe:(tagOne - 200)];
        [HFTools imageViewUpdateWithUrl:partModelOne.image_url withImageView:_ivShopOne withPlaceholderImage:@""];
        _lblShopOne.text = partModelOne.parts_name;
        
        NSInteger tagTwo = _ivShopTwo.tag;
        ZYShopPartModel *partModelTwo = [self.partModel.factory.parts objectAtIndexSafe:(tagTwo - 200)];
        [HFTools imageViewUpdateWithUrl:partModelTwo.image_url withImageView:_ivShopTwo withPlaceholderImage:@""];
        _lblShopTwo.text = partModelTwo.parts_name;
        
        NSInteger tagThree = _ivShopThree.tag;
        ZYShopPartModel *partModelThree = [self.partModel.factory.parts objectAtIndexSafe:(tagThree - 200)];
        [HFTools imageViewUpdateWithUrl:partModelThree.image_url withImageView:_ivShopThree withPlaceholderImage:@""];
        _lblShopThree.text = partModelThree.parts_name;
        
        NSInteger tagFour = _ivShopFour.tag;
        ZYShopPartModel *partModelFour = [self.partModel.factory.parts objectAtIndexSafe:(tagFour - 200)];
        [HFTools imageViewUpdateWithUrl:partModelFour.image_url withImageView:_ivShopFour withPlaceholderImage:@""];
        _lblShopFour.text = partModelFour.parts_name;
        
    }
}
#pragma mark - 交互事件
- (void)tapClickAction:(UITapGestureRecognizer *)tap{
    UIImageView *ivImage = (UIImageView *)tap.view;
    NSInteger tag = ivImage.tag;
    ZYShopPartModel *partModel = [self.partModel.factory.parts objectAtIndexSafe:(tag - 200)];
    if (partModel && self.delegate && [self.delegate respondsToSelector:@selector(shopDetailStoreCellClickPart:)]) {
        [self.delegate shopDetailStoreCellClickPart:partModel.Id];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
