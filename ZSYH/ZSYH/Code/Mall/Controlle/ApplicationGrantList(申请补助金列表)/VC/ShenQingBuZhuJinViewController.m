//
//  ShenQingBuZhuJinViewController.m
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "ShenQingBuZhuJinViewController.h"
#import "IQTextView.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "SkillsCerModel.h"

@interface ShenQingBuZhuJinViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *anZhuangXinXiBgView;
@property (nonatomic, strong) UIView *guZhangBgView;
//@property (nonatomic, strong) IQTextView *guZhangTextView;
@property (nonatomic, strong) UITextField *guZhangTextView;

@property (nonatomic, strong) UIView *addphotoBgView;
@property (nonatomic, strong) UIView *shangChuanBgView;

@property (nonatomic, strong) NSMutableArray *addImageArr;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, strong) UIImageView *myimage;

@property (nonatomic, assign) BOOL showData;//数据展示初始数据
@property (nonatomic, strong) NSMutableArray *deleteIdsArr;//删除的图片id数组

@property (nonatomic, strong) UILabel *nameLab;
@property (nonatomic, strong) UILabel *xingHaoLab;
@property (nonatomic, strong) UILabel *gongShiBuZhuJinLab;
@property (nonatomic, strong) UILabel *erWeiMaYouXiaoQiLab;
@property (nonatomic, strong) UILabel *wuYouZhiBaoQiLab;
@property (nonatomic, strong) UILabel *buTieQiLab;
@property (nonatomic, strong) UILabel *anZhuangRiQiLab;

@property (nonatomic, strong) UILabel *xingMingLab;
@property (nonatomic, strong) UILabel *phoneLab;
@property (nonatomic, strong) UILabel *diZhiLab;
@property (nonatomic, strong) UILabel *feiYongLab;

@property (nonatomic, strong) UIView *shouHuoInfoBgView;
@property (nonatomic, strong) UITextField *nameTF;
@property (nonatomic, strong) UITextField *phoneTF;
//@property (nonatomic, strong) IQTextView *diZhiTextView;
@property (nonatomic, strong) UITextView *diZhiTextView;
@property (nonatomic, strong) UIView *bgViewsecOne;

@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UILabel *tipsLab;
@property (nonatomic, assign) BOOL deleteLastObject;
@property (nonatomic, strong) UIView *imageBackView;

@property (nonatomic, strong) UILabel *diZhiWordLab;
@property (nonatomic, strong) UILabel *feiYongWordLab;
@property (nonatomic, strong) UILabel *zhaoPianWordLab;
@property (nonatomic, strong) UILabel *placeLab;
@property (nonatomic, strong) UILabel *stirngLenghLabel;

@end

@implementation ShenQingBuZhuJinViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"申请补助金";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.imageArr  = [NSMutableArray array];
    self.deleteIdsArr = [NSMutableArray array];
    self.addImageArr = [NSMutableArray array];
    [self configViews];
    [self requestJudgePartsStatus];
    [self refreshViewsData];
    
    [self shwoBgviewsec];
}

-(void)refreshViewsData {
    
    self.nameLab.text = self.detailModel.type_name;
    self.xingHaoLab.text  = self.detailModel.attributeName;
    self.gongShiBuZhuJinLab.text  = [NSString stringWithFormat:@"%@%@",@"¥",self.detailModel.compensationPrice];
    self.erWeiMaYouXiaoQiLab.text  =[NSString stringWithFormat:@"%@至%@",self.detailModel.productionDate,self.detailModel.termValidity];
    self.wuYouZhiBaoQiLab.text  = [NSString stringWithFormat:@"%@个月",self.detailModel.freeWarranty];
    self.buTieQiLab.text  = [NSString stringWithFormat:@"候保工时费补贴期：%@个月",self.detailModel.warranty];
    self.anZhuangRiQiLab.text  = self.detailModel.anTime;
    
    self.xingMingLab.text  = self.detailModel.user_name;
    self.phoneLab.text  = self.detailModel.user_phone;
    self.diZhiLab.text  = self.detailModel.user_address;
    self.feiYongLab.text  = self.detailModel.cost;
}

//是否可以申请补助金
-(void)requestJudgePartsStatus {
    
    NSDictionary *param = @{
        @"qrCode":NOTNIL(self.qrCode)
    };
    [NetWorkTool POST:judgePartsStatus param:param success:^(id dic) {
        //隐藏下面内容
        NSString *errorCode = dic[@"errorCode"];
        if ([errorCode isEqualToString:@"0"]) {
            self.anZhuangXinXiBgView.hidden = NO;
            self.guZhangBgView.hidden = NO;
            self.addphotoBgView.hidden = NO;
            self.shouHuoInfoBgView.hidden = NO;
            
            [self addAnZhuangXinXiView];
            [self addGuZhangMiaoShuView];
            [self addShangChuanGuZhangPhotoView];
            [self addShuoHuoXinXiView];
        }
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        //隐藏下面
        NSString *errorCode = dic[@"errorCode"];
        if ([errorCode isEqualToString:@"10001"]) {
            self.anZhuangXinXiBgView.hidden = YES;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self alreadyInstallView];
            [self.imageV setImage:imgname(@"errorCode001")];
            self.tipsLab.text = @"此配件暂未安装";
            KMyLog(@"11111111-------%@", dic[@"errorCode"]);
        } else if ([errorCode isEqualToString:@"10002"]) {
            
            self.anZhuangXinXiBgView.hidden = YES;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self alreadyInstallView];
            
            [self.imageV setImage:imgname(@"errorCode002")];
            self.tipsLab.text = @"此配件已安装,候保保障中~";
            KMyLog(@"22222222-------%@", dic[@"errorCode"]);
        } else if ([errorCode isEqualToString:@"10003"]) {
            
            self.anZhuangXinXiBgView.hidden = NO;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self addAnZhuangXinXiView];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH/2-179/2, self.anZhuangXinXiBgView.bottom + 42, 179, 166.5)];
            [imageV setImage:imgname(@"errorCode003")];
            [self.scrollView addSubview:imageV];
            self.imageV = imageV;
            
            UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(84, imageV.bottom + 39, KWIDTH - 84*2, 20)];
            tipsLab.text = @"此配件已申请补助金";
            tipsLab.font = FontSize(13);
            tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
            tipsLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:tipsLab];
            self.tipsLab = tipsLab;
            KMyLog(@"33333333-------%@", dic[@"errorCode"]);
        } else if ([errorCode isEqualToString:@"10004"]) {
            
            self.anZhuangXinXiBgView.hidden = NO;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self anZhuangXinXiBgView];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH/2-179/2, self.anZhuangXinXiBgView.bottom + 42, 179, 166.5)];
            [imageV setImage:imgname(@"errorCode004005")];
            [self.scrollView addSubview:imageV];
            self.imageV = imageV;
            
            UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(74, imageV.bottom + 39, KWIDTH - 84*2, 20)];
            tipsLab.text = @"此配件已安装,服役期已满";
            tipsLab.font = FontSize(13);
            tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
            tipsLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:tipsLab];
            self.tipsLab = tipsLab;
            KMyLog(@"4444444455555555-------%@", dic[@"errorCode"]);
        } else if ([errorCode isEqualToString:@"10005"]) {
            
            self.anZhuangXinXiBgView.hidden = NO;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self anZhuangXinXiBgView];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH/2-179/2, self.anZhuangXinXiBgView.bottom + 42, 179, 166.5)];
            [imageV setImage:imgname(@"errorCode004005")];
            [self.scrollView addSubview:imageV];
            self.imageV = imageV;
            
            UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(74, imageV.bottom + 39, KWIDTH - 84*2, 20)];
            tipsLab.text = @"此配件已安装,服役期已满";
            tipsLab.font = FontSize(13);
            tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
            tipsLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:tipsLab];
            self.tipsLab = tipsLab;
            KMyLog(@"4444444455555555-------%@", dic[@"errorCode"]);
        }else {
            
            self.anZhuangXinXiBgView.hidden = NO;
            self.guZhangBgView.hidden = YES;
            self.addphotoBgView.hidden = YES;
            self.shouHuoInfoBgView.hidden = YES;
            
            [self anZhuangXinXiBgView];
            
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH/2-179/2, self.anZhuangXinXiBgView.bottom + 42, 179, 166.5)];
            [imageV setImage:imgname(@"errorCode006")];
            [self.scrollView addSubview:imageV];
            self.imageV = imageV;
            
            UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(74, imageV.bottom + 39, KWIDTH - 84*2, 20)];
            tipsLab.text = @"此配件已安装,服役期已满";
            tipsLab.font = FontSize(13);
            tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
            tipsLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:tipsLab];
            self.tipsLab = tipsLab;
            KMyLog(@"66666666-------%@", dic[@"errorCode"]);
        }
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (void)upLoadImgs {
    
    if (strIsEmpty(self.guZhangTextView.text)) {
        ShowToastWithText(@"请输入产品故障描述");
        return;
    }
    
    UIImageView *myyy = [self.imageArr lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }
    
    if (_imageArr.count < 1) {
        ShowToastWithText(@"请添加故障产品图片");
        [self.imageArr addObject:self.myimage];
    } else if (_imageArr.count > 0 && _imageArr.count < 2) {
        ShowToastWithText(@"请至少添加两张故障产品图片");
        [self.imageArr addObject:self.myimage];
    } else {
        
        if (strIsEmpty(self.nameTF.text)) {
            ShowToastWithText(@"请输入收货人姓名");
            return;
        }
        //去除字符串中空格
        self.phoneTF.text = [self.phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![self isMobileNumberOnly:self.phoneTF.text]) {
            ShowToastWithText(@"请输入正确的收货人电话");
            return;
        }
        if (strIsEmpty(self.phoneTF.text)) {
            ShowToastWithText(@"请输入收货人电话");
            return;
        }
        if (strIsEmpty(self.diZhiTextView.text)) {
            ShowToastWithText(@"请输入收货人地址");
            return;
        }
        
        NSMutableArray *arr = [NSMutableArray array];
        //[_imageArr removeLastObject];
        for (UIImage *image in _imageArr) {
            NSMutableArray *imageArr = [NSMutableArray array];
            [imageArr addObject:image];
            [NetWorkTool UploadPicWithUrl:invoiceUploadImg param:nil key:@"imgUrl" image:imageArr withSuccess:^(id dic) {
                if ([dic[@"status"] intValue] == 0) {
                    [arr addObject:dic[@"data"]];
                }
                if (arr.count == self->_imageArr.count) {
                    NSString *faultImgStr = [arr componentsJoinedByString:@"|"];
                    [self commitRequest:faultImgStr];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }
}

-(void)commitRequest:(NSString *)faultImgStr {
    
    NSString *string = [NSString stringWithFormat:@"%@%@%@",self.detailModel.productionDate, @"至",self.detailModel.productionDate];
    NSDictionary *param = @{
        @"partsId":NOTNIL(self.detailModel.partsId),
        @"factoryId":NOTNIL(self.detailModel.factory_id),
        @"qrcode":NOTNIL(self.qrCode),
        @"warranty":NOTNIL(self.detailModel.warranty),
        //@"install":NOTNIL(self.detailModel.install),
        @"install":NOTNIL(string),
        @"freeWarranty":NOTNIL(self.detailModel.freeWarranty),
        @"compensationPrice":NOTNIL(self.detailModel.compensationPrice),
        @"installDate":NOTNIL(self.detailModel.anTime),
        @"installImg":NOTNIL(self.detailModel.imgUrl),//安装凭证url 多个以|隔开
        @"faultImg":NOTNIL(faultImgStr),//故障凭证 url 多个以|隔开
        @"faultDetails":NOTNIL(self.guZhangTextView.text),//故障描述
        @"username":NOTNIL(self.nameTF.text),
        @"userPhone":NOTNIL(self.phoneTF.text),
        @"userAddress":NOTNIL(self.diZhiTextView.text),
    };
    
    [NetWorkTool POST:applyGrantsApp param:param success:^(id dic) {
        
        //隐藏下面内容
        NSString *errorCode = dic[@"errorCode"];
        if ([errorCode isEqualToString:@"0"]) {
            
        }
        KMyLog(@"提交补助金申请 成功 ^^^^ %@", dic[@"data"]);
        
        [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecOne];
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)sureButAction:(UIButton *)btn {
    //[self upLoadImgs];
    NSLog(@"按钮点击...");
    btn.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        btn.enabled = YES;
    });
    
    [self upLoadImgs];
}

-(void)configViews {
    [self addBackTableView];
    [self addPeiJianXinXiHeaderView];
}

-(void)addPeiJianXinXiHeaderView {
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 255)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    [self.scrollView addSubview:headerView];
    self.headerView = headerView;
    
    UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 95)];
    backImageView.image = [UIImage imageNamed:@"申请补助金"];
    [headerView addSubview:backImageView];
    //配件信息 白色背景
    UIView *whiteBgView = [[UIView alloc]initWithFrame:CGRectMake(41, 20, ScreenW-41-41,225)];
    whiteBgView.backgroundColor = [UIColor whiteColor];
    whiteBgView.layer.cornerRadius = 8;
    whiteBgView.layer.masksToBounds = YES;
    [headerView addSubview:whiteBgView];
    //配件信息
    UILabel *infoWordLab = [[UILabel alloc] init];
    infoWordLab.frame = CGRectMake(0, 15, ScreenW-41-41, 14);
    infoWordLab.text = @"配件信息";
    infoWordLab.textAlignment = NSTextAlignmentCenter;
    infoWordLab.font = KFontPingFangSCRegular(14);
    infoWordLab.textColor = K333333;
    [whiteBgView addSubview:infoWordLab];
    //配件名称
    UILabel *nameWordLab = [[UILabel alloc] init];
    nameWordLab.frame = CGRectMake(16, 44, 60, 12);
    nameWordLab.textAlignment = NSTextAlignmentLeft;
    nameWordLab.text = @"配件名称：";
    nameWordLab.font = KFontPingFangSCRegular(12);
    nameWordLab.textColor = K333333;
    [whiteBgView addSubview:nameWordLab];
    
    UILabel *nameLab = [[UILabel alloc] init];
    nameLab.frame = CGRectMake(nameWordLab.right, 44, ScreenW-41-41-76, 12);
    nameLab.textAlignment = NSTextAlignmentLeft;
    nameLab.text = @"";
    nameLab.font = KFontPingFangSCRegular(12);
    nameLab.textColor = K666666;
    [whiteBgView addSubview:nameLab];
    self.nameLab = nameLab;
    //配件型号
    UILabel *xingHaoWordLab = [[UILabel alloc] init];
    xingHaoWordLab.frame = CGRectMake(16, nameWordLab.bottom+12, 60, 12);
    xingHaoWordLab.textAlignment = NSTextAlignmentLeft;
    xingHaoWordLab.text = @"配件型号：";
    xingHaoWordLab.font = KFontPingFangSCRegular(12);
    xingHaoWordLab.textColor = K333333;
    [whiteBgView addSubview:xingHaoWordLab];
    
    UILabel *xingHaoLab = [[UILabel alloc] init];
    xingHaoLab.frame = CGRectMake(xingHaoWordLab.right, nameWordLab.bottom+12, ScreenW-41-41-76, 12);
    xingHaoLab.textAlignment = NSTextAlignmentLeft;
    xingHaoLab.text = @"";
    xingHaoLab.font = KFontPingFangSCRegular(12);
    xingHaoLab.textColor = K666666;
    [whiteBgView addSubview:xingHaoLab];
    self.xingHaoLab = xingHaoLab;
    
    //工时费补助金：
    UILabel *gongShiBuZhuJinWordLab = [[UILabel alloc] init];
    gongShiBuZhuJinWordLab.frame = CGRectMake(16, xingHaoWordLab.bottom+12, 7*12, 12);
    gongShiBuZhuJinWordLab.textAlignment = NSTextAlignmentLeft;
    gongShiBuZhuJinWordLab.text = @"工时费补助金：";
    gongShiBuZhuJinWordLab.font = KFontPingFangSCRegular(12);
    gongShiBuZhuJinWordLab.textColor = K333333;
    [whiteBgView addSubview:gongShiBuZhuJinWordLab];
    
    UILabel *gongShiBuZhuJinLab = [[UILabel alloc] initWithFrame: CGRectMake(gongShiBuZhuJinWordLab.right, xingHaoWordLab.bottom+12, ScreenW-16-7*12, 12)];
    gongShiBuZhuJinLab.textAlignment = NSTextAlignmentLeft;
    gongShiBuZhuJinLab.text = @"";
    gongShiBuZhuJinLab.font = KFontPingFangSCRegular(12);
    gongShiBuZhuJinLab.textColor = K666666;
    [whiteBgView addSubview:gongShiBuZhuJinLab];
    self.gongShiBuZhuJinLab = gongShiBuZhuJinLab;
    
    // 候保二维码有效期：
    UILabel *erWeiMaYouXiaoQiWordLab = [[UILabel alloc] init];
    erWeiMaYouXiaoQiWordLab.frame = CGRectMake(16, gongShiBuZhuJinWordLab.bottom+12, 9*12, 12);
    erWeiMaYouXiaoQiWordLab.textAlignment = NSTextAlignmentLeft;
    erWeiMaYouXiaoQiWordLab.text = @"候保二维码有效期：";
    erWeiMaYouXiaoQiWordLab.font = KFontPingFangSCRegular(12);
    erWeiMaYouXiaoQiWordLab.textColor = K333333;
    [whiteBgView addSubview:erWeiMaYouXiaoQiWordLab];
    
    UILabel *erWeiMaYouXiaoQiLab = [[UILabel alloc] init];
    erWeiMaYouXiaoQiLab.frame = CGRectMake(erWeiMaYouXiaoQiWordLab.right, gongShiBuZhuJinWordLab.bottom+12, ScreenW-41-41-76, 12);
    erWeiMaYouXiaoQiLab.textAlignment = NSTextAlignmentLeft;
    erWeiMaYouXiaoQiLab.text = @"";
    erWeiMaYouXiaoQiLab.font = KFontPingFangSCRegular(12);
    erWeiMaYouXiaoQiLab.textColor = K666666;
    [whiteBgView addSubview:erWeiMaYouXiaoQiLab];
    self.erWeiMaYouXiaoQiLab = erWeiMaYouXiaoQiLab;
    
    // 无忧质保期：
    UILabel *wuYouZhiBaoQiWordLab = [[UILabel alloc] init];
    wuYouZhiBaoQiWordLab.frame = CGRectMake(16, erWeiMaYouXiaoQiWordLab.bottom+12, 12*6, 12);
    wuYouZhiBaoQiWordLab.textAlignment = NSTextAlignmentLeft;
    wuYouZhiBaoQiWordLab.text = @"无忧质保期：";
    wuYouZhiBaoQiWordLab.font = KFontPingFangSCRegular(12);
    wuYouZhiBaoQiWordLab.textColor = K333333;
    [whiteBgView addSubview:wuYouZhiBaoQiWordLab];
    
    UILabel *wuYouZhiBaoQiLab = [[UILabel alloc] init];
    wuYouZhiBaoQiLab.frame = CGRectMake(wuYouZhiBaoQiWordLab.right, erWeiMaYouXiaoQiWordLab.bottom+12, ScreenW-41-41-76, 12);
    wuYouZhiBaoQiLab.textAlignment = NSTextAlignmentLeft;
    wuYouZhiBaoQiLab.text = @"";
    wuYouZhiBaoQiLab.font = KFontPingFangSCRegular(12);
    wuYouZhiBaoQiLab.textColor = K666666;
    [whiteBgView addSubview:wuYouZhiBaoQiLab];
    self.wuYouZhiBaoQiLab = wuYouZhiBaoQiLab;
    
    // 候保工时费补贴期：3个月
    UILabel *buTieQiLab = [[UILabel alloc] init];
    buTieQiLab.frame = CGRectMake(16, wuYouZhiBaoQiWordLab.bottom+12, ScreenW-41-41-16, 12);
    buTieQiLab.textAlignment = NSTextAlignmentLeft;
    buTieQiLab.text = @"候保工时费补贴期：3个月";
    buTieQiLab.font = KFontPingFangSCRegular(12);
    buTieQiLab.textColor =  [UIColor colorWithHexString:@"#F6A44B"];
    [whiteBgView addSubview:buTieQiLab];
    self.buTieQiLab = buTieQiLab;
    
    //安装日期：
    UILabel *anZhuangRiQiWordLab = [[UILabel alloc] init];
    anZhuangRiQiWordLab.frame = CGRectMake(16, buTieQiLab.bottom+12, 12*5, 12);
    anZhuangRiQiWordLab.textAlignment = NSTextAlignmentLeft;
    anZhuangRiQiWordLab.text = @"安装日期：";
    anZhuangRiQiWordLab.font = KFontPingFangSCRegular(12);
    anZhuangRiQiWordLab.textColor = K333333;
    [whiteBgView addSubview:anZhuangRiQiWordLab];
    
    UILabel *anZhuangRiQiLab = [[UILabel alloc] init];
    anZhuangRiQiLab.frame = CGRectMake(anZhuangRiQiWordLab.right, buTieQiLab.bottom+12, ScreenW-41-41-76, 12);
    anZhuangRiQiLab.textAlignment = NSTextAlignmentLeft;
    anZhuangRiQiLab.text = @"";
    anZhuangRiQiLab.font = KFontPingFangSCRegular(12);
    anZhuangRiQiLab.textColor = K666666;
    [whiteBgView addSubview:anZhuangRiQiLab];
    self.anZhuangRiQiLab = anZhuangRiQiLab;
}

//产品安装信息
-(void)addAnZhuangXinXiView {
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.headerView.bottom+10, ScreenW, 260)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:bgView];
    self.anZhuangXinXiBgView = bgView;
    
    //产品安装信息 文字
    UIImageView *infoImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 11, 4, 15)];
    infoImage.image = [UIImage imageNamed:@"矩形 704"];
    //infoImage.backgroundColor = [UIColor cyanColor];
    [bgView addSubview:infoImage];
    
    UILabel *infoLab = [[UILabel alloc]initWithFrame:CGRectMake(36, 12, 100, 14)];
    infoLab.font = FontSize(14);
    infoLab.textColor = [UIColor colorWithHexString:@"#333333"];
    infoLab.text = @"产品安装信息";
    [bgView addSubview:infoLab];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(20, infoLab.bottom+11, ScreenW, 0.5)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [bgView addSubview:lineView];
    
    UIButton *zhanKaiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    zhanKaiBtn.frame = CGRectMake(ScreenW-20-42, 0, 42, 42);
    [zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_down_icon"] forState:UIControlStateNormal];
    [zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:zhanKaiBtn];
   
    //姓名
    UILabel *xingMingLab = [[UILabel alloc] init];
    xingMingLab.frame = CGRectMake(37, lineView.bottom+17, 60, 12);
    xingMingLab.textAlignment = NSTextAlignmentLeft;
    xingMingLab.text = self.detailModel.user_name;
    xingMingLab.font = KFontPingFangSCRegular(12);
    xingMingLab.textColor = K333333;
    [bgView addSubview:xingMingLab];
    self.xingMingLab = xingMingLab;
    //电话
    UILabel *phoneLab = [[UILabel alloc] init];
    phoneLab.frame = CGRectMake(xingMingLab.right, lineView.bottom+17, ScreenW-37-60-5, 12);
    phoneLab.textAlignment = NSTextAlignmentLeft;
    phoneLab.text = self.detailModel.user_phone;
    phoneLab.font = KFontPingFangSCRegular(12);
    phoneLab.textColor = K666666;
    [bgView addSubview:phoneLab];
    self.phoneLab = phoneLab;
    //地址
    UILabel *diZhiWordLab = [[UILabel alloc] init];
    diZhiWordLab.frame = CGRectMake(37, xingMingLab.bottom+12, 12*3, 12);
    diZhiWordLab.textAlignment = NSTextAlignmentLeft;
    diZhiWordLab.text = @"地址：";
    diZhiWordLab.font = KFontPingFangSCRegular(12);
    diZhiWordLab.textColor = K333333;
    [bgView addSubview:diZhiWordLab];
    self.diZhiWordLab = diZhiWordLab;
    
    //地址 内容
    CGFloat height2 = [NSString heightWithWidth:KWIDTH - 84 font:12 text:self.detailModel.user_address];
    UILabel *diZhiLab = [[UILabel alloc] initWithFrame:CGRectMake(71, xingMingLab.bottom+12, KWIDTH-84, height2)];
    diZhiLab.text = self.detailModel.user_address;
    diZhiLab.font = FontSize(12);
    diZhiLab.numberOfLines = 0;
    diZhiLab.textColor = K666666;
    [bgView addSubview:diZhiLab];
    self.diZhiLab = diZhiLab;
    //费用
    UILabel *feiYongWordLab = [[UILabel alloc] init];
    feiYongWordLab.frame = CGRectMake(37, diZhiLab.bottom+12, 12*3, 12);
    feiYongWordLab.textAlignment = NSTextAlignmentLeft;
    feiYongWordLab.text = @"费用：";
    feiYongWordLab.font = KFontPingFangSCRegular(12);
    feiYongWordLab.textColor = K333333;
    [bgView addSubview:feiYongWordLab];
    self.feiYongWordLab = feiYongWordLab;
    
    UILabel *feiYongLab = [[UILabel alloc] init];
    feiYongLab.frame = CGRectMake(feiYongWordLab.right, diZhiLab.bottom+12, (ScreenW-71-13), 12);
    feiYongLab.textAlignment = NSTextAlignmentLeft;
    feiYongLab.text = [NSString stringWithFormat:@"%@%@",@"¥",self.detailModel.cost];
    feiYongLab.font = KFontPingFangSCRegular(12);
    feiYongLab.textColor = K666666;
    [bgView addSubview:feiYongLab];
    self.feiYongLab = feiYongLab;
    //初始安装完工照片
    UILabel *zhaoPianWordLab = [[UILabel alloc] init];
    zhaoPianWordLab.frame = CGRectMake(37, feiYongWordLab.bottom+12, 150, 12);
    zhaoPianWordLab.textAlignment = NSTextAlignmentLeft;
    zhaoPianWordLab.text = @"初始安装完工照片";
    zhaoPianWordLab.font = KFontPingFangSCRegular(12);
    zhaoPianWordLab.textColor = K333333;
    [bgView addSubview:zhaoPianWordLab];
    self.zhaoPianWordLab = zhaoPianWordLab;
    
    /*NSString *imageStr = self.detailModel.imgUrl;
    NSArray *imageList = [imageStr componentsSeparatedByString:@"|"];
    for (int i = 0; i < imageList.count; i ++) {
        UIImageView * button = [[UIImageView alloc]init];
        [button setImage:[UIImage imageNamed:imageList[i]]];
        //下面是九宫格的设计
        NSInteger l = i % 3;
        NSInteger h = i / 3;
        NSInteger leftMagin = (ScreenW-80-58*3)/3;
        NSInteger kMagin = 14;
        CGFloat width = 58;
        CGFloat height = 58;
        //通过个数位置确定具体的frame
        button.frame = CGRectMake(leftMagin + (leftMagin + width) * l,zhaoPianWordLab.bottom+14 + (kMagin + height) * h, width, height);
        [button setImage:[UIImage imageNamed:imageList[i]]];
        SYBigImage * bigI = [[SYBigImage alloc]init];
        [button addGestureRecognizer:bigI];
        button.userInteractionEnabled = YES;
        [bgView addSubview:button];
        if (i== imageList.count-1) {
            CGFloat height = button.height+button.frame.origin.y+10;
            self.anZhuangXinXiBgView.frame = CGRectMake(0, self.headerView.bottom+10, ScreenW, height);
        }
    }*/
    
    //照片背景 View
    UIView *imageBackView = [[UIView alloc] init];
    imageBackView.frame = CGRectMake(32.5, zhaoPianWordLab.bottom + 5, KWIDTH-65, 75);
    [bgView addSubview:imageBackView];
    self.imageBackView = imageBackView;

    NSString *imagesStr = self.detailModel.imgUrl;
    NSArray *imagearr = [imagesStr componentsSeparatedByString:@"|"];
    CGFloat wei = (KWIDTH-25-46-46)/4;

    if (imagearr.count > 0) {
        [self.imageBackView removeAllSubviews];
        for (int i = 0; i < imagearr.count; i++) {
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.imageBackView addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:imagearr[i]] placeholderImage:defaultImg];
            myimage.frame = CGRectMake((wei+10)*(i % 4)+10, (wei + 10) * (i / 4)+5, wei, wei);
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
            self.imageBackView.frame = CGRectMake(32.5, zhaoPianWordLab.bottom + 10, bgView.width - 65, (wei + 10) * (i / 4 + 1));
        }
    }
}

-(void)zhanKaiBtnClick:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        
        self.xingMingLab.hidden = YES;
        self.diZhiWordLab.hidden= YES;
        self.diZhiLab.hidden= YES;
        self.feiYongWordLab.hidden= YES;
        self.feiYongLab.hidden= YES;
        self.zhaoPianWordLab.hidden= YES;
        self.imageBackView.hidden= YES;
        
        self.anZhuangXinXiBgView.height = 37;
        self.guZhangBgView.top = self.anZhuangXinXiBgView.bottom+10;
        self.addphotoBgView.top = self.guZhangBgView.bottom+10;
        self.shouHuoInfoBgView.top = self.addphotoBgView.bottom+10;
        [sender setImage:[UIImage imageNamed:@"arrow_up_icon"] forState:UIControlStateNormal];
    } else {
        self.xingMingLab.hidden = NO;
        self.diZhiWordLab.hidden= NO;
        self.diZhiLab.hidden= NO;
        self.feiYongWordLab.hidden= NO;
        self.feiYongLab.hidden= NO;
        self.zhaoPianWordLab.hidden= NO;
        self.imageBackView.hidden= NO;
        
        self.anZhuangXinXiBgView.height = 260;
        self.guZhangBgView.top = self.anZhuangXinXiBgView.bottom+10;
        self.addphotoBgView.top = self.guZhangBgView.bottom+10;
        self.shouHuoInfoBgView.top = self.addphotoBgView.bottom+10;
        [sender setImage:[UIImage imageNamed:@"arrow_down_icon"] forState:UIControlStateNormal];
    }
}

-(void)addGuZhangMiaoShuView {
    
    UIView *guZhangBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.anZhuangXinXiBgView.bottom+10, ScreenW, 130)];
    guZhangBgView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:guZhangBgView];
    self.guZhangBgView = guZhangBgView;
    
    //申请补助金金额:
    UIImageView *buZhuJinImg = [[UIImageView alloc]initWithFrame:CGRectMake(19, 15, 14, 14)];
    buZhuJinImg.image = [UIImage imageNamed:@"钱 (1)"];
    [guZhangBgView addSubview:buZhuJinImg];
    
    UILabel *buZhuJinEWordLab = [[UILabel alloc] init];
    buZhuJinEWordLab.frame = CGRectMake(43, 0, 12*8, 41);
    buZhuJinEWordLab.textAlignment = NSTextAlignmentLeft;
    buZhuJinEWordLab.text = @"申请补助金金额:";
    buZhuJinEWordLab.font = KFontPingFangSCRegular(12);
    buZhuJinEWordLab.textColor = K333333;
    [guZhangBgView addSubview:buZhuJinEWordLab];
    
    UILabel *buZhuJinELab = [[UILabel alloc] init];
    buZhuJinELab.frame = CGRectMake(buZhuJinEWordLab.right, 0, 200, 41);
    buZhuJinELab.textAlignment = NSTextAlignmentLeft;
    buZhuJinELab.text = [NSString stringWithFormat:@"¥%@",self.detailModel.compensationPrice];
    buZhuJinELab.font = KFontPingFangSCRegular(12);
    buZhuJinELab.textColor = K666666;
    [guZhangBgView addSubview:buZhuJinELab];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(20, buZhuJinELab.bottom, ScreenW-40, 0.5)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [guZhangBgView addSubview:lineView];
    
    //产品故障描述：
    UIImageView *guZhangImg = [[UIImageView alloc]initWithFrame:CGRectMake(19, lineView.bottom+10, 14, 14)];
    guZhangImg.image = [UIImage imageNamed:@"故障3"];
    [guZhangBgView addSubview:guZhangImg];
    
    UILabel *guZhangWordLab = [[UILabel alloc] init];
    guZhangWordLab.frame = CGRectMake(guZhangImg.right+9, lineView.bottom+10, 150, 12);
    guZhangWordLab.textAlignment = NSTextAlignmentLeft;
    guZhangWordLab.text = @"产品故障描述：";
    guZhangWordLab.font = KFontPingFangSCRegular(12);
    guZhangWordLab.textColor = K333333;
    [guZhangBgView addSubview:guZhangWordLab];
    
    //self.guZhangTextView = [[IQTextView alloc]init];
    self.guZhangTextView = [[UITextField alloc]init];
    self.guZhangTextView.frame = CGRectMake(44, guZhangWordLab.bottom+10, ScreenW-44-10, 30);
    self.guZhangTextView.placeholder = @"请输入产品故障描述";
    self.guZhangTextView.font = KFontPingFangSCRegular(12);
    self.guZhangTextView.textColor = K999999;
    self.guZhangTextView.delegate = self;
    self.guZhangTextView.textColor = K666666;
    self.guZhangTextView.layer.cornerRadius = 4;
    self.guZhangTextView.layer.borderWidth = 0.5;
    self.guZhangTextView.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    [guZhangBgView addSubview:self.guZhangTextView];
}

-(void)addShangChuanGuZhangPhotoView {
    
    UIView *addphotoBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.guZhangBgView.bottom+10, ScreenW, 137)];
    addphotoBgView.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:addphotoBgView];
    self.addphotoBgView =addphotoBgView;
    
    //  上传故障产品照片
    UIImageView *xiangJiImg = [[UIImageView alloc]initWithFrame:CGRectMake(19, 15, 14, 14)];
    xiangJiImg.image = [UIImage imageNamed:@"相机添加 (1)"];
    [addphotoBgView addSubview:xiangJiImg];
    
    UILabel *photoWordLab = [[UILabel alloc] init];
    photoWordLab.frame = CGRectMake(43,5, ScreenW-43-15, 40);
    photoWordLab.textAlignment = NSTextAlignmentLeft;
    photoWordLab.text = @"上传故障产品照片(请上传产品上候保二维码和产品全景图片)";
    photoWordLab.font = KFontPingFangSCRegular(12);
    photoWordLab.textColor = K333333;
    photoWordLab.numberOfLines = 0;
    [addphotoBgView addSubview:photoWordLab];
    
    UIView *shangChuanBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 45, ScreenW, 90)];
    shangChuanBgView.backgroundColor = [UIColor whiteColor];
    [addphotoBgView addSubview:shangChuanBgView];
    self.shangChuanBgView = shangChuanBgView;
    
    self.myimage = [[UIImageView alloc]init];
    self->_myimage.image = imgname(@"相机");
    self->_myimage.tag = 10001;
    [self.imageArr addObject:self->_myimage];
    [self showinageWirhArr:self->_imageArr];
}

-(void)addShuoHuoXinXiView {
    //收货信息 白色背景
    UIView *whiteBgView = [[UIView alloc]initWithFrame:CGRectMake(0, self.addphotoBgView.bottom+10, ScreenW,210+100)];
    whiteBgView.backgroundColor = [UIColor whiteColor];
    whiteBgView.layer.cornerRadius = 8;
    whiteBgView.layer.masksToBounds = YES;
    [self.scrollView addSubview:whiteBgView];
    self.shouHuoInfoBgView = whiteBgView;
    
    //收货人姓名：
    UIImageView *xiangJiImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 14, 14)];
    xiangJiImg.image = [UIImage imageNamed:@"个人 (11)"];
    [whiteBgView addSubview:xiangJiImg];
    
    UILabel *nameWordLab = [[UILabel alloc] init];
    nameWordLab.frame = CGRectMake(43, 0, 12*6, 42);
    nameWordLab.textAlignment = NSTextAlignmentLeft;
    nameWordLab.text = @"收货人姓名：";
    nameWordLab.font = KFontPingFangSCRegular(12);
    nameWordLab.textColor = K333333;
    [whiteBgView addSubview:nameWordLab];
    
    UITextField *nameTF = [[UITextField alloc] init];
    nameTF.frame = CGRectMake(nameWordLab.right, 0, ScreenW-43-68, 42);
    nameTF.textAlignment = NSTextAlignmentLeft;
    nameTF.placeholder = @"请输入收货人姓名";
    nameTF.font = KFontPingFangSCRegular(12);
    nameTF.textColor = K666666;
    [whiteBgView addSubview:nameTF];
    self.nameTF = nameTF;
    
    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(20, 42, ScreenW-40, 0.5)];
    lineView1.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [whiteBgView addSubview:lineView1];
    
    //收货人电话：
    UIImageView *phoneImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, lineView1.bottom+12, 14, 14)];
    phoneImg.image = [UIImage imageNamed:@"shenqingbuzhujinImg"];
    [whiteBgView addSubview:phoneImg];
    
    UILabel *phoneWordLab = [[UILabel alloc] init];
    phoneWordLab.frame = CGRectMake(43, lineView1.bottom, 12*6, 42);
    phoneWordLab.textAlignment = NSTextAlignmentLeft;
    phoneWordLab.text = @"收货人电话：";
    phoneWordLab.font = KFontPingFangSCRegular(12);
    phoneWordLab.textColor = K333333;
    [whiteBgView addSubview:phoneWordLab];
    
    UITextField *phoneTF = [[UITextField alloc] init];
    phoneTF.frame = CGRectMake(phoneWordLab.right, lineView1.bottom, ScreenW-43-68, 42);
    phoneTF.textAlignment = NSTextAlignmentLeft;
    phoneTF.placeholder = @"请输入收货人电话";
    phoneTF.font = KFontPingFangSCRegular(12);
    phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    phoneTF.textColor = K666666;
    [whiteBgView addSubview:phoneTF];
    self.phoneTF = phoneTF;
    
    UIView *lineView2 = [[UIView alloc]initWithFrame:CGRectMake(20, phoneWordLab.bottom, ScreenW-40, 0.5)];
    lineView2.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [whiteBgView addSubview:lineView2];
    
    // 收货地址：
    UIImageView *diZhiImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, lineView2.bottom+12, 14, 14)];
    diZhiImg.image = [UIImage imageNamed:@"addressImg"];
    [whiteBgView addSubview:diZhiImg];
    
    UILabel *diZhiWordLab = [[UILabel alloc] init];
    diZhiWordLab.frame = CGRectMake(43, lineView2.bottom+13, 68, 12);
    diZhiWordLab.textAlignment = NSTextAlignmentLeft;
    diZhiWordLab.text = @"收货地址：";
    diZhiWordLab.font = KFontPingFangSCRegular(12);
    diZhiWordLab.textColor = K333333;
    [whiteBgView addSubview:diZhiWordLab];
    
    //IQTextView *diZhiTextView = [[IQTextView alloc] init];
    UITextView *diZhiTextView = [[UITextView alloc] init];
    diZhiTextView.frame = CGRectMake(43, diZhiWordLab.bottom+16, ScreenW-44-10, 40);
    diZhiTextView.textAlignment = NSTextAlignmentLeft;
    //diZhiTextView.placeholder = @"请输入收货地址";
    diZhiTextView.font = KFontPingFangSCRegular(12);
    diZhiTextView.textColor = K666666;
    diZhiTextView.delegate = self;
    diZhiTextView.layer.cornerRadius = 4;
    diZhiTextView.layer.borderWidth = 0.5;
    diZhiTextView.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    [whiteBgView addSubview:diZhiTextView];
    self.diZhiTextView = diZhiTextView;
    
    //占位 Label
    UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
    placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    placeLab.text = @"请输入收货地址";
    placeLab.font = FontSize(14);
    self.placeLab = placeLab;
    [self.diZhiTextView addSubview:placeLab];
    
    //注：请提供接收新配件所需地址
    UILabel *beiZhuWordLab = [[UILabel alloc] init];
    beiZhuWordLab.frame = CGRectMake(ScreenW-20-200, diZhiTextView.bottom+14, 200, 10);
    beiZhuWordLab.textAlignment = NSTextAlignmentRight;
    beiZhuWordLab.text = @"注：请提供接收新配件所需地址";
    beiZhuWordLab.font = KFontPingFangSCRegular(10);
    beiZhuWordLab.textColor = [UIColor colorWithHexString:@"#FF0000"];
    [whiteBgView addSubview:beiZhuWordLab];
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, beiZhuWordLab.bottom+20, KWIDTH, 150)];
    footerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    [whiteBgView addSubview:footerView];
    
    UIButton *sureBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [sureBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    sureBut.frame  = CGRectMake((KWIDTH-150)/2, 24, 150, 36);
    [sureBut setBackgroundImage:imgname(@"按钮背景") forState:UIControlStateNormal];
    
    sureBut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    [sureBut setTitle:@"提交" forState:UIControlStateNormal];
    [sureBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBut.layer.masksToBounds = YES;
    sureBut.layer.cornerRadius = 18;
    [sureBut addTarget:self action:@selector(sureButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [footerView addSubview: sureBut];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if (self.diZhiTextView == textView) {
        self.placeLab.hidden = YES;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.diZhiTextView == textView) {
        if (textView.text.length < 1) {
            self.placeLab.hidden = NO;
        }
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    KMyLog(@"%@", textView.text);
    
    if (self.diZhiTextView == textView) {
        self.placeLab.hidden = YES;
        //实时显示字数
        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
        //字数限制操作
        if (textView.text.length >= 60) {
            textView.text = [textView.text substringToIndex:60];
            self.stirngLenghLabel.text = @"60/60";
        }
        //取消按钮点击权限，并显示提示文字
        if (textView.text.length == 0) {
            self.placeLab.hidden = NO;
        }
    }
}

- (void)alreadyInstallView {
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH/2-179/2, self.headerView.bottom + 42, 179, 166.5)];
    [self.scrollView addSubview:imageV];
    self.imageV = imageV;
    
    UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(74, imageV.bottom + 39, KWIDTH - 84*2, 20)];
    tipsLab.font = FontSize(13);
    tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
    tipsLab.textAlignment = NSTextAlignmentCenter;
    [self.scrollView addSubview:tipsLab];
    self.tipsLab = tipsLab;
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    
    [self.shangChuanBgView removeAllSubviews];
    CGFloat www = (KWIDTH -43-43)/4;
    
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(43+(www+10)* (i % 4),  (www + 10) * (i / 4) +5, www, www);
        
        [self.shangChuanBgView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if ([self.accessAddModel.deleButHidden isEqualToString:@"1"]) {
            deleBut.hidden = YES;
        } else {
            deleBut.hidden = NO;
        }
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 4) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            }else{
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        }else{
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
    _showData = NO;
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind] removeFromSuperview];
    NSLog(@"删除%ld图片", ind);
    [_imageArr removeObjectAtIndex:ind];
    [self showinageWirhArr:_imageArr];
    if (self.imageArrayChangeBlock) {
        self.imageArrayChangeBlock(_imageArr);
    }
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    [self.addImageArr addObject:image];//新加的 放入提交接口上送参数中
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)addBackTableView{
    [self.view addSubview:self.scrollView];
}

-(void)leftBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 弹出框的背景图 厂家订单 推送成功
 */
-(void)shwoBgviewsec{
    self.bgViewsecOne = [[UIView alloc]init];
    self.bgViewsecOne.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsecOne.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor clearColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecOne addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(320);
        make.left.offset(30);
        make.right.offset(-30);
    }];
    
    UIImageView *backImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH-60, 330)];
    [backImgView setImage:imgname(@"popupBackImg")];
    backImgView.userInteractionEnabled = YES;
    [whiteBGView addSubview:backImgView];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(20, 90, KWIDTH-60-40, 21)];
    [backImgView addSubview:UpLable];
    UpLable.font = FontSize(13);
    UpLable.textColor = [UIColor colorWithHexString:@"#19950C"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"工时费补助金申请";
    
    UILabel *UpTwoLable = [[UILabel alloc]initWithFrame:CGRectMake(20, UpLable.bottom, KWIDTH-60-40, 21)];
    [backImgView addSubview:UpTwoLable];
    UpTwoLable.font = FontSize(16);
    UpTwoLable.textColor = [UIColor colorWithHexString:@"#19950C"];
    UpTwoLable.textAlignment = NSTextAlignmentCenter;
    UpTwoLable.text = @"提交成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(20, UpTwoLable.bottom+10, KWIDTH-60-40, 63)];
    [backImgView addSubview:DowLable];
    DowLable.font = FontSize(13);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"很抱歉产品给您带来麻烦，您可以先解决好客\n户的问题，我们将在24小时内审核，给出申请\n补助回复。";
    DowLable.numberOfLines = 0;
    
    UILabel *DowTwoLable = [[UILabel alloc]initWithFrame:CGRectMake(20, DowLable.bottom+10, KWIDTH-60-40, 20)];
    [backImgView addSubview:DowTwoLable];
    DowTwoLable.font = FontSize(14);
    DowTwoLable.textColor = [UIColor colorWithHexString:@"#70BE68"];
    DowTwoLable.textAlignment = NSTextAlignmentCenter;
    DowTwoLable.text = @"正在审核中请耐心等待......";
    DowTwoLable.textAlignment = NSTextAlignmentCenter;
    DowTwoLable.numberOfLines = 0;
    
    UIButton *centerBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    centerBut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    [centerBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [centerBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    centerBut.titleLabel.textColor = K999999;
    centerBut.layer.masksToBounds = YES;
    centerBut.layer.cornerRadius = 17;
    [centerBut addTarget:self action:@selector(centerButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [backImgView addSubview:centerBut];
    [centerBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.left.offset(95);
        make.right.offset(-95);
        make.bottom.offset(-28);
    }];
}

-(void)centerButAction:(UIButton *)but{
    [_bgViewsecOne removeFromSuperview];
    [singlTool shareSingTool].needReasfh = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
        _scrollView.contentSize = CGSizeMake(0, KHEIGHT+420);
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end

