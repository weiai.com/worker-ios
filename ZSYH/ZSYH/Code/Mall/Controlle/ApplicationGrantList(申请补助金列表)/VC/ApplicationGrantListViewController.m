//
//  ApplicationGrantListViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "ApplicationGrantListViewController.h"
#import "ApplicationGrantListTableViewCell.h"
#import "ApplicationGrantListModel.h"
#import "OrderDetailFinishedViewController.h"
#import "OrderDetailDaiShenHeViewController.h"

@interface ApplicationGrantListViewController ()<UITableViewDelegate, UITableViewDataSource> {
    BOOL _isDelete;
}
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *listArray;

@end

@implementation ApplicationGrantListViewController

#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    if ([singlTool shareSingTool].needReasfh) {
        [self.myTableView.mj_header beginRefreshing];
        [singlTool shareSingTool].needReasfh = NO;
    }
    
    [self requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"申请补助金列表";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.page = 1;
    
    [self.view addSubview:self.myTableView];
    // Do any additional setup after loading the view.
}

-(void)requestData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    NSString *pagg = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNumber"] = pagg;
    param[@"pageSize"] = @"10";
    
    if (_page == 1) {
        [weakSelf.mydateSource removeAllObjects];
        [self.listArray removeAllObjects];
    }
    [NetWorkTool POST:getApplyGrantsListApp param:param success:^(id dic) {
        NSLog(@"获取补助金申请列表所有数据 %@", dic);
        
        self.listArray = [ApplicationGrantListModel mj_objectArrayWithKeyValuesArray:[dic[@"data"]objectForKey:@"rows"]];
        [self.mydateSource addObjectsFromArray:self.listArray];
        
        [self.myTableView reloadData];
        
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 195;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",(long)indexPath.section, (long)indexPath.row];
    //通过唯一标识创建Cell实例
    ApplicationGrantListTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ApplicationGrantListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    //mycell.backgroundColor = [UIColor clearColor];
    ApplicationGrantListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    [mycell refasf:model];
    
        mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
            switch (ind) {
                case 0: {
                    //生成检测报告按钮点击事件
                    [HFTools callMobilePhone:@"400-961-5811"];
                }
                    break;
                default:
                    break;
            }
        };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ApplicationGrantListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    //状态 -1申请失败 0,1申请中， 2申请成功
    if ([model.status isEqualToString:@"0"]||[model.status isEqualToString:@"1"]) {
        OrderDetailDaiShenHeViewController *vc = [[OrderDetailDaiShenHeViewController alloc] init];
        vc.idStr = model.id;
        vc.statusStr = model.status;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        OrderDetailFinishedViewController *vc = [[OrderDetailFinishedViewController alloc] init];
        vc.idStr = model.id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        
        //if ([self.from isEqualToString:@"tabbar"]) {
        //    _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49-kTabbarHeight) style:(UITableViewStyleGrouped)];
        //}else {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, KNavHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStyleGrouped)];
        //}
        
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.tableFooterView = [UIView new];
        adjustInset(_myTableView);
        [_myTableView registerClass:[ApplicationGrantListTableViewCell class] forCellReuseIdentifier:@"ApplicationGrantListTableViewCell"];
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
          
            self.page = 1;
            [self requestData];//请求数据
        }];
        self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            self.page += 1;
            [self requestData];//请求数据
        }];
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
