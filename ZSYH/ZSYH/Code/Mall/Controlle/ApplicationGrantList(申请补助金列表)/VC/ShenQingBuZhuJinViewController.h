//
//  ShenQingBuZhuJinViewController.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "SaoMaPeiJianDetailModel.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ShenQingBuZhuJinViewController : BaseViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;
@property (nonatomic, strong) SaoMaPeiJianDetailModel *detailModel;
@property (nonatomic, strong) NSString *qrCode;

@property (nonatomic, copy) void(^imageArrayChangeBlock)(NSArray *imageArray);

@end

NS_ASSUME_NONNULL_END
