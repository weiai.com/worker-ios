//
//  OrderDetailDaiShenHeViewController.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailDaiShenHeViewController : BaseViewController
@property(nonatomic,strong)NSString *idStr;
@property(nonatomic,strong)NSString *statusStr;

@end

NS_ASSUME_NONNULL_END
