//
//  OrderDetailFinishedViewController.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailFinishedViewController : BaseViewController
@property(nonatomic,strong)NSString *idStr;

@end

NS_ASSUME_NONNULL_END
