//
//  OrderDetailFinishedViewController.m
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//
#import "OrderDetailFinishedViewController.h"
#import "BuZhuJinZheDieCell.h"
#import "KeHuShouHuoDiZhiCell.h"
#import "BoHuiReasonCell.h"

#import "BuZhuJinPeiJianInfoCell.h"
#import "ChanPinAnZhuangInfoCell.h"
#import "ShenQingBuZhuJinInfoCell.h"
#import "GrantDetailContentModel.h"
#import "GrantDetailTopDetailsListModel.h"
@interface OrderDetailFinishedViewController ()<UITableViewDelegate,UITableViewDataSource>
//列表
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) GrantDetailContentModel *detailModel;
@property (nonatomic,strong) NSMutableArray *detailsListArray;

@property (nonatomic,strong)UILabel *timelab;
@property (nonatomic,assign)BOOL peiJianInfoCellSelected;
@property (nonatomic,assign)BOOL anZhuangInfoCellSelected;
@property (nonatomic,assign)BOOL shenQingInfoCellSelected;

@property (nonatomic,assign)CGFloat peiJianCellHeight;
@property (nonatomic,assign)CGFloat anZhuangXinXiCellHeight;
@property (nonatomic,assign)CGFloat buZhuXinXiCellHeight;

@property (nonatomic,assign)CGFloat topLabY;

@property (nonatomic,assign)BOOL boHuiCellShow;
@property (nonatomic,strong) NSString *boHuiYuanYinStr;
@end

static NSString *const KeHuShouHuoDiZhiCellID = @"KeHuShouHuoDiZhiCell";
static NSString *const BoHuiReasonCellID = @"BoHuiReasonCell";

static NSString *const BuZhuJinZheDieCellID = @"BuZhuJinZheDieCell";

static NSString *const BuZhuJinPeiJianInfoCellID = @"BuZhuJinPeiJianInfoCell";
static NSString *const ChanPinAnZhuangInfoCellID = @"ChanPinAnZhuangInfoCell";
static NSString *const ShenQingBuZhuJinInfoCellID = @"ShenQingBuZhuJinInfoCell";

@implementation OrderDetailFinishedViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    
    [self requestGrantsDetail];

}

-(void)requestGrantsDetail
{
    NSDictionary *param = @{
        @"id":NOTNIL(self.idStr)
    };
    [NetWorkTool POST:repairApplyGrantsGetApplyGrantsById param:param success:^(id dic) {
        self.detailModel = [GrantDetailContentModel mj_objectWithKeyValues:dic[@"data"]];
        
        self.detailsListArray = [GrantDetailTopDetailsListModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"detailsList"]];
        self.detailModel = [GrantDetailContentModel mj_objectWithKeyValues:dic[@"data"][@"repairApplyGrants"]];
        
        [self configViews];
        [self.listTableView reloadData];
        KMyLog(@"已完成 订单详情 成功与否 ^^^ %@", dic[@"data"]);
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}
-(void)configViews
{
    [self addBackTableView];
}

-(void)addBackTableView
{
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([BoHuiReasonCell class]) bundle:nil] forCellReuseIdentifier:BoHuiReasonCellID];
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([KeHuShouHuoDiZhiCell class]) bundle:nil] forCellReuseIdentifier:KeHuShouHuoDiZhiCellID];
    
    [self.listTableView registerClass:[BuZhuJinZheDieCell class] forCellReuseIdentifier:BuZhuJinZheDieCellID];
    
    [self.listTableView registerClass:[BuZhuJinPeiJianInfoCell class] forCellReuseIdentifier:BuZhuJinPeiJianInfoCellID];
    [self.listTableView registerClass:[ChanPinAnZhuangInfoCell class] forCellReuseIdentifier:ChanPinAnZhuangInfoCellID];
    [self.listTableView registerClass:[ShenQingBuZhuJinInfoCell class] forCellReuseIdentifier:ShenQingBuZhuJinInfoCellID];
    [self.view addSubview:self.listTableView];
    [self addHeaderView];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-TabbarSafeBottomMargin) style:UITableViewStyleGrouped];
        _listTableView.hidden = NO;
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _listTableView.estimatedSectionHeaderHeight = 0;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor =  [UIColor colorWithHexString:@"#F5F5F5"];
    }
    return _listTableView;
}

#pragma mark UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-40, 0.5)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    return headerView;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-40, 0.5)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section==0){
        return 1;
    } else if (section == 1) {
        return 1;
    } else if (section==2) {
        if (self.peiJianInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    } else if (section==3) {
        if (self.anZhuangInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    } else if (section==4) {
        if (self.shenQingInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    }
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (self.boHuiCellShow ==YES) {
            BoHuiReasonCell *cell = [tableView dequeueReusableCellWithIdentifier:BoHuiReasonCellID];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.reasonLab.text = self.boHuiYuanYinStr;
            return cell;
        }else
        {
            NSString *cellIdentifier = @"iden";
               UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
               if (!cell) {
                   cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
               }
               return cell;
        }
        
    } else if (indexPath.section==1) {
        
        KeHuShouHuoDiZhiCell *cell = [tableView dequeueReusableCellWithIdentifier:KeHuShouHuoDiZhiCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.detailModel;
        return cell;
    } else if (indexPath.section==2) {
        if (indexPath.row==0) {
            
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"配件信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.zhanKaiBtn.tag = indexPath.section+100;
            return cell;
        }
        if (indexPath.row==1) {
            BuZhuJinPeiJianInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinPeiJianInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.peiJianCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    } else if (indexPath.section==3) {
        if (indexPath.row==0) {
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"产品安装信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.zhanKaiBtn.tag = indexPath.section+100;
            return cell;
        }
        
        if (indexPath.row==1) {
            
            ChanPinAnZhuangInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ChanPinAnZhuangInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.anZhuangXinXiCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    } else if (indexPath.section==4) {
        if (indexPath.row==0) {
            
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"申请补助金信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.zhanKaiBtn.tag = indexPath.section+100;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row==1) {
            ShenQingBuZhuJinInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ShenQingBuZhuJinInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.buZhuXinXiCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    }
    NSString *cellIdentifier = @"iden3";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        if (self.boHuiCellShow==YES) {
            return [tableView fd_heightForCellWithIdentifier:BoHuiReasonCellID configuration:^(BoHuiReasonCell * cell) {
                cell.boHuiYuanYinStr = self.boHuiYuanYinStr;
            }];
        }
        return 0;
    } else if (indexPath.section==1) {
        return [tableView fd_heightForCellWithIdentifier:KeHuShouHuoDiZhiCellID configuration:^(KeHuShouHuoDiZhiCell * cell) {
            cell.model = self.detailModel;
        }];
    } else if (indexPath.section ==2) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.peiJianCellHeight>0) {
                return self.peiJianCellHeight;
            }
            return 215;
        }
    } else if (indexPath.section ==3) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.anZhuangXinXiCellHeight>0) {
                return self.anZhuangXinXiCellHeight;
            }
            return 235;
        }
    } else if (indexPath.section ==4) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.buZhuXinXiCellHeight>0) {
                return self.buZhuXinXiCellHeight;
            }
            return 255;
        }
    }
    return 0;
}

-(void)zhanKaiBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    UIView *view = [sender superview];
    BuZhuJinZheDieCell *cell = (BuZhuJinZheDieCell *)[view superview];
    if (sender.selected) {
        if (sender.tag-100 ==2) {
            self.peiJianInfoCellSelected = YES;
        }
        if (sender.tag-100 ==3) {
            self.anZhuangInfoCellSelected = YES;
        }
        if (sender.tag-100 ==4) {
            self.shenQingInfoCellSelected = YES;
        }
        
        [cell.zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_up_icon"] forState:UIControlStateNormal];
    } else {
        if (sender.tag-100 ==2) {
            self.peiJianInfoCellSelected = NO;
        }
        if (sender.tag-100 ==3) {
            self.anZhuangInfoCellSelected = NO;
        }
        if (sender.tag-100 ==4) {
            self.shenQingInfoCellSelected = NO;
        }
        [cell.zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_down_icon"] forState:UIControlStateNormal];
    }
    [self.listTableView reloadData];
}

-(void)addHeaderView {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, KNavHeight, ScreenW, 180)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 150)];
    backImageView.image = [UIImage imageNamed:@"申请补助金订单详情"];
    backImageView.userInteractionEnabled =YES;
    [headerView addSubview:backImageView];
    
    UIImageView *titleImg = [[UIImageView alloc]initWithFrame:CGRectMake(95, 12, ScreenW-95*2, 31.5)];
    [headerView addSubview:titleImg];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 58, ScreenW, 1)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    [headerView addSubview:lineView];
    
    //待审核
    UIImageView *shenHeImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4, lineView.centerY-10, 20,20)];
    shenHeImageView.image = [UIImage imageNamed:@"待审核对号"];
    [headerView addSubview:shenHeImageView];
    
    UILabel *shenHelab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4, shenHeImageView.bottom+5, 50, 12)];
    shenHelab.font = FontSize(12);
    shenHelab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    shenHelab.text = @"待审核";
    shenHelab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:shenHelab];
    
    //回寄
    UIImageView *huiJiImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4*2+20, lineView.centerY-4.5, 9,9)];
    huiJiImageView.image = [UIImage imageNamed:@"订单详情圆圈"];
    [headerView addSubview:huiJiImageView];
    
    UILabel *huiJilab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4*2+20, huiJiImageView.bottom+5, 80, 12)];
    huiJilab.font = FontSize(12);
    huiJilab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    huiJilab.text = @"回寄";
    huiJilab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:huiJilab];
    
    //完成
    UIImageView *wanChengImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4*3+20*2, lineView.centerY-4.5, 9,9)];
    wanChengImageView.image = [UIImage imageNamed:@"订单详情圆圈"];
    [headerView addSubview:wanChengImageView];
    
    UILabel *wanChenglab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4*3+20*2, wanChengImageView.bottom+5, 50, 12)];
    wanChenglab.font = FontSize(12);
    wanChenglab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    wanChenglab.text = @"完成";
    wanChenglab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:wanChenglab];
    
    //流程  三角箭头
    UIImageView *arrowImgView = [[UIImageView alloc]initWithFrame:CGRectMake(wanChengImageView.centerX-7, lineView.bottom+51-10, 14,10)];
    arrowImgView.image = [UIImage imageNamed:@"三角箭头"];
    [headerView addSubview:arrowImgView];
    [headerView layoutIfNeeded];
    
    //WithFrame:CGRectMake((ScreenW-295)/2, lineView.bottom+51, 295,74)
    //审核时间背景imageview
    UIView *timeImageView = [[UIView alloc]init];
    timeImageView.backgroundColor = [UIColor whiteColor];
    timeImageView.layer.cornerRadius = 4;
    timeImageView.layer.masksToBounds = YES;
    [headerView addSubview:timeImageView];
    
    [timeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).mas_offset(51);
        make.left.equalTo(headerView.mas_left).mas_offset(30);
        make.right.equalTo(headerView.mas_right).mas_offset(-30);
        make.height.mas_greaterThanOrEqualTo(94);
    }];
    [headerView layoutIfNeeded];
    
    //完成详情
    _topLabY = 20;
    for (int i=0; i<self.detailsListArray.count; i++) {
        GrantDetailTopDetailsListModel *model = [GrantDetailTopDetailsListModel mj_objectWithKeyValues:[self.detailsListArray safeObjectAtIndex:i]];
        
        CGFloat height = [self heightFromString:[NSString stringWithFormat:@"%@ %@",model.createTime,model.content] withFont:FontSize(12) constraintToWidth:KWIDTH - 80];
        UILabel *timelab = [[UILabel alloc]initWithFrame:CGRectMake(10, _topLabY, KWIDTH - 80, height)];
        timelab.font = FontSize(12);
        timelab.textColor = [UIColor colorWithHexString:@"#666666"];
        timelab.text = [NSString stringWithFormat:@"%@ %@",model.createTime,model.content];
        timelab.textAlignment = NSTextAlignmentLeft;
        timelab.numberOfLines = 0;
        [timeImageView addSubview:timelab];
        
        _topLabY = _topLabY+height+9*i;
        if (i== self.detailsListArray.count-1) {
            //    状态 -1驳回 申请失败 0发起审核 1开始回寄 3确认回寄 4取消回寄 2已完成申请成功
            
            self.boHuiCellShow = NO;
            //三角箭头 居右
            timelab.textColor = [UIColor colorWithHexString:@"#F2A225"];
            //已完成申请成功
            if ([model.state isEqualToString:@"2"]) {
                titleImg.image = [UIImage imageNamed:@"申请成功"];
                titleImg.frame = CGRectMake(50, 15, KWIDTH-50*2, 31.5);
            }
            //4取消回寄
            else if ([model.state isEqualToString:@"4"]) {
                titleImg.image = [UIImage imageNamed:@"取消回寄"];
            }
            //-1驳回 申请失败
            else if ([model.state isEqualToString:@"-1"]) {
                titleImg.image = [UIImage imageNamed:@"申请失败"];
                titleImg.frame = CGRectMake(95, 15, KWIDTH-95*2, 31.5);
                
                if (model.refuse.length>0) {
                    self.boHuiCellShow = YES;
                    self.boHuiYuanYinStr = model.refuse;
                    
                }else{
                    self.boHuiCellShow = NO;
                }
            }
            [self.view layoutIfNeeded];
            timeImageView.height = timelab.bottom+28;
            headerView.bottom = timeImageView.bottom;
            headerView.height = lineView.bottom+51+timeImageView.height;

        }
    }
    self.listTableView.tableHeaderView = headerView;
}

-(void)leftBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

@end



