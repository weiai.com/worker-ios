//
//  OrderDetailDaiShenHeViewController.m
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//  待审核

#import "OrderDetailDaiShenHeViewController.h"
#import "BuZhuJinZheDieCell.h"
#import "KeHuShouHuoDiZhiCell.h"
#import "BuZhuJinPeiJianInfoCell.h"
#import "ChanPinAnZhuangInfoCell.h"
#import "ShenQingBuZhuJinInfoCell.h"
#import "GrantDetailContentModel.h"
#import "GrantDetailTopDetailsListModel.h"

@interface OrderDetailDaiShenHeViewController ()<UITableViewDelegate,UITableViewDataSource>
//列表
@property (nonatomic,strong) UITableView *listTableView;
@property (nonatomic,strong) GrantDetailContentModel *detailModel;
@property (nonatomic,strong) NSMutableArray *detailsListArray;
@property (nonatomic,strong)UIView *footerView;

@property (nonatomic,strong)UILabel *timelab;
@property (nonatomic,assign)BOOL peiJianInfoCellSelected;
@property (nonatomic,assign)BOOL anZhuangInfoCellSelected;
@property (nonatomic,assign)BOOL shenQingInfoCellSelected;

@property (nonatomic,assign)CGFloat peiJianCellHeight;
@property (nonatomic,assign)CGFloat anZhuangXinXiCellHeight;
@property (nonatomic,assign)CGFloat buZhuXinXiCellHeight;

@property (nonatomic,assign)CGFloat topLabY;
@property (nonatomic,assign)BOOL huiJiCellHidden;
@property (nonatomic,strong)UIView *bgViewsecOne;

@end

static NSString *const KeHuShouHuoDiZhiCellID = @"KeHuShouHuoDiZhiCell";

static NSString *const BuZhuJinZheDieCellID = @"BuZhuJinZheDieCell";

static NSString *const BuZhuJinPeiJianInfoCellID = @"BuZhuJinPeiJianInfoCell";
static NSString *const ChanPinAnZhuangInfoCellID = @"ChanPinAnZhuangInfoCell";
static NSString *const ShenQingBuZhuJinInfoCellID = @"ShenQingBuZhuJinInfoCell";

@implementation OrderDetailDaiShenHeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    
    [self requestGrantsDetail];
    [self addFooterView];
    [self shwoBgviewOne];
}

-(void)requestGrantsDetail
{
    NSDictionary *param = @{
        @"id":NOTNIL(self.idStr)
    };
    [NetWorkTool POST:repairApplyGrantsGetApplyGrantsById param:param success:^(id dic) {
        self.detailModel = [GrantDetailContentModel mj_objectWithKeyValues:dic[@"data"]];
        
        self.detailsListArray = [GrantDetailTopDetailsListModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"detailsList"]];
        self.detailModel = [GrantDetailContentModel mj_objectWithKeyValues:dic[@"data"][@"repairApplyGrants"]];
        [self configViews];
        [self.listTableView reloadData];
        KMyLog(@"订单详情 成功与否 ^^^ %@", dic[@"data"]);
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)configViews
{
    [self addBackTableView];
}

-(void)addBackTableView
{
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([KeHuShouHuoDiZhiCell class]) bundle:nil] forCellReuseIdentifier:KeHuShouHuoDiZhiCellID];
    
    [self.listTableView registerClass:[BuZhuJinZheDieCell class] forCellReuseIdentifier:BuZhuJinZheDieCellID];
    
    [self.listTableView registerClass:[BuZhuJinPeiJianInfoCell class] forCellReuseIdentifier:BuZhuJinPeiJianInfoCellID];
    [self.listTableView registerClass:[ChanPinAnZhuangInfoCell class] forCellReuseIdentifier:ChanPinAnZhuangInfoCellID];
    [self.listTableView registerClass:[ShenQingBuZhuJinInfoCell class] forCellReuseIdentifier:ShenQingBuZhuJinInfoCellID];
    [self.view addSubview:self.listTableView];
    [self addHeaderView];
}

- (UITableView *)listTableView
{
    if (!_listTableView) {
        _listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-TabbarSafeBottomMargin) style:UITableViewStylePlain];
        _listTableView.hidden = NO;
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _listTableView.estimatedSectionHeaderHeight = 0;
        _listTableView.estimatedSectionFooterHeight = 0;
        _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _listTableView.backgroundColor =  [UIColor colorWithHexString:@"#F5F5F5"];
    }
    return _listTableView;
}

#pragma mark UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-40, 0.5)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    return headerView;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH-40, 0.5)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0){
        return 1;
    }
    if (section==1) {
        if (self.peiJianInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    } else if (section==2) {
        if (self.anZhuangInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    } else if (section==3) {
        if (self.shenQingInfoCellSelected ==YES) {
            return 2;
        } else {
            return 1;
        }
        return 1;
    }
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (self.huiJiCellHidden ==YES) {
                    NSString *cellIdentifier = @"iden";
                      UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
                      if (!cell) {
                          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                      }
            return cell;
            
            
               }else
               {
                   KeHuShouHuoDiZhiCell *cell = [tableView dequeueReusableCellWithIdentifier:KeHuShouHuoDiZhiCellID];
                         cell.selectionStyle = UITableViewCellSelectionStyleNone;
                         cell.model = self.detailModel;
                         return cell;
                   
               }
      
    } else if (indexPath.section==1) {
        if (indexPath.row==0) {
            
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"配件信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.zhanKaiBtn.tag = indexPath.section+100;
            return cell;
        }
        if (indexPath.row==1) {
            BuZhuJinPeiJianInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinPeiJianInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.peiJianCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    } else if (indexPath.section==2) {
        if (indexPath.row==0) {
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"产品安装信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.zhanKaiBtn.tag = indexPath.section+100;
            return cell;
        }
        
        if (indexPath.row==1) {
            ChanPinAnZhuangInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ChanPinAnZhuangInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.anZhuangXinXiCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    } else if (indexPath.section==3) {
        if (indexPath.row==0) {
            
            BuZhuJinZheDieCell *cell = [tableView dequeueReusableCellWithIdentifier:BuZhuJinZheDieCellID];
            cell.infoLab.text = @"申请补助金信息";
            [cell.zhanKaiBtn addTarget:self action:@selector(zhanKaiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.zhanKaiBtn.tag = indexPath.section+100;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        if (indexPath.row==1) {
            ShenQingBuZhuJinInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ShenQingBuZhuJinInfoCellID];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.heightHandle = ^(CGFloat height) {
                self.buZhuXinXiCellHeight = height;
                [self.listTableView beginUpdates];
                [self.listTableView endUpdates];
            };
            cell.model = self.detailModel;
            return cell;
        }
    }
    NSString *cellIdentifier = @"iden3";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (self.huiJiCellHidden ==YES) {
            return 0;
        } else {
            return [tableView fd_heightForCellWithIdentifier:KeHuShouHuoDiZhiCellID configuration:^(KeHuShouHuoDiZhiCell * cell) {
                cell.model = self.detailModel;
            }];
        }
    } else if (indexPath.section ==1) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.peiJianCellHeight>0) {
                return self.peiJianCellHeight;
            }
            return 215;
        }
    } else if (indexPath.section ==2) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.anZhuangXinXiCellHeight>0) {
                return self.anZhuangXinXiCellHeight;
            }
            return 215;
        }
    } else if (indexPath.section ==3) {
        if (indexPath.row==0) {
            return 37;
        }
        if (indexPath.row==1){
            if (self.buZhuXinXiCellHeight>0) {
                return self.buZhuXinXiCellHeight;
            }
            return 255;
        }
        
    }
    return 0;
}

-(void)zhanKaiBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    UIView *view = [sender superview];
    BuZhuJinZheDieCell *cell = (BuZhuJinZheDieCell *)[view superview];
    if (sender.selected) {
        if (sender.tag-100 ==1) {
            self.peiJianInfoCellSelected = YES;
        }
        if (sender.tag-100 ==2) {
            self.anZhuangInfoCellSelected = YES;
        }
        if (sender.tag-100 ==3) {
            self.shenQingInfoCellSelected = YES;
        }
        
        [cell.zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_up_icon"] forState:UIControlStateNormal];
    } else {
        if (sender.tag-100 ==1) {
            self.peiJianInfoCellSelected = NO;
        }
        if (sender.tag-100 ==2) {
            self.anZhuangInfoCellSelected = NO;
        }
        if (sender.tag-100 ==3) {
            self.shenQingInfoCellSelected = NO;
        }
        [cell.zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_down_icon"] forState:UIControlStateNormal];
    }
    [self.listTableView reloadData];
}

-(void)addHeaderView {
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, KNavHeight, ScreenW, 180)];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 150)];
    backImageView.image = [UIImage imageNamed:@"申请补助金订单详情"];
    backImageView.userInteractionEnabled = YES;
    [headerView addSubview:backImageView];
    
    UIImageView *topTipsImg = [[UIImageView alloc]initWithFrame:CGRectMake(70, 12, ScreenW-70*2, 31.5)];
    topTipsImg.image = [UIImage imageNamed:@"subsidyApplication"];
    [headerView addSubview:topTipsImg];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 58, ScreenW, 1)];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    [headerView addSubview:lineView];
    
    //待审核
    UIImageView *shenHeImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4, lineView.centerY-10, 20,20)];
    shenHeImageView.image = [UIImage imageNamed:@"待审核对号"];
    [headerView addSubview:shenHeImageView];
    
    UILabel *shenHelab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4, shenHeImageView.bottom+5, 50, 12)];
    shenHelab.font = FontSize(12);
    shenHelab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    shenHelab.text = @"待审核";
    shenHelab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:shenHelab];
    
    //回寄
    UIImageView *huiJiImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4*2+20, lineView.centerY-4.5, 9,9)];
    huiJiImageView.image = [UIImage imageNamed:@"订单详情圆圈"];
    [headerView addSubview:huiJiImageView];
    
    UILabel *huiJilab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4*2+20, huiJiImageView.bottom+5, 80, 12)];
    huiJilab.font = FontSize(12);
    huiJilab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    huiJilab.text = @"回寄";
    huiJilab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:huiJilab];
    
    //完成
    UIImageView *wanChengImageView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenW-60)/4*3+20*2, lineView.centerY-4.5, 9,9)];
    wanChengImageView.image = [UIImage imageNamed:@"订单详情圆圈"];
    [headerView addSubview:wanChengImageView];
    
    UILabel *wanChenglab = [[UILabel alloc]initWithFrame:CGRectMake((ScreenW-60)/4*3+20*2, wanChengImageView.bottom+5, 50, 12)];
    wanChenglab.font = FontSize(12);
    wanChenglab.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    wanChenglab.text = @"完成";
    wanChenglab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:wanChenglab];
    
    //流程  三角箭头
    UIImageView *arrowImgView = [[UIImageView alloc]initWithFrame:CGRectMake(shenHeImageView.centerX-7, lineView.bottom+51-10, 14,10)];
    arrowImgView.image = [UIImage imageNamed:@"三角箭头"];
    [headerView addSubview:arrowImgView];
    [headerView layoutIfNeeded];
    
    //审核时间背景imageview
    UIView *timeImageView = [[UIView alloc]init];
    timeImageView.backgroundColor = [UIColor whiteColor];
    timeImageView.layer.cornerRadius = 4;
    timeImageView.layer.masksToBounds = YES;
    [headerView addSubview:timeImageView];
    
    [timeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).mas_offset(51);
        make.left.equalTo(headerView.mas_left).mas_offset(30);
        make.right.equalTo(headerView.mas_right).mas_offset(-30);
        make.height.mas_greaterThanOrEqualTo(74);
    }];
    [headerView layoutIfNeeded];
    
    //完成详情
    _topLabY = 20;
    for (int i=0; i<self.detailsListArray.count; i++) {
        GrantDetailTopDetailsListModel *model = [GrantDetailTopDetailsListModel mj_objectWithKeyValues:[self.detailsListArray safeObjectAtIndex:i]];
        
        CGFloat height = [self heightFromString:[NSString stringWithFormat:@"%@ %@",model.createTime,model.content] withFont:FontSize(12) constraintToWidth:(ScreenW-80)];
        UILabel *timelab = [[UILabel alloc]initWithFrame:CGRectMake(10, _topLabY, (ScreenW-80), height)];
        //timelab.backgroundColor = [UIColor greenColor];
        timelab.font = FontSize(12);
        timelab.textColor = [UIColor colorWithHexString:@"#F2A225"];
        timelab.text = [NSString stringWithFormat:@"%@ %@",model.createTime,model.content];
        timelab.textAlignment = NSTextAlignmentLeft;
        timelab.numberOfLines = 0;
        [timeImageView addSubview:timelab];
        
        _topLabY = _topLabY+height+9*i;
        if (i== self.detailsListArray.count-1) {
            //状态 -1驳回 申请失败 0发起审核 1开始回寄 3确认回寄 4取消回寄 2已完成申请成功
            //三角箭头 居左
            if ([model.state isEqualToString:@"0"]) {
                //不显示回寄厂家地址cell
                self.huiJiCellHidden = YES;
                arrowImgView.frame = CGRectMake(shenHeImageView.centerX-7, lineView.bottom+51-10, 14,10);
            }
            //1  显示底部2个按钮
            if ([model.state isEqualToString:@"1"]) {
                //三角箭头 居中
                self.footerView.hidden = NO;
                self.huiJiCellHidden = NO;
                arrowImgView.frame = CGRectMake(huiJiImageView.centerX-7, lineView.bottom+51-10, 14,10);
            } else {
                self.footerView.hidden = YES;
            }
        }
    }
    //CGFloat height = timeImageView.top+74;
    self.listTableView.tableHeaderView = headerView;
}

-(void)leftBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addFooterView
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 100)];
    footerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    self.footerView = footerView;
    
    UIButton *faFangBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [faFangBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    faFangBut.frame  = CGRectMake(20, 32, (KWIDTH-20*2-25)/2, 32);
    [faFangBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [faFangBut setTitle:@"取消回寄" forState:UIControlStateNormal];
    [faFangBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    faFangBut.titleLabel.font = KFontPingFangSCMedium(15);
    faFangBut.layer.masksToBounds = YES;
    faFangBut.layer.cornerRadius = 16;
    [faFangBut addTarget:self action:@selector(cancleButAction) forControlEvents:(UIControlEventTouchUpInside)];
    [footerView addSubview: faFangBut];
    
    UIButton *boHuiBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [boHuiBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    boHuiBut.frame  = CGRectMake(faFangBut.right+25, 32, (KWIDTH-20*2-25)/2, 32);
    [boHuiBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [boHuiBut setTitle:@"确定回寄" forState:UIControlStateNormal];
    [boHuiBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    boHuiBut.layer.masksToBounds = YES;
    boHuiBut.layer.cornerRadius = 16;
    boHuiBut.titleLabel.font = KFontPingFangSCMedium(15);
    [boHuiBut addTarget:self action:@selector(sureButAction) forControlEvents:(UIControlEventTouchUpInside)];
    [footerView addSubview: boHuiBut];
    
    self.listTableView.tableFooterView = footerView;
}

//取消回寄
-(void)cancleButAction {
    KMyLog(@"取消回寄");
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecOne];
}

/**
 弹出框的背景图 厂家订单 推送成功
 */
-(void)shwoBgviewOne{
    self.bgViewsecOne = [[UIView alloc]init];
    self.bgViewsecOne.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsecOne.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecOne addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(181);
        make.left.offset(38);
        make.right.offset(-38);
    }];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(15, 27, KWIDTH-76-30, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentLeft;
    UpLable.text = @"温馨提示";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(15, UpLable.bottom+20, KWIDTH-76-30, 20)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(13);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.textAlignment = NSTextAlignmentLeft;
    DowLable.text = @"您确定要取消回寄吗？";
    DowLable.numberOfLines = 0;
    
    UIButton *leftBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftBut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
    //leftBut.backgroundColor = [UIColor orangeColor];
    [leftBut setTitle:@"否" forState:(UIControlStateNormal)];
    leftBut.titleLabel.textColor = K999999;
    leftBut.layer.masksToBounds = YES;
    leftBut.layer.cornerRadius = 4;
    [leftBut addTarget:self action:@selector(leftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftBut];
    [leftBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.left.offset((KWIDTH-38*2)/2);
        make.width.offset(50);
        make.bottom.offset(-18);
    }];
    
    UIButton *rightBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [rightBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    //rightBut.backgroundColor = [UIColor cyanColor];
    [rightBut setTitle:@"是" forState:(UIControlStateNormal)];
    rightBut.titleLabel.textColor = [UIColor colorWithHexString:@"#70BE68"];
    rightBut.layer.masksToBounds = YES;
    rightBut.layer.cornerRadius = 4;
    [rightBut addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:rightBut];
    [rightBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.right.offset(-30);
        make.width.offset(50);
        make.bottom.offset(-18);
    }];
}

-(void)leftBtnAction:(UIButton *)but{
    [_bgViewsecOne removeFromSuperview];
    [singlTool shareSingTool].needReasfh = YES;
}

- (void)rightBtnAction:(UIButton *)button {
    NSLog(@"取消回寄");
    
    NSDictionary *param = @{
        @"id":NOTNIL(self.idStr),
    };
    [NetWorkTool POST:repairApplyGrantsCancelReturn param:param success:^(id dic) {
        [self.bgViewsecOne removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];

    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        [self.bgViewsecOne removeFromSuperview];
        
    } fail:^(NSError *error) {
        [self.bgViewsecOne removeFromSuperview];
        
    } needUser:NO];
}


-(void)sureButAction {
    NSDictionary *param = @{
        @"id":NOTNIL(self.idStr),//补助金id
    };
    [NetWorkTool POST:repairApplyGrantsPartsReturn param:param success:^(id dic) {
        [self.navigationController popViewControllerAnimated:YES];
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
    } fail:^(NSError *error) {
        
    } needUser:NO];
};

@end
