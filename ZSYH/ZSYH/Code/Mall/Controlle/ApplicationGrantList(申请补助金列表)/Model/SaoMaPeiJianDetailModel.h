//
//  SaoMaPeiJianDetailModel.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/14.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SaoMaPeiJianDetailModel : NSObject


@property(nonatomic,strong)NSString *row;
@property(nonatomic,strong)NSString *type_name;     //类型
@property(nonatomic,strong)NSString *type_id;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *parts_name;    //配件名字
@property(nonatomic,strong)NSString *price;
@property(nonatomic,strong)NSString *count;
@property(nonatomic,strong)NSString *total_price;
@property(nonatomic,strong)NSString *qrCode;
@property(nonatomic,strong)NSString *qrcode;        //扫码安装的二维码信息
@property(nonatomic,strong)NSString *warranty;      //保质期
@property(nonatomic,strong)NSString *productionDate;    //有效期起始时间
@property(nonatomic,strong)NSString *termValidity;  //有效期截止时间
@property (nonatomic, copy) NSMutableArray *imageArray;
@property(nonatomic,strong)NSString *install;
@property(nonatomic,strong)NSString *user_name;
@property(nonatomic,strong)NSString *user_phone;
@property(nonatomic,strong)NSString *user_address;
@property(nonatomic,strong)NSString *cost;
@property(nonatomic,strong)NSString *imgUrl;


@property(nonatomic,strong)NSString *compensationPrice;
@property(nonatomic,strong)NSString *deleButHidden;
@property(nonatomic,strong)NSString *shTime;
@property(nonatomic,strong)NSString *attributeName;
@property(nonatomic,strong)NSString *remarks;
@property(nonatomic,strong)NSString *image_url;
@property(nonatomic,strong)NSString *factory_address;
@property(nonatomic,strong)NSString *anTime;
@property(nonatomic,strong)NSString *parts_price;
@property(nonatomic,strong)NSString *repair_user_price;
@property(nonatomic,strong)NSString *is_hot_push;
@property(nonatomic,strong)NSString *contractPrice;
@property(nonatomic,strong)NSString *modifyDate;
@property(nonatomic,strong)NSString *repairUser;
@property(nonatomic,strong)NSString *parts_number;
@property(nonatomic,strong)NSString *parts_quantity;
@property(nonatomic,strong)NSString *agent_user_price;
@property(nonatomic,strong)NSString *sale_price;
@property(nonatomic,strong)NSString *is_today_hot;
@property(nonatomic,strong)NSString *freeWarranty;
@property(nonatomic,strong)NSString *salePrice;
@property(nonatomic,strong)NSString *hisList;
@property(nonatomic,strong)NSString *is_new_push;
@property(nonatomic,strong)NSString *parts_model;
@property(nonatomic,strong)NSString *lower_agentA;
@property(nonatomic,strong)NSString *partsId;
@property(nonatomic,strong)NSString *secondName;
@property(nonatomic,strong)NSString *parts_id;
@property(nonatomic,strong)NSString *parts_details;
@property(nonatomic,strong)NSString *classification;
@property(nonatomic,strong)NSString *lower_user;
@property(nonatomic,strong)NSString *soft;
@property(nonatomic,strong)NSString *firstName;
@property(nonatomic,strong)NSString *createDate;
@property(nonatomic,strong)NSString *parts_type;
@property(nonatomic,strong)NSString *lower_agentB;
@property(nonatomic,strong)NSString *factory_id;
@property(nonatomic,strong)NSString *parts_place;
@property(nonatomic,strong)NSString *parts_logo;
@property(nonatomic,strong)NSString *factory_name;
@property(nonatomic,strong)NSString *is_good_choice;
@property(nonatomic,strong)NSString *unit;
@property(nonatomic,strong)NSString *quantity;
//@property(nonatomic,strong)NSString *newTime;
@property(nonatomic,strong)NSString *parts_brand;



@end

NS_ASSUME_NONNULL_END
