//
//  ApplicationGrantListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApplicationGrantListModel : NSObject
@property (nonatomic, strong) NSString *transfer;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *user_address;
@property (nonatomic, strong) NSString *person_name;
@property (nonatomic, strong) NSString *user_name;

@property (nonatomic, strong) NSString *repairId;
@property (nonatomic, strong) NSString *cost;
@property (nonatomic, strong) NSString *faultDetails;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *open_id;

@property (nonatomic, strong) NSString *installImg;
@property (nonatomic, strong) NSString *transferPersonId;
@property (nonatomic, strong) NSString *user_phone;
@property (nonatomic, strong) NSString *secondName;
@property (nonatomic, strong) NSString *qrcode;

@property (nonatomic, strong) NSString *userPhone;
@property (nonatomic, strong) NSString *userAddress;
@property (nonatomic, strong) NSString *factoryAddress;
@property (nonatomic, strong) NSString *freeWarranty;
@property (nonatomic, strong) NSString *factoryName;

@property (nonatomic, strong) NSString *factoryPhone;
@property (nonatomic, strong) NSString *factoryId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *type_name;

@property (nonatomic, strong) NSString *faultImg;
@property (nonatomic, strong) NSString *warranty;
@property (nonatomic, strong) NSString *installDate;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *install;

@property (nonatomic, strong) NSString *transferTime;
@property (nonatomic, strong) NSString *parts_name;
@property (nonatomic, strong) NSString *attributeName;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *compensationPrice;

@property (nonatomic, strong) NSString *partsId;

@end

NS_ASSUME_NONNULL_END
