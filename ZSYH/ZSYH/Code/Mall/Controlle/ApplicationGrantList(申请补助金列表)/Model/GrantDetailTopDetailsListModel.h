//
//  GrantDetailTopDetailsListModel.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/20.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GrantDetailTopDetailsListModel : NSObject
@property (nonatomic,strong) NSString *createTime;//过程时间
@property (nonatomic,strong) NSString *content;//内容
@property (nonatomic,strong) NSString *state;//状态 -1驳回 0发起审核 1开始回寄 3确认回寄 4取消回寄 2已完成
@property (nonatomic,strong) NSString *refuse;//驳回原因 当驳回的时候，展示

@end

NS_ASSUME_NONNULL_END
