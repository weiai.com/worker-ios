//
//  GrantDetailContentModel.h
//  FactorySale
//
//  Created by 李 on 2020/3/30.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GrantDetailContentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GrantDetailContentModel : NSObject
@property (nonatomic,strong) NSString *type_name;//产品名称
@property (nonatomic,strong) NSString *attributeName;//     属性名称
@property (nonatomic,strong) NSString *compensationPrice;//工时费补助金
@property (nonatomic,strong) NSString *install;//候保二维码有效期
@property (nonatomic,strong) NSString *freeWarranty;// 无忧质保期格式2020-01-01至2021-01-01
@property (nonatomic,strong) NSString *warranty;//候保工时费补贴期限

@property (nonatomic,strong) NSString *installDate;//安装日期
@property (nonatomic,strong) NSString *user_name;//
@property (nonatomic,strong) NSString *user_phone;//
@property (nonatomic,strong) NSString *user_address;//
@property (nonatomic,strong) NSString *cost;//
@property (nonatomic,strong) NSString *imgUrl;//安装凭证 多个用|隔开
@property (nonatomic,strong) NSString *faultDetails;// 产品故障描述
@property (nonatomic,strong) NSString *username;//收货人姓名
@property (nonatomic,strong) NSString *userAddress;// userAddress
@property (nonatomic,strong) NSString *faultImg;//产品故障图片 多个以|隔开
@property (nonatomic,strong) NSString *factoryName;//
@property (nonatomic,strong) NSString *factoryPhone;//创建时间
@property (nonatomic,strong) NSString *factoryAddress;//
@property (nonatomic,strong) NSString *userPhone;//
@property (nonatomic,strong) NSString *productionDate;
@property (nonatomic,strong) NSString *termValidity;
@property (nonatomic,strong) NSString *status;
@end

NS_ASSUME_NONNULL_END
