//
//  KeHuShouHuoDiZhiCell.h
//  FactorySale
//
//  Created by 李 on 2020/3/31.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrantDetailContentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface KeHuShouHuoDiZhiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *diZhiLab;
@property(nonatomic,strong)GrantDetailContentModel *model;

@end

NS_ASSUME_NONNULL_END
