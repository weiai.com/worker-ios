//
//  ApplicationGrantListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationGrantListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ApplicationGrantListTableViewCell : UITableViewCell

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *orderTitLab;       //订单编号
@property (nonatomic, strong) UILabel *stateLab;      //状态

@property (nonatomic, strong) UILabel *pjmcTitLab;    //配件名称 标题
@property (nonatomic, strong) UILabel *pjmcConLab;    //配件名称 内容

@property (nonatomic, strong) UILabel *pjxhTitLab;    //配件型号 标题
@property (nonatomic, strong) UILabel *pjxhConLab;    //配件型号 内容

@property (nonatomic, strong) UILabel *bzjeTitLab;    //补助金额 标题
@property (nonatomic, strong) UILabel *bzjeConLab;    //补助金额 内容

@property (nonatomic, strong) UILabel *dateTitLab;    //申请时间 标题
@property (nonatomic, strong) UILabel *dateConLab;    //申请时间 内容

@property (nonatomic, strong) UILabel *gzmsTitLab;    //故障描述 标题
@property (nonatomic, strong) UILabel *gzmsConLab;    //故障描述 内容

@property (nonatomic, strong) UIButton *ConCusSerBtn;  //联系客服 按钮

-(void)refasf:(ApplicationGrantListModel *)model;

@end

NS_ASSUME_NONNULL_END
