//
//  ShenQingBuZhuJinInfoCell.m
//  FactorySale
//
//  Created by 李 on 2020/3/26.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import "ShenQingBuZhuJinInfoCell.h"
#import "SYBigImage.h"

@interface ShenQingBuZhuJinInfoCell ()

@property(nonatomic,strong) UILabel *shenQingJinELab;
@property(nonatomic,strong) UILabel *guZhangMiaoShuLab;
@property(nonatomic,strong) UILabel *nameLab;
@property(nonatomic,strong) UILabel *phoneLab;
@property(nonatomic,strong) UILabel *diZhiLab;
@property(nonatomic,strong) UILabel *pingZhengWordLab;

@end
@implementation ShenQingBuZhuJinInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(20, 0, ScreenW, 0.5)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [self.contentView addSubview:lineView];
        
        //申请金额：
        UILabel *shenQingJinEWordLab = [[UILabel alloc] init];
        shenQingJinEWordLab.frame = CGRectMake(39, lineView.bottom+16, 12*5, 12);
        shenQingJinEWordLab.font = KFontPingFangSCRegular(12);
        shenQingJinEWordLab.textColor = K333333;
        shenQingJinEWordLab.text = @"申请金额：";
        [self.contentView addSubview:shenQingJinEWordLab];
        
        UILabel *shenQingJinELab = [[UILabel alloc] init];
        shenQingJinELab.frame = CGRectMake(shenQingJinEWordLab.right, lineView.bottom+16, KWIDTH-39-14*5, 12);
        shenQingJinELab.font = KFontPingFangSCRegular(12);
        shenQingJinELab.textColor = K666666;
        shenQingJinELab.text = @"";
        [self.contentView addSubview:shenQingJinELab];
        self.shenQingJinELab =shenQingJinELab;
        
        // 产品故障描述：
        UILabel *guZhangMiaoShuWordLab = [[UILabel alloc] init];
        guZhangMiaoShuWordLab.frame = CGRectMake(39, 40, 12*7, 12);
        guZhangMiaoShuWordLab.font = KFontPingFangSCRegular(12);
        guZhangMiaoShuWordLab.textColor = K333333;
        guZhangMiaoShuWordLab.text = @"产品故障描述：";
        [self.contentView addSubview:guZhangMiaoShuWordLab];
        
        UILabel *guZhangMiaoShuLab = [[UILabel alloc] init];
        guZhangMiaoShuLab.frame = CGRectMake(guZhangMiaoShuWordLab.right, 40, KWIDTH-39-14*5, 12);
        guZhangMiaoShuLab.font = KFontPingFangSCRegular(12);
        guZhangMiaoShuLab.textColor = K666666;
        guZhangMiaoShuLab.text = @"";
        [self.contentView addSubview:guZhangMiaoShuLab];
        self.guZhangMiaoShuLab =guZhangMiaoShuLab;
        
        //收货人姓名
        UILabel *nameWordLab = [[UILabel alloc] init];
        nameWordLab.frame = CGRectMake(39, guZhangMiaoShuWordLab.bottom+12, 12*6, 12);
        nameWordLab.font = KFontPingFangSCRegular(12);
        nameWordLab.textColor = K333333;
        nameWordLab.text = @"收货人姓名：";
        [self.contentView addSubview:nameWordLab];
        
        UILabel *nameLab = [[UILabel alloc] init];
        nameLab.frame = CGRectMake(nameWordLab.right, guZhangMiaoShuWordLab.bottom+12, KWIDTH-39-14*5, 12);
        nameLab.font = KFontPingFangSCRegular(12);
        nameLab.textColor = K666666;
        nameLab.text = @"";
        [self.contentView addSubview:nameLab];
        self.nameLab =nameLab;
        
        //收货人电话
        UILabel *phoneWordLab = [[UILabel alloc] init];
        phoneWordLab.frame = CGRectMake(39, nameWordLab.bottom+12, 12*6, 12);
        phoneWordLab.font = KFontPingFangSCRegular(12);
        phoneWordLab.textColor = K333333;
        phoneWordLab.text = @"收货人电话：";
        [self.contentView addSubview:phoneWordLab];
        
        UILabel *phoneLab = [[UILabel alloc] init];
        phoneLab.frame = CGRectMake(phoneWordLab.right, nameWordLab.bottom+12, KWIDTH-39-14*5, 12);
        phoneLab.font = KFontPingFangSCRegular(12);
        phoneLab.textColor = K666666;
        phoneLab.text = @"";
        [self.contentView addSubview:phoneLab];
        self.phoneLab =phoneLab;
        
        //收货人地址
        UILabel *diZhiWordLab = [[UILabel alloc] init];
        diZhiWordLab.frame = CGRectMake(39, phoneWordLab.bottom+12, 12*6, 12);
        diZhiWordLab.font = KFontPingFangSCRegular(12);
        diZhiWordLab.textColor = K333333;
        diZhiWordLab.text = @"收货人地址：";
        [self.contentView addSubview:diZhiWordLab];
        
        UILabel *diZhiLab = [[UILabel alloc] init];
        diZhiLab.frame = CGRectMake(nameWordLab.right, phoneWordLab.bottom+12, KWIDTH-39-14*5, 12);
        diZhiLab.font = KFontPingFangSCRegular(12);
        diZhiLab.textColor = K666666;
        diZhiLab.text = @"";
        [self.contentView addSubview:diZhiLab];
        self.diZhiLab =diZhiLab;
        
        //故障凭证
        UILabel *pingZhengWordLab = [[UILabel alloc] init];
        pingZhengWordLab.frame = CGRectMake(39, diZhiLab.bottom+15, KWIDTH-39, 12);
        pingZhengWordLab.font = KFontPingFangSCRegular(12);
        pingZhengWordLab.textColor = K333333;
        pingZhengWordLab.text = @"故障产品照片";
        [self.contentView addSubview:pingZhengWordLab];
        self.pingZhengWordLab = pingZhengWordLab;
    }
    return self;
}

-(void)setModel:(GrantDetailContentModel *)model
{
    _model =model;
    if (_model) {
        self.shenQingJinELab.text = [NSString stringWithFormat:@"%@%@", @"¥",model.compensationPrice];
        self.guZhangMiaoShuLab.text = model.faultDetails;
        self.nameLab.text = model.username;
        self.phoneLab.text = model.userPhone;
        self.diZhiLab.text = model.userAddress;
        
        //        NSString *imageStr = _model.faultImg;
        //        NSArray *array = [imageStr componentsSeparatedByString:@"|"];
        //        NSArray * imageList = array;
        //
        //        for (int i = 0; i < imageList.count; i ++) {
        //            UIImageView * button = [[UIImageView alloc]init];
        //            [button setImage:[UIImage imageNamed:imageList[i]]];
        //            //下面是九宫格的设计
        //            NSInteger l = i % 3;
        //            NSInteger h = i / 3;
        //            NSInteger leftMagin = (ScreenW-80-58*3)/3;
        //            NSInteger kMagin = 14;
        //            CGFloat width = 58;
        //            CGFloat height = 58;
        //            //通过个数位置确定具体的frame
        //            button.frame = CGRectMake(leftMagin + (leftMagin + width) * l,self.pingZhengWordLab.bottom+14 + (kMagin + height) * h, width, height);
        //            [button sd_setImageWithURL:[NSURL URLWithString:imageList[i]]];
        //            SYBigImage * bigI = [[SYBigImage alloc]init];
        //            [button addGestureRecognizer:bigI];
        //            button.userInteractionEnabled = YES;
        //            [self.contentView addSubview:button];
        //
        //            if (i== imageList.count-1) {
        //                CGFloat height = button.height+button.frame.origin.y+10;
        //                if (self.heightHandle) {
        //                    self.heightHandle(height);
        //                }
        //            }
        //        }
        
        //照片背景 View
        UIView *imageBackView = [[UIView alloc] init];
        imageBackView.frame = CGRectMake(32.5, self.pingZhengWordLab.bottom + 5, KWIDTH-65, 75);
        [self.contentView addSubview:imageBackView];
        self.imageBackView = imageBackView;
        
        NSString *imagesStr = self.model.faultImg;
        NSArray *imagearr = [imagesStr componentsSeparatedByString:@"|"];
        CGFloat wei = (KWIDTH-25-46-46)/4;
        
        if (imagearr.count > 0) {
            [self.imageBackView removeAllSubviews];
            for (int i = 0; i < imagearr.count; i++) {
                UIImageView *myimage = [[UIImageView alloc]init];
                [self.imageBackView addSubview:myimage];
                [myimage sd_setImageWithURL:[NSURL URLWithString:imagearr[i]] placeholderImage:defaultImg];
                myimage.frame = CGRectMake((wei+10)*(i % 4)+10, (wei + 10) * (i / 4)+5, wei, wei);
                myimage.userInteractionEnabled = YES;
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [myimage addGestureRecognizer:bigI];
                self.imageBackView.frame = CGRectMake(32.5, self.pingZhengWordLab.bottom + 10, self.contentView.width - 65, (wei + 10) * (i / 4 + 1));
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
