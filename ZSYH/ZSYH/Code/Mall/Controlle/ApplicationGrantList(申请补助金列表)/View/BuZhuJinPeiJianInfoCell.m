//
//  BuZhuJinPeiJianInfoCell.m
//  FactorySale
//
//  Created by 李 on 2020/3/26.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import "BuZhuJinPeiJianInfoCell.h"
#import "SYBigImage.h"

@interface BuZhuJinPeiJianInfoCell ()

@property(nonatomic,strong) UILabel *nameContentLab;
@property(nonatomic,strong) UILabel *typeContentLab;
@property(nonatomic,strong) UILabel *buZhuJinContentLab;
@property(nonatomic,strong) UILabel *youXiaoQiWordLab;
@property(nonatomic,strong) UILabel *anZhuangTimeLab;
@property(nonatomic,strong) UILabel *erWeiMaTimeLab;
@property(nonatomic,strong) UILabel *baoZhiQiLab;
@property(nonatomic,strong)UILabel *pingZhengWordLab;
@end
@implementation BuZhuJinPeiJianInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        //配件名称：
        UILabel *nameWordLab = [[UILabel alloc] init];
        nameWordLab.frame = CGRectMake(39, 13, 12*5, 12);
        nameWordLab.font = KFontPingFangSCRegular(12);
        nameWordLab.textColor = K333333;
        nameWordLab.text = @"配件名称：";
        [self.contentView addSubview:nameWordLab];
        
        UILabel *nameContentLab = [[UILabel alloc] init];
        nameContentLab.frame = CGRectMake(nameWordLab.right, 13, KWIDTH-39*2-12*5, 12);
        nameContentLab.font = KFontPingFangSCRegular(12);
        nameContentLab.textColor = K666666;
        nameContentLab.text = @"";
        [self.contentView addSubview:nameContentLab];
        self.nameContentLab = nameContentLab;
        
        //配件型号：
        UILabel *typeLab = [[UILabel alloc] init];
        typeLab.frame = CGRectMake(39, nameContentLab.bottom + 15, 12*5, 12);
        typeLab.font = KFontPingFangSCRegular(12);
        typeLab.textColor = K333333;
        typeLab.text = @"配件型号：";
        [self.contentView addSubview:typeLab];
        
        UILabel *typeContentLab = [[UILabel alloc] init];
        typeContentLab.frame = CGRectMake(typeLab.right, nameContentLab.bottom + 15, KWIDTH-39*2-12*5, 12);
        typeContentLab.font = KFontPingFangSCRegular(12);
        typeContentLab.textColor = K666666;
        typeContentLab.text = @"";
        [self.contentView addSubview:typeContentLab];
        self.typeContentLab = typeContentLab;
        
        //工时费补助金：
        UILabel *buZhuJinWordLab = [[UILabel alloc] init];
        buZhuJinWordLab.frame = CGRectMake(39, typeLab.bottom+15, 12*7, 12);
        buZhuJinWordLab.font = KFontPingFangSCRegular(12);
        buZhuJinWordLab.textColor = K333333;
        buZhuJinWordLab.text = @"工时费补助金：";
        [self.contentView addSubview:buZhuJinWordLab];
        
        UILabel *buZhuJinContentLab = [[UILabel alloc] init];
        buZhuJinContentLab.frame = CGRectMake(buZhuJinWordLab.right, typeLab.bottom+15, KWIDTH-39*2-12*7, 12);
        buZhuJinContentLab.font = KFontPingFangSCRegular(12);
        buZhuJinContentLab.textColor = K666666;
        buZhuJinContentLab.text = @"";
        [self.contentView addSubview:buZhuJinContentLab];
        self.buZhuJinContentLab = buZhuJinContentLab;
        
        //候保二维码有效期：
        UILabel *erWeiMaTimeWordLab = [[UILabel alloc] init];
        erWeiMaTimeWordLab.frame = CGRectMake(39, buZhuJinWordLab.bottom+15, 12*9, 12);
        erWeiMaTimeWordLab.font = KFontPingFangSCRegular(12);
        erWeiMaTimeWordLab.textColor = K333333;
        erWeiMaTimeWordLab.text = @"候保二维码有效期：";
        [self.contentView addSubview:erWeiMaTimeWordLab];
        
        
        UILabel *erWeiMaTimeLab = [[UILabel alloc] init];
        erWeiMaTimeLab.frame = CGRectMake(erWeiMaTimeWordLab.right, buZhuJinWordLab.bottom+15, KWIDTH-39*2-12*9, 12);
        erWeiMaTimeLab.font = KFontPingFangSCRegular(12);
        erWeiMaTimeLab.textColor = K666666;
        erWeiMaTimeLab.text = @"";
        [self.contentView addSubview:erWeiMaTimeLab];
        self.erWeiMaTimeLab = erWeiMaTimeLab;
        
        //无忧质保期：
        
        UILabel *baoZhiQiWordLab = [[UILabel alloc] init];
        baoZhiQiWordLab.frame = CGRectMake(39, erWeiMaTimeWordLab.bottom+15, 12*6, 12);
        baoZhiQiWordLab.font = KFontPingFangSCRegular(12);
        baoZhiQiWordLab.textColor = K333333;
        baoZhiQiWordLab.text = @"无忧质保期：";
        [self.contentView addSubview:baoZhiQiWordLab];
        
        UILabel *baoZhiQiLab = [[UILabel alloc] init];
        baoZhiQiLab.frame = CGRectMake(baoZhiQiWordLab.right, erWeiMaTimeWordLab.bottom+15, KWIDTH-39*2-12*6, 12);
        baoZhiQiLab.font = KFontPingFangSCRegular(12);
        baoZhiQiLab.textColor = K666666;
        baoZhiQiLab.text = @"";
        [self.contentView addSubview:baoZhiQiLab];
        self.baoZhiQiLab = baoZhiQiLab;
        
        
        //候保工时费补贴期：
        UILabel *youXiaoQiWordLab = [[UILabel alloc] init];
        youXiaoQiWordLab.frame = CGRectMake(39, baoZhiQiWordLab.bottom+15, KWIDTH-39, 12);
        youXiaoQiWordLab.font = KFontPingFangSCRegular(12);
        youXiaoQiWordLab.textColor = [UIColor colorWithHexString:@"#F6A44B"];
        youXiaoQiWordLab.text = @"候保工时费补贴期：";
        [self.contentView addSubview:youXiaoQiWordLab];
        self.youXiaoQiWordLab = youXiaoQiWordLab;

        // 安装日期：
        UILabel *anZhuangTimeWordLab = [[UILabel alloc] init];
        anZhuangTimeWordLab.frame = CGRectMake(39, youXiaoQiWordLab.bottom+15, 12*5, 12);
        anZhuangTimeWordLab.font = KFontPingFangSCRegular(12);
        anZhuangTimeWordLab.textColor = K333333;
        anZhuangTimeWordLab.text = @"安装日期：";
        [self.contentView addSubview:anZhuangTimeWordLab];

        UILabel *anZhuangTimeLab = [[UILabel alloc] init];
        anZhuangTimeLab.frame = CGRectMake(nameWordLab.right, youXiaoQiWordLab.bottom+15, KWIDTH-39*2-12*5, 12);
        anZhuangTimeLab.font = KFontPingFangSCRegular(12);
        anZhuangTimeLab.textColor = K666666;
        anZhuangTimeLab.text = @"";
        [self.contentView addSubview:anZhuangTimeLab];
        self.anZhuangTimeLab = anZhuangTimeLab;
        
    }
    return self;
}

-(void)setModel:(GrantDetailContentModel *)model
{
    _model =model;
    if (_model) {
        
        self.nameContentLab.text = model.type_name;
        self.typeContentLab.text = model.attributeName; //配件型号
        self.buZhuJinContentLab.text =[NSString stringWithFormat:@"¥%@",model.compensationPrice];
        self.erWeiMaTimeLab.text =[NSString stringWithFormat:@"%@个月",model.install];
        self.baoZhiQiLab.text = [NSString stringWithFormat:@"%@个月",model.freeWarranty];
        self.youXiaoQiWordLab.text =[NSString stringWithFormat:@"候保工时费补贴期：%@个月",model.warranty];
        self.anZhuangTimeLab.text = model.installDate;

    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
