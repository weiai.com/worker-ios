//
//  ChanPinAnZhuangInfoCell.h
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrantDetailContentModel.h"


NS_ASSUME_NONNULL_BEGIN
typedef void(^heightHandle)(CGFloat height);

@interface ChanPinAnZhuangInfoCell : UITableViewCell

@property (nonatomic, strong) UIButton *zhanKaiBtn;
@property (nonatomic, strong) UIView *imageBackView;
@property (nonatomic, strong) GrantDetailContentModel *model;
@property (nonatomic, strong) heightHandle heightHandle;

@end

NS_ASSUME_NONNULL_END
