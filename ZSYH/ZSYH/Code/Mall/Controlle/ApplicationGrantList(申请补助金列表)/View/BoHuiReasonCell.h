//
//  BoHuiReasonCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/22.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BoHuiReasonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reasonLab;
@property(nonatomic,strong)NSString *boHuiYuanYinStr;

@end

NS_ASSUME_NONNULL_END
