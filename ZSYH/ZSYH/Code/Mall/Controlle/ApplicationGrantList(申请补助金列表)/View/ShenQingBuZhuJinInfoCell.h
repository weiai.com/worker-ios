//
//  ShenQingBuZhuJinInfoCell.h
//  FactorySale
//
//  Created by 李 on 2020/3/26.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrantDetailContentModel.h"
typedef void(^heightHandle)(CGFloat height);

NS_ASSUME_NONNULL_BEGIN

@interface ShenQingBuZhuJinInfoCell : UITableViewCell
@property (nonatomic, strong) UIView *imageBackView;
@property (nonatomic, strong) GrantDetailContentModel *model;
@property (nonatomic, strong) heightHandle heightHandle;

@end

NS_ASSUME_NONNULL_END
