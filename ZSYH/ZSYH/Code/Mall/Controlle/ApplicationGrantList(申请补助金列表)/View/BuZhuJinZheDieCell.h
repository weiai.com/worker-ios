//
//  BuZhuJinZheDieCell.h
//  FactorySale
//
//  Created by 李 on 2020/4/1.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuZhuJinZheDieCell : UITableViewCell
@property(nonatomic,strong)NSString *infoTitle;
@property(nonatomic,strong)UIButton *zhanKaiBtn;
@property(nonatomic,strong)UILabel *infoLab;

@end

NS_ASSUME_NONNULL_END
