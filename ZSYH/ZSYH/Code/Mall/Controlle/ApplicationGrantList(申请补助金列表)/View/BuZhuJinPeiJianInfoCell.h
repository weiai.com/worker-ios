//
//  BuZhuJinPeiJianInfoCell.h
//  FactorySale
//
//  Created by 李 on 2020/3/26.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrantDetailContentModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^heightHandle)(CGFloat height);
@interface BuZhuJinPeiJianInfoCell : UITableViewCell
@property(nonatomic,strong)UIButton *zhanKaiBtn;
@property(nonatomic,strong)GrantDetailContentModel *model;
@property(nonatomic,strong)heightHandle heightHandle;

@end

NS_ASSUME_NONNULL_END
