//
//  ApplicationGrantListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "ApplicationGrantListTableViewCell.h"

@implementation ApplicationGrantListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 185);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        //bottomV.layer.borderWidth = 1;
        //bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        //订单编号
        UILabel *orderTitLab = [[UILabel alloc] init];
        orderTitLab.frame = CGRectMake(10, 10, KWIDTH/2, 17);
        orderTitLab.font = KFontPingFangSCRegular(12);
        orderTitLab.textColor = K999999;
        [bottomV addSubview:orderTitLab];
        self.orderTitLab = orderTitLab;
        //订单状态
        UILabel *stateLab = [[UILabel alloc] init];
        stateLab.frame = CGRectMake(bottomV.right - 100, 10, 100, 17);
        stateLab.font = KFontPingFangSCRegular(12);
        stateLab.textColor = K999999;
        [bottomV addSubview:stateLab];
        self.stateLab = stateLab;
        //分割线
        UILabel *topLine = [[UILabel alloc] init];
        topLine.frame = CGRectMake(0, orderTitLab.bottom +7, KWIDTH, 1);
        topLine.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [bottomV addSubview:topLine];
        //配件名称 标题
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(10, topLine.bottom +7, 70, 20);
        pjmcTitLab.text = @"配件名称:";
        pjmcTitLab.font = KFontPingFangSCMedium(13);
        pjmcTitLab.textColor = K333333;
        [bottomV addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        //配件名称 内容
        UILabel *pjmcConLab = [[UILabel alloc] init];
        pjmcConLab.frame = CGRectMake(pjmcTitLab.right, topLine.bottom +7, KWIDTH-20-12-48-12-70-12, 20);
        //pjmcConLab.backgroundColor = [UIColor redColor];
        pjmcConLab.font = KFontPingFangSCMedium(13);
        pjmcConLab.textColor = K666666;
        [bottomV addSubview:pjmcConLab];
        self.pjmcConLab = pjmcConLab;
        //配件型号 标题
        UILabel *pjxhTitLab = [[UILabel alloc] init];
        pjxhTitLab.frame = CGRectMake(10, pjmcTitLab.bottom +2, 70, 20);
        pjxhTitLab.text = @"配件型号:";
        pjxhTitLab.font = KFontPingFangSCMedium(13);
        pjxhTitLab.textColor = K333333;
        [bottomV addSubview:pjxhTitLab];
        self.pjxhTitLab = pjxhTitLab;
        //配件型号 内容
        UILabel *pjxhConLab = [[UILabel alloc] init];
        pjxhConLab.frame = CGRectMake(pjxhTitLab.right, pjmcTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        //pjxhConLab.backgroundColor = [UIColor orangeColor];
        pjxhConLab.font = KFontPingFangSCMedium(13);
        pjxhConLab.textColor = K666666;
        [bottomV addSubview:pjxhConLab];
        self.pjxhConLab = pjxhConLab;
        //补助金额 标题
        UILabel *bzjeTitLab = [[UILabel alloc] init];
        bzjeTitLab.frame = CGRectMake(10, pjxhTitLab.bottom +2, 70, 20);
        bzjeTitLab.text = @"补助金额:";
        bzjeTitLab.font = KFontPingFangSCMedium(13);
        bzjeTitLab.textColor = K333333;
        [bottomV addSubview:bzjeTitLab];
        self.bzjeTitLab = bzjeTitLab;
        //补助金额 内容
        UILabel *bzjeConLab = [[UILabel alloc] init];
        bzjeConLab.frame = CGRectMake(pjmcTitLab.right, pjxhTitLab.bottom +2, KWIDTH-20-12-48-12-70-12-20, 20);
        //bzjeConLab.backgroundColor = [UIColor orangeColor];
        bzjeConLab.font = KFontPingFangSCMedium(13);
        bzjeConLab.textColor = K666666;
        [bottomV addSubview:bzjeConLab];
        self.bzjeConLab = bzjeConLab;
        //申请时间 标题
        UILabel *dateTitLab = [[UILabel alloc] init];
        dateTitLab.frame = CGRectMake(10, bzjeConLab.bottom +2, 70, 20);
        dateTitLab.text = @"申请时间:";
        dateTitLab.font = KFontPingFangSCMedium(13);
        dateTitLab.textColor = K333333;
        [bottomV addSubview:dateTitLab];
        self.dateTitLab = dateTitLab;
        //申请时间 内容
        UILabel *dateConLab = [[UILabel alloc] init];
        dateConLab.frame = CGRectMake(pjxhTitLab.right, bzjeConLab.bottom +2, KWIDTH-20-12-48-12-70-12-20, 20);
        //dateConLab.backgroundColor = [UIColor greenColor];
        dateConLab.font = KFontPingFangSCMedium(13);
        dateConLab.textColor = K666666;
        [bottomV addSubview:dateConLab];
        self.dateConLab = dateConLab;
        
        //分割线
        UILabel *downLine = [[UILabel alloc] init];
        downLine.frame = CGRectMake(0, dateTitLab.bottom + 16, KWIDTH, 1);
        downLine.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [bottomV addSubview:downLine];
        
        //故障描述 标题
        UILabel *gzmsTitLab = [[UILabel alloc] init];
        gzmsTitLab.frame = CGRectMake(10, downLine.bottom +10, 70, 20);
        gzmsTitLab.text = @"故障描述:";
        gzmsTitLab.font = KFontPingFangSCMedium(13);
        gzmsTitLab.textColor = K333333;
        [bottomV addSubview:gzmsTitLab];
        self.dateTitLab = gzmsTitLab;
        
        //故障描述 内容
        UILabel *gzmsConLab = [[UILabel alloc] init];
        gzmsConLab.frame = CGRectMake(gzmsTitLab.right, downLine.bottom +10, KWIDTH-20-10-70-10, 20);
        //gzmsConLab.backgroundColor = [UIColor greenColor];
        gzmsConLab.font = KFontPingFangSCMedium(13);
        gzmsConLab.textColor = K666666;
        [bottomV addSubview:gzmsConLab];
        self.gzmsConLab = gzmsConLab;
        
        //联系客服 按钮
        UIButton *ConCusSerBtn = [[UIButton alloc] initWithFrame:CGRectMake(bottomV.right-90, pjxhConLab.bottom +2, 80, 28)];
        ConCusSerBtn.backgroundColor = [UIColor orangeColor];
        //[ConCusSerBtn setImage:imgname(@"nowBtnBackimg5") forState:UIControlStateNormal];
        [ConCusSerBtn setTitle:@"联系客服" forState:UIControlStateNormal];
        [ConCusSerBtn setBackgroundColor:zhutiColor];
        ConCusSerBtn.titleLabel.font = FontSize(14);
        ConCusSerBtn.layer.cornerRadius = 14;
        ConCusSerBtn.layer.masksToBounds = YES;
        [ConCusSerBtn addTarget:self action:@selector(ContactCustomerService:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:ConCusSerBtn];
        self.ConCusSerBtn = ConCusSerBtn;
    }
    return self;
}

-(void)refasf:(ApplicationGrantListModel *)model{
    self.orderTitLab.text = [NSString stringWithFormat:@"%@%@",@"订单编号: ", model.id]; //订单编号
    if ([model.status isEqualToString:@"-1"]) {//状态 -1申请失败 0,1申请中， 2申请成功
        self.stateLab.text = @"补助金申请失败";
        self.ConCusSerBtn.hidden = NO;
        self.stateLab.textColor = [UIColor colorWithHexString:@"#FF0000"];
    } else if ([model.status isEqualToString:@"0"]) {
        self.stateLab.text = @"补助金申请中...";
        self.ConCusSerBtn.hidden = YES;
        self.stateLab.textColor = [UIColor colorWithHexString:@"#45B03A"];
    } else if ([model.status isEqualToString:@"1"]) {
        self.stateLab.text = @"补助金申请中...";
        self.ConCusSerBtn.hidden = YES;
        self.stateLab.textColor = [UIColor colorWithHexString:@"#45B03A"];
    } else if ([model.status isEqualToString:@"2"]) {
        self.stateLab.text = @"补助金申请结束";
        self.ConCusSerBtn.hidden = YES;
        self.stateLab.textColor = [UIColor colorWithHexString:@"#F7B23C"];
    }
    self.pjmcConLab.text = model.type_name; //配件名称
    self.pjxhConLab.text = model.attributeName; //配件型号
    self.bzjeConLab.text = [NSString stringWithFormat:@"%@%@",@"¥",model.compensationPrice]; //补助金额
    self.dateConLab.text = model.createTime; //申请时间

    self.gzmsConLab.text = model.faultDetails; //故障描述
}

//联系客服 按钮 点击事件
- (void)ContactCustomerService:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//联系客服
    }
    NSLog(@"您点击了 申请补助金列表 联系客服 按钮");
}

@end
