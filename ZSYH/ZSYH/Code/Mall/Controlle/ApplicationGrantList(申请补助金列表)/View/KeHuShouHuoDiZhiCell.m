//
//  KeHuShouHuoDiZhiCell.m
//  FactorySale
//
//  Created by 李 on 2020/3/31.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import "KeHuShouHuoDiZhiCell.h"

@implementation KeHuShouHuoDiZhiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(GrantDetailContentModel *)model
{
    _model =model;
    if (_model) {
         self.nameLab.text = model.username;
         self.phoneLab.text = model.userPhone;
         self.diZhiLab.text = model.userAddress;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
