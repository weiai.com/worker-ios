//
//  BuZhuJinZheDieCell.m
//  FactorySale
//
//  Created by 李 on 2020/4/1.
//  Copyright © 2020 主事丫环. All rights reserved.
//

#import "BuZhuJinZheDieCell.h"

@implementation BuZhuJinZheDieCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        //配件信息图片  文字
        UIImageView *infoImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 11, 4, 15)];
        infoImage.image = [UIImage imageNamed:@"矩形 704"];
        //infoImage.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:infoImage];
        
        UILabel *infoLab = [[UILabel alloc]initWithFrame:CGRectMake(36, 12, 100, 14)];
        infoLab.font = FontSize(14);
        infoLab.textColor = [UIColor colorWithHexString:@"#333333"];
        [self.contentView addSubview:infoLab];
        self.infoLab = infoLab;
        
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, infoLab.bottom+11, ScreenW, 0.5)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [self.contentView addSubview:lineView];
        
        UIButton *zhanKaiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        zhanKaiBtn.frame = CGRectMake(ScreenW-17-44, 0, 44, 44);
        [zhanKaiBtn setImage:[UIImage imageNamed:@"arrow_down_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:zhanKaiBtn];
        self.zhanKaiBtn = zhanKaiBtn;
    }
    return self;
}

-(void)setInfoTitle:(NSString *)infoTitle{
    _infoTitle = infoTitle;
    self.infoLab.text = _infoTitle;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
