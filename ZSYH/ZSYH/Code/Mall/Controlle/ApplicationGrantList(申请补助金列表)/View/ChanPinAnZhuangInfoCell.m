//
//  ChanPinAnZhuangInfoCell.m
//  ZSYH
//
//  Created by 李卓雄 on 2020/4/13.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "ChanPinAnZhuangInfoCell.h"
#import "SYBigImage.h"
@interface ChanPinAnZhuangInfoCell()
@property(nonatomic,strong)UILabel *xingMingLab;
@property(nonatomic,strong)UILabel *phoneLab;
@property(nonatomic,strong)UILabel *diZhiLab;
@property(nonatomic,strong)UILabel *feiYongLab;

@property(nonatomic,strong)UILabel *zhaoPianWordLab;

@end

@implementation ChanPinAnZhuangInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(20, 0, ScreenW, 0.5)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [self.contentView addSubview:lineView];
        
        //姓名
        UILabel *xingMingLab = [[UILabel alloc] init];
        xingMingLab.frame = CGRectMake(37, lineView.bottom+17, 60, 12);
        xingMingLab.textAlignment = NSTextAlignmentLeft;
        xingMingLab.text = @"";
        xingMingLab.font = KFontPingFangSCRegular(12);
        xingMingLab.textColor = K333333;
        [self.contentView addSubview:xingMingLab];
        self.xingMingLab = xingMingLab;
        
        //电话
        UILabel *phoneLab = [[UILabel alloc] init];
        phoneLab.frame = CGRectMake(xingMingLab.right, lineView.bottom+17, ScreenW-37-60-5, 12);
        phoneLab.textAlignment = NSTextAlignmentLeft;
        phoneLab.text = @"";
        phoneLab.font = KFontPingFangSCRegular(12);
        phoneLab.textColor = K666666;
        [self.contentView addSubview:phoneLab];
        self.phoneLab = phoneLab;
        
        //地址
        UILabel *diZhiWordLab = [[UILabel alloc] init];
        diZhiWordLab.frame = CGRectMake(37, xingMingLab.bottom+12, 12*3, 12);
        diZhiWordLab.textAlignment = NSTextAlignmentLeft;
        diZhiWordLab.text = @"地址：";
        diZhiWordLab.font = KFontPingFangSCRegular(12);
        diZhiWordLab.textColor = K333333;
        [self.contentView addSubview:diZhiWordLab];
        [self.contentView layoutIfNeeded];
        
        //地址 内容
        UILabel *diZhiLab = [[UILabel alloc] init];
        diZhiLab.textAlignment = NSTextAlignmentLeft;
        diZhiLab.text = @"";
        diZhiLab.numberOfLines =0;
        diZhiLab.font = KFontPingFangSCRegular(12);
        diZhiLab.textColor = K666666;
        [self.contentView addSubview:diZhiLab];
        self.diZhiLab = diZhiLab;
        
        [diZhiLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(xingMingLab.mas_bottom).mas_offset(12);
            make.left.equalTo(self.contentView.mas_left).mas_offset(71);
            make.right.equalTo(self.contentView.mas_right).mas_offset(-30);
            make.height.mas_greaterThanOrEqualTo(12);
        }];
        [self.contentView layoutIfNeeded];
    
        //费用
        UILabel *feiYongWordLab = [[UILabel alloc] init];
        feiYongWordLab.textAlignment = NSTextAlignmentLeft;
        feiYongWordLab.text = @"费用：";
        feiYongWordLab.font = KFontPingFangSCRegular(12);
        feiYongWordLab.textColor = K333333;
        [self.contentView addSubview:feiYongWordLab];
        [feiYongWordLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(diZhiLab.mas_bottom).mas_offset(12);
            make.left.equalTo(self.contentView.mas_left).mas_offset(37);
            make.width.mas_equalTo(36);
            make.height.mas_equalTo(12);
            
        }];
        
        UILabel *feiYongLab = [[UILabel alloc] init];
        feiYongLab.textAlignment = NSTextAlignmentLeft;
        feiYongLab.text = @"¥";
        feiYongLab.font = KFontPingFangSCRegular(12);
        feiYongLab.textColor = K666666;
        [self.contentView addSubview:feiYongLab];
        self.feiYongLab = feiYongLab;
        [self.contentView addSubview:feiYongLab];
        [feiYongLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(diZhiLab.mas_bottom).mas_offset(12);
            make.left.equalTo(feiYongWordLab.mas_right).mas_offset(0);
            make.width.mas_equalTo(ScreenW-71-13);
            make.height.mas_equalTo(12);
        }];
        
        //初始安装完工照片
        UILabel *zhaoPianWordLab = [[UILabel alloc] init];
        zhaoPianWordLab.textAlignment = NSTextAlignmentLeft;
        zhaoPianWordLab.text = @"初始安装完工照片";
        zhaoPianWordLab.font = KFontPingFangSCRegular(12);
        zhaoPianWordLab.textColor = K333333;
        [self.contentView addSubview:zhaoPianWordLab];
        [self.contentView addSubview:zhaoPianWordLab];
        [zhaoPianWordLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).mas_offset(37);
            make.top.equalTo(feiYongWordLab.mas_bottom).mas_offset(12);
            make.width.mas_equalTo(150);
            make.height.mas_equalTo(12);
        }];
        self.zhaoPianWordLab = zhaoPianWordLab;
    }
    return self;
}
-(void)setModel:(GrantDetailContentModel *)model
{
    _model =model;
    if (_model) {
        
        self.xingMingLab.text = model.user_name;
        self.phoneLab.text = model.user_phone;
        self.diZhiLab.text = model.user_address;
        
        self.feiYongLab.text =[NSString stringWithFormat:@"¥%@",model.cost];
        [self.contentView layoutSubviews];
        [self.contentView layoutIfNeeded];
        
//        NSString *imageStr = _model.imgUrl;
//        NSArray *array = [imageStr componentsSeparatedByString:@"|"];
//        NSArray * imageList = array;
//
//        for (int i = 0; i < imageList.count; i ++) {
//            UIImageView * button = [[UIImageView alloc]init];
//            [button setImage:[UIImage imageNamed:imageList[i]]];
//            //下面是九宫格的设计
//            NSInteger l = i % 3;
//            NSInteger h = i / 3;
//            NSInteger leftMagin = (ScreenW-80-58*3)/3;
//            NSInteger kMagin = 14;
//            CGFloat width = 58;
//            CGFloat height = 58;
//            //通过个数位置确定具体的frame
//            button.frame = CGRectMake(leftMagin + (leftMagin + width) * l,self.zhaoPianWordLab.bottom+14 + (kMagin + height) * h, width, height);
//            [button sd_setImageWithURL:[NSURL URLWithString:imageList[i]]];
//            SYBigImage * bigI = [[SYBigImage alloc]init];
//            button.userInteractionEnabled = YES;
//            [button addGestureRecognizer:bigI];
//            [self.contentView addSubview:button];
//
//            if (i== imageList.count-1) {
//
//                CGFloat height = button.height+button.frame.origin.y+10;
//                if (self.heightHandle) {
//                    self.heightHandle(height);
//                }
//            }
//        }
        
        //照片背景 View
        UIView *imageBackView = [[UIView alloc] init];
        imageBackView.frame = CGRectMake(32.5, self.zhaoPianWordLab.bottom + 5, KWIDTH-65, 75);
        [self.contentView addSubview:imageBackView];
        self.imageBackView = imageBackView;

        NSString *imagesStr = self.model.imgUrl;
        NSArray *imagearr = [imagesStr componentsSeparatedByString:@"|"];
        CGFloat wei = (KWIDTH-25-46-46)/4;

        if (imagearr.count > 0) {
            [self.imageBackView removeAllSubviews];
            for (int i = 0; i < imagearr.count; i++) {
                UIImageView *myimage = [[UIImageView alloc]init];
                [self.imageBackView addSubview:myimage];
                [myimage sd_setImageWithURL:[NSURL URLWithString:imagearr[i]] placeholderImage:defaultImg];
                myimage.frame = CGRectMake((wei+10)*(i % 4)+10, (wei + 10) * (i / 4)+5, wei, wei);
                myimage.userInteractionEnabled = YES;
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [myimage addGestureRecognizer:bigI];
                self.imageBackView.frame = CGRectMake(32.5, self.zhaoPianWordLab.bottom + 5, self.contentView.width - 65, (wei + 10) * (i / 4 + 1));
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
