//
//  CSShopViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSShopViewController.h"
#import "GoodListModel.h"
#import "CSgoodsViewController.h"
#import "HomeGoodsCell.h"
#import "CSshopModel.h"
#import "CSshopGoodsModel.h"
#import <AMapNaviKit/AMapNaviKit.h>
#import "CSshopSelePuViewController.h"
#import "QPLocationManager.h"
#import "CSshopSearchViewController.h"
//#import "SpeechSynthesizer.h"

@interface CSShopViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,AMapNaviCompositeManagerDelegate>

@property (nonatomic, strong) AMapNaviCompositeManager *compositeManager;
@property(nonatomic,strong)NSMutableArray *modelArr;
@property (weak, nonatomic) IBOutlet UIImageView *shopImage;
@property (weak, nonatomic) IBOutlet UILabel *shopName;
@property (weak, nonatomic) IBOutlet UILabel *shopAddress;
@property (weak, nonatomic) IBOutlet UICollectionView *mycollectionView;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,strong)CSshopModel *shopmodel;
@property (weak, nonatomic) IBOutlet UIButton *zonghebut;
@property(nonatomic,assign)NSInteger typepage;
@property(nonatomic,assign)UIButton *oldbut;
@property(nonatomic,strong)NSString *selsStr;//产品筛选

@end

@implementation CSShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    self.title = @"店铺信息";
    [self.rightbutton
     setImage:imgname(@"saosuo") forState:(UIControlStateNormal)];
    self.page = 1;
    [self showcollect];
    
    [self requestGoods];
    self.oldbut = self.zonghebut;
    self.typepage = 0;
    self.shopAddress.userInteractionEnabled = YES;
    UITapGestureRecognizer *gex = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapactiondddddd)];
    [self.shopAddress addGestureRecognizer:gex];
    // Do any additional setup after loading the view from its nib.
}

- (void)rightClick{
    CSshopSearchViewController *vc = [CSshopSearchViewController new];
    vc.idstr = _shopmodel.Id;
    [self.navigationController pushViewController:vc animated:YES];
}

// init
- (AMapNaviCompositeManager *)compositeManager {
    if (!_compositeManager) {
        _compositeManager = [[AMapNaviCompositeManager alloc] init];  // 初始化
        _compositeManager.delegate = self;  // 如果需要使用AMapNaviCompositeManagerDelegate的相关回调（如自定义语音、获取实时位置等），需要设置delegate
    }
    return _compositeManager;
}

-(void)tapactiondddddd{
    [[QPLocationManager sharedManager] getGps:^(NSString *province, NSString *city, NSString *area, NSString *street) {
        NSLog(@"province=%@,street=%@",province,street);
        [self gaodeSricnd];
    }];
}

-(void)gaodeSricnd{
    
    CGFloat longituded = [[USER_DEFAULT objectForKey:@"longitude"] floatValue];
    CGFloat latitude = [[USER_DEFAULT objectForKey:@"latitude"] floatValue];
    
    AMapNaviCompositeUserConfig *config = [[AMapNaviCompositeUserConfig alloc] init];
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeStart location:[AMapNaviPoint locationWithLatitude:latitude longitude:longituded] name:@"当前位置" POIId:@""];     //传入起点，并且带高德POIId
    //    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeWay location:[AMapNaviPoint locationWithLatitude:39.941823 longitude:116.426319] name:@"北京大学" POIId:@"B000A816R6"];            //传入途径点，并且带高德POIId
    
    CGFloat longituded33 = [_shopmodel.lon floatValue];
    CGFloat latitude33 = [_shopmodel.lat floatValue];
    
    [config setRoutePlanPOIType:AMapNaviRoutePlanPOITypeEnd location:[AMapNaviPoint locationWithLatitude:latitude33 longitude:longituded33] name:_shopmodel.shop_name POIId:nil];
    
    //传入终点，并且带高德POIId
    [self.compositeManager presentRoutePlanViewControllerWithOptions:config];
}

-(void)showcollect{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //layout.itemSize = CGSizeMake(KWIDTH/2 , 260);
    layout.itemSize = CGSizeMake(KWIDTH/2 - 15 , 240);
    //layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
    //layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    //layout.minimumLineSpacing = 0;
    //layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    //_mycollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight+32, KWIDTH, KHEIGHT-kNaviHeight-32) collectionViewLayout:layout];
    adjustInset(_mycollectionView);
    _mycollectionView.collectionViewLayout =layout;
    _mycollectionView.dataSource = self;
    _mycollectionView.delegate = self;
    _mycollectionView.backgroundColor =RGBA(237, 237, 237, 1);
    
    [_mycollectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
    [_mycollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
    _mycollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self requestGoods];
    }];
    
    _mycollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page += 1;
        [self requestGoods];
    }];
    [_mycollectionView reloadData];
}

-(void)requwithId:(NSInteger)ind{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *pagg = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNum"] = pagg;
    param[@"shopId"] = _shopid;
    
    if (ind == 2) {
        //类型
        param[@"typeId"] = _selsStr;
        
    }else if (ind == 1){
        //销量
        param[@"sales"] = @"1";
        
    }
    if (_page == 1) {
        [self.modelArr removeAllObjects];
    }
    
    [NetWorkTool POST:mailHomeseleapi param:param success:^(id dic) {
        KMyLog(@"++++++++++%@",[HFTools toJSONString:dic]);
        
        self.modelArr = [CSshopGoodsModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.mycollectionView reloadData];
        self->_page == 1 ?[self->_mycollectionView.mj_header endRefreshing]:[self->_mycollectionView.mj_footer endRefreshing];
    } other:^(id dic) {
        //[self->_mycollectionView.mj_footer endRefreshingWithNoMoreData];
        //[self->_mycollectionView.mj_header endRefreshing];
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (void)requestGoods {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"shopId"] = _shopid;
    NSString *pagg = [NSString stringWithFormat:@"%ld",(long)_page];
    
    param[@"pageNum"] = pagg;
    
    if (_page == 1) {
        [self.modelArr removeAllObjects];
    }
    
    kWeakSelf;
    [NetWorkTool POST:shopHome param:param success:^(id dic) {
        KMyLog(@"++++++++++%@",[HFTools toJSONString:dic]);
        weakSelf.shopmodel = [[CSshopModel alloc]init];
        [weakSelf.shopmodel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        if ([weakSelf.shopmodel.shop_image hasPrefix:@"http"]) {
            [weakSelf.shopImage sd_setImageWithURL:[NSURL URLWithString:weakSelf.shopmodel.shop_image] placeholderImage:defaultImg];
        } else {
            sdimg(self->_shopImage, weakSelf.shopmodel.shop_image);
        }
        self.shopName.text = weakSelf.shopmodel.shop_name;
        self.shopAddress.text = weakSelf.shopmodel.shop_address;
        
        NSDictionary *myarr = [dic objectForKey:@"data"];
        
        self.modelArr = [CSshopGoodsModel mj_objectArrayWithKeyValuesArray:myarr[@"products"]];
        
        [self.mycollectionView reloadData];
        self->_page == 1 ?[self->_mycollectionView.mj_header endRefreshing]:[self->_mycollectionView.mj_footer endRefreshing];
    } other:^(id dic) {
        [self->_mycollectionView.mj_footer endRefreshingWithNoMoreData];
        [self->_mycollectionView.mj_header endRefreshing];
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

#pragma mark - CollectionView -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];
    
    CSshopGoodsModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    
    if ([model.product_pic hasPrefix:@"http"]) {
        [cell.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    }else{
        sdimg(cell.goodsImage, model.product_pic);
    }
    
    cell.price.text = [NSString stringWithFormat:@"¥%.2f", [model.product_price floatValue]];
    cell.goodsTrpe.text = [NSString stringWithFormat:@"%@",model.product_price];
    //    if ([model.product_bel_type integerValue] == 1) {
    //        cell.goodsTrpe.text = @"候保产品";
    //        cell.fukuanLB.text = @"";
    //    }else{
    //        cell.goodsTrpe.text = @"";
    //        cell.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    //    }
    if ([model.channel integerValue] == 1) {
        cell.goodsTrpe.text = @"候保产品";
        //cell.fukuanLB.text = @"";
        cell.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    } else {
        cell.goodsTrpe.text = @"";
        cell.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    }
    cell.goodsname.text = [NSString stringWithFormat:@"%@",model.product_name];
    
    CGFloat ffff = [model.distance floatValue];
    if (ffff<100) {
        cell.distenceLb.text = [NSString stringWithFormat:@"%@米",model.distance];
    } else {
        //self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff];
        cell.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff / 1000];
    }
    if (strIsEmpty(model.distance)) {
        cell.distenceLb.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = model.Id;
    vc.isshop =@"shop";
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)zongheAction:(UIButton *)sender {
    [self.oldbut  setTitleColor:K666666 forState:(UIControlStateNormal)];
    
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    
    if (self.oldbut.tag == 777) {
        [self.oldbut setImage:imgname(@"seleIMage") forState:(UIControlStateNormal)];
    }
    sender.tag = 555;
    
    self.oldbut =sender;
    
    self.typepage = 0;
    self.page = 1;
    [self requwithId:0];
}

- (IBAction)saveAction:(UIButton *)sender {
    [self.oldbut  setTitleColor:K666666 forState:(UIControlStateNormal)];
    
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    
    if (self.oldbut.tag == 777) {
        [self.oldbut setImage:imgname(@"seleIMage") forState:(UIControlStateNormal)];
    }
    self.oldbut =sender;
    self.typepage = 1;
    self.page = 1;
    [self requwithId:1];
}

- (IBAction)seletocPrudent:(UIButton *)sender {
    [self.oldbut  setTitleColor:K666666 forState:(UIControlStateNormal)];
    
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [sender setImage:imgname(@"seleimge") forState:(UIControlStateNormal)];
    
    sender.tag = 777;
    self.oldbut =sender;
    self.typepage = 2;
    self.page = 1;
    
    CSshopSelePuViewController *vc = [[CSshopSelePuViewController alloc]init];
    kWeakSelf;
    vc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
        weakSelf.selsStr = str;
        [self requwithId:2];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 打电话
 */
- (IBAction)shopCallAction:(UIButton *)sender {
    [HFTools callMobilePhone:self.shopmodel.shop_phone];
}

@end
