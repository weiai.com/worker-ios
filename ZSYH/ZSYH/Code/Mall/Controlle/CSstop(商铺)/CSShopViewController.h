//
//  CSShopViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSShopViewController : BaseViewController
@property(nonatomic,strong)NSString *shopid;

@end

NS_ASSUME_NONNULL_END
