//
//  CSshopSelePuViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSshopSelePuViewController.h"
#import "CSproductTypeTableViewCell.h"
#import "CSnewMallViewController.h"
#import "CSmailPrucateModel.h"
@interface CSshopSelePuViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *leftTableview;
@property(nonatomic,strong)UITableView *centerTableview;
@property(nonatomic,strong)UITableView *rightterTableview;

@property(nonatomic,strong)NSMutableArray *leftDateSource;
@property(nonatomic,strong)NSMutableArray *centerDateSource;
@property(nonatomic,strong)NSMutableArray *rightDateSource;

@property(nonatomic,strong)CSmailPrucateModel *selemodel;

@end

@implementation CSshopSelePuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品筛选";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.leftDateSource = [NSMutableArray arrayWithCapacity:1];
    self.centerDateSource = [NSMutableArray arrayWithCapacity:1];
    self.rightDateSource = [NSMutableArray arrayWithCapacity:1];
//    [self.rightbutton setTitle:@"完成" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [self showui];
    [self requestleft];
    
}
-(void)requestleft{
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pId"] = @"0";
    [NetWorkTool POST:getfenlei param:mudic success:^(id dic) {
        KMyLog(@"%@",dic);
        for (NSMutableDictionary *mudoc in [dic objectForKey:@"data"]) {
            CSmailPrucateModel *model = [[CSmailPrucateModel alloc]init];
            model.isele = @"0";
            [model setValuesForKeysWithDictionary:mudoc];
            [self.leftDateSource addObject:model];
        }
        [self.leftTableview reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
- (void)rightClick{
    if (!self.selemodel) {
        ShowToastWithText(@"请选择配件品类");
        return;
    }
        if (self.myblock) {
            self.myblock(self.selemodel.typeName, self.selemodel.Id);
    
        }
        [self.navigationController popViewControllerAnimated:YES];
 
}

-(void)showui{
    CGFloat  leftright = 30;
    CGFloat  tabWith  = (KWIDTH - leftright - leftright-10)/3;
    
    self.leftTableview  = [[UITableView alloc]initWithFrame:CGRectMake(leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight-44) style:(UITableViewStylePlain)];
    _leftTableview.delegate = self;
    _leftTableview.dataSource = self;
    _leftTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_leftTableview];
    
    _leftTableview.backgroundColor = [UIColor whiteColor];
    self.centerTableview  = [[UITableView alloc]initWithFrame:CGRectMake(tabWith+5+leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight-44) style:(UITableViewStylePlain)];
    _centerTableview.delegate = self;
    _centerTableview.dataSource = self;
    [self.view addSubview:_centerTableview];
    _centerTableview.backgroundColor = [UIColor whiteColor];
    _centerTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.rightterTableview  = [[UITableView alloc]initWithFrame:CGRectMake(tabWith+tabWith+10+leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight-44) style:(UITableViewStylePlain)];
    _rightterTableview.delegate = self;
    _rightterTableview.dataSource = self;
    [self.view addSubview:_rightterTableview];
    _rightterTableview.backgroundColor = [UIColor whiteColor];
    _rightterTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    
    
    [self.leftTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
    [self.centerTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
    [self.rightterTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
    
    
    UILabel *biiii = [[UILabel alloc]initWithFrame:CGRectMake(0, KHEIGHT-44, KWIDTH, 44)];
    biiii.layer.borderColor = [UIColor colorWithHexString:@"#CBCBCB"].CGColor;
    biiii.layer.borderWidth = 1;
    [self.view addSubview:biiii];
 
    
    UIButton *leftButt = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftButt setTitleColor:K666666 forState:(UIControlStateNormal)];
    [leftButt setTitle:@"重置" forState:(UIControlStateNormal)];
    leftButt.frame = CGRectMake(0, 0, KWIDTH/2, 44);
    [leftButt addTarget:self action:@selector(leftbutttaxction:) forControlEvents:(UIControlEventTouchUpInside)];
    [biiii addSubview:leftButt];
    
    UIButton *righttButt = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [righttButt setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [righttButt setTitle:@"完成" forState:(UIControlStateNormal)];
    righttButt.frame = CGRectMake(KWIDTH/2, 0, KWIDTH/2, 44);
    [righttButt addTarget:self action:@selector(righttButtttaxction:) forControlEvents:(UIControlEventTouchUpInside)];
    [biiii addSubview:righttButt];
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH/2-0.5, 0, 1, 44)];
    line.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB"];
    [biiii addSubview:line];
    UILabel *lineleft = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 1, 44)];
    lineleft.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB"];
    [biiii addSubview:lineleft];
    UILabel *linetight = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-1, 0, 1, 44)];
    linetight.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB"];
    [biiii addSubview:linetight];
    
    UILabel *linetbottom = [[UILabel alloc]initWithFrame:CGRectMake(0, 43, 1, 44)];
    linetbottom.backgroundColor = [UIColor colorWithHexString:@"#CBCBCB"];
    [biiii addSubview:linetbottom];
    biiii.userInteractionEnabled = YES;
}
-(void)leftbutttaxction:(UIButton *)but{
    if (self.myblock) {
        self.myblock(@"", @"");
    }
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)righttButtttaxction:(UIButton *)but{
    if (self.myblock) {
        self.myblock(@"", @"");
    }
    if (self.myblock) {
        self.myblock(self.selemodel.typeName, self.selemodel.Id);
        
    }
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableview) {
        
        CSmailPrucateModel *model = [_leftDateSource safeObjectAtIndex:indexPath.row];
        self.selemodel = model;
        for (CSmailPrucateModel *mymodel in _leftDateSource) {
            mymodel.isele =@"0";
        }
        
        model.isele = @"1";
        [self.leftTableview reloadData];
        [self showcenterWithid:model.Id];
        
    }else if (tableView  == _centerTableview){
        
        CSmailPrucateModel *model = [_centerDateSource safeObjectAtIndex:indexPath.row];
        
        self.selemodel = model;
        
        for (CSmailPrucateModel *mymodel in _centerDateSource) {
            mymodel.isele =@"0";
        }
        
        model.isele = @"1";
        [self.centerTableview reloadData];
        
        [self showrightWithid:model.Id];
    }else{
        
        CSmailPrucateModel *model = [_rightDateSource safeObjectAtIndex:indexPath.row];
        self.selemodel = model;
        
        for (CSmailPrucateModel *mymodel in _rightDateSource) {
            mymodel.isele =@"0";
        }
        model.isele = @"1";
        [self.rightterTableview reloadData];
        //        if (self.myblock) {
        //            self.myblock(model.fault_name, model.parent_id);
        //        }
    }
    
}
-(void)showcenterWithid:(NSString *)str{
    [self.centerDateSource removeAllObjects];
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pid"] = str;
    [NetWorkTool POST:getfenleiWithID param:mudic success:^(id dic) {
        self.centerDateSource = [CSmailPrucateModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.centerTableview reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}
-(void)showrightWithid:(NSString *)str{
    [self.rightDateSource removeAllObjects];
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pid"] = str;
    [NetWorkTool POST:getfenleiWithID param:mudic success:^(id dic) {

        self.rightDateSource = [CSmailPrucateModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.rightterTableview reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableview) {
        return _leftDateSource.count;
        
    }else if (tableView  == _centerTableview){
        
        return _centerDateSource.count;
    }else{
        
        return _rightDateSource.count;
        
        
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSproductTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSproductTypeTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (tableView == _leftTableview) {
        
        CSmailPrucateModel *model = [_leftDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        }else{
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
            
        }
        
        
    }else if (tableView  == _centerTableview){
        
        CSmailPrucateModel *model = [_centerDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        }else{
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
            
        }
        
    }else{
        
        CSmailPrucateModel *model = [_rightDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        }else{
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
            
        }
        
    }
    
    
    
    return cell;
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
