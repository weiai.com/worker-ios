//
//  InstallRecordsListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "InstallRecordsListTableViewCell.h"

@implementation InstallRecordsListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 185);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        //bottomV.layer.borderWidth = 1;
        //bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        //订单编号
        UILabel *orderTitLab = [[UILabel alloc] init];
        orderTitLab.frame = CGRectMake(10, 10, KWIDTH-40, 17);
        //orderTitLab.backgroundColor = [UIColor cyanColor];
        orderTitLab.text = @"订单编号:12345678900987654321";
        orderTitLab.font = KFontPingFangSCRegular(12);
        orderTitLab.textColor = K999999;
        [bottomV addSubview:orderTitLab];
        self.orderTitLab = orderTitLab;
        //分割线
        UILabel *topLine = [[UILabel alloc] init];
        topLine.frame = CGRectMake(0, orderTitLab.bottom +7, KWIDTH, 1);
        topLine.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [bottomV addSubview:topLine];
        //用户姓名 标题
        UILabel *nameTitLab = [[UILabel alloc] init];
        nameTitLab.frame = CGRectMake(10, topLine.bottom +7, 70, 20);
        nameTitLab.text = @"用户姓名:";
        nameTitLab.font = KFontPingFangSCMedium(13);
        nameTitLab.textColor = K333333;
        [bottomV addSubview:nameTitLab];
        self.nameTitLab = nameTitLab;
        //用户姓名 内容
        UILabel *nameConLab = [[UILabel alloc] init];
        nameConLab.frame = CGRectMake(nameTitLab.right, topLine.bottom +7, KWIDTH-20-12-48-12-70-12, 20);
        //nameConLab.backgroundColor = [UIColor redColor];
        nameConLab.font = KFontPingFangSCMedium(13);
        nameConLab.textColor = [UIColor colorWithHexString:@"#45B03A"];
        [bottomV addSubview:nameConLab];
        self.nameConLab = nameConLab;
        //用户电话 标题
        UILabel *phoneTitLab = [[UILabel alloc] init];
        phoneTitLab.frame = CGRectMake(10, nameTitLab.bottom +2, 70, 20);
        phoneTitLab.text = @"用户电话:";
        phoneTitLab.font = KFontPingFangSCMedium(13);
        phoneTitLab.textColor = K333333;
        [bottomV addSubview:phoneTitLab];
        self.phoneTitLab = phoneTitLab;
        //用户电话 内容
        UILabel *phoneConLab = [[UILabel alloc] init];
        phoneConLab.frame = CGRectMake(phoneTitLab.right, nameTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        //phoneConLab.backgroundColor = [UIColor orangeColor];
        phoneConLab.font = KFontPingFangSCMedium(13);
        phoneConLab.textColor = K666666;
        [bottomV addSubview:phoneConLab];
        self.phoneConLab = phoneConLab;
        //配件名称 标题
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(10, phoneConLab.bottom +2, 70, 20);
        pjmcTitLab.text = @"配件名称:";
        pjmcTitLab.font = KFontPingFangSCMedium(13);
        pjmcTitLab.textColor = K333333;
        [bottomV addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        //配件名称 内容
        UILabel *pjmcConLab = [[UILabel alloc] init];
        pjmcConLab.frame = CGRectMake(pjmcTitLab.right, phoneTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        //pjmcConLab.backgroundColor = [UIColor orangeColor];
        pjmcConLab.font = KFontPingFangSCMedium(13);
        pjmcConLab.textColor = K666666;
        [bottomV addSubview:pjmcConLab];
        self.pjmcConLab = pjmcConLab;
        //配件型号 标题
        UILabel *pjxhTitLab = [[UILabel alloc] init];
        pjxhTitLab.frame = CGRectMake(10, pjmcTitLab.bottom +2, 70, 20);
        pjxhTitLab.text = @"配件型号:";
        pjxhTitLab.font = KFontPingFangSCMedium(13);
        pjxhTitLab.textColor = K333333;
        [bottomV addSubview:pjxhTitLab];
        self.pjxhTitLab = pjxhTitLab;
        //配件型号 内容
        UILabel *pjxhConLab = [[UILabel alloc] init];
        pjxhConLab.frame = CGRectMake(pjxhTitLab.right, pjmcTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        //pjxhConLab.backgroundColor = [UIColor greenColor];
        pjxhConLab.font = KFontPingFangSCMedium(13);
        pjxhConLab.textColor = K666666;
        [bottomV addSubview:pjxhConLab];
        self.pjxhConLab = pjxhConLab;
        
        //分割线
        UILabel *downLine = [[UILabel alloc] init];
        downLine.frame = CGRectMake(0, pjxhTitLab.bottom + 16, KWIDTH, 1);
        downLine.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
        [bottomV addSubview:downLine];
        
        //地址 logo
        UIImageView *addressImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, downLine.bottom + 10, 12, 15)];
        //addressImg.backgroundColor = [UIColor orangeColor];
        [addressImg setImage:imgname(@"addressImg")];
        [bottomV addSubview:addressImg];
        
        //地址 内容
        UILabel *addConLab = [[UILabel alloc] init];
        addConLab.frame = CGRectMake(addressImg.right + 10, downLine.bottom +7.5, KWIDTH-20-15-10-10-10, 20);
        //addConLab.backgroundColor = [UIColor cyanColor];
        addConLab.font = KFontPingFangSCMedium(13);
        addConLab.textColor = K666666;
        [bottomV addSubview:addConLab];
        self.addConLab = addConLab;
        
        //填写预约信息 按钮
        //        UIButton *txyyxxBtn = [[UIButton alloc] init];
        //        txyyxxBtn.frame = CGRectMake(bottomV.right - 116, fwdzTitLab.bottom +10, 112, 28);
        //        [txyyxxBtn setImage:[UIImage imageNamed:@"tianxieyuyuexinxi"] forState:UIControlStateNormal];
        //        [txyyxxBtn addTarget:self action:@selector(tianxieyuyuexinxi:) forControlEvents:UIControlEventTouchUpInside];
        //        [bottomV addSubview:txyyxxBtn];
        //        self.txyyxxBtn = txyyxxBtn;
        //
        //        //生成检测报告 按钮
        //        UIButton *scjcbgBtn = [[UIButton alloc] init];
        //        scjcbgBtn.frame = CGRectMake(bottomV.right - 116, fwdzTitLab.bottom +10, 112, 28);
        //        [scjcbgBtn setImage:[UIImage imageNamed:@"shengchengjiancebaogao"] forState:UIControlStateNormal];
        //        [scjcbgBtn addTarget:self action:@selector(shengchengjiancebaogao:) forControlEvents:UIControlEventTouchUpInside];
        //        [bottomV addSubview:scjcbgBtn];
        //        self.scjcbgBtn = scjcbgBtn;
    }
    return self;
}

-(void)refasf:(InstallRecordsListModel *)model{
    self.orderTitLab.text = [NSString stringWithFormat:@"%@%@",@"订单编号: ", model.id]; //订单编号
    self.nameConLab.text = model.user_name; //用户姓名
    self.phoneConLab.text = model.user_phone; //用户电话
    self.pjmcConLab.text = model.type_name; //配件名称
    self.pjxhConLab.text = model.attributeName; //配件型号
    self.addConLab.text = model.user_address; //用户地址
}

//生成检测报告 按钮 点击事件
- (void)shengchengjiancebaogao:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//生成检测报告
    }
    NSLog(@"您点击了 检测单 生成检测报告 按钮");
}

//填写预约信息 按钮 点击事件
- (void)tianxieyuyuexinxi:(UIButton *)button {
    if (self.myblock) {
        self.myblock(1, @"");//填写预约信息
    }
    NSLog(@"您点击了 检测单 填写预约信息 按钮");
}

@end
