//
//  InstallRecordsListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstallRecordsListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstallRecordsListTableViewCell : UITableViewCell

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *orderTitLab;       //订单编号
//@property (nonatomic, strong) UILabel *stateLab;      //状态

@property (nonatomic, strong) UILabel *nameTitLab;    //用户姓名 标题
@property (nonatomic, strong) UILabel *nameConLab;    //用户姓名 内容

@property (nonatomic, strong) UILabel *phoneTitLab;    //用户电话 标题
@property (nonatomic, strong) UILabel *phoneConLab;    //用户电话 内容

@property (nonatomic, strong) UILabel *pjmcTitLab;    //配件名称 标题
@property (nonatomic, strong) UILabel *pjmcConLab;    //配件名称 内容

@property (nonatomic, strong) UILabel *pjxhTitLab;    //配件型号 标题
@property (nonatomic, strong) UILabel *pjxhConLab;    //配件型号 内容

@property (nonatomic, strong) UILabel *addConLab;     //用户地址 内容

//@property (nonatomic, strong) UIButton *txyyxxBtn;    //填写预约信息 按钮
//@property (nonatomic, strong) UIButton *scjcbgBtn;    //生成检测报告 按钮

@property (nonatomic, strong) UIButton *cancleOrBtn;  //取消预订 按钮

-(void)refasf:(InstallRecordsListModel *)model;
@end

NS_ASSUME_NONNULL_END
