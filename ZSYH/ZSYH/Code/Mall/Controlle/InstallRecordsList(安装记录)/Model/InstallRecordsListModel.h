//
//  InstallRecordsListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstallRecordsListModel : NSObject

@property (nonatomic, copy) NSString *user_address;
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *quality;
@property (nonatomic, copy) NSString *attributeName;
@property (nonatomic, copy) NSString *parts_id;

@property (nonatomic, copy) NSString *imgUrl;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *validity;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *type_name;

@property (nonatomic, strong) NSString *user_phone;
@property (nonatomic, strong) NSString *subsidyMoney;
@property (nonatomic, strong) NSString *fault_common;
@property (nonatomic, strong) NSString *user_name;
@property (nonatomic, strong) NSString *warranty;

@property (nonatomic, strong) NSString *create_time;
@property (nonatomic, strong) NSString *rep_user_id;
@property (nonatomic, strong) NSString *update_time;
@property (nonatomic, strong) NSString *cost;

@end

NS_ASSUME_NONNULL_END
