//
//  InstallRecordsListViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstallRecordsListViewController : BaseViewController
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;

@end

NS_ASSUME_NONNULL_END
