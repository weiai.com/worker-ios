//
//  NewScanCodeInstallViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewScanCodeInstallViewController : BaseViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;
@property (nonatomic, strong) NSString *qrcodeStr;

@property (nonatomic, strong) NSDictionary *dataDic;

@property (nonatomic, copy) void(^imageArrayChangeBlock)(NSArray *imageArray);

@property(nonatomic,copy)void (^myblock)(NSString *str);

@end

NS_ASSUME_NONNULL_END
