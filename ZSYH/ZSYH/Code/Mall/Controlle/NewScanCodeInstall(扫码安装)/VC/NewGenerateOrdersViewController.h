//
//  NewGenerateOrdersViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/15.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewGenerateOrdersViewController : UIViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;
@property (nonatomic, strong) NSString *dataId;
@property (nonatomic, strong) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
