//
//  NewGenerateOrdersViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/15.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "NewGenerateOrdersViewController.h"
#import "SYBigImage.h"
#import "NewGenerateOrdersViewController.h"

@interface NewGenerateOrdersViewController ()<UITextViewDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *topWhiteView;
@property (nonatomic, strong) UIView *downWhiteView;

@property (nonatomic, strong) UILabel *pjmcConLab; //配件名称
@property (nonatomic, strong) UILabel *pjxhConLab; //配件型号
@property (nonatomic, strong) UILabel *bzjeConLab; //补助金额

@property (nonatomic, strong) UILabel *phoneConLab;
@property (nonatomic, strong) UITextView *addrConLab; //地址输入框
@property (nonatomic, strong) UILabel *placeLab;
@property (nonatomic, strong) UILabel *costConLab;

@property (nonatomic, strong) UIView *addView;
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) NSDictionary *mydic;
@property (nonatomic, strong) NSMutableArray *imageArr;

@property (nonatomic, strong) UIView *bgViewsecOne;
@property (nonatomic, strong) UIView *lookInstallView;
@property (nonatomic, strong) UIView *imageBackView;

@property (nonatomic, strong) UILabel *nameConLab; //用户名称
@property (nonatomic, strong) UIButton *leftbutton;

@end

@implementation NewGenerateOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成订单";
    //[self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        
    [self newScanCodeInstallTopUI];
    [self requestDown];
    [self setupNavViewAction];
}

- (void)setupNavViewAction{
    
    _leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    _leftbutton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_leftbutton setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    [_leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [_leftbutton setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
    [_leftbutton addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_leftbutton];
}

-(void)leftBtnClick {
    if ([self.titleStr isEqualToString:@"扫码安装"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//获取扫描安装详情 数据请求
- (void)requestDown {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = self.dataId;
    
    [NetWorkTool POST:getScanOrderById param:param success:^(id dic) {
        //隐藏下面内容
        NSString *errorCode = dic[@"errorCode"];
        if ([errorCode isEqualToString:@"0"]) {
            
        }
        NSLog(@"-------- ^^^ ________ %@", dic);
        
        self.mydic = [dic objectForKey:@"data"];
        [self newScanCodeInstallTopUI];
        [self newScanCodeInstallDownUI];

    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)leftClick
{
    //判断是从哪个vc push过来的
    NSArray *vcsArray = self.navigationController.viewControllers;
    NSInteger vcCount = vcsArray.count;
    UIViewController *lastVC = vcsArray[vcCount-2];
    if ([lastVC isKindOfClass:[NewGenerateOrdersViewController class]]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)newScanCodeInstallTopUI {
    
    [self.view addSubview:self.scrollView];
    
    UIImageView *topImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KNavHeight, KWIDTH, 95)];
    [topImgView setImage:imgname(@"scanCodeTopImg")];
    [self.scrollView addSubview:topImgView];
    
    UIImageView *topWhiteView = [[UIImageView alloc] initWithFrame:CGRectMake(30, KNavHeight + 20, KWIDTH - 60, 208)];
    topWhiteView.backgroundColor = [UIColor whiteColor];
    topWhiteView.layer.cornerRadius = 10;
    topWhiteView.layer.masksToBounds = YES;
    [self.scrollView addSubview:topWhiteView];
    self.topWhiteView = topWhiteView;
    //配件信息
    UILabel *pjInfoTitLab = [[UILabel alloc] initWithFrame:CGRectMake((KWIDTH-60)/2-30, 10, 60, 20)];
    //pjInfoTitLab.backgroundColor = [UIColor cyanColor];
    pjInfoTitLab.text = @"配件信息";
    pjInfoTitLab.textColor = K333333;
    pjInfoTitLab.textAlignment = NSTextAlignmentCenter;
    pjInfoTitLab.font = FontSize(14);
    [topWhiteView addSubview:pjInfoTitLab];
    //配件名称 标题
    UILabel *pjmcTitLab = [[UILabel alloc] init];
    pjmcTitLab.frame = CGRectMake(10, pjInfoTitLab.bottom +15, 70, 20);
    //pjmcTitLab.backgroundColor = [UIColor orangeColor];
    pjmcTitLab.text = @"配件名称:";
    pjmcTitLab.font = KFontPingFangSCMedium(13);
    pjmcTitLab.textColor = K333333;
    [topWhiteView addSubview:pjmcTitLab];
    //配件名称 内容
    UILabel *pjmcConLab = [[UILabel alloc] init];
    pjmcConLab.frame = CGRectMake(pjmcTitLab.right, pjInfoTitLab.bottom +15, KWIDTH-20-60-70, 20);
    //pjmcConLab.backgroundColor = [UIColor redColor];
    pjmcConLab.font = KFontPingFangSCMedium(13);
    pjmcConLab.textColor = K666666;
    pjmcConLab.text = [self.mydic objectForKey:@"type_name"];
    [topWhiteView addSubview:pjmcConLab];
    self.pjmcConLab = pjmcConLab;
    //配件型号 标题
    UILabel *pjxhTitLab = [[UILabel alloc] init];
    pjxhTitLab.frame = CGRectMake(10, pjmcTitLab.bottom +5, 70, 20);
    //pjxhTitLab.backgroundColor = [UIColor greenColor];
    pjxhTitLab.text = @"配件型号:";
    pjxhTitLab.font = KFontPingFangSCMedium(13);
    pjxhTitLab.textColor = K333333;
    [topWhiteView addSubview:pjxhTitLab];
    //配件型号 内容
    UILabel *pjxhConLab = [[UILabel alloc] init];
    pjxhConLab.frame = CGRectMake(pjxhTitLab.right, pjmcTitLab.bottom +5, KWIDTH-20-60-70, 20);
    //pjxhConLab.backgroundColor = [UIColor orangeColor];
    pjxhConLab.font = KFontPingFangSCMedium(13);
    pjxhConLab.textColor = K666666;
    pjxhConLab.text = [self.mydic objectForKey:@"attributeName"];
    [topWhiteView addSubview:pjxhConLab];
    self.pjxhConLab = pjxhConLab;
    //工时费补助金 标题
    UILabel *bzjeTitLab = [[UILabel alloc] init];
    bzjeTitLab.frame = CGRectMake(10, pjxhTitLab.bottom +5, 100, 20);
    //bzjeTitLab.backgroundColor = [UIColor redColor];
    bzjeTitLab.text = @"工时费补助金:";
    bzjeTitLab.font = KFontPingFangSCMedium(13);
    bzjeTitLab.textColor = K333333;
    [topWhiteView addSubview:bzjeTitLab];
    //工时费补助金 内容
    UILabel *bzjeConLab = [[UILabel alloc] init];
    bzjeConLab.frame = CGRectMake(bzjeTitLab.right, pjxhTitLab.bottom +5, KWIDTH-20-60-100, 20);
    //bzjeConLab.backgroundColor = [UIColor orangeColor];
    bzjeConLab.font = KFontPingFangSCMedium(13);
    bzjeConLab.textColor = K666666;
    bzjeConLab.text = [NSString stringWithFormat:@"%@%@", @"¥",[self.mydic objectForKey:@"subsidyMoney"]];
    [topWhiteView addSubview:bzjeConLab];
    //    self.bzjeConLab = bzjeConLab;
    //候保二维码有效期 标题
    UILabel *dateTitLab = [[UILabel alloc] init];
    dateTitLab.frame = CGRectMake(10, bzjeConLab.bottom +5, 110, 20);
    //dateTitLab.backgroundColor = [UIColor cyanColor];
    dateTitLab.text = @"候保二维码有效期:";
    dateTitLab.font = KFontPingFangSCMedium(13);
    dateTitLab.textColor = K333333;
    [topWhiteView addSubview:dateTitLab];
    //    self.dateTitLab = dateTitLab;
    //候保二维码有效期 内容
    UILabel *dateConLab = [[UILabel alloc] init];
    dateConLab.frame = CGRectMake(dateTitLab.right, bzjeConLab.bottom +5, KWIDTH-20-60-110, 20);
    //dateConLab.backgroundColor = [UIColor greenColor];
    dateConLab.font = KFontPingFangSCMedium(13);
    dateConLab.textColor = K666666;
    //dateConLab.text = [NSString stringWithFormat:@"%@%@%@",self.accessAddModel.productionDate,@"至",self.accessAddModel.termValidity];
    dateConLab.text = [NSString stringWithFormat:@"%@",[self.mydic objectForKey:@"validity"]];
    [topWhiteView addSubview:dateConLab];
    //self.dateConLab = dateConLab;
    //无忧质保期 标题
    UILabel *zbqTitLab = [[UILabel alloc] init];
    zbqTitLab.frame = CGRectMake(10, dateTitLab.bottom +5, 90, 20);
    //zbqTitLab.backgroundColor = [UIColor redColor];
    zbqTitLab.text = @"无忧质保期:";
    zbqTitLab.font = KFontPingFangSCMedium(13);
    zbqTitLab.textColor = K333333;
    [topWhiteView addSubview:zbqTitLab];
    //self.zbqTitLab = zbqTitLab;
    //无忧质保期 内容
    UILabel *zbqConLab = [[UILabel alloc] init];
    zbqConLab.frame = CGRectMake(zbqTitLab.right, dateTitLab.bottom +5, KWIDTH-20-60-90, 20);
    //zbqConLab.backgroundColor = [UIColor orangeColor];
    zbqConLab.font = KFontPingFangSCMedium(13);
    zbqConLab.textColor = K666666;
    //zbqConLab.text = [NSString stringWithFormat:@"%@%@", self.accessAddModel.freeWarranty, @"个月"];
    zbqConLab.text = [NSString stringWithFormat:@"%@%@", [self.mydic objectForKey:@"quality"], @"个月"];
    [topWhiteView addSubview:zbqConLab];
    //self.zbqConLab = zbqConLab;
    
    //候保工时费补贴期 标题
    UILabel *btqTitLab = [[UILabel alloc] init];
    btqTitLab.frame = CGRectMake(10, zbqTitLab.bottom +5, 120, 20);
    //btqTitLab.backgroundColor = [UIColor cyanColor];
    btqTitLab.text = @"候保工时费补贴期:";
    btqTitLab.font = KFontPingFangSCMedium(13);
    btqTitLab.textColor = [UIColor colorWithHexString:@"#F6A44B"];
    [topWhiteView addSubview:btqTitLab];
    //self.btqTitLab = btqTitLab;
    //候保工时费补贴期 内容
    UILabel *btqConLab = [[UILabel alloc] init];
    btqConLab.frame = CGRectMake(btqTitLab.right, zbqTitLab.bottom +5, KWIDTH-20-60-120, 20);
    //btqConLab.backgroundColor = [UIColor greenColor];
    btqConLab.font = KFontPingFangSCMedium(13);
    btqConLab.textColor = [UIColor colorWithHexString:@"#F6A44B"];
    //btqConLab.text = [NSString stringWithFormat:@"%@%@",self.accessAddModel.warranty,@"个月"];
    btqConLab.text = [NSString stringWithFormat:@"%@%@",[self.mydic objectForKey:@"warranty"],@"个月"];
    [topWhiteView addSubview:btqConLab];
    //self.btqConLab = btqConLab;
    
    //安装日期 标题
    UILabel *insDateTitLab = [[UILabel alloc] init];
    insDateTitLab.frame = CGRectMake(10, btqTitLab.bottom +5, 70, 20);
    //insDateTitLab.backgroundColor = [UIColor cyanColor];
    insDateTitLab.text = @"安装日期:";
    insDateTitLab.font = KFontPingFangSCMedium(13);
    insDateTitLab.textColor = K333333;
    [topWhiteView addSubview:insDateTitLab];
    //self.insDateTitLab = insDateTitLab;
    //安装日期 内容
    UILabel *insDateConLab = [[UILabel alloc] init];
    insDateConLab.frame = CGRectMake(insDateTitLab.right, btqTitLab.bottom +5, KWIDTH-20-60-70, 20);
    //insDateConLab.backgroundColor = [UIColor greenColor];
    insDateConLab.font = KFontPingFangSCMedium(13);
    insDateConLab.textColor = K666666;
    insDateConLab.text = [NSString stringWithFormat:@"%@",[self.mydic objectForKey:@"create_time"]];
    [topWhiteView addSubview:insDateConLab];
    //self.insDateConLab = insDateConLab;
    
    self.topWhiteView.frame = CGRectMake(30, KNavHeight + 20, KWIDTH - 60,insDateTitLab.bottom+10);
}

//newGetScanOrderById
- (void)newScanCodeInstallDownUI {
    
    [self.view addSubview:self.scrollView];
    
    UIView *downWhiteView = [[UIView alloc] initWithFrame:CGRectMake(0, self.topWhiteView.bottom+10, KWIDTH, 294)];
    downWhiteView.backgroundColor = [UIColor whiteColor];
    downWhiteView.layer.cornerRadius = 10;
    downWhiteView.layer.masksToBounds = YES;
    [self.scrollView addSubview:downWhiteView];
    self.downWhiteView = downWhiteView;
    //用户安装信息
    UILabel *insUserTitLab = [[UILabel alloc] initWithFrame:CGRectMake(KWIDTH/2-45, 16.5, 90, 20)];
    insUserTitLab.text = @"安装用户信息";
    insUserTitLab.textColor = K333333;
    insUserTitLab.textAlignment = NSTextAlignmentCenter;
    insUserTitLab.font = FontSize(14);
    [downWhiteView addSubview:insUserTitLab];
    
    //分割线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, insUserTitLab.bottom + 14.5, KWIDTH, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [downWhiteView addSubview:line];
    
    //用户姓名 内容
    UILabel *nameConLab = [[UILabel alloc] init];
    nameConLab.frame = CGRectMake(32.5, line.bottom +15, 100, 20);
    //nameConLab.backgroundColor = [UIColor orangeColor];
    nameConLab.font = KFontPingFangSCMedium(13);
    nameConLab.text = [self.mydic objectForKey:@"user_name"];
    [downWhiteView addSubview:nameConLab];
    self.nameConLab = nameConLab;
    //用户手机号 内容
    UILabel *phoneConLab = [[UILabel alloc] init];
    phoneConLab.frame = CGRectMake(nameConLab.right, line.bottom +15, KWIDTH-65-100, 20);
    //phoneConLab.backgroundColor = [UIColor redColor];
    phoneConLab.font = KFontPingFangSCMedium(13);
    phoneConLab.textColor = K666666;
    phoneConLab.text = [self.mydic objectForKey:@"user_phone"];
    [downWhiteView addSubview:phoneConLab];
    self.phoneConLab = phoneConLab;
    //安装地址 标题
    UILabel *addrTitLab = [[UILabel alloc] init];
    addrTitLab.frame = CGRectMake(32.5, phoneConLab.bottom +10, 50, 20);
    //addrTitLab.backgroundColor = [UIColor redColor];
    addrTitLab.text = @"地址";
    addrTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:addrTitLab];
    //安装地址 内容
    UITextView *addrConLab = [[UITextView alloc] init];
    addrConLab.frame = CGRectMake(addrTitLab.right, phoneConLab.bottom +5, KWIDTH-65-50, 45);
    //addrConLab.backgroundColor = [UIColor orangeColor];
    addrConLab.font = KFontPingFangSCMedium(13);
    addrConLab.textColor = K666666;
    addrConLab.editable = NO;
    addrConLab.text = [self.mydic objectForKey:@"user_address"];
    [downWhiteView addSubview:addrConLab];
    self.addrConLab = addrConLab;
    //费用 标题
    UILabel *costTitLab = [[UILabel alloc] init];
    costTitLab.frame = CGRectMake(32.5, addrConLab.bottom +5, 50, 20);
    //costTitLab.backgroundColor = [UIColor cyanColor];
    costTitLab.text = @"费用";
    costTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:costTitLab];
    //费用 内容
    UILabel *costConLab = [[UILabel alloc] init];
    costConLab.frame = CGRectMake(costTitLab.right, addrConLab.bottom +5, KWIDTH-65-50, 20);
    //costConLab.backgroundColor = [UIColor greenColor];
    costConLab.font = KFontPingFangSCMedium(13);
    costConLab.textColor = K666666;
    costConLab.text = [NSString stringWithFormat:@"%@%@", @"¥",[self.mydic objectForKey:@"cost"]];
    [downWhiteView addSubview:costConLab];
    self.costConLab = costConLab;

    //上传初始安装完工照片 标题
    UILabel *photoTitLab = [[UILabel alloc] init];
    photoTitLab.frame = CGRectMake(32.5, costTitLab.bottom +5, KWIDTH/2, 20);
    //photoTitLab.backgroundColor = [UIColor redColor];
    photoTitLab.text = @"初始安装完工照片";
    photoTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:photoTitLab];
    
    //照片背景 View
    UIView *imageBackView = [[UIView alloc] init];
    imageBackView.frame = CGRectMake(32.5, photoTitLab.bottom + 5, KWIDTH-65, 75);
    [downWhiteView addSubview:imageBackView];
    self.imageBackView = imageBackView;

    NSString *imagesStr = [_mydic objectForKey:@"imgUrl"];
    NSArray *imagearr = [imagesStr componentsSeparatedByString:@"|"];
    CGFloat wei = (self.scrollView.frame.size.width - 25-46-46)/4;

    if (imagearr.count > 0) {
        [self.imageBackView removeAllSubviews];
        for (int i = 0; i < imagearr.count; i++) {
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.imageBackView addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:imagearr[i]] placeholderImage:defaultImg];
            myimage.frame = CGRectMake((wei+10)*(i % 4)+10, (wei + 10) * (i / 4)+5, wei, wei);
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
            self.imageBackView.frame = CGRectMake(32.5, photoTitLab.bottom + 10, downWhiteView.width - 65, (wei + 10) * (i / 4 + 1));
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT+75);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
