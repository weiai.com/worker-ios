//
//  NewScanCodeInstallViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2020/4/12.
//  Copyright © 2020 魏堰青. All rights reserved.
//

#import "NewScanCodeInstallViewController.h"
#import "SYBigImage.h"
#import "XWScanImage.h"
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "NewGenerateOrdersViewController.h"

@interface NewScanCodeInstallViewController ()<UITextViewDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *topWhiteView;
@property (nonatomic, strong) UIView *downWhiteView;

@property (nonatomic, strong) UILabel *pjmcConLab; //配件名称
@property (nonatomic, strong) UILabel *pjxhConLab; //配件型号
@property (nonatomic, strong) UILabel *bzjeConLab; //补助金额

@property (nonatomic, strong) UITextField *nameConTF;
@property (nonatomic, strong) UITextField *phoneConTF;
@property (nonatomic, strong) UITextView *addrConTV; //地址输入框
@property (nonatomic, strong) UILabel *placeLab;
@property (nonatomic, strong) UITextField *costConTF;

@property (nonatomic, strong) UIView *addView;
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) NSDictionary *mydic;
@property (nonatomic, strong) NSMutableArray *imageArr;

@property (nonatomic, strong) UIView *bgViewsecOne;
@property (nonatomic, strong) UIView *lookInstallView;
@property (nonatomic, strong) UIImageView *imageV;
@property (nonatomic, strong) UILabel *tipsLab;

@end

@implementation NewScanCodeInstallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"扫码安装";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"addpicimage");
    [self.imageArr addObject:_myimage];
    
    [self newScanCodeInstallTopUI];
    //[self loadContent];
    
    [self shwoBgviewsec];
    
    //字符串转化为date
    //    NSString *birthdayStr = @"2020-04-15";
    //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
    //    NSDate *birthdayDate = [dateFormatter dateFromString:birthdayStr];
    //KMyLog(@"--------- %@", birthdayDate);
    
    //    if ([self.accessAddModel.anTime isEqualToString:@""] || self.accessAddModel.anTime.length > 0) {
    //        [self alreadyInstallView];
    //        [self.imageV setImage:imgname(@"alreadyInstallImg")];
    //        self.tipsLab.text = @"该配件已安装过,请勿重复安装哦~";
    //    } else {
    //        [self newScanCodeInstallDownUI];
    //    }
    
    [self compareDate:self.accessAddModel.termValidity withDate:self.accessAddModel.NewTime];
    //    NSString *a = self.accessAddModel.termValidity;
    //    NSString *b = self.accessAddModel.NewTime;
    //    [self compareDate:a withDate:b];
    //    KMyLog(@"termValidity --- %@------------NewTime ---%@",self.accessAddModel.termValidity, self.accessAddModel.NewTime);
    // Do any additional setup after loading the view.
}

//比较两个日期的大小
- (NSInteger)compareDate:(NSString*)aDate withDate:(NSString*)bDate
{
    NSInteger aa;
    NSDateFormatter *dateformater = [[NSDateFormatter alloc] init];
    [dateformater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dta = [[NSDate alloc] init];
    NSDate *dtb = [[NSDate alloc] init];
    dta = [dateformater dateFromString:aDate];
    dtb = [dateformater dateFromString:bDate];
    NSComparisonResult result = [dta compare:dtb];
    if (result == NSOrderedAscending){
        //newTime比anTime大
        aa=1;
        KMyLog(@"newTime(当前日期)大于termValidity(有效期)");
        [self alreadyInstallView];
        [self.imageV setImage:imgname(@"expiredImg")];
        self.tipsLab.text = @"此配件已过二维码有效期~";
    }else {
        //newTime比anTime大
        aa=-1;
        KMyLog(@"newTime(当前日期)小于termValidity(有效期)");
        if ([self.accessAddModel.anTime isEqualToString:@""]) {
            [self newScanCodeInstallDownUI];
        } else {
            [self alreadyInstallView];
            [self.imageV setImage:imgname(@"alreadyInstallImg")];
            self.tipsLab.text = @"该配件已安装过,请勿重复安装哦~";
        }
    }
    
    //    NSComparisonResult result = [self.accessAddModel.termValidity compare:self.accessAddModel.NewTime];
    //    if (result == NSOrderedSame){
    //        //相等  aa=0
    //        KMyLog(@"b等于a");
    //    } else if (result==NSOrderedAscending){
    //        //bDate比aDate大
    //        aa=1;
    //        KMyLog(@"b大于a");
    //        [self alreadyInstallView];
    //        [self.imageV setImage:imgname(@"expiredImg")];
    //        self.tipsLab.text = @"此配件已过二维码有效期~";
    //    } else if (result == NSOrderedDescending){
    //        //bDate比aDate小
    //        aa=-1;
    //        KMyLog(@"b小于a");
    //        if ([self.accessAddModel.anTime isEqualToString:@""] || self.accessAddModel.anTime.length > 0) {
    //            [self alreadyInstallView];
    //            [self.imageV setImage:imgname(@"alreadyInstallImg")];
    //            self.tipsLab.text = @"该配件已安装过,请勿重复安装哦~";
    //        } else {
    //            [self newScanCodeInstallDownUI];
    //        }
    //    }
    return aa;
}

//该配件已安装过,请勿重复安装
- (void)alreadyInstallView {
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(74, self.topWhiteView.bottom + 42, KWIDTH - 74*2, 176.5)];
    //[imageV setImage:imgname(@"alreadyInstallImg")];
    [self.scrollView addSubview:imageV];
    self.imageV = imageV;
    
    UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(74, imageV.bottom + 39, KWIDTH - 84*2, 20)];
    //tipsLab.text = @"该配件已安装过,请勿重复安装哦~";
    tipsLab.font = FontSize(13);
    tipsLab.textColor = [UIColor colorWithHexString:@"#B67F30"];
    tipsLab.textAlignment = NSTextAlignmentCenter;
    [self.scrollView addSubview:tipsLab];
    self.tipsLab = tipsLab;
}

- (void)newScanCodeInstallTopUI {
    
    [self.view addSubview:self.scrollView];
    
    UIImageView *topImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, KNavHeight, KWIDTH, 95)];
    [topImgView setImage:imgname(@"scanCodeTopImg")];
    [self.scrollView addSubview:topImgView];
    
    UIImageView *topWhiteView = [[UIImageView alloc] initWithFrame:CGRectMake(30, KNavHeight + 20, KWIDTH - 60, 208)];
    topWhiteView.backgroundColor = [UIColor whiteColor];
    topWhiteView.layer.cornerRadius = 10;
    topWhiteView.layer.masksToBounds = YES;
    [self.scrollView addSubview:topWhiteView];
    self.topWhiteView = topWhiteView;
    //配件信息
    UILabel *pjInfoTitLab = [[UILabel alloc] initWithFrame:CGRectMake((KWIDTH-60)/2-30, 10, 60, 20)];
    //pjInfoTitLab.backgroundColor = [UIColor cyanColor];
    pjInfoTitLab.text = @"配件信息";
    pjInfoTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
    pjInfoTitLab.textAlignment = NSTextAlignmentCenter;
    pjInfoTitLab.font = KFontPingFangSCMedium(14);
    [topWhiteView addSubview:pjInfoTitLab];
    //配件名称 标题
    UILabel *pjmcTitLab = [[UILabel alloc] init];
    pjmcTitLab.frame = CGRectMake(10, pjInfoTitLab.bottom +15, 70, 20);
    //pjmcTitLab.backgroundColor = [UIColor orangeColor];
    pjmcTitLab.text = @"配件名称:";
    pjmcTitLab.font = KFontPingFangSCMedium(13);
    pjmcTitLab.textColor = K333333;
    [topWhiteView addSubview:pjmcTitLab];
    //配件名称 内容
    UILabel *pjmcConLab = [[UILabel alloc] init];
    pjmcConLab.frame = CGRectMake(pjmcTitLab.right, pjInfoTitLab.bottom +15, KWIDTH-20-60-70, 20);
    //pjmcConLab.backgroundColor = [UIColor redColor];
    pjmcConLab.font = KFontPingFangSCMedium(13);
    pjmcConLab.textColor = K666666;
    pjmcConLab.text = self.accessAddModel.type_name;
    [topWhiteView addSubview:pjmcConLab];
    self.pjmcConLab = pjmcConLab;
    //配件型号 标题
    UILabel *pjxhTitLab = [[UILabel alloc] init];
    pjxhTitLab.frame = CGRectMake(10, pjmcTitLab.bottom +5, 70, 20);
    //pjxhTitLab.backgroundColor = [UIColor greenColor];
    pjxhTitLab.text = @"配件型号:";
    pjxhTitLab.font = KFontPingFangSCMedium(13);
    pjxhTitLab.textColor = K333333;
    [topWhiteView addSubview:pjxhTitLab];
    //配件型号 内容
    UILabel *pjxhConLab = [[UILabel alloc] init];
    pjxhConLab.frame = CGRectMake(pjxhTitLab.right, pjmcTitLab.bottom +5, KWIDTH-20-60-70, 20);
    //pjxhConLab.backgroundColor = [UIColor orangeColor];
    pjxhConLab.font = KFontPingFangSCMedium(13);
    pjxhConLab.textColor = K666666;
    pjxhConLab.text = self.accessAddModel.attributeName;
    [topWhiteView addSubview:pjxhConLab];
    self.pjxhConLab = pjxhConLab;
    //工时费补助金 标题
    UILabel *bzjeTitLab = [[UILabel alloc] init];
    bzjeTitLab.frame = CGRectMake(10, pjxhTitLab.bottom +5, 100, 20);
    //bzjeTitLab.backgroundColor = [UIColor redColor];
    bzjeTitLab.text = @"工时费补助金:";
    bzjeTitLab.font = KFontPingFangSCMedium(13);
    bzjeTitLab.textColor = K333333;
    [topWhiteView addSubview:bzjeTitLab];
    //工时费补助金 内容
    UILabel *bzjeConLab = [[UILabel alloc] init];
    bzjeConLab.frame = CGRectMake(bzjeTitLab.right, pjxhTitLab.bottom +5, KWIDTH-20-60-100, 20);
    //bzjeConLab.backgroundColor = [UIColor orangeColor];
    bzjeConLab.font = KFontPingFangSCMedium(13);
    bzjeConLab.textColor = K666666;
    bzjeConLab.text = [NSString stringWithFormat:@"%@%@", @"¥",self.accessAddModel.compensationPrice];
    [topWhiteView addSubview:bzjeConLab];
    //    self.bzjeConLab = bzjeConLab;
    //候保二维码有效期 标题
    UILabel *dateTitLab = [[UILabel alloc] init];
    dateTitLab.frame = CGRectMake(10, bzjeConLab.bottom +5, 110, 20);
    //dateTitLab.backgroundColor = [UIColor cyanColor];
    dateTitLab.text = @"候保二维码有效期:";
    dateTitLab.font = KFontPingFangSCMedium(13);
    dateTitLab.textColor = K333333;
    [topWhiteView addSubview:dateTitLab];
    //    self.dateTitLab = dateTitLab;
    //候保二维码有效期 内容
    UILabel *dateConLab = [[UILabel alloc] init];
    dateConLab.frame = CGRectMake(dateTitLab.right, bzjeConLab.bottom +5, KWIDTH-20-60-110, 20);
    //dateConLab.backgroundColor = [UIColor greenColor];
    dateConLab.font = KFontPingFangSCMedium(13);
    dateConLab.textColor = K666666;
    dateConLab.text = [NSString stringWithFormat:@"%@%@%@",self.accessAddModel.productionDate,@"至",self.accessAddModel.termValidity];
    [topWhiteView addSubview:dateConLab];
    //self.dateConLab = dateConLab;
    //无忧质保期 标题
    UILabel *zbqTitLab = [[UILabel alloc] init];
    zbqTitLab.frame = CGRectMake(10, dateTitLab.bottom +5, 90, 20);
    //zbqTitLab.backgroundColor = [UIColor redColor];
    zbqTitLab.text = @"无忧质保期:";
    zbqTitLab.font = KFontPingFangSCMedium(13);
    zbqTitLab.textColor = K333333;
    [topWhiteView addSubview:zbqTitLab];
    //self.zbqTitLab = zbqTitLab;
    //无忧质保期 内容
    UILabel *zbqConLab = [[UILabel alloc] init];
    zbqConLab.frame = CGRectMake(zbqTitLab.right, dateTitLab.bottom +5, KWIDTH-20-60-90, 20);
    //zbqConLab.backgroundColor = [UIColor orangeColor];
    zbqConLab.font = KFontPingFangSCMedium(13);
    zbqConLab.textColor = K666666;
    zbqConLab.text = [NSString stringWithFormat:@"%@%@", self.accessAddModel.freeWarranty, @"个月"];
    [topWhiteView addSubview:zbqConLab];
    //self.zbqConLab = zbqConLab;
    
    //候保工时费补贴期 标题
    UILabel *btqTitLab = [[UILabel alloc] init];
    btqTitLab.frame = CGRectMake(10, zbqTitLab.bottom +5, 120, 20);
    //btqTitLab.backgroundColor = [UIColor cyanColor];
    btqTitLab.text = @"候保工时费补贴期:";
    btqTitLab.font = KFontPingFangSCMedium(13);
    btqTitLab.textColor = [UIColor colorWithHexString:@"#F6A44B"];
    [topWhiteView addSubview:btqTitLab];
    //self.btqTitLab = btqTitLab;
    //候保工时费补贴期 内容
    UILabel *btqConLab = [[UILabel alloc] init];
    btqConLab.frame = CGRectMake(btqTitLab.right, zbqTitLab.bottom +5, KWIDTH-20-60-120, 20);
    //btqConLab.backgroundColor = [UIColor greenColor];
    btqConLab.font = KFontPingFangSCMedium(13);
    btqConLab.textColor = [UIColor colorWithHexString:@"#F6A44B"];
    btqConLab.text = [NSString stringWithFormat:@"%@%@",self.accessAddModel.warranty,@"个月"];
    [topWhiteView addSubview:btqConLab];
    //self.btqConLab = btqConLab;
}

//newGetScanOrderById
- (void)newScanCodeInstallDownUI {
    
    [self.view addSubview:self.scrollView];
    
    UIView *downWhiteView = [[UIView alloc] initWithFrame:CGRectMake(15, self.topWhiteView.bottom+10, KWIDTH - 30, 335+13)];
    downWhiteView.backgroundColor = [UIColor whiteColor];
    downWhiteView.layer.cornerRadius = 10;
    downWhiteView.layer.masksToBounds = YES;
    [self.scrollView addSubview:downWhiteView];
    self.downWhiteView = downWhiteView;
    //用户安装信息
    UILabel *insUserTitLab = [[UILabel alloc] initWithFrame:CGRectMake((KWIDTH-30)/2-45, 10, 90, 20)];
    //insUserTitLab.backgroundColor = [UIColor cyanColor];
    insUserTitLab.text = @"安装用户信息";
    insUserTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
    insUserTitLab.textAlignment = NSTextAlignmentCenter;
    insUserTitLab.font = KFontPingFangSCMedium(14);
    [downWhiteView addSubview:insUserTitLab];
    //用户姓名 标题
    NSMutableAttributedString *nameStr = [NSString getAttributedStringWithOriginalString:@"* 用户姓名" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor redColor] changeFont:FontSize(14)];
    UILabel *nameTitLab = [[UILabel alloc] init];
    nameTitLab.frame = CGRectMake(10, insUserTitLab.bottom +15, 100, 20);
    //nameTitLab.backgroundColor = [UIColor orangeColor];
    nameTitLab.attributedText = nameStr;
    nameTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:nameTitLab];
    //用户姓名 内容
    UITextField *nameConTF = [[UITextField alloc] init];
    nameConTF.frame = CGRectMake(nameTitLab.right, insUserTitLab.bottom +10, KWIDTH-20-30-100, 30);
    nameConTF.placeholder = @"请输入用户姓名";
    //nameConTF.backgroundColor = [UIColor redColor];
    nameConTF.font = KFontPingFangSCMedium(13);
    nameConTF.textColor = K666666;
    [downWhiteView addSubview:nameConTF];
    self.nameConTF = nameConTF;
    //分割线
    UILabel *lineOne = [[UILabel alloc] initWithFrame:CGRectMake(10, nameTitLab.bottom + 10, downWhiteView.width - 20, 1)];
    lineOne.backgroundColor = [UIColor colorWithHexString:@"#F2F1F6"];
    [downWhiteView addSubview:lineOne];
    //用户手机号 标题
    NSMutableAttributedString *phoneStr = [NSString getAttributedStringWithOriginalString:@"* 用户手机号" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *phoneTitLab = [[UILabel alloc] init];
    phoneTitLab.frame = CGRectMake(10, lineOne.bottom +10, 100, 20);
    //phoneTitLab.backgroundColor = [UIColor orangeColor];
    phoneTitLab.attributedText = phoneStr;
    phoneTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:phoneTitLab];
    //用户手机号 内容
    UITextField *phoneConTF = [[UITextField alloc] init];
    phoneConTF.frame = CGRectMake(phoneTitLab.right, lineOne.bottom +5, KWIDTH-20-30-100, 30);
    //phoneConTF.backgroundColor = [UIColor cyanColor];
    phoneConTF.placeholder = @"请输入用户手机号";
    phoneConTF.keyboardType = UIKeyboardTypeNumberPad;
    phoneConTF.font = KFontPingFangSCMedium(13);
    phoneConTF.textColor = K666666;
    [downWhiteView addSubview:phoneConTF];
    self.phoneConTF = phoneConTF;
    //分割线
    UILabel *lineTwo = [[UILabel alloc] initWithFrame:CGRectMake(10, phoneTitLab.bottom + 10, downWhiteView.width - 20, 1)];
    lineTwo.backgroundColor = [UIColor colorWithHexString:@"#F2F1F6"];
    [downWhiteView addSubview:lineTwo];
    //安装地址 标题
    NSMutableAttributedString *addressStr = [NSString getAttributedStringWithOriginalString:@"* 安装地址" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *addrTitLab = [[UILabel alloc] init];
    addrTitLab.frame = CGRectMake(10, lineTwo.bottom +10, 100, 20);
    //addrTitLab.backgroundColor = [UIColor redColor];
    addrTitLab.attributedText = addressStr;
    addrTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:addrTitLab];
    //安装地址 内容
    UITextView *addrConTV = [[UITextView alloc] init];
    addrConTV.frame = CGRectMake(addrTitLab.right, lineTwo.bottom +10, KWIDTH-20-30-100, 40);
    //addrConTV.backgroundColor = [UIColor orangeColor];
    addrConTV.font = KFontPingFangSCMedium(13);
    addrConTV.textColor = K666666;
    addrConTV.delegate = self;
    [downWhiteView addSubview:addrConTV];
    self.addrConTV = addrConTV;
    //占位文字
    UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, KWIDTH, 14)];
    placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    placeLab.text = @"请输入用户地址";
    placeLab.font = KFontPingFangSCMedium(13);
    self.placeLab = placeLab;
    [self.addrConTV addSubview:placeLab];
    //分割线
    UILabel *lineThr = [[UILabel alloc] initWithFrame:CGRectMake(10, addrConTV.bottom + 10, downWhiteView.width - 20, 1)];
    lineThr.backgroundColor = [UIColor colorWithHexString:@"#F2F1F6"];
    [downWhiteView addSubview:lineThr];
    //费用 标题
    //NSMutableAttributedString *costStr = [NSString getAttributedStringWithOriginalString:@"* 费用:" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *costTitLab = [[UILabel alloc] init];
    costTitLab.frame = CGRectMake(10, lineThr.bottom +10, 100, 20);
    //costTitLab.backgroundColor = [UIColor cyanColor];
    costTitLab.text = @"费用";
    costTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:costTitLab];
    //self.costTitLab = costTitLab;
    //费用 内容
    UITextField *costConTF = [[UITextField alloc] init];
    costConTF.frame = CGRectMake(costTitLab.right, lineThr.bottom +5, KWIDTH-20-30-100, 30);
    //costConTF.backgroundColor = [UIColor greenColor];
    costConTF.placeholder = @"请输入安装费用";
    costConTF.keyboardType = UIKeyboardTypeNumberPad;
    costConTF.font = KFontPingFangSCMedium(13);
    costConTF.textColor = K666666;
    [downWhiteView addSubview:costConTF];
    self.costConTF = costConTF;
    
    //分割线
    UILabel *lineFou = [[UILabel alloc] initWithFrame:CGRectMake(10, costTitLab.bottom + 10, downWhiteView.width - 20, 1)];
    lineFou.backgroundColor = [UIColor colorWithHexString:@"#F2F1F6"];
    [downWhiteView addSubview:lineFou];
    
    //上传初始安装完工照片 标题
    NSMutableAttributedString *photoStr = [NSString getAttributedStringWithOriginalString:@"* 上传初始安装完工照片" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *photoTitLab = [[UILabel alloc] init];
    photoTitLab.frame = CGRectMake(10, lineFou.bottom +5, KWIDTH/2, 20);
    //photoTitLab.backgroundColor = [UIColor redColor];
    photoTitLab.attributedText = photoStr;
    photoTitLab.font = KFontPingFangSCMedium(13);
    [downWhiteView addSubview:photoTitLab];
    //self.photoTitLab = photoTitLab;
    
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(10, photoTitLab.bottom + 5, KWIDTH-20-30, 93)];
    [downWhiteView addSubview: self.addView];
    [self showinageWirhArr:_imageArr];
    
    CGRect ffff = self.addView.frame;
    ffff.size.height = self.addView.height;
    self.addView.frame = ffff;
    
    UIButton *submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(KWIDTH/2-75, _downWhiteView.bottom + 34.5, 150, 36)];
    submitBtn.backgroundColor = zhutiColor;
    submitBtn.layer.cornerRadius = 18;
    submitBtn.layer.masksToBounds = YES;
    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:submitBtn];
    
    self.phoneConTF.delegate = self;
    [self.phoneConTF addTarget:self action:@selector(phonetextFieldDidChange:)  forControlEvents:UIControlEventEditingChanged]; //手机号限制11位
    
}

//加载数据
- (void)loadContent {
    if (self.accessAddModel.imageArray.count > 0) {
        _imageArr = self.accessAddModel.imageArray;
    } else {
        UIImageView *addImageView = [[UIImageView alloc] initWithImage:imgname(@"addpicimage")];
        [self.accessAddModel.imageArray addObject:addImageView];
        _imageArr = self.accessAddModel.imageArray;
    }
    [self showinageWirhArr:_imageArr];
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind] removeFromSuperview];
    NSLog(@"删除%ld图片", ind);
    [_imageArr removeObjectAtIndex:ind];
    [self showinageWirhArr:_imageArr];
    if (self.imageArrayChangeBlock) {
        self.imageArrayChangeBlock(_imageArr);
    }
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    NSLog(@"输出一下数组 %@", self.imageArr);
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    
    CGFloat www = (KWIDTH-30-20-30)/4;
    
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];//
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];//
        [image removeAllSubviews];
        image.frame = CGRectMake((www+10)*i, 5, www, www);
        [self.addView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        if ([self.accessAddModel.deleButHidden isEqualToString:@"1"]) {
            deleBut.hidden = YES;
        }else{
            deleBut.hidden = NO;
        }
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 4) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            }else{
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        }else{
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

//判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

//手机号码 限制输入11位
- (void)phonetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 11;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([textView isFirstResponder]) {
        if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
            return NO;
        }
        //if ([self hasEmoji:text] || [self stringContainsEmoji:text]){
        //return NO;
        //}
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.placeLab.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length < 1) {
        self.placeLab.hidden = NO;
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT+200);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/**
 弹出框的背景图 厂家订单 推送成功
 */
-(void)shwoBgviewsec{
    self.bgViewsecOne = [[UIView alloc]init];
    self.bgViewsecOne.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsecOne.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecOne addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(181);
        make.left.offset(38);
        make.right.offset(-38);
    }];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(15, 27, KWIDTH-76-30, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentLeft;
    UpLable.text = @"温馨提示";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(15, UpLable.bottom+10, KWIDTH-76-30, 63)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(13);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"候保产品二维码，属于在线记忆追溯码，产品\n候保码只能安装一次，未实际更换安装，请勿\n确认，以免失去您的候保保障，您确认安装吗？";
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.numberOfLines = 0;
    
    UIButton *leftBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftBut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
    [leftBut setTitle:@"取消" forState:(UIControlStateNormal)];
    leftBut.titleLabel.textColor = K999999;
    leftBut.layer.masksToBounds = YES;
    leftBut.layer.cornerRadius = 4;
    [leftBut addTarget:self action:@selector(leftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftBut];
    [leftBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.left.offset(36);
        make.width.offset(116);
        make.bottom.offset(-18);
    }];
    
    UIButton *rightBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [rightBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [rightBut setTitle:@"确认" forState:(UIControlStateNormal)];
    rightBut.titleLabel.textColor = [UIColor colorWithHexString:@"#70BE68"];
    rightBut.layer.masksToBounds = YES;
    rightBut.layer.cornerRadius = 4;
    [rightBut addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:rightBut];
    [rightBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.right.offset(-36);
        make.width.offset(116);
        make.bottom.offset(-18);
    }];
}

-(void)leftBtnAction:(UIButton *)but{
    [_bgViewsecOne removeFromSuperview];
    [singlTool shareSingTool].needReasfh = YES;
    //[self.navigationController popToRootViewControllerAnimated:YES];
}

//提交按钮 点击事件处理
- (void)submitBtnAction:(UIButton *)button {
    NSLog(@"提交");
    
    if (strIsEmpty(self.nameConTF.text)) {
        ShowToastWithText(@"请输入用户姓名");
        return;
    }
    //去除字符串中空格
    self.phoneConTF.text = [self.phoneConTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:self.phoneConTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    if (strIsEmpty(self.phoneConTF.text)) {
        ShowToastWithText(@"请输入用户手机号");
        return;
    }
    if (strIsEmpty(self.addrConTV.text)) {
        ShowToastWithText(@"请输入用户地址");
        return;
    }
//    if (strIsEmpty(self.costConTF.text)) {
//        ShowToastWithText(@"请输入安装费用");
//        return;
//    }
    
    UIImageView *myyy = [self.imageArr lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }
    
    if (_imageArr.count>0) {
        [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecOne];
    } else {
        ShowToastWithText(@"请添加初始安装完工照片");
        [self.imageArr addObject:self.myimage];
    }
}

-(void)rightBtnAction:(UIButton *)but{
    
    NSLog(@"按钮点击...");
    but.enabled = NO;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        but.enabled = YES;
    });
    
    if (_imageArr.count > 0) {
        NSMutableArray *arr = [NSMutableArray array];
        //[_imageArr removeLastObject];
        for (UIImage *image in _imageArr) {
            NSMutableArray *imageArray = [NSMutableArray array];
            [imageArray addObject:image];
            [NetWorkTool UploadPicWithUrl:invoiceUploadImg param:nil key:@"imgUrl" image:imageArray withSuccess:^(id dic) {
                if ([dic[@"status"] intValue] == 0) {
                    [arr addObject:dic[@"data"]];
                }
                if (arr.count == self->_imageArr.count) {
                    NSString *faultImgStr = [arr componentsJoinedByString:@"|"];
                    [self submit:faultImgStr];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }else{
        //没有图片
        [self submit:@""];
    }
}

//扫码申请补助金 提交申请 数据请求  （JSON格式传参）
-(void)submit:(NSString *)imgs {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"partsId"] = self.accessAddModel.partsId;
    param[@"qrcode"] = self.accessAddModel.qrcode;
    param[@"userName"] = self.nameConTF.text;
    param[@"userPhone"] = self.phoneConTF.text;
    param[@"userAddress"] = self.addrConTV.text;
    
    param[@"faultCommon"] = @"";
    param[@"cost"] = self.costConTF.text;
    param[@"validity"] = [NSString stringWithFormat:@"%@%@%@",self.accessAddModel.productionDate,@" 至 ",self.accessAddModel.termValidity];
    param[@"quality"] = self.accessAddModel.quantity;
    param[@"warranty"] = self.accessAddModel.warranty;
    
    param[@"subsidyMoney"] = self.accessAddModel.compensationPrice;
    param[@"imgUrl"] = imgs;
    
    //NSString *json = [HFTools toJSONString:param];
    //NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    //allDic[@""] = json;
    
    [NetWorkTool POST:createScanOrderIos param:param success:^(id dic) {
        //隐藏下面内容
        NSString *errorCode = dic[@"errorCode"];
        if ([errorCode isEqualToString:@"0"]) {
            
        }
        NSLog(@"-------- ^^^ ________ %@", dic);
        
        [self->_bgViewsecOne removeFromSuperview];
        //[self.navigationController popToRootViewControllerAnimated:YES];
        
        NewGenerateOrdersViewController *vc = [[NewGenerateOrdersViewController alloc] init];
        vc.accessAddModel = self.accessAddModel;
        vc.titleStr = @"扫码安装";
        vc.dataId = dic[@"data"];
        [self.navigationController pushViewController:vc animated:YES];
        
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
