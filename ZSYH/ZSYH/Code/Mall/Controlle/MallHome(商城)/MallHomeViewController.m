//
//  MallHomeViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "MallHomeViewController.h"
#import "SGPagingView.h"
#import "MallBannerViewController.h"
#import "CSMallTitleModel.h"
#import "CSMallHomeViewController.h"
#import "CSgoodsViewController.h"
#import "AddCommentController.h"
#import "CSpayViewController.h"
#import "CSSearchViewController.h"

@interface MallHomeViewController ()<SGPageTitleViewDelegate, UITextFieldDelegate,SGPageContentScrollViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;
@property(nonatomic,strong)NSMutableArray *titArr;
@end

@implementation MallHomeViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].index = 1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setTitle:@"首页" forState:(UIControlStateNormal)];
    [self.leftbutton setImage:imgname(@"") forState:(UIControlStateNormal)];
    UITextField *search = [[UITextField alloc]init];
    [search createLeftViewImage:@"sousuo1"];
    search.delegate = self;
    self.navigationItem.titleView = search;
    search.returnKeyType = UIReturnKeySearch;
    search.backgroundColor = [UIColor colorWithHexString:@"#e5e5e5"];
    
    self.titArr = [NSMutableArray arrayWithCapacity:1];
    
    [self requestTitle];
    
    [self.rightbutton setTitle:@"详情" forState:(UIControlStateNormal)];
}

- (void)rightClick{
    [self.navigationController pushViewController:[CSpayViewController new] animated:YES];
}

-(void)requestTitle{
    [NetWorkTool POST:MmallList param:nil success:^(id dic) {
        
        self.titArr = [CSMallTitleModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        CSMallTitleModel *model = [[CSMallTitleModel alloc]init];
        model.id = @"";
        model.typeName = @"平台展示";
        [self.titArr insertObject:model atIndex:0];
        [self showNav];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

-(void)showNav{
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.indicatorAdditionalWidth = 10; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
    configure.titleGradientEffect = YES;
    
    configure.titleSelectedColor= [UIColor colorWithHexString:@"#70BE68"];
    configure.titleColor= [UIColor colorWithHexString:@"#999999"];
    configure.showIndicator = NO;
    NSMutableArray *titleArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *childVCS = [NSMutableArray arrayWithCapacity:1];
    
    for (int i = 0; i<_titArr.count; i++) {
        CSMallTitleModel *model = _titArr[i];
        MallBannerViewController *oneVC = [[MallBannerViewController alloc] init];
        CSMallHomeViewController *twoVC = [[CSMallHomeViewController alloc] init];
        oneVC.type = [NSString stringWithFormat:@"%@",model.id];
        twoVC.type = [NSString stringWithFormat:@"%@",model.id];
        [titleArr addObject:model.typeName];
        if (i == 0) {
            [childVCS addObject:oneVC];
        }else{
            [childVCS addObject:twoVC];
        }
    }
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 44) delegate:self titleNames:titleArr configure:configure];
    self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, kNaviHeight+44, KWIDTH, KHEIGHT-kNaviHeight-44) parentVC:self childVCs:childVCS];
    
    _pageContentScrollView.delegatePageContentScrollView = self;
    [self.view addSubview:self.pageTitleView ];
    
    [self.view addSubview:_pageContentScrollView];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [super textFieldShouldReturn:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [super textFieldShouldReturn:textField];
    CSSearchViewController *vc = [CSSearchViewController new];
    vc.search = textField.text;
    [self.navigationController pushViewController:vc animated:YES];
    return YES;
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 1 || index == 5) {
        [_pageTitleView removeBadgeForIndex:index];
    }
}

@end

