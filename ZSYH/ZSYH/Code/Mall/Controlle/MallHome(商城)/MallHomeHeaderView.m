
//
//  MallHomeHeaderView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "MallHomeHeaderView.h"
#import <SCAdView.h>
#import "CSBannerModel.h"

@interface MallHomeHeaderView ()<SCAdViewDelegate>
@property (nonatomic, strong) SCAdView *scAdView;
@property (nonatomic, strong) UIView *pageControlBackView;
@property (nonatomic, strong) UIView *currentPageView;
@end

@implementation MallHomeHeaderView

- (void)buildSubview {
    UIView *pageControlBackView = [[UIView alloc] init];
    pageControlBackView.frame = CGRectMake(0, 164, KWIDTH, 10);
    pageControlBackView.backgroundColor = [UIColor clearColor];
    self.pageControlBackView = pageControlBackView;
    [self addSubview:pageControlBackView];
    
    
    //左侧 立即订购按钮
    UIButton *leftBtn = [[UIButton alloc] init];
    leftBtn.frame = CGRectMake(0, 189, KWIDTH/2, 194);
    [leftBtn setImage:imgname(@"peijiantupian") forState:UIControlStateNormal];
    [self addSubview:leftBtn];
    //右侧 立即购买按钮
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(KWIDTH/2 , 189, KWIDTH/2, 194);
    [rightBtn setImage:imgname(@"组 3422") forState:UIControlStateNormal];
    [self addSubview:rightBtn];
    //分割线
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#F1F1F1"];
    line.frame = CGRectMake(0, leftBtn.bottom +10, KWIDTH, 10);
    [self addSubview:line];
    //热门推荐 Logo
    UIImageView *hotImg = [[UIImageView alloc] init];
    [hotImg setImage:imgname(@"rementuijian")];
    hotImg.frame = CGRectMake(10, line.bottom+17, 22, 22);
    [self addSubview:hotImg];
    //热门推荐 提示文本
    UILabel *hotLab = [[UILabel alloc] init];
    hotLab.frame = CGRectMake(hotImg.right +10, line.bottom +17, KWIDTH-20-22-10, 22);
    hotLab.text = @"热门推荐";
    hotLab.font = FontSize(16);
    hotLab.textColor = K333333;
    [self addSubview:hotLab];
}

- (void)reloadPageControlViewWithCount:(NSInteger)count {
    [self.pageControlBackView removeAllSubviews];
    CGFloat viewWidth = 18;
    CGFloat viewSpace = 5;
    CGFloat viewHeight = 4;
    CGFloat viewTop = 10;
    CGFloat allWidth = viewWidth * count + (count - 1) * viewSpace;
    CGFloat left = KWIDTH / 2 - allWidth / 2;
    for (int i = 0; i < count; i++) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(left + (viewWidth + viewSpace) * i, viewTop, viewWidth, viewHeight);
        view.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
        view.layer.cornerRadius = viewHeight / 2;
        view.layer.masksToBounds = YES;
        [self.pageControlBackView addSubview:view];
        if (i == 0) {
            self.currentPageView = view;
            view.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
        }
    }
}

//加载网络数据
- (void)loadContent {
    NSArray *bannerArr = self.data;
    NSMutableArray *mu = [NSMutableArray arrayWithCapacity:1];
    if (bannerArr.count == 0) {
        return;
    }
    for (CSBannerModel *model in bannerArr) {
        [mu addObject:model.pic_url];
    }
    [self.scAdView reloadWithDataArray:mu];
    [self reloadPageControlViewWithCount:bannerArr.count];
}

- (SCAdView *)scAdView {
    if (!_scAdView) {
        SCAdView *adView = [[SCAdView alloc] initWithBuilder:^(SCAdViewBuilder *builder) {
            builder.viewFrame = (CGRect){0, 0, KWIDTH, 174};
            builder.adItemSize = (CGSize){(KWIDTH - 60 * 2), 120};
            builder.minimumLineSpacing = 25;
            builder.secondaryItemMinAlpha = 1;
            builder.threeDimensionalScale = 1.2;
            builder.infiniteCycle = 2.f;//无线轮播时间
            builder.itemCellNibName = @"SCAdDemoCollectionViewCell";
        }];
        adView.backgroundColor = [UIColor clearColor];
        adView.delegate = self;
        [adView play];
        _scAdView = adView;
        [self addSubview:adView];
    }
    return _scAdView;
}

#pragma mark - 轮播代理
-(void)sc_didClickAd:(id)adModel{
    
    
}
- (void)sc_scrollToIndex:(NSInteger)index{
    //NSLog(@"sc_scrollToIndex-->%ld",index);
    NSArray *viewArray = self.pageControlBackView.subviews;
    self.currentPageView.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
    UIView *currentView = viewArray[index];
    currentView.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    self.currentPageView = currentView;
}
@end
