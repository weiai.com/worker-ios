//
//  MallBannerViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "MallBannerViewController.h"
#import "GoodsDetailController.h"
#import "GoodListModel.h"
#import "GoodsDetailController.h"
#import "HomeGoodsCell.h"
#import "CSBannerModel.h"
#import "CSgoodsViewController.h"

@interface MallBannerViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)XSQBanner *banner;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray *modelArr;
@property(nonatomic,strong)NSMutableArray *bannerArr;

@end

@implementation MallBannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    [self requestGoods];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.bannerArr = [NSMutableArray arrayWithCapacity:1];
    [self loadBanner];
    
   
}


-(void)loadBanner{
   
    [NetWorkTool POST:Mbannerapi param:nil success:^(id dic) {
        
        self.bannerArr = [CSBannerModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.collectionView reloadData];

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

- (void)requestGoods {
   
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"typeId"] = NOTNIL(_type);
    
    [NetWorkTool POST:MmallDetaileID param:param success:^(id dic) {
        
        self.modelArr = [GoodListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.collectionView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

#pragma mark - CollectionView -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];

    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    [cell refreshs:model];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"head" forIndexPath:indexPath];
        
        header.backgroundColor = [UIColor whiteColor];
        
        NSMutableArray *mu = [NSMutableArray arrayWithCapacity:1];
        for (CSBannerModel *model in _bannerArr) {
            [mu addObject:model.pic_url];
        }
            _banner = [[XSQBanner alloc]initWithFrame:CGRectMake(10, 10, KWIDTH-20, 150)];
            _banner.pageLocation = PageControlLocationCenter;
            [_banner updateImageArr:mu AndImageClickBlock:^(NSInteger index) {
                NSLog(@"wodebanner%ld",index);
//                CateBannerModel *mod = _model.ad_list[index];
//                ShopDetailController *vc = [ShopDetailController new];
//                vc.storeId = mod.ID;
//                if ([mod.ID integerValue] >0) {
//                    [self.navigationController pushViewController:vc animated:YES];
//                }

            }];
            [header addSubview:_banner];
        UILabel *malable = [[UILabel alloc]initWithFrame:CGRectMake(0, 154+22, KWIDTH, 28)];
        malable.text = @"常见配件推荐";
        malable.textAlignment = NSTextAlignmentCenter;
        [header addSubview:malable];

        return header;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = model.Id;
     [self.navigationController pushViewController:vc animated:YES];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(KWIDTH/2, 260);
          layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-kTabbarHeight-45) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView reloadData];
        
    }
    return _collectionView;
}

@end
