//
//  CSSearchViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSSearchViewController : BaseViewController
@property(nonatomic,copy)NSString *search;
@property(nonatomic,assign)BOOL isFirst;
@end

NS_ASSUME_NONNULL_END
