//
//  CSMallHomeViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMallHomeViewController.h"
#import "GoodListModel.h"
#import "CSgoodsViewController.h"
#import "HomeGoodsCell.h"
@interface CSMallHomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)XSQBanner *banner;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray *modelArr;

@end

@implementation CSMallHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    [self requestGoods];
}

- (void)requestGoods {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"typeId"] = NOTNIL(_type);
    
    [NetWorkTool POST:MmallDetaileID param:param success:^(id dic) {
        
        self.modelArr = [GoodListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.collectionView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

#pragma mark - CollectionView -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];
    
        GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
        [cell refreshs:model];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(KWIDTH/2 , 260);
//        layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-kTabbarHeight-45) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView reloadData];
    }
    return _collectionView;
}

@end
