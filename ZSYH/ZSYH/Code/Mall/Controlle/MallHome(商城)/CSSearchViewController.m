//
//  CSSearchViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSSearchViewController.h"
#import "GoodListModel.h"
#import "CSgoodsViewController.h"
#import "HomeGoodsCell.h"
#import "CSsearchModel.h"
#import "QPLocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import "ZYBuyNowTableViewCell.h"
#import "ZYGoodsDetailViewController.h"
#import "NewZYBuyNowModel.h"

@interface CSSearchViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) XSQBanner *banner;
@property (nonatomic, strong) NSMutableArray *modelArr;
@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *odlBut;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger typepage;
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation CSSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.page = 1;
    self.modelArr = [NSMutableArray arrayWithCapacity:1];

    UITextField *searchTF = [[UITextField alloc]init];
    [searchTF PlasecreateLeftViewImage:@""];
    searchTF.delegate = self;
    self.navigationItem.titleView = searchTF;
    searchTF.returnKeyType = UIReturnKeySearch;
    searchTF.backgroundColor = [UIColor colorWithHexString:@"#e5e5e5"];
    self.searchTF = searchTF;
    
    [self.rightbutton setBackgroundImage:imgname(@"searchright") forState:(UIControlStateNormal)];
    self.rightbutton.frame = CGRectMake(0, 0, 60, 30);
    //[self.rightbutton setTitle:@"搜索" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.rightbutton.titleLabel.font= FontSize(13);
    [self.rightbutton addTarget:self action:@selector(rightbuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self showdetaile];
    
}

-(void)showdetaile {
    
    [self.view addSubview:self.scrollView];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYBuyNowTableViewCell class] forCellReuseIdentifier:@"ZYBuyNowTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
    //self.mytableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
    //    [self requestGoods];//请求数据
    //}];
    [_scrollView addSubview:_mytableView];
}

-(void)rightbuttonAction:(UIButton *)but{
    NSInteger indd = but.tag -9834;
    [self.odlBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [but setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    self.odlBut = but;
    self.typepage = indd;
    _page = 1;
    [self requestGoods];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    self.searchTF = textField;
    [super textFieldShouldReturn:textField];
    _search = textField.text;
    return YES;
}

- (void)requestGoods {
    [self.modelArr removeAllObjects];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"name"] = self.searchTF.text;
   
    param[@"type"] = @"1";
    NSString *pp = [NSString stringWithFormat:@"%d",_page];
    param[@"pageNum"] = pp;
    param[@"classification"] = @""; //分类 1配件 2整机
    
    if (self.typepage == 1) {
        param[@"type"] = @"2";
    }
    
    if (_page == 1) {
        [self.modelArr removeAllObjects];
    }
    
    [NetWorkTool POST:getPartsListSearch param:param success:^(id dic) {
        
        self.modelArr = [NewZYBuyNowModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.mytableView reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
        [self.mytableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {

        [self.mytableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYBuyNowTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYBuyNowTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    
    [mycell refasf:model];
    
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
        
        switch (ind) {
            case 0: {
                ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
                vc.shopID = model.id;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    
    ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
    vc.shopID = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end

