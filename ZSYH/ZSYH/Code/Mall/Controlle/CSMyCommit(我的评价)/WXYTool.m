//
//  WXYTool.m
//  SocialMall
//
//  Created by wanshangwl on 2018/4/26.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "WXYTool.h"
#import "LBPhotoBrowserManager.h"

@implementation WXYTool


+ (void)showImageWithURLArray:(NSArray *)array selectedIndex:(NSInteger)index frame:(CGRect)frame {
    NSMutableArray *datas = [NSMutableArray array];
    
    for (NSString *str in array) {
        LBPhotoWebItem *item = [[LBPhotoWebItem alloc]initWithURLString:appendd(str) frame:frame placeholdImage:defaultImg placeholdSize:CGSizeMake(KWIDTH, KWIDTH)];
        [datas addObject:item];
    }

    [LBPhotoBrowserManager.defaultManager showImageWithWebItems:datas selectedIndex:index fromImageViewSuperView:[UIApplication sharedApplication].keyWindow];
}


+ (void)showLocalImg:(NSArray *)array selectedIndex:(NSInteger)index frame:(CGRect)frame {

    NSMutableArray *datas = [NSMutableArray array];
    
    for (UIImage *image in array) {
        LBPhotoLocalItem *item = [[LBPhotoLocalItem alloc]initWithImage:image frame:frame];
        [datas addObject:item];
    }

    [LBPhotoBrowserManager.defaultManager showImageWithLocalItems:datas selectedIndex:index fromImageViewSuperView:[UIApplication sharedApplication].keyWindow];
}


@end
