//
//  CommentsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CommentsModel : BaseModel
@property(nonatomic,strong)NSMutableArray *images_list;
@end

NS_ASSUME_NONNULL_END
