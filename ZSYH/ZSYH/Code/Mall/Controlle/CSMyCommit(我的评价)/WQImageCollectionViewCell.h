//
//  WQImageCollectionViewCell.h
//  ZengBei
//
//  Created by 魏堰青 on 2018/10/29.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WQImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myimage;

@end
