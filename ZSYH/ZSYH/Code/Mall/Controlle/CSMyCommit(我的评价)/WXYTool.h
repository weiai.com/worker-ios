//
//  WXYTool.h
//  SocialMall
//
//  Created by wanshangwl on 2018/4/26.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXYTool : NSObject

+ (void)showImageWithURLArray:(NSArray *)array selectedIndex:(NSInteger)index frame:(CGRect)frame;

+ (void)showLocalImg:(NSArray *)array selectedIndex:(NSInteger)index frame:(CGRect)frame;


@end
