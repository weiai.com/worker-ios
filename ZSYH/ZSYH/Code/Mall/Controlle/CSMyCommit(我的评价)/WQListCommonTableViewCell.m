//
//  WQListCommonTableViewCell.m
//  ZengBei
//
//  Created by 魏堰青 on 2018/10/29.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "WQListCommonTableViewCell.h"
#import "CommentsModel.h"
#import "WQImageCollectionViewCell.h"

static NSString *const cellId = @"WQImageCollectionViewCell";

@interface WQListCommonTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)CommentsModel *model;

@end
@implementation WQListCommonTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.myicon.layer.masksToBounds = YES;
    self.myicon.layer.cornerRadius = 22;
    
    self.star.isShow = YES;
    self.star.BGViewWidth = 90;
    self.star.widthDistence = 5;
    self.star.gardernumber = 3;
    [self.star loadSubviews];
    // Configure the view for the selected state
}
-(void)showdetaliewithmodel:(CommentsModel *)model{
    self.model = model;
    self.ind = 3;
    
//    self.nameLB.text = [NSString stringWithFormat:@"%@",model.nickname];
//    self.timeLB.text = [XSQTools change:model.add_time];
//    self.contentLB.text = [NSString stringWithFormat:@"%@",model.content];
//    [self.myicon sd_setImageWithURL:[NSURL URLWithString:append(model.logo)] placeholderImage:defaultImg];

    [self showUI];
    
    
    
    
}



- (void)showUI {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.itemSize = CGSizeMake((KWIDTH-40)/_ind, 100);
       layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    CGFloat imgh = _model.images_list.count/_ind;
    int a;
    a = (ceil(imgh) + 1) * 100;
    CGRect fff =   self.myImageView.frame ;

    if (_model.images_list.count == 0) {
        fff.size.height= 0.001;
    }else{
        fff.size.height= a*100;

    }
    self.myImageView.frame  = fff;
    
    self.mycollectionView = [[UICollectionView alloc]initWithFrame:self.myImageView.bounds collectionViewLayout:layout];
    _mycollectionView.backgroundColor = [UIColor clearColor];
    _mycollectionView.delegate = self;
    _mycollectionView.dataSource = self;
    [_mycollectionView registerNib:[UINib nibWithNibName:cellId bundle:nil] forCellWithReuseIdentifier:cellId];
    [self.myImageView addSubview:_mycollectionView];
    [_mycollectionView reloadData];
    
}


#pragma mark - 列表代理 -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  
    return _model.images_list.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WQImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    
    [cell.myimage sd_setImageWithURL:[NSURL URLWithString:append(_model.images_list[indexPath.row])] placeholderImage:defaultImg];
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    if (self.select) {
        self.select(indexPath.row);
    }

}


//- (void)setLabColor:(UIColor *)labColor {
//    _labColor = labColor;
//    [_collectionView reloadData];
//}
//
//
//- (void)setLabFont:(CGFloat)labFont {
//    _labFont = labFont;
//    [_collectionView reloadData];
//}
//
//
//- (void)setImgs:(NSArray *)imgs {
//    _imgs = imgs;
//    [_collectionView reloadData];
//}
@end
