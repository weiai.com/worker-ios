//
//  MyCommentsController.m
//  AiBao
//
//  Created by Mac on 2018/1/13.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "MyCommentsController.h"
#import <MJRefresh.h>
#import "WXYTool.h"
#import "WQListCommonTableViewCell.h"
#import "CSgoodscommitModel.h"
#import "CSCommitHeader.h"
#import "CScommitNopicTableViewCell.h"

@interface MyCommentsController ()<UITableViewDelegate,UITableViewDataSource> {
    NSInteger _page;
}
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (strong,nonatomic) UITableView    *tableView;
@property (strong,nonatomic) CSCommitHeader *headerView;

@end

static NSString *const cellId = @"WQListCommonTableViewCell";
@implementation MyCommentsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评论";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
    
    _page = 1;
    _modelArr = [NSMutableArray arrayWithCapacity:1];
    
    [self requestHotGoodss];
    [self showHeader];
}

-(void)showHeader{
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    [muarr addObject:@" 全部(0) "];
    [muarr addObject:@" 好评(0) "];
    [muarr addObject:@" 差评(0) "];
    
    self.headerView = [[CSCommitHeader alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 88) modelArr:muarr block:^(NSInteger index) {
        
    }];
    [self.view addSubview:self.tableView];
    
    _page = 1;
    [self requestHotGoodss];
}

- (void)clickBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestHotGoodss {
    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = NOTNIL(_goodsId);
    mudic[@"type"] = @"0";
    kWeakSelf;
    
    [NetWorkTool POST:MmallgoodsbuyID param:mudic success:^(id dic) {
        KMyLog(@"++++++++++%@",[HFTools toJSONString:dic]);
        if (self->_page == 1) {
            [self->_modelArr removeAllObjects];
        }
        
        for (NSDictionary *evdic in [[dic objectForKey:@"data"] objectForKey:@"evaluates"]) {
            CSgoodscommitModel *model = [[CSgoodscommitModel alloc]init];
            [model setValuesForKeysWithDictionary:evdic];
            [self.modelArr addObject:model];
        }
        [self.tableView reloadData];
        
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        
    } fail:^(NSError *error) {
        [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        
    } needUser:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _modelArr.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CScommitNopicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CScommitNopicTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CSgoodscommitModel   *model = _modelArr[indexPath.section];
    
    mysdimg(cell.userImage, model.image_url);
    if (model.nickname.length == 1) {
        cell.userNameLB.text = @"*";
    }else if (model.nickname.length == 2) {
        NSInteger length = 1;
        NSString *name = [HFTools replaceStringWithString:model.nickname Asterisk:1 length:length];
        cell.userNameLB.text = name;
    }else if (model.nickname.length >= 3) {
        NSInteger length = model.nickname.length-2;
        NSString *name = [HFTools replaceStringWithString:model.nickname Asterisk:1 length:length];
        cell.userNameLB.text = name;
    }
    cell.contentLB.text = model.evaluate_content;
    cell.timeLB.text = [model.create_time substringToIndex:10];
    
    if ([model.evaluate_score integerValue]  == 0) {
        //好评
        cell.commitLB.text = @"好评";
        cell.commitImage.image = imgname(@"haopingpj");
    }else  if ([model.evaluate_score integerValue]  == 1) {
        //中评
        cell.commitLB.text = @"中评";
        cell.commitImage.image = imgname(@"zhongpingpj");
        
    }else  if ([model.evaluate_score integerValue]  == 2) {
        //差评
        cell.commitLB.text = @"差评";
        cell.commitImage.image = imgname(@"chapingpj");
        
        
    }
    
    //    WQListCommonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    //
    //    CommentsModel *model = _modelArr[indexPath.section];
    //    [cell showdetaliewithmodel:model];
    //    [cell.mycollectionView reloadData];
    /*CommentsModel *model = _modelArr[indexPath.section];
     [cell refreshUI:model];
     cell.clickImg = ^(UIView *view, NSInteger index) {
     CGRect frame = CGRectMake(view.left+10, view.top+cell.imgView.top+cell.top+64-tableView.contentOffset.y, view.width, view.height);
     [WXYTool showImageWithURLArray:model.img selectedIndex:index frame:frame];
     };*/
    return cell;
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    //CommentsModel *model = _modelArr[indexPath.row];
//    CommentsModel *model = _modelArr[indexPath.section];
//    NSLog(@"加油加油%@",model.images_list);
//
//    if (model.images_list.count>0) {
//        CGFloat imgh = model.images_list.count/3;
//        int a;
//           a = (ceil(imgh) + 1) * 100;
//        NSLog(@"dddddddddd%d",a);
//        CGFloat ch = [NSString heightWithWidth:KWIDTH-20 font:15 text:model.content]+120+a;
//        return ch;
//    }else{
//        CGFloat ch = [NSString heightWithWidth:KWIDTH-20 font:15 text:model.content]+110;
//        return ch;
//
//    }
//
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 0.1)];
    //    UIView *back = [[UIView alloc]initWithFrame:CGRectMake(10, 0, KWIDTH-20, 80)];
    //    back.backgroundColor = KRGB(238, 238, 238);
    //    [view addSubview:back];
    //    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(back.width-70, 10, 60, 60)];
    //    img.image = imgname(@"列表图一");
    //    [back addSubview:img];
    //    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, back.width-80, back.height)];
    //    label.textColor = KTEXTCOLOR;
    //    label.text = @"郭大侠(文化路店)";
    //    [back addSubview:label];
    
    return view;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 0.1)];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        //        _tableView.tableHeaderView = _headerView;
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_tableView reloadData];
        adjustInset(_tableView);
        [_tableView registerNib:[UINib nibWithNibName:@"CScommitNopicTableViewCell" bundle:nil] forCellReuseIdentifier:@"CScommitNopicTableViewCell"];
        _tableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(KWIDTH/2-50, KHEIGHT/2-36-60, 100, 72) image:imgname(@"tableviewPlaseholder") viewClick:^{
            //            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        _tableView.placeHolderView.backgroundColor = [UIColor clearColor];
        //        TableRegisterNib(_tableView, @"CScommitNopicTableViewCell");
        //        self.tableView.estimatedRowHeight = 666;
        //        self.tableView.rowHeight = UITableViewAutomaticDimension;
        _page = 1;
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self->_page = 1;
            [self requestHotGoodss];
        }];
        
        //        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        //            _page++;
        //            [self requestHotGoodss];
        //        }];
    }
    return _tableView;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
