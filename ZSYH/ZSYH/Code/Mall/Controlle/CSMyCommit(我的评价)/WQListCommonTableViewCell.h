//
//  WQListCommonTableViewCell.h
//  ZengBei
//
//  Created by 魏堰青 on 2018/10/29.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsModel.h"
#import "StarEvaluator.h"
@interface WQListCommonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myicon;

@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLB;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;

@property (weak, nonatomic) IBOutlet UIImageView *myImageView;
@property (weak, nonatomic) IBOutlet StarEvaluator *star;


@property(nonatomic,strong)UICollectionView *mycollectionView;
@property (nonatomic,  copy) void(^select)(NSInteger index);
@property(nonatomic,assign)NSInteger ind;



-(void)showdetaliewithmodel:(CommentsModel *)model;

@end
