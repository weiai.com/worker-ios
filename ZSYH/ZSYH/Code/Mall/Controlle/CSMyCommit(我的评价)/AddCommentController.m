//
//  AddCommentController.m
//  TianYuanShiJi
//
//  Created by Mac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//
#import "TZImagePickerController.h"

#import "AddCommentController.h"
#import "UITextView+category.h"
#import <IQKeyboardManager.h>
#import "XHStarRateView.h"
#import "MBProgressHUD.h"
#import "TZTestCell.h"
#import "WXYTool.h"


@interface AddCommentController ()<TZImagePickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource> {
    NSMutableArray *_photos;
    NSMutableArray *_assets;
}
@property (weak, nonatomic) IBOutlet UITextView *content;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

static NSString * const cellId = @"TZTestCell";

@implementation AddCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评价";
    [self.rightbutton setTitle:@"提交" forState:UIControlStateNormal];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    _photos = [NSMutableArray array];
    _assets = [NSMutableArray array];
    [_content setPlaceholder:@"请输入产品描述"];
    
    int num = KWIDTH>320?5:4;
    CGFloat w = (KWIDTH-20-(num-1)*5)/num;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(w, w);
    layout.minimumInteritemSpacing = 5;
    layout.minimumLineSpacing = 5;
    [_collectionView setCollectionViewLayout:layout];
    [_collectionView registerClass:[TZTestCell class] forCellWithReuseIdentifier:cellId];
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photos.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TZTestCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    //cell.asset = _assets[indexPath.row];
    if (indexPath.row == 0) {
        cell.deleteBtn.hidden = YES;
        cell.imageView.image = imgname(@"plus");
    }else {
        cell.deleteBtn.hidden = NO;
        cell.imageView.image = _photos[indexPath.row-1];
    }

    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.deleteBtn.tag = indexPath.row;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (_photos.count == 9) {
            ShowToastWithText(@"最多选择9张");
            return;
        }
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9-_photos.count columnNumber:4 delegate:self pushPhotoPickerVc:YES];
        imagePickerVc.sortAscendingByModificationDate = NO;
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }else {
        TZTestCell *cell = (TZTestCell *)[collectionView cellForItemAtIndexPath:indexPath];
        CGRect frame = CGRectMake(cell.left+10, cell.top+collectionView.top+80, cell.width, cell.width);
        [WXYTool showLocalImg:_photos selectedIndex:indexPath.row-1 frame:frame];
    }
}


#pragma mark - Click Event

- (void)deleteBtnClik:(UIButton *)sender {
    TZTestCell *cell = (TZTestCell *)sender.superview.superview;
    NSIndexPath *indexPath = [_collectionView indexPathForCell:cell];
    NSInteger index = indexPath.row-1;

    [_photos removeObjectAtIndex:index];
    [_assets removeObjectAtIndex:index];
    
    [self.collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self->_collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self->_collectionView reloadData];
    }];
}


#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
}

// 当选择器自己dismiss的时候，会执行下面的代理方法
// isSelectOriginalPhoto 选择了原图
// asset通过：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]获得原图
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    [_photos addObjectsFromArray:photos];
    [_assets addObjectsFromArray:assets];
    [self.collectionView reloadData];
}


- (void)rightClick {
    NSString *str = [NSString showTip:@[@{@"请输入您的评价":_content.text}]];
    
    if (![str isEqualToString:@"ok"]) {
        ShowToastWithText(str);
    }else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (_photos.count > 0) {
            NSMutableArray *arr = [NSMutableArray array];
            
            for (UIImage *image in _photos) {
                NSString *url = [NSString stringWithFormat:@"%@Upload/imageUp",HOST_URL];
                NSMutableDictionary *param = [NSMutableDictionary dictionary];
                [NetWorkTool UploadPicWithUrl:url param:param key:@"upfile" image:image withSuccess:^(id dic) {
                    if ([dic[@"status"] intValue] == 200) {
                        NSLog(@"----%@",dic[@"result"]);
                        [arr addObject:dic[@"result"]];
                    }
                    if (arr.count == self->_photos.count) {
                        [self submit:[arr componentsJoinedByString:@","]];
                    }
                } failure:^(NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                }];
            }
        }else{
            //没有图片
            [self submit:@""];
        }
    }
}


-(void)submit:(NSString *)imgs {
    
    NSString *url = [NSString stringWithFormat:@"%@Order/addComment",HOST_URL];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    param[@"userId"] = KUID;
    param[@"rec_id"] = NOTNIL(_rec_id);
    param[@"content"] = NOTNIL(_content.text);
    param[@"order_id"] = NOTNIL(_orderId);
    param[@"goods_id"] = NOTNIL(_goodsId);
    param[@"userToken"] = KTKID;
    param[@"comment_img"] = imgs;
  
    [NetWorkTool POST:url parameters:param progress:nil complation:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        ShowToastWithText(dic[@"msg"]);
        if ([dic[@"status"] intValue] == 200) {
            self.finish();
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSError *error) {
        [self.navigationController popViewControllerAnimated:YES];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
