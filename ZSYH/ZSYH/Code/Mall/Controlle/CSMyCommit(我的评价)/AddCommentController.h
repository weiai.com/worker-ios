//
//  AddCommentController.h
//  TianYuanShiJi
//
//  Created by Mac on 2017/12/14.
//  Copyright © 2017年 Mac. All rights reserved.
//

#import "BaseViewController.h"

@interface AddCommentController : BaseViewController

@property (nonatomic,copy) void (^finish)(void);

@property (nonatomic, copy) NSString *goodsId;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *rec_id;

@end
