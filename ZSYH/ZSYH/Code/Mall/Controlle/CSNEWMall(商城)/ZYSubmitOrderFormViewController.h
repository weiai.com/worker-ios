//
//  ZYSubmitOrderFormViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYSubmitOrderFormViewController : BaseViewController
@property (nonatomic, strong) NSMutableArray *orderArray;
@end

NS_ASSUME_NONNULL_END
