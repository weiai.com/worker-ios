//
//  CSPIngtaiViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSPIngtaiViewController.h"
#import "CSdoodsCommitView.h"
#import "CSgoodsShopView.h"
#import "CSGoodsModel.h"
#import "CSgoodscommitModel.h"
#import "CSBannerModel.h"
#import "CSShopViewController.h"
#import "CSshopModel.h"

@interface CSPIngtaiViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property (nonatomic,strong) XSQBanner *banner;
@property (nonatomic,assign) BOOL isrefa;
@property(nonatomic,strong)CSGoodsModel *modlel;

@property(nonatomic,strong)NSMutableArray *commitArr;
@property(nonatomic,strong)NSMutableArray *imageArr;

@property(nonatomic,strong)CSshopModel *shopModel;
@property(nonatomic,strong)NSMutableArray *shopProductArr;

@end

@implementation CSPIngtaiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"产品详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.isrefa  = NO;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.commitArr = [NSMutableArray arrayWithCapacity:1];
    self.imageArr = [NSMutableArray arrayWithCapacity:1];
    self.shopProductArr = [NSMutableArray arrayWithCapacity:1];
    
    // Do any additional setup after loading the view.
    [self requestDetaile];
}
-(void)requestDetaile{
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"proId"] = _goodsID;
    mudic[@"type"] = @"1";

    kWeakSelf;
    [NetWorkTool POST:MmallgoodsbuyID param:mudic success:^(id dic) {
        weakSelf.modlel = [[CSGoodsModel alloc]init];
        //        KMyLog(@"详情++%@",dic);
        KMyLog(@"++++++++++%@",[HFTools toJSONString:dic]);
        
        for (NSDictionary *imageDic in [[dic objectForKey:@"data"] objectForKey:@"imgs"]) {
            [weakSelf.imageArr addObject:[imageDic objectForKey:@"product_img_url"]];
        }
        
        
        [self showdetaile];
        
        
    } other:^(id dic) {
    } fail:^(NSError *error) {
    } needUser:NO];
    
    
}








-(void)showdetaile{
    [self.view addSubview:self.scrollView];
    
    //    for (int i = 0; i < 2; i++) {
    //        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //        btn.frame = CGRectMake(KWIDTH/2.0*i, KHEIGHT-50, KWIDTH/2, 50);
    //        btn.titleLabel.font = FontSize(15);
    //        btn.tag = 3333+i;
    //        btn.backgroundColor = i==0?[UIColor lightGrayColor]:KBColor;
    //        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //        [btn setTitle:i==0?@"加入购物车":@"立即购买" forState:UIControlStateNormal];
    //        [btn addTarget:self action:@selector(buyAddCart:) forControlEvents:UIControlEventTouchUpInside];
    //        [self.view addSubview:btn];
    //    }
    
    
    /*
    //创建产品详情
    [self.scrollView addSubview:self.banner];
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    [muarr addObject:_modlel.productPic];
    for (CSBannerModel *model in _imageArr) {
        [muarr addObject:model.product_img_url];
        
    }
    if (muarr.count <1) {
        [muarr addObject:_modlel.productPic];
    }
    [_banner updateImageArr:muarr AndImageClickBlock:^(NSInteger index) {
        
    }];
    
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(10, _banner.bottom+10, KWIDTH-20, 40)];
    name.textColor = KTEXTCOLOR;
    name.font = FontSize(18);
    name.text = _modlel.productName;
    name.numberOfLines = 2;
    [name sizeToFit];
    [self.scrollView addSubview:name];
    UILabel *price = [[UILabel alloc]initWithFrame:CGRectMake(10, name.bottom+10, KWIDTH-160, 20)];
    price.textColor = RGBA(252, 23, 39,1);
    price.font = FontSize(18);
    price.text =[NSString stringWithFormat:@"¥%@",_modlel.productPrice];
    [self.scrollView addSubview:price];
    self.scrollView.contentSize = CGSizeMake(KWIDTH,price.bottom+20);

    UILabel *pingtaiLB = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-140, name.bottom+10, 120, 20)];
    pingtaiLB.textColor = [UIColor colorWithHexString:@"#FEC351"];
    pingtaiLB.textAlignment = NSTextAlignmentCenter;
    pingtaiLB.font = FontSize(13);
    pingtaiLB.text =[NSString stringWithFormat:@"平台直接提供保障"];
    pingtaiLB.backgroundColor = [UIColor colorWithHexString:@"#F2E8D5"];
    
//    if ([_modlel.productBelType integerValue] == 1) {
        [self.scrollView addSubview:pingtaiLB];
        

//    }
    UILabel *con = [[UILabel alloc]initWithFrame:CGRectMake(0, pingtaiLB.bottom+10, KWIDTH, 30)];
    con.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:con];
    self.scrollView.contentSize = CGSizeMake(KWIDTH,con.bottom+20);

    */
    CGFloat ffhei = 0;
    for (int i = 0; i<_imageArr.count; i++) {
    
        NSString *strurl=_imageArr[i];
        
        
        UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10+KWIDTH*i, KWIDTH, KWIDTH)];

        [self.scrollView addSubview:imagev];

        if ([strurl hasPrefix:@"http"]) {
            [imagev sd_setImageWithURL:[NSURL URLWithString:strurl] placeholderImage:defaultImg];
        }else{
            sdimg(imagev, strurl);
      
        }
        ffhei =imagev.bottom;
    }
    
    
    NSString *tit = _modlel.productInfo;
    CGFloat sdffs = [NSString heightWithWidth:KWIDTH-32 font:14 text:tit];
    UILabel *conttitle = [[UILabel alloc]initWithFrame:CGRectMake(16, ffhei+16, KWIDTH-32, sdffs)];
    conttitle.font = FontSize(14);
    conttitle.text = tit;
    conttitle.numberOfLines = 0;
//    [self.scrollView addSubview:conttitle];

//    self.scrollView.contentSize = CGSizeMake(KWIDTH,conttitle.bottom+20);

    
    self.scrollView.contentSize = CGSizeMake(KWIDTH,ffhei+20);

    /*
    CGFloat heig = price.bottom;
    if (self.commitArr.count >0) {
        
        
        UILabel *moreLB = [[UILabel alloc]initWithFrame:CGRectMake(0, heig+20, KWIDTH, 24)];
        moreLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        moreLB.textAlignment = NSTextAlignmentCenter;
        moreLB.font = FontSize(13);
        moreLB.text =[NSString stringWithFormat:@"————— 评价 —————"];
        moreLB.textColor = [UIColor colorWithHexString:@"#999999"];
        [self.scrollView addSubview:moreLB];
        CSgoodscommitModel *model = _commitArr[0];
        CGFloat  nbheigtf =  [NSString heightWithWidth:KWIDTH-32 font:14 text:model.evaluate_content];
        CSdoodsCommitView *commView = [[CSdoodsCommitView alloc]initWithFrame:CGRectMake(0, moreLB.bottom, KWIDTH, 120+nbheigtf)];
        commView.contentLB.text =model.evaluate_content;
        commView.pingjiaLB.text = [NSString stringWithFormat:@"产品评价（%ld）",_commitArr.count];
        [commView.lookAllButton addTarget:self action:@selector(lookMallAction) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:commView];
        heig = commView.bottom;
        
    }
    UILabel *commViewDowBgLable = [[UILabel alloc]initWithFrame:CGRectMake(0, heig, KWIDTH, 8)];
    commViewDowBgLable.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:commViewDowBgLable];
    UIImageView *shopImage = [[UIImageView alloc]initWithFrame:CGRectMake(16, commViewDowBgLable.bottom+8, 40, 40)];
    [self.scrollView addSubview:shopImage];
    if ([self.shopModel.shop_image hasPrefix:@"http"]) {
        [shopImage sd_setImageWithURL:[NSURL URLWithString:_shopModel.shop_image] placeholderImage:defaultImg];
    }else{
        sdimg(shopImage, _shopModel.shop_image);
    }
    
    //    UILabel *shopName = [[UILabel alloc]initWithFrame:CGRectMake(64, commViewDowBgLable.bottom+18, KWIDTH-80, 14)];
    UILabel *shopName = [[UILabel alloc]init];
    
    //    shopName.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //    shopName.textAlignment = NSTextAlignmentCenter;
    shopName.font = FontSize(13);
    shopName.text =self.shopModel .shop_name;
    shopName.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.scrollView addSubview:shopName];
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jindian)];
    shopName.userInteractionEnabled = YES;
    [shopName addGestureRecognizer:ges];
    [shopName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(commViewDowBgLable.mas_bottom).offset(18);
        make.left.offset(64);
        make.height.offset(14);
    }];
    UIButton *jindianbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [jindianbut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [self.scrollView addSubview:jindianbut];
    //    jindianbut.frame = CGRectMake(shopName.right, commViewDowBgLable.bottom+18, 30, 14) ;
    [jindianbut addTarget:self action:@selector(jindian) forControlEvents:(UIControlEventTouchUpInside)];
    
    [jindianbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(commViewDowBgLable.mas_bottom).offset(18);
        make.left.mas_equalTo(shopName.mas_right).offset(0);
        make.height.offset(14);
        make.width.offset(30);
        
    }];
    UIButton *phonebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [phonebut setTitle:@"  电话联系" forState:(UIControlStateNormal)];
    [phonebut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [phonebut setImage:imgname(@"phonecall") forState:(UIControlStateNormal)];
    [phonebut addTarget:self action:@selector(phgoneaction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.scrollView addSubview:phonebut];
    phonebut.titleLabel.font = FontSize(10);
    //    [phonebut setBackgroundColor:[UIColor redColor]];
    phonebut.frame = CGRectMake(KWIDTH-86, commViewDowBgLable.bottom+18, 70, 20);
    //    [phonebut mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.top.mas_equalTo(commViewDowBgLable.mas_bottom).offset(18);
    //        make.right.offset(-10);
    //        make.height.offset(2000);
    //        make.width.offset(60);
    //    }];
    
    
    
    
    
    
    
    
    
    
    
    
    CSgoodsShopView *shopCollectView = [[CSgoodsShopView alloc]initWithFrame:CGRectMake(0, shopImage.bottom+8, KWIDTH, 120) modelArr:_shopProductArr block:^(NSInteger index) {
        KMyLog(@"点击了地%ld个",index);
    }];
    
    [self.scrollView addSubview:shopCollectView];
    
    
    //    UILabel *bg = [[UILabel alloc]initWithFrame:CGRectMake(0, shopCollectView.bottom, KWIDTH, 1000)];
    //  bg.backgroundColor =   [UIColor colorWithHexString:@"#F2F2F2"];
    //    [self.scrollView addSubview:bg];
    
    UILabel *refaLB = [[UILabel alloc]initWithFrame:CGRectMake(0, shopCollectView.bottom+8, KWIDTH, 11)];
    refaLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    refaLB.textAlignment = NSTextAlignmentCenter;
    refaLB.font = FontSize(8);
    refaLB.text =[NSString stringWithFormat:@"上拉查看图文详情"];
    refaLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.scrollView addSubview:refaLB];
    self.scrollView.contentSize = CGSizeMake(KWIDTH,refaLB.bottom+20);
    
    */
    
    
    
}
-(void)phgoneaction:(UIButton *)but{
    [HFTools callMobilePhone: _shopModel.shop_phone];
}
/**
 进店
 */
-(void)jindian{
    
    if ([_isshop isEqualToString:@"shop"]) {
        
    }else{
        CSShopViewController *jin = [[CSShopViewController alloc]init];
        jin.shopid = _shopModel.Id;
        [self.navigationController pushViewController:jin animated:YES];
        
    }
}
/**
 查看更多评价
 */
-(void)lookMallAction{
    KMyLog(@"查看更多评价");
    NSNotification *nof = [NSNotification notificationWithName:@"refasthcommit" object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:nof];
    
    
    
}

- (XSQBanner *)banner {
    if (!_banner) {
        _banner = [[XSQBanner alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KWIDTH)];
        _banner.pageLocation = PageControlLocationCenter;
        
        
    }
    return _banner;
}


- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
