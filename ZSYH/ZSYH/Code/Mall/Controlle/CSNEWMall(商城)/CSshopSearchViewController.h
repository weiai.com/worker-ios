//
//  CSshopSearchViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSshopSearchViewController : BaseViewController
@property(nonatomic,copy)NSString *idstr;
@property(nonatomic,copy)NSString *search;

@end

NS_ASSUME_NONNULL_END
