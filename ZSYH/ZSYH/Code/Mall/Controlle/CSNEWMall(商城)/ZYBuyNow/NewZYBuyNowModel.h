//
//  NewZYBuyNowModel.h
//  ZSYH
//
//  Created by 李 on 2019/12/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewZYBuyNowModel : NSObject
@property (nonatomic, copy) NSString *guaranteePeriod;
@property (nonatomic, copy) NSString *factory_id;
@property (nonatomic, copy) NSString *parts_remarks;
@property (nonatomic, copy) NSString *parts_details;
@property (nonatomic, copy) NSString *parts_name;

@property (nonatomic, copy) NSString *modifyDate;
@property (nonatomic, copy) NSString *is_good_choice;
@property (nonatomic, copy) NSString *is_new_push;
@property (nonatomic, copy) NSString *parts_price;
@property (nonatomic, copy) NSString *repair_user_price;

@property (nonatomic, copy) NSString *parts_type;
@property (nonatomic, copy) NSString *parts_brand;
@property (nonatomic, copy) NSString *lower_agentB;
@property (nonatomic, copy) NSString *lower_agentA;
@property (nonatomic, copy) NSString *parts_number;

@property (nonatomic, copy) NSString *soft;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *is_hot_push;
@property (nonatomic, copy) NSString *parts_place;
@property (nonatomic, copy) NSString *lower_user;

@property (nonatomic, copy) NSString *parts_quantity;
@property (nonatomic, copy) NSString *is_today_hot;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *agent_user_price;
@property (nonatomic, copy) NSString *parts_model;

@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *cellCount; //多少人购买
@property (nonatomic, copy) NSString *classification;
@property (nonatomic, copy) NSString *salePrice;
@property (nonatomic, copy) NSString *factory_name;

@property (nonatomic, copy) NSString *type_name;
@property (nonatomic, copy) NSString *parts_logo;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *damage_details;
@property (nonatomic, copy) NSString *agent_remakes;

@end

NS_ASSUME_NONNULL_END
