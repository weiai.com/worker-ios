//
//  ZYBuyNowViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowViewController.h"
#import "ZYBuyNowTableViewCell.h"
#import "GoodListModel.h"
#import "NewZYBuyNowModel.h"
#import "ZYGoodObjectPreViewController.h" //好物优选
#import "ZYNewProductViewController.h"    //新品推荐
#import "ZYGoodsDetailViewController.h"   //配件详情

@interface ZYBuyNowViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) NSMutableSet <NSString *>*saoYiSaoStrSet;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *modelArr;

@end

@implementation ZYBuyNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
    self.title = @"我要预订";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    
    [self showdetaile];
    
    [self requestData];
    // Do any additional setup after loading the view.
}

-(void)showdetaile {

    [self.view addSubview:self.scrollView];
    
    //左侧 好物优选
    UIButton *leftBtn = [[UIButton alloc] init];
    leftBtn.frame = CGRectMake(10, kNaviHeight + 16, (KWIDTH-30)/2, 189);
    [leftBtn setImage:imgname(@"组 3425") forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:leftBtn];
    
    //右侧 新品推荐
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(2 +(KWIDTH-14)/2 +10 , kNaviHeight + 16, (KWIDTH-30)/2, 189);
    [rightBtn setImage:imgname(@"组 3588") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:rightBtn];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, leftBtn.bottom +10, KWIDTH, KHEIGHT - kNaviHeight -26-189);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYBuyNowTableViewCell class] forCellReuseIdentifier:@"ZYBuyNowTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    [self.scrollView addSubview:_mytableView];
}

- (void)requestData {
    //立即订购 数据请求
    
    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *pp = [NSString stringWithFormat:@"%d",_page];
    param[@"pageNum"] = pp;
    param[@"type"] = @"1"; //立即订购 热火推荐 1
    
    [NetWorkTool POST:getPartsList param:param success:^(id dic) {
        NSLog(@"有没有数据呀 %@", dic);
        
        self.modelArr = [NewZYBuyNowModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.mytableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (void)leftBtnAction:(UIButton *)button {
    
    ZYGoodObjectPreViewController *goodVC = [[ZYGoodObjectPreViewController alloc] init];

    [self.navigationController pushViewController:goodVC animated:YES];
    NSLog(@"您点击了 右上按钮");
}

- (void)rightBtnAction:(UIButton *)button {
    
    ZYNewProductViewController *newVC = [[ZYNewProductViewController alloc] init];
    
    [self.navigationController pushViewController:newVC animated:YES];
    NSLog(@"您点击了 右下按钮");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 333;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(0, 0, KWIDTH, 38);
    
    UIImageView *imageV = [[UIImageView alloc] init];
    imageV.frame = CGRectMake(10, 8, 20, 20);
    [imageV setImage:imgname(@"rementuijian")];
    [headerView addSubview:imageV];
    
    UILabel *headerLab = [[UILabel alloc] init];
    headerLab.frame = CGRectMake(imageV.right +5, 8, KWIDTH -20-20-5, 20);
    headerLab.text = @"热门推荐";
    headerLab.textColor = K333333;
    headerLab.textAlignment = NSTextAlignmentLeft;
    [headerView addSubview:headerLab];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYBuyNowTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYBuyNowTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    [mycell.goodImg sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil]; //产品图片
    mycell.proTitLab.text = model.parts_name; //描述信息
    mycell.numTitLab.text = [NSString stringWithFormat:@"%@%@", model.cellCount,@"人订购"];
    
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];

    ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
    vc.shopID = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"配件信息已提交\n可在已处理页面查看配件详情";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"明天继续努力";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    self.navigationController.tabBarController.selectedIndex = 4;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);

        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
