//
//  ZYBuyNowTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewZYBuyNowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYBuyNowTableViewCell : UITableViewCell
@property (nonatomic, copy) void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UIImageView *goodImg; //产品图片
@property (nonatomic, strong) UILabel *comTitlab; //公司名称
@property (nonatomic, strong) UILabel *proTitLab; //描述信息
@property (nonatomic, strong) UILabel *numTitLab; //多少人购买

-(void)refasf:(NewZYBuyNowModel *)model;

@end

NS_ASSUME_NONNULL_END
