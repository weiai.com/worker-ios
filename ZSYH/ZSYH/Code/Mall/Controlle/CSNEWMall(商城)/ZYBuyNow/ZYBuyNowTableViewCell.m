//
//  ZYBuyNowTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBuyNowTableViewCell.h"

@implementation ZYBuyNowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 125);
        bottomV.layer.cornerRadius = 10;
        //bottomV.layer.masksToBounds = YES;
        //bottomV.layer.borderWidth = 1;
        //bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        UIImageView *goodImg = [[UIImageView alloc] init];
        goodImg.frame = CGRectMake(10, 10, 100, 100);
        [bottomV addSubview:goodImg];
        goodImg.layer.cornerRadius = 10;
        goodImg.layer.masksToBounds = YES;
        self.goodImg = goodImg;
        
        //公司名称
        UILabel *comTitLab = [[UILabel alloc] initWithFrame:CGRectMake(goodImg.right + 5, 15, KWIDTH-30-100, 40)];
        //comTitLab.textColor = [UIColor colorWithHexString:@"#45B03A"];
        comTitLab.textColor = K333333;
        comTitLab.font = FontSize(13);
        comTitLab.numberOfLines = 0;
        [bottomV addSubview:comTitLab];
        self.comTitlab = comTitLab;
        
        //公司名称
        UILabel *proTitLab = [[UILabel alloc] initWithFrame:CGRectMake(goodImg.right + 10, comTitLab.bottom + 5, KWIDTH-20-100-10, 20)];
        proTitLab.backgroundColor = [UIColor cyanColor];
        proTitLab.textColor = K333333;
        proTitLab.numberOfLines = 0;
        proTitLab.font = FontSize(13);
        //[bottomV addSubview:proTitLab];
        self.proTitLab = proTitLab;
        
        //立即订购 按钮
        UIButton *orderNowBtn = [[UIButton alloc] init];
        orderNowBtn.frame = CGRectMake(bottomV.right-84, proTitLab.bottom +5, 74, 27);
        //orderNowBtn.backgroundColor = [UIColor orangeColor];
        [orderNowBtn setBackgroundImage:[UIImage imageNamed:@"nowBtnBackimg5"] forState:UIControlStateNormal];
        orderNowBtn.layer.cornerRadius = 27/2;
        orderNowBtn.layer.masksToBounds = YES;
        [orderNowBtn setTitle:@"立即预订" forState:UIControlStateNormal];
        orderNowBtn.titleLabel.font = FontSize(12);
        [orderNowBtn setTitleColor:[UIColor colorWithHexString:@"#397500"] forState:UIControlStateNormal];
        [orderNowBtn addTarget:self action:@selector(orderNowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:orderNowBtn];
        
    }
    return self;
}

- (void)refasf:(NewZYBuyNowModel *)model{
    self.comTitlab.text = model.parts_details;
    [self.goodImg sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil]; //产品图片
    //self.proTitLab.text = model.type_name; //产品详情
    self.numTitLab.text = [NSString stringWithFormat:@"%@%@", model.cellCount,@"人订购"];
}

//立即订购按钮 点击事件处理
- (void)orderNowBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//立即购买
    }
    NSLog(@"您点击了 配件商城列表 Cell里边的立即订购按钮");
}

@end
