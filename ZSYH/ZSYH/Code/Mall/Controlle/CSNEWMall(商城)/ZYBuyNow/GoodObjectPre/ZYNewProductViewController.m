//
//  ZYNewProductViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//
//新品推荐
#import "ZYNewProductViewController.h"
#import "ZYBuyNowTableViewCell.h"
#import "NewZYBuyNowModel.h"
#import "ZYGoodsDetailViewController.h" //产品详情

@interface ZYNewProductViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) NSMutableSet <NSString *>*saoYiSaoStrSet;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *modelArr;

@end

@implementation ZYNewProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;

    self.title = @"整机";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.modelArr = [NSMutableArray arrayWithCapacity:1];

    [self requestData];
    [self showdetaile];
    // Do any additional setup after loading the view.
}

-(void)showdetaile {
    
    kWeakSelf;
    
    [self.view addSubview:self.scrollView];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYBuyNowTableViewCell class] forCellReuseIdentifier:@"ZYBuyNowTableViewCell"];
    _mytableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(KWIDTH/2-95, 155, 190, 166) image:imgname(@"emptyStateImg") viewClick:^{
        [weakSelf.mytableView.mj_header beginRefreshing];
    }];
    _mytableView.placeHolderView.backgroundColor = [UIColor clearColor];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
    self.mytableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self requestData];//请求数据
    }];
    [_scrollView addSubview:_mytableView];
}

- (void)requestData {
    //立即订购 数据请求
    kWeakSelf;

    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *pp = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNum"] = pp;
    param[@"type"] = @"2"; //整机 type:2
    param[@"classification"] = @"2"; //整机 classification:2
    
    [NetWorkTool POST:getPartsList param:param success:^(id dic) {
        
        self.modelArr = [NewZYBuyNowModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYBuyNowTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYBuyNowTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态

    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    [mycell refasf:model];
    
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
        
        switch (ind) {
            case 0: {
                ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
                vc.shopID = model.id;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            default:
                break;
        }
    };
    
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];

    ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
    vc.shopID = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
