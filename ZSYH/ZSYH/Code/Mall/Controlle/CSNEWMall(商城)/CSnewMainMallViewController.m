//
//  CSnewMainMallViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewMainMallViewController.h"
#import "CSSearchViewController.h"
#import "CSBannerModel.h"
#import "ZYBuyNowTableViewCell.h" //新版改动
#import "NewZYBuyNowModel.h"
#import "ZYGoodObjectPreViewController.h" //配件(原:好物优选)
#import "ZYNewProductViewController.h"    //整机(原:新品推荐)
#import "ZYGoodsDetailViewController.h"
#import "BannerDetailsViewController.h" //点击轮播图 跳转详情
#import "InstallRecordsListViewController.h" //安装列表
#import "ApplicationGrantListViewController.h" //申请补助金列表
#import "CSmakeAccAddmodel.h"
#import "HWScanViewController.h"
#import "NewScanCodeInstallViewController.h"
#import "ShenQingBuZhuJinViewController.h"
#import "SaoMaPeiJianDetailModel.h"

@interface CSnewMainMallViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) XSQBanner *banner;
@property (nonatomic, strong) NSMutableArray *modelArr;
@property (nonatomic, strong) NSMutableArray *bannerArr;
@property (nonatomic, strong) UITextField *mytestfd;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) UITableView *myTableView; //新版本改动
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, assign) NSInteger index;
@end

@implementation CSnewMainMallViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.mytestfd.text = @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    self.page = 1;
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 40)];
    self.navigationItem.titleView = backView;
    
    UITextField *testfd = [[UITextField alloc]initWithFrame:CGRectMake(16, 5, KWIDTH-67, 30)];
    testfd.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    testfd.tintColor = zhutiColor;
    //[backView addSubview:testfd];
    
    testfd.layer.masksToBounds = YES;
    testfd.layer.cornerRadius =2;
    testfd.placeholder = @"  请输入产品名称";
    testfd.returnKeyType = UIReturnKeySearch;
    [testfd setBorderStyle: UITextBorderStyleRoundedRect];
    testfd.delegate = self;
    self.mytestfd = testfd;
    
    UIButton *searchB = [UIButton new];
    searchB.frame = CGRectMake(16, 5, KWIDTH-67, 30);
    [searchB setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    [searchB.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [searchB setTitle:@"  请输入产品名称" forState:(UIControlStateNormal)];
    [searchB setTitleColor:[UIColor lightGrayColor] forState:(UIControlStateNormal)];
    searchB.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    searchB.layer.masksToBounds = YES;
    searchB.layer.cornerRadius = 2;
    //设置边框的颜色
    [searchB.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    //设置边框的粗细
    [searchB.layer setBorderWidth:.3];
    [searchB addTarget:self action:@selector(serachAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [backView addSubview:searchB];
    //搜索 按钮
    UIButton *searce = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [searce setImage:imgname(@"saosuo") forState:(UIControlStateNormal)];
    searce.frame = CGRectMake(KWIDTH-95, 0, 40, 40);
    [backView addSubview:searce];
    [searce addTarget:self action:@selector(serachAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    
    [self.view addSubview:self.myTableView];
    
    self.bannerArr = [NSMutableArray arrayWithCapacity:1];
    [self loadBanner];
    
    [self requestData];
}

- (void)serachAction:(UIButton *)but{
    CSSearchViewController *vc = [CSSearchViewController new];
    vc.search = self.mytestfd.text;
    vc.isFirst = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [super textFieldShouldReturn:textField];
    CSSearchViewController *vc = [CSSearchViewController new];
    vc.search = textField.text;
    [self.navigationController pushViewController:vc animated:YES];
    return YES;
}

//轮播图数据请求
-(void)loadBanner{
    [NetWorkTool POST:Mbannerapi param:nil success:^(id dic) {
        KMyLog(@"产品展示 ------ 轮播图 %@", dic);
        self.bannerArr = [CSBannerModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

#pragma mark - 请求数据
- (void)requestData {
    //配件商城 首页 数据请求
    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *pp = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNum"] = pp;
    param[@"type"] = @""; //配件首页 传空
    param[@"classification"] = @""; //配件首页 传空
    
    [NetWorkTool POST:getPartsList param:param success:^(id dic) {
        KMyLog(@"产品展示 数据信息 %@", dic[@"data"]);
        
        self.modelArr = [NewZYBuyNowModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

//新版本UI展示
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    headerView.frame = CGRectMake(0, 0, KWIDTH, 560);
    
    //顶部轮播图
    NSMutableArray *mu = [NSMutableArray arrayWithCapacity:1];
    for (CSBannerModel *model in _bannerArr) {
        [mu addObject:model.pic_url];
    }
    _banner = [[XSQBanner alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 144)];
    _banner.pageLocation = PageControlLocationCenter;
    [_banner updateImageArr:mu AndImageClickBlock:^(NSInteger index) {
        
        self.index = index;
        
        CSBannerModel *model = self.bannerArr[self.index];
        
        if (model.details.length == 0) {
            
        } else {
            BannerDetailsViewController *vc = [[BannerDetailsViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.bannerId = model.id;
            vc.titleStr = model.title;
            vc.partsId = model.partsId;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
    [headerView addSubview:_banner];
    
    //查看安装记录 背景View
    UIImageView *topImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, _banner.bottom + 10, KWIDTH-20, 90)];
    //topImgView.backgroundColor = [UIColor cyanColor];
    [topImgView setImage:imgname(@"topImgView")];
    
    UITapGestureRecognizer *topTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topImgViewAction)];
    [topImgView addGestureRecognizer:topTapGesture];
    topImgView.userInteractionEnabled = YES;
    [headerView addSubview:topImgView];
    
    UILabel *topTitLab = [[UILabel alloc] initWithFrame:CGRectMake(90, 18, KWIDTH-110, 15)];
    //topTitLab.backgroundColor = [UIColor cyanColor];
    topTitLab.text = @"点击扫描要安装产品上的候保二维码";
    topTitLab.font = FontSize(13);
    topTitLab.textColor = [UIColor colorWithHexString:@"#19950C"];
    [topImgView addSubview:topTitLab];
    
    UILabel *topTipLab = [[UILabel alloc] initWithFrame:CGRectMake(90, topTitLab.bottom +5, KWIDTH-180, 15)];
    //topTipLab.backgroundColor = [UIColor cyanColor];
    topTipLab.text = @"一键安装保障维修售后";
    topTipLab.font = FontSize(13);
    topTipLab.textColor = [UIColor colorWithHexString:@"#19950C"];
    [topImgView addSubview:topTipLab];
    
    UIButton *topViewBtn = [[UIButton alloc] initWithFrame:CGRectMake(topImgView.right - 125, topTipLab.bottom + 6.5, 110, 23)];
    topViewBtn.backgroundColor = [UIColor colorWithHexString:@"#19950C"];
    topViewBtn.titleLabel.font = FontSize(12);
    topViewBtn.layer.cornerRadius = 11.5;
    topViewBtn.layer.masksToBounds = YES;
    [topViewBtn setTitle:@"点击查看安装记录" forState:UIControlStateNormal];
    [topViewBtn setImage:imgname(@"smallPointImg") forState:UIControlStateNormal];
    [topViewBtn setTitleEdgeInsets:UIEdgeInsetsMake(0,-topViewBtn.imageView.size.width, 0, topViewBtn.imageView.size.width)];
    [topViewBtn setImageEdgeInsets:UIEdgeInsetsMake(0, topViewBtn.titleLabel.bounds.size.width, 0, -topViewBtn.titleLabel.bounds.size.width)];
    [topViewBtn addTarget:self action:@selector(topViewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [topImgView addSubview:topViewBtn];
    
    //查看工时费补贴记录 背景View
    UIImageView *downImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, topImgView.bottom + 10, KWIDTH-20, 90)];
    //downImgView.backgroundColor = [UIColor orangeColor];
    [downImgView setImage:imgname(@"downImageView")];
    downImgView.userInteractionEnabled = YES;
    [headerView addSubview:downImgView];
    
    UITapGestureRecognizer *buZhuJinTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buZhuJinTapAction)];
    [downImgView addGestureRecognizer:buZhuJinTap];
    
    UILabel *downTitLab = [[UILabel alloc] initWithFrame:CGRectMake(90, 18, KWIDTH-110, 15)];
    //downTitLab.backgroundColor = [UIColor cyanColor];
    downTitLab.text = @"点击扫描损坏产品上候保二维码";
    downTitLab.font = FontSize(13);
    downTitLab.textColor = [UIColor colorWithHexString:@"#FFA800"];
    [downImgView addSubview:downTitLab];
    
    UILabel *downTipLab = [[UILabel alloc] initWithFrame:CGRectMake(90, topTitLab.bottom +5, KWIDTH-180, 15)];
    //downTipLab.backgroundColor = [UIColor cyanColor];
    downTipLab.text = @"一键扫码申请工时费补助金";
    downTipLab.font = FontSize(13);
    downTipLab.textColor = [UIColor colorWithHexString:@"#FFA800"];
    [downImgView addSubview:downTipLab];
    
    UIButton *downViewBtn = [[UIButton alloc] initWithFrame:CGRectMake(topImgView.right - 160,downTipLab.bottom + 6.5, 145, 23)];
    downViewBtn.backgroundColor = [UIColor colorWithHexString:@"#FFA800"];
    downViewBtn.titleLabel.font = FontSize(12);
    downViewBtn.layer.cornerRadius = 11.5;
    downViewBtn.layer.masksToBounds = YES;
    [downViewBtn setTitle:@"点击查看工时费补贴记录" forState:UIControlStateNormal];
    [downViewBtn setImage:imgname(@"smallPointImg") forState:UIControlStateNormal];
    [downViewBtn setTitleEdgeInsets:UIEdgeInsetsMake(0,-downViewBtn.imageView.size.width, 0, downViewBtn.imageView.size.width)];
    [downViewBtn setImageEdgeInsets:UIEdgeInsetsMake(0, downViewBtn.titleLabel.bounds.size.width, 0, -downViewBtn.titleLabel.bounds.size.width)];
    [downViewBtn addTarget:self action:@selector(downViewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [downImgView addSubview:downViewBtn];
    
    //左侧 配件按钮
    UIImageView *peijianImg = [[UIImageView alloc] init];
    peijianImg.frame = CGRectMake(10, downImgView.bottom +10, (KWIDTH-30)/2, 135);
    [peijianImg setImage:imgname(@"peijianLogoImg")];
    [headerView addSubview:peijianImg];
    //配件图片 添加点击事件
    UITapGestureRecognizer *tapGestureL = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftImageAction:)];
    [peijianImg addGestureRecognizer:tapGestureL];
    peijianImg.userInteractionEnabled = YES;
    //右侧 整机按钮
    UIImageView *zhengjiImg = [[UIImageView alloc] init];
    zhengjiImg.frame = CGRectMake(peijianImg.right +10, downImgView.bottom +10, (KWIDTH-30)/2, 135);
    [zhengjiImg setImage:imgname(@"dianqiShopImg")];
    [headerView addSubview:zhengjiImg];
    //整机图片 添加点击事件
    UITapGestureRecognizer *tapGestureR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightImageAction:)];
    [zhengjiImg addGestureRecognizer:tapGestureR];
    zhengjiImg.userInteractionEnabled = YES;
    //分割线
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#F1F1F1"];
    line.frame = CGRectMake(0, peijianImg.bottom +10, KWIDTH, 10);
    [headerView addSubview:line];
    //热门推荐 Logo
    UIImageView *hotImg = [[UIImageView alloc] init];
    [hotImg setImage:imgname(@"rementuijian")];
    hotImg.frame = CGRectMake(10, line.bottom+17, 22, 22);
    [headerView addSubview:hotImg];
    //热门推荐 提示文本
    UILabel *hotLab = [[UILabel alloc] init];
    hotLab.frame = CGRectMake(hotImg.right +10, line.bottom +17, KWIDTH-20-22-10, 22);
    hotLab.text = @"热门推荐";
    hotLab.font = FontSize(16);
    hotLab.textColor = K333333;
    [headerView addSubview:hotLab];
    
    self.headerView = headerView;
    
    return self.headerView;
}

//上面 点击扫描要安装产品的候保二维码 背景图片 点击事件处理
- (void)topImgViewAction {
    NSLog(@"您点击了 上面 点击扫描要安装产品的候保二维码 背景图片");
    if (KUID == nil || [KUID isEqualToString:@""]) {
                 [self clickLogin];
        return;
             }
    kWeakSelf;
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        [weakSelf requesrtWith:str];
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
}
-(void)clickLogin {
    CSLoginViewController *vc = [[CSLoginViewController alloc]init];
    BaseNavViewController *nav = [[BaseNavViewController alloc]initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:YES completion:nil];
}
#pragma mark - 请求二维码中的配件信息
- (void)requesrtWith:(NSString *)strr {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool POST:getScanQrCode param:param success:^(id dic) {
        KMyLog(@"扫描结果%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        
        if (![mudic isKindOfClass:[NSDictionary class]]) {
            ShowToastWithText(@"数据格式不正确");
            return ;
        }
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc] init];
        //新配件
        [mymodel setValuesForKeysWithDictionary:mudic];
        mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
        mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
        mymodel.ne_name =[mudic objectForKey:@"parts_name"];
        mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.ne_isSaoyisao = @"1";
        
        NewScanCodeInstallViewController *vc = [[NewScanCodeInstallViewController alloc] init];
        vc.accessAddModel = mymodel;
        vc.qrcodeStr = mymodel.qrcode;
        [self.navigationController pushViewController:vc animated:YES];
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

//查看安装记录 按钮 点击事件
- (void)topViewBtnAction:(UIButton *)button {
    NSLog(@"查看安装记录");
    InstallRecordsListViewController *vc = [[InstallRecordsListViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

//查看工时费补贴记录 按钮 点击事件
- (void)downViewBtnAction:(UIButton *)button {
    NSLog(@"查看工时费补贴记录");
    ApplicationGrantListViewController *vc = [[ApplicationGrantListViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

//扫码进入 申请补助金界面
- (void)buZhuJinTapAction {
    if (KUID == nil || [KUID isEqualToString:@""]) {
                    [self clickLogin];
           return;
                }
    kWeakSelf;
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        [weakSelf requestShenQingBuZhuJinInfo:str];
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
}

#pragma mark - 请求申请补助金 信息
- (void)requestShenQingBuZhuJinInfo:(NSString *)str {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = str;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool POST:getScanQrCode param:param success:^(id dic) {
        KMyLog(@"扫描结果%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        SaoMaPeiJianDetailModel *mymodel = [SaoMaPeiJianDetailModel mj_objectWithKeyValues:dic[@"data"]];
        
        ShenQingBuZhuJinViewController *vc = [[ShenQingBuZhuJinViewController alloc] init];
        vc.detailModel = mymodel;
        vc.qrCode = mymodel.qrcode;
        [self.navigationController pushViewController:vc animated:YES];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 560;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    //通过唯一标识创建Cell实例
    ZYBuyNowTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYBuyNowTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    [mycell refasf:model];
    
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
        switch (ind) {
            case 0: {
                ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
                vc.shopID = model.id;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewZYBuyNowModel *model = [self.modelArr safeObjectAtIndex:indexPath.row];
    ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
    vc.shopID = model.id;
    [self.navigationController pushViewController:vc animated:YES];
}

//左侧 配件按钮 点击事件处理
- (void)leftImageAction:(UIButton *)button {
    ZYGoodObjectPreViewController *buyNowVC = [[ZYGoodObjectPreViewController alloc] init];
    [self.navigationController pushViewController:buyNowVC animated:YES];
    NSLog(@"您点击了 左侧 配件按钮");
}

//右侧 整机按钮 点击事件处理
- (void)rightImageAction:(UIButton *)button {
    ZYNewProductViewController *buyNowVC = [[ZYNewProductViewController alloc] init];
    [self.navigationController pushViewController:buyNowVC animated:YES];
    NSLog(@"您点击了 右侧 整机按钮");
}

-(UITableView *)myTableView{
    
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight-kTabbarHeight) style:(UITableViewStyleGrouped)];
        _myTableView.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        [_myTableView registerClass:[ZYBuyNowTableViewCell class] forCellReuseIdentifier:@"ZYBuyNowTableViewCell"];
        kWeakSelf;
        
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(KWIDTH/2-76, KHEIGHT-kNaviHeight-kTabbarHeight-135, 152, 135) image:imgname(@"emptyStateImg") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        _myTableView.placeHolderView.backgroundColor = [UIColor clearColor];
    }
    return _myTableView;
}

@end
