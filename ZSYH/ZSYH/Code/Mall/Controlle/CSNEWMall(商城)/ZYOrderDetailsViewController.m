//
//  ZYOrderDetailsViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYOrderDetailsViewController.h"
#import "ZYOrderDetailsTableViewCell.h"
#import "ZYMyOrderGoodsModel.h"

@interface ZYOrderDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,ZYOrderDetailsCellDelegate>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIButton *secSelBtn; //分区区头选择按钮
@property (nonatomic, strong) UIView *bgViewsecCancle; //取消原因 弹窗
@property (nonatomic, strong) UILabel *shopTitLab;
@property (nonatomic, strong) UITextView *cancelTV;

@property (nonatomic, strong) UILabel *dateConLab; //下单时间
@property (nonatomic, strong) UILabel *nameConLab; //名称
@property (nonatomic, strong) UILabel *phoneLab;   //手机号
@property (nonatomic, strong) UILabel *addConLab;  //收货地址
@property (nonatomic, strong) NSDictionary *detailDic;
@property (nonatomic, strong) NSString *orderId;

@end

@implementation ZYOrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"预订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.detailDic = [NSDictionary dictionary];
    
    [self requestData];
    [self showdetaile];
    [self shwoBgviewsecCancle]; //取消原因
    // Do any additional setup after loading the view.
}

-(void)showdetaile {
    
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight, KWIDTH, 120)];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    bgview.backgroundColor = [UIColor whiteColor];
    
    UILabel *dateTitLab = [[UILabel alloc] init];
    dateTitLab.frame = CGRectMake(16, 11, 70, 20);
    dateTitLab.text = @"下单时间";
    dateTitLab.font = FontSize(14);
    dateTitLab.textColor = K666666;
    [bgview addSubview:dateTitLab];

    UILabel *dateConLab = [[UILabel alloc] init];
    dateConLab.frame = CGRectMake(dateTitLab.right +23, 11, KWIDTH-32-70-20, 20);
    dateConLab.font = FontSize(12);
    dateTitLab.textColor = K999999;
    [bgview addSubview:dateConLab];
    self.dateConLab = dateConLab;

    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line.frame = CGRectMake(10, dateTitLab.bottom +8, KWIDTH -20, 1);
    [bgview addSubview:line];

    UILabel *nameTitLab = [[UILabel alloc] init];
    nameTitLab.frame = CGRectMake(16, line.bottom +9, 70, 20);
    nameTitLab.text = @"";
    nameTitLab.font = FontSize(12);
    nameTitLab.textColor = K666666;
    [bgview addSubview:nameTitLab];

    UILabel *nameConLab = [[UILabel alloc] init];
    nameConLab.frame = CGRectMake(nameTitLab.right +23, line.bottom +9, (KWIDTH -32-70-20)/2, 20);
    nameConLab.font = FontSize(14);
    nameConLab.textColor = [UIColor colorWithHexString:@"#272727"];
    [bgview addSubview:nameConLab];
    self.nameConLab = nameConLab;

    UILabel *phoneLab = [[UILabel alloc] init];
    phoneLab.frame = CGRectMake(nameConLab.right, line.bottom +9, (KWIDTH -32-70-20)/2, 20);
    phoneLab.textAlignment = NSTextAlignmentRight;
    phoneLab.font = FontSize(14);
    phoneLab.textColor = [UIColor colorWithHexString:@"#272727"];
    [bgview addSubview:phoneLab];
    self.phoneLab = phoneLab;

    UILabel *addTitLab = [[UILabel alloc] init];
    addTitLab.frame = CGRectMake(16, nameTitLab.bottom +9, 70, 20);
    addTitLab.text = @"收货地址";
    addTitLab.font = FontSize(14);
    addTitLab.textColor = K666666;
    [bgview addSubview:addTitLab];

    UILabel *addConLab = [[UILabel alloc] init];
    addConLab.frame = CGRectMake(addTitLab.right +23, nameConLab.bottom +5, KWIDTH-32-70-20, 40);
    addConLab.numberOfLines = 0;
    addConLab.font = FontSize(12);
    addConLab.textColor = K666666;
    [bgview addSubview:addConLab];
    self.addConLab = addConLab;
    
    //self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 56)];
    
    _mytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, bgview.bottom +2, KWIDTH, KHEIGHT-kNaviHeight-bgview.height-2) style:UITableViewStyleGrouped];
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYOrderDetailsTableViewCell class] forCellReuseIdentifier:@"ZYOrderDetailsTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    _mytableView.tableFooterView = self.tabFootView;
    [_scrollView addSubview:_mytableView];
}

//获取订单详情 数据请求
- (void)requestData {
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] =NOTNIL(self.orderIdStr);
    param[@"user_type"] = @"1"; //用户类型
    
    [weakSelf.mydateSource removeAllObjects];
    [NetWorkTool POST:getPartsOrderInfo param:param success:^(id dic) {
        
        self.detailDic = dic[@"data"];
        KMyLog(@"----------预订单详情 ---------- %@", dic);
        self.mydateSource = [ZYMyOrderGoodsModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"parts"]];
        self.dateConLab.text = self.detailDic[@"createTime"];  //下单时间
        self.nameConLab.text = self.detailDic[@"user_name"];   //用户姓名
        self.phoneLab.text = self.detailDic[@"user_phone"];    //用户手机号
        self.addConLab.text = self.detailDic[@"user_address"]; //用户地址
        
        [self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
    } other:^(id dic) {
        //[weakSelf.mydateSource removeAllObjects];
        //[self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        //[weakSelf.mydateSource removeAllObjects];
        //[self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 250;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(20, 0, KWIDTH-40, 34);
    
    UIButton *secSelBtn = [[UIButton alloc] init];
    secSelBtn.frame = CGRectMake(16, 4.5, 25, 25);
    [secSelBtn setImage:[UIImage imageNamed:@"dianpu (1)"] forState:UIControlStateNormal];
    [secSelBtn addTarget:self action:@selector(secSelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:secSelBtn];
    self.secSelBtn = secSelBtn;
    
    UILabel *shopTitLab = [[UILabel alloc] init];
    shopTitLab.frame = CGRectMake(secSelBtn.right +2, 10, KWIDTH-32-25-2-2-20, 17);
    shopTitLab.numberOfLines = 0;
    shopTitLab.text = [NSString stringWithFormat:@"%@",@"主事丫环生产厂"];
    shopTitLab.font = FontSize(14);
    shopTitLab.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [headerView addSubview:shopTitLab];
    self.shopTitLab = shopTitLab;
    
    UIImageView *topBgImg = [[UIImageView alloc] init];
    topBgImg.frame = CGRectMake(shopTitLab.right +3, 10, 20, 20);
    [topBgImg setImage:imgname(@"jinruimage")];
    [headerView addSubview:topBgImg];
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line.frame = CGRectMake(0, 39, KWIDTH, 1);
    [headerView addSubview:line];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYOrderDetailsTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYOrderDetailsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    //self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    
    //数据赋值
    ZYMyOrderGoodsModel *model = [self.mydateSource safeObjectAtIndex:indexPath.row];
    
    self.shopTitLab.text = self.detailDic[@"factory_name"]; //店铺名称
    
    mycell.indexPath = indexPath;
    mycell.delegate = self;
    
    [mycell refasf:model];

    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 1: {
                //取消原因 弹窗
                [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecCancle];
                self.orderId = model.Id;
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"您点击了第 %ld 个分区 第%ld行Cell", indexPath.section, indexPath.row);
}

//分区区头 选择按钮 点击事件处理
- (void)secSelBtnAction:(UIButton *)button {
    NSLog(@"您点击了 分区 区头 的选择按钮");
}

/**
 弹出框的背景图 取消原因
 */
-(void)shwoBgviewsecCancle{
    self.bgViewsecCancle = [[UIView alloc]init];
    self.bgViewsecCancle.frame = self.view.bounds;
    self.bgViewsecCancle.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecCancle addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-22*2)/2-30, 34, 60, 60)];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"tipImage"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 19, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"确定取消该订预订单?";
    
    UIButton *cancelButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancelButton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [cancelButton setBackgroundColor:[UIColor whiteColor]];
    cancelButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius = 4;
    cancelButton.layer.borderWidth = 1;
    cancelButton.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    cancelButton.titleLabel.font = FontSize(14);
    [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.offset(28);
        make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.bottom.offset(-19);
    }];
    
    UIButton *confirmButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [confirmButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:1]];
    confirmButton.layer.masksToBounds = YES;
    confirmButton.layer.cornerRadius = 4;
    confirmButton.titleLabel.font = FontSize(14);
    [confirmButton addTarget:self action:@selector(confirmButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

//取消弹窗 暂不取消按钮 点击事件
-(void)cancelButtonAction:(UIButton *)but{
    [_bgViewsecCancle removeFromSuperview];
    NSLog(@"您点击了 暂不取消 按钮");
}

//取消弹窗 确认取消按钮 点击事件
-(void)confirmButtonAction:(UIButton *)but{
    [self cancleReason];
    NSLog(@"您点击了 确定取消 按钮");
}

//取消购物订单 数据请求
- (void)cancleReason {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"id"] = NOTNIL(self.orderId); //订单id
    param[@"reason"] = @""; //取消原因 文本
    
    [NetWorkTool POST:cancelOrders param:param success:^(id dic) {
        [self.bgViewsecCancle removeFromSuperview];
        [self requestData];
        [self.mytableView reloadData];
        //发送取消候保预订单成功的通知
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CANCLE_HB_ORDER_PART_SUCCESS" object:nil];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
