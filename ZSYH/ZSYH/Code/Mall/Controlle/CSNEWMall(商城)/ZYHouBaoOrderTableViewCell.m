//
//  ZYHouBaoOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHouBaoOrderTableViewCell.h"

@implementation ZYHouBaoOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    //frame.origin.y += 10;
    //frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH, 131);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UIImageView *proImg = [[UIImageView alloc] init];
        proImg.frame = CGRectMake(16, 13, 59, 59);
        proImg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [bottomV addSubview:proImg];
        self.proImg = proImg;
        
        UILabel *proTitLab = [[UILabel alloc] init];
        proTitLab.frame = CGRectMake(proImg.right +10, 10, KWIDTH-32-59-13, 40);
        proTitLab.font = FontSize(14);
        proTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        proTitLab.numberOfLines = 0;
        proTitLab.textAlignment = NSTextAlignmentLeft;
        [bottomV addSubview:proTitLab];
        self.proTitLab = proTitLab;
        
        UILabel *typeTitLab = [[UILabel alloc] init];
        typeTitLab.frame = CGRectMake(proImg.right +10, proTitLab.bottom +7, 30, 20);
        typeTitLab.text = @"型号";
        typeTitLab.font = FontSize(14);
        typeTitLab.textColor = K333333;
        [bottomV addSubview:typeTitLab];
        self.typeTitLab = typeTitLab;
        
        UILabel *typeConLab = [[UILabel alloc] init];
        typeConLab.frame = CGRectMake(typeTitLab.right +10, proTitLab.bottom +7, KWIDTH/2, 20);
        typeConLab.font = FontSize(14);
        typeConLab.textColor = K333333;
        [bottomV addSubview:typeConLab];
        self.typeConLab = typeConLab;
        
        //修改数量
        UIButton *changeQuBtn = [[UIButton alloc] init];
        changeQuBtn.frame = CGRectMake(bottomV.right - 162, typeTitLab.bottom +10, 69, 26);
        //changeQuBtn.layer.borderWidth = 1;
        //changeQuBtn.layer.borderColor = [UIColor colorWithHexString:@"#979797"].CGColor;
        //[changeQuBtn setImage:[UIImage imageNamed:@"组 3550"] forState:UIControlStateNormal];
        //[changeQuBtn addTarget:self action:@selector(changeQuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        //[bottomV addSubview:changeQuBtn];
        self.changeQuBtn = changeQuBtn;
        
        //取消预订
        UIButton *cancleOrBtn = [[UIButton alloc] init];
        cancleOrBtn.frame = CGRectMake(changeQuBtn.right +8, typeTitLab.bottom +10, 69, 26);
        [cancleOrBtn setTitle:@"取消预订" forState:UIControlStateNormal];
        cancleOrBtn.titleLabel.font = FontSize(14);
        [cancleOrBtn setTitleColor:[UIColor colorWithHexString:@"#9B9B9B"] forState:UIControlStateNormal];
        cancleOrBtn.layer.borderWidth = 1;
        cancleOrBtn.layer.borderColor = [UIColor colorWithHexString:@"#9B9B9B"].CGColor;
        cancleOrBtn.layer.cornerRadius = 5;
        cancleOrBtn.layer.masksToBounds = YES;
        [cancleOrBtn addTarget:self action:@selector(cancleOrBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:cancleOrBtn];
        self.cancleOrBtn = cancleOrBtn;
    }
    return self;
}

-(void)refasf:(ZYMyOrderGoodsModel *)model {
    
    [self.proImg sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil];
    self.proTitLab.text = model.parts_details;
    self.typeConLab.text = model.name;
    self.countLab.text = [NSString stringWithFormat:@"%@%@%@",@"共计", model.count, @"个"];
}

//取消预订 按钮点击事件
- (void)cancleOrBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//取消预订
    }
    NSLog(@"您点击了 候保预订单 取消预订 按钮");
}

@end
