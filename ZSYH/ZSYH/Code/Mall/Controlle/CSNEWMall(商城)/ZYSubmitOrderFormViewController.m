//
//  ZYSubmitOrderFormViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYSubmitOrderFormViewController.h"
#import "ZYSubmitOrderFormTableViewCell.h"
#import "HFAddressViewController.h" //我的地址
#import "ZYMyOrderStoresModel.h"//订单model
#import "ZYMyOrderGoodsModel.h"//产品model
#import "HFAddressModel.h"//收获地址model

@interface ZYSubmitOrderFormViewController ()<UITableViewDelegate,UITableViewDataSource>{
    __block BOOL _isRequesting;//是否正在请求数据
}
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *tabFootView;

@property (nonatomic, assign) BOOL rowChose;

@property (nonatomic, strong) UIView *bgViewsec; //订购成功 弹窗
@property (nonatomic, strong) UIView *bgViewsecAddress; //添加地址 弹窗

@property (nonatomic, strong) UIView *viewAddress;//收货地址
@property (nonatomic, strong) UILabel *lblAddName;//收货人姓名
@property (nonatomic, strong) UILabel *lblAddPhone;//收货人联系方式
@property (nonatomic, strong) UILabel *lblAddress;//收货人地址
@property (nonatomic, strong) UIView *viewAddressNone;//无收获地址
@property (nonatomic, strong) HFAddressModel *addressModel;//选择的收获地址
@property (nonatomic, assign) BOOL haveSelectedAddress;
@property (nonatomic, assign) NSInteger is_default;
@end

@implementation ZYSubmitOrderFormViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    if (!self.addressModel) {
         [self requestAddressData];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    //置空
    self.haveSelectedAddress = NO;
    self.is_default = 0;
    self.addressModel = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"提交预订单";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self showdetaile];
    
    [self shwoBgviewsec]; //订购成功 弹窗
    [self shwoBgviewsecAdd]; //添加地址 弹窗
}
#pragma mark - 界面布局
-(void)showdetaile {
    
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight +10, KWIDTH, 90)];
    bgview.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    
    _viewAddress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 90)];
    _viewAddress.hidden = YES;
    [bgview addSubview:_viewAddress];
    
    UILabel *nameTitLab = [[UILabel alloc] init];
    nameTitLab.frame = CGRectMake(16, 11, 70, 20);
    nameTitLab.text = @"收件人:";
    nameTitLab.font = FontSize(14);
    nameTitLab.textColor = K666666;
    [_viewAddress addSubview:nameTitLab];
    
    _lblAddName = [[UILabel alloc] init];
    _lblAddName.frame = CGRectMake(nameTitLab.right +2, 12, (KWIDTH-32-70-20)/2, 20);
    _lblAddName.font = FontSize(14);
    _lblAddName.textColor = [UIColor colorWithHexString:@"#272727"];
    [_viewAddress addSubview:_lblAddName];
    
    _lblAddPhone = [[UILabel alloc] init];
    _lblAddPhone.frame = CGRectMake(_lblAddName.right, 12, (KWIDTH-32-70-20)/2, 20);
    _lblAddPhone.textAlignment = NSTextAlignmentRight;
    _lblAddPhone.font = FontSize(14);
    _lblAddPhone.textColor = [UIColor colorWithHexString:@"#272727"];
    [_viewAddress addSubview:_lblAddPhone];
    
    UILabel *addTitLab = [[UILabel alloc] init];
    addTitLab.frame = CGRectMake(16, nameTitLab.bottom +5, 70, 20);
    addTitLab.text = @"收货地址:";
    addTitLab.font = FontSize(14);
    addTitLab.textColor = K666666;
    [_viewAddress addSubview:addTitLab];
    
    _lblAddress = [[UILabel alloc] init];
    _lblAddress.frame = CGRectMake(addTitLab.right +2, _lblAddName.bottom +5, KWIDTH-32-70-20, 40);
    _lblAddress.numberOfLines = 2;
    _lblAddress.font = FontSize(14);
    _lblAddress.textColor = K666666;
    [_viewAddress addSubview:_lblAddress];
    
    UIImageView *topBgImg = [[UIImageView alloc] init];
    topBgImg.frame = CGRectMake(KWIDTH-5-24, 31, 24, 28);
    [topBgImg setImage:imgname(@"jinruimage")];
    [bgview addSubview:topBgImg];
    
    _viewAddressNone = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, 90)];
    _viewAddressNone.hidden = NO;
    [bgview addSubview:_viewAddressNone];
    
    UIImageView *centerImg = [[UIImageView alloc] init];
    centerImg.frame = CGRectMake(KWIDTH/2-20, 10, 40, 40);
    [centerImg setImage:imgname(@"address")];
    [_viewAddressNone addSubview:centerImg];
    
    UILabel *bottomLab = [[UILabel alloc] init];
    bottomLab.frame = CGRectMake(0, centerImg.bottom +5, KWIDTH, 20);
    bottomLab.text = @"请添加收货地址";
    bottomLab.font = FontSize(14);
    bottomLab.textColor = K666666;
    bottomLab.textAlignment = NSTextAlignmentCenter;
    [_viewAddressNone addSubview:bottomLab];
    
    UIImageView *ivArrow = [[UIImageView alloc] init];
    ivArrow.frame = CGRectMake(centerImg.right-40, 30, 20, 30);
    [ivArrow setImage:imgname(@"jinruimage")];
    [_viewAddressNone addSubview:ivArrow];
    
    UIButton *btnAddress = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddress.frame = CGRectMake(0, 0, KWIDTH, 90);
    [btnAddress addTarget:self action:@selector(btnAddressClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:btnAddress];
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 0)];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, bgview.bottom +10, KWIDTH, KHEIGHT-kNaviHeight-10-90-2-56);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_mytableView registerClass:[ZYSubmitOrderFormTableViewCell class] forCellReuseIdentifier:@"ZYSubmitOrderFormTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    _mytableView.tableFooterView = self.tabFootView;
    [_scrollView addSubview:_mytableView];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"提交预订单" forState:(UIControlStateNormal)];
    tuiusongBut.frame = CGRectMake(0, KHEIGHT - 56, KWIDTH, 56);
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:tuiusongBut];

}
#pragma mark - 请求收获地址
- (void)requestAddressData{
    kWeakSelf;
    [NetWorkTool POST:CmyaddressList param:nil success:^(id dic) {
        NSLog(@"是否有默认地址 ----%@", dic);
        for (NSDictionary *dict in dic[@"data"]) {
            HFAddressModel *model = [[HFAddressModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            
            NSString *isDefault = [NSString stringWithFormat:@"%ld", (long)model.is_default];
            
            if ([isDefault isEqualToString:@""]||[isDefault isEqualToString:@"0"]) {
                //无默认地址
                weakSelf.viewAddress.hidden = YES;
                weakSelf.viewAddressNone.hidden = NO;
                weakSelf.addressModel = model;
            } else if ([isDefault isEqualToString:@"1"]) {
                //有默认地址
                weakSelf.viewAddress.hidden = NO;
                weakSelf.viewAddressNone.hidden = YES;
                weakSelf.lblAddName.text = model.user_name;
                weakSelf.lblAddPhone.text = model.phone;
                weakSelf.lblAddress.text = [NSString stringWithFormat:@"%@%@",model.addressName,model.address];
                weakSelf.addressModel = model;
                return ;
            } else {   //没有选择 并且 没有设置过默认
                if (self.haveSelectedAddress == NO && self.is_default ==0)
                {
                    //请求接口 存在默认的 显示默认的
                    if (self.addressModel) {
                        weakSelf.viewAddress.hidden = NO;
                        weakSelf.viewAddressNone.hidden = YES;
                        weakSelf.lblAddName.text = weakSelf.addressModel.user_name;
                        weakSelf.lblAddPhone.text = weakSelf.addressModel.phone;
                        weakSelf.lblAddress.text = [NSString stringWithFormat:@"%@%@",weakSelf.addressModel.addressName,weakSelf.addressModel.address];
                        return ;
                    } else  {
                        weakSelf.viewAddress.hidden = YES;
                        weakSelf.viewAddressNone.hidden = NO;
                    }
                }
                //有选择 或者 设置过默认
                else {
                    if (weakSelf.addressModel.user_name.length>0||weakSelf.addressModel.phone.length>0||weakSelf.addressModel.address.length>0) {
                    
                        weakSelf.viewAddress.hidden = NO;
                        weakSelf.viewAddressNone.hidden = YES;
                        
                        self.lblAddName.text = weakSelf.addressModel.user_name;
                        self.lblAddPhone.text = weakSelf.addressModel.phone;
                        self.lblAddress.text = [NSString stringWithFormat:@"%@%@",weakSelf.addressModel.addressName,weakSelf.addressModel.address];
                        return ;
                    }
                }
            }
        }
    } other:^(id dic) {
        weakSelf.viewAddress.hidden = YES;
        weakSelf.viewAddressNone.hidden = NO;
        
    } fail:^(NSError *error) {
        weakSelf.viewAddress.hidden = YES;
        weakSelf.viewAddressNone.hidden = NO;
        
    } needUser:YES];
}
#pragma mark - 地址选择点击
- (void)btnAddressClickAction:(UIButton *)sender{
    //地址选择列表
    kWeakSelf;
    HFAddressViewController *vc = [[HFAddressViewController alloc] init];
    vc.chooseAddressBlock = ^(HFAddressModel * _Nonnull model) {
        [weakSelf updateAddressWith:model];
    };
    [self.navigationController pushViewController:vc animated:YES];
}
//更新收货地址
- (void)updateAddressWith:(HFAddressModel *)model{
    if (model) {
        //点击或者点击默认 传回的地址状态
        self.haveSelectedAddress = YES;
        self.is_default = model.is_default;
        
        self.addressModel = model;
        self.viewAddress.hidden = NO;
        self.viewAddressNone.hidden = YES;
        self.lblAddName.text = model.user_name;
        self.lblAddPhone.text = model.phone;
        self.lblAddress.text = [NSString stringWithFormat:@"%@%@",model.addressName,model.address];
    }
}

#pragma mark - 提交
-(void)tuisongAction:(UIButton *)but{
    
    if (self.addressModel) {
        if (_isRequesting) {
            return;
        }
        _isRequesting = YES;
        __block NSString *orderIDStr = @"";//订单ID拼接
        [self.orderArray enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull objPart, NSUInteger idx, BOOL * _Nonnull stop) {
                orderIDStr = [orderIDStr stringByAppendingString: orderIDStr.length > 0 ? [NSString stringWithFormat:@",%@",objPart.Id] : objPart.Id];
            }];
        }];
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObjectIfNotNil:@"1" forKey:@"user_type"];//2所有经销商 1维修工
        [dict setObjectIfNotNil:self.addressModel.id forKey:@"address"];
        [dict setObjectIfNotNil:orderIDStr forKey:@"ids"];
        [NetWorkTool POST:USER_SUBMIT_ORDER_CAR param:dict success:^(id dic) {
            [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];
            //更新订购车数据
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_ADD_ORDER_SUCCESS" object:nil];
        } other:^(id dic) {
            self->_isRequesting = NO;
        } fail:^(NSError *error) {
            self->_isRequesting = NO;
        } needUser:YES];
    } else {
        //提示添加收货地址
        [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecAddress];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.orderArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ZYMyOrderStoresModel *model = [self.orderArray objectAtIndexSafe:section];
    return model.parts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 235;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ZYMyOrderStoresModel *model = [self.orderArray objectAtIndexSafe:section];
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(20, 0, KWIDTH-40, 40);
    
    UIImageView *ivShop = [[UIImageView alloc] initWithFrame:CGRectMake(16, 10, 20, 20)];
    ivShop.image = imgname(@"dianpu (1)");
    [headerView addSubview:ivShop];
    
    UILabel *shopTitLab = [[UILabel alloc] init];
    shopTitLab.frame = CGRectMake(ivShop.right + 2, 10, KWIDTH-20-6-30-20-10, 20);
    shopTitLab.text = model.factory_name;
    shopTitLab.numberOfLines = 0;
    shopTitLab.font = FontSize(14);
    shopTitLab.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [headerView addSubview:shopTitLab];
    
    UIButton *btnShop = [UIButton buttonWithType:UIButtonTypeCustom];
    btnShop.frame = CGRectMake(0, 0, kScreen_Width, 40);
    [btnShop addTarget:self action:@selector(btnShopClickAction:) forControlEvents:UIControlEventTouchUpInside];
    btnShop.tag = 100 + section;
    [headerView addSubview:btnShop];

    return headerView;
}
- (void)btnShopClickAction:(UIButton *)sender{
    NSInteger index = sender.tag - 100;
    ZYMyOrderStoresModel *model = [self.orderArray objectAtIndexSafe:index];
    KMyLog(@"店铺名称%@-%@",model.factory_name,model.Id);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ZYMyOrderStoresModel *model = [self.orderArray objectAtIndexSafe:indexPath.section];
    ZYMyOrderGoodsModel *goodModel = [model.parts objectAtIndexSafe:indexPath.row];
    
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYSubmitOrderFormTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYSubmitOrderFormTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.goodsModel = goodModel;
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

/**
 弹出框的背景图  提交
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"预订成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"可在候保预订单中查看";
    //self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow1:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
}

-(void)ikenow1:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 弹出框的背景图  提交
 */
-(void)shwoBgviewsecAdd {
    self.bgViewsecAddress = [[UIView alloc]init];
    self.bgViewsecAddress.frame = self.view.bounds;
    self.bgViewsecAddress.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecAddress addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-30, 34, 60, 60)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 30;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 1461"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"";
    
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, KWIDTH-44, 34)];
    [whiteBGView addSubview:topLable];
    topLable.font = FontSize(16);
    topLable.numberOfLines = 0;
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.textAlignment =  NSTextAlignmentCenter;
    topLable.text = @"请先添加收货地址";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow2:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-29);
    }];
    
}

-(void)ikenow2:(UIButton *)but{
    [_bgViewsecAddress removeFromSuperview];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
