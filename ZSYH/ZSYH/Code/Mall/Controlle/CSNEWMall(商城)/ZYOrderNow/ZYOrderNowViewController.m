//
//  ZYOrderNowViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYOrderNowViewController.h"
#import "HomeGoodsCell.h"
#import "CSPIngtaiViewController.h"

#import "CSnewMallViewController.h" //原来的 综合 离我最近 销量最高
#import "ZYAccessoriesViewController.h" //配件
#import "ZYMachineViewController.h" //整机

#import "ZYFeedbackViewController.h" //留言反馈
#import "ZYGoodsDetailViewController.h" //产品详情

@interface ZYOrderNowViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) XSQBanner *banner;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic,assign) NSInteger page;

@end

@implementation ZYOrderNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"立即购买";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.page = 1;
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [self showdetaile];
    
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    [self requestGoods];
    
    // Do any additional setup after loading the view.
}

-(void)showdetaile {
    
    [self.view addSubview:self.scrollView];
    
    //左侧 配件按钮
    UIButton *leftBtn = [[UIButton alloc] init];
    leftBtn.frame = CGRectMake(10, 10, (KWIDTH-30)/2, 194);
    [leftBtn setImage:imgname(@"组 3421") forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:leftBtn];
    
    //右侧 整机按钮
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(2 +(KWIDTH-14)/2 +10 , 10, (KWIDTH-30)/2, 194);
    [rightBtn setImage:imgname(@"组 3422") forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:rightBtn];
}

- (void)requestGoods {
    if (_page == 1) {
        [_modelArr removeAllObjects];
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *pp = [NSString stringWithFormat:@"%ld",(long)self.page];
    param[@"pageNum"] = pp;
    
    [NetWorkTool POST:MmallHomeAD param:param success:^(id dic) {

        self.modelArr = [GoodListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.collectionView reloadData];
        self.page == 1 ?[self.collectionView.mj_header endRefreshing]:[self.collectionView.mj_footer endRefreshing];
    } other:^(id dic) {
        
        //_page == 1 ?[_collectionView.mj_header endRefreshing]:[_collectionView.mj_footer endRefreshing];
        [self.collectionView.mj_header endRefreshing];
        //[_collectionView.mj_footer endRefreshingWithNoMoreData];
        [self.collectionView.mj_footer endRefreshing];
    } fail:^(NSError *error) {
        self.page == 1 ?[self.collectionView.mj_header endRefreshing]:[self.collectionView.mj_footer endRefreshing];
        
    } needUser:NO];
}

#pragma mark - CollectionView -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];
    
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    model.productBelType = @"1";
    [cell refresh:model];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"head" forIndexPath:indexPath];
        
        header.backgroundColor = [UIColor whiteColor];
        
        //UILabel *malable = [[UILabel alloc]initWithFrame:CGRectMake(0, 154+22, KWIDTH, 28)];
        //malable.text = @"常见配件推荐";
        //malable.textAlignment = NSTextAlignmentCenter;
        //[header addSubview:malable];
        
        //左侧 配件按钮
        UIButton *leftBtn = [[UIButton alloc] init];
        leftBtn.frame = CGRectMake(10, 10, (KWIDTH-30)/2, 194);
        [leftBtn setImage:imgname(@"组 3421") forState:UIControlStateNormal];
        [leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [header addSubview:leftBtn];

        //右侧 整机按钮
        UIButton *rightBtn = [[UIButton alloc] init];
        rightBtn.frame = CGRectMake(2 +(KWIDTH-14)/2 +10 , 10, (KWIDTH-30)/2, 194);
        [rightBtn setImage:imgname(@"组 3422") forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [header addSubview:rightBtn];
        
        return header;
    }
    return nil;
}

//左侧 配件按钮 点击事件
- (void)leftBtnAction:(UIButton *)button {
//    CSnewMallViewController *vc = [[CSnewMallViewController alloc] init];
//    //[self.navigationController pushViewController:vc animated:YES];
    ZYAccessoriesViewController *accVC = [[ZYAccessoriesViewController alloc] init];
    [self.navigationController pushViewController:accVC animated:YES];
    
    //提交留言
//    ZYFeedbackViewController *feedVC = [[ZYFeedbackViewController alloc] init];
//    [self.navigationController pushViewController:feedVC animated:YES];

    NSLog(@"您点击了 左侧 配件按钮");
}

//右侧 整机按钮 点击事件
- (void)rightBtnAction:(UIButton *)button {
    
    //CSnewMallViewController *vc = [[CSnewMallViewController alloc] init];
    //[self.navigationController pushViewController:vc animated:YES];
    
    //ZYMachineViewController *macVC = [[ZYMachineViewController alloc] init];
    //[self.navigationController pushViewController:macVC animated:YES];
    ShowToastWithText(@"暂未开放,敬请期待");
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    //CSPIngtaiViewController *vc = [CSPIngtaiViewController new];
    //vc.goodsID = model.Id;
    
    ZYGoodsDetailViewController *vc = [[ZYGoodsDetailViewController alloc] init];
    vc.shopID = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(KWIDTH/2 - 15, 240);
        layout.headerReferenceSize = CGSizeMake(KWIDTH, 214);
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight-kTabbarHeight) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = RGBA(237, 237, 237, 1);
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.page = 1;
            [self requestGoods];
        }];
        
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            self.page += 1;
            [self requestGoods];
        }];
        [_collectionView reloadData];
    }
    return _collectionView;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
