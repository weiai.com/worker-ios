//
//  ZYMyOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYMyOrderGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZYMyOrderGoodsCellDelegate <NSObject>
//选中按钮点击
- (void)orderGoodsCellClick:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected;
@end

@interface ZYMyOrderTableViewCell : UITableViewCell

@property (nonatomic, strong) NSIndexPath *indexPath;//位置
@property (nonatomic, strong) ZYMyOrderGoodsModel *goodsModel;
@property (nonatomic, weak) id <ZYMyOrderGoodsCellDelegate> delegate;

@property (nonatomic, strong) UIImageView *proImg;
@property (nonatomic, strong) UILabel *proTitLab;
@property (nonatomic, strong) UILabel *typeConLab;

+ (CGFloat)cellHeight;
@end

NS_ASSUME_NONNULL_END
