//
//  ZYHouBaoOrderFormViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHouBaoOrderFormViewController.h" //候保订货单
#import "ZYMyOrderViewController.h" //左侧 我的预订
#import "ZYHouBaoOrderViewController.h"//候保预订单

static int const labelWith = 60;

@interface ZYHouBaoOrderFormViewController ()<UIScrollViewDelegate>
@property (nonatomic, weak) UIScrollView *titleScrollView;
@property (nonatomic, weak) UIScrollView *contentScrollView;
@property (nonatomic, strong) UIButton *oldButton;
@property (nonatomic, strong) UIView *backView;//我的预订title
@property (nonatomic, strong) UIView *titlebackView;//候保预订单title
@property (nonatomic, strong) UIButton *rightButton;//我的预订管理按钮
@property (nonatomic, strong) ZYMyOrderViewController *myOrderVC;//我的预订
@property(nonatomic,copy)NSString *currentTitle;
@end

@implementation ZYHouBaoOrderFormViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self setScrollViewTopandBottom];
    [self setupViewControllers];
    [self setTitlesButton];
    
    self.backView.hidden = NO;
}

//初始化标题ScrollView
-(void)setupScrollView{
    NSInteger count = self.childViewControllers.count;
    //设置标题滚动条
    self.titleScrollView.contentSize = CGSizeMake(count * labelWith, 0);
    self.titleScrollView.showsHorizontalScrollIndicator = NO;
    //设置内容滚动条
    self.contentScrollView.contentSize = CGSizeMake(ScreenW*count, 0);
    //开启分页
    self.contentScrollView.pagingEnabled = YES;
    //没有弹簧效果
    self.contentScrollView.bounces = NO;
    //隐藏水平滚动条
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //设置协议
    self.contentScrollView.delegate = self;
}

//设置页面scrollView
-(void)setScrollViewTopandBottom{
    
    UIScrollView *contentScroll = [[UIScrollView alloc]init];
    //contentScroll.frame = CGRectMake(0, kNaviHeight, ScreenW, KHEIGHT-60-kNaviHeight);
    contentScroll.frame = CGRectMake(0, KNavHeight, ScreenW, KHEIGHT - 60 - KNavHeight);
    contentScroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentScroll];
    self.contentScrollView = contentScroll;
    self.contentScrollView.userInteractionEnabled = YES;
    self.contentScrollView.scrollEnabled = YES;
    self.contentScrollView.pagingEnabled = YES;
    //显示水平方向的滚动条(默认YES)
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //显示垂直方向的滚动条(默认YES)
    self.contentScrollView.showsVerticalScrollIndicator = NO;
    self.contentScrollView.bounces = NO;
    
    self.contentScrollView.contentSize = CGSizeMake(ScreenW*2, 0);
    
    self.contentScrollView.delegate = self;
}

#pragma mark -- UISCrollview代理方法
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSInteger index = scrollView.contentOffset.x/scrollView.bounds.size.width;
    //CGFloat offsetX = index * ScreenW;
    
    UIButton *but = (UIButton *)[self.view viewWithTag:100+index];
    [self.oldButton setTitleColor:[self colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    
    [but setTitleColor:[self colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    if (self.oldButton == but) {
        
    } else {
        if (index == 0) {
            [but setImage:imgname(@"myorder-is") forState:(UIControlStateNormal)];
            [self.oldButton setImage:imgname(@"hborder-no") forState:(UIControlStateNormal)];
            [self shownavleft];
        } else {
            [self.oldButton setImage:imgname(@"myorder-no") forState:(UIControlStateNormal)];
            [but setImage:imgname(@"hborder-is") forState:(UIControlStateNormal)];
            [self shownavright];
        }
    }
    self.oldButton = but;
}

-(void)setTitlesButton{
    NSArray *arr = @[@"我的预订",@"候保预订单"];
    NSArray *imagearr = @[@"myorder-is",@"hborder-no"];
    
    CGFloat wwww = ScreenW/arr.count;
    CGFloat hhhhh = 60;
    CGFloat yyyy = KHEIGHT-60;
    
    UIImageView *mybg = [[UIImageView alloc]initWithFrame:CGRectMake(0, yyyy, KWIDTH, hhhhh)];
    mybg.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mybg];
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line.frame = CGRectMake(0, 0, KWIDTH, 1);
    [mybg addSubview:line];
    
    mybg.userInteractionEnabled = YES;
    
    for (int i = 0; i<arr.count; i++) {
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(wwww*i, 0, wwww, hhhhh);
        [mybg addSubview:myButton];
        myButton.tag = 100+i;
        [myButton setImage:imgname(imagearr[i]) forState:(UIControlStateNormal)];
        [myButton setTitle:arr[i] forState:UIControlStateNormal];
        [myButton addTarget:self action:@selector(titleClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [myButton setTitleColor:[ self colorWithHexString:@"#666666"] forState:UIControlStateNormal];
        myButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        myButton.titleLabel.font = FontSize(12);
        myButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;//使图片和文字水平居中显示
        [myButton setTitleEdgeInsets:UIEdgeInsetsMake(myButton.imageView.frame.size.height+10 ,-myButton.imageView.frame.size.width, 0.0,0.0)];//文字距离上边框的距离增加imageView的高度，距离左边框减少imageView的宽度，距离下边框和右边框距离不变
        [myButton setImageEdgeInsets:UIEdgeInsetsMake(-10, 0.0,0.0, -myButton.titleLabel.bounds.size.width)];//图片距离右边框距离减少图片的宽度，其它不边
        
        if (i == 0) {
            self.oldButton = myButton;
            [self titleClick:myButton];
            [self.oldButton setTitleColor:[self colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        }
    }
}

-(UIView *)backView{
    if (!_backView) {
        
        _backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, kNaviHeight)];
        _backView.backgroundColor = [UIColor whiteColor];
        
        UILabel *titl = [[UILabel alloc]initWithFrame:CGRectMake(0, kNaviHeight-37, KWIDTH, 30)];
        titl.font = FontSize(18);
        titl.text = @"我的预订";
        titl.textColor = kColorWithHex(0x484848);
        titl.textAlignment = NSTextAlignmentCenter;
        [_backView addSubview:titl];
        
        UIButton  *leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(10, kNaviHeight-37, 30, 30)];
        leftbutton.titleLabel.font = [UIFont systemFontOfSize:14];
        [leftbutton setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
        [leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
        [leftbutton setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
        [leftbutton addTarget:self action:@selector(leftClickactin) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:leftbutton];
        
        _rightButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 70,kNaviHeight-37, 50, 30)];
        [_rightButton setTitle:@"管理" forState:UIControlStateNormal];
        [_rightButton setTitleColor:kColorWithHex(0x484848) forState:UIControlStateNormal];
        _rightButton.titleLabel.font = FontSize(14);
        [_rightButton addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:_rightButton];
        
        UILabel *line = [[UILabel alloc] init];
        line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line.frame = CGRectMake(0, kNaviHeight-1, KWIDTH, 1);
        [_backView addSubview:line];
        
        [self.view addSubview: _backView];
    }
    return _backView;
}

- (void)rightBtnAction:(UIButton *)button {
    if ([button.titleLabel.text isEqualToString:@"管理"]) {
        [button setTitle:@"完成" forState:UIControlStateNormal];
        [self.myOrderVC managerMyOrderListAction:1];
    }else{
        [button setTitle:@"管理" forState:UIControlStateNormal];
        [self.myOrderVC managerMyOrderListAction:2];
    }
}

- (void)leftClickactin{
    if ([self.currentTitle isEqualToString:@"我的预订"]) {
        [self.navigationController popViewControllerAnimated:YES];

    }
    else if ([self.currentTitle isEqualToString:@"候保预订单"])
    {
        //0取出label
        NSInteger index = 0;
        //2.1计算滚动位置
        CGFloat offsetX = index * ScreenW;
        [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    }else
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
}

- (void)shownavleft{
    self.backView.hidden = NO;
    self.titlebackView.hidden = YES;
    self.currentTitle = @"我的预订";
}

-(UIView *)titlebackView{
    
    if (!_titlebackView) {
        _titlebackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, kNaviHeight)];
        _titlebackView.backgroundColor = [UIColor whiteColor];
        
        UILabel *titl = [[UILabel alloc]initWithFrame:CGRectMake(0, kNaviHeight-37, KWIDTH, 30)];
        titl.font = FontSize(18);
        titl.text = @"候保预订单";
        titl.textColor = kColorWithHex(0x484848);
        titl.textAlignment = NSTextAlignmentCenter;
        [_titlebackView addSubview:titl];
        
        UIButton  *leftbutton = [[UIButton alloc] initWithFrame:CGRectMake(10, kNaviHeight-37, 30, 30)];
        leftbutton.titleLabel.font = [UIFont systemFontOfSize:14];
        [leftbutton setContentEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
        [leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
        [leftbutton setTitleColor:KTEXTCOLOR forState:UIControlStateNormal];
        [leftbutton addTarget:self action:@selector(leftClickactin) forControlEvents:UIControlEventTouchUpInside];
        [_titlebackView addSubview:leftbutton];
        
        UILabel *line = [[UILabel alloc] init];
        line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line.frame = CGRectMake(0, kNaviHeight-1, KWIDTH, 1);
        [_titlebackView addSubview:line];
        
        [self.view addSubview:_titlebackView];
        _titlebackView.hidden = YES;
    }
    return _titlebackView;
}

-(void)shownavright{
    
    //backView.backgroundColor = [UIColor redColor];
    //[self.backView removeFromSuperview];
    //self.title = @"订货单";
    //
    //[self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.titlebackView.hidden = NO;
    self.backView.hidden = YES;
    self.currentTitle = @"候保预订单";
}

-(void)titleClick:(UIButton *)but{
    //0取出label
    NSInteger index = but.tag-100;
    //2.1计算滚动位置
    CGFloat offsetX = index * ScreenW;
    [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

//显示页面
-(void)showVC:(NSInteger)index {
    CGFloat offsetX = index * ScreenW;
    UIViewController *vc = self.childViewControllers[index];
    //判断当前控制器的View 有没有加载过 如果已经加载过 就不需要加载
    if (vc.isViewLoaded) return;
    vc.view.frame = CGRectMake(offsetX, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:vc.view];
}

- (void)setupViewControllers {
//    DISAorderGoodLeftViewController *seveVC = [[DISAorderGoodLeftViewController alloc]init];
//    seveVC.view.frame = CGRectMake(0, 0, ScreenW, self.contentScrollView.frame.size.height);
//    [self.contentScrollView addSubview:seveVC.view];
//    [self addChildViewController:seveVC];
//
//    DISAorderGoodRightViewController *evaluate = [[DISAorderGoodRightViewController alloc]init];
//    evaluate.view.frame = CGRectMake(ScreenW, 0, ScreenW, self.contentScrollView.frame.size.height);
//    [self.contentScrollView addSubview:evaluate.view];
//    [self addChildViewController:evaluate];
    
    //我的预订
    ZYMyOrderViewController *myOrderVC = [[ZYMyOrderViewController alloc] init];
    myOrderVC.view.frame = CGRectMake(0, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:myOrderVC.view];
    [self addChildViewController:myOrderVC];
    self.myOrderVC = myOrderVC;
    
    //候保预订单
    ZYHouBaoOrderViewController *houbaoVC = [[ZYHouBaoOrderViewController alloc] init];
    houbaoVC.view.frame = CGRectMake(ScreenW, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:houbaoVC.view];
    [self addChildViewController:houbaoVC];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
