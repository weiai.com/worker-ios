//
//  CSnewMallViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewMallViewController.h"
#import "GoodListModel.h"
#import "CSgoodsViewController.h"
#import "HomeGoodsCell.h"
#import "CSnewMainMallViewController.h"
#import "QPLocationManager.h"
#import <CoreLocation/CoreLocation.h>

@interface CSnewMallViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)XSQBanner *banner;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray *modelArr;
@property(nonatomic,copy)NSString *type;

@property(nonatomic,strong)UIButton *odlBut;
@property(nonatomic,assign) NSInteger page;

@property(nonatomic,assign) NSInteger typepage;

@end

@implementation CSnewMallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    UITextField *search = [[UITextField alloc]init];
//    [search createLeftViewImage:@"sousuo1"];
//    search.delegate = self;
//    self.navigationItem.titleView = search;
//    search.keyboardType = UIKeyboardTypeWebSearch;
//    search.backgroundColor = [UIColor colorWithHexString:@"#e5e5e5"];
    self.page= 1;
    self.typepage= 0;

    self.title = _str;
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    [self location];
//    [self requestGoods];
    
    [self showHeader];
    // Do any additional setup after loading the view from its nib.
}

-(void)leftClick{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSnewMainMallViewController class]]) {
            CSnewMainMallViewController *vc =(CSnewMainMallViewController *)controller;
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

-(void)showHeader{
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 32)];
    bgview.backgroundColor = [UIColor whiteColor];
    bgview.userInteractionEnabled  =YES;
    [self.view addSubview:bgview];
    NSArray *arr = @[@"综合",@"离我最近",@"销量最高"];
    CGFloat butww = KWIDTH/arr.count;
    for (int i = 0; i <arr.count; i++) {
        
        UIButton *mhybut =[UIButton   buttonWithType:(UIButtonTypeCustom)];
        [mhybut setTitle:arr[i] forState:(UIControlStateNormal)];
        mhybut.frame = CGRectMake(butww*i, 0, butww, 32);
        mhybut.titleLabel.font = FontSize(14);
        [mhybut setTitleColor:K999999 forState:(UIControlStateNormal)];
        [bgview addSubview:mhybut];
        mhybut.tag = 9834+i;
        [mhybut addTarget:self action:@selector(butAction:) forControlEvents:(UIControlEventTouchUpInside)];
//        if ([mhybut.titleLabel.text isEqualToString:@"产品筛选"]) {
//            if (strIsEmpty(_type)) {
//                [mhybut setImage:imgname(@"seleimge") forState:(UIControlStateNormal)];
//            }else{
//                [mhybut setImage:imgname(@"seleimge") forState:(UIControlStateNormal)];
//        }
//        }
        
        if (i  == 0) {
            self.odlBut = mhybut;
            [self.odlBut setTitleColor:zhutiColor forState:(UIControlStateNormal)];
        }
    }
}

-(void)butAction:(UIButton *)but{
    NSInteger indd = but.tag -9834;
    [self.odlBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [but setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    self.odlBut = but;
    self.typepage= indd;
    _page = 1;
    if (indd == 1) {
        [self location];//离我最近
    }else{
        [self requestGoods];
    }
}

-(void)location {
    [[QPLocationManager sharedManager] getGps:^(NSString *province, NSString *city, NSString *area, NSString *street) {
        NSLog(@"province=%@,street=%@",province,street);
        [self requestGoods];
    }];
}

//是否开启定位
- (BOOL)isLocationServiceOpen {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        return NO;
    } else
        return YES;
}

- (void)requestGoods {
    
    if ([self isLocationServiceOpen]) {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"typeId"] = _idstr;
    NSString *pp = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNum"] = pp;
    
    NSString *lochstr = [NSString stringWithFormat:@"%@:%@",[USER_DEFAULT objectForKey:@"longitude"],[USER_DEFAULT objectForKey:@"latitude"]];
    param[@"local"] = lochstr;
    
    if (self.typepage == 1) {
        //离我最近
        NSString *lochstr = [NSString stringWithFormat:@"%@:%@",[USER_DEFAULT objectForKey:@"longitude"],[USER_DEFAULT objectForKey:@"latitude"]];
        param[@"local"] = lochstr;
    }else if (self.typepage == 2){
     //销量
        param[@"sales"] = @"1";
    }
    if (_page == 1) {
        [self.modelArr removeAllObjects];
    }
    kWeakSelf;
    
    [NetWorkTool POST:MmallDetaileID param:param success:^(id dic) {
        if (weakSelf.page == 1) {
            [self.modelArr removeAllObjects];
        }
        NSArray *myarr = [dic objectForKey:@"data"];
        for (NSDictionary *mydic in myarr) {
            GoodListModel *model = [[GoodListModel alloc]init];
            [model setValuesForKeysWithDictionary:mydic];
            [self.modelArr addObject:model];
        }
        [self.collectionView reloadData];
        self->_page == 1 ?[self->_collectionView.mj_header endRefreshing]:[self->_collectionView.mj_footer endRefreshing];
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } other:^(id dic) {
        [self->_collectionView.mj_footer endRefreshingWithNoMoreData];
        [self->_collectionView.mj_header endRefreshing];
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } fail:^(NSError *error) {
        [self->_collectionView.mj_footer endRefreshing];

        [self->_collectionView.mj_header endRefreshing];
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:NO];
    
    }else{
        //未开启定位权限
        UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"需要您开启定位权限,是否允许" preferredStyle:UIAlertControllerStyleAlert];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"取消");
        }]];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"确定");
            if ([[[UIDevice currentDevice] systemVersion] integerValue] >9.9) {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            } else {
                
            }
        }]];
    }
}

#pragma mark - CollectionView -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    [cell refreshs:model];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(KWIDTH/2-15 , 240);
        //        layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight+32, KWIDTH, KHEIGHT-kNaviHeight-32) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView reloadData];
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.page = 1;
            [self requestGoods];
        }];
        
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            self.page += 1;
            [self requestGoods];
        }];
    }
    return _collectionView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
