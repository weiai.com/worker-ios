//
//  ZYOrderDetailsTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYOrderDetailsTableViewCell.h"
@interface ZYOrderDetailsTableViewCell()
@property(nonatomic,strong)UILabel *yxqTitLab;
@property(nonatomic,strong)UILabel *pfjeTitLab;
@end
@implementation ZYOrderDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    //frame.origin.y += 10;
    //frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH, 250);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UIImageView *proImg = [[UIImageView alloc] init];
        proImg.frame = CGRectMake(16, 20, 59, 59);
        proImg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [bottomV addSubview:proImg];
        self.proImg = proImg;
        
        UILabel *proTitLab = [[UILabel alloc] init];
        proTitLab.frame = CGRectMake(proImg.right +10, 17, KWIDTH-32-59-13, 40);
        proTitLab.numberOfLines = 0;
        proTitLab.font = FontSize(14);
        proTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bottomV addSubview:proTitLab];
        self.proTitLab = proTitLab;
        
        UILabel *typeTitLab = [[UILabel alloc] init];
        typeTitLab.frame = CGRectMake(proImg.right +10, proTitLab.bottom +7, 30, 20);
        typeTitLab.text = @"型号";
        typeTitLab.font = FontSize(14);
        typeTitLab.textColor = [UIColor colorWithHexString:@"#666666"];
        [bottomV addSubview:typeTitLab];
        self.typeTitLab = typeTitLab;
        
        UILabel *typeConLab = [[UILabel alloc] init];
        typeConLab.frame = CGRectMake(typeTitLab.right +10, proTitLab.bottom +7, KWIDTH/2, 20);
        typeConLab.font = FontSize(14);
        typeConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:typeConLab];
        self.typeConLab = typeConLab;
        
        UILabel *yxqTitLab = [[UILabel alloc] init];
        yxqTitLab.frame = CGRectMake(24, proImg.bottom +24, 70, 20);
        yxqTitLab.text = @"有效期";
        yxqTitLab.font = FontSize(14);
        yxqTitLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:yxqTitLab];
        self.yxqTitLab = yxqTitLab;
        
        UILabel *yxqConLab = [[UILabel alloc] init];
        yxqConLab.frame = CGRectMake(yxqTitLab.right +20, proImg.bottom +24, KWIDTH/2, 20);
        yxqConLab.font = FontSize(14);
        yxqConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:yxqConLab];
        self.yxqConLab = yxqConLab;
        
        UILabel *line1 = [[UILabel alloc] init];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line1.frame = CGRectMake(10, yxqTitLab.bottom +7, KWIDTH -20, 1);
        [bottomV addSubview:line1];
        
        UILabel *bzqTitLab = [[UILabel alloc] init];
        bzqTitLab.frame = CGRectMake(24, line1.bottom +7, 70, 20);
        bzqTitLab.text = @"质保期";
        bzqTitLab.font = FontSize(14);
        bzqTitLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:bzqTitLab];
        
        UILabel *bzqConLab = [[UILabel alloc] init];
        bzqConLab.frame = CGRectMake(bzqTitLab.right +20, line1.bottom +7, KWIDTH/2, 20);
        bzqConLab.font = FontSize(14);
        bzqConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:bzqConLab];
        self.bzqConLab = bzqConLab;
        
        UILabel *line2 = [[UILabel alloc] init];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line2.frame = CGRectMake(10, bzqTitLab.bottom +7, KWIDTH -20, 1);
        [bottomV addSubview:line2];
        
        UILabel *pfjeTitLab = [[UILabel alloc] init];
        pfjeTitLab.frame = CGRectMake(24, line2.bottom +7, 70, 20);
        pfjeTitLab.text = @"赔付金额";
        pfjeTitLab.font = FontSize(14);
        pfjeTitLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        [bottomV addSubview:pfjeTitLab];
        self.pfjeTitLab = pfjeTitLab;
        
        UILabel *pfjeConLab = [[UILabel alloc] init];
        pfjeConLab.frame = CGRectMake(pfjeTitLab.right +20, line2.bottom +7, KWIDTH/2, 20);
        pfjeConLab.font = FontSize(14);
        pfjeConLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        [bottomV addSubview:pfjeConLab];
        self.pfjeConLab = pfjeConLab;
        
        UILabel *line3 = [[UILabel alloc] init];
        line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line3.frame = CGRectMake(10, pfjeTitLab.bottom +7, KWIDTH -20, 1);
        [bottomV addSubview:line3];
        self.line3 = line3;

        UIButton *cancleOrBtn = [[UIButton alloc] init];
        cancleOrBtn.frame = CGRectMake(bottomV.right -84, line3.bottom +9, 69, 26);
        [cancleOrBtn setImage:[UIImage imageNamed:@"组 3507"] forState:UIControlStateNormal];
        [cancleOrBtn addTarget:self action:@selector(cancleOrBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:cancleOrBtn];
        self.cancleOrBtn = cancleOrBtn;
    }
    return self;
}

-(void)refasf:(ZYMyOrderGoodsModel *)model {
    
    [self.proImg sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil];
    self.proTitLab.text = model.parts_name;
    self.typeConLab.text = model.attribute;
    
    self.yxqConLab.text = [NSString stringWithFormat:@"自出厂日期有效期为%@个月", model.install];
    self.bzqConLab.text = [NSString stringWithFormat:@"%@%@", model.warranty, @"个月"];
    self.pfjeConLab.text = [NSString stringWithFormat:@"%@%@%@",@"¥", model.compensationPrice, @"/元"];
    //有效期数值为0 补贴工时费 数值为0 不显示
    if ([model.install integerValue]==0) {
        _yxqConLab.hidden = YES;
        _yxqTitLab.hidden = YES;
    }
    if ([model.compensationPrice integerValue]==0) {
        _pfjeConLab.hidden = YES;
        _pfjeTitLab.hidden = YES;
        self.line3.hidden = YES;
    }
    
    //parts state 状态1正常2取消3电器厂已下单
    if ([model.state isEqualToString:@"1"]) {
        KMyLog(@"正常状态");
    } else if ([model.state isEqualToString:@"2"]) {
        self.cancleOrBtn.hidden = YES;
        
        UILabel *stateLab = [[UILabel alloc] init];
        stateLab.frame = CGRectMake(KWIDTH-84, self.line3.bottom+10, 69, 26);
        stateLab.text = @"已取消";
        stateLab.textColor = [UIColor colorWithHexString:@"#D84B4A"];
        stateLab.font = FontSize(14);
        stateLab.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:stateLab];
        
    } else if ([model.state isEqualToString:@"3"]) {
        KMyLog(@"暂时先不管");
    }
}

//取消预订 按钮点击事件
- (void)cancleOrBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(1,@"");//取消预订
    }
    NSLog(@"您点击了 取消预订 按钮");
}

@end
