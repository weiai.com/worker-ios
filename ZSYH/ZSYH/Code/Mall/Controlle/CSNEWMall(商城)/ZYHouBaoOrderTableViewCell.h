//
//  ZYHouBaoOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYMyOrderGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZYHouBaoOrderCellDelegate <NSObject>

-(void)deleteDataWithCell:(UITableViewCell *_Nullable)cell;
-(void)editDataWithCell:(UITableViewCell *_Nonnull)cell;

@end

@interface ZYHouBaoOrderTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);
@property (nonatomic, strong) UIImageView *proImg; //产品图片
@property (nonatomic, strong) UILabel *proTitLab; //描述信息
@property (nonatomic, strong) UILabel *typeTitLab; //型号 标题
@property (nonatomic, strong) UILabel *typeConLab; //型号 内容
@property (nonatomic, strong) UILabel *countLab; //总数量
@property (nonatomic, strong) UIButton *changeQuBtn; //修改数量 按钮
@property (nonatomic, strong) UIButton *cancleOrBtn; //取消预订 按钮

@property (nonatomic, strong) NSIndexPath *indexPath;

@property(assign, nonatomic)id <ZYHouBaoOrderCellDelegate> delegate;

-(void)refasf:(ZYMyOrderGoodsModel *)model;
@end

NS_ASSUME_NONNULL_END
