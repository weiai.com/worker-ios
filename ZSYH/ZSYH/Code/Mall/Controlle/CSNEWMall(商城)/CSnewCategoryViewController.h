//
//  CSnewCategoryViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewCategoryViewController : BaseViewController
@property(nonatomic,copy)void (^myblock)(NSString *str,NSString *idstr);

@end

NS_ASSUME_NONNULL_END
