//
//  ZYOrderDetailsViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYOrderDetailsViewController : BaseViewController
@property(nonatomic,copy)void (^myblock)(NSString *str);
@property(nonatomic, strong) NSString *orderIdStr;

@end

NS_ASSUME_NONNULL_END
