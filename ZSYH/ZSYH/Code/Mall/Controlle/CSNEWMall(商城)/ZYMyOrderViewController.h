//
//  ZYMyOrderViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyOrderViewController : BaseViewController
- (void)managerMyOrderListAction:(NSInteger)actionType;//1完成 2管理
@end

NS_ASSUME_NONNULL_END
