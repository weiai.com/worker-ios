//
//  ZYHouBaoOrderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHouBaoOrderViewController.h"
#import "ZYHouBaoOrderTableViewCell.h"
#import "ZYOrderDetailsViewController.h" //预订单详情 跳转
#import "ZYMyOrderStoresModel.h"
#import "ZYMyOrderGoodsModel.h"

@interface ZYHouBaoOrderViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,ZYHouBaoOrderCellDelegate>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myStoreCount;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIView *bgViewsecCancle; //取消原因 弹窗
@property (nonatomic, strong) UITextView *cancelTV; //取消原因 输入框

@property (nonatomic, strong) UIButton *secSelBtn; //区头 选择按钮
@property (nonatomic, strong) UILabel *shopTitLab; //区头 店铺名称
@property (nonatomic, strong) UILabel *topDateLab; //区头 日期时间

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) UILabel *placeLab;

@end

@implementation ZYHouBaoOrderViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.title = @"候保预订单";
    //[self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self.view addSubview:self.scrollView];
    
    self.myStoreCount = [NSMutableArray arrayWithCapacity:1];
    
    [self requestData];
    [self showdetaile];
    [self shwoBgviewsecCancle]; //取消原因
    // Do any additional setup after loading the view.
    
    //我的预订提交订单成功后更新候保预订单
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"USER_ADD_ORDER_SUCCESS" object:nil];
    //候保预订单配件取消预订后更新候保预订单
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"CANCLE_HB_ORDER_PART_SUCCESS" object:nil];
}

-(void)showdetaile {
    
    [self.view addSubview:self.scrollView];
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 56)];
    
    _mytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT -kNaviHeight -60) style:UITableViewStyleGrouped];
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYHouBaoOrderTableViewCell class] forCellReuseIdentifier:@"ZYHouBaoOrderTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    kWeakSelf;
    KKTableViewNoDataView *viewNoneData = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(KWIDTH/2-106, KHEIGHT/2-36-60, 213, 158) image:imgname(@"empty_data_text_icon") viewClick:^{
        [weakSelf.mytableView.mj_header beginRefreshing];
    }];
    _mytableView.placeHolderView = viewNoneData;
    _mytableView.tableFooterView = self.tabFootView;
    _mytableView.placeHolderView.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:_mytableView];
}

//候保订货单 数据请求
- (void)requestData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"user_type"] = @"1"; //用户类型
    
    [weakSelf.myStoreCount removeAllObjects];
    
    [NetWorkTool POST:myPartsOrders param:param success:^(id dic) {
        KMyLog(@"候保预订单 数据 %@", dic);
        
        NSArray *orderList = [dic objectForKeyNotNil:@"data"];
        [orderList enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ZYMyOrderStoresModel *model = [ZYMyOrderStoresModel mj_objectWithKeyValues:obj];
            [weakSelf.myStoreCount addObjectNotNil:model];
        }];
        
        [weakSelf.mytableView reloadData];
        
        [weakSelf.mytableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.myStoreCount removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.myStoreCount removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.myStoreCount.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ZYMyOrderStoresModel *model = [self.myStoreCount objectAtIndexSafe:section];
    return model.parts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 131;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(0, 0, KWIDTH,60);
    
    UILabel *topDateLab = [[UILabel alloc] init];
    topDateLab.frame = CGRectMake(16, 8, KWIDTH - 32, 18);
    topDateLab.textColor = K999999;
    topDateLab.text = @"2019-10-11 15:07:29";
    topDateLab.font = FontSize(14);
    [headerView addSubview:topDateLab];
    self.topDateLab = topDateLab;
    
    UIButton *secSelBtn = [[UIButton alloc] init];
    secSelBtn.frame = CGRectMake(16, topDateLab.bottom+2, 25, 25);
    [secSelBtn setImage:[UIImage imageNamed:@"dianpu (1)"] forState:UIControlStateNormal];
    [secSelBtn addTarget:self action:@selector(secSelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:secSelBtn];
    self.secSelBtn = secSelBtn;
    
    UILabel *shopTitLab = [[UILabel alloc] init];
    shopTitLab.frame = CGRectMake(secSelBtn.right +2, topDateLab.bottom +5, KWIDTH-32-25-2-2-20, 20);
    shopTitLab.numberOfLines = 0;
    shopTitLab.font = FontSize(14);
    shopTitLab.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [headerView addSubview:shopTitLab];
    self.shopTitLab = shopTitLab;
    
    UIImageView *topBgImg = [[UIImageView alloc] init];
    topBgImg.frame = CGRectMake(shopTitLab.right +2, topDateLab.bottom +5, 20, 20);
    [topBgImg setImage:imgname(@"jinruimage")];
    [headerView addSubview:topBgImg];
    
    ZYMyOrderStoresModel *storeModel = [self.myStoreCount objectAtIndexSafe:section];
    self.topDateLab.text = [NSString stringWithFormat:@"%@",storeModel.createDate];
    self.shopTitLab.text = storeModel.factory_name;
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",(long)indexPath.section, (long)indexPath.row];
    //通过唯一标识创建Cell实例
    ZYHouBaoOrderTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYHouBaoOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态

    ZYMyOrderStoresModel *storeModel = [self.myStoreCount objectAtIndexSafe:indexPath.section];
    //数据赋值
    ZYMyOrderGoodsModel *model = [storeModel.parts objectAtIndexSafe:indexPath.row];
    [mycell refasf:model];
    
    mycell.indexPath = indexPath;
    mycell.delegate = self;
    
    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 0: {
                //取消原因 弹窗
                [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecCancle];
                self.orderId = model.Id;
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZYMyOrderStoresModel *storesModel = self.myStoreCount[indexPath.section];
    
    //跳转预订单详情 页面
    ZYOrderDetailsViewController *vc = [[ZYOrderDetailsViewController alloc] init];
    vc.orderIdStr = storesModel.Id;
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"您点击的是第%ld个分区 第%ld行", indexPath.section, indexPath.row);
}

//分区区头 选择按钮 点击事件处理
- (void)secSelBtnAction:(UIButton *)button {
    NSLog(@"您点击了 分区 区头 的选择按钮");
}

/**
 弹出框的背景图 取消原因
 */
-(void)shwoBgviewsecCancle{
    self.bgViewsecCancle = [[UIView alloc]init];
    self.bgViewsecCancle.frame = self.view.bounds;
    self.bgViewsecCancle.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecCancle addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-22*2)/2-30, 34, 60, 60)];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"tipImage"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 19, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"确定取消该订预订单?";
    
    UIButton *cancelButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancelButton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [cancelButton setBackgroundColor:[UIColor whiteColor]];
    cancelButton.layer.masksToBounds = YES;
    cancelButton.layer.cornerRadius = 4;
    cancelButton.layer.borderWidth = 1;
    cancelButton.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    cancelButton.titleLabel.font = FontSize(14);
    [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.offset(28);
        make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.bottom.offset(-19);
    }];
    
    UIButton *confirmButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [confirmButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:1]];
    confirmButton.layer.masksToBounds = YES;
    confirmButton.layer.cornerRadius = 4;
    confirmButton.titleLabel.font = FontSize(14);
    [confirmButton addTarget:self action:@selector(confirmButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}
//取消原因 暂不取消按钮 点击事件
-(void)cancelButtonAction:(UIButton *)but{
    [_bgViewsecCancle removeFromSuperview];
    //[self.navigationController popToRootViewControllerAnimated:YES];
    self.cancelTV.text = @"";
    self.placeLab.hidden = NO;
    NSLog(@"您点击了 暂不取消 按钮");
}

//取消原因 确认取消按钮 点击事件
- (void)confirmButtonAction:(UIButton *)but{
    [self cancleReason]; //确定取消 数据请求
    NSLog(@"您点击了 确定取消 按钮");
}

//取消购物订单 数据请求
- (void)cancleReason {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"id"] = NOTNIL(self.orderId); //订单id
    param[@"reason"] = @""; //取消原因 文本
    
    [NetWorkTool POST:cancelOrders param:param success:^(id dic) {
        [self.bgViewsecCancle removeFromSuperview];
        self.cancelTV.text = @"";
        self.placeLab.hidden = NO;
        [self requestData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.placeLab.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length < 1) {
        self.placeLab.hidden = NO;
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
