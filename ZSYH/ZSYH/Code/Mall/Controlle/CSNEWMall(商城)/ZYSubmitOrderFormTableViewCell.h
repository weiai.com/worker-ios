//
//  ZYSubmitOrderFormTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYMyOrderGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYSubmitOrderFormTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *proImg; //产品图片
@property (nonatomic, strong) UILabel *proTitLab; //配件名称
@property (nonatomic, strong) UILabel *typeConLab; //型号 内容
@property (nonatomic, strong) UILabel *yxqLab; //有效期
@property (nonatomic, strong) UILabel *bzqLab; //保质期
@property (nonatomic, strong) UILabel *pfjeLab; //赔付金额
@property (nonatomic, strong) ZYMyOrderGoodsModel *goodsModel;//订单产品
@end

NS_ASSUME_NONNULL_END
