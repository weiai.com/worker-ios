//
//  ZYMyOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyOrderTableViewCell.h"

@interface ZYMyOrderTableViewCell ()
@property (nonatomic, strong) UIButton *btnCycle;//选中按钮
@end

@implementation ZYMyOrderTableViewCell



+ (CGFloat)cellHeight{
    return 87.0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    //frame.origin.y += 10;
    //frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH, 97);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];

        UIButton *rowSelBtn = [[UIButton alloc] init];
        rowSelBtn.frame = CGRectMake(5, 19, 30, 30);
        [rowSelBtn setImage:[UIImage imageNamed:@"selequyu"] forState:UIControlStateNormal];
        [rowSelBtn setImage:imgname(@"seleQuyuyew") forState:UIControlStateSelected];
        [rowSelBtn addTarget:self action:@selector(rowSelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:rowSelBtn];
        self.btnCycle = rowSelBtn;

        UIImageView *proImg = [[UIImageView alloc] init];
        proImg.frame = CGRectMake(rowSelBtn.right +2, 9, 64, 64);
        proImg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [bottomV addSubview:proImg];
        self.proImg = proImg;

        UILabel *proTitLab = [[UILabel alloc] init];
        proTitLab.frame = CGRectMake(proImg.right +10, 11, KWIDTH-3-30-3-10-64-10, 20);
        proTitLab.text = @"名称";
        proTitLab.font = FontSize(14);
        proTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bottomV addSubview:proTitLab];
        self.proTitLab = proTitLab;

        UILabel *typeTitLab = [[UILabel alloc] init];
        typeTitLab.frame = CGRectMake(proImg.right +10, proTitLab.bottom +5, 30, 20);
        typeTitLab.text = @"型号";
        typeTitLab.font = FontSize(14);
        typeTitLab.textColor = [UIColor colorWithHexString:@"#666666"];
        [bottomV addSubview:typeTitLab];

        UILabel *typeConLab = [[UILabel alloc] init];
        typeConLab.frame = CGRectMake(typeTitLab.right +10, proTitLab.bottom +5, KWIDTH/2, 20);
        typeConLab.text = @"型号类型";
        typeConLab.font = FontSize(14);
        typeConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:typeConLab];
        self.typeConLab = typeConLab;
    }
    return self;
}

- (void)setGoodsModel:(ZYMyOrderGoodsModel *)goodsModel{
    if (goodsModel) {
        _goodsModel = goodsModel;
        [self.proImg sd_setImageWithURL:[NSURL URLWithString:goodsModel.image_url] placeholderImage:nil];
        self.proTitLab.text = goodsModel.parts_name;
        self.typeConLab.text = goodsModel.name;
        self.btnCycle.selected = goodsModel.isOrderSelected;
    }
}
#pragma mark - 交互事件
- (void)rowSelBtnAction:(UIButton *)button {
    if (self.goodsModel.isOrderSelected) {
        self.goodsModel.isOrderSelected = NO;
        self.btnCycle.selected = NO;
        if (self.delegate && [self.delegate respondsToSelector:@selector(orderGoodsCellClick:isSelected:)]) {
            [self.delegate orderGoodsCellClick:self.indexPath isSelected:YES];
        }
    }else{
        self.goodsModel.isOrderSelected = YES;
        self.btnCycle.selected = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(orderGoodsCellClick:isSelected:)]) {
            [self.delegate orderGoodsCellClick:self.indexPath isSelected:YES];
        }
    }
}


@end
