//
//  ZYMyOrderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyOrderViewController.h"
#import "ZYStoresTableViewCell.h"
#import "ZYMyOrderTableViewCell.h"
#import "HFShopCarHeaderView.h" //分区头部 headerView
#import "ZYSubmitOrderFormViewController.h" //提交订购单
#import "CSShopViewController.h"
#import "ZYMyOrderStoresModel.h"
#import "ZYMyOrderGoodsModel.h"

#import "ZYGoodsDetailViewController.h"//产品详情

@interface ZYMyOrderViewController ()<UITableViewDelegate, UITableViewDataSource, ZYMyStoreCellDelegate,ZYMyOrderGoodsCellDelegate>

@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) NSMutableArray *myStoreSouce;

@property (nonatomic, strong) UIView *tabFootView;

@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UIView *bgViewsecDele;

@property (nonatomic, assign) BOOL allChose;
@property (nonatomic, strong) UIButton *allSelBtn;//全选按钮
@property (nonatomic, strong) UIButton *orderNowBtn;//订购按钮
@property (nonatomic, strong) UIButton *deleteBtn;//删除按钮

@end

@implementation ZYMyOrderViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (_mytableView) {
        //[self requestListData];
    }
    //[singlTool shareSingTool].isLjiBuy = 1;
    //[self.bottomView changeBottomViewTotalPrice:@"0.00" goodsCount:@"0"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"USER_ADD_ORDER_SUCCESS" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.myStoreSouce = [NSMutableArray arrayWithCapacity:1];
    
    [self showdetaile];
    
    [self requestData]
    ;
}
#pragma mark - 管理按钮
- (void)managerMyOrderListAction:(NSInteger)actionType{
    if (actionType == 1) {
        //完成 - 进行删除操作
        self.deleteBtn.hidden = NO;
        self.orderNowBtn.hidden = YES;
    }else{
        //管理 - 进行立即订购操作
        self.deleteBtn.hidden = YES;
        self.orderNowBtn.hidden = NO;
    }
    self.allChose = NO;
    self.allSelBtn.selected = NO;
    [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isSelected = NO;
        [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isOrderSelected = NO;
        }];
    }];
    [_mytableView reloadData];
}
//我的预订 数据请求
- (void)requestData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"user_type"] = @"1"; //用户类型
    
    [weakSelf.myStoreSouce removeAllObjects];
    [NetWorkTool POST:myOrderCar param:param success:^(id dic) {
        
        NSArray *dataArr = [dic objectForKeyNotNil:@"data"];
        [dataArr enumerateObjectsUsingBlock:^(NSDictionary  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ZYMyOrderStoresModel *model = [ZYMyOrderStoresModel mj_objectWithKeyValues:obj];
            [weakSelf.myStoreSouce addObjectNotNil:model];
        }];
        KMyLog(@"---------- 我的预订 ---------- %@", dic);

        [self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
    } other:^(id dic) {
        //[weakSelf.mydateSource removeAllObjects];
        //[self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        //[weakSelf.mydateSource removeAllObjects];
        //[self.mytableView reloadData];
        //[self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

-(void)showdetaile {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 56)];
    //列表
    _mytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT - 60 - KNavHeight - 56) style:UITableViewStylePlain];
    _mytableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _mytableView.backgroundColor = [UIColor clearColor];
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    [_mytableView registerClass:[ZYStoresTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZYStoresTableViewCell class])];
    [_mytableView registerClass:[ZYMyOrderTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZYMyOrderTableViewCell class])];
    _mytableView.tableFooterView = self.tabFootView;
    _mytableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_mytableView.bounds image:imgname(@"empty_data_text_icon") viewClick:^{
        [self->_mytableView.mj_header beginRefreshing];
    }];
    [self.view addSubview:_mytableView];

    UIView *bottomView = [[UIView alloc] init];
    bottomView.frame = CGRectMake(0, KHEIGHT - 60 - KNavHeight - 56, KWIDTH, 56);
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line.frame = CGRectMake(0, 0, KWIDTH, 1);
    [bottomView addSubview:line];
    
    //全选按钮
    UIButton *allSelBtn = [[UIButton alloc] init];
    allSelBtn.backgroundColor = [UIColor whiteColor];
    allSelBtn.frame = CGRectMake(10, 8, 90, 40);
    [allSelBtn setImage:[UIImage imageNamed:@"selequyu"] forState:UIControlStateNormal];
    [allSelBtn setImage:[UIImage imageNamed:@"seleQuyuyew"] forState:UIControlStateSelected];
    [allSelBtn setTitle:@"全选" forState:UIControlStateNormal];
    allSelBtn.titleLabel.font = FontSize(14);
    [allSelBtn setTitleColor:[UIColor colorWithHexString:@"#484848"] forState:UIControlStateNormal];
    [allSelBtn addTarget:self action:@selector(bottomSelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:allSelBtn];
    self.allSelBtn = allSelBtn;
    
    //删除按钮
    UIButton *deleteBtn = [[UIButton alloc] init];
    deleteBtn.backgroundColor = [UIColor whiteColor];
    deleteBtn.frame = CGRectMake(bottomView.right-187, 8, 169, 42);
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = FontSize(14);
    deleteBtn.layer.borderColor = [UIColor redColor].CGColor;
    deleteBtn.layer.borderWidth = 1;
    deleteBtn.layer.cornerRadius = 21;
    deleteBtn.layer.masksToBounds = YES;
    [deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:deleteBtn];
    self.deleteBtn = deleteBtn;
    
    //立即订购按钮
    UIButton *orderNowBtn = [[UIButton alloc] init];
    orderNowBtn.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    orderNowBtn.frame = CGRectMake(bottomView.right-187, 8, 169, 42);
    [orderNowBtn setTitle:@"我要预订" forState:UIControlStateNormal];
    orderNowBtn.titleLabel.font = FontSize(14);
    orderNowBtn.layer.cornerRadius = 21;
    orderNowBtn.layer.masksToBounds = YES;
    [orderNowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderNowBtn addTarget:self action:@selector(orderNowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:orderNowBtn];
    self.orderNowBtn = orderNowBtn;
}
#pragma mark - 底部功能按钮
- (void)bottomSelBtnAction:(UIButton *)button {
    self.allChose = !self.allChose;
    if (self.allChose) {
        //全选
        self.allSelBtn.selected = YES;
        [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isSelected = YES;
            [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isOrderSelected = YES;
            }];
        }];
    } else {
        //取消全选
        self.allSelBtn.selected = NO;;
        [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            obj.isSelected = NO;
            [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isOrderSelected = NO;
            }];
        }];
    }
    [_mytableView reloadData];
}

//立即订购按钮 点击事件处理
- (void)orderNowBtnAction:(UIButton *)button {
    [self managerOrderCarRequestDataWithType:1];
    NSLog(@"您点击了 底部的 立即订购 按钮");
}

//删除按钮 点击事件处理
- (void)deleteBtnAction:(UIButton *)button {
    //弹出提示框
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecDele];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.myStoreSouce.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    ZYMyOrderStoresModel *model = [self.myStoreSouce objectAtIndexSafe:section];
    return model.parts.count + 1;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    ZYMyOrderStoresModel *model = [self.myStoreSouce objectAtIndexSafe:indexPath.section];
    if (indexPath.row == 0) {
        ZYStoresTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZYStoresTableViewCell class]) forIndexPath:indexPath];
        cell.model = model;
        cell.delegate = self;
        cell.indexPath = indexPath;
        return cell;
    }else{
        ZYMyOrderGoodsModel *goodsModel = [model.parts objectAtIndexSafe:indexPath.row - 1];
        ZYMyOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZYMyOrderTableViewCell class]) forIndexPath:indexPath];
        cell.goodsModel = goodsModel;
        cell.indexPath = indexPath;
        cell.delegate = self;
        return cell;
    }
    return [UITableViewCell new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [ZYStoresTableViewCell cellHeight];
    }else{
        return [ZYMyOrderTableViewCell cellHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"您点击了第 %ld 个分区第 %ld 行", indexPath.section, indexPath.row);
    ZYMyOrderStoresModel *model = [self.myStoreSouce objectAtIndexSafe:indexPath.section];
    if (indexPath.row != 0) {
        ZYMyOrderGoodsModel *goodsModel = [model.parts objectAtIndexSafe:indexPath.row - 1];
        ZYGoodsDetailViewController *vc = [ZYGoodsDetailViewController new];
        vc.shopID = goodsModel.partsId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - ZMyOrderCarStoreCellDelegate
- (void)ZYStoreCellClick:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected{
    //刷新section
    NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:indexPath.section];
    [self.mytableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    
    [self checkIsAllSelected];
}

#pragma mark - ZMyOrderCarShopOrderCellDelegate
//选中按钮点击
- (void)orderGoodsCellClick:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected{
    //刷新cell
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    NSIndexPath *indexZero = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
    
    ZYMyOrderStoresModel *model = [self.myStoreSouce objectAtIndexSafe:indexPath.section];
    __block NSInteger selectedNumber = 0;
    [model.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isOrderSelected == YES) {
            selectedNumber++;
        }
    }];
    if (selectedNumber == model.parts.count) {
        //全选
        model.isSelected = YES;
    }else{
        //非全选
        model.isSelected = NO;
    }
    
    [self.mytableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,indexZero,nil] withRowAnimation:UITableViewRowAnimationNone];
    
    [self checkIsAllSelected];
}

//检查是否触发了全选
- (void)checkIsAllSelected{
    __block NSInteger selectedSection = 0;
    [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isSelected) {
            selectedSection++;
        }
    }];
    if (selectedSection == self.myStoreSouce.count) {
        //全选
        self.allSelBtn.selected = YES;
        self.allChose = YES;
    }else{
        self.allSelBtn.selected = NO;
        self.allChose = NO;
    }
}

//- (void)orderCarBottomViewClick:(ZMyOrderCarBottomViewClickType)clickType{
//    if (clickType == ZMyOrderCarBottomViewClickTypeNoneSelected) {
//        //取消全选
//        [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            obj.isSelected = NO;
//            [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                obj.isOrderSelected = NO;
//            }];
//        }];
//        [self.mytableView reloadData];
//    }else if (clickType == ZMyOrderCarBottomViewClickTypeSelected){
//        //全选
//        [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            obj.isSelected = YES;
//            [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                obj.isOrderSelected = YES;
//            }];
//        }];
//        [self.mytableView reloadData];
//    }else if (clickType == ZMyOrderCarBottomViewClickTypeBuy){
//        //立即订购
//        [self managerOrderCarRequestDataWithType:1];
//    }else if (clickType == ZMyOrderCarBottomViewClickTypeDelete){
//        //删除
//        [self managerOrderCarRequestDataWithType:2];
//    }
//}
#pragma mark - 交互事件
//管理
- (void)rightClick{
//    if (self.viewBottom.btnType == ZMyOrderCarBottomViewClickTypeBuy) {
//        [self.viewBottom updateBtnBuy:ZMyOrderCarBottomViewClickTypeDelete];
//        [self.rightbutton setTitle:@"完成" forState:(UIControlStateNormal)];
//    }else if (self.viewBottom.btnType == ZMyOrderCarBottomViewClickTypeDelete){
//        [self.viewBottom updateBtnBuy:ZMyOrderCarBottomViewClickTypeBuy];
//        [self.rightbutton setTitle:@"管理" forState:(UIControlStateNormal)];
//    }
}
#pragma mark - 管理订购车接口处理
- (void)managerOrderCarRequestDataWithType:(NSInteger)type{
    __block NSString *orderIDStr = @"";//订单ID拼接
    __block NSString *countStr = @"";//订单数量拼接

    [self.myStoreSouce enumerateObjectsUsingBlock:^(ZYMyOrderStoresModel  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull objPart, NSUInteger idx, BOOL * _Nonnull stop) {
            if (objPart.isOrderSelected) {
                orderIDStr = [orderIDStr stringByAppendingString: orderIDStr.length > 0 ? [NSString stringWithFormat:@",%@",objPart.Id] : objPart.Id];
                countStr = [countStr stringByAppendingString: countStr.length > 0 ? [NSString stringWithFormat:@",%@",objPart.count] : objPart.count];
            }
        }];
    }];

    if (orderIDStr.length > 0) {
        kWeakSelf;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObjectIfNotNil:@(type) forKey:@"type"];
        [dict setObjectIfNotNil:@"2" forKey:@"user_type"];
        [dict setObjectIfNotNil:orderIDStr forKey:@"ids"];
        [dict setObjectIfNotNil:countStr forKey:@"counts"];
        [NetWorkTool POST:USER_DEL_ORDER_CAR param:dict success:^(id dic) {
            if (type == 1) {
                //提交成功
                NSArray *orderList = [dic objectForKeyNotNil:@"data"];
                __block NSMutableArray *submitOrderArray = [NSMutableArray array];
                [orderList enumerateObjectsUsingBlock:^(NSDictionary  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    ZYMyOrderStoresModel *model = [ZYMyOrderStoresModel mj_objectWithKeyValues:obj];
                    [submitOrderArray addObjectNotNil:model];
                }];
                ZYSubmitOrderFormViewController *submitVC = [[ZYSubmitOrderFormViewController alloc] init];
                submitVC.orderArray = submitOrderArray;
                [self.navigationController pushViewController:submitVC animated:YES];
                
            } else if (type == 2){
                //删除成功
                [weakSelf requestData];
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } other:^(id dic) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } fail:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } needUser:YES];
    } else {
        ShowToastWithText(@"请选择订购的产品");
    }
}

#pragma mark - 删除我的预订弹窗交互
//我在想想
-(void)tleftBtnAction:(UIButton *)but{
    [_bgViewsecDele removeFromSuperview];
}
//删除
-(void)trightBtnAction:(UIButton *)but{
    [_bgViewsecDele removeFromSuperview];
    [self managerOrderCarRequestDataWithType:2];
}


#pragma mark - 删除弹窗
- (UIView *)bgViewsecDele{
    if (!_bgViewsecDele) {
        UIWindow *window = [[UIApplication sharedApplication].delegate window];
        _bgViewsecDele = [[UIView alloc]init];
        _bgViewsecDele.frame = window.bounds;
        _bgViewsecDele.backgroundColor = kColorWithHex(0xb1b0af);
        UIView *whiteBGView = [[UIView alloc]init];
        whiteBGView.backgroundColor = [UIColor whiteColor];
        whiteBGView.layer.masksToBounds = YES;
        whiteBGView.layer.cornerRadius = 6;
        [_bgViewsecDele addSubview:whiteBGView];
        [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.offset(0);
            make.height.offset(240);
            make.left.offset(22);
            make.right.offset(-22);
        }];
        
        UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-30, 34, 60, 60)];
        upImage.layer.masksToBounds = YES;
        upImage.layer.cornerRadius = 20;
        [whiteBGView addSubview:upImage];
        upImage.image = [UIImage imageNamed:@"tipImage"];
        
        UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom +19, KWIDTH-44, 21)];
        [whiteBGView addSubview:UpLable];
        UpLable.font = FontSize(16);
        UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
        UpLable.textAlignment =  NSTextAlignmentCenter;
        UpLable.text = @"确认删除?";
        
        UIButton *leftButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [leftButton setTitle:@"取消" forState:(UIControlStateNormal)];
        [leftButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [leftButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:1]];
        leftButton.layer.masksToBounds = YES;
        leftButton.layer.cornerRadius = 4;
        leftButton.titleLabel.font = FontSize(14);
        [leftButton addTarget:self action:@selector(tleftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [whiteBGView addSubview:leftButton];
        [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(36);
            make.left.offset(28);
            make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
            make.bottom.offset(-19);
        }];
        
        UIButton *rightButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [rightButton setTitle:@"确认" forState:(UIControlStateNormal)];
        [rightButton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
        [rightButton setBackgroundColor:[UIColor whiteColor]];
        rightButton.layer.masksToBounds = YES;
        rightButton.layer.cornerRadius = 4;
        rightButton.layer.borderWidth = 1;
        rightButton.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        rightButton.titleLabel.font = FontSize(14);
        [rightButton addTarget:self action:@selector(trightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [whiteBGView addSubview:rightButton];
        [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(36);
            make.left.equalTo(whiteBGView.mas_centerX).offset(13);
            make.right.offset(-28);
            make.bottom.offset(-19);
        }];

    }
    return _bgViewsecDele;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
