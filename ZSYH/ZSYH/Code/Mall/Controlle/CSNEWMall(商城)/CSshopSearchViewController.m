//
//  CSshopSearchViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSshopSearchViewController.h"
#import "GoodListModel.h"
#import "CSgoodsViewController.h"
#import "HomeGoodsCell.h"
@interface CSshopSearchViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)XSQBanner *banner;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray *modelArr;
@property(nonatomic,strong)UITextField *searchTextField;

@end

@implementation CSshopSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    UITextField *search = [[UITextField alloc]init];
    [search createLeftViewImage:@"sousuo1"];
//    search.leftView.frame = CGRectMake(0, 0, 10, 20);
    search.delegate = self;
    self.navigationItem.titleView = search;
    search.returnKeyType = UIReturnKeySearch;
    search.backgroundColor = [UIColor colorWithHexString:@"#e5e5e5"];
    
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
//    [self requestGoods];
    
    [self.rightbutton setBackgroundImage:imgname(@"searchright") forState:(UIControlStateNormal)];
    [self.rightbutton setTitle:@"" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.rightbutton.titleLabel.font= FontSize(13);
    // Do any additional setup after loading the view.
}

- (void)rightClick{
    _search =  self.searchTextField.text;
    [self requestGoods];
    [self.searchTextField resignFirstResponder];
}

////编辑区域，即光标的位置
//-(CGRect)editingRectForBounds:(CGRect)bounds{
//    returnCGRectMake(20,0,bounds.size.width ,bounds.size.height);
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [super textFieldShouldReturn:textField];
    _search = textField.text;
    [self requestGoods];
    return YES;
}

- (void)requestGoods {
    [self.modelArr removeAllObjects];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"name"] = NOTNIL(_search);
    param[@"id"] = NOTNIL(_idstr);

    [NetWorkTool POST:mailHomeseleapi param:param success:^(id dic) {

        self.modelArr = [GoodListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.collectionView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

#pragma mark - CollectionView -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeGoodsCell" forIndexPath:indexPath];
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    [cell refreshs:model];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    GoodListModel *model = [_modelArr safeObjectAtIndex:indexPath.row];
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(KWIDTH/2-15 , 240);
        //        layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        
        [_collectionView registerNib:[UINib nibWithNibName:@"HomeGoodsCell" bundle:nil] forCellWithReuseIdentifier:@"HomeGoodsCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView reloadData];
        
    }
    return _collectionView;
}

@end

