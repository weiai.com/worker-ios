//
//  ZYFeedbackViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYFeedbackViewController.h"

@interface ZYFeedbackViewController ()<UITextViewDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UITextView *yijianTV;
@property (nonatomic, strong) UILabel *placeLab;

@end

@implementation ZYFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"留言反馈";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self showdetaile];
    [self shwoBgviewsec]; //背景弹窗
    // Do any additional setup after loading the view.
}

-(void)showdetaile {
    
    [self.view addSubview:self.scrollView];
    
    UIView *bottomV = [[UIView alloc] init];
    bottomV.backgroundColor = [UIColor whiteColor];
    bottomV.frame = CGRectMake(0, kNaviHeight +10, KWIDTH, 226);
    [self.scrollView addSubview:bottomV];
    
    UITextView *yijianTV = [[UITextView alloc] init];
    yijianTV.frame = CGRectMake(18, 24, KWIDTH - 36, 162);
    yijianTV.font = FontSize(14);
    yijianTV.layer.borderWidth = 1;
    yijianTV.layer.borderColor = [UIColor colorWithHexString:@"#CCCCCC"].CGColor;
    yijianTV.layer.cornerRadius = 8;
    yijianTV.layer.masksToBounds = YES;
    yijianTV.delegate = self;
    self.yijianTV = yijianTV;
    [bottomV addSubview:yijianTV];
    
    UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
    placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    placeLab.text = @"请输入您的反馈意见(不超过125个字)";
    placeLab.font = FontSize(14);
    self.placeLab = placeLab;
    [self.yijianTV addSubview:placeLab];
    
    //底部 提交留言按钮
    UIButton *bottomBtn = [[UIButton alloc] init];
    bottomBtn.frame = CGRectMake(28, bottomV.bottom + 232, KWIDTH-56, 48);
    [bottomBtn setTitle:@"提交留言" forState:UIControlStateNormal];
    bottomBtn.titleLabel.font = FontSize(16);
    [bottomBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bottomBtn.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    bottomBtn.layer.cornerRadius = 5;
    [bottomBtn addTarget:self action:@selector(bottomBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:bottomBtn];

}

//底部 提交留言按钮 点击事件
- (void)bottomBtnAction:(UIButton *)button {
    [self requestData];
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([textView isFirstResponder]) {
        if ([[[textView textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textView textInputMode] primaryLanguage]) {
            return NO;
        }
        
        //if ([self hasEmoji:text] || [self stringContainsEmoji:text]){
            //return NO;
        //}
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.placeLab.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length < 1) {
        self.placeLab.hidden = NO;
    }
}
#pragma mark - 判断字符串中是否有表情
//- (BOOL)stringContainsEmoji:(NSString *)string {
//    
//    __block BOOL returnValue = NO;
//    
//    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
//     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
//         
//         const unichar hs = [substring characterAtIndex:0];
//         // surrogate pair
//         if (0xd800 <= hs && hs <= 0xdbff) {
//             if (substring.length > 1) {
//                 const unichar ls = [substring characterAtIndex:1];
//                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
//                 if (0x1d000 <= uc && uc <= 0x1f77f) {
//                     returnValue = YES;
//                 }
//             }
//         } else if (substring.length > 1) {
//             const unichar ls = [substring characterAtIndex:1];
//             if (ls == 0x20e3) {
//                 returnValue = YES;
//             }
//             
//         } else {
//             // non surrogate
//             if (0x2100 <= hs && hs <= 0x27ff) {
//                 returnValue = YES;
//             } else if (0x2B05 <= hs && hs <= 0x2b07) {
//                 returnValue = YES;
//             } else if (0x2934 <= hs && hs <= 0x2935) {
//                 returnValue = YES;
//             } else if (0x3297 <= hs && hs <= 0x3299) {
//                 returnValue = YES;
//             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
//                 returnValue = YES;
//             }
//         }
//     }];
//    
//    return returnValue;
//}
#pragma mark - 判断是否是九宫格键盘
//-(BOOL)isNineKeyBoard:(NSString *)string
//{
//    NSString *other = @"➋➌➍➎➏➐➑➒";
//    int len = (int)string.length;
//    for(int i=0;i<len;i++)
//    {
//        if(!([other rangeOfString:string].location != NSNotFound))
//            return NO;
//    }
//    return YES;
//}
#pragma mark - 判断是否含有表情(第三方键盘)
//- (BOOL)hasEmoji:(NSString*)string;
//{
//    NSString *pattern = @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
//    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
//    BOOL isMatch = [pred evaluateWithObject:string];
//    return isMatch;
//}

//提交留言 数据请求 levelPartsMessage
- (void)requestData {
    NSString *contentStr = [_yijianTV.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (strIsEmpty(contentStr)) {
        ShowToastWithText(@"请输入您的留言");
        return;
    }else if (contentStr.length > 125){
        ShowToastWithText(@"留言内容不超过125个字");
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(self.orderId); //订单id
    param[@"message"] = self.yijianTV.text; //您的意见或建议
    
    [NetWorkTool POST:levelPartsMessage param:param success:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgViewsec];

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"您的留言已提交成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"明天继续努力";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
