//
//  ZYSubmitOrderFormTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYSubmitOrderFormTableViewCell.h"
@interface ZYSubmitOrderFormTableViewCell()
@property (nonatomic, strong)UILabel *yxqTitLab;
@property (nonatomic, strong)UILabel *pfjeTitLab;
@end
@implementation ZYSubmitOrderFormTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setFrame:(CGRect)frame{
  
    frame.size.height -= 10;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH, 225);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        bottomV.backgroundColor = kColorWithHex(0xffffff);
        [self.contentView addSubview:bottomV];
        
        UIImageView *proImg = [[UIImageView alloc] init];
        proImg.frame = CGRectMake(16, 15, 59, 59);
        proImg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [bottomV addSubview:proImg];
        self.proImg = proImg;
        
        UILabel *proTitLab = [[UILabel alloc] init];
        proTitLab.frame = CGRectMake(proImg.right +10, 15, KWIDTH-32-59-13, 40);
        proTitLab.numberOfLines = 0;
        proTitLab.font = FontSize(14);
        proTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bottomV addSubview:proTitLab];
        self.proTitLab = proTitLab;
        
        UILabel *typeTitLab = [[UILabel alloc] init];
        typeTitLab.frame = CGRectMake(proImg.right +10, proTitLab.bottom +5, 45, 20);
        typeTitLab.text = @"已选择";
        typeTitLab.font = FontSize(12);
        typeTitLab.textColor = [UIColor colorWithHexString:@"#979797"];
        [bottomV addSubview:typeTitLab];
        
        UILabel *typeConLab = [[UILabel alloc] init];
        typeConLab.frame = CGRectMake(typeTitLab.right +10, proTitLab.bottom +5, KWIDTH/2, 20);
        typeConLab.font = FontSize(12);
        typeConLab.textColor = [UIColor colorWithHexString:@"#222222"];
        [bottomV addSubview:typeConLab];
        self.typeConLab = typeConLab;
        
        UILabel *yxqTitLab = [[UILabel alloc] init];
        yxqTitLab.frame = CGRectMake(24, proImg.bottom + 27, 70, 20);
        yxqTitLab.text = @"有效期";
        yxqTitLab.font = FontSize(14);
        yxqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
        [bottomV addSubview:yxqTitLab];
        self.yxqTitLab = yxqTitLab;
        
        UILabel *yxqLab = [[UILabel alloc] init];
        yxqLab.frame = CGRectMake(yxqTitLab.right + 20, proImg.bottom + 27, KWIDTH/2, 20);
        yxqLab.font = FontSize(14);
        yxqLab.textColor = [UIColor colorWithHexString:@"#4a4a4a"];
        [bottomV addSubview:yxqLab];
        self.yxqLab = yxqLab;
        
        UILabel *line1 = [[UILabel alloc] init];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line1.frame = CGRectMake(10, yxqTitLab.bottom +8, KWIDTH -20, 1);
        [bottomV addSubview:line1];
        
        UILabel *bzqTitLab = [[UILabel alloc] init];
        bzqTitLab.frame = CGRectMake(24, line1.bottom +8, 70, 20);
        bzqTitLab.text = @"质保期";
        bzqTitLab.font = FontSize(14);
        bzqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
        [bottomV addSubview:bzqTitLab];
        
        UILabel *bzqLab = [[UILabel alloc] init];
        bzqLab.frame = CGRectMake(yxqTitLab.right +20, line1.bottom +8, KWIDTH/2, 20);
        bzqLab.font = FontSize(14);
        bzqLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:bzqLab];
        self.bzqLab = bzqLab;
        
        UILabel *line2 = [[UILabel alloc] init];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        line2.frame = CGRectMake(10, bzqTitLab.bottom +8, KWIDTH -20, 1);
        [bottomV addSubview:line2];
        
        UILabel *pfjeTitLab = [[UILabel alloc] init];
        pfjeTitLab.frame = CGRectMake(24, line2.bottom +8, 90, 20);
        pfjeTitLab.text = @"补贴工时费";
        pfjeTitLab.font = FontSize(14);
        pfjeTitLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        [bottomV addSubview:pfjeTitLab];
        self.pfjeTitLab = pfjeTitLab;
        
        UILabel *pfjeLab = [[UILabel alloc] init];
        pfjeLab.frame = CGRectMake(pfjeTitLab.right, line2.bottom +8, KWIDTH/2, 20);
        pfjeLab.font = FontSize(14);
        pfjeLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        [bottomV addSubview:pfjeLab];
        self.pfjeLab = pfjeLab;
        
    }
    return self;
}
- (void)setGoodsModel:(ZYMyOrderGoodsModel *)goodsModel{
    if (goodsModel) {
        _goodsModel = goodsModel;
        [HFTools imageViewUpdateWithUrl:goodsModel.image_url withImageView:_proImg withPlaceholderImage:@""];
        _proTitLab.text = goodsModel.parts_name;//名称
        _typeConLab.text = goodsModel.name;//型号
        _yxqLab.text = [NSString stringWithFormat:@"自出厂日期有效期为%@个月",goodsModel.install];//有效期
        _bzqLab.text = [NSString stringWithFormat:@"%@个月",goodsModel.warranty];//质保期
        _pfjeLab.text = [NSString stringWithFormat:@"￥%@/个",goodsModel.compensationPrice];
        //有效期数值为0 补贴工时费 数值为0 不显示
        if ([goodsModel.install integerValue]==0) {
            _yxqLab.hidden = YES;
            _yxqTitLab.hidden = YES;
        }
        if ([goodsModel.compensationPrice integerValue]==0) {
            _pfjeLab.hidden = YES;
            _pfjeTitLab.hidden = YES;
        }
    }
}
@end
