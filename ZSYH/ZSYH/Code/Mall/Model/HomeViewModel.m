//
//  HomeViewModel.m
//  ZengBei
//
//  Created by wanshangwl on 2018/7/10.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "HomeViewModel.h"

@implementation HomeViewModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"cate_list":[CateListModel class],
             @"category":[CateListModel class],
             @"store_list":[StoreListModel class],
             @"recommend":[GoodListModel class],
             @"banner":[CateBannerModel class],
             @"ad_list":[CateBannerModel class]};
}

@end

@implementation CateListModel

@end

@implementation StoreListModel

@end

@implementation CateBannerModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID":@"id"};
}
@end
