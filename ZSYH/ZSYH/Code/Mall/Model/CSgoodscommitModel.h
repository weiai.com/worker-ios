//
//  CSgoodscommitModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/2.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSgoodscommitModel : BaseModel
@property(nonatomic,strong)NSString * create_time;
@property(nonatomic,strong)NSString * id;
@property(nonatomic,strong)NSString * evaluate_content;

@property(nonatomic,strong)NSString * nickname;
@property(nonatomic,strong)NSString * product_id;
@property(nonatomic,strong)NSString * rep_user_id;
@property(nonatomic,strong)NSString * image_url;
@property(nonatomic,strong)NSString * evaluate_score;

@end

NS_ASSUME_NONNULL_END
