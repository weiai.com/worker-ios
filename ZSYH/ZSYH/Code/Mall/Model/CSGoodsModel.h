//
//  CSGoodsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSGoodsModel : BaseModel
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *productInfo;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *number;

@property (nonatomic, copy) NSString *productName;
@property (nonatomic, copy) NSString *productPic;
@property (nonatomic, copy) NSString *productBelType;
@property (nonatomic, copy) NSString *productTypeId;
@property (nonatomic, copy) NSString *productPrice;
@property (nonatomic, strong) id createTime;
@property (nonatomic, copy) NSString *shopId;
@property (nonatomic, copy) NSString *productState;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, strong) id updateTime;
@end

NS_ASSUME_NONNULL_END
