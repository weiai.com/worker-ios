//
//  CSBannerModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/2.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSBannerModel : BaseModel
@property (nonatomic, strong)NSString * id;
@property (nonatomic, strong)NSString * product_id;
@property (nonatomic, strong)NSString * product_img_url;
@property (nonatomic, copy) NSString *link_url;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *pic_url;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) id updateTime;
@property (nonatomic, copy) NSString *shop_id;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, strong) id createTime;
@property (nonatomic, copy) NSString *details;
@property (nonatomic, copy) NSString *partsId;
@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
