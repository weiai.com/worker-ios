//
//  CSmailPrucateModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSmailPrucateModel : BaseModel
@property(nonatomic,strong)NSString *parentId;
@property(nonatomic,strong)NSString *Id;
@property(nonatomic,strong)NSString *updateTime;
@property(nonatomic,strong)NSString *typeName;
@property(nonatomic,strong)NSString *typeLevel;
@property(nonatomic,strong)NSString *typeCode;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *parentCode;
@property(nonatomic,strong)NSString *isele;


@end

NS_ASSUME_NONNULL_END
