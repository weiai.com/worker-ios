//
//  CSshopGoodsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSshopGoodsModel : BaseModel
@property(nonatomic,strong)NSString *Id;
@property(nonatomic,strong)NSString *product_bel_type;
@property(nonatomic,strong)NSString *product_info;
@property(nonatomic,strong)NSString *product_name;
@property(nonatomic,strong)NSString *product_pic;
@property(nonatomic,strong)NSString *product_price;
@property(nonatomic,strong)NSString *product_state;
@property(nonatomic,strong)NSString *product_type_id;
@property(nonatomic,strong)NSString *shop_id;
@property(nonatomic,strong)NSString *Stock;
@property(nonatomic,strong)NSString *count;
@property(nonatomic,strong)NSString *channel;
@property(nonatomic,strong)NSString *distance;

@end

NS_ASSUME_NONNULL_END
