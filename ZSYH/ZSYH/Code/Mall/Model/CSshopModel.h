//
//  CSshopModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSshopModel : BaseModel
@property(nonatomic,strong)NSString *agent_id;
@property(nonatomic,strong)NSString *create_time;
@property(nonatomic,strong)NSString *Id;
@property(nonatomic,strong)NSString *lat;
@property(nonatomic,strong)NSString *lon;
@property(nonatomic,strong)NSString *shop_address;
@property(nonatomic,strong)NSString *shop_area;
@property(nonatomic,strong)NSString *shop_image;
@property(nonatomic,strong)NSString *shop_name;
@property(nonatomic,strong)NSString *shop_phone;
@property(nonatomic,strong)NSArray *products;


@end

NS_ASSUME_NONNULL_END
