//
//  CSsearchModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSsearchModel : BaseModel
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_info;
@property (nonatomic, copy) NSString *product_type_id;
@property (nonatomic, copy) NSString *product_pic;
@property (nonatomic, copy) NSString *product_price;
@property (nonatomic, copy) NSString *shop_name;
@property (nonatomic, copy) NSString *product_bel_type;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, copy) NSString *stock;
@property (nonatomic, assign) double _version_;
@property (nonatomic, copy) NSString *product_state;
@property (nonatomic, copy) NSString *shop_id;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *productInfo;
@property (nonatomic, assign) double dist;


@end

NS_ASSUME_NONNULL_END
