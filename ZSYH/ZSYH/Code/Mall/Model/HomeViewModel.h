//
//  HomeViewModel.h
//  ZengBei
//
//  Created by wanshangwl on 2018/7/10.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GoodListModel.h"

@interface CateBannerModel : NSObject
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *link;
@property(nonatomic,copy)NSString *images;
@property(nonatomic,copy)NSString *ID;
@end



@interface CateListModel : NSObject
@property(nonatomic,copy)NSString *cate_id;
@property(nonatomic,copy)NSString *cate_name;
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *type;

@end



@interface StoreListModel : NSObject

@property(nonatomic,copy)NSString *store_id;
@property(nonatomic,copy)NSString *store_name;
@property(nonatomic,copy)NSString *logo;
@property(nonatomic,copy)NSString *banner;
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *level;
@property(nonatomic,copy)NSString *mobile;
@property(nonatomic,copy)NSString *address;
@property(nonatomic,copy)NSString *is_collect;

@property (nonatomic,strong) NSArray *tips;

@property(nonatomic,assign)BOOL isSelect;

@end



@interface HomeViewModel : NSObject
@property(nonatomic,strong)NSArray *category;
@property(nonatomic,strong)NSArray *ad_list;
@property(nonatomic,strong)NSArray *cate_list;
@property(nonatomic,strong)NSArray *store_list;
@property(nonatomic,strong)NSArray *banner;
@property(nonatomic,strong)NSArray *recommend;
@end
