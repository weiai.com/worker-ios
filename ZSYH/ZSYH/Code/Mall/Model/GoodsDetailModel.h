//
//  GoodsDetailModel.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/27.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeViewModel.h"

@interface SpecMapModel:NSObject
@property(nonatomic,copy)NSString *market;
@property(nonatomic,copy)NSString *selling;
@property(nonatomic,copy)NSString *stock;
@end

@interface SpecPropModel:NSObject

@property(nonatomic,copy)NSString *key;
@property(nonatomic,copy)NSString *value;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,assign)BOOL isSelect;
//@property(nonatomic,strong)NSArray *keys;
//@property(nonatomic,strong)NSArray *values;

@end

@interface GoodsInfoModel:NSObject

@property(nonatomic,copy)NSString *goods_id;
@property(nonatomic,copy)NSString *goods_name;
@property(nonatomic,copy)NSString *price;
@property(nonatomic,copy)NSString *sale;

@property(nonatomic,copy)NSString *spec_type;
@property(nonatomic,copy)NSString *shipping;

@property(nonatomic,strong)NSDictionary *spec_map;
@property(nonatomic,strong)NSArray *spec_prop;

@property(nonatomic,strong)NSArray *attr;
@property(nonatomic,strong)NSArray *goods_images;
@property(nonatomic,strong)NSArray *desc_images;

@end






@interface GoodsDetailModel : NSObject

@property(nonatomic,copy)NSString *is_collect;
@property(nonatomic,strong)NSArray *evaluate;
@property(nonatomic,strong)NSDictionary *activity_info;
@property(nonatomic,strong)StoreListModel *store_info;
@property(nonatomic,strong)GoodsInfoModel *goods_info;

@end

