//
//  GoodListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodListModel : BaseModel

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *productName;//
@property (nonatomic, copy) NSString *productInfo;//
@property (nonatomic, copy) NSString *count;//
@property (nonatomic, strong) NSString *productPic;//
@property (nonatomic, copy) NSString *productBelType;//
@property (nonatomic, copy) NSString *productTypeId;
@property (nonatomic, copy) NSString *productPrice;
@property (nonatomic, strong) id createTime;//
@property (nonatomic, copy) NSString *shopId;//
@property (nonatomic, copy) NSString *productState;//
@property (nonatomic, strong) id updateTime;//
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *channel;

@property (nonatomic, copy) NSString *stock; //
@property (nonatomic, copy) NSString *name;//
@property (nonatomic, copy) NSString *typeNameFirst; //
@property (nonatomic, copy) NSString *typeNameThird; //
@property (nonatomic, copy) NSString *typeNameSecond;//
@property (nonatomic, copy) NSString *shop_name;//
@end

NS_ASSUME_NONNULL_END
