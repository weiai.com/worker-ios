//
//  HomeGoodsCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSsearchModel.h"
#import "GoodListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeGoodsCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *goodsTrpe;
@property (weak, nonatomic) IBOutlet UILabel *goodsname;
@property (weak, nonatomic) IBOutlet UILabel *fukuanLB;
@property (weak, nonatomic) IBOutlet UILabel *distenceLb;

-(void)refresh:(GoodListModel *)model;
-(void)refreshs:(GoodListModel *)model;
-(void)refreshsSearch:(CSsearchModel *)model;

@end

NS_ASSUME_NONNULL_END
