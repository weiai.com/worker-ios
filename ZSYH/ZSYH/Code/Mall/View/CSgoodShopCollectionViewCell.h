//
//  CSgoodShopCollectionViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSgoodShopCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myimage;
@property (weak, nonatomic) IBOutlet UILabel *titlb;
@property (weak, nonatomic) IBOutlet UILabel *priceLB;

@end

NS_ASSUME_NONNULL_END
