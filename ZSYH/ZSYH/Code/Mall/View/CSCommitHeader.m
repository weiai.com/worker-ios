//
//  CSCommitHeader.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSCommitHeader.h"

@interface CSCommitHeader ()


@property (nonatomic,strong) NSArray *titles;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic,  copy) void(^select)(NSInteger index);
@property (nonatomic,strong) NSArray *segArray;

@property (nonatomic,  strong) UIButton *oldBut;//记录选择的哪一个
@property (nonatomic,  assign) BOOL isanim;//记录是否需要动画


@end
@implementation CSCommitHeader

- (instancetype)initWithFrame:(CGRect)frame modelArr:(NSArray *)Arr block:(void(^)(NSInteger index))select{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"CSCommitHeader" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;
        self.titles = Arr;
        self.select=select;
        self.isanim = NO;
        [self collectionArr:Arr];

        
    }
    return self;
    
}
-(NSArray *)titles{
    
    if (!_titles) {
        self.titles = [[NSArray alloc]init];
    }
    return _titles;
}

- (void)collectionArr:(NSArray *)array {

    self.segArray =array;
    CGFloat y = 8;
//    CGFloat w = self.width / 3.5 - 20;
    CGFloat h = 25;
    CGFloat fengxi = 16;
//    CGFloat fx = fengxi/2;
//    if (_segArray.count<5) {
//        w = (self.width ) / _segArray.count ;
//
//    }
//
    CGFloat winx = 24;
    for (int i = 0; i < _segArray.count; i++) {
        
        CGFloat ww =  [self getWidthByText:_segArray[i] font:[UIFont systemFontOfSize:16]] +fengxi;
        
        UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        
        btn.frame = CGRectMake(winx, y, ww, h);
        winx = winx +ww+fengxi;
        [btn setTitle:_segArray[i] forState:(UIControlStateNormal)];
        
        [btn setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
        btn.layer.borderWidth = 1;
        btn.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
        [btn setBackgroundColor:[UIColor whiteColor]];
        
        [btn addTarget:self action:@selector(selectButtonClick:) forControlEvents:(UIControlEventTouchUpInside)];
        
        btn.titleLabel.font = FontSize(12);
        
        btn.tag = 88 + i;
        
        [self.scrollView addSubview:btn];
        

        
        if (i == 0) {
            _oldBut = btn;
        }
        
        
        if (i == (_segArray.count-1)) {
            self.scrollView.contentSize = CGSizeMake(winx, h);
            if (winx>KWIDTH) {
                _isanim = YES;
            }
        }
    }
    
    [_oldBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    _oldBut.layer.borderWidth = 1;
    _oldBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
//    [self.scrollView addSubview:self.positionView];
    
//    self.positionView.centerX = _selectBtn.centerX;
//    [self addSubview:self.bottomView];


}

- (void)selectButtonClick:(UIButton *)sender {
    
    /*! 点击的是同一个按钮返回 */
    if (sender == _oldBut) { return; }
    
    /*! 获取点击按钮的对应下标 */
    NSInteger index = sender.tag - 88;
    
    /*! 点击更新文字颜色 */
    [sender setTitleColor:[UIColor colorWithHexString:@"70BE68"] forState:(UIControlStateNormal)];
    sender.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    [_oldBut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
    _oldBut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    _oldBut = sender;
    
    
    /** 修改位置坐标 */
//    [UIView animateWithDuration:0.3 animations:^{
//        self->_positionView.centerX = sender.centerX;
//    }];
//
    if (_select) {
        self.select(index);
    }
    /** 更新偏移量 */
    [self updateContentOffsetX];
    
    
}

- (void)updateContentOffsetX {
    
    if (!_isanim) {
        return;
    }
    CGFloat left_x = _oldBut.centerX - self.width / 2;
    CGFloat max_x = _scrollView.contentSize.width - _scrollView.width;
    CGFloat right_x = _scrollView.contentSize.width - _oldBut.centerX - self.width/2;
    
    if (left_x > 0) {
        if (right_x > 0) {
            [_scrollView setContentOffset:CGPointMake(left_x, 0) animated:YES];
        } else {
            [_scrollView setContentOffset:CGPointMake(max_x, 0) animated:YES];
        }
    } else {
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}



- (CGFloat)getWidthByText:(NSString *)text font:(UIFont *)font {
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAXFLOAT, 0)];
    label.text = text;
    label.font = font;
    [label sizeToFit];
    CGFloat width = label.frame.size.width;
    return width;
}



@end

