//
//  CSdoodsCommitView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSdoodsCommitView.h"

@implementation CSdoodsCommitView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"CSdoodsCommitView" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;
        self.lookAllButton.layer.borderWidth = 1;
        self.lookAllButton.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    }
    return self;
}
@end
