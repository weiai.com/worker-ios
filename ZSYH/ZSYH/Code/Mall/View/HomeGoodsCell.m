//
//  HomeGoodsCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "HomeGoodsCell.h"
@implementation HomeGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithHexString:@"#F2F2F2"].CGColor;
    self.layer.masksToBounds =YES;
    self.layer.cornerRadius = 4;
    
    
//    self.distenceLb.hidden = YES;
    // Initialization code
}
-(void)refresh:(GoodListModel *)model{
    
    if ([model.productPic hasPrefix:@"http"]) {
        [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.productPic] placeholderImage:defaultImg];
    }else{
        sdimg(self.goodsImage, model.productPic);
    }
    
    
    self.price.text = [NSString stringWithFormat:@"¥%.2f", [model.productPrice floatValue]];
    if ([model.productBelType integerValue] == 1) {
        self.goodsTrpe.text = @"候保产品";
        self.fukuanLB.text = @"";
    }else{
        self.goodsTrpe.text = @"";
        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    }
//    if ([model.channel integerValue] == 1) {
//        self.goodsTrpe.text = @"候保产品";
//        self.fukuanLB.text = @"";
//    }else{
//        self.goodsTrpe.text = @"";
//        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
//    }
    self.goodsname.text = [NSString stringWithFormat:@"%@",model.productInfo];

    
    CGFloat ffff = [model.distance floatValue];
    if (ffff<100) {
        self.distenceLb.text = [NSString stringWithFormat:@"%@米",model.distance];

    }else {
        //self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff];
        self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff / 1000];

    }
    if (strIsEmpty(model.distance)) {
        self.distenceLb.hidden = YES;
    }
    
}

-(void)refreshs:(GoodListModel *)model{
    
    if ([model.productPic hasPrefix:@"http"]) {
        [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.productPic] placeholderImage:defaultImg];
    }else{
        sdimg(self.goodsImage, model.productPic);
    }
    
    
    self.price.text = [NSString stringWithFormat:@"¥%.2f", [model.productPrice floatValue]];
    //    if ([model.productBelType integerValue] == 1) {
    //        self.goodsTrpe.text = @"候保产品";
    //        self.fukuanLB.text = @"";
    //    }else{
    //        self.goodsTrpe.text = @"";
    //        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    //    }
    if ([model.channel integerValue] == 1) {
        self.goodsTrpe.text = @"候保产品";
        //self.fukuanLB.text = @"";
        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count]; //8.7改


    }else{
        self.goodsTrpe.text = @"";
        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    }
    self.goodsname.text = [NSString stringWithFormat:@"%@",model.productName];
    
    CGFloat ffff = [model.distance floatValue];
    if (ffff<100) {
        self.distenceLb.text = [NSString stringWithFormat:@"%@米",model.distance];

    }else {
        //self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff];
        self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff / 1000];

    }
    if (strIsEmpty(model.distance)) {
        self.distenceLb.hidden = YES;
        
    }
    
}
-(void)refreshsSearch:(CSsearchModel *)model{
    
    if ([model.product_pic hasPrefix:@"http"]) {
        [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    }else{
        sdimg(self.goodsImage, model.product_pic);
    }
    
    
    self.price.text = [NSString stringWithFormat:@"¥%.2f",[model.product_price floatValue]];
    self.price.font = [UIFont systemFontOfSize:15];
    //    self.goodsTrpe.text = [NSString stringWithFormat:@"%@",model.productPrice];
//    if ([model.product_bel_type integerValue] == 1) {
//        self.goodsTrpe.text = @"候保产品";
//        self.fukuanLB.text = @"";
//    }else{
//        self.goodsTrpe.text = @"";
//        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
//        self.fukuanLB.text = @"";
//
//    }
    if ([model.channel integerValue] == 1) {
        self.goodsTrpe.text = @"候保产品";
//        self.fukuanLB.text = @"";
        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
    }else{
        self.goodsTrpe.text = @"";
        self.fukuanLB.text = [NSString stringWithFormat:@"%@人付款",model.count];
//        self.fukuanLB.text = @"";
    }
    self.goodsname.text = [NSString stringWithFormat:@"%@",model.product_name];
    
//    CGFloat ffff = [model.dist floatValue];
//    if (ffff<100) {
//        self.distenceLb.text = [NSString stringWithFormat:@"%@米",model.dist];
//
//    }else {
//        //self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff];
//        self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",ffff / 1000];
//
//    }
    
    NSString *dist = [NSString stringWithFormat:@"%lf\n",model.dist];
    CGFloat distFloat = [dist floatValue];
    
    self.distenceLb.text = [NSString stringWithFormat:@"%.1f千米",distFloat];
    
//    if (strIsEmpty(model.dist)) {
//            self.distenceLb.hidden = YES;
//    }
}

@end
