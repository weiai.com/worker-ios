//
//  CSdoodsCommitView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSdoodsCommitView : UIView
@property (weak, nonatomic) IBOutlet UILabel *pingjiaLB;
@property (weak, nonatomic) IBOutlet UIImageView *picImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLB;
@property (weak, nonatomic) IBOutlet UIButton *lookAllButton;


@end

NS_ASSUME_NONNULL_END
