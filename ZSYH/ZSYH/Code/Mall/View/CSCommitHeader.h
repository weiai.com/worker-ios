//
//  CSCommitHeader.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSCommitHeader : UIView
@property (weak, nonatomic) IBOutlet UILabel *ganderLB;
- (instancetype)initWithFrame:(CGRect)frame modelArr:(NSArray *)Arr block:(void(^)(NSInteger index))select;

@end

NS_ASSUME_NONNULL_END
