//
//  CSgoodsShopView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSgoodsShopView.h"
#import "CSgoodShopCollectionViewCell.h"
@interface CSgoodsShopView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSArray *titles;


@property (nonatomic,  copy) void(^select)(NSInteger index);


@end
static NSString *const cellId = @"CSgoodShopCollectionViewCell";

@implementation CSgoodsShopView
- (instancetype)initWithFrame:(CGRect)frame modelArr:(NSArray *)Arr block:(void(^)(NSInteger index))select{
    self = [super initWithFrame:frame];

    if (self) {
        _titles = Arr;
        _select = select;
        
        [self collectionArr:Arr];
    }
    return self;
    
    
    
}


- (void)collectionArr:(NSArray *)array {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(85, 115);
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//横向
    _collectionView.showsHorizontalScrollIndicator = NO;

    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerNib:[UINib nibWithNibName:cellId bundle:nil] forCellWithReuseIdentifier:cellId];

    [self addSubview:_collectionView];
    [_collectionView reloadData];
    
}



#pragma mark - 列表代理 -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
    return _titles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CSgoodShopCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    CSGoodsModel *model =[_titles safeObjectAtIndex:indexPath.row];
    if ([model.productPic hasPrefix:@"http"]) {
        [cell.myimage sd_setImageWithURL:[NSURL URLWithString:model.productPic] placeholderImage:defaultImg];
    }else{
        sdimg(cell.myimage, model.productPic);
        
    }
    cell.titlb.text = model.productName;
    cell.priceLB.text = [NSString stringWithFormat:@"¥%@",model.productPrice];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    if (self.select) {
        self.select(indexPath.row);
    }
    
}

//
//- (void)setLabColor:(UIColor *)labColor {
//    _labColor = labColor;
//    [_collectionView reloadData];
//}
//
//
//- (void)setLabFont:(CGFloat)labFont {
//    _labFont = labFont;
//    [_collectionView reloadData];
//}
//
//
//- (void)setImgs:(NSArray *)imgs {
//    _imgs = imgs;
//    [_collectionView reloadData];
//}

@end
