//
//  ScanOrderListViewController.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "YJBaseTableViewController.h"
//typedef NS_OPTIONS(NSInteger, ComeFromType) {
//    ComeFromTypeScanOrder = 0,
//    ComeFromTypeHouBaoOrder = 1 << 0,
//    ComeFromTypeOther = 1 << 1
//};
NS_ASSUME_NONNULL_BEGIN

@interface ScanOrderListViewController : YJBaseTableViewController
//@property (nonatomic, assign) ComeFromType comeFromType;
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;
@end

NS_ASSUME_NONNULL_END
