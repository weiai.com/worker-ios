//
//  ScanOrderListViewController.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "ScanOrderListViewController.h"
#import "ScanOrderListCell.h"
#import "ZYAddHoubaoAccessoriesViewController.h"
#import "ZYHoubaoAccessoriesViewController.h"
#import "HouBaoAccessoryDetailVC.h"             //扫码订单-配件详情
#import "HouBaoAccessOrderAccessDetailVC.h"     //候保订单-配件详情

@interface ScanOrderListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UIView *bgview;

@end

@implementation ScanOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //维修单
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.listTableView reloadData];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requeset) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requeset) name:@"applicationWillEnterForeground" object:nil];
    
    [self requeset];
}

#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    
    if ([singlTool shareSingTool].needReasfh) {
        [self.listTableView.mj_header beginRefreshing];
        [singlTool shareSingTool].needReasfh = NO;
    }
}

- (void)setUpNavigationBar {
    [super setUpNavigationBar];
}

- (void)buildSubviews {
    [super buildSubviews];
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - KNavHeight - 44);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor;
    self.listTableView.estimatedRowHeight = kScaleNum(148);
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionHeaderHeight = kScaleNum(44);
    self.listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionFooterHeight = kScaleNum(44);
    self.listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.listTableView.isShowBtn = NO;
    self.listTableView.showNoData = YES;
    self.listTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.listTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
    [self.listTableView.mj_header beginRefreshing];
    }];
    //self.listTableView.customImg = [UIImage imageNamed:(@"page_icon01_")];
    //self.listTableView.customMsg = @"暂时还没有数据";
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KBottomSafeViewHeight)];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.listTableView.tableFooterView = tableFooterView;
    kWeakSelf;
    self.listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requeset];
    }];
}

- (void)requeset{
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSInteger ind = [_type integerValue];
    NSLog(@"%ld", ind);
    // 订单状态(0生成订单（未处理）//添加候保配件
    // ，1已处理，//查看配件详情
    [weakSelf.mydateSource removeAllObjects];
    NSString *url = @"";
//    if (self.comeFromType == ComeFromTypeHouBaoOrder) {
//        url = HbRepOrderList;
//        ind-=1;
//        if (ind >1) {
//            ind +=1;
//        }
//        NSString *stt  = [NSString stringWithFormat:@"%ld",ind];
//        if (ind == -1) {
//            stt = @"";
//        }
//        param[@"type"] = stt;
//        KMyLog(@"当前的ind是多少哦 %@", stt);
//    } else {
        NSString *stt  = [NSString stringWithFormat:@"%ld",ind];
        ind--;
        if (ind == -1) {
            stt = @"";
        }
        param[@"state"] = stt;
        url = getScanOrders;
//    }
    //NSLog(@"请求链接%@ 参数%@", url, param);
    [NetWorkTool POST:url param:param success:^(id dic) {
        
        self.mydateSource = [CSmaintenanceListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 148;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.bgview.hidden = scrollView.contentOffset.y>0 ? YES:NO;
}

// any offset changes
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ScanOrderListCell *cell = [ScanOrderListCell cellViewWithTableView:tableView];
    
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    cell.data = model;
    //cell.comeFromType = self.comeFromType;
    [cell loadContent];
    kWeakSelf;
    cell.accessoriesDetailBlock = ^{
//        switch (self.comeFromType) {
//            case ComeFromTypeScanOrder:
//            {
                HouBaoAccessoryDetailVC *detailVC = [[HouBaoAccessoryDetailVC alloc] init];
                detailVC.model = model;
                [weakSelf.navigationController pushViewController:detailVC animated:YES];
//            }
//                break;
//            case ComeFromTypeHouBaoOrder:
//            {
//                HouBaoAccessOrderAccessDetailVC *detailVC = [[HouBaoAccessOrderAccessDetailVC alloc] init];
//                detailVC.maintenanceListModel = model;
//                KMyLog(@"有没有传值过去 %@", model);
//                [weakSelf.navigationController pushViewController:detailVC animated:YES];
//            }
//                break;
//            default:
//                break;
//        }
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return kScaleNum(10);
}

/**
 * 分区头
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

/**
 * 分区脚
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    //switch (self.comeFromType) {
        //case ComeFromTypeScanOrder:
        //{
            //扫码订单列表Cell点击跳转页面
            HouBaoAccessoryDetailVC *detailVC = [[HouBaoAccessoryDetailVC alloc] init];
            detailVC.model = model;
            [self.navigationController pushViewController:detailVC animated:YES];
        //}
            //break;
        //case ComeFromTypeHouBaoOrder:
        //{
            //HouBaoAccessOrderAccessDetailVC *detailVC = [[HouBaoAccessOrderAccessDetailVC alloc] init];
            //detailVC.maintenanceListModel = model;
            //[self.navigationController pushViewController:detailVC animated:YES];
        //}
            //break;
        //default:
            //break;
    //}
}

@end
