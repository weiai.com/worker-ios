//
//  ScanOrderListCell.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"
#import "ScanOrderListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanOrderListCell : BaseLeftImageLabelRightImageLabelCell
//@property (nonatomic, assign) ComeFromType comeFromType;
@property (nonatomic, copy) void(^accessoriesDetailBlock)(void);

@end

NS_ASSUME_NONNULL_END
