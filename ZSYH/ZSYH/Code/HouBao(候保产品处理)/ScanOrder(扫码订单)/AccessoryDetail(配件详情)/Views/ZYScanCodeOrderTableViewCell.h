//
//  ZYScanCodeOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseLeftImageLabelRightImageLabelCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYScanCodeOrderTableViewCell : BaseLeftImageLabelRightImageLabelCell
@property (nonatomic, copy) void(^accessoryDamageBlock)(void);

@end

NS_ASSUME_NONNULL_END
