//
//  ZYScanCodeOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYScanCodeOrderTableViewCell.h"
#import "CSmaintenanceListModel.h"

@interface ZYScanCodeOrderTableViewCell ()
@property (nonatomic, strong) UIButton *accessoryDamageButton;
@property (nonatomic, strong) NSMutableArray *contentLabelArray;

@end

@implementation ZYScanCodeOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(10), kScaleNum(10)));
    }];
    [self.backView shadowshadowOpacity:0.12 borderWidth:kScaleNum(1) borderColor:kMain_Color erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
    
    NSArray *titleArray = @[@"配件品类:", @"配件名称:", @"有效期:", @"保质期:"];
    self.contentLabelArray = [NSMutableArray arrayWithCapacity:1];
    for (int i = 0; i < titleArray.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.textColor = kMainTextColor66;
        titleLabel.font = KFontPingFangSCLight(14);
        titleLabel.text = titleArray[i];
        [self.backView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44), 0, 0));
            make.top.equalTo(self.backView).mas_offset(kScaleNum(30) + kScaleNum(15 + 15) * i);
            make.height.mas_equalTo(kScaleNum(15));
            make.width.mas_equalTo(kScaleNum(65));
        }];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.textAlignment = NSTextAlignmentLeft;
        contentLabel.textColor = kMainTextColor66;
        contentLabel.font = KFontPingFangSCMedium(14);
        contentLabel.text = titleArray[i];
        [self.backView addSubview:contentLabel];
        [self.contentLabelArray addObject:contentLabel];
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(titleLabel);
            make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
            make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
        }];
        
        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self.backView addSubview:bottomLine];
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kLineHeight);
            make.left.equalTo(titleLabel);
            make.right.equalTo(self.backView);
            make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(7));
        }];
        
        if (i == titleArray.count - 1) {
            UIButton *accessoryDamageButton = [UIButton buttonWithType:UIButtonTypeCustom];
            accessoryDamageButton.adjustsImageWhenDisabled = NO;
            accessoryDamageButton.adjustsImageWhenHighlighted = NO; accessoryDamageButton.layer.masksToBounds = YES;
            [accessoryDamageButton rounded:kScaleNum(15) width:kScaleNum(1) color:kMain_Color];
            accessoryDamageButton.backgroundColor = kMain_Color;
            NSIndexPath *indexPath = self.data;
            if (indexPath.row % 2 == 0) {
                [accessoryDamageButton setTitle:@"配件已损坏并申请质保" forState:UIControlStateNormal];
            } else if (indexPath.row % 2 == 1) {
                [accessoryDamageButton setHidden:YES];
            }
            
            [accessoryDamageButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            accessoryDamageButton.titleLabel.font = KFontPingFangSCRegular(12);
            [accessoryDamageButton sizeToFit];
            [accessoryDamageButton addTarget:self action:@selector(accessoryDamageButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
            [self.backView addSubview:accessoryDamageButton];
            self.accessoryDamageButton = accessoryDamageButton;
            [accessoryDamageButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(bottomLine.mas_bottom).mas_offset(kScaleNum(12));
                make.right.equalTo(self.backView).mas_offset(kScaleNum(-12));
                make.height.mas_equalTo(kScaleNum(30));
                make.width.mas_equalTo(accessoryDamageButton.width + kScaleNum(30));
                make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-12));
            }];
        }
    }
}

- (void)loadContent {
    CSmaintenanceListModel *model = self.data;
    UILabel *typeNameLabel = [self.contentLabelArray firstObject];//配件类型
    UILabel *accessoryNameLabel = [self.contentLabelArray objectAtIndex:1];//配件名
    UILabel *termOfValidityLabel = [self.contentLabelArray objectAtIndex:2];//有效期
    UILabel *expirationDateLabel = [self.contentLabelArray objectAtIndex:3];//保质期
    
    typeNameLabel.text = model.type_name;
    accessoryNameLabel.text = model.product_name;
    
    if (![model.validity isEqualToString:@"(null)至(null)"]) {
        termOfValidityLabel.text = model.validity;
    } else {
        termOfValidityLabel.text = [NSString stringWithFormat:@"%@个月", model.install];
    }
    
    expirationDateLabel.text = [NSString stringWithFormat:@"%@%@", model.quality,@"个月"];

    //状态1正常2取消3电器厂已下单
    if ([model.state isEqualToString:@"1"]) {
        KMyLog(@"正常状态");
    } else if ([model.state isEqualToString:@"2"]) {
        KMyLog(@"取消");
        self.accessoryDamageButton.hidden = YES;
        
        UIImageView *sunhuaiImg = [[UIImageView alloc] init];
        sunhuaiImg.frame = CGRectMake(self.backView.right-149, self.backView.height-40, 112, 25);
        [sunhuaiImg setImage:imgname(@"smallSunhuai")];
        [self.backView addSubview:sunhuaiImg];
        
    } else if ([model.state isEqualToString:@"3"]) {
        KMyLog(@"敬请期待");
    }
    
}

- (void)accessoryDamageButtonEvent:(UIButton *)sender {
    NSLog(@"配件损坏");
    if (self.accessoryDamageBlock) {
        self.accessoryDamageBlock();
    }
}

@end
