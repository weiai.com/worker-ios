//
//  HouBaoAccessoryDetailAccessoryInfoCell.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouBaoAccessoryDetailAccessoryInfoCell : BaseLeftImageLabelRightImageLabelCell
@property (nonatomic, copy) void(^accessoryDamageBlock)(void);

@property (nonatomic, strong) UILabel *typeNameLabel;//配件类型
@property (nonatomic, strong) UILabel *accessoryNameLabel;//配件名
@property (nonatomic, strong) UILabel *termOfValidityLabel;//有效期
@property (nonatomic, strong) UILabel *expirationDateLabel;//保质期

//----------------------------------------------------------------/

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *pjplTitLab; //配件品类 标题
@property (nonatomic, strong) UILabel *pjplConLab; //配件品类 内容
@property (nonatomic, strong) UILabel *pjmcTitLab; //配件名称 标题
@property (nonatomic, strong) UILabel *pjmcConLab; //配件名称 内容
@property (nonatomic, strong) UILabel *yxqTitLab; //有效期 标题
@property (nonatomic, strong) UILabel *yxqConLab; //有效期 内容
@property (nonatomic, strong) UILabel *bzqTitLab; //保质期 标题
@property (nonatomic, strong) UILabel *bzqConLab; //保质期 内容

@end

NS_ASSUME_NONNULL_END
