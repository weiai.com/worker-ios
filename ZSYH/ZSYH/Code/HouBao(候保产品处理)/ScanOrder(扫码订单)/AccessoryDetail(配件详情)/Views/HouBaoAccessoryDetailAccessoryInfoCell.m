//
//  HouBaoAccessoryDetailAccessoryInfoCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoAccessoryDetailAccessoryInfoCell.h"
#import "CSmaintenanceListModel.h"

@interface HouBaoAccessoryDetailAccessoryInfoCell ()
@property (nonatomic, strong) UIButton *accessoryDamageButton;
@property (nonatomic, strong) NSMutableArray *contentLabelArray;
@end

@implementation HouBaoAccessoryDetailAccessoryInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH, 219);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UILabel *bgLabel = [[UILabel alloc] init];
        bgLabel.frame = CGRectMake(10, 10, KWIDTH-20, 199);
        bgLabel.layer.borderWidth = 1;
        bgLabel.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        bgLabel.layer.cornerRadius = 12;
        bgLabel.layer.masksToBounds = YES;
        [bottomV addSubview:bgLabel];
        
        UILabel *pjplTitLab = [[UILabel alloc] init];
        pjplTitLab.frame = CGRectMake(16, 32, 70, 14);
        pjplTitLab.text = @"配件品类";
        pjplTitLab.font = FontSize(14);
        pjplTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bgLabel addSubview:pjplTitLab];
        self.pjplTitLab = pjplTitLab;
        
        UILabel *pjplConLab = [[UILabel alloc] init];
        pjplConLab.frame = CGRectMake(pjplTitLab.right+10, 32, KWIDTH-20-32-70-10, 14);
        pjplConLab.font = FontSize(14);
        pjplConLab.textColor = K333333;
        [bgLabel addSubview:pjplConLab];
        self.pjplConLab = pjplConLab;
        
        UILabel *line = [[UILabel alloc] init];
        line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        line.frame = CGRectMake(16, pjplTitLab.bottom+8, KWIDTH-20-32, 1);
        [bgLabel addSubview:line];
        
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(16, line.bottom+9, 70, 14);
        pjmcTitLab.text = @"配件名称";
        pjmcTitLab.font = FontSize(14);
        pjmcTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bgLabel addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        
        UILabel *pjmcConLab = [[UILabel alloc] init];
        pjmcConLab.frame = CGRectMake(pjplTitLab.right+10, line.bottom+9, KWIDTH-20-32-70-10, 14);
        pjmcConLab.font = FontSize(14);
        pjmcConLab.textColor = K333333;
        [bgLabel addSubview:pjmcConLab];
        self.pjmcConLab = pjmcConLab;
        
        UILabel *line1 = [[UILabel alloc] init];
        line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        line1.frame = CGRectMake(16, pjmcTitLab.bottom+8, KWIDTH-20-32, 1);
        [bgLabel addSubview:line1];
        
        UILabel *yxqTitLab = [[UILabel alloc] init];
        yxqTitLab.frame = CGRectMake(16, line1.bottom+9, 70, 14);
        yxqTitLab.text = @"有效期";
        yxqTitLab.font = FontSize(14);
        yxqTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bgLabel addSubview:yxqTitLab];
        self.yxqTitLab = yxqTitLab;
        
        UILabel *yxqConLab = [[UILabel alloc] init];
        yxqConLab.frame = CGRectMake(pjplTitLab.right+10, line1.bottom+9, KWIDTH-20-32-70-10, 14);
        yxqConLab.font = FontSize(14);
        yxqConLab.textColor = K333333;
        [bgLabel addSubview:yxqConLab];
        self.yxqConLab = yxqConLab;
        
        UILabel *line2 = [[UILabel alloc] init];
        line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        line2.frame = CGRectMake(16, yxqTitLab.bottom+8, KWIDTH-20-32, 1);
        [bgLabel addSubview:line2];
        
        UILabel *bzqTitLab = [[UILabel alloc] init];
        bzqTitLab.frame = CGRectMake(16, line2.bottom+9, 70, 14);
        bzqTitLab.text = @"保质期";
        bzqTitLab.font = FontSize(14);
        bzqTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
        [bgLabel addSubview:bzqTitLab];
        self.bzqTitLab = bzqTitLab;
        
        UILabel *bzqConLab = [[UILabel alloc] init];
        bzqConLab.frame = CGRectMake(pjplTitLab.right+10, line2.bottom+9, KWIDTH-20-32-70-10, 14);
        bzqConLab.font = FontSize(14);
        bzqConLab.textColor = K333333;
        [bgLabel addSubview:bzqConLab];
        self.bzqConLab = bzqConLab;
        
        UILabel *line3 = [[UILabel alloc] init];
        line3.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        line3.frame = CGRectMake(16, bzqTitLab.bottom+8, KWIDTH-20-32, 1);
        [bgLabel addSubview:line3];
    }
    return self;
}

/*- (void)buildSubview {
 [super buildSubview];
 self.contentView.backgroundColor = [UIColor clearColor];
 self.backgroundColor = [UIColor clearColor];
 [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
 make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
 }];
 [self.backView shadowshadowOpacity:0.12 borderWidth:kScaleNum(1) borderColor:kMain_Color erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
 
 NSArray *titleArray = @[@"配件品类:", @"配件名称:", @"有效期:", @"保质期:"];
 self.contentLabelArray = [NSMutableArray arrayWithCapacity:1];
 for (int i = 0; i < titleArray.count; i++) {
 UILabel *titleLabel = [[UILabel alloc] init];
 titleLabel.backgroundColor = [UIColor clearColor];
 titleLabel.textAlignment = NSTextAlignmentLeft;
 titleLabel.textColor = kMainTextColor66;
 titleLabel.font = KFontPingFangSCLight(14);
 titleLabel.text = titleArray[i];
 [self.backView addSubview:titleLabel];
 [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44), 0, 0));
 make.top.equalTo(self.backView).mas_offset(kScaleNum(30) + kScaleNum(15 + 15) * i);
 make.height.mas_equalTo(kScaleNum(15));
 make.width.mas_equalTo(kScaleNum(65));
 }];
 
 UILabel *contentLabel = [[UILabel alloc] init];
 contentLabel.backgroundColor = [UIColor clearColor];
 contentLabel.textAlignment = NSTextAlignmentLeft;
 contentLabel.textColor = kMainTextColor66;
 contentLabel.font = KFontPingFangSCMedium(14);
 contentLabel.text = titleArray[i];
 [self.backView addSubview:contentLabel];
 [self.contentLabelArray addObject:contentLabel];
 [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
 make.top.height.equalTo(titleLabel);
 make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
 make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
 }];
 
 UIView *bottomLine = [[UIView alloc] init];
 bottomLine.backgroundColor = kMainLineColor;
 [self.backView addSubview:bottomLine];
 [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
 make.height.mas_equalTo(kLineHeight);
 make.left.equalTo(titleLabel);
 make.right.equalTo(self.backView);
 make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(7));
 }];
 
 if (i == titleArray.count - 1) {
 UIButton *accessoryDamageButton = [UIButton buttonWithType:UIButtonTypeCustom];
 accessoryDamageButton.adjustsImageWhenDisabled = NO;
 accessoryDamageButton.adjustsImageWhenHighlighted = NO; accessoryDamageButton.layer.masksToBounds = YES;
 [accessoryDamageButton rounded:kScaleNum(15) width:kScaleNum(1) color:kMain_Color];
 accessoryDamageButton.backgroundColor = kMain_Color;
 NSIndexPath *indexPath = self.data;
 if (indexPath.row % 2 == 0) {
 [accessoryDamageButton setTitle:@"配件已损坏并申请质保" forState:UIControlStateNormal];
 } else if (indexPath.row % 2 == 1) {
 [accessoryDamageButton setHidden:YES];
 }
 
 [accessoryDamageButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
 accessoryDamageButton.titleLabel.font = KFontPingFangSCRegular(12);
 [accessoryDamageButton sizeToFit];
 [accessoryDamageButton addTarget:self action:@selector(accessoryDamageButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
 [self.backView addSubview:accessoryDamageButton];
 self.accessoryDamageButton = accessoryDamageButton;
 [accessoryDamageButton mas_makeConstraints:^(MASConstraintMaker *make) {
 make.top.equalTo(bottomLine.mas_bottom).mas_offset(kScaleNum(12));
 make.right.equalTo(self.backView).mas_offset(kScaleNum(-12));
 make.height.mas_equalTo(kScaleNum(30));
 make.width.mas_equalTo(accessoryDamageButton.width + kScaleNum(30));
 make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-12));
 }];
 }
 }
 }
 
 - (void)loadContent {
 CSmaintenanceListModel *model = self.data;
 UILabel *typeNameLabel = [self.contentLabelArray firstObject];//配件类型
 UILabel *accessoryNameLabel = [self.contentLabelArray objectAtIndex:1];//配件名
 UILabel *termOfValidityLabel = [self.contentLabelArray objectAtIndex:2];//有效期
 UILabel *expirationDateLabel = [self.contentLabelArray objectAtIndex:3];//保质期
 typeNameLabel.text = model.type_name;
 accessoryNameLabel.text = model.product_name;
 termOfValidityLabel.text = model.validity;
 
 //状态1正常2取消3电器厂已下单
 if ([model.state isEqualToString:@"1"]) {
 KMyLog(@"正常状态");
 } else if ([model.state isEqualToString:@"2"]) {
 KMyLog(@"取消");
 self.accessoryDamageButton.hidden = YES;
 } else if ([model.state isEqualToString:@"3"]) {
 KMyLog(@"敬请期待");
 }
 
 expirationDateLabel.text = [NSString stringWithFormat:@"%@", model.quality];
 }*/

- (void)accessoryDamageButtonEvent:(UIButton *)sender {
    NSLog(@"配件损坏");
    if (self.accessoryDamageBlock) {
        self.accessoryDamageBlock();
    }
}

@end
