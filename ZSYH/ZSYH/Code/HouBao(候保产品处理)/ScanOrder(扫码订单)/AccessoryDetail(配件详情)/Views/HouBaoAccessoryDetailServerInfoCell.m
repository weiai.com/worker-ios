//
//  HouBaoAccessoryDetailServerInfoCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoAccessoryDetailServerInfoCell.h"
#import "CSmaintenanceListModel.h"

@interface HouBaoAccessoryDetailServerInfoCell ()
@property (nonatomic, strong) UILabel *orderNumberLabel;
@property (nonatomic, strong) NSMutableArray *textFieldArray;
@property (nonatomic, strong) UIView *line;
@end

@implementation HouBaoAccessoryDetailServerInfoCell

- (NSMutableArray *)textFieldArray {
    if (!_textFieldArray) {
        _textFieldArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _textFieldArray;
}

- (void)buildSubview {
    [super buildSubview];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
    }];
    [self.backView shadowshadowOpacity:0.12 borderWidth:0 borderColor:[UIColor clearColor] erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
    
    self.titleLabel.text = @"已损坏";
    UIFont *font = KFontPingFangSCRegular(14);
    self.titleLabel.font = font;
    self.titleLabel.textColor = kColorWithHex(0xF88B1F);
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(10));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(16));
    }];
    
    self.bottomLine.backgroundColor = kMainBackGroundColor;
    [self.bottomLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(70));
        make.left.right.equalTo(self.backView);
        make.height.mas_equalTo(kScaleNum(2));
    }];
    
    NSArray *titleArray = @[@"用户姓名", @"用户手机号", @"故障原因", @"备      注"];
    for (int i = 0; i < titleArray.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        NSMutableAttributedString *attrString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"%@", titleArray[i]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
        titleLabel.attributedText = attrString;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:titleLabel];
        if (i == titleArray.count - 1) {
            
            [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44 - 12), 0, 0));
                make.top.equalTo(self->_line.mas_bottom).mas_offset(kScaleNum(9));
                make.height.mas_greaterThanOrEqualTo(kScaleNum(15));
                make.width.mas_equalTo(kScaleNum(77));
            }];
            
        }else
        {
            [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44 - 12), 0, 0));
                make.top.equalTo(self.bottomLine.mas_bottom).mas_offset(kScaleNum(20) + kScaleNum(15 + 25) * i);
                make.height.mas_greaterThanOrEqualTo(kScaleNum(15));
                make.width.mas_equalTo(kScaleNum(77));
            }];
        }
       
        
        //label
        UILabel *searchLab = [[UILabel alloc] init];
        searchLab.backgroundColor = [UIColor clearColor];
        searchLab.numberOfLines = 0;
        searchLab.userInteractionEnabled = NO;
        searchLab.font = KFontPingFangSCMedium(14);
        [self.backView addSubview:searchLab];
        [self.textFieldArray addObject:searchLab];
        [searchLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_top);
            make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
            make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
            make.height.mas_greaterThanOrEqualTo(kScaleNum(15));
        }];

        
        _line = [[UIView alloc] init];
        _line.backgroundColor = kMainLineColor;
        [self.backView addSubview:_line];
        [_line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kLineHeight);
            make.left.equalTo(titleLabel);
            make.top.equalTo(searchLab.mas_bottom).mas_offset(kScaleNum(9));
            make.right.equalTo(self.backView);
        }];
        
        if (i == titleArray.count - 1) {
            UILabel *orderNumberLabel = [[UILabel alloc] init];
            orderNumberLabel.backgroundColor = [UIColor clearColor];
            orderNumberLabel.font = FontSize(12);
            orderNumberLabel.textColor = K666666;
            orderNumberLabel.textAlignment = NSTextAlignmentLeft;
            [self.backView addSubview:orderNumberLabel];
            self.orderNumberLabel = orderNumberLabel;
            [orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(titleLabel);
                make.top.equalTo(self->_line.mas_bottom).mas_offset(kScaleNum(13));
                make.bottom.equalTo(self.backView).mas_equalTo(kScaleNum(-16));
            }];
        }
    }
}

- (void)loadContent {
    
    CSmaintenanceListModel *model = self.data;
    UILabel *nameTextField = [self.textFieldArray objectAtIndex:0];
    UILabel *phoneTextField = [self.textFieldArray objectAtIndex:1];
    UILabel *faultTextField = [self.textFieldArray objectAtIndex:2];
    UILabel *remakeTextField = [self.textFieldArray objectAtIndex:3];
    nameTextField.text = model.user_name;
    phoneTextField.text = model.user_phone;
    faultTextField.text = model.fault_common;
    remakeTextField.text = model.remake;
    
    self.orderNumberLabel.text = [NSString stringWithFormat:@"%@%@", @"订单编号:", model.Id];
    
    if (model.state.integerValue == 1) {//已安装
        self.titleLabel.text = @"已安装";
    } else {//已损坏
        //self.titleLabel.text = @"已损坏";
        self.titleLabel.hidden = YES;
        
        UIImageView *sunhuaiImg = [[UIImageView alloc] init];
        sunhuaiImg.frame = CGRectMake(KWIDTH/2-35, 13, 70, 46);
        [sunhuaiImg setImage:imgname(@"sunhuai")];
        [self.backView addSubview:sunhuaiImg];
    }
}

@end
