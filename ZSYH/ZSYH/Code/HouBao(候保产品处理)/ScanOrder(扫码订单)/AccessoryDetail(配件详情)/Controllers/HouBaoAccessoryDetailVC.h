//
//  HouBaoAccessoryDetailVC.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "YJBaseTableViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouBaoAccessoryDetailVC : YJBaseTableViewController
@property (nonatomic, strong) CSmaintenanceListModel *model;
@property (nonatomic, copy) void(^myblock)(NSString *str);
@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
