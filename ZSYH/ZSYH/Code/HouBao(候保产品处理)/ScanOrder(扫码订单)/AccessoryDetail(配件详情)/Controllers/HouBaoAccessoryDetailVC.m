//
//  HouBaoAccessoryDetailVC.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoAccessoryDetailVC.h"
#import "HouBaoAccessoryDetailServerInfoCell.h"
//#import "HouBaoAccessoryDetailAccessoryInfoCell.h"
#import "ScanMakeOrderSectionFooterView.h"
#import "ZYScanCodeOrderTableViewCell.h"

@interface HouBaoAccessoryDetailVC ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UIView *bgViewthr;
@property (nonatomic, strong) NSString *indexOrderId;
@end

@implementation HouBaoAccessoryDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requeset];
    [self shwoPushOrderSuccessAlert]; //确认该配件已损坏?
    [self shwoTipsSuccessAlert]; //温馨提示
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScaleNum(150), 44)];
    titleLabel.text = @"配件详情";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = FontSize(16);
    self.navigationItem.titleView = titleLabel;
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, 0, 30, 44);
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(backEvent:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBBItem = [[UIBarButtonItem alloc] initWithCustomView:loginBtn];
    self.navigationItem.leftBarButtonItem = backBBItem;
    
}

- (void)backEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buildSubviews {
    
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - KNavHeight-TabbarSafeBottomMargin);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor;
    self.listTableView.estimatedRowHeight = kScaleNum(215);
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionHeaderHeight = kScaleNum(44);
    self.listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionFooterHeight = kScaleNum(44);
    self.listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.listTableView.isShowBtn = NO;
    self.listTableView.showNoData = YES;
    self.listTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.listTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self.listTableView.mj_header beginRefreshing];
    }];
    //self.listTableView.customImg = [UIImage imageNamed:(@"page_icon01_")];
    //self.listTableView.customMsg = @"暂时还没有数据哟";
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KBottomSafeViewHeight)];
    tableFooterView.backgroundColor = [UIColor redColor];
    self.listTableView.tableFooterView = tableFooterView;
    
}

#pragma mark - 数据请求
//这个界面不用数据请求,从前面携带
- (void)requeset {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setObject:[NSString stringWithFormat:@"%@", _model.parts_id] forKey:@"id"];
    [param setObject:[NSString stringWithFormat:@"%@", @"1"] forKey:@"user_type"];

    // 订单状态(0生成订单（未处理）//添加候保配件
    NSLog(@"请求链接 参数%@", param);
    [self.dataSourceArray removeAllObjects];
    [NetWorkTool POST:getPartsInfo param:param success:^(id dic) {
        NSLog(@"配件详情%@", dic);
        NSDictionary *data = [dic objectForKey:@"data"];
        if ([data isKindOfClass:[NSDictionary class]]) {
            NSDictionary *factory = [dic objectForKey:@"factory"];
            NSArray *parts = [factory objectForKey:@"parts"];
            if ([parts isKindOfClass:[NSArray class]]) {
                for (NSDictionary *partsDic in parts) {//配件数组
                    CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc]init];
                    [model setValuesForKeysWithDictionary:partsDic];
                    [weakSelf.dataSourceArray addObject:model];
                }
            }
            //整体请求的数据
            weakSelf.dataSourceDictionary = [data mutableCopy];
        }
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.dataSourceArray removeAllObjects];
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.dataSourceArray removeAllObjects];
        [self.listTableView reloadData];
        [self.listTableView.mj_header endRefreshing];
        
    } needUser:YES];
}

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {//第一分区返回配件数量
        return 1;
    }
    return 1;//第0分区返回1个,用户信息
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        HouBaoAccessoryDetailServerInfoCell *cell = [HouBaoAccessoryDetailServerInfoCell cellViewWithTableView:tableView];
        cell.data = self.model;
        [cell loadContent];
        [tableView beginUpdates];
        [tableView endUpdates];
        return cell;
    } else {
        ZYScanCodeOrderTableViewCell *cell = [ZYScanCodeOrderTableViewCell cellViewWithTableView:tableView];
        cell.data = self.model;
        [cell loadContent];
        cell.accessoryDamageBlock = ^{
            /*[LXAlertView initWithTitleAlertTitle:@"确认该候保配件已损坏？" titleImage:imgname(@"tipImage") Message:@"" sureTitle:@"确定" cancleTitle:@"取消" sureClicked:^(LXAlertView *alertView) {
                NSLog(@"确定");
                [LXAlertView initWithAlertTitleImage:imgname(@"wenXinTiShi") Message:@"已损坏的配件如在质保期内，请携带损坏配件到购买该配件的配件点，进行更换并申请补助。" sureTitle:@"知道了" sureClicked:^(LXAlertView *alertView) {
                    NSLog(@"知道了");
                }];
            } cancleClicked:^(LXAlertView *alertView) {
                
            }];*/
            //确认该候保配件已经损坏?
            [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];
            self.indexOrderId = self.model.Id;
            NSLog(@"哈哈哈哈哈哈哈哈哈 %@ 哈哈哈哈哈哈哈哈哈哈", self.indexOrderId);
            [self requeset];
            [self.listTableView reloadData];
        };
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return [UIView new];
    } else {
        ScanMakeOrderSectionFooterView *footerView = [ScanMakeOrderSectionFooterView sectionViewWithTableView:tableView];
        [footerView.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
        return footerView;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
}

- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
    [LXAlertView initWithTitleAlertTitle:@"订单已提交" titleImage:imgname(@"successapy") Message:@"可在我的订单或候保产品处理页面中查看该订单" sureTitle:@"我知道了" sureClicked:^(LXAlertView *alertView) {
        NSLog(@"我知道了");
    }];
}

/**
 弹出框的背景图 确认该候保配件已损坏？
 */
-(void)shwoPushOrderSuccessAlert {
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-30, 34, 60, 60)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 30;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 1461"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"";
    
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, KWIDTH-20, 34)];
    [whiteBGView addSubview:topLable];
    topLable.font = FontSize(16);
    topLable.numberOfLines = 0;
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.textAlignment =  NSTextAlignmentCenter;
    topLable.text = @"确认该候保配件已损坏？";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"";
    
    UIButton *endButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [endButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [endButton setTitleColor:K999999 forState:(UIControlStateNormal)];
    [endButton setBackgroundColor:[[UIColor colorWithHexString:@"#F2F2F2"] colorWithAlphaComponent:0.5]];
    endButton.layer.masksToBounds = YES;
    endButton.layer.cornerRadius = 4;
    [endButton addTarget:self action:@selector(endButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:endButton];
    [endButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.offset(28);
        make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.bottom.offset(-19);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(toHandleAction) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

- (void)endButtonAction {
    [_bgViewsec removeFromSuperview];
    [singlTool shareSingTool].needReasfh = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)toHandleAction {
    [_bgViewsec removeFromSuperview];
    //数据赋值
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(self.indexOrderId);
    
    [NetWorkTool POST:appGuaranteed param:param success:^(id dic) {
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    //确认该候保配件已经损坏?
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewthr];
    
    NSLog(@"您点击了 弹窗里面的确定按钮");
}

/**
 弹出框的背景图  温馨提示
 */
-(void)shwoTipsSuccessAlert {
    self.bgViewthr = [[UIView alloc]init];
    self.bgViewthr.frame = self.view.bounds;
    self.bgViewthr.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewthr addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(331);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH-22*2, 96)];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"wenXinTiShi"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(43, upImage.bottom+42, KWIDTH-44-86, 84)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"已损坏的配件如在质保期内，请携\n带损坏配件到购买该配件的配件\n点，进行更换并申请补助。";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikeno:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(70);
        make.right.offset(-70);
        make.bottom.offset(-27);
    }];
}

- (void)ikeno:(UIButton *)button {
    [_bgViewthr removeFromSuperview];
    if (self.myblock) {
        self.myblock(@"");
    }
    [singlTool shareSingTool].needReasfh = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"您点击了弹窗里边的取消按钮");
}

@end
