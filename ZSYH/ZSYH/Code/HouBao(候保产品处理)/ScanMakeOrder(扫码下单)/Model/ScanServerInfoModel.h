//
//  ScanServerInfoModel.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanServerInfoModel : BaseModel
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *user_phone;
@property (nonatomic, copy) NSString *user_address;
@property (nonatomic, copy) NSString *fault_common;
@property (nonatomic, copy) NSString *cost;
@property (nonatomic, copy) NSString *remake;
@end

NS_ASSUME_NONNULL_END
