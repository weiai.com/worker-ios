//
//  SaoMaAnZhuangFuWuXinXiCell.m
//  ZSYH
//
//  Created by 李 on 2019/11/20.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "SaoMaAnZhuangFuWuXinXiCell.h"

@interface SaoMaAnZhuangFuWuXinXiCell ()
@property (nonatomic, strong) ScanServerInfoModel *serverInfoModel;
@end

@implementation SaoMaAnZhuangFuWuXinXiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.backView shadowshadowOpacity:0.12 borderWidth:0 borderColor:[UIColor clearColor] erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
    
    [self.nameTF addTarget:self action:@selector(changeTextAction:) forControlEvents:UIControlEventEditingChanged];
    [self.phoneTF addTarget:self action:@selector(changeTextAction:) forControlEvents:UIControlEventEditingChanged];
    self.reasonTextView.delegate =self;
    self.noteTextView.delegate =self;
    self.reasonTextView.layoutManager.allowsNonContiguousLayout = NO;
    self.noteTextView.layoutManager.allowsNonContiguousLayout = NO;
    
    self.reasonTextView.scrollEnabled = YES;
    self.noteTextView.scrollEnabled = YES;
    
    self.nameTF.textColor = [UIColor colorWithHexString:@"#333333"];
    self.phoneTF.textColor = [UIColor colorWithHexString:@"#333333"];
    self.reasonTextView.textColor = [UIColor colorWithHexString:@"#333333"];
    self.noteTextView.textColor = [UIColor colorWithHexString:@"#333333"];
    
    NSArray *titleArray = @[@"用户姓名", @"用户手机号", @"故障原因", @"备注"];
    
        self.nameWordLab.backgroundColor = [UIColor clearColor];
        NSMutableAttributedString *attrString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[0]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
        self.nameWordLab.attributedText = attrString;
        self.nameWordLab.textAlignment = NSTextAlignmentLeft;
    
    self.phoneWordLab.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *attrString2 = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[1]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
    self.phoneWordLab.attributedText = attrString2;
    self.phoneWordLab.textAlignment = NSTextAlignmentLeft;
    
    self.reasonWordLab.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *attrString3 = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[2]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
    self.reasonWordLab.attributedText = attrString3;
    self.reasonWordLab.textAlignment = NSTextAlignmentLeft;
    
    
    self.noteLab.backgroundColor = [UIColor clearColor];
    NSMutableAttributedString *attrString4 = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[3]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
    self.noteLab.attributedText = attrString4;
    self.noteLab.textAlignment = NSTextAlignmentLeft;
    
}
- (ScanServerInfoModel *)serverInfoModel {
    if (!_serverInfoModel) {
        _serverInfoModel = [[ScanServerInfoModel alloc] init];
    }
    return _serverInfoModel;
}
#pragma textFieldDelegate
- (void)changeTextAction:(UITextField *)textField {
    if (textField == self.nameTF) {
        self.serverInfoModel.user_name = textField.text;
    }
    else if (textField == self.phoneTF)
    {
         self.serverInfoModel.user_phone = textField.text;
    }   
    if (self.changeTextBlock) {
        self.changeTextBlock(self.serverInfoModel);
    }
}

#pragma textViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.reasonTextView) {
        if (self.changeTextBlock) {
            if (textView.text.length>=0) {
                self.reasonPlaceHolder.hidden = YES;
            }else
            {
                self.reasonPlaceHolder.hidden = NO;
            }
            self.serverInfoModel.fault_common = textView.text;
            self.changeTextBlock(self.serverInfoModel);
        }
    }
    if (textView == self.noteTextView)
    {
        if (self.changeTextBlock) {
            if (textView.text.length>=0) {
                self.notePlaceHolder.hidden = YES;
            }else
            {
                self.notePlaceHolder.hidden = NO;
            }
            self.serverInfoModel.remake = textView.text;
            self.changeTextBlock(self.serverInfoModel);
        }
    }
   
}
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == self.reasonTextView) {
        if (self.changeTextBlock) {
            if (textView.text.length>=0) {
                self.reasonPlaceHolder.hidden = YES;
            }else
            {
                self.reasonPlaceHolder.hidden = NO;
            }
            self.serverInfoModel.fault_common = textView.text;
            self.changeTextBlock(self.serverInfoModel);
        }
    }
    if (textView == self.noteTextView)
    {
        if (self.changeTextBlock) {
            if (textView.text.length>=0) {
                self.notePlaceHolder.hidden = YES;
            }else
            {
                self.notePlaceHolder.hidden = NO;
            }
            self.serverInfoModel.remake = textView.text;
            self.changeTextBlock(self.serverInfoModel);
        }
    }
    
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == self.reasonTextView) {
       
            if (textView.text.length>=0) {
                self.reasonPlaceHolder.hidden = YES;
            }else
            {
                self.reasonPlaceHolder.hidden = NO;
            }
           
    }
    if (textView == self.noteTextView)
    {
      
            if (textView.text.length>=0) {
                self.notePlaceHolder.hidden = YES;
            }else
            {
                self.notePlaceHolder.hidden = NO;
            }
           
    }
    return YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
