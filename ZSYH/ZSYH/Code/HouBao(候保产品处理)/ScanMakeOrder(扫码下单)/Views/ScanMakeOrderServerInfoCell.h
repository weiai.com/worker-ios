//
//  ScanMakeOrderServerInfoCell.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"
#import "ScanServerInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ScanMakeOrderServerInfoCell : BaseLeftImageLabelRightImageLabelCell
@property (nonatomic, copy) void(^changeTextBlock)(ScanServerInfoModel *serverInfoModel);

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *headerTitLab;

@property (nonatomic, strong) UILabel *stateLab;      //状态
@property (nonatomic, strong) UILabel *nameLab;       //姓名 内容
@property (nonatomic, strong) UILabel *phoneLab;      //电话 内容
@property (nonatomic, strong) UILabel *addressLab;    //地址 内容
@property (nonatomic, strong) UILabel *fwlxTitLab;    //服务类型 标题
@property (nonatomic, strong) UILabel *fwlxConLab;    //服务类型 内容

@property (nonatomic, strong) UILabel *dqplTitLab;    //电器品类 标题
@property (nonatomic, strong) UILabel *dqplConLab;    //电器品类 内容

@property (nonatomic, strong) UILabel *yysjTitLab;    //预约时间 标题
@property (nonatomic, strong) UILabel *yysjConLab;    //预约时间 内容

@property (nonatomic, strong) UILabel *fwdzTitLab;    //服务地址 标题
@property (nonatomic, strong) UILabel *fwdzConLab;    //服务地址 内容

-(void)refasf:(CSmaintenanceListModel *)model;

@end

NS_ASSUME_NONNULL_END
