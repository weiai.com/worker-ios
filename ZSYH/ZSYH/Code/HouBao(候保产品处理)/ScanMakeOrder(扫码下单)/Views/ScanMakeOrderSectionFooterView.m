//
//  ScanMakeOrderSectionFooterView.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "ScanMakeOrderSectionFooterView.h"

@implementation ScanMakeOrderSectionFooterView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)buildSubview {
    [super buildSubview];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.backView.backgroundColor = [UIColor clearColor];
    self.rightButton.backgroundColor = kMain_Color;
    [self.rightButton rounded:kScaleNum(4)];
    [self.rightButton setTitle:@"提交" forState:UIControlStateNormal];
    [self.rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = FontSize(16);
    [self.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(40), kScaleNum(28), kScaleNum(40), kScaleNum(28)));
        make.height.mas_equalTo(kScaleNum(48));
    }];
}

- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
}

@end
