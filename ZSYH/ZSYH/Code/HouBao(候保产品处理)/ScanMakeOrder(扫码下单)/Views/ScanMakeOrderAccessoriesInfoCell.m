//
//  ScanMakeOrderAccessoriesInfoCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "ScanMakeOrderAccessoriesInfoCell.h"
#import "CSmakeAccAddmodel.h"

@interface ScanMakeOrderAccessoriesInfoCell ()
@property (nonatomic, strong) UILabel *dowLabel;
@property (nonatomic, strong) NSMutableArray *contentLabelArray;
@end

@implementation ScanMakeOrderAccessoriesInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
    }];
    [self.backView shadowshadowOpacity:0.12 borderWidth:0 borderColor:[UIColor clearColor] erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
    
    self.titleLabel.text = @"配件信息";
    UIFont *font = KFontPingFangSCMedium(16);
    self.titleLabel.font = font;
    self.titleLabel.textColor = kMainTextColor33;
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(16));
        make.centerX.equalTo(self.backView);
    }];
    
    self.contentLabelArray = [NSMutableArray arrayWithCapacity:1];
    NSArray *titleArray = @[@"配件品类:", @"配件名称:", @"有效期:", @"保质期:", @"补贴费用:"];
    for (int i = 0; i < titleArray.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.textColor = kMainTextColor66;
        titleLabel.font = KFontPingFangSCLight(14);
        titleLabel.text = titleArray[i];
        [self.backView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44), 0, 0));
            make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(kScaleNum(20) + kScaleNum(15 + 15) * i);
            make.height.mas_equalTo(kScaleNum(15));
            make.width.mas_equalTo(kScaleNum(65));
            if (i == titleArray.count - 1) {
                make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-43));
            }
        }];
        
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.backgroundColor = [UIColor clearColor];
        contentLabel.textAlignment = NSTextAlignmentLeft;
        contentLabel.textColor = kMainTextColor66;
        contentLabel.font = KFontPingFangSCMedium(14);
        contentLabel.text = titleArray[i];
        [self.contentLabelArray addObject:contentLabel];
        [self.backView addSubview:contentLabel];
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(titleLabel);
            make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
            make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
        }];
        
        if (i == titleArray.count - 1) {
            UILabel *dowLabel = [[UILabel alloc] init];
            dowLabel.backgroundColor = [UIColor clearColor];
            dowLabel.textAlignment = NSTextAlignmentLeft;
            dowLabel.textColor = [UIColor redColor];
            dowLabel.font = KFontPingFangSCMedium(14);
            [self.backView addSubview:dowLabel];
            self.dowLabel = dowLabel;
            [dowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(titleLabel);
                make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(13));
                make.bottom.equalTo(self.backView).mas_equalTo(kScaleNum(-16));
            }];
        }
    }
}

- (void)loadContent {
    
    CSmakeAccAddmodel *addAccessoryModel = self.data;
    if (self.contentLabelArray.count >= 4) {
        UILabel *typeNameLabel = [self.contentLabelArray firstObject];//配件类型
        UILabel *accessoryNameLabel = [self.contentLabelArray objectAtIndex:1];//配件名
        UILabel *termOfValidityLabel = [self.contentLabelArray objectAtIndex:2];//有效期
        UILabel *expirationDateLabel = [self.contentLabelArray objectAtIndex:3];//保质期
        UILabel *subsidyfeeLabel = [self.contentLabelArray objectAtIndex:4]; //补贴费用
        typeNameLabel.text = addAccessoryModel.type_name;
        accessoryNameLabel.text = addAccessoryModel.parts_name;
        //有效期不存在时候，取install字段+个月
        if (addAccessoryModel.productionDate.length==0 && addAccessoryModel.termValidity.length==0 && addAccessoryModel.install.length>0) {
            termOfValidityLabel.text = [NSString stringWithFormat:@"%@个月",addAccessoryModel.install];
        } else {
            termOfValidityLabel.text = [NSString stringWithFormat:@"%@至%@",addAccessoryModel.productionDate ,addAccessoryModel.termValidity];
        }
        expirationDateLabel.text = [NSString stringWithFormat:@"%@个月", addAccessoryModel.warranty];
        subsidyfeeLabel.text = [NSString stringWithFormat:@"%@%@", @"￥",addAccessoryModel.compensationPrice];
        self.dowLabel.text = [NSString stringWithFormat:@"%@%@%@",@"从安装之日起",addAccessoryModel.warranty,@"个月质保补贴工时费"];
    }
}

@end
