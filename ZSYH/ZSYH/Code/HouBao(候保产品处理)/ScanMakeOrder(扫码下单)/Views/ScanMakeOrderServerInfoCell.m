//
//  ScanMakeOrderServerInfoCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "ScanMakeOrderServerInfoCell.h"

@interface ScanMakeOrderServerInfoCell ()
@property (nonatomic, strong) UILabel *dowLabel;
@property (nonatomic, strong) ScanServerInfoModel *serverInfoModel;
@end

@implementation ScanMakeOrderServerInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)setFrame:(CGRect)frame{
//    frame.origin.x += 10;
//    frame.origin.y += 10;
//    frame.size.height -= 10;
//    frame.size.width -= 20;
//    [super setFrame:frame];
//}
//
//-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
//    {
//        self.contentView.backgroundColor = [UIColor clearColor];
//        self.backgroundColor = [UIColor clearColor];
//        [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
//        }];
//        [self.backView shadowshadowOpacity:0.12 borderWidth:0 borderColor:[UIColor clearColor] erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];
//
//        self.titleLabel.text = @"需求服务信息";
//        UIFont *font = KFontPingFangSCMedium(16);
//        self.titleLabel.font = font;
//        self.titleLabel.textColor = kMainTextColor33;
//        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.backView).mas_offset(kScaleNum(16));
//            make.centerX.equalTo(self.backView);
//        }];
//
//        NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*用户姓名" originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:KFontPingFangSCLight(14)];
//        UILabel *nameTitLab = [[UILabel alloc]initWithFrame:CGRectMake(0, self.titleLabel.bottom + 26, KWIDTH-44, 42)];
//        [self.backView addSubview:nameTitLab];
//        nameTitLab.font = FontSize(14);
//        nameTitLab.numberOfLines = 0;
//        nameTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
//        nameTitLab.attributedText = attStr;
//        nameTitLab.textAlignment = NSTextAlignmentCenter;
//
////        NSArray *titleArray = @[@"用户姓名", @"用户手机号", @"故障原因"];
////        NSArray *placeholderArray = @[@"请填写用户姓名", @"请填写用户手机号", @"请填写故障原因"];
////        for (int i = 0; i < titleArray.count; i++) {
////            UILabel *titleLabel = [[UILabel alloc] init];
////            titleLabel.backgroundColor = [UIColor clearColor];
////            NSMutableAttributedString *attrString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[i]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
////            titleLabel.attributedText = attrString;
////            titleLabel.textAlignment = NSTextAlignmentLeft;
////            [self.backView addSubview:titleLabel];
////            [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
////                make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(44 - 12), 0, 0));
////                make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(kScaleNum(20) + kScaleNum(15 + 25) * i);
////                make.height.mas_equalTo(kScaleNum(15));
////                make.width.mas_equalTo(kScaleNum(77));
////                if (i == titleArray.count - 1) {
////                    make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-23));
////                }
////            }];
////
////            //输入框
////            UITextField *searchTF = [[UITextField alloc] init];
////            searchTF.backgroundColor = [UIColor clearColor];
////            searchTF.borderStyle = UITextBorderStyleNone;
////            searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
////            searchTF.placeholder = placeholderArray[i];
////            searchTF.font = KFontPingFangSCMedium(14);
////            searchTF.tag = 1000 + i;
////
////            [searchTF addTarget:self action:@selector(changeTextAction:) forControlEvents:UIControlEventEditingChanged];
////            [self.backView addSubview:searchTF];
////            [searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
////                make.top.height.equalTo(titleLabel);
////                make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
////                make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
////            }];
////
////            UIView *bottomLine = [[UIView alloc] init];
////            bottomLine.backgroundColor = kMainLineColor;
////            [self.backView addSubview:bottomLine];
////            [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
////                make.height.mas_equalTo(kLineHeight);
////                make.left.equalTo(titleLabel);
////                make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(9));
////                make.right.equalTo(self.backView);
////            }];
////        }
//    }
//    return self;
//}

- (ScanServerInfoModel *)serverInfoModel {
    if (!_serverInfoModel) {
        _serverInfoModel = [[ScanServerInfoModel alloc] init];
    }
    return _serverInfoModel;
}

- (void)buildSubview {
    [super buildSubview];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
    }];
    [self.backView shadowshadowOpacity:0.12 borderWidth:0 borderColor:[UIColor clearColor] erRadius:kScaleNum(12) shadowColor:[UIColor blackColor] shadowRadius:kScaleNum(12) shadowOffset:CGSizeMake(0, 3)];

    self.titleLabel.text = @"需求服务信息";
    UIFont *font = KFontPingFangSCMedium(16);
    self.titleLabel.font = font;
    self.titleLabel.textColor = kMainTextColor33;
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(16));
        make.centerX.equalTo(self.backView);
    }];

}

- (void)loadContent {
    NSArray *titleArray = @[@"用户姓名", @"用户手机号", @"故障原因", @"备注"];
    NSArray *placeholderArray = @[@"请填写用户姓名", @"请填写用户手机号", @"请填写故障原因", @"请填写备注信息"];
    for (int i = 0; i < titleArray.count; i++) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.backgroundColor = [UIColor clearColor];
        NSMutableAttributedString *attrString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"*%@", titleArray[i]] originalColor:kMainTextColor66 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"*" changeColor:kColorWithHex(0xFF000A) changeFont:KFontPingFangSCLight(14)];
        titleLabel.attributedText = attrString;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.backView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(UIEdgeInsetsMake(0, kScaleNum(15), 0, 0));
            make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(kScaleNum(20) + kScaleNum(15 + 25) * i);
            make.height.mas_equalTo(kScaleNum(15));
            make.width.mas_equalTo(kScaleNum(77));
            if (i == titleArray.count - 1) {
                make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-23));
            }
        }];

        //输入框
        UITextField *searchTF = [[UITextField alloc] init];
        searchTF.backgroundColor = [UIColor clearColor];
        searchTF.borderStyle = UITextBorderStyleNone;
        searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        searchTF.placeholder = placeholderArray[i];
        searchTF.font = KFontPingFangSCMedium(14);
        searchTF.tag = 1000 + i;
        [searchTF addTarget:self action:@selector(changeTextAction:) forControlEvents:UIControlEventEditingChanged];
        [self.backView addSubview:searchTF];
        [searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.height.equalTo(titleLabel);
            make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
            make.left.equalTo(titleLabel.mas_right).mas_offset(kScaleNum(15));
        }];

        UIView *bottomLine = [[UIView alloc] init];
        bottomLine.backgroundColor = kMainLineColor;
        [self.backView addSubview:bottomLine];
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kLineHeight);
            make.left.equalTo(titleLabel);
            make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(9));
            make.right.equalTo(self.backView);
        }];
    }
}

- (void)changeTextAction:(UITextField *)textField {
    NSInteger tag = textField.tag - 1000;
    NSLog(@"%@", textField.text);
    switch (tag) {
        case 0:
            self.serverInfoModel.user_name = textField.text;
            break;
        case 1:
            self.serverInfoModel.user_phone = textField.text;
            break;
        case 2:
            self.serverInfoModel.fault_common = textField.text;
            break;
        case 3:
            self.serverInfoModel.remake = textField.text;
            break;
        default:
            break;
    }
    if (self.changeTextBlock) {
        self.changeTextBlock(self.serverInfoModel);
    }
}

@end
