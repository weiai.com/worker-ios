//
//  SaoMaAnZhuangFuWuXinXiCell.h
//  ZSYH
//
//  Created by 李 on 2019/11/20.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScanServerInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SaoMaAnZhuangFuWuXinXiCell : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameWordLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneWordLab;
@property (weak, nonatomic) IBOutlet UILabel *reasonWordLab;
@property (weak, nonatomic) IBOutlet UILabel *noteLab;

@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UILabel *reasonPlaceHolder;
@property (weak, nonatomic) IBOutlet UITextView *reasonTextView;
@property (weak, nonatomic) IBOutlet UILabel *notePlaceHolder;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (nonatomic, copy) void(^changeTextBlock)(ScanServerInfoModel *serverInfoModel);
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

NS_ASSUME_NONNULL_END
