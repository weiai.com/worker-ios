//
//  ScanMakeOrderViewController.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "YJBaseTableViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ScanMakeOrderViewController : YJBaseTableViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;

@end

NS_ASSUME_NONNULL_END
