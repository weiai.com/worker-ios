//
//  ScanMakeOrderViewController.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/18.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "ScanMakeOrderViewController.h"
#import "ScanMakeOrderAccessoriesInfoCell.h"
#import "ScanMakeOrderSectionFooterView.h"
#import "SaoMaAnZhuangFuWuXinXiCell.h"

@interface ScanMakeOrderViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) ScanServerInfoModel *serverInfoModel;
@property (nonatomic, strong) UITableView *listTableView;
@property (nonatomic, strong) UIView *bgView;

@end
static NSString *const SaoMaAnZhuangFuWuXinXiCellID = @"SaoMaAnZhuangFuWuXinXiCell";

@implementation ScanMakeOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self shwoBgview];
    // Do any additional setup after loading the view.
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScaleNum(150), 44)];
    titleLabel.text = @"扫码安装";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = FontSize(16);
    self.navigationItem.titleView = titleLabel;
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, 0, 30, 44);
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(backEvent:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBBItem = [[UIBarButtonItem alloc] initWithCustomView:loginBtn];
    self.navigationItem.leftBarButtonItem = backBBItem;
    self.navigationItem.leftBarButtonItem.title = @"";
}

- (void)backEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buildSubviews {

    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - KNavHeight);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor;
    self.listTableView.estimatedRowHeight = kScaleNum(215);
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionHeaderHeight = kScaleNum(44);
    self.listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionFooterHeight = kScaleNum(44);
    self.listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.listTableView.isShowBtn = NO;
    self.listTableView.showNoData = YES;
    self.listTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.listTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self.listTableView.mj_header beginRefreshing];
    }];
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SaoMaAnZhuangFuWuXinXiCell class]) bundle:nil] forCellReuseIdentifier:SaoMaAnZhuangFuWuXinXiCellID];
    
    //self.listTableView.customImg = [UIImage imageNamed:(@"page_icon01_")];
    //self.listTableView.customMsg = @"暂时还没有数据哟";
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KBottomSafeViewHeight)];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.listTableView.tableFooterView = tableFooterView;
}

#pragma mark - 请求信息
- (void)requesrtData {
    
    if (self.serverInfoModel.user_name.length == 0) {
        ShowToastWithText(@"请填写用户姓名");
        return;
    }
    if (self.serverInfoModel.user_phone.length == 0) {
        ShowToastWithText(@"请填写用户手机号");
        return;
    }
    if (self.serverInfoModel.fault_common.length == 0) {
        ShowToastWithText(@"请填写故障原因");
        return;
    }
    if (self.serverInfoModel.remake.length == 0) {
        ShowToastWithText(@"请填写备注信息");
        return;
    }
    
    //效验输入的是否是正确的手机号 去除字符串中空格
    self.serverInfoModel.user_phone = [self.serverInfoModel.user_phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:self.serverInfoModel.user_phone]) {
        ShowToastWithText(@"请输入正确电话号码");
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"parts_id"] = safe(self.accessAddModel.Id);
    param[@"qrcode"] = safe(self.accessAddModel.qrcode);
    param[@"user_name"] = safe(self.serverInfoModel.user_name);
    param[@"user_phone"] = safe(self.serverInfoModel.user_phone);;
    param[@"user_address"] = safe(self.serverInfoModel.user_address);
    param[@"fault_common"] = safe(self.serverInfoModel.fault_common);
    param[@"cost"] = safe(self.serverInfoModel.cost);
    param[@"remake"] = safe(self.serverInfoModel.remake);
    
    //install
    
    if (self.accessAddModel.productionDate == nil && self.accessAddModel.productionDate == NULL && self.accessAddModel.termValidity == nil && self.accessAddModel.termValidity == NULL) {
        NSString *time = [NSString stringWithFormat:@"%@个月", self.accessAddModel.install];
        param[@"validity"] = safe(time);
    } else {
        NSString *time = [NSString stringWithFormat:@"%@至%@", self.accessAddModel.productionDate, self.accessAddModel.termValidity];
        param[@"validity"] = safe(time);
    }
    param[@"quality"] = safe(self.accessAddModel.warranty);
    NSString *dataString = [HFTools toJSONString:param];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkTool POST:createScanOrder param:@{@"data" : dataString} success:^(id dic) {
        KMyLog(@"生成扫码单%@",dic);
       
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgView];

    } other:^(id dic) {
        NSString *tip = dic[@"msg"];
        ShowToastWithText(tip);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row ==1) {
        kWeakSelf;
        return [tableView fd_heightForCellWithIdentifier:SaoMaAnZhuangFuWuXinXiCellID configuration:^(SaoMaAnZhuangFuWuXinXiCell * cell) {
          
            cell.nameTF.text = weakSelf.serverInfoModel.user_name;
            cell.phoneTF.text = weakSelf.serverInfoModel.user_phone;
            cell.reasonTextView.text = weakSelf.serverInfoModel.fault_common;
            cell.noteTextView.text = weakSelf.serverInfoModel.remake;

        }];
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ScanMakeOrderAccessoriesInfoCell *cell = [ScanMakeOrderAccessoriesInfoCell cellViewWithTableView:tableView];
        cell.data = self.accessAddModel;
        [cell loadContent];
        return cell;
    } else {
        
        
        kWeakSelf;
        SaoMaAnZhuangFuWuXinXiCell *cell = [tableView dequeueReusableCellWithIdentifier:SaoMaAnZhuangFuWuXinXiCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.changeTextBlock = ^(ScanServerInfoModel * _Nonnull serverInfoModel)
        {
            weakSelf.serverInfoModel = serverInfoModel;
            [weakSelf.listTableView beginUpdates];
            [weakSelf.listTableView endUpdates];
        };
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ScanMakeOrderSectionFooterView *footerView = [ScanMakeOrderSectionFooterView sectionViewWithTableView:tableView];
    [footerView.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
}

#pragma mark - 提交扫码安装信息
- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
    [self requesrtData];
}

///判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = self.view.bounds;
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"订单已提交";
    //self.bgUPLable = UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.numberOfLines = 0;
    DowLable.text = @"可在我的订单服务单中\n查看该订单";
    //self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    //if (self.myblock) {
    //    self.myblock(@"");
    //}
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
