//
//  HBScanAddAccessTableViewCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HBScanAddAccessTableViewCell.h"
#import "CSmakeAccAddmodel.h"

@interface HBScanAddAccessTableViewCell ()
@property (nonatomic, strong) UILabel *topTitle;
@property (nonatomic, strong) UITextField *accessTypeTextField;
@property (nonatomic, strong) UITextField *accessNameTextField;
@property (nonatomic, strong) UIView *secondBottomLine;

@end

@implementation HBScanAddAccessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.adjustsImageWhenDisabled = NO;
    closeButton.adjustsImageWhenHighlighted = NO; 
    closeButton.backgroundColor = [UIColor clearColor];
    closeButton.userInteractionEnabled = YES;
    [closeButton setImage:imgname(@"Cicon_close") forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:closeButton];
    [closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView);
        make.left.equalTo(self.backView).mas_offset(kScaleNum(10));
        make.width.mas_equalTo(kScaleNum(44));
        make.height.mas_equalTo(kScaleNum(56));
    }];
    
    UIImage *addImage = [UIImage imageNamed:@"scanAddAccess"];
    self.leftImageView.image = addImage;
    self.leftImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addAccessAction)];
    [self.leftImageView addGestureRecognizer:tap];
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(13));
        make.centerX.equalTo(self.backView);
        make.width.mas_equalTo(addImage.size.width);
        make.height.mas_equalTo(addImage.size.height);
    }];
    
    UILabel *topTitle = [[UILabel alloc] init];
    topTitle.backgroundColor = [UIColor clearColor];
    topTitle.textAlignment = NSTextAlignmentCenter;
    topTitle.textColor = kMainTextColor66;
    topTitle.font = KFontPingFangSCMedium(16);
    topTitle.text = @"新增配件";
    topTitle.hidden = YES;
    [self.backView addSubview:topTitle];
    self.topTitle = topTitle;
    [topTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(13));
        make.centerX.equalTo(self.backView);
        make.height.mas_equalTo(self.leftImageView);
    }];
    
    self.titleLabel.textColor = kMainTextColor33;
    self.titleLabel.font = KFontPingFangSCLight(14);
    self.titleLabel.text = @"配件品类:";
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(closeButton.mas_bottom).mas_offset(kScaleNum(13));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(20));
        make.width.mas_equalTo(kScaleNum(65));
        make.height.mas_equalTo(kScaleNum(15));
    }];
    
    //输入框
    UITextField *searchTF = [[UITextField alloc] init];
    searchTF.backgroundColor = [UIColor clearColor];
    searchTF.layer.cornerRadius = 4;
    searchTF.layer.masksToBounds = YES;
    searchTF.borderStyle = UITextBorderStyleNone;
    searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    searchTF.placeholder = @"请扫码自动生成";
    searchTF.font = regular14Font;
    [self.backView addSubview:searchTF];
    self.accessTypeTextField = searchTF;
    [searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel.mas_right).mas_offset(kScaleNum(22));
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-15));
        make.top.equalTo(self.titleLabel).mas_offset(kScaleNum(0));
        make.height.mas_equalTo(self.titleLabel);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(kScaleNum(17));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(10));
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
        make.height.mas_equalTo(kScaleNum(1));
    }];
    
    self.contentLabel.textColor = kMainTextColor33;
    self.contentLabel.font = KFontPingFangSCLight(14);
    self.contentLabel.text = @"配件品类:";
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomLine.mas_bottom).mas_offset(kScaleNum(17));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(20));
        make.width.mas_equalTo(kScaleNum(65));
        make.height.mas_equalTo(kScaleNum(15));
        
    }];
    
    //输入框
    UITextField *accessNameTextField = [[UITextField alloc] init];
    accessNameTextField.backgroundColor = [UIColor clearColor];
    accessNameTextField.layer.cornerRadius = 4;
    accessNameTextField.layer.masksToBounds = YES;
    accessNameTextField.borderStyle = UITextBorderStyleNone;
    accessNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    accessNameTextField.placeholder = @"请扫码自动生成";
    accessNameTextField.font = regular14Font;
    [self.backView addSubview:accessNameTextField];
    self.accessNameTextField = accessNameTextField;
    [accessNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentLabel.mas_right).mas_offset(kScaleNum(22));
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-15));
        make.top.equalTo(self.contentLabel).mas_offset(kScaleNum(0));
        make.height.mas_equalTo(self.titleLabel);
    }];
    
    UIView *bottomLine = [[UIView alloc] init];
    bottomLine.backgroundColor = kMainLineColor;
    [self.backView addSubview:bottomLine];
    self.secondBottomLine = bottomLine;
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentLabel.mas_bottom).mas_offset(kScaleNum(17));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(10));
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-10));
        make.height.mas_equalTo(kScaleNum(1));
        make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-25));
    }];
}

- (void)loadContent {
    CSmakeAccAddmodel *model = self.data;
    self.accessNameTextField.text = model.parts_name;
    self.accessTypeTextField.text = model.type_name;
    BOOL isHouBaoAccessory = YES;
    if (isHouBaoAccessory) {
        
    } else {
        
    }
}

#pragma mark - 添加配件
- (void)addAccessAction {
    if (self.scanBlock) {
        self.scanBlock();
    }
}
- (void)closeButtonEvent:(UIButton *)sender {
    if (self.closeBlock) {
        self.closeBlock();
    }
}

@end
