//
//  HBScanAddAccessHeaderView.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/20.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HBScanAddAccessHeaderView.h"

@implementation HBScanAddAccessHeaderView

- (void)buildSubview {
    [super buildSubview];
    self.backView.backgroundColor = kMainBackGroundColor;
    
    self.titleLabel.font = KFontPingFangSCRegular(14);
    self.titleLabel.textColor = kColorWithHex(0xD84B4A);
    self.titleLabel.text = @"请您扫描新安装侯保配件的二维码";
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.backView);
        make.left.equalTo(self.backView).mas_offset(kScaleNum(16));
        make.height.mas_equalTo(kScaleNum(40));
    }];
}

- (void)loadContent {
    
}

@end
