//
//  HBScanAddAccessHeaderView.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/20.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftLabelRightButtonHeaderFooterView.h"

NS_ASSUME_NONNULL_BEGIN

@interface HBScanAddAccessHeaderView : BaseLeftLabelRightButtonHeaderFooterView

@end

NS_ASSUME_NONNULL_END
