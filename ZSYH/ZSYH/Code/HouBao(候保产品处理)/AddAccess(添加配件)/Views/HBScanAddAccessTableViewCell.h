//
//  HBScanAddAccessTableViewCell.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface HBScanAddAccessTableViewCell : BaseLeftImageLabelRightImageLabelCell
@property (nonatomic, copy) void(^closeBlock)(void);
@property (nonatomic, copy) void(^scanBlock)(void);
@end

NS_ASSUME_NONNULL_END
