//
//  HBAddAccessTableHeaderView.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HBAddAccessTableHeaderView.h"

@implementation HBAddAccessTableHeaderView

- (void)buildSubview {
    [super buildSubview];
    
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *topLine = [[UIView alloc] init];
    topLine.backgroundColor = kMainLineColor;
    [self addSubview:topLine];
    [topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kScaleNum(10));
        make.left.right.top.equalTo(self);
    }];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(kScaleNum(10));
        make.left.equalTo(self).mas_offset(kScaleNum(16));
        make.height.mas_equalTo(kScaleNum(55));
    }];
    
    UIImage *image = imgname(@"jinruimage");
    self.rightImageView.image = image;
    self.rightImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).mas_offset(kScaleNum(-21));
        make.centerY.equalTo(self.titleLabel);
        make.width.mas_equalTo(image.size.width);
        make.height.mas_equalTo(image.size.height);
    }];
    
    self.bottomLine.backgroundColor = kMainBackGroundColor;
    [self.bottomLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.height.mas_equalTo(kScaleNum(2));
    }];
    
    UIImage *addImage = imgname(@"addAccessory");
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    loginBtn.userInteractionEnabled = YES;
    [loginBtn setBackgroundImage:addImage forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:loginBtn];
    self.addAccessButton = loginBtn;
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomLine.mas_bottom).mas_offset(kScaleNum(4));
        make.centerX.equalTo(self);
        make.width.mas_equalTo(addImage.size.width / addImage.size.height * kScaleNum(48));
        make.height.mas_equalTo(kScaleNum(48));
    }];
}

- (void)loadContent {
    NSAttributedString *attString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"订单编号:%@", @"687912175913668608"] originalColor:kMainTextColor33 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"订单编号:" changeColor:kMainTextColor66 changeFont:KFontPingFangSCLight(16)];
    self.titleLabel.attributedText = attString;
}

- (void)loginBtnEvent:(UIButton *)sender {
    NSLog(@"点击添加");
}

@end
