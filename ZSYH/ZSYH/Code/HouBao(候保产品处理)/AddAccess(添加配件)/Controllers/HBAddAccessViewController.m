//
//  HBAddAccessViewController.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HBAddAccessViewController.h"
#import "HBAddAccessTableHeaderView.h"
#import "HBScanAddAccessTableViewCell.h"
#import "ScanMakeOrderSectionFooterView.h"
#import "HBScanAddAccessHeaderView.h"
#import "CSmakeAccAddmodel.h"
#import "HWScanViewController.h"        //扫一扫

@interface HBAddAccessViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableSet *saoYiSaoStrSet;

@end

@implementation HBAddAccessViewController

- (NSMutableSet *)saoYiSaoStrSet {
    if (!_saoYiSaoStrSet) {
        _saoYiSaoStrSet = [NSMutableSet setWithCapacity:1];
    }
    return _saoYiSaoStrSet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScaleNum(150), 44)];
    titleLabel.text = @"扫码安装";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = FontSize(16);
    self.navigationItem.titleView = titleLabel;
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, 0, 30, 44);
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(backEvent:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBBItem = [[UIBarButtonItem alloc] initWithCustomView:loginBtn];
    self.navigationItem.leftBarButtonItem = backBBItem;
    
    self.navBar.hidden = YES;
    self.navBar.backButton.hidden = NO;
    self.navBar.seperateLine.hidden = NO;
    self.navBar.titleLabel.text = @"扫码安装";
}

- (void)backEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buildSubviews {
    
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - KNavHeight);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor;
    self.listTableView.estimatedRowHeight = kScaleNum(215);
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionHeaderHeight = kScaleNum(44);
    self.listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionFooterHeight = kScaleNum(44);
    self.listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.listTableView.isShowBtn = NO;
    self.listTableView.showNoData = NO;
    self.listTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.listTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self.listTableView.mj_header beginRefreshing];
    }];
    //self.listTableView.customImg = [UIImage imageNamed:(@"page_icon01_")];
    //self.listTableView.customMsg = @"暂时还没有数据哟";
    
    HBAddAccessTableHeaderView *headerView = [[HBAddAccessTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, kScaleNum(122))];
    [headerView loadContent];
    [headerView.addAccessButton addTarget:self action:@selector(addAccessAction) forControlEvents:UIControlEventTouchUpInside];
    self.listTableView.tableHeaderView = headerView;
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KBottomSafeViewHeight)];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.listTableView.tableFooterView = tableFooterView;
}

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.dataSourceArray.count == 0) {
        return 0;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    HBScanAddAccessHeaderView *footerView = [HBScanAddAccessHeaderView sectionViewWithTableView:tableView];
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HBScanAddAccessTableViewCell *cell = [HBScanAddAccessTableViewCell cellViewWithTableView:tableView];
    CSmakeAccAddmodel *model = [self.dataSourceArray safeObjectAtIndex:indexPath.row];
    cell.data = model;
    [cell loadContent];
    kWeakSelf;
    cell.closeBlock = ^{
        [weakSelf.dataSourceArray removeObjectAtIndex:indexPath.row];
        [weakSelf.listTableView reloadData];
    };
    
    cell.scanBlock = ^{
        HWScanViewController *vc = [[HWScanViewController alloc]init];
        vc.myblock = ^(NSString *str) {
            if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                ShowToastWithText(@"此配件已扫描");
                return ;
            }
            [weakSelf.saoYiSaoStrSet addObject:str];
            [weakSelf requesrtWith:str atIndexPath:indexPath];
        };
        UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
        [self presentViewController:na animated:YES completion:nil];

    };
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ScanMakeOrderSectionFooterView *footerView = [ScanMakeOrderSectionFooterView sectionViewWithTableView:tableView];
    [footerView.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    return footerView;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
    
}

#pragma mark - 添加按钮
- (void)addAccessAction {
    [self.dataSourceArray addObject:[CSmakeAccAddmodel new]];
    [self.listTableView reloadData];
}

#pragma mark - 提交按钮
- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
    [LXAlertView initWithTitleAlertTitle:@"订单已提交" titleImage:imgname(@"successapy") Message:@"可在我的订单或候保产品处理页面中查看该订单" sureTitle:@"我知道了" sureClicked:^(LXAlertView *alertView) {
        NSLog(@"我知道了");
    }];
}

#pragma mark - 请求二维码中的配件信息
- (void)requesrtWith:(NSString *)strr atIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"扫描结果%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        if (![mudic isKindOfClass:[NSDictionary class]]) {
            ShowToastWithText(@"数据格式不正确");
            return ;
        }
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc] init];
        //新配件
        [mymodel setValuesForKeysWithDictionary:mudic];
        mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
        mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
        mymodel.ne_name =[mudic objectForKey:@"parts_name"];
        mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.ne_isSaoyisao = @"1";
        [self.dataSourceArray replaceObjectAtIndex:indexPath.row withObject:mymodel];
        [self.listTableView reloadData];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}

@end
