//
//  ZYHouBaoOrderDetailViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYHouBaoOrderDetailViewController : BaseViewController
@property (nonatomic, strong) CSmaintenanceListModel *mymodel;
@property (nonatomic, strong) CSmaintenanceListModel *maintenanceListModel;

@property (nonatomic, strong) NSString *orderId;

@property (nonatomic, strong) UILabel *fwlxTitLab;
@property (nonatomic, strong) UILabel *fwlxConLab;

@property (nonatomic, strong) UILabel *dqplTitLab;
@property (nonatomic, strong) UILabel *dqplConLab;

@property (nonatomic, strong) UILabel *gzyyTitLab;
@property (nonatomic, strong) UILabel *gzyyConLab;

@property (nonatomic, strong) UILabel *numTitLab;
@property (nonatomic, strong) UILabel *numConLab;

@property (nonatomic, strong) UILabel *bzxxTitLab;
@property (nonatomic, strong) UILabel *bzxxConLab;

@property (nonatomic, strong) UILabel *yyrqTitLab;
@property (nonatomic, strong) UILabel *yyrqConLab;

@property (nonatomic, strong) UILabel *yysjTitLab;
@property (nonatomic, strong) UILabel *yysjConLab;

@property (nonatomic, strong) UILabel *addrTitLab;
@property (nonatomic, strong) UILabel *addrConLab;

@property (nonatomic, strong) UILabel *orderNumT;
@property (nonatomic, strong) UILabel *orderNumC;

@end

NS_ASSUME_NONNULL_END
