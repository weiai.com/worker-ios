//
//  ZYHouBaoOrderDetailViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHouBaoOrderDetailViewController.h"
#import "ZYHouBaoAccessDetailModel.h"
#import "HouBaoAccessOrderAccessDetailVC.h"

@interface ZYHouBaoOrderDetailViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSDictionary *dataDic;

@end

@implementation ZYHouBaoOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配件详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self requeest];
   
}

-(void)requeest{
    
    _mymodel = [[CSmaintenanceListModel alloc]init];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:(NSUInteger)1];
    param[@"orderId"] = NOTNIL(self.orderId);
    kWeakSelf;
    
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        [weakSelf.mymodel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        if (strIsEmpty(weakSelf.mymodel.orderId)) {
            weakSelf.mymodel.orderId = weakSelf.mymodel.rep_order_id;
        }
        [self orderDetailUI];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)orderDetailUI {
    [self.view addSubview:self.scrollView];
    
    //上 背景Label
    UIView *topBgView = [[UIView alloc] init];
    topBgView.backgroundColor = [UIColor whiteColor];
    topBgView.frame = CGRectMake(10, kNaviHeight +10, KWIDTH-20, 72);
    [self.scrollView addSubview:topBgView];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: topBgView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(12,12)];
    //创建 layer
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = topBgView.bounds;
    //赋值
    maskLayer.path = maskPath.CGPath;
    topBgView.layer.mask = maskLayer;
    
    UIButton *pjxqBtn = [[UIButton alloc] init];
    pjxqBtn.backgroundColor = [UIColor whiteColor];
    pjxqBtn.frame = CGRectMake(topBgView.right-99, 39, 79, 26);
    pjxqBtn.layer.borderWidth = 1;
    pjxqBtn.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    pjxqBtn.layer.cornerRadius = 13;
    pjxqBtn.layer.masksToBounds = YES;
    [pjxqBtn setTitle:@"配件详情" forState:UIControlStateNormal];
    pjxqBtn.titleLabel.font = FontSize(14);
    [pjxqBtn setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:UIControlStateNormal];
    [pjxqBtn addTarget:self action:@selector(pjxqBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [topBgView addSubview:pjxqBtn];
    
    //下 背景Label
    UIView *downBgView = [[UIView alloc] init];
    downBgView.backgroundColor = [UIColor whiteColor];
    downBgView.frame = CGRectMake(10, topBgView.bottom +2, KWIDTH-20, 475);
    [self.scrollView addSubview:downBgView];
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect: downBgView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(12,12)];
    //创建 layer
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = downBgView.bounds;
    //赋值
    maskLayer1.path = maskPath1.CGPath;
    downBgView.layer.mask = maskLayer1;
    
    UILabel *fwlxTitLab = [[UILabel alloc] init];
    fwlxTitLab.frame = CGRectMake(16, 22, 70, 14);
    fwlxTitLab.text = @"服务类型";
    fwlxTitLab.font = FontSize(14);
    fwlxTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:fwlxTitLab];
    self.fwlxTitLab = fwlxTitLab;
    
    UILabel *fwlxConLab = [[UILabel alloc] init];
    fwlxConLab.frame = CGRectMake(fwlxTitLab.right+10, 22, KWIDTH-20-32-70-10, 14);
    fwlxConLab.font = FontSize(14);
    fwlxConLab.text = _mymodel.service_name;
    fwlxConLab.textColor = K333333;
    [downBgView addSubview:fwlxConLab];
    self.fwlxConLab = fwlxConLab;
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line.frame = CGRectMake(16, fwlxTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line];
    
    UILabel *dqplTitLab = [[UILabel alloc] init];
    dqplTitLab.frame = CGRectMake(16, line.bottom +8, 70, 14);
    dqplTitLab.text = @"电器品类";
    dqplTitLab.font = FontSize(14);
    dqplTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:dqplTitLab];
    self.fwlxTitLab = dqplTitLab;
    
    UILabel *dqplConLab = [[UILabel alloc] init];
    dqplConLab.frame = CGRectMake(fwlxTitLab.right+10, line.bottom +8, KWIDTH-20-32-70-10, 14);
    dqplConLab.font = FontSize(14);
    dqplConLab.text = _mymodel.fault_name;
    dqplConLab.textColor = K333333;
    [downBgView addSubview:dqplConLab];
    self.fwlxConLab = dqplConLab;
    
    UILabel *line1 = [[UILabel alloc] init];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line1.frame = CGRectMake(16, dqplTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line1];
    
    UILabel *gzyyTitLab = [[UILabel alloc] init];
    gzyyTitLab.frame = CGRectMake(16, line1.bottom +8, 70, 14);
    gzyyTitLab.text = @"故障原因";
    gzyyTitLab.font = FontSize(14);
    gzyyTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:gzyyTitLab];
    self.gzyyTitLab = gzyyTitLab;
    
    UILabel *gzyyConLab = [[UILabel alloc] init];
    gzyyConLab.frame = CGRectMake(fwlxTitLab.right+10, line1.bottom +8, KWIDTH-20-32-70-10, 14);
    gzyyConLab.font = FontSize(14);
    gzyyConLab.text = _mymodel.common_failures;
    gzyyConLab.textColor = K333333;
    [downBgView addSubview:gzyyConLab];
    self.gzyyConLab = gzyyConLab;
    
    UILabel *line2 = [[UILabel alloc] init];
    line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line2.frame = CGRectMake(16, gzyyTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line2];
    
    UILabel *numTitLab = [[UILabel alloc] init];
    numTitLab.frame = CGRectMake(16, line2.bottom +8, 70, 14);
    numTitLab.text = @"数       量";
    numTitLab.font = FontSize(14);
    numTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:numTitLab];
    self.numTitLab = numTitLab;
    
    UILabel *numConLab = [[UILabel alloc] init];
    numConLab.frame = CGRectMake(fwlxTitLab.right+10, line2.bottom +8, KWIDTH-20-32-70-10, 14);
    numConLab.font = FontSize(14);
    numConLab.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
    numConLab.textColor = K333333;
    [downBgView addSubview:numConLab];
    self.numConLab = numConLab;
    
    UILabel *line3 = [[UILabel alloc] init];
    line3.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line3.frame = CGRectMake(16, numTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line3];
    
    UILabel *bzxxTitLab = [[UILabel alloc] init];
    bzxxTitLab.frame = CGRectMake(16, line3.bottom +18, 70, 14);
    bzxxTitLab.text = @"备注信息";
    bzxxTitLab.font = FontSize(14);
    bzxxTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:bzxxTitLab];
    self.bzxxTitLab = bzxxTitLab;
    
    CGFloat heig = [NSString heightWithWidth:KWIDTH-16-70-10-10-20 font:14 text:_mymodel.fault_comment];
    UILabel *bzxxConLab = [[UILabel alloc]initWithFrame:CGRectMake(bzxxTitLab.right+10, line3.bottom+15, KWIDTH-20-16-70-10-16, heig)];
    bzxxConLab.font = FontSize(14);
    bzxxConLab.textColor = K999999;
    bzxxConLab.text = _mymodel.fault_comment;
    bzxxConLab.numberOfLines = 0;
    [downBgView addSubview:bzxxConLab];
    heig = 14;
    
    UILabel *line4 = [[UILabel alloc] init];
    line4.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line4.frame = CGRectMake(16, bzxxConLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line4];
    
    UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(16, line4.bottom+8, 30, 14)];
    userLab.font = FontSize(14);
    userLab.textColor = K999999;
    userLab.text = @"用户";
    [downBgView addSubview:userLab];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(67, line4.bottom+8, 60, 14)];
    name.font = FontSize(14);
    name.textColor = K666666;
    name.text = _mymodel.user_name;
    [downBgView addSubview:name];
    
    UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+10, line4.bottom+8, 75, 14)];
    phoneA.font = FontSize(14);
    phoneA.textColor = K999999;
    phoneA.text = @"用户电话";
    [downBgView addSubview:phoneA];
    
    UIButton *phoneB = [UIButton new];
    phoneB.frame = CGRectMake(phoneA.right+5, line4.bottom+3, 30, 30);
    [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
    [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
    [downBgView addSubview:phoneB];
    
    UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line4.bottom+8, KWIDTH-20-67-60-70-30-10-15, 14)];
    phone.font = FontSize(14);
    phone.textColor = K666666;
    phone.textAlignment = NSTextAlignmentLeft;
    phone.text = _mymodel.user_phone;
    [downBgView addSubview:phone];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
    phone.userInteractionEnabled = YES;
    [phone addGestureRecognizer:tap];
    
    UILabel *line5 = [[UILabel alloc] init];
    line5.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line5.frame = CGRectMake(16, userLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line5];
    
    UILabel *yyrqTitLab = [[UILabel alloc] init];
    yyrqTitLab.frame = CGRectMake(16, line5.bottom +8, 70, 14);
    yyrqTitLab.text = @"上门日期";
    yyrqTitLab.font = FontSize(14);
    yyrqTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:yyrqTitLab];
    self.yyrqTitLab = yyrqTitLab;
    
    UILabel *yyrqConLab = [[UILabel alloc] init];
    yyrqConLab.frame = CGRectMake(fwlxTitLab.right+10, line5.bottom +8, KWIDTH-20-32-70-10, 14);
    yyrqConLab.font = FontSize(14);
    yyrqConLab.text = _mymodel.order_date;
    yyrqConLab.textColor = K333333;
    [downBgView addSubview:yyrqConLab];
    self.yyrqConLab = yyrqConLab;
    
    UILabel *line6 = [[UILabel alloc] init];
    line6.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line6.frame = CGRectMake(16, yyrqTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line6];
    
    UILabel *yysjTitLab = [[UILabel alloc] init];
    yysjTitLab.frame = CGRectMake(16, line6.bottom +8, 70, 14);
    yysjTitLab.text = @"上门时间";
    yysjTitLab.font = FontSize(14);
    yysjTitLab.textColor = [UIColor colorWithHexString:@"#5F5F5F"];
    [downBgView addSubview:yysjTitLab];
    self.yysjTitLab = yysjTitLab;
    
    UILabel *yysjConLab = [[UILabel alloc] init];
    yysjConLab.frame = CGRectMake(fwlxTitLab.right+10, line6.bottom +8, KWIDTH-20-32-70-10, 14);
    yysjConLab.font = FontSize(14);
    yysjConLab.text = _mymodel.order_time;
    yysjConLab.textColor = K333333;
    [downBgView addSubview:yysjConLab];
    self.yysjConLab = yysjConLab;
    
    UILabel *line7 = [[UILabel alloc] init];
    line7.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    line7.frame = CGRectMake(16, yysjTitLab.bottom+10, KWIDTH-20-16, 1);
    [downBgView addSubview:line7];
    
    UILabel *addrTitLab = [[UILabel alloc]initWithFrame:CGRectMake(16, line7.bottom+8, 70, 14)];
    addrTitLab.font = FontSize(14);
    addrTitLab.textColor = K999999;
    addrTitLab.text = @"地址";
    [downBgView addSubview:addrTitLab];
    self.addrTitLab = addrTitLab;
    
    CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
    UILabel *addrConLab = [[UILabel alloc]initWithFrame:CGRectMake(addrTitLab.right+10, line7.bottom+8, KWIDTH-20-16-70-10-16 , heig2)];
    addrConLab.font = FontSize(14);
    addrConLab.textColor = K999999;
    addrConLab.text = _mymodel.user_address;
    addrConLab.numberOfLines = 0;
    [downBgView addSubview:addrConLab];
    self.addrConLab = addrConLab;
    
    UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(16, addrConLab.bottom+10, KWIDTH-16-20, 1)];
    line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [downBgView addSubview:line11];
    
    UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(16, line11.bottom+8, 30, 14)];
    dianti.font = FontSize(14);
    dianti.textColor = K999999;
    dianti.text = @"电梯";
    [downBgView addSubview:dianti];
    
    UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+8, 30, 14)];
    diantilb.font = FontSize(14);
    diantilb.textColor = K666666;
    diantilb.text = @"无";
    if ([_mymodel.has_elevator integerValue] == 1) {
        diantilb.text = @"有";
    }
    [downBgView addSubview:diantilb];
    
    UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line11.bottom+8, 30, 14)];
    louceng.font = FontSize(14);
    louceng.textColor = K999999;
    louceng.text = @"楼层";
    [downBgView addSubview:louceng];
    
    UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line11.bottom+8, 35, 14)];
    loucenglb.font = FontSize(14);
    loucenglb.textColor = K666666;
    loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
    [downBgView addSubview:loucenglb];
    
    UILabel *line12 = [[UILabel alloc]initWithFrame:CGRectMake(16, dianti.bottom+10, KWIDTH-16-20, 1)];
    line12.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [downBgView addSubview:line12];
    
    UILabel *orderNumT = [[UILabel alloc] init];
    orderNumT.frame = CGRectMake(16, line12.bottom +8, 70, 14);
    orderNumT.text = @"订单编号:";
    orderNumT.font = FontSize(14);
    orderNumT.textColor = [UIColor colorWithHexString:@"#979797"];
    [downBgView addSubview:orderNumT];
    self.orderNumT = orderNumT;
    
    UILabel *orderNumC = [[UILabel alloc] init];
    orderNumC.frame = CGRectMake(fwlxTitLab.right+10, line12.bottom +8, KWIDTH-20-32-70-10, 14);
    orderNumC.font = FontSize(14);
    orderNumC.text = _mymodel.Id;
    orderNumC.textColor = [UIColor colorWithHexString:@"#979797"];
    [downBgView addSubview:orderNumC];
    self.orderNumC = orderNumC;
}

- (void)pjxqBtnAction:(UIButton *)button {
    //HouBaoAccessOrderAccessDetailVC *vc = [[HouBaoAccessOrderAccessDetailVC alloc] init];
    //[self.navigationController popToViewController:vc animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"您点击了最上边的 配件详情 按钮");
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
