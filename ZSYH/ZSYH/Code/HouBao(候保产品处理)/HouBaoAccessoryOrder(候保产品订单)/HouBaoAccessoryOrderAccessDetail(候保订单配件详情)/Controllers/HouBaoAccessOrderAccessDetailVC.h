//
//  HouBaoAccessOrderAccessDetailVC.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "YJBaseTableViewController.h"
#import "CSmaintenanceListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HouBaoAccessOrderAccessDetailVC : BaseViewController
@property (nonatomic, strong) CSmaintenanceListModel *maintenanceListModel;

@end

NS_ASSUME_NONNULL_END
