//
//  HouBaoAccessOrderAccessDetailVC.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoAccessOrderAccessDetailVC.h"
#import "HBAccessOrderAccessDetailSectionHeader.h"  //订单编号
#import "HouBaoAccessoryDetailAccessoryInfoCell.h"  //配件cell
#import "ScanMakeOrderSectionFooterView.h"          //提交按钮
#import "ZYHouBaoAccessDetailModel.h"
#import "ZYHouBaoOrderDetailViewController.h"       //配件详情

@interface HouBaoAccessOrderAccessDetailVC ()<UITableViewDelegate, UITableViewDataSource, BaseTableViewHeaderFooterViewDelegate>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UILabel *orderConLab;

@end

@implementation HouBaoAccessOrderAccessDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配件详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [self requeset];
    [self buildSubviews];
    
}

- (void)buildSubviews {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 56)];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT -kNaviHeight -60);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[HouBaoAccessoryDetailAccessoryInfoCell class] forCellReuseIdentifier:@"HouBaoAccessoryDetailAccessoryInfoCell"];
    _mytableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.mytableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self.mytableView.mj_header beginRefreshing];
    }];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    _mytableView.tableFooterView = self.tabFootView;
    [self.view addSubview:_mytableView];
}

- (void)requeset {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    [param setObject:safe(self.maintenanceListModel.repid) forKey:@"orderId"];//
    // 订单状态(0生成订单（未处理）//添加候保配件
    // ，1已处理，//查看配件详情
    NSLog(@"请求链接%@", param);
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        
        self.mydateSource = [ZYHouBaoAccessDetailModel mj_objectArrayWithKeyValuesArray:dic[@"data"][@"pros"]];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
    } other:^(id dic) {
        //[weakSelf.dataSourceArray removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        //[weakSelf.dataSourceArray removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 229;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*HouBaoAccessoryDetailAccessoryInfoCell *cell = [HouBaoAccessoryDetailAccessoryInfoCell cellViewWithTableView:tableView];
     cell.data = self.maintenanceListModel;
     [cell loadContent];
     cell.accessoryDamageBlock = ^{
     if (indexPath.row % 2 == 0) {
     [LXAlertView initWithTitleAlertTitle:@"确认该候保配件已损坏？" titleImage:imgname(@"tipImage") Message:@"" sureTitle:@"确定" cancleTitle:@"取消" sureClicked:^(LXAlertView *alertView) {
     NSLog(@"确定");
     [LXAlertView initWithAlertTitleImage:imgname(@"wenXinTiShi") Message:@"已损坏的配件如在质保期内，请携带损坏配件到购买该配件的配件点，进行更换并申请补助。" sureTitle:@"知道了" sureClicked:^(LXAlertView *alertView) {
     NSLog(@"知道了");
     }];
     } cancleClicked:^(LXAlertView *alertView) {
     
     }];
     } else if (indexPath.row % 2 == 1) {
     NSLog(@"空的");
     }
     };*/
    
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    
    //通过唯一标识创建Cell实例
    HouBaoAccessoryDetailAccessoryInfoCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[HouBaoAccessoryDetailAccessoryInfoCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    
    //数据赋值
    ZYHouBaoAccessDetailModel *model = [self.mydateSource safeObjectAtIndex:indexPath.row];
    mycell.pjplConLab.text = model.name;
    mycell.pjmcConLab.text = model.product_name;
    mycell.yxqConLab.text = [NSString stringWithFormat:@"%@%@%@",model.productionDate, @" 至 ", model.termValidity];
    mycell.bzqConLab.text = [NSString stringWithFormat:@"%@%@",model.warranty,@"个月"];
    
    return mycell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    /*HBAccessOrderAccessDetailSectionHeader *headerView = [HBAccessOrderAccessDetailSectionHeader sectionViewWithTableView:tableView];
     headerView.data = self.maintenanceListModel;
     [headerView loadContent];
     headerView.delegate = self;*/
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    headerView.frame = CGRectMake(0, 0, KWIDTH, 75);
    
    UIButton *headerBtn = [[UIButton alloc] init];
    headerBtn.backgroundColor = [UIColor whiteColor];
    headerBtn.frame = CGRectMake(0, 10, KWIDTH, 55);
    [headerBtn addTarget:self action:@selector(headerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:headerBtn];
    
    UILabel *orderTitLab = [[UILabel alloc] init];
    orderTitLab.frame = CGRectMake(16, 12.5, 90, 30);
    orderTitLab.text = @"订单编号:";
    orderTitLab.font = FontSize(16);
    orderTitLab.textColor = [UIColor colorWithHexString:@"#666666"];
    [headerBtn addSubview:orderTitLab];
    
    UILabel *orderConLab = [[UILabel alloc] init];
    orderConLab.frame = CGRectMake(orderTitLab.right +10, 12.5, KWIDTH-32-90-10-30, 30);
    orderConLab.text = self.maintenanceListModel.repid;
    orderConLab.font = FontSize(16);
    orderConLab.textColor = [UIColor colorWithHexString:@"#666666"];
    [headerBtn addSubview:orderConLab];
    self.orderConLab = orderConLab;
    
    UIImageView *image = [[UIImageView alloc] init];
    [image setImage:imgname(@"Cjinru")];
    image.frame = CGRectMake(orderConLab.right +10, 15, 15, 25);
    [headerBtn addSubview:image];
    
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    //    ScanMakeOrderSectionFooterView *footerView = [ScanMakeOrderSectionFooterView sectionViewWithTableView:tableView];
    //    [footerView.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    //    return footerView;
    
    return [UIView new];
}

- (void)baseTableViewHeaderFooterView:(BaseTableViewHeaderFooterView *)baseTableViewHeaderFooterView event:(id)event {
    NSLog(@"查看详情");
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
}

//- (void)submitAction:(UIButton *)sender {
//    NSLog(@"点击提交");
//    [LXAlertView initWithTitleAlertTitle:@"订单已提交" titleImage:imgname(@"successapy") Message:@"可在我的订单或候保产品处理页面中查看该订单" sureTitle:@"我知道了" sureClicked:^(LXAlertView *alertView) {
//        NSLog(@"我知道了");
//    }];
//}

- (void)headerBtnAction:(UIButton *)button {
    
    ZYHouBaoOrderDetailViewController *detailVC = [[ZYHouBaoOrderDetailViewController alloc] init];
    //    CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc] init];
    //detailVC.orderID = model.repid;
    detailVC.orderId = self.maintenanceListModel.repid;
    detailVC.mymodel = self.maintenanceListModel;
    [self.navigationController pushViewController:detailVC animated:YES];
    NSLog(@"您点击了 区头的 按钮 %@", self.maintenanceListModel.repid);
}

@end
