//
//  HBAccessOrderAccessDetailSectionHeader.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HBAccessOrderAccessDetailSectionHeader.h"
#import "CSmaintenanceListModel.h"

@implementation HBAccessOrderAccessDetailSectionHeader

- (void)buildSubview {
    [super buildSubview];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.backView);
        make.left.equalTo(self.backView).mas_offset(kScaleNum(16));
        make.height.mas_equalTo(kScaleNum(55));
    }];
    
    UIImage *image = imgname(@"jinruimage");
    self.rightImageView.image = image;
    self.rightImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.rightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-21));
        make.centerY.equalTo(self.backView);
        make.width.mas_equalTo(image.size.width);
        make.height.mas_equalTo(image.size.height);
    }];
}

- (void)loadContent {
    CSmaintenanceListModel *model = self.data;
    NSAttributedString *attString = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"订单编号:%@", model.repid] originalColor:kMainTextColor33 originalFont:KFontPingFangSCLight(14) originalLineSpacing:0 changeString:@"订单编号:" changeColor:kMainTextColor66 changeFont:KFontPingFangSCLight(16)];
    self.titleLabel.attributedText = attString;
}

@end
