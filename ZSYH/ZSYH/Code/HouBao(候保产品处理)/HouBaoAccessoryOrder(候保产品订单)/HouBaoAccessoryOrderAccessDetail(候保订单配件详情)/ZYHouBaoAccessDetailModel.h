//
//  ZYHouBaoAccessDetailModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYHouBaoAccessDetailModel : NSObject

@property (nonatomic, strong) NSString *install;
@property (nonatomic, strong) NSString *count;
@property (nonatomic, strong) NSString *qrcode;
@property (nonatomic, strong) NSString *product_type_id;
@property (nonatomic, strong) NSString *product_name;

@property (nonatomic, strong) NSString *contractPrice;
@property (nonatomic, strong) NSString *isaddnew;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *order_id;
@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSString *warranty;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *termValidity;

@property (nonatomic, strong) NSString *soft;
@property (nonatomic, strong) NSString *productionDate;
@property (nonatomic, strong) NSString *type_name;
@property (nonatomic, strong) NSString *salePrice;
@property (nonatomic, strong) NSString *compensationPrice;

@property (nonatomic, strong) NSString *product_model;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *partsId;

/*"install" : "24",
 "count" : "1",
 "qrcode" : "68231634377245082421111111111555",
 "product_type_id" : "666437356936499200",
 "product_name" : "测试配件一号",
 
 "contractPrice" : 8,
 "isaddnew" : "0",
 "name" : "测试测试8",
 "order_id" : "696200133095723008",
 "type" : "2",
 
 "warranty" : "8",
 "state" : "1",
 "quantity" : 0,
 "id" : "696200973009293312",
 "termValidity" : "2021-08-23",
 
 "soft" : 2,
 "productionDate" : "2019-08-23",
 "type_name" : "电脑板",
 "salePrice" : 12,
 "compensationPrice" : 19,
 
 "product_model" : "1",
 "createDate" : "2019-09-25 14:07:00",
 "partsId" : "1231313213131"*/
@end

NS_ASSUME_NONNULL_END
