//
//  ZYHouBaoOrderListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseLeftImageLabelRightImageLabelCell.h"
#import "ScanOrderListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYHouBaoOrderListTableViewCell : BaseLeftImageLabelRightImageLabelCell
//@property (nonatomic, assign) ComeFromType comeFromType;
@property (nonatomic, copy) void(^accessoriesDetailBlock)(void);

@end

NS_ASSUME_NONNULL_END
