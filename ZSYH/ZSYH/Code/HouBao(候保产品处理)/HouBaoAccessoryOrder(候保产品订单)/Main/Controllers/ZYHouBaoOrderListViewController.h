//
//  ZYHouBaoOrderListViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YJBaseTableViewController.h"
typedef NS_OPTIONS(NSInteger, ComeFromType) {
    ComeFromTypeScanOrder = 0,
    ComeFromTypeHouBaoOrder = 1 << 0,
    ComeFromTypeOther = 1 << 1
};
NS_ASSUME_NONNULL_BEGIN

@interface ZYHouBaoOrderListViewController : YJBaseTableViewController
@property (nonatomic, assign) ComeFromType comeFromType;
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;

@end

NS_ASSUME_NONNULL_END
