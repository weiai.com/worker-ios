//
//  ZYHouBaoAddAccessViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYHouBaoAddAccessViewController : BaseViewController
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);
@property(nonatomic, strong) NSString *orderId;
@property(nonatomic, strong) NSString *repId;

@end

NS_ASSUME_NONNULL_END
