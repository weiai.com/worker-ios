//
//  HouBaoAccessoryOrderVC.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouBaoAccessoryOrderVC : BaseViewController
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;
@end

NS_ASSUME_NONNULL_END
