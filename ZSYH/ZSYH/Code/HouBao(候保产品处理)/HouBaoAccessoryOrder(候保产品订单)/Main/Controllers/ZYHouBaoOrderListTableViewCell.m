//
//  ZYHouBaoOrderListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHouBaoOrderListTableViewCell.h"
#import "CSmaintenanceListModel.h"

@interface ZYHouBaoOrderListTableViewCell ()
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *faultReasonLabel;
@property (nonatomic, strong) UIButton *accessoriesDetailButton;
@end

@implementation ZYHouBaoOrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backView.backgroundColor = [UIColor whiteColor];
    [self.backView shadowshadowOpacity:0.2 borderWidth:kScaleNum(1) borderColor:kMain_Color erRadius:kScaleNum(8) shadowColor:[[UIColor blackColor] colorWithAlphaComponent:0.2] shadowRadius:kScaleNum(3) shadowOffset:CGSizeMake(0, -3)];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(kScaleNum(10), kScaleNum(10), kScaleNum(0), kScaleNum(10)));
    }];
    
    self.titleLabel.text = @"订单编号:";
    self.titleLabel.font = KFontPingFangSCRegular(10);
    self.titleLabel.textColor = kMainTextColor99;
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backView).mas_offset(kScaleNum(16));
        make.left.equalTo(self.backView).mas_offset(kScaleNum(15));
    }];
    
    UILabel *statusLabel = [[UILabel alloc] init];
    statusLabel.backgroundColor = [UIColor clearColor];
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.textColor = kColorWithHex(0xF88B1F);
    statusLabel.font = KFontPingFangSCLight(11);
    statusLabel.text = @"已处理";
    [self.backView addSubview:statusLabel];
    self.statusLabel = statusLabel;
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel);
        make.right.equalTo(self.backView).mas_offset(kScaleNum(-15));
    }];
    
    UIImage *leftImage = imgname(@"组 3123");
    self.leftImageView.image = leftImage;
    self.leftImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.leftImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView).mas_offset(kScaleNum(20));
        make.top.equalTo(self.titleLabel.mas_bottom).mas_offset(kScaleNum(20));
        make.width.mas_equalTo(leftImage.size.width);
        make.height.mas_equalTo(leftImage.size.height);
    }];
    
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.textColor = kMainTextColor33;
    nameLabel.font = KFontPingFangSCMedium(14);
    nameLabel.text = @"姓名";
    [self.backView addSubview:nameLabel];
    self.nameLabel = nameLabel;
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftImageView);
        make.left.equalTo(self.leftImageView.mas_right).mas_offset(kScaleNum(12));
    }];
    
    UILabel *phoneLabel = [[UILabel alloc] init];
    phoneLabel.backgroundColor = [UIColor clearColor];
    phoneLabel.textAlignment = NSTextAlignmentRight;
    phoneLabel.textColor = kMainTextColor33;
    phoneLabel.font = KFontPingFangSCMedium(14);
    phoneLabel.text = @"18838143828";
    [self.backView addSubview:phoneLabel];
    self.phoneLabel = phoneLabel;
    [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.leftImageView);
        make.right.equalTo(statusLabel);
    }];
    
    UILabel *addressLabel = [[UILabel alloc] init];
    addressLabel.backgroundColor = [UIColor clearColor];
    addressLabel.textAlignment = NSTextAlignmentLeft;
    addressLabel.textColor = kMainTextColor33;
    addressLabel.font = KFontPingFangSCMedium(14);
    addressLabel.text = @"河南省华南城";
    addressLabel.numberOfLines = 0;
    [self.backView addSubview:addressLabel];
    self.addressLabel = addressLabel;
    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameLabel.mas_bottom).mas_offset(kScaleNum(8));
        make.left.equalTo(nameLabel);
        make.right.equalTo(phoneLabel);
    }];
    
    //故障原因
    UILabel *faultReasonLabel = [[UILabel alloc] init];
    faultReasonLabel.backgroundColor = [UIColor clearColor];
    faultReasonLabel.text = @"故障原因:";
    faultReasonLabel.textAlignment = NSTextAlignmentLeft;
    faultReasonLabel.textColor = kMainTextColor33;
    faultReasonLabel.font = KFontPingFangSCMedium(14);
    [self.backView addSubview:faultReasonLabel];
    self.faultReasonLabel = faultReasonLabel;
    [faultReasonLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addressLabel.mas_bottom).mas_offset(kScaleNum(8));
        make.left.equalTo(nameLabel);
        make.right.equalTo(phoneLabel);
        make.bottom.equalTo(self.backView).mas_offset(kScaleNum(-10 - 26 - 10));
    }];
    
    UIButton *accessoriesDetailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    accessoriesDetailButton.adjustsImageWhenDisabled = NO;
    accessoriesDetailButton.adjustsImageWhenHighlighted = NO; accessoriesDetailButton.layer.masksToBounds = YES;
    [accessoriesDetailButton rounded:kScaleNum(13) width:kScaleNum(1) color:kMain_Color];
    [accessoriesDetailButton setTitle:@"配件详情" forState:UIControlStateNormal];
    [accessoriesDetailButton setTitleColor:kMain_Color forState:UIControlStateNormal];
    accessoriesDetailButton.titleLabel.font = KFontPingFangSCRegular(12);
    [accessoriesDetailButton sizeToFit];
    [accessoriesDetailButton addTarget:self action:@selector(accessoriesDetailButtonEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.backView addSubview:accessoriesDetailButton];
    self.accessoriesDetailButton = accessoriesDetailButton;
    [accessoriesDetailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(faultReasonLabel.mas_bottom).mas_offset(kScaleNum(10));
        make.right.equalTo(phoneLabel);
        make.height.mas_equalTo(kScaleNum(26));
        make.width.mas_equalTo(accessoriesDetailButton.width + kScaleNum(30));
    }];
}

- (void)loadContent {
    CSmaintenanceListModel *model = self.data;
    self.titleLabel.text = [NSString stringWithFormat:@"%@%@",@"订单编号:",safe(model.Id)];
    self.nameLabel.text = safe(model.user_name);
    self.phoneLabel.text = safe(model.user_phone);
    self.addressLabel.text = safe(model.user_address);
    
    //    KMyLog(@"输出一下 state 的状态是 %f", model.delstate);
    
    //    if (self.comeFromType == ComeFromTypeScanOrder) {
    //    self.faultReasonLabel.attributedText = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"故障原因:%@", model.fault_common] originalColor:kColorWithHex(0xCA9F61) originalFont:KFontPingFangSCMedium(14) originalLineSpacing:0 changeString:@"故障原因:" changeColor:kMainTextColor33 changeFont:KFontPingFangSCMedium(14)];//字段不一样
    //    if (model.state.integerValue == 1) {//已处理
    //        self.statusLabel.text = @"已处理";
    //    } else {//已损坏
    //        self.statusLabel.text = @"已损坏";
    //    }
    //    } else if (self.comeFromType == ComeFromTypeHouBaoOrder) {
            self.faultReasonLabel.attributedText = [NSString getAttributedStringWithOriginalString:[NSString stringWithFormat:@"故障原因:%@", model.common_failures] originalColor:kColorWithHex(0xCA9F61) originalFont:KFontPingFangSCMedium(14) originalLineSpacing:0 changeString:@"故障原因:" changeColor:kMainTextColor33 changeFont:KFontPingFangSCMedium(14)];//字段不一样
            if (model.delstate == 0) {//未处理
                self.statusLabel.text = @"未处理";
                [self.accessoriesDetailButton setTitle:@"去处理" forState:UIControlStateNormal];
            } else {//已处理
                self.statusLabel.text = @"已处理";
                [self.accessoriesDetailButton setTitle:@"配件详情" forState:UIControlStateNormal];
            }
    //    }
    CGSize size = [self.accessoriesDetailButton sizeThatFits:CGSizeMake(CGFLOAT_MAX, kScaleNum(26))];
    [self.accessoriesDetailButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.faultReasonLabel.mas_bottom).mas_offset(kScaleNum(10));
        make.right.equalTo(self.phoneLabel);
        make.height.mas_equalTo(kScaleNum(26));
        make.width.mas_equalTo(size.width + kScaleNum(30));
    }];
}

- (void)accessoriesDetailButtonEvent:(UIButton *)sender {
    NSLog(@"点击配件详情");
    if (self.accessoriesDetailBlock) {
        self.accessoriesDetailBlock();
    }
}


@end
