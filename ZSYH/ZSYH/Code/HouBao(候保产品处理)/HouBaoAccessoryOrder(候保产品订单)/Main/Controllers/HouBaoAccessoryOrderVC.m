//
//  HouBaoAccessoryOrderVC.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/19.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoAccessoryOrderVC.h"
#import "SGPagingView.h"
#import "CWPageControl.h"
#import "CenterTouchTableView.h"

#import "CSOrderContentViewController.h"
//#import "ScanOrderListViewController.h"
#import "ZYHouBaoOrderListViewController.h"

@interface HouBaoAccessoryOrderVC ()< UIGestureRecognizerDelegate,SGPageTitleViewDelegate, SGPageContentScrollViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;
@property (nonatomic, strong) NSMutableArray *titArr;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, strong) NSMutableArray *banerArr;

@end

@implementation HouBaoAccessoryOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"候保订单";
    self.titArr = [NSMutableArray arrayWithCapacity:1];
    self.banerArr = [NSMutableArray arrayWithCapacity:1];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#E6E6E6"];
    self.imageArr = [NSMutableArray arrayWithCapacity:1];
    if (@available(iOS 11.0, *)) {
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, 0, 30, 44);
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(backEvent:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBBItem = [[UIBarButtonItem alloc] initWithCustomView:loginBtn];
    self.navigationItem.leftBarButtonItem = backBBItem;
    [self showNav];
}

- (void)backEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showNav{
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.indicatorAdditionalWidth = 10; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
    configure.titleGradientEffect = YES;
    
    configure.titleSelectedColor= [UIColor colorWithHexString:@"#70BE68"];
    configure.titleColor= [UIColor colorWithHexString:@"#999999"];
    configure.showIndicator = YES;
    configure.indicatorColor = zhutiColor;
    configure.bottomSeparatorColor= [UIColor whiteColor];
    
    NSMutableArray *titleArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *childVCS = [NSMutableArray arrayWithCapacity:1];
    [titleArr addObject:@"全部"];
    [titleArr addObject:@"未处理"];
    [titleArr addObject:@"已处理"];
    
    for (int i = 0; i<titleArr.count; i++) {
        ZYHouBaoOrderListViewController *oneVC = [[ZYHouBaoOrderListViewController alloc] init];
        oneVC.from = self.from;
        oneVC.comeFromType = ComeFromTypeHouBaoOrder;
        oneVC.type = [NSString stringWithFormat:@"%d",i];
        [childVCS addObject:oneVC];
    }
    
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 44) delegate:self titleNames:titleArr configure:configure];
  
    self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, kNaviHeight+44, KWIDTH, KHEIGHT-kNaviHeight-44) parentVC:self childVCs:childVCS];
    
    _pageContentScrollView.delegatePageContentScrollView = self;
    [self.view addSubview:self.pageTitleView ];
    
    [self.view addSubview:_pageContentScrollView];
    
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 1 || index == 3) {
        [_pageTitleView removeBadgeForIndex:index];
    }
}

@end
