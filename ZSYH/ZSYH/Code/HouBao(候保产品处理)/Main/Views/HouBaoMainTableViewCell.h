//
//  HouBaoMainTableViewCell.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/17.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "BaseLeftImageLabelRightImageLabelCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface HouBaoMainTableViewCell : BaseLeftImageLabelRightImageLabelCell
@property (nonatomic, copy) void(^buttonBlock)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
