//
//  HouBaoMainTableViewCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/17.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoMainTableViewCell.h"

@implementation HouBaoMainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    
    NSArray *imageNameArray = @[@"scanOrder", @"houBaoOrder", @"houBaoDingHuo", @"houBaoScan_small"];
    CGFloat space = kScaleNum(7);
    CGFloat topMarge = kScaleNum(46);
    CGFloat leftMarge = kScaleNum(26);
    CGFloat imageViewWidth = (KScreenWidth - leftMarge * 2 - space) / 2;
    CGSize imageSize = [UIImage imageNamed:@"scanOrder"].size;
    CGFloat imageViewHeight = imageSize.height / imageSize.width * imageViewWidth;
    for (int i = 0; i < imageNameArray.count; i++) {
        UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        loginBtn.adjustsImageWhenDisabled = NO;
        loginBtn.adjustsImageWhenHighlighted = NO;
        loginBtn.tag = 200 + i;
        [loginBtn addTarget:self action:@selector(loginBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        [loginBtn setBackgroundImage:[UIImage imageNamed:imageNameArray[i]] forState:UIControlStateNormal];
        [self.backView addSubview:loginBtn];
        [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.backView).mas_offset(topMarge + (kScaleNum(11) + imageViewHeight) * (i / 2));
            make.left.equalTo(self.backView).mas_offset(leftMarge + (space + imageViewWidth) * (i % 2));
            make.width.mas_equalTo(imageViewWidth);
            make.height.mas_equalTo(imageViewHeight);
            if (i == imageNameArray.count - 1) {
                make.bottom.equalTo(self.backView).mas_offset(-topMarge);
            }
        }];
    }
}

- (void)loadContent {
    
}

- (void)loginBtnEvent:(UIButton *)sender {
    NSLog(@"%ld", sender.tag - 200);
    NSInteger tag = sender.tag - 200;
    if (self.buttonBlock) {
        self.buttonBlock(tag);
    }
}

@end
