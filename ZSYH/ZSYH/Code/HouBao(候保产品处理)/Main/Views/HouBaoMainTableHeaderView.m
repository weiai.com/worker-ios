//
//  HouBaoMainTableHeaderView.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/17.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoMainTableHeaderView.h"

@implementation HouBaoMainTableHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)buildSubview {
    
    //全部背景
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = [UIColor whiteColor];
    [self addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(0);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).mas_offset(0);
    }];
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn addTarget:self action:@selector(loginBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"houBaoScan_big"] forState:UIControlStateNormal];
    [backView addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(kScaleNum(20));
        make.left.equalTo(backView).mas_offset(kScaleNum(27));
        make.right.equalTo(backView).mas_offset(kScaleNum(-27));
        make.bottom.equalTo(self).mas_offset(kScaleNum(-20));
    }];
}

- (void)loginBtnEvent:(UIButton *)sender {
    NSLog(@"%ld", sender.tag - 200);
    if (self.scanActionBlock) {
        self.scanActionBlock();
    }
}

@end
