//
//  ZYBindExistingOrderViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYBindExistingOrderViewController : BaseViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;
@property (nonatomic, strong) NSString *qrcodeStr;

@end

NS_ASSUME_NONNULL_END
