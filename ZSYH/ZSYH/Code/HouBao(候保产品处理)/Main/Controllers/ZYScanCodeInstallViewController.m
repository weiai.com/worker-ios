//
//  ZYScanCodeInstallViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//
//扫码安装页面
#import "ZYScanCodeInstallViewController.h"
#import "HWScanViewController.h"        //扫一扫
#import "CSmakeAccAddmodel.h"
#import "ScanMakeOrderViewController.h"
#import "ScanMakeOrderAccessoriesInfoCell.h"
#import "ScanMakeOrderSectionFooterView.h"
#import "ScanServerInfoModel.h"
#import "ZYBindExistingOrderViewController.h" //绑定已有订单

@interface ZYScanCodeInstallViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) ScanServerInfoModel *serverInfoModel;
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIImageView *topImgView;
@property (nonatomic, strong) UIImageView *botImgView;

@end

@implementation ZYScanCodeInstallViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"扫码安装";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor whiteColor];

    KMyLog(@"有没有数据传过来 %@", self.accessAddModel.qrcode);

    [self buildSubviews];
 
}
- (void)buildSubviews {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 56)];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight-123);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ScanMakeOrderAccessoriesInfoCell class] forCellReuseIdentifier:@"ScanMakeOrderAccessoriesInfoCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    _mytableView.tableFooterView = self.tabFootView;
    _mytableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_mytableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self->_mytableView.mj_header beginRefreshing];
    }];
    [self.view addSubview:_mytableView];
    
    //底部 背景View
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor clearColor];
    bottomView.frame = CGRectMake(0, self.mytableView.bottom, KWIDTH, 123);
    [self.view addSubview:bottomView];
    
    //扫码安装 按钮
    UIButton *newOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    newOrderBtn.frame = CGRectMake(16, 10, (KWIDTH-32-25)/2, 48);
    [newOrderBtn setTitle:@"扫码安装" forState:UIControlStateNormal];
    newOrderBtn.titleLabel.font = FontSize(16);
    newOrderBtn.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    newOrderBtn.layer.cornerRadius = 4;
    newOrderBtn.layer.masksToBounds = YES;
    [newOrderBtn addTarget:self action:@selector(newOrderBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:newOrderBtn];
    
    //绑定已有账单 按钮
    UIButton *buildOrderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    buildOrderBtn.frame = CGRectMake(newOrderBtn.right +25, 10, (KWIDTH-32-25)/2, 48);
    [buildOrderBtn setTitle:@"绑定已有订单" forState:UIControlStateNormal];
    buildOrderBtn.titleLabel.font = FontSize(16);
    buildOrderBtn.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    buildOrderBtn.layer.cornerRadius = 4;
    buildOrderBtn.layer.masksToBounds = YES;
    [buildOrderBtn addTarget:self action:@selector(buildOrderBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:buildOrderBtn];
}

//扫码安装 按钮 点击事件处理
- (void)newOrderBtnAction:(UIButton *)button {
    //查询是否已经安装，安装过就提示，不再跳转
    [self requesrtData];
}

//绑定已有订单 按钮 点击事件处理
- (void)buildOrderBtnAction:(UIButton *)button {
    
    //查询此二维码是否已经绑定过，绑定过，就不跳转
    [self requestBangDingState];
    
}
-(void)requestBangDingState
{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"qrcode"] = NOTNIL(self.accessAddModel.qrcode);//二维码id
    [NetWorkTool POST:cs_orderCheckPartsAz param:param success:^(id dic) {
        
      
        ZYBindExistingOrderViewController *vc = [[ZYBindExistingOrderViewController alloc] init];
        vc.accessAddModel = self.accessAddModel;
        vc.qrcodeStr = self.accessAddModel.qrcode;
        [self.navigationController pushViewController:vc animated:YES];
        KMyLog(@"调取成功 %@", dic);
        
    } other:^(id dic) {
        NSString *tip = dic[@"msg"];
        ShowToastWithText(tip);
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
#pragma mark - 请求信息
- (void)requesrtData {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"qrcode"] = NOTNIL(self.accessAddModel.qrcode);//二维码id
    [NetWorkTool POST:cs_orderCheckPartsAz param:param success:^(id dic) {
        
        
        //扫码安装 新配件
        ScanMakeOrderViewController *scanMakeOrderVC = [[ScanMakeOrderViewController alloc] init];
        scanMakeOrderVC.accessAddModel = self.accessAddModel;
        [self.navigationController pushViewController:scanMakeOrderVC animated:YES];
        
    } other:^(id dic) {
        NSString *tip = dic[@"msg"];
        ShowToastWithText(tip);
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
   
}
#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ScanMakeOrderAccessoriesInfoCell *cell = [ScanMakeOrderAccessoriesInfoCell cellViewWithTableView:tableView];
    cell.data = self.accessAddModel;
    [cell loadContent];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
}

#pragma mark - 提交扫码安装信息
- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
}

///判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}
@end
