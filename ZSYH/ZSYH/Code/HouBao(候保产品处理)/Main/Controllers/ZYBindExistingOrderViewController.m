//
//  ZYBindExistingOrderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//
//绑定已有订单
#import "ZYBindExistingOrderViewController.h"
#import "ZYBindExistingOrderTableViewCell.h"
#import "ZYOrderDetailsViewController.h"

@interface ZYBindExistingOrderViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) NSString *leftTimeStr;
@property (nonatomic, strong) NSString *rightTimeStr;
@property (nonatomic, strong) UILabel *leftDateLab;
@property (nonatomic, strong) UILabel *rightDateLab;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) NSString *orderId;

@end

@implementation ZYBindExistingOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"绑定已有订单";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#E6E6E6"];

    self.mydateSource = [NSMutableArray arrayWithCapacity:1];

    [self showdetaile];
    [self getRepOrdersListData];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view.
}

//数据请求 处理
- (void)getRepOrdersListData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if ([self.leftDateLab.text isEqual: @"请选择开始日期"] || [self.rightDateLab.text isEqual: @"请选择结束日期"]) {
        param[@"begTime"] = @""; //开始日期
        param[@"endTime"] = @""; //结束日期
    } else {
        param[@"begTime"] = self.leftTimeStr; //开始日期
        param[@"endTime"] = self.rightTimeStr; //结束日期
    }
    
    [_mydateSource removeAllObjects];
    [self.mytableView reloadData];
    
    [NetWorkTool POST:getRepOrdersList param:param success:^(id dic) {
        
        self.mydateSource = [CSmaintenanceListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.mytableView reloadData];
        [self.mytableView.mj_header endRefreshing];
        
    } needUser:YES];
}

-(void)showdetaile {
    
    [self.view addSubview:self.scrollView];
    //顶部 背景View
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    topView.frame = CGRectMake(0, kNaviHeight, KWIDTH, 44);
    [self.scrollView addSubview:topView];
    
    //选择时间 Label
    UILabel *leftDateLab = [[UILabel alloc] init];
    leftDateLab.frame = CGRectMake(32, 10, (KWIDTH-32*2-20-15-60)/2, 24);
    leftDateLab.text = @"请选择开始日期";
    leftDateLab.font = FontSize(13);
    leftDateLab.textColor = K666666;
    leftDateLab.textAlignment = NSTextAlignmentCenter;
    [topView addSubview:leftDateLab];
    self.leftDateLab = leftDateLab;
    //左侧 选择时间 Label 添加点击事件
    leftDateLab.userInteractionEnabled = YES;
    UITapGestureRecognizer *leftLabelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(leftSelectDate:)];
    [leftDateLab addGestureRecognizer:leftLabelTapGestureRecognizer];
    //下划线
    UILabel *leftLine = [[UILabel alloc] init];
    leftLine.backgroundColor = [UIColor colorWithHexString:@"#CCCCCC"];
    leftLine.frame = CGRectMake(32, leftDateLab.bottom+3, (KWIDTH-32*2-20-15-60)/2, 1);
    [topView addSubview:leftLine];
    
    //至
    UILabel *zhiLab = [[UILabel alloc] init];
    zhiLab.frame = CGRectMake(leftDateLab.right +10, 10, 15, 24);
    zhiLab.text = @"至";
    zhiLab.font = FontSize(14);
    zhiLab.textColor = K333333;
    [topView addSubview:zhiLab];
    
    //选择时间 Label
    UILabel *rightDateLab = [[UILabel alloc] init];
    rightDateLab.frame = CGRectMake(zhiLab.right +10, 10, (KWIDTH-32*2-20-15-60)/2, 24);
    rightDateLab.text = @"请选择结束日期";
    rightDateLab.font = FontSize(13);
    rightDateLab.textAlignment = NSTextAlignmentCenter;
    rightDateLab.textColor = K666666;
    [topView addSubview:rightDateLab];
    self.rightDateLab = rightDateLab;
    //左侧 选择时间 Label 添加点击事件
    rightDateLab.userInteractionEnabled = YES;
    UITapGestureRecognizer *rightLabelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightSelectDate:)];
    [rightDateLab addGestureRecognizer:rightLabelTapGestureRecognizer];
    //下划线
    UILabel *rightLine = [[UILabel alloc] init];
    rightLine.backgroundColor = [UIColor colorWithHexString:@"#CCCCCC"];
    rightLine.frame = CGRectMake(zhiLab.right +10, leftDateLab.bottom+3, (KWIDTH-32*2-20-15-60)/2, 1);
    [topView addSubview:rightLine];
    
    //完成 按钮
    UIButton *okBtn = [[UIButton alloc] init];
    okBtn.frame = CGRectMake(rightDateLab.right +10, 10, 60, 24);
    [okBtn setTitle:@"完成" forState:UIControlStateNormal];
    okBtn.titleLabel.font = FontSize(16);
    [okBtn setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(okBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:okBtn];
    
    //区尾 View
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 0)];
    //列表 TableView
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, kNaviHeight +54, KWIDTH, KHEIGHT -kNaviHeight-54);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYBindExistingOrderTableViewCell class] forCellReuseIdentifier:@"ZYBindExistingOrderTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    _mytableView.tableFooterView = self.tabFootView;
    [_scrollView addSubview:_mytableView];
}

-(void)leftSelectDate:(UITapGestureRecognizer *)recognizer{
    
    //新建日期选择器
    MHDatePicker *datePicker = [[MHDatePicker alloc] init];
    datePicker.isBeforeTime = YES;
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [datePicker didFinishSelectedDate:^(NSDate *selectedDate) {
        NSString *selectedDateStr = [formatter stringFromDate:selectedDate];
        self.leftTimeStr = selectedDateStr;
        NSArray  *myarray = [selectedDateStr componentsSeparatedByString:@"-"];
        self.leftDateLab.text =[NSString stringWithFormat:@"%@年%@月%@日",myarray[0],myarray[1],myarray[2]];
        self.leftDateLab.textColor = K333333;
        
    }];
    //KMyLog(@"选择日期");
    //[[UIApplication sharedApplication].keyWindow addSubview:self.bgViewRiqi];
}

-(void)rightSelectDate:(UITapGestureRecognizer *)recognizer{
    
    //新建日期选择器
    MHDatePicker *datePicker = [[MHDatePicker alloc] init];
    datePicker.isBeforeTime = YES;
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [datePicker didFinishSelectedDate:^(NSDate *selectedDate) {
        NSString *selectedDateStr = [formatter stringFromDate:selectedDate];
        self.rightTimeStr = selectedDateStr;
        NSArray  *myarray = [selectedDateStr componentsSeparatedByString:@"-"];
        self.rightDateLab.text = [NSString stringWithFormat:@"%@年%@月%@日",myarray[0],myarray[1],myarray[2]];
        self.rightDateLab.textColor = K333333;
    }];
    //KMyLog(@"选择日期");
    //[[UIApplication sharedApplication].keyWindow addSubview:self.bgViewRiqi];
}

//完成按钮 点击事件
- (void)okBtnAction:(UIButton *)button {
    
    if ([self.leftDateLab.text isEqualToString:@"请选择开始日期"]  ) {
        ShowToastWithText(@"请选择开始日期");
        return;
    }else if( [self.rightDateLab.text isEqualToString:@"请选择结束日期"]){
        ShowToastWithText(@"请选择结束日期");
        return;
    }
    
    [self getRepOrdersListData];
    [self.mytableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 177;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",(long)indexPath.section, (long)indexPath.row];
    
    //通过唯一标识创建Cell实例
    ZYBindExistingOrderTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYBindExistingOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    mycell.backgroundColor = [UIColor clearColor];
    
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [mycell refasf:model];
    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 0: {
                //确定绑定此订单?
                [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];
                KMyLog(@"确定绑定此订单?");
                self.orderId = model.orderId; //订单编号
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec {
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(239);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-22*2)/2-30, 34, 60, 60)];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"tipImage"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 19, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"确定绑定此订单?";
    
    UIButton *cancleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancleBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancleBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [cancleBut setBackgroundColor:[UIColor whiteColor]];
    cancleBut.layer.masksToBounds = YES;
    cancleBut.layer.cornerRadius = 4;
    cancleBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    cancleBut.layer.borderWidth = 1;
    [cancleBut addTarget:self action:@selector(cancleButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:cancleBut];
    [cancleBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(45);
        make.width.offset((KWIDTH-22*2-45*2-29)/2);
        make.bottom.offset(-22);
    }];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 4;
    [yesBut addTarget:self action:@selector(yesBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.right.offset(-45);
        make.width.offset((KWIDTH-22*2-45*2-29)/2);
        make.bottom.offset(-22);
    }];
}

- (void)cancleButAction:(UIButton *)button {
    [_bgViewsec removeFromSuperview];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)yesBtnAction:(UIButton *)button {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(self.orderId); //订单编号
    param[@"qrcode"] = NOTNIL(self.qrcodeStr); //二维码id
    
    KMyLog(@"传过去的参数 是 %@", param);
    
    [NetWorkTool POST:bindingOrder param:param success:^(id dic) {
        
        [self.bgViewsec removeFromSuperview];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        KMyLog(@"调取成功 %@", dic);

    } other:^(id dic) {
        NSString *tip = dic[@"msg"];
        ShowToastWithText(tip);
        [self.bgViewsec removeFromSuperview];

    } fail:^(NSError *error) {
        [self.bgViewsec removeFromSuperview];

    } needUser:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}
@end
