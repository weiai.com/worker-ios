//
//  ZYBindExistingOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBindExistingOrderTableViewCell.h"

@implementation ZYBindExistingOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 169);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        bottomV.layer.borderWidth = 1;
        bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        //最上面 日期时间
        UILabel *dateLab = [[UILabel alloc] init];
        dateLab.frame = CGRectMake(15, 14, KWIDTH/2, 17);
        dateLab.font = FontSize(12);
        dateLab.textColor = K999999;
        [bottomV addSubview:dateLab];
        self.dateLab = dateLab;
        //用户类型 图片
        UIImageView *leftImg = [[UIImageView alloc] init];
        leftImg.frame = CGRectMake(12, dateLab.bottom+22, 48, 48);
        [leftImg setImage:imgname(@"jiancedanleftimg")];
        [bottomV addSubview:leftImg];
        self.leftImg = leftImg;
        //检测单 提示文字
        UILabel *tipsTitLab = [[UILabel alloc] init];
        tipsTitLab.frame = CGRectMake(6, leftImg.bottom +8, 60, 17);
        tipsTitLab.text = @"检测单";
        tipsTitLab.textAlignment = NSTextAlignmentCenter;
        tipsTitLab.font = FontSize(12);
        tipsTitLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:tipsTitLab];
        self.tipsTitLab = tipsTitLab;
        //服务类型 标题
        UILabel *fwlxTitLab = [[UILabel alloc] init];
        fwlxTitLab.frame = CGRectMake(leftImg.right+12, dateLab.bottom +7, 70, 20);
        fwlxTitLab.text = @"服务类型:";
        fwlxTitLab.font = FontSize(14);
        fwlxTitLab.textColor = K666666;
        [bottomV addSubview:fwlxTitLab];
        self.fwlxTitLab = fwlxTitLab;
        //服务类型 b内容
        UILabel *fwlxConLab = [[UILabel alloc] init];
        fwlxConLab.frame = CGRectMake(fwlxTitLab.right, dateLab.bottom +7, KWIDTH-20-12-48-12-70-12, 20);
        fwlxConLab.font = FontSize(14);
        fwlxConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:fwlxConLab];
        self.fwlxConLab = fwlxConLab;
        //电器品类 标题
        UILabel *dqplTitLab = [[UILabel alloc] init];
        dqplTitLab.frame = CGRectMake(leftImg.right+12, fwlxTitLab.bottom +2, 70, 20);
        dqplTitLab.text = @"电器品类:";
        dqplTitLab.font = FontSize(14);
        dqplTitLab.textColor = K666666;
        [bottomV addSubview:dqplTitLab];
        self.dqplTitLab = dqplTitLab;
        //电器品类 内容
        UILabel *dqplConLab = [[UILabel alloc] init];
        dqplConLab.frame = CGRectMake(fwlxTitLab.right, fwlxTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        dqplConLab.font = FontSize(14);
        dqplConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:dqplConLab];
        self.dqplConLab = dqplConLab;
        //预约时间 标题
        UILabel *yysjTitLab = [[UILabel alloc] init];
        yysjTitLab.frame = CGRectMake(leftImg.right+12, dqplTitLab.bottom +2, 70, 20);
        yysjTitLab.text = @"预约时间:";
        yysjTitLab.font = FontSize(14);
        yysjTitLab.textColor = K666666;
        [bottomV addSubview:yysjTitLab];
        self.yysjTitLab = yysjTitLab;
        //预约时间 内容
        UILabel *yysjConLab = [[UILabel alloc] init];
        yysjConLab.frame = CGRectMake(yysjTitLab.right, dqplTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        yysjConLab.font = FontSize(14);
        yysjConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:yysjConLab];
        self.yysjConLab = yysjConLab;
        //服务地址 标题
        UILabel *fwdzTitLab = [[UILabel alloc] init];
        fwdzTitLab.frame = CGRectMake(leftImg.right+12, yysjTitLab.bottom +2, 70, 20);
        fwdzTitLab.text = @"服务地址:";
        fwdzTitLab.font = FontSize(14);
        fwdzTitLab.textColor = K666666;
        [bottomV addSubview:fwdzTitLab];
        self.fwdzTitLab = fwdzTitLab;
        //服务地址 内容
        UILabel *fwdzConLab = [[UILabel alloc] init];
        fwdzConLab.frame = CGRectMake(fwlxTitLab.right, yysjTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        fwdzConLab.font = FontSize(14);
        fwdzConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:fwdzConLab];
        self.fwdzConLab = fwdzConLab;
        
        UIButton *scjcbgBtn = [[UIButton alloc] init];
        scjcbgBtn.frame = CGRectMake(bottomV.right - 136, fwdzTitLab.bottom +10, 112, 28);
        [scjcbgBtn setImage:[UIImage imageNamed:@"bangdingcidingdan"] forState:UIControlStateNormal];
        [scjcbgBtn addTarget:self action:@selector(shengchengjiancebaogao:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:scjcbgBtn];
        self.scjcbgBtn = scjcbgBtn;
    }
    return self;
}

- (void)refasf:(CSmaintenanceListModel *)model{
    
    self.dateLab.text = model.create_time;        //日期时间
    self.fwlxConLab.text = model.service_name;    //服务类型
    self.dqplConLab.text = model.fault_name;      //电器品类
    
    //预约时间
    if (model.order_time == NULL && model.order_date == NULL) {
        self.yysjConLab.hidden = YES;
    } else {
        NSInteger indd = [[model.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        NSString *timeStr = [NSString stringWithFormat:@"%@ %@", ap, model.order_time];
        self.yysjConLab.text = [NSString stringWithFormat:@"%@%@", model.order_date, timeStr];
    }
    
    self.fwdzConLab.text = model.user_address;    //地址

    if ([model.user_type isEqualToString:@"0"]) { //0:个人用户
        [self.leftImg setImage:imgname(@"gerenyonghu")];
        self.tipsTitLab.text = @"个人用户";
    } else { //2:电器厂商
        [self.leftImg setImage:imgname(@"dianqichangjia")];
        self.tipsTitLab.text = @"电器厂家";
    }
}

//绑定此订单 按钮 点击事件
- (void)shengchengjiancebaogao:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//确定绑定此订单?
    }
}

@end
