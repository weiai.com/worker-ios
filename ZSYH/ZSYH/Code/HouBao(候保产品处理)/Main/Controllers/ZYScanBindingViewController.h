//
//  ZYScanBindingViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "YJBaseTableViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYScanBindingViewController : YJBaseTableViewController
@property (nonatomic, strong) CSmakeAccAddmodel *accessAddModel;

@end

NS_ASSUME_NONNULL_END
