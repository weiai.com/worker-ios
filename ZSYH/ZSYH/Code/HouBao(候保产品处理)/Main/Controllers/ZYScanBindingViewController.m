//
//  ZYScanBindingViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYScanBindingViewController.h"
#import "ScanMakeOrderAccessoriesInfoCell.h"
#import "ScanMakeOrderSectionFooterView.h"
#import "ScanServerInfoModel.h"
@interface ZYScanBindingViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) ScanServerInfoModel *serverInfoModel;

@end

@implementation ZYScanBindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScaleNum(150), 44)];
    titleLabel.text = @"扫码安装";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = FontSize(16);
    self.navigationItem.titleView = titleLabel;
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(0, 0, 30, 44);
    loginBtn.adjustsImageWhenDisabled = NO;
    loginBtn.adjustsImageWhenHighlighted = NO;
    [loginBtn setImage:imgname(@"left_icon") forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(backEvent:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBBItem = [[UIBarButtonItem alloc] initWithCustomView:loginBtn];
    self.navigationItem.leftBarButtonItem = backBBItem;
    self.navigationItem.leftBarButtonItem.title = @"";
}

- (void)backEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buildSubviews {
    
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT - KNavHeight);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor;
    self.listTableView.estimatedRowHeight = kScaleNum(215);
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionHeaderHeight = kScaleNum(44);
    self.listTableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedSectionFooterHeight = kScaleNum(44);
    self.listTableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.listTableView.isShowBtn = NO;
    self.listTableView.showNoData = YES;
    self.listTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.listTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self.listTableView.mj_header beginRefreshing];
    }];
    //self.listTableView.customImg = [UIImage imageNamed:(@"page_icon01_")];
    //self.listTableView.customMsg = @"暂时还没有数据哟";
    
    UIView *tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KBottomSafeViewHeight)];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.listTableView.tableFooterView = tableFooterView;
}

/*
#pragma mark - 请求信息
- (void)requesrtData {
    if (self.serverInfoModel.user_name.length == 0) {
        ShowToastWithText(@"请填写用户姓名");
        return;
    }
    if (self.serverInfoModel.user_phone.length == 0) {
        ShowToastWithText(@"请填写用户手机号");
        return;
    }
    if (self.serverInfoModel.user_address.length == 0) {
        ShowToastWithText(@"请填写详细地址");
        return;
    }
    if (self.serverInfoModel.fault_common.length == 0) {
        ShowToastWithText(@"请填写故障原因");
        return;
    }
    if (self.serverInfoModel.cost.length == 0) {
        ShowToastWithText(@"请填写费用");
        return;
    }
    
    //效验输入的是否是正确的手机号 去除字符串中空格
    self.serverInfoModel.user_phone = [self.serverInfoModel.user_phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:self.serverInfoModel.user_phone]) {
        ShowToastWithText(@"请输入正确电话号码");
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"parts_id"] = safe(self.accessAddModel.Id);
    param[@"qrcode"] = safe(self.accessAddModel.qrcode);
    param[@"user_name"] = safe(self.serverInfoModel.user_name);
    param[@"user_phone"] = safe(self.serverInfoModel.user_phone);;
    param[@"user_address"] = safe(self.serverInfoModel.user_address);
    param[@"fault_common"] = safe(self.serverInfoModel.fault_common);
    param[@"cost"] = safe(self.serverInfoModel.cost);
    NSString *time = [NSString stringWithFormat:@"%@至%@", self.accessAddModel.productionDate, self.accessAddModel.termValidity];
    param[@"validity"] = safe(time);
    param[@"quality"] = safe(self.accessAddModel.warranty);
    NSString *dataString = [HFTools toJSONString:param];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkTool POST:createScanOrder param:@{@"data" : dataString} success:^(id dic) {
        KMyLog(@"生成扫码单%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [LXAlertView initWithTitleAlertTitle:@"订单已提交" titleImage:imgname(@"successapy") Message:@"可在我的订单或候保产品处理页面中查看该订单" sureTitle:@"我知道了" sureClicked:^(LXAlertView *alertView) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}*/

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        ScanMakeOrderAccessoriesInfoCell *cell = [ScanMakeOrderAccessoriesInfoCell cellViewWithTableView:tableView];
        cell.data = self.accessAddModel;
        [cell loadContent];
        return cell;

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    ScanMakeOrderSectionFooterView *footerView = [ScanMakeOrderSectionFooterView sectionViewWithTableView:tableView];
    [footerView.rightButton addTarget:self action:@selector(submitAction:) forControlEvents:UIControlEventTouchUpInside];
    return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
}

#pragma mark - 提交扫码安装信息
- (void)submitAction:(UIButton *)sender {
    NSLog(@"点击提交");
    //[self requesrtData];
}

///判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
