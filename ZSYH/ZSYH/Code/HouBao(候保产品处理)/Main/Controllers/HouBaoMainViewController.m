//
//  HouBaoMainViewController.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/17.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoMainViewController.h"
#import "HouBaoMainTableViewCell.h"
#import "HouBaoMainTableHeaderView.h"
#import "CSnewForumViewController.h"    //候保订单(老的)
#import "HouBaoAccessoryOrderVC.h"      //候保订单(lyj)
#import "ScanOrderControlVC.h"          //扫码订单
#import "ScanMakeOrderViewController.h"
#import "HWScanViewController.h"        //扫一扫
#import "CSmakeAccAddmodel.h"
#import "HBAddAccessViewController.h"   //测试跳转
#import "ZYHouBaoOrderFormViewController.h" //候保订货单
#import "ZYMyOrderViewController.h"     //我的预订
#import "ZYHouBaoOrderViewController.h" //候保预订单

#import "ZYScanCodeInstallViewController.h" //扫码安装
#import "ZYScanBindingViewController.h" //扫码安装

@interface HouBaoMainViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSMutableSet *saoYiSaoStrSet;

@end

@implementation HouBaoMainViewController

- (NSMutableSet *)saoYiSaoStrSet {
    if (!_saoYiSaoStrSet) {
        _saoYiSaoStrSet = [NSMutableSet setWithCapacity:1];
    }
    return _saoYiSaoStrSet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHoubaoCPCLUI];
}

- (void)setUpNavigationBar {
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kScaleNum(150), 44)];
    titleLabel.text = @"扫码安装";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = FontSize(16);
    self.navigationItem.titleView = titleLabel;
    
    self.navBar.hidden = YES;
    self.navBar.backButton.hidden = NO;
    self.navBar.seperateLine.hidden = NO;
    self.navBar.titleLabel.text = @"扫码安装";
}

//新版本UI
- (void)createHoubaoCPCLUI {
    //最上方 安装候保配件展示图片
    UIImageView *topImgView = [[UIImageView alloc] init];
    topImgView.backgroundColor = [UIColor redColor];
    topImgView.frame = CGRectMake(0, 0, KWIDTH, kScaleNum(206));
    [topImgView setImage:imgname(@"anzhuanghoubaopeijian")];
    [self.view addSubview:topImgView];
    
    UIImageView *bottomImg = [[UIImageView alloc] init];
    bottomImg.backgroundColor = [UIColor cyanColor];
    bottomImg.frame = CGRectMake(kScaleNum(50), topImgView.bottom+kScaleNum(46), KWIDTH-kScaleNum(50)*2, kScaleNum(191));
    //[bottomImg setImage:imgname(@"houbaopeijiananzhuang")];
    [self.view addSubview:bottomImg];
    //UIImageView添加点击事件
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(houbaoBtnAction:)];
    [bottomImg addGestureRecognizer:tapGesture];
    bottomImg.userInteractionEnabled = YES;
}

//候保配件安装按钮 点击事件处理
- (void)houbaoBtnAction:(UIButton *)button {
    
    kWeakSelf;
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        
//        if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
//            KMyLog(@"输出一下 str %@", str);
//            ShowToastWithText(@"此配件已扫描");
//            return ;
//        }
//        [weakSelf.saoYiSaoStrSet addObject:str];
        [weakSelf requesrtWith:str];
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
    
    KMyLog(@"您点击了 候保配件安装 按钮");
}

#pragma mark - 请求二维码中的配件信息
- (void)requesrtWith:(NSString *)strr {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"扫描结果%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        if (![mudic isKindOfClass:[NSDictionary class]]) {
            ShowToastWithText(@"数据格式不正确");
            return ;
        }
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc] init];
        //新配件
        [mymodel setValuesForKeysWithDictionary:mudic];
        mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
        mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
        mymodel.ne_name =[mudic objectForKey:@"parts_name"];
        mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.ne_isSaoyisao = @"1";
        
        //跳转到 扫码安装页面
        ZYScanCodeInstallViewController *vc = [[ZYScanCodeInstallViewController alloc] init];
        vc.accessAddModel = mymodel;
        vc.qrcodeStr = mymodel.qrcode;
        [self.navigationController pushViewController:vc animated:YES];
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

/*
 //暂时注释掉
 - (void)buildSubviews {
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.listTableView.showsVerticalScrollIndicator = YES;
    self.listTableView.backgroundColor = kMainBackGroundColor; //暂时先注释掉
    //self.listTableView.backgroundColor = [UIColor whiteColor];
    self.listTableView.estimatedRowHeight = kScaleNum(215);
    //表头
    HouBaoMainTableHeaderView *tableHeaderView = [[HouBaoMainTableHeaderView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, kScaleNum(170))];
    self.listTableView.tableHeaderView = tableHeaderView;
    kWeakSelf;
    tableHeaderView.scanActionBlock = ^{//扫一扫
        HWScanViewController *vc = [[HWScanViewController alloc]init];
        vc.myblock = ^(NSString *str) {
            if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                //ShowToastWithText(@"此配件已扫描");
                //return ;
            }
            [weakSelf.saoYiSaoStrSet addObject:str];
            [weakSelf requesrtWith:str];
        };
        UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
        [self presentViewController:na animated:YES completion:nil];
        
        //HBAddAccessViewController *addAccessVC = [[HBAddAccessViewController alloc] init];
        //[self.navigationController pushViewController:addAccessVC animated:YES];
    };
}

#pragma mark -- UITableViewDelegate 个数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
    //return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kScaleNum(10);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HouBaoMainTableViewCell *cell = [HouBaoMainTableViewCell cellViewWithTableView:tableView];
    kWeakSelf;
    cell.buttonBlock = ^(NSInteger index) {
        switch (index) {
            case 0:
            {
                //扫码订单
                ScanOrderControlVC *forumVC = [[ScanOrderControlVC alloc] init];
                [self.navigationController pushViewController:forumVC animated:YES];
            }
                break;
            case 1:
            {
                //候保订单
                HouBaoAccessoryOrderVC *forumVC = [[HouBaoAccessoryOrderVC alloc] init];
                [self.navigationController pushViewController:forumVC animated:YES];
            }
                break;
            case 2:
            {
                //候保订货单(我的预订)
                //ZYMyOrderViewController *myOrderVC = [[ZYMyOrderViewController alloc] init];//我的预订
                //ZYHouBaoOrderViewController *myOrderVC = [[ZYHouBaoOrderViewController alloc] init]; //候保预订单
                
                ZYHouBaoOrderFormViewController *myOrderVC = [[ZYHouBaoOrderFormViewController alloc] init]; //候保预订单
                [self.navigationController pushViewController:myOrderVC animated:YES];
            }
                break;
            case 3:
            {
                //扫一扫
                //HBAddAccessViewController *addAccessVC = [[HBAddAccessViewController alloc] init];
                //[self.navigationController pushViewController:addAccessVC animated:YES];
                
                //HWScanViewController *vc = [[HWScanViewController alloc]init];
                //    vc.myblock = ^(NSString *str) {
                //        if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                //            //ShowToastWithText(@"此配件已扫描");
                //            //return ;
                //        }
                //        [weakSelf.saoYiSaoStrSet addObject:str];
                //        [weakSelf requesrtWith:str];
                //    };
                //    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                //    [self presentViewController:na animated:YES completion:nil];
                KMyLog(@"敬请期待");
            }
                break;
            default:
                break;
        }
    };
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选中%@", indexPath);
    
}

#pragma mark - 请求二维码中的配件信息
- (void)requesrtWith:(NSString *)strr {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"扫描结果%@",dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        if (![mudic isKindOfClass:[NSDictionary class]]) {
            ShowToastWithText(@"数据格式不正确");
            return ;
        }
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc] init];
        //新配件
        [mymodel setValuesForKeysWithDictionary:mudic];
        mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
        mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
        mymodel.ne_name =[mudic objectForKey:@"parts_name"];
        mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.ne_isSaoyisao = @"1";
        
        ScanMakeOrderViewController *scanMakeOrderVC = [[ScanMakeOrderViewController alloc] init];
        scanMakeOrderVC.accessAddModel = mymodel;
        [self.navigationController pushViewController:scanMakeOrderVC animated:YES];
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}*/

@end
