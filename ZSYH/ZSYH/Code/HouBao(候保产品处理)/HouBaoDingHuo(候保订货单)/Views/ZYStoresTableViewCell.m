//
//  ZYStoresTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYStoresTableViewCell.h"

@interface ZYStoresTableViewCell ()
@property (nonatomic, strong) UIButton *btnSelected;//选中按钮
@property (nonatomic, strong) UILabel *lblName;//店铺名称
@end

@implementation ZYStoresTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    self.contentView.backgroundColor = kColorWithHex(0xf2f2f2);
    
    UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kScreen_Width, 36)];
    viewBg.backgroundColor = kColorWithHex(0xffffff);
    [self.contentView addSubview:viewBg];
    
    _btnSelected = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnSelected.frame = CGRectMake(3, 3, 30, 30);
    [_btnSelected setImage:imgname(@"selequyu") forState:UIControlStateNormal];
    [_btnSelected setImage:imgname(@"seleQuyuyew") forState:UIControlStateSelected];
    [_btnSelected addTarget:self action:@selector(btnSelectedClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewBg addSubview:_btnSelected];
    
    _lblName = [UILabel new];
    _lblName.preferredMaxLayoutWidth = kScreen_Width - 100;
    _lblName.font = [UIFont systemFontOfSize:12];
    _lblName.textColor = kColorWithHex(0x333333);
    [viewBg addSubview:_lblName];
    [_lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.btnSelected.mas_centerY);
        make.leading.equalTo(self.btnSelected.mas_trailing).offset(2);
    }];
    
    UIImageView *ivArrow = [UIImageView new];
    ivArrow.image = imgname(@"jinruimage");
    [viewBg addSubview:ivArrow];
    [ivArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.lblName.mas_centerY);
        make.leading.equalTo(self.lblName.mas_trailing).offset(5);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
}
#pragma mark - 数据更新
- (void)setModel:(ZYMyOrderStoresModel *)model{
    if (model) {
        _model = model;
        _lblName.text = model.factory_name;
        _btnSelected.selected = model.isSelected;
    }
}
#pragma mark - 交互事件
- (void)btnSelectedClickAction:(UIButton *)sender{
    if (self.model && self.delegate && [self.delegate respondsToSelector:@selector(ZYStoreCellClick:isSelected:)]) {
        if (self.model.isSelected) {
            self.model.isSelected = NO;
            sender.selected = NO;
            [self.model.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isOrderSelected = NO;
            }];
            
            [self.delegate ZYStoreCellClick:self.indexPath isSelected:NO];
        }else{
            self.model.isSelected = YES;
            sender.selected = YES;
            [self.model.parts enumerateObjectsUsingBlock:^(ZYMyOrderGoodsModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.isOrderSelected = YES;
            }];
            
            [self.delegate ZYStoreCellClick:self.indexPath isSelected:YES];
        }
    }
}
+ (CGFloat)cellHeight{
    return 46.0;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
