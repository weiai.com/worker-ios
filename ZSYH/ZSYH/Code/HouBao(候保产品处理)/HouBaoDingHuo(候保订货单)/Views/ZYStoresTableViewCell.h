//
//  ZYStoresTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYMyOrderStoresModel.h"

NS_ASSUME_NONNULL_BEGIN


@protocol ZYMyStoreCellDelegate <NSObject>
//选中按钮点击
- (void)ZYStoreCellClick:(NSIndexPath *)indexPath isSelected:(BOOL)isSelected;
@end

@interface ZYStoresTableViewCell : UITableViewCell
@property (nonatomic, strong) ZYMyOrderStoresModel *model;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, weak) id <ZYMyStoreCellDelegate> delegate;

+ (CGFloat)cellHeight;//高度

@end

NS_ASSUME_NONNULL_END
