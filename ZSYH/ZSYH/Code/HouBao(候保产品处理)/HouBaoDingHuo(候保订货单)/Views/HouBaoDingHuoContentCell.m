//
//  HouBaoDingHuoContentCell.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/17.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HouBaoDingHuoContentCell.h"

@implementation HouBaoDingHuoContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)buildSubview {
    [super buildSubview];
    UILabel *timeLabel = [[UILabel alloc] init];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textAlignment = NSTextAlignmentLeft;
    
}

- (void)loadContent {
    
}

@end
