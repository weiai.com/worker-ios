//
//  ZYMyOrderStoresModel.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/8.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyOrderStoresModel.h"

@implementation ZYMyOrderStoresModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"parts" : [ZYMyOrderGoodsModel class]
             };
}
- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.Id = value;
    }
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"Id": @"id"};
}
@end
