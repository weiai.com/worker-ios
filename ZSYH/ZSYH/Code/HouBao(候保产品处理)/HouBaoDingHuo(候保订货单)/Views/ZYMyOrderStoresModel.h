//
//  ZYMyOrderStoresModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/8.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZYMyOrderGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyOrderStoresModel : NSObject

@property (nonatomic, copy) NSString *factory_name;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *factory_address;
@property (nonatomic, copy) NSString *factory_area;
@property (nonatomic, copy) NSString *factory_password;

@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *modifyDate;
@property (nonatomic, copy) NSString *factory_email;
@property (nonatomic, copy) NSString *factory_phone;
@property (nonatomic, copy) NSString *factory_code;

@property (nonatomic, copy) NSMutableArray<ZYMyOrderGoodsModel *> *parts;
@property (nonatomic, copy) NSString *factory_person;
@property (nonatomic, copy) NSString *factory_img;
@property (nonatomic, copy) NSString *factory_remarks;
@property (nonatomic, copy) NSString *factoryId;

@property (nonatomic, copy) NSString *repairUserId;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *isSubFactory;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *agentId;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *addressId;
@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, assign) BOOL isSelected;//标记店铺是否被勾选

@end

NS_ASSUME_NONNULL_END
