//
//  ZYMyOrderGoodsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/8.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyOrderGoodsModel : NSObject
@property (nonatomic, copy) NSString *parts_brand;
@property (nonatomic, copy) NSString *contractPrice;
@property (nonatomic, copy) NSString *lower_user;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *parts_details;

@property (nonatomic, copy) NSString *parts_number;
@property (nonatomic, copy) NSString *salePrice;
@property (nonatomic, copy) NSString *lower_agentB;
@property (nonatomic, copy) NSString *attribute;
@property (nonatomic, copy) NSString *repairUserId;

@property (nonatomic, copy) NSString *is_today_hot;
@property (nonatomic, copy) NSString *is_hot_push;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *careateTime;
@property (nonatomic, copy) NSString *lower_agentA;

@property (nonatomic, copy) NSString *factory_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *parts_model;
@property (nonatomic, copy) NSString *agent_user_price;
@property (nonatomic, assign) NSInteger quantity;

@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *parts_quantity;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *parts_place;
@property (nonatomic, copy) NSString *parts_type;

@property (nonatomic, copy) NSString *is_good_choice;
@property (nonatomic, copy) NSString *is_new_push;
@property (nonatomic, copy) NSString *warranty;
@property (nonatomic, copy) NSString *modifyDate;
@property (nonatomic, copy) NSString *parts_price;

@property (nonatomic, copy) NSString *install;
@property (nonatomic, copy) NSString *parts_name;
@property (nonatomic, copy) NSString *repair_user_price;
@property (nonatomic, copy) NSString *parts_remarks;
@property (nonatomic, assign) NSInteger soft;

@property (nonatomic, copy) NSString *compensationPrice;
@property (nonatomic, copy) NSString *partsId;
@property (nonatomic, copy) NSString *orderId;

//预订单详情
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *cancelReason;
@property (nonatomic, copy) NSString *factoryId;
@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, copy) NSString *classification;
@property (nonatomic, copy) NSString *factory_name;
@property (nonatomic, copy) NSString *factory_address;
@property (nonatomic, copy) NSString *factory_area;
@property (nonatomic, copy) NSString *factory_password;

@property (nonatomic, copy) NSString *factory_email;
@property (nonatomic, copy) NSString *factory_phone;
@property (nonatomic, copy) NSString *factory_code;
@property (nonatomic, copy) NSString *factory_person;

@property (nonatomic, copy) NSString *factory_img;
@property (nonatomic, copy) NSString *factory_remarks;

@property (nonatomic, assign) BOOL isOrderSelected;//标记订购车里的配件是否被勾选

@end

NS_ASSUME_NONNULL_END
