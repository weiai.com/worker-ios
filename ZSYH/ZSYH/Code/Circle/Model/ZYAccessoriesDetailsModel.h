//
//  ZYAccessoriesDetailsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYAccessoriesDetailsModel : BaseModel
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *type_name;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *product_type_id;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *product_name;


@end

NS_ASSUME_NONNULL_END
