//
//  ZYHoubaoContentViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYHoubaoContentViewController : BaseViewController
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;

@end

NS_ASSUME_NONNULL_END
