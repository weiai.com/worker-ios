//
//  ZYHoubaoOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYHoubaoOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderNumLB;

@property (weak, nonatomic) IBOutlet UILabel *orderStateLB;
@property (weak, nonatomic) IBOutlet UILabel *serviceTypeLB;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLB;

@property (weak, nonatomic) IBOutlet UIButton *makeOrderBut;

@property (weak, nonatomic) IBOutlet UIImageView *headerImg;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (nonatomic, copy) void (^orderBlock)(NSString *ind);

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);


-(void)refasf:(CSmaintenanceListModel *)model;

@end

NS_ASSUME_NONNULL_END
