//
//  ZYHoubaoContentViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHoubaoContentViewController.h"
#import "ZYHoubaoOrderTableViewCell.h"
#import "ZYAddHoubaoAccessoriesViewController.h"
#import "ZYHoubaoAccessoriesViewController.h"

@interface ZYHoubaoContentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UIView *bgview;

@end

@implementation ZYHoubaoContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //维修单
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requeset) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requeset) name:@"applicationWillEnterForeground" object:nil];
    
    [self requeset];
}

#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    
    //    [self requeset];
    //    [self.myTableView.mj_header beginRefreshing];
    
    if ([singlTool shareSingTool].needReasfh) {
        [self.myTableView.mj_header beginRefreshing];
        [singlTool shareSingTool].needReasfh = NO;
    }
}

-(void)requeset{
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSInteger ind = [_type integerValue];
    
    ind-=1;
    if (ind >1) {
        ind +=1;
    }
    NSString *stt  = [NSString stringWithFormat:@"%ld",ind];
    if (ind == -1) {
        stt = @"";
    }
    
    // 订单状态(0生成订单（未处理）//添加候保配件
    // ，1已处理，//查看配件详情
    param[@"type"] = stt;
    [weakSelf.mydateSource removeAllObjects];
    [NetWorkTool POST:HbRepOrderList param:param success:^(id dic) {
        
        self.mydateSource = [CSmaintenanceListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 152;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //NSLog(@"滑动开始%f",scrollView.contentOffset.y);
    self.bgview.hidden = scrollView.contentOffset.y>0 ? YES:NO;
}

// any offset changes
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZYHoubaoOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZYHoubaoOrderTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell refasf:model];
    
//    if ([model.ne_isSaoyisao integerValue] == 1){
//        cell.ne_nameTF.userInteractionEnabled = NO;
//        cell.nebut.userInteractionEnabled = NO;
//    }else {
//        cell.ne_nameTF.userInteractionEnabled = YES;
//        cell.nebut.userInteractionEnabled = YES;
//    }
    
    cell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 2:
            {
                CSmaintenanceListModel *model = self->_mydateSource[indexPath.row];
                
                NSString *delstateStr = [[NSNumber numberWithDouble:model.delstate]stringValue];
                
                if ([delstateStr isEqualToString:@"0"]) {
                    //未处理
                    ZYAddHoubaoAccessoriesViewController *addV = [[ZYAddHoubaoAccessoriesViewController alloc] init];
                    addV.mymodel = model;
                    addV.orderID = model.orderId;
                    [self.navigationController pushViewController:addV animated:YES];
                    [self->_myTableView reloadData];
                } else {
                    //已处理
                    ZYHoubaoAccessoriesViewController *Vc = [[ZYHoubaoAccessoriesViewController alloc] init];
                    Vc.mymodel = model;
                    Vc.orderID = model.orderId;
                    [self.navigationController pushViewController:Vc animated:YES];
                    [self->_myTableView reloadData];
                }

            }
                break;
            default:
                break;
        }
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

/**
 * 分区头
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

/**
 * 分区脚
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    NSString *delstateStr = [[NSNumber numberWithDouble:model.delstate]stringValue];
    
    if ([delstateStr isEqualToString:@"0"]) {
        //未处理
        ZYAddHoubaoAccessoriesViewController *addV = [[ZYAddHoubaoAccessoriesViewController alloc] init];
        addV.mymodel = model;
        addV.orderID = model.orderId;
        [self.navigationController pushViewController:addV animated:YES];
        [_myTableView reloadData];
    } else {
        //已处理
        ZYHoubaoAccessoriesViewController *Vc = [[ZYHoubaoAccessoriesViewController alloc] init];
        Vc.mymodel = model;
        Vc.orderID = model.orderId;
        [self.navigationController pushViewController:Vc animated:YES];
        [_myTableView reloadData];
    }
}

-(UITableView *)myTableView{
    if (!_myTableView) {
      
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight- 44) style:(UITableViewStyleGrouped)];
       
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"ZYHoubaoOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZYHoubaoOrderTableViewCell"];
        adjustInset(_myTableView);
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requeset];//请求数据
        }];
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
