//
//  ZYBusinessLicenseViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/2.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYBusinessLicenseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"

@interface ZYBusinessLicenseViewController ()
@property (weak, nonatomic) IBOutlet UILabel *bgViiew;
@property (weak, nonatomic) IBOutlet UIImageView *leftImg;
@property (weak, nonatomic) IBOutlet UILabel *leftTitLab;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
@property (nonatomic,strong) UIImageView *myimage;
@property (nonatomic,strong) UIView *addView;//
@property (nonatomic,assign) CGFloat bottomf;//
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UILabel *bgUPLable;
@property (nonatomic,strong) UILabel *bgdowLable;

@end

@implementation ZYBusinessLicenseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成专属二维码";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    //addpicimage "添加图片"的占位图片
    _myimage.image = imgname(@"addYingYeZhiZhao");
    [self.imageArr addObject:_myimage];
    
    [self showAddImage];
    [self shwoBgview];
}

- (void)showAddImage {
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(16, 125, 100, 100)];
    [self.view addSubview: self.addView];
    CGFloat www = 100;
    self.bottomf = 115;
    [self showinageWirhArr:_imageArr];
    
    CGRect ffff = self.addView.frame;
    ffff.size.height = www;
    self.addView.frame = ffff;
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind]  removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    //self.bottomf = 0;
    CGFloat www = 100;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(16+(www+10)*i, self.bottomf+10, www, www);
        [self.view addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 1) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        }else{
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

//提交按钮点击事件
- (IBAction)commitBtnAction:(id)sender {
    NSLog(@"您点击了 相关证件页面 提交按钮");
    
    [self requestLoadImg];
}

- (void)requestLoadImg {
    
    UIImageView *myyy = [self.imageArr  lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }
    
    if (_imageArr.count>0) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [NetWorkTool UploadPicWithUrl:uploadLicense param:nil key:@"images" image:_imageArr withSuccess:^(id dic) {
            KMyLog(@"____________%@",dic);
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
            [[UIApplication sharedApplication].keyWindow addSubview:self.bgView];
        } failure:^(NSError *error) {
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
        }];
    } else {
        ShowToastWithText(@"请先上传您的营业执照");
        [self.imageArr addObject:self.myimage];
    }
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"您的营业执照已上传成功";
    self.bgUPLable =UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"请耐心等候审核";
    self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    
    //    if (self.myblock) {
    //        self.myblock(@"");
    //    }
    //    [self.navigationController popViewControllerAnimated:YES];
    
    self.navigationController.tabBarController.selectedIndex = 3;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
