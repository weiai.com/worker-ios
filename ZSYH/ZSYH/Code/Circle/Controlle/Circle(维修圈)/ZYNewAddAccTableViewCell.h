//
//  ZYNewAddAccTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmakeAccAddmodel.h"
#import "ZYAccessoriesDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYNewAddAccTableViewCell : UITableViewCell

@property (strong, nonatomic) ZYAccessoriesDetailsModel *myModel;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIButton *saomaBtn;

@property (weak, nonatomic) IBOutlet UILabel *rowLB;

@property (weak, nonatomic) IBOutlet UITextField *product_top_name;
@property (weak, nonatomic) IBOutlet UITextField *product_name;

@property(nonatomic,copy)void (^myblock)(NSInteger stute,NSString *coun);
-(void)refashWithmodel:(ZYAccessoriesDetailsModel *)model;
@end

NS_ASSUME_NONNULL_END
