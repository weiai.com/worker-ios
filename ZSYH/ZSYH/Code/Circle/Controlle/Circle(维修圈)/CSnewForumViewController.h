//
//  CSnewForumViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewForumViewController : BaseViewController
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;

@end

NS_ASSUME_NONNULL_END
