//
//  ZYAddHoubaoAccessoriesViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYAddHoubaoAccessoriesViewController : BaseViewController
@property(nonatomic,strong)CSmaintenanceListModel *mymodel;
@property(nonatomic,strong)NSString *orderID;

@property(nonatomic,strong)NSString *shouhou;
////////////////////////////////////////////////////////////////////
@property(nonatomic,strong)NSString *orderNumberStr;
@property(nonatomic,copy)void (^mybleoc)(NSString *str);

@property(nonatomic,strong)NSString *newid;

@end

NS_ASSUME_NONNULL_END
