//
//  ZYAddHoubaoAccessoriesViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYAddHoubaoAccessoriesViewController.h"
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"
#import "SeleproductTypeViewController.h"
#import "CSchangeNewViewController.h"
#import "CSChangeTwoTableViewCell.h"
#import "HWScanViewController.h"

#import "ZYChangeAccTableViewCell.h"
#import "ZYNewAddAccTableViewCell.h"

//测试
#import "ZYLogisticsInfoViewController.h"

@interface ZYAddHoubaoAccessoriesViewController () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;

@property(nonatomic,strong)UIView *tabFootView;
//@property(nonatomic,strong) UILabel *tatolLB;
//@property (weak, nonatomic) IBOutlet UITextField *mytf;
//@property (weak, nonatomic) IBOutlet UILabel *zongfeiLB;

@property (weak, nonatomic) IBOutlet UILabel *orderNumberLB;

@property(nonatomic,strong)UIView *bgViewsec;
@property (weak, nonatomic) IBOutlet UIButton *AddBtn; //新增按钮
@property (weak, nonatomic) IBOutlet UIButton *replaceBtn; //更换按钮
@property (nonatomic, strong) NSMutableSet <NSString *>*saoYiSaoStrSet;
@end

@implementation ZYAddHoubaoAccessoriesViewController

- (NSMutableSet <NSString *>*)saoYiSaoStrDic {
    if (!_saoYiSaoStrSet) {
        _saoYiSaoStrSet = [NSMutableSet setWithCapacity:1];
    }
    return _saoYiSaoStrSet;
}

- (void)leftClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"候保配件";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    //    [self.rightbutton setImage:imgname(@"Csaoyosao") forState:(UIControlStateNormal)];
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerNib:[UINib nibWithNibName:@"ZYNewAddAccTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZYNewAddAccTableViewCell"];
    [_mytableView registerNib:[UINib nibWithNibName:@"ZYChangeAccTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZYChangeAccTableViewCell"];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
//    self.mytf.keyboardType = UIKeyboardTypeNumberPad;
//    self.orderNumberLB.text = self.newid;
//    [self.mytf addRules];
//    self.mytf.delegate = self;
    
    self.orderNumberLB.text = self.mymodel.Id;
    [self showtabFootView];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view from its nib.
}

-(void)showtabFootView {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
//    self.tatolLB = [[UILabel alloc]initWithFrame:CGRectMake(0, 36, KWIDTH, 18)];
//    self.tatolLB .font = FontSize(18);
//    self.tatolLB.textAlignment = NSTextAlignmentCenter;
//    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥0.00"];
//    [self.tabFootView addSubview: self.tatolLB ];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"提交" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 48, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _mytableView.tableFooterView = self.tabFootView;
}

#pragma mark - 提交
-(void)tuisongAction:(UIButton *)but{
    //推送账单
    // 订单状态(0生成订单（已接单）//生成检测报告
    // ，1已检测，//生成账单
    //2生成账单（未支付），//去支付
    // 3已支付（已完成），//去评价
    //4已评价，
    //  5已取消)',//先不管
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
//    if (strIsEmpty(_mytf.text)) {
//        ShowToastWithText(@"请输入工时费");
//        return;
//    }
//
//    param[@"peopleCost"] = _mytf.text;
    param[@"orderId"] = self.mymodel.repid;
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *oldmuarr = [NSMutableArray arrayWithCapacity:1];
    if (_mydateSource.count == 0) {
        ShowToastWithText(@"请先添加配件");
        return;
    }
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];

        if ([model.typeProduct integerValue] == 2) {
            //设计更换配件 暂不操作

            NSMutableDictionary *oldmuDic = [NSMutableDictionary dictionaryWithCapacity:1];
            if (strIsEmpty(model.ne_name)) {
                ShowToastWithText(@"请扫描新更换配件");
                return;
            }
            if (strIsEmpty(model.ne_type_id)) {
                ShowToastWithText(@"请扫描新更换配件");
                return;
            }
            if (strIsEmpty(model.old_name)) {
                ShowToastWithText(@"请扫描旧配件");
                return;
            }
            if (strIsEmpty(model.old_type_id)) {
                ShowToastWithText(@"请扫描旧配件");
                return;
            }

//            muDic[@"price"] = model.total_price;
//            if (strIsEmpty( model.old_price)) {
//                ShowToastWithText(@"配件费不能为空");
//                return;
//            }
//            if ([model.old_price integerValue] == 0) {
//                ShowToastWithText(@"配件费不能为0");
//                return;
//            }
            muDic[@"type_id"] = model.ne_type_id;
            muDic[@"count"] = @"1";
            muDic[@"name"] = model.ne_name;
            muDic[@"qrcode"] = model.qrCode;
            muDic[@"product_model"] = @"0";

            [muarr addObject:muDic];

//            oldmuDic[@"price"] = model.old_price;
            oldmuDic[@"price"] = @"";
            oldmuDic[@"type_id"] = model.old_type_id;
            oldmuDic[@"count"] = @"1";
            oldmuDic[@"name"] = model.old_name;
            oldmuDic[@"qrcode"] = model.old_qrCode;
            [oldmuarr addObject:oldmuDic];
        } else {
            if (strIsEmpty(model.name)) {
                ShowToastWithText(@"请扫描添加配件");
                return;
            }
            if (strIsEmpty(model.type_id)) {
                ShowToastWithText(@"请扫描添加配件");
                return;
            }
//            if (strIsEmpty(model.price)) {
//                ShowToastWithText(@"配件费不能为空");
//                return;
//            }
//            if ([model.price integerValue] == 0) {
//                ShowToastWithText(@"配件费不能为0");
//                return;
//            }
//            muDic[@"price"] = model.price;
            muDic[@"price"] = @"";
            muDic[@"type_id"] = model.type_id;
            muDic[@"count"] = model.count;
            muDic[@"name"] = model.name;
            muDic[@"productId"] = model.type_id;
            muDic[@"qrcode"] = model.qrCode;
            muDic[@"product_model"] = @"1";
            [muarr addObject:muDic];
        }
    }

    param[@"products"] = muarr;
    param[@"up_products"] = oldmuarr;

    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];

    allDic[@"data"] = json;
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [NetWorkTool POST:delRepProduct param:allDic success:^(id dic) {

        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        weakSelf.bgViewsec.center = self.view.center;

   

    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

//更换配件按钮点击事件
/**
 更换配件
  @param sender 更换配件
 */
- (IBAction)replaceBtnAction:(id)sender {    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    
    CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
    //    mymodel.qrCode =@"";
    //    mymodel.type_name =@"";
    //    mymodel.price =@"";
    //    mymodel.price = @"0";
    //    mymodel.type_id =@"";
    //    mymodel.Id =@"";
    //    mymodel.typeProduct = @"2";
    //    mymodel.total_price = @"";
    //    mymodel.ne_name =@"";
    //    mymodel.old_name =@"";
    //    mymodel.isSaoyisao =@"2";
    //
    //    mymodel.type_name =@"";
    //
    //    //        mymodel.count =[mudic objectForKey:@"count"];
    //    mymodel.count  = @"";
    //    mymodel.name = @"";
    //    mymodel.old_type_name = @"";
    //    mymodel.ne_type_name = @"";
    
    mymodel.row = @"";    mymodel.type_name = @"";
    mymodel.type_id = @"";    mymodel.name = @"";
    mymodel.price = @"";    mymodel.count = @"1";
    mymodel.total_price = @"";    mymodel.qrCode = @"";
    mymodel.isSaoyisao = @"2";    mymodel.typeProduct = @"2";
    mymodel.old_type_name = @"";  mymodel.old_type_id = @"";
    mymodel.old_name = @"";     mymodel.old_price = @"1";
    mymodel.old_count = @"1";    mymodel.old_total_price = @"";
    
    mymodel.old_isSaoyisao = @"2";    mymodel.ne_type_name = @"";
    mymodel.ne_type_id = @"";    mymodel.ne_name = @"";
    mymodel.ne_price = @"2";    mymodel.ne_count = @"";
    mymodel.ne_total_price = @"";    mymodel.ne_qrCode = @"";
    mymodel.ne_isSaoyisao = @"2"; model.typeProduct = @"2";
    model.ne_count = @"1"; model.old_count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

//新增配件按钮点击事件
/**
 @param sender 添加配件
 */
- (IBAction)newAddBtnClick:(id)sender {
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.isSaoyisao = @"1";
    model.count = @"1";
    [self.mydateSource addObject:model];
    [self.mytableView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([model.typeProduct integerValue] == 2) {
        return 378;
    }else{
        return 163;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    model.row = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    if ([model.typeProduct integerValue] == 2) {
        ZYChangeAccTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:@"ZYChangeAccTableViewCell" forIndexPath:indexPath];
        mycell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (!strIsEmpty(model.old_type_name)) {
//            [mycell.oldBut setTitleColor:K333333 forState:(UIControlStateNormal)];
//            [mycell.oldBut setTitle:model.old_type_name forState:(UIControlStateNormal)];
        }else{
//            [mycell.oldBut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
//            [mycell.oldBut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        if (!strIsEmpty(model.ne_type_name)) {
//            [mycell.nebut setTitleColor:K333333 forState:(UIControlStateNormal)];
//            [mycell.nebut setTitle:model.ne_type_name forState:(UIControlStateNormal)];
        }else{
//            [mycell.nebut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
//            [mycell.nebut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        mycell.old_top_nameTF.text = model.old_name;
        mycell.old_nameTF.text = model.old_name;

        mycell.ne_top_nameTF.text = model.ne_name;
        mycell.ne_nameTF.text = model.ne_name;
//        mycell.PeiJianFeiTF.text = model.total_price;
        mycell.rowLB.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        //    mycell.old_nameTF.userInteractionEnabled = YES;
        //    mycell.oldBut.userInteractionEnabled = YES;
        //    mycell.ne_nameTF.userInteractionEnabled = YES;
        //    mycell.nebut.userInteractionEnabled = YES;
        
//        if ([model.old_isSaoyisao integerValue] == 1) {
//            mycell.old_top_nameTF.userInteractionEnabled = NO;
////            mycell.oldBut.userInteractionEnabled = NO;
//        }
//        if ([model.old_isSaoyisao integerValue] == 1) {
//            mycell.old_nameTF.userInteractionEnabled = NO;
////            mycell.oldBut.userInteractionEnabled = NO;
//        }
        
        if ([model.old_isSaoyisao integerValue] == 1) {
            mycell.ne_top_nameTF.userInteractionEnabled = NO;
//            mycell.oldBut.userInteractionEnabled = NO;
        }
        if ([model.ne_isSaoyisao integerValue] == 1){
            mycell.ne_nameTF.userInteractionEnabled = NO;
//            mycell.nebut.userInteractionEnabled = NO;
        }
        kWeakSelf;
        mycell.myblock = ^(NSUInteger ind,NSString *str) {
            
            switch (ind) {
                case 0:
                {//上边的扫一扫
                    //记录扫一扫的位置 如:1-0 第1个位置的第0位上的扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                            ShowToastWithText(@"此配件已扫描");
                            return ;
                        }
                        [weakSelf.saoYiSaoStrSet addObject:str];
                        [self requesrtWith:str withind:indexPath.row isup:1];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 1:
                {///删除
                    [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                    [weakSelf.mytableView reloadData];
                    
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
//                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle]
//                    ffftotle = ffftotle +[self.mytf.text floatValue];
//                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                default:
                    break;
            }
        };
        return mycell;
    } else {
        //CSmakeAccontListcell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmakeAccontListcell" forIndexPath:indexPath];
        ZYNewAddAccTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZYNewAddAccTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell refashWithmodel:model];
        //self.myblock(0, @""); 0配件单价 1 配件名称2删除 3配件数量 4扫一扫 5选择配件种类 6 7
        kWeakSelf;
        cell.myblock = ^(NSInteger stute, NSString * _Nonnull coun) {
            switch (stute) {
                case 0:
                {//配件单价
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.count floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];                CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    model.price = coun;
                }
                    break;
                case 1:
                {//配件名称
                    model.name = coun;
                }
                    break;
                case 2:
                {//删除
                    [weakSelf.mydateSource removeObject:model];
                    [weakSelf.mytableView reloadData];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                }
                    break;
                case 3:
                {//配件数量
                    model.count = coun;
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.price floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
//                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
//                    ffftotle = ffftotle +[self.mytf.text floatValue];
//                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 4:
                {//扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                            ShowToastWithText(@"此配件已扫描");
                            return ;
                        }
                        [weakSelf.saoYiSaoStrSet addObject:str];
                        [self requesrtWith:str withind:indexPath.row];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 5:
//                {//选择配件种类
//                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
//                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
//                        model.type_name = str;
//                        model.type_id = idstr;
//                        [weakSelf.mytableView reloadData];
//                    };
//                    [self.navigationController pushViewController:seleVc animated:YES];
//                }
                    break;
                default:
                    break;
            }
        };
        
        if ((indexPath.row+1) == _mydateSource.count) {
            [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
        return cell;
    }
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind isup:(NSInteger )index{
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = weakSelf.mydateSource[ind];

            mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
            mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.ne_name =[mudic objectForKey:@"parts_name"];
            mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.ne_isSaoyisao = @"1";

        [self.mytableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
        mymodel.qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.type_name =[mudic objectForKey:@"type_name"];
        mymodel.price =[mudic objectForKey:@"parts_price"];
        mymodel.price = @"0";
        mymodel.type_id =[mudic objectForKey:@"parts_type"];
        mymodel.Id =[mudic objectForKey:@"id"];
        
        //mymodel.count =[mudic objectForKey:@"count"];
        mymodel.count  = @"1";
        mymodel.name =[mudic objectForKey:@"parts_name"];
        mymodel.isSaoyisao = @"0";
        
        [self->_mydateSource removeObjectAtIndex:ind];
        [self->_mydateSource insertObject:mymodel atIndex:ind];
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    CGFloat ffftotle = 0;
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        CGFloat motltlt = [model.total_price floatValue];
        ffftotle = ffftotle +motltlt;
    }
    return YES;
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"配件信息已提交\n可在已处理页面查看配件详情";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //    DowLable.text = @"明天继续努力";
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    
//    if (self.mybleoc) {
//        self.mybleoc(@"");
//    }    
    //[self.navigationController popViewControllerAnimated:YES];
    self.navigationController.tabBarController.selectedIndex = 4;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    if (textField == self.mytf) {
//        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
//        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
//            ShowToastWithText(@"只能输入数字");
//            return NO;
//        }else{
//            return YES;
//        }
//    }else{
        return YES;
//    }
}

@end
