//
//  ZYChangeAccTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYChangeAccTableViewCell.h"

@interface ZYChangeAccTableViewCell ()<UITextFieldDelegate>

@end

@implementation ZYChangeAccTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.old_nameTF.delegate = self;
    self.ne_nameTF.delegate = self;
    //不可手输
    self.old_top_nameTF.userInteractionEnabled = NO;
    self.ne_top_nameTF.userInteractionEnabled = NO;
    self.old_nameTF.userInteractionEnabled = NO;
    self.ne_nameTF.userInteractionEnabled = NO;
    
    // Initialization code
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.old_nameTF) {
        if (self.myblock) {
            self.myblock(2,textField.text);//上边的输入名称
        }
    }else{
        if (self.myblock) {
            self.myblock(5,textField.text);//下边的输入名称
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)upaction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(0,@"");//上边的扫一扫
    }
}

- (IBAction)upselseprotype:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(1,@"");//上边的选择品类
    }
}

- (IBAction)dowchannge:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(3,@"");//下边的扫一扫
    }
}

- (IBAction)dowsles:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(4,@"");//下边的选择品类
    }
}

- (IBAction)chaaction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(6,@"");//删除
    }
}

@end
