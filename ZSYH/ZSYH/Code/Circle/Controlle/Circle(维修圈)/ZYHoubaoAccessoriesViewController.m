//
//  ZYHoubaoAccessoriesViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/27.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHoubaoAccessoriesViewController.h"
#import "ZYChangeAccTableViewCell.h"
#import "ZYNewAddAccTableViewCell.h"
#import "ZYAccessoriesDetailsModel.h"

@interface ZYHoubaoAccessoriesViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;

@end

@implementation ZYHoubaoAccessoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配件详情";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];

    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerNib:[UINib nibWithNibName:@"ZYNewAddAccTableViewCell" bundle:nil] forCellReuseIdentifier:@"ZYNewAddAccTableViewCell"];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
    self.orderNumLab.text = self.mymodel.Id;
    
    [self requesrtWith];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 163;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZYNewAddAccTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ZYNewAddAccTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.product_top_name.userInteractionEnabled = NO;
    cell.product_name.userInteractionEnabled = NO;
    
    cell.closeBtn.hidden = YES;
    cell.saomaBtn.hidden = YES;
    
    ZYAccessoriesDetailsModel *model = self.mydateSource[indexPath.row];
    cell.product_top_name.text = model.product_name;
    cell.product_name.text = model.type_name;
    cell.rowLB.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    
    //    [cell refashWithmodel:model];
    //    if ((indexPath.row+1) == _mydateSource.count) {
    //        [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    //    }
    return cell;
}

- (void)requesrtWith {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(self.mymodel.repid);
    
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        
        for (NSMutableDictionary *mudic in [[dic objectForKey:@"data"] objectForKey:@"pros"]) {
            ZYAccessoriesDetailsModel *model = [[ZYAccessoriesDetailsModel alloc]init];
            [model setValuesForKeysWithDictionary:mudic];
            [self.mydateSource addObject:model];
        }
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

@end
