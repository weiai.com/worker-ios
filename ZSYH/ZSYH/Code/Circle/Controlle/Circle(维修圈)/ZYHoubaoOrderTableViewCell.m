//
//  ZYHoubaoOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYHoubaoOrderTableViewCell.h"

@implementation ZYHoubaoOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgImage.layer.masksToBounds = YES;
    self.bgImage.layer.cornerRadius = 8;
    self.bgImage.layer.borderWidth = 1;
    self.bgImage.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    
   
}

-(void)refasf:(CSmaintenanceListModel *)model{
    
    [self.headerImg setImage:imgname(@"组 3123")];
    self.orderStateLB.textColor = [UIColor colorWithHexString:@"#F88B1F"];
    self.bgImage.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    
    //self.order_number.text = [NSString stringWithFormat:@"订单编号:%@",model.orderNumber];
    self.orderNumLB.text = [NSString stringWithFormat:@"%@%@",@"订单编号:",model.Id];
    self.orderStateLB.text = [NSString stringWithFormat:@"%@",model.order_state];
    self.serviceTypeLB.text = @"电器维修";
    self.productTypeLB.text = [NSString stringWithFormat:@"%@",model.fault_name];
    
    // 订单状态(0生成订单（已处理）//查看配件详情
    // 1未处理，//添加候保配件
    
    self.makeOrderBut.hidden = NO;
    NSString *delstateStr = [[NSNumber numberWithDouble:model.delstate]stringValue];
    
    if ([model.fault_type isEqualToString:@"3"]) { //维修
        
        if ([delstateStr isEqualToString:@"0"]) {
            //未处理
            self.orderStateLB.text = @"未处理";
            [self.makeOrderBut setImage:imgname(@"组 3107") forState:(UIControlStateNormal)];
        } else {
            //已处理
            self.orderStateLB.text = @"已处理";
            [self.makeOrderBut setImage:imgname(@"组 3114") forState:(UIControlStateNormal)];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)makeOrderAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(2, @"");
    }
}

//- (IBAction)lrfeAction:(UIButton *)sender {
//    if (self.orderBlock) {
//        self.orderBlock(@"0");
//    }
//}
//
//- (IBAction)centerAction:(UIButton *)sender {
//    if (self.orderBlock) {
//        self.orderBlock(@"1");
//    }
//}

@end
