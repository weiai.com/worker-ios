//
//  ZYNewAddAccTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYNewAddAccTableViewCell.h"

@interface ZYNewAddAccTableViewCell ()<UITextFieldDelegate>
@property(nonatomic,strong)UIView *bgViewsec;

@end

@implementation ZYNewAddAccTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.product_top_name.userInteractionEnabled = NO;
    self.product_name.userInteractionEnabled = NO;
    self.product_name.delegate = self;
    [self.product_name addTarget:self action:@selector(inputContentClick:) forControlEvents:(UIControlEventEditingDidEnd)];
    [self.product_name addTarget:self action:@selector(inputContentClending:) forControlEvents:(UIControlEventEditingDidBegin)];
    self.rowLB.layer.borderWidth = 1;
    self.rowLB.layer.borderColor = [UIColor colorWithHexString:@"#333333"].CGColor;
    self.rowLB.layer.masksToBounds = YES;
    self.rowLB.layer.cornerRadius = self.rowLB.width/2;
    // Initialization code
}

- (void)inputContentClending:(UITextField *)sender {
//    if ([self.myModel.isSaoyisao integerValue] == 1) {
//
//    } else {
//
//    }
}

- (void)inputContentClick:(UITextField *)sender {
//    if ([self.myModel.isSaoyisao integerValue] == 1) {
//
//    } else {
//
//    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    if (textField == self.product_price) {
//        self.product_price.text = @"";
//        if (self.myblock) {
//            self.myblock(0, @"");
//        }
//    } else if(textField == self.product_name){
//        if (self.myblock) {
//            self.myblock(1, @"");
//        }
//    }
    return YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)refashWithmodel:(CSmakeAccAddmodel *)model{

    self.myModel = model;
    self.product_name.userInteractionEnabled = YES;
    
    if ([model.isSaoyisao integerValue] == 0) {
        self.product_name.userInteractionEnabled = NO;
    }

    self.product_top_name.text = model.name;
    self.product_name.text = model.name;

    if (strIsEmpty(model.total_price)) {
        model.total_price = @"0";
    }
    self.rowLB.text = model.row;
}

- (IBAction)jinashaoBUt:(UIButton *)sender {
    KMyLog(@"ddd");
//    [self.product_price resignFirstResponder];
    [self.product_name resignFirstResponder];
    
    //NSInteger ind = [self.product_count.text integerValue];
//    ind -=1;
//    if (ind == 0) {
//        ind = 1;
//    }
//    if (ind == -1) {
//        ind = 1;
//    }
//    //self.product_count.text = [NSString stringWithFormat:@"%ld",ind];
//
//    if ([self.product_price.text floatValue] >= 0) {
//        CGFloat fff = [self.product_price.text floatValue] * ind;
//        self.totalPrice.text = [NSString stringWithFormat:@"%.2f",fff];
//        if (self.myblock) {
//            //self.myblock(3, self.product_count.text);
//        }
//    }
}

- (IBAction)addbut:(UIButton *)sender {
    [self.product_name resignFirstResponder];
    //NSInteger ind = [self.product_count.text integerValue];
//    ind +=1;
//    if (ind == 0) {
//        ind = 1;
//    }
//    self.product_count.text = [NSString stringWithFormat:@"%ld",ind];
//    if ([self.product_price.text floatValue] >= 0) {
//        CGFloat fff = [self.product_price.text floatValue] * ind;
//        self.totalPrice.text = [NSString stringWithFormat:@"%.2f",fff];
//
//    }
    if (self.myblock) {
        //self.myblock(3, self.product_count.text);
    }
}

- (IBAction)saoyisao:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2 3配件数量 4扫一扫 5 6 7
    if (self.myblock) {
        self.myblock(4, @"");
    }
}

- (IBAction)seletorProductGAtegory:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2 3配件数量 4扫一扫 5选择配件种类 6 7
//    if (self.myblock) {
//        self.myblock(5, @"");
//    }
}

- (IBAction)deldeAction:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2删除 3配件数量 4扫一扫 5选择配件种类 6 7
    if (self.myblock) {
        self.myblock(2, @"");
    }
}

@end
