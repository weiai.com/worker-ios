//
//  ZYLogisticsHeaderView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYLogisticsHeaderView : UIView
@property (copy, nonatomic)NSString *number;
@property (copy, nonatomic)NSString *company;
@property (copy, nonatomic)NSString *phone;
@property (nonatomic,copy) NSString * wltype;
@property (nonatomic,copy) NSString * imageUrl;
@end

NS_ASSUME_NONNULL_END
