//
//  ZYLogisticsHeaderView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYLogisticsHeaderView.h"

@interface ZYLogisticsHeaderView ()
@property (nonatomic,strong) UIImageView *goodsPic;
@property (nonatomic,strong) UILabel *type;
@property (strong, nonatomic) UILabel *numLabel;
@property (strong, nonatomic) UILabel *comLabel;
@property (strong, nonatomic) UILabel *phoneLabel;
@property (nonatomic,strong) UIWebView *phoneCallWebView;
@end

#define GRAY_TITLECOLOR 0x9D9D9D

@implementation ZYLogisticsHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setPhone:(NSString *)phone {
    _phone = phone;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"官方电话：%@",phone]];
    [text addAttribute:NSForegroundColorAttributeName value:nckColor(GRAY_TITLECOLOR) range:NSMakeRange(0, 5)];

    self.phoneLabel.attributedText = text;
}

- (void)setNumber:(NSString *)number {
    _number = number;
    self.numLabel.text = [NSString stringWithFormat:@"运单编号：%@",number];
}

- (void)setCompany:(NSString *)company {
    _company = company;
    self.comLabel.text = [NSString stringWithFormat:@"承运公司：%@",company];
}
- (void)setWltype:(NSString *)wltype {
    _wltype = wltype;
    
    NSMutableAttributedString *wlStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"物流状态：%@",wltype]];
    NSRange range = [[NSString stringWithFormat:@"物流状态：%@",wltype] rangeOfString: wltype];
    [wlStr addAttribute:NSForegroundColorAttributeName value:nckColor(0x07A628) range:range];
    self.type.attributedText = wlStr;
}
- (void)setImageUrl:(NSString *)imageUrl {
    _imageUrl = imageUrl;
    
}
- (void)setupUI {
    self.backgroundColor=[UIColor whiteColor];
    
    self.goodsPic.frame=CGRectMake(15, 13, 56,56);
    self.goodsPic.image = [UIImage imageNamed:@"组 3202"];
    [self addSubview:self.goodsPic];
    
    self.comLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.goodsPic.right+16 ,18, 200, 2*Gap)];
    self.comLabel.font = [UIFont systemFontOfSize:12];
    self.comLabel.textColor = nckColor(GRAY_TITLECOLOR);
    self.comLabel.text = @"承运公司:";
    [self addSubview:self.comLabel];

    
    self.numLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.goodsPic.right+16 ,CGRectGetMaxY(self.comLabel.frame)+7, ScreenW-56-16-16, 2*Gap)];
    self.numLabel.font = [UIFont systemFontOfSize:12];
    self.numLabel.textColor = nckColor(GRAY_TITLECOLOR);
    self.numLabel.text = @"快递单号:";
    [self addSubview:self.numLabel];
    
    UILabel *line=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.goodsPic.frame)+15, KWIDTH, 0.5)];
    line.backgroundColor=nckColor(0xf1f1f1);
    [self addSubview:line];
    
    UILabel *wuLiuDongTaiLab =[[UILabel alloc]initWithFrame:CGRectMake(16,line.bottom+10, 100, 20)];
    wuLiuDongTaiLab.textColor = [UIColor colorWithHexString:@"#666666"];
    wuLiuDongTaiLab.font = [UIFont systemFontOfSize:14];
    wuLiuDongTaiLab.text = @"物流动态";
    [self addSubview:wuLiuDongTaiLab];
    
}
#pragma mark 懒加载
-(UIImageView *)goodsPic{
    
    if(!_goodsPic) {
        
        _goodsPic =[[UIImageView alloc]init];
        
    }
    
    return _goodsPic;
    
}
-(UILabel *)type{
    
    if(!_type) {
        
        _type =[[UILabel alloc]init];
        
    }
    
    return _type;
    
}
-(UILabel *)numLabel{
    
    if(!_numLabel) {
        
        _numLabel =[[UILabel alloc]init];
        
    }
    
    return _numLabel;
    
}
-(UILabel *)comLabel{
    
    if(!_comLabel) {
        
        _comLabel =[[UILabel alloc]init];
        
    }
    
    return _comLabel;
    
}
-(UILabel *)phoneLabel{
    
    if(!_phoneLabel) {
        
        _phoneLabel =[UILabel new];
        
    }
    
    return _phoneLabel;
    
}

- (void)callPhoneThree:(NSString *)phoneNum{
    /*--------拨号方法三-----------*/
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNum]];
    if ( !_phoneCallWebView ) {
        
        _phoneCallWebView = [[UIWebView alloc] initWithFrame:CGRectZero];// 这个webView只是一个后台的容易 不需要add到页面上来  效果跟方法二一样 但是这个方法是合法的
    }
    [_phoneCallWebView loadRequest:[NSURLRequest requestWithURL:phoneURL]];
}

@end
