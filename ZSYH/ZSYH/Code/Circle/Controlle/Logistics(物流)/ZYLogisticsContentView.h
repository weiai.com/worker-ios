//
//  ZYLogisticsContentView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYLogisticsHeaderView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYLogisticsContentView : UIView

/**
 运单号码
 */
@property (copy, nonatomic)NSString *number;

/**
 承运公司
 */
@property (copy, nonatomic)NSString *company;

/**
 官方电话
 */
@property (copy, nonatomic)NSString *phone;

/**
 物流状态
 */
@property (nonatomic,copy) NSString * wltype;

/**
 图片url
 */
@property (nonatomic,copy) NSString * imageUrl;
@property (strong, nonatomic)NSArray *datas;
@property (nonatomic,strong) ZYLogisticsHeaderView *header ;
- (instancetype)initWithDatas:(NSArray*)array;
- (void)reloadDataWithDatas:(NSArray *)array;

NS_ASSUME_NONNULL_END

@end
