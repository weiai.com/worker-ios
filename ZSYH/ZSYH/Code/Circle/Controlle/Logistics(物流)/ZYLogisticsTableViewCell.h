//
//  ZYLogisticsTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYLogisticsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYLogisticsTableViewCell : UITableViewCell
@property (assign, nonatomic) BOOL hasUpLine;
@property (assign, nonatomic) BOOL hasDownLine;
@property (assign, nonatomic) BOOL currented;

- (void)reloadDataWithModel:(ZYLogisticsModel *)model;
@end

NS_ASSUME_NONNULL_END
