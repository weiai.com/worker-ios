//
//  NSString+phone.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (phone)

#pragma mark - 电话验证
+ (BOOL)isMobilePhoneOrtelePhone:(NSString *)mobileNum;

@end

NS_ASSUME_NONNULL_END
