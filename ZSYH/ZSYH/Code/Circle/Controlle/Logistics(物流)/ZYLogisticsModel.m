//
//  ZYLogisticsModel.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYLogisticsModel.h"

@interface ZYLogisticsModel ()

@property (assign, nonatomic)CGFloat tempHeight;
@end

@implementation ZYLogisticsModel

- (CGFloat)height {
    
    if (_tempHeight == 0) {
        
        NSDictionary * dict=[NSDictionary dictionaryWithObject: [UIFont systemFontOfSize:12] forKey:NSFontAttributeName];
        
        CGRect rect=[self.remark boundingRectWithSize:CGSizeMake(KWIDTH - ok_leftSpace - 2*ok_rightSpace, CGFLOAT_MAX) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
        
        _tempHeight = rect.size.height + 50;;
    }
    
    return _tempHeight;
}
@end
