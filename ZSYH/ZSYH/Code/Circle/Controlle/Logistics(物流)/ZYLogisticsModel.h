//
//  ZYLogisticsModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
@interface ZYLogisticsModel : NSObject
//@property (copy, nonatomic)NSString *dsc;
//@property (copy, nonatomic)NSString *date;
//@property (assign, nonatomic, readonly)CGFloat height;

@property (nonatomic, copy) NSString *datetime;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *zone;
@property (assign, nonatomic, readonly)CGFloat height;

@end
