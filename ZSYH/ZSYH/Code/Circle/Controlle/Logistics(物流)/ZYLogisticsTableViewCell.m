//
//  ZYLogisticsTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYLogisticsTableViewCell.h"
#import "ZYLogisticsTableCellContentView.h"

@interface ZYLogisticsTableViewCell ()

@property (strong, nonatomic)ZYLogisticsTableCellContentView *customView;

@end

@implementation ZYLogisticsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setupUI];
    }
    
    return self;
}

- (void)reloadDataWithModel:(ZYLogisticsModel*)model {
    
    [self.customView reloadDataWithModel:model];
}

- (void)setHasUpLine:(BOOL)hasUpLine {
    
    self.customView.hasUpLine = hasUpLine;
}

- (void)setHasDownLine:(BOOL)hasDownLine {
    
    self.customView.hasDownLine = hasDownLine;
}

- (void)setCurrented:(BOOL)currented {
    
    self.customView.currented = currented;
}

- (void)setupUI {
    
    ZYLogisticsTableCellContentView *custom = [[ZYLogisticsTableCellContentView alloc]init];
    [self addSubview:custom];
    self.customView = custom;
    
    custom.currented = NO;
    custom.hasUpLine = YES;
    custom.hasDownLine = YES;
    [custom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
