//
//  ZYLogisticsInfoViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYLogisticsInfoViewController.h"
#import "ZYLogisticsContentView.h"
#import "ZYLogisticsModel.h"

@interface ZYLogisticsInfoViewController ()
@property (nonatomic,strong) NSMutableArray *dataArry;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) NSMutableArray *areaArr;
@property (nonatomic,strong) NSMutableArray *dateArr;
@property (nonatomic,strong) NSDictionary *resultDic;
@end

@implementation ZYLogisticsInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"物流信息";
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 131)];
    [imageView setImage:imgname(@"组 3195")];
    self.imageView = imageView;
    [self.view addSubview:imageView];
    [self configViews];
    [self requstLogistics]; //请求数据
}

-(void)configViews
{
    
}

- (void)requstLogistics {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"com"] = NOTNIL(self.model.courierCompany); //快递公司
    param[@"no"] = NOTNIL(self.model.courierNumber); //快递订单号
    param[@"key"] = @"9a4090e5886ecd9fe24c1926053d315a";
    
    [NetWorkTool POST:LOOKLogisticsInfo param:param success:^(id dic) {
        
        self.resultDic = dic;
        for (NSMutableDictionary *dci in [[dic objectForKey:@"result"] objectForKey:@"list"]) {
            ZYLogisticsModel *model = [[ZYLogisticsModel alloc]init];
            [model setValuesForKeysWithDictionary:dci];
            [weakSelf.dataArry addObject:model];
            //日期数组
            self.dateArr = [[NSMutableArray alloc] init];
            [self.dateArr addObject:dci[@"datetime"]];
           
            //地点数组
            self.areaArr = [[NSMutableArray alloc] init];
            [self.areaArr addObject:dci[@"remark"]];
            
        }
        NSLog(@"时间数组是什么 %@", self.dateArr);
        NSLog(@"地点数组是什么 %@", self.areaArr);
        [self reloadViewsWithData];
    } other:^(id dic) {
    
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

-(void)reloadViewsWithData
{
    for (NSInteger i = self.areaArr.count-1;i>=0 ; i--) {
        ZYLogisticsModel *model = [[ZYLogisticsModel alloc]init];
        model.remark = [self.areaArr objectAtIndex:i];
        model.datetime = [self.dateArr objectAtIndex:i];
        [self.dataArry addObject:model];
    }
    // 数组倒叙
    self.dataArry = (NSMutableArray *)[[self.dataArry reverseObjectEnumerator] allObjects];
    ZYLogisticsContentView *logis = [[ZYLogisticsContentView alloc]initWithDatas:self.dataArry];
    // 给headView赋值
    logis.company = [NSString stringWithFormat:@"%@",self.resultDic[@"result"][@"company"]];
    logis.number = [NSString stringWithFormat:@"%@",self.resultDic[@"result"][@"no"]];
    logis.frame = CGRectMake(0, _imageView.bottom + 10, KWIDTH, KHEIGHT - kNaviHeight - 111);
    [self.view addSubview:logis];
}

- (NSMutableArray *)dataArry {
    if (!_dataArry) {
        _dataArry = [NSMutableArray array];
    }
    return _dataArry;
}

@end
