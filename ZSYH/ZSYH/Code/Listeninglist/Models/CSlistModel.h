//
//  CSlistModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSlistModel : BaseModel

@property (nonatomic, copy) NSString *fixed_price;//一口价
@property (nonatomic, copy) NSString *product_source;//配件来源 1厂家提供 2维修工提供
@property (nonatomic, copy) NSString *common_failures;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *order_date;
@property (nonatomic, copy) NSString *user_address;
@property (nonatomic, copy) NSString *user_phone;
@property (nonatomic, copy) NSString *has_elevator;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *service_name;
@property (nonatomic, assign) double repair_number;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *order_state;
@property (nonatomic, copy) NSString *floorNum;
@property (nonatomic, copy) NSString *fault_category;
@property (nonatomic, copy) NSString *fault_comment;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *user_address_id;
@property (nonatomic, copy) NSString *fault_type;
@property (nonatomic, copy) NSString *cancel_reason;
@property (nonatomic, copy) NSString *user_type;
@property (nonatomic, copy) NSString *fault_name;
@property (nonatomic, copy) NSString *isprivate_work;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *is_protect;
@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *company_phone;
@property (nonatomic, copy) NSString *is_again_order;
@property (nonatomic, copy) NSString *credit_score; //信用分

@property (nonatomic, copy) NSString *rep_order_id;

@property (nonatomic, assign) NSInteger isup;//0:维修单  1:售后维修

@property (nonatomic, copy) NSString *limitTime;
@property (nonatomic, copy) NSString *repairUserId;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *isread;
//@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *orderDate;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *state;

@property (nonatomic, copy) NSString *handleDate;
@property (nonatomic, copy) NSString *cancelDate;
@property (nonatomic, copy) NSString *app_order_id;
@property (nonatomic, copy) NSString *order_type_id; //1:检测单
@end

NS_ASSUME_NONNULL_END
//http://192.168.2.114:8080/shopOrd/submitOrder?data={"user":{"userId":"1","addressId":"2"},"products":[{"id":"3"},{"id":"4"}],"order":{"caiId":"622921611967729664","privace":"258","send_type":"5"}}
