//
//  CSListDetaileModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSListDetaileModel : BaseModel
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *orderDate;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *hasElevator;
@property (nonatomic, copy) NSString *floorNumber;
@property (nonatomic, copy) NSString *orderTypeId;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *repairUserId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *hasCharges;
@property (nonatomic, copy) NSString *faultTypeId;
@property (nonatomic, copy) NSString *faultComment;
@property (nonatomic, copy) NSString *orderState;
@property (nonatomic, copy) NSString *orderAddress;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *orderDetailAdd;

@end

NS_ASSUME_NONNULL_END
