//
//  CSSHlistModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSSHlistModel : BaseModel
@property (nonatomic, copy) NSString *limitTime;
@property (nonatomic, copy) NSString *repairUserId;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *isread;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *orderDate;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *state;
@end

NS_ASSUME_NONNULL_END
