//
//  ZYAccessoryIsDamagedTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYAccessoryIsDamagedTableViewCell.h"

@implementation ZYAccessoryIsDamagedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 209);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(16, 32, 70, 14);
        pjmcTitLab.text = @"配件名称";
        pjmcTitLab.font = FontSize(14);
        pjmcTitLab.textColor = K666666;
        [bottomV addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        
        UIView *bgVieW = [[UIView alloc] init];
        bgVieW.frame = CGRectMake(34, pjmcTitLab.bottom + 8, KWIDTH-78, 81);
        bgVieW.layer.borderWidth = 1;
        bgVieW.layer.cornerRadius = 8;
        bgVieW.layer.masksToBounds = YES;
        bgVieW.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        [bottomV addSubview:bgVieW];
        
        UILabel *pjmcConLab = [[UILabel alloc] init];
        pjmcConLab.frame = CGRectMake(10, 10, KWIDTH-78, 56);
        pjmcConLab.font = FontSize(14);
        [bgVieW addSubview:pjmcConLab];
        self.pjmcConLab = pjmcConLab;
        
        UIButton *pjshsqzbBtn = [[UIButton alloc] init];
        pjshsqzbBtn.frame = CGRectMake(KWIDTH-140, bgVieW.bottom +8, 110, 32);
        [pjshsqzbBtn setImage:imgname(@"yisunhuai") forState:(UIControlStateNormal)];
        [pjshsqzbBtn addTarget:self action:@selector(leftClostBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:pjshsqzbBtn];
        self.pjshsqzbBtn = pjshsqzbBtn;
        //设置圆角
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:pjshsqzbBtn.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(8, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = pjshsqzbBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        pjshsqzbBtn.layer.mask = maskLayer;
        
    }
    return self;
}

//配件已损坏并申请质保按钮 点击事件
- (void)leftClostBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//配件已损坏?
    }
}


@end
