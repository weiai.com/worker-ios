//
//  CSnewchangeTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSnewchangeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rowLB;
@property (weak, nonatomic) IBOutlet UITextField *old_nameTF;

@property (weak, nonatomic) IBOutlet UITextField *ne_nameTF;
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);
        
@property (weak, nonatomic) IBOutlet UIButton *oldBut;
@property (weak, nonatomic) IBOutlet UIButton *nebut;

@end

NS_ASSUME_NONNULL_END
