//
//  CSnewJieGuoViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修详情

#import "CSnewJieGuoViewController.h"
#import "CSnewPTViewController.h"
#import "CSnewListDetalieViewController.h"
#import "CSDQCViewController.h"
#import "CSchangeErciViewController.h"
#import "ZOrderAfterSaleChangePartVC.h"//更改配件
#import "CSnewConViewController.h"
#import "ZYPersonServiceOrderDetailsVC.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"


@interface CSnewJieGuoViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UILabel *bgUPLable;
@property (nonatomic, strong) UILabel *bgdowLable;
@property (nonatomic, strong) NSString *eooroCodeStr;
@property (nonatomic, strong) UIView *bgview2;
@property (nonatomic, strong) UILabel *jieguoBg;
@property (nonatomic, strong) UIView *bgview3;
@end

@implementation CSnewJieGuoViewController

- (void)viewWillAppear:(BOOL)animated {
    [self requestData]; //售后单获取配件列表数据请求
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"售后维修详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self shwoBgview];
}

- (void)leftClick {
    {
        //判断是从哪个vc push过来的
        NSArray *vcsArray = self.navigationController.viewControllers;
        NSInteger vcCount = vcsArray.count;
        UIViewController *lastVC = vcsArray[vcCount-2];
        if ([lastVC isKindOfClass:[CSnewConViewController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
/***点击更换配件 调用数据请求
 *errorCode 0(有数据), 002(无数据)
 */
- (void)requestData {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(_model.id);
    [NetWorkTool POST:getRepPros param:param success:^(id dic) {
        KMyLog(@"售后维修详情 数据------ %@ ------", dic);
        CSSHmainLIstModel *model= [CSSHmainLIstModel mj_objectWithKeyValues:dic[@"data"]];
        self->_model.state = model.afterOrderState;
        self.eooroCodeStr = dic[@"errorCode"];
        [self showdetaile];
    } other:^(id dic) {
        self.eooroCodeStr = dic[@"errorCode"];
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)seleAction:(UIButton *)but{
    //选择处理结果
    NSInteger index = but.tag-93;
    if (index == 2) {
        self.bgUPLable.text = @"处理成功";
        self.bgdowLable.text = @"提醒客户下单给我！";
        [self queryActin:@"3"];
    } else if (index == 1){
        //更换配件按钮 点击跳转事件
        ZOrderAfterSaleChangePartVC *vc = [[ZOrderAfterSaleChangePartVC alloc] init];
        vc.apporderid = _model.app_order_id;
        vc.orderID = _model.id;
        vc.idStr = _model.orderId;
        vc.model = _model;
        
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        self.bgUPLable.text = @"处理成功，结束售后";
        self.bgdowLable.text = @"";
        [self queryActin:@"5"];
    }
}

-(void)queryActin:(NSString *)mybut{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param [@"id"] = NOTNIL(_model.id);
    param [@"state"] = NOTNIL(mybut);
    //状态值(1维修工知晓 2已处理（更换配件） 3已处理（去下单）)
    [NetWorkTool POST:changeErci param:param success:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgView];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showdetaile{
    
    if (self.scrollView) {
        [self.scrollView removeAllSubviews];
    }
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight+ 10, KWIDTH, 83)];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    bgview. backgroundColor = [UIColor whiteColor];
    
    UILabel *yuyue = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 40)];
    yuyue.font = KFontPingFangSCMedium(14);
    yuyue.textColor = K666666;
    yuyue.text = @"上门日期";
    [bgview addSubview:yuyue];
    
    UILabel *yuyuelb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, 0, KWIDTH-yuyue.right-16, 40)];
    yuyuelb.font = KFontPingFangSCMedium(14);
    yuyuelb.textColor = K666666;
    NSString *ssdfdsf = @" ";
    yuyuelb.text = [NSString stringWithFormat:@"%@ %@%@",_model.orderDate,ssdfdsf,_model.orderTime];
    [bgview addSubview:yuyuelb];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(16, yuyue.bottom, KWIDTH-32, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [bgview addSubview:line];
    //_model.account = @"jfdslafja;lsfjd;askfj;lajf;afafljal;sdfjsa;fkja;fja;ffsdfasfsaf";
    
    UILabel *liyou = [[UILabel alloc]initWithFrame:CGRectMake(16, line.bottom, 83, 40)];
    liyou.font = KFontPingFangSCMedium(14);
    liyou.textColor = K666666;
    liyou.text = @"申请理由";
    [bgview addSubview:liyou];
    CGFloat fff = [NSString heightWithWidth:KWIDTH-16-16-83 font:14 text:_model.account];
    
    UILabel *liyoulb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, line.bottom+12, KWIDTH-16-16-83, fff)];
    liyoulb.font = KFontPingFangSCMedium(14);
    liyoulb.textColor = K666666;
    
    if (_model.account.length > 0) {
        liyoulb.text = _model.account;
    } else {
        liyoulb.text = _model.afterAccount;
    }
    [bgview addSubview:liyoulb];
    liyoulb.numberOfLines = 0;
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(16, liyoulb.bottom, KWIDTH-32, 1)];
    [bgview addSubview:line1];
    bgview.height =line1.bottom+7;
    
    UIView *bgview2 = [[UIView alloc]initWithFrame:CGRectMake(0, bgview.bottom+10, KWIDTH, 43)];
    [self.scrollView addSubview:bgview2];
    bgview2. backgroundColor = [UIColor whiteColor];
    
    UILabel *fuwudan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 44, 43)];
    fuwudan.font = KFontPingFangSCMedium(14);
    fuwudan.textColor = K666666;
    fuwudan.text = @"服务单";
    [bgview2 addSubview:fuwudan];
    
    UIImageView *myinage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH-22, 15, 7, 13)];
    myinage.image = imgname(@"Cjinru");
    [bgview2 addSubview:myinage];
    bgview2.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(servicactionddd:)];
    [bgview2 addGestureRecognizer:ges];
    self.bgview2 = bgview2;
    
    //状态 -2已取消(维修工知晓后取消) -1已取消(直接取消) 0未接单 1维修工知晓 2已处理（更换配件） 3已处理（去下单） 4已处理（已下单）5售后结束
    if ([_model.state integerValue] == -2) { //维修工知晓后取消
        [self changeUIState];  //加载处理之后的状态UI

    } else if ([_model.state integerValue] == -1) { //已取消
        [self changeUIState];  //加载处理之后的状态UI

    } else if ([_model.state integerValue] == 0) { //未接单
        [self loadBgViewThreeViewUI]; //加载未处理之前的状态UI

    } else if ([_model.state integerValue] == 1) { //维修工知晓
        [self loadBgViewThreeViewUI]; //加载未处理之前的状态UI

    } else if ([_model.state integerValue] == 2) { //已处理（更换配件）
        [self changeUIState];  //加载处理之后的状态UI

    } else if ([_model.state integerValue] == 3) { //已处理（去下单）
        [self changeUIState];  //加载处理之后的状态UI

    } else if ([_model.state integerValue] == 4) { //已处理（已下单）
        [self changeUIState];  //加载处理之后的状态UI

    } else if ([_model.state integerValue] == 5) { //售后结束
        [self changeUIState];  //加载处理之后的状态UI

    }
}

- (void)loadBgViewThreeViewUI {
    if (self.jieguoBg) {
        [self.jieguoBg removeFromSuperview];
    }
    UIView *bgview3 = [[UIView alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+10, KWIDTH, 43)];
    [self.scrollView addSubview:bgview3];
    _bgview3 = bgview3;
    bgview3. backgroundColor = [UIColor whiteColor];
    
    CGFloat yyyy = KHEIGHT-_bgview2.bottom-10-290+kNaviHeight;
    CGFloat yyyyye = KHEIGHT-_bgview2.bottom-10;
    if (yyyy < 0) {
        bgview3.height = yyyyye-yyyy;
    } else {
        bgview3.height = yyyyye;
    }
    _scrollView.contentSize = CGSizeMake(KWIDTH, bgview3.bottom);
    
    UILabel *titleLB = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH, 22)];
    [bgview3 addSubview:titleLB];
    titleLB.textColor = [UIColor colorWithHexString:@"#232620"];
    titleLB.font = FontSize(16);
    titleLB.text = @"给出处理结果:";
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(16, titleLB.bottom+12, KWIDTH-32, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [bgview3 addSubview:line3];
    
    NSInteger indec = 0;
    if (indec == 0) {
        //选择处理结果
        NSArray *arr = @[@"不处理，结束售后",@"更换配件",@"客户下单给我"];
        
        for (int i = 0; i <arr.count; i++) {
            UIButton *mybut = [UIButton buttonWithType:(UIButtonTypeCustom)];
            [mybut setTitle:arr[i] forState:(UIControlStateNormal)];
            
            mybut.frame = CGRectMake(38, 102+(40+34)*i, KWIDTH-38-38, 40);
            mybut.layer.masksToBounds = YES;
            mybut.layer.cornerRadius = 4;
            mybut.layer.borderWidth= 1;
            mybut.layer.borderColor = zhutiColor.CGColor;
            [mybut setTitleColor:zhutiColor forState:(UIControlStateNormal)];
            [bgview3 addSubview:mybut];
            mybut.tag = 93+i;
            [mybut addTarget:self action:@selector(seleAction:) forControlEvents:(UIControlEventTouchUpInside)];
        }
    }
}

- (void)changeUIState {
    if (self.bgview3) {
        [self.bgview3 removeFromSuperview];
    }
    NSUInteger ind = [_model.state integerValue];
    
    if (ind == 0) {
        
        UIButton *quebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [quebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [quebut setTitle:@"已知晓，准时上门" forState:(UIControlStateNormal)];
        [quebut setBackgroundColor:zhutiColor];
        quebut.layer.masksToBounds = YES;
        quebut.layer.cornerRadius = 4;
        [quebut addTarget:self action:@selector(queryActin:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:quebut];
        
        [quebut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(28);
            make.right.offset(-28);
            make.height.offset(48);
            make.bottom.offset(-130);
        }];
        
    }else if (ind == 2) {
        //更换配件
        _jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:_jieguoBg];
        _jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [_jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [_jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        if (_model.handleDate.length > 0) {
            namedow.text = [_model.handleDate substringToIndex:16];
        } else {
            namedow.text = [NSString stringWithFormat:@"%@%@", _model.afterOrderDate, _model.afterOrderTime];
        }
        [nameLbBG addSubview:namedow];
        
    }else  if (ind == 3) {
        //更换配件
        _jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:_jieguoBg];
        _jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [_jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [_jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        if (_model.handleDate.length > 0) {
            namedow.text = [_model.handleDate substringToIndex:16];
        } else {
            namedow.text = [NSString stringWithFormat:@"%@%@", _model.afterOrderDate, _model.afterOrderTime];
        }
        
        [nameLbBG addSubview:namedow];
        name.text = @"客户下单给我";
        
    }else  if (ind == 5) {
        //更换配件
        _jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:_jieguoBg];
        _jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [_jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [_jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        
        if (_model.handleDate.length > 0) {
            namedow.text = [_model.handleDate substringToIndex:16];
        } else {
            namedow.text = [NSString stringWithFormat:@"%@%@", _model.afterOrderDate, _model.afterOrderTime];
        }
        
        [nameLbBG addSubview:namedow];
        name.text = @"不处理，售后结束";
        
    }else  if (ind == 4) {
        //更换配件
        _jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:_jieguoBg];
        _jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [_jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [_jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        if (_model.handleDate.length > 0) {
            namedow.text = [_model.handleDate substringToIndex:16];
        } else {
            namedow.text = [NSString stringWithFormat:@"%@%@", _model.afterOrderDate, _model.afterOrderTime];
        }
        
        [nameLbBG addSubview:namedow];
        name.text = @"客户下单给我";
        
    } else  if (ind == -2) {
        
        //更换配件
        _jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, _bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:_jieguoBg];
        _jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [_jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [_jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        if (_model.handleDate.length > 0) {
            namedow.text = [_model.handleDate substringToIndex:16];
        } else {
            namedow.text = [NSString stringWithFormat:@"%@%@", _model.afterOrderDate, _model.afterOrderTime];
        }
        
        [nameLbBG addSubview:namedow];
        name.text = @"已取消";
    }
}


/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = self.view.bounds;
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"发送成功";
    self.bgUPLable = UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"请准时上门处理！";
    self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    
    if (self.myblock) {
        self.myblock(@"");
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)servicactionddd:(UITapGestureRecognizer *)ges{
    
    if ([_model.user_type isEqualToString:@"0"]||[_model.usertype isEqualToString:@"0"]) { //个人订单
        //跳转到个人订单 服务单详情
        ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
        vc.orderID = self.model.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([_model.user_type isEqualToString:@"2"]||[_model.usertype isEqualToString:@"2"]) { //电器厂家订单
        //跳转到电器厂家 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
        vc.orderID = self.model.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
