//
//  ZYTJXPJHandLoseViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSHmainLIstModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZYTJXPJHandLoseViewController : BaseViewController
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);
@property(nonatomic, strong) NSString *orderStr;
@property(nonatomic, strong) NSString *apporderid;
@property(nonatomic, strong) NSString *repProId;
@property(nonatomic, strong) NSString *idStr;
@property(nonatomic, strong) CSSHmainLIstModel *CSSHmainLIstModel;

@end

NS_ASSUME_NONNULL_END
