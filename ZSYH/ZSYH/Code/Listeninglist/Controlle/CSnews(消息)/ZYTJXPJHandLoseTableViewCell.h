//
//  ZYTJXPJHandLoseTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmakeAccAddmodel.h"
#import "ZYAccessoriesDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^textEndHandle)(NSString *);

@interface ZYTJXPJHandLoseTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) ZYAccessoriesDetailsModel *myModel;
@property (nonatomic, copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UIButton *closeBtn; //删除按钮
@property (nonatomic, strong) UILabel *AddTitLab; //新增配件 标题
@property (nonatomic, strong) UILabel *pjmcTitLab; //配件名称 标题
@property (nonatomic, strong) UITextField *pjmcConTF; //配件名称 输入框
@property (nonatomic, strong) UIView *line; //底部灰线
@property (nonatomic,strong)textEndHandle textEndHandle;


@end

NS_ASSUME_NONNULL_END
