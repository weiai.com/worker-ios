//
//  ZYAccessoryIsDamagedTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYAccessoryIsDamagedTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property(nonatomic, strong) UILabel *pjmcTitLab; //配件名称 标题
@property(nonatomic, strong) UILabel *pjmcConLab; //配件名称 内容

@property(nonatomic, strong) UIButton *pjshsqzbBtn; //底部按钮

@end

NS_ASSUME_NONNULL_END
