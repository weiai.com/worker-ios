//
//  ZYReplacementPartsTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYReplacementPartsTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property(nonatomic, strong) UILabel *pjplTitLab; //配件品类 标题
@property(nonatomic, strong) UILabel *pjplConLab; //配件品类 内容

@property(nonatomic, strong) UILabel *pjmcTitLab; //配件名称 标题
@property(nonatomic, strong) UILabel *pjmcConLab; //配件名称 内容

@property(nonatomic, strong) UILabel *pjyxqTitLab; //有效期 标题
@property(nonatomic, strong) UILabel *pjyxqConLab; //有效期 内容

@property(nonatomic, strong) UILabel *pjbzqTitLab; //保质期 标题
@property(nonatomic, strong) UILabel *pjbzqConLab; //保质期 内容

@property(nonatomic, strong) UIView *line4;

@property(nonatomic, strong) UIButton *pjshsqzbBtn; //底部按钮
@end

NS_ASSUME_NONNULL_END
