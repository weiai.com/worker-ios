//
//  CancleViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSSHmainLIstModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CancleViewController : BaseViewController
@property(nonatomic,strong)CSSHmainLIstModel *model;

@end

NS_ASSUME_NONNULL_END
