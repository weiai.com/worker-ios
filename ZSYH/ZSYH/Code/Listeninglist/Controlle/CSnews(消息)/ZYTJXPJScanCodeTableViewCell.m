//
//  ZYTJXPJScanCodeTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTJXPJScanCodeTableViewCell.h"

@implementation ZYTJXPJScanCodeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 159);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UIButton *closeBtn = [[UIButton alloc] init];
        closeBtn.frame = CGRectMake(10, 0, 34, 38);
        [closeBtn setImage:imgname(@"cellCha") forState:(UIControlStateNormal)];
        [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:closeBtn];
        //self.closeBtn = closeBtn;
        
        UIButton *sacnBtn = [[UIButton alloc] init];
        sacnBtn.frame = CGRectMake(KWIDTH/2-67, 13, 134, 30);
        [sacnBtn setImage:imgname(@"组 3352") forState:(UIControlStateNormal)];
        [sacnBtn addTarget:self action:@selector(scanBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:sacnBtn];
        //self.sacnBtn = sacnBtn;
        
        UILabel *pjplTitLab = [[UILabel alloc] init];
        pjplTitLab.frame = CGRectMake(16, sacnBtn.bottom +13, 70, 14);
        pjplTitLab.textAlignment = NSTextAlignmentLeft;
        pjplTitLab.text = @"配件品类";
        pjplTitLab.font = FontSize(14);
        pjplTitLab.textColor = K666666;
        [bottomV addSubview:pjplTitLab];
        //self.rowLab = rowLab;
        
        UITextField *pjplConTF = [[UITextField alloc] init];
        pjplConTF.frame = CGRectMake(pjplTitLab.right + 8, sacnBtn.bottom +13, KWIDTH-36-80, 14);
        pjplConTF.placeholder = @"请扫码自动生成";
        //pjplConTF.font = FontSize(14);
        pjplConTF.font = KFontPingFangSCMedium(14);
        pjplConTF.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
        [bottomV addSubview:pjplConTF];
        self.pjplTF = pjplConTF;
        
        UIView *line1 = [[UIView alloc] init];
        line1.frame = CGRectMake(10, pjplTitLab.bottom +17, KWIDTH-20, 1);
        line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line1];
        
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(16, line1.bottom + 9, 70, 14);
        pjmcTitLab.text = @"配件名称";
        pjmcTitLab.font = FontSize(14);
        pjmcTitLab.textColor = K666666;
        [bottomV addSubview:pjmcTitLab];
        //self.nameTLab = nameTLab;
        
        UITextField *pjmcConTF = [[UITextField alloc] init];
        pjmcConTF.frame = CGRectMake(pjplTitLab.right + 8, line1.bottom + 9, KWIDTH-36-80, 14);
        pjmcConTF.placeholder = @"请扫码自动生成";
        pjmcConTF.font = KFontPingFangSCMedium(14);
        pjmcConTF.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
        [bottomV addSubview:pjmcConTF];
        self.pjmcTF = pjmcConTF;
        
        UILabel *line2 = [[UILabel alloc] init];
        line2.frame = CGRectMake(10, pjmcTitLab.bottom + 17, KWIDTH - 20, 1);
        line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line2];
        
    }
    return self;
}

//新增候保配件 扫码按钮 点击事件
- (void)scanBtnAction:(UIButton *)button {
    NSLog(@"您点击了 新增候保配件 扫码按钮");
    if (self.myblock) {
        self.myblock(0,@"");//扫一扫
    }
}

- (void)closeBtnAction:(UIButton *)button {
    NSLog(@"您点击了 右上角 删除按钮");
    if (self.myblock) {
        self.myblock(1,@"");//删除
    }
}

@end
