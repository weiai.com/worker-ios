//
//  ZYReplacementPartsTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYReplacementPartsTableViewCell.h"

@implementation ZYReplacementPartsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 209);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UILabel *pjplTitLab = [[UILabel alloc] init];
        pjplTitLab.frame = CGRectMake(16, 32, 70, 14);
        pjplTitLab.textAlignment = NSTextAlignmentLeft;
        pjplTitLab.text = @"配件品类";
        pjplTitLab.font = FontSize(14);
        pjplTitLab.textColor = K666666;
        [bottomV addSubview:pjplTitLab];
        self.pjplTitLab = pjplTitLab;
        
        UILabel *pjplConLab = [[UILabel alloc] init];
        pjplConLab.frame = CGRectMake(pjplTitLab.right + 8, 32, KWIDTH-36-80, 14);
        pjplConLab.text = @"洗衣机  滚筒  电机";
        pjplConLab.font = FontSize(14);
        [bottomV addSubview:pjplConLab];
        self.pjplConLab = pjplConLab;
        
        UIView *line1 = [[UIView alloc] init];
        line1.frame = CGRectMake(0, pjplTitLab.bottom +8, KWIDTH-20, 1);
        line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line1];
        
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(16, line1.bottom +8, 70, 14);
        pjmcTitLab.text = @"配件名称";
        pjmcTitLab.font = FontSize(14);
        pjmcTitLab.textColor = K666666;
        [bottomV addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        
        UILabel *pjmcConLab = [[UILabel alloc] init];
        pjmcConLab.frame = CGRectMake(pjplTitLab.right + 8, line1.bottom + 8, KWIDTH-36-80, 14);
        pjmcConLab.text = @"A5型";
        pjmcConLab.font = FontSize(14);
        [bottomV addSubview:pjmcConLab];
        self.pjmcConLab = pjmcConLab;
        
        UILabel *line2 = [[UILabel alloc] init];
        line2.frame = CGRectMake(0, pjmcTitLab.bottom + 8, KWIDTH - 20, 1);
        line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line2];
        
        UILabel *pjyxqTitLab = [[UILabel alloc] init];
        pjyxqTitLab.frame = CGRectMake(16, line2.bottom + 9, 70, 14);
        pjyxqTitLab.text = @"有效期";
        pjyxqTitLab.font = FontSize(14);
        pjyxqTitLab.textColor = K666666;
        [bottomV addSubview:pjyxqTitLab];
        self.pjyxqTitLab = pjyxqTitLab;
        
        UILabel *pjyxqConLab = [[UILabel alloc] init];
        pjyxqConLab.frame = CGRectMake(pjyxqTitLab.right+8, line2.bottom + 9, KWIDTH-36-80, 14);
        pjyxqConLab.textAlignment = NSTextAlignmentLeft;
        pjyxqConLab.text = @"2019-10-30 至 2020-10-30";
        pjyxqConLab.font = FontSize(14);
        [bottomV addSubview:pjyxqConLab];
        self.pjyxqConLab = pjyxqConLab;
        
        UILabel *line3 = [[UILabel alloc] init];
        line3.frame = CGRectMake(0, pjyxqTitLab.bottom + 9, KWIDTH - 20, 1);
        line3.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line3];
        
        UILabel *pjbzqTitLab = [[UILabel alloc] init];
        pjbzqTitLab.frame = CGRectMake(16, line3.bottom + 9, 70, 14);
        pjbzqTitLab.text = @"保质期";
        pjbzqTitLab.font = FontSize(14);
        pjbzqTitLab.textColor = K666666;
        [bottomV addSubview:pjbzqTitLab];
        self.pjbzqTitLab = pjbzqTitLab;
        
        UILabel *pjbzqConLab = [[UILabel alloc] init];
        pjbzqConLab.frame = CGRectMake(pjbzqTitLab.right+8, line3.bottom + 8, KWIDTH-36-80, 14);
        pjbzqConLab.text = @"3个月";
        pjbzqConLab.font = FontSize(14);
        [bottomV addSubview:pjbzqConLab];
        self.pjbzqConLab = pjbzqConLab;
        
        UILabel *line4 = [[UILabel alloc] init];
        line4.frame = CGRectMake(0, pjbzqTitLab.bottom +8, KWIDTH - 20, 1);
        line4.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line4];
        self.line4 = line4;
        
        UIButton *pjshsqzbBtn = [[UIButton alloc] init];
        pjshsqzbBtn.frame = CGRectMake(KWIDTH-183, line4.bottom +8, 153, 32);
        [pjshsqzbBtn setImage:imgname(@"组 3281") forState:(UIControlStateNormal)];
        [pjshsqzbBtn addTarget:self action:@selector(leftClostBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:pjshsqzbBtn];
        self.pjshsqzbBtn = pjshsqzbBtn;
        //设置圆角
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:pjshsqzbBtn.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(8, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = pjshsqzbBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        pjshsqzbBtn.layer.mask = maskLayer;
        
    }
    return self;
}

//配件已损坏并申请质保按钮 点击事件
- (void)leftClostBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//确认该候保配件已损坏?
    }
}

@end
