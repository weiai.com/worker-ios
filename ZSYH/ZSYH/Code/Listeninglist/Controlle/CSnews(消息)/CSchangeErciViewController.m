//
//  CSchangeErciViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSchangeErciViewController.h"
#import "CSmakeAccAddmodel.h"
#import "CSmakeAccontListcell.h"
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"
#import "SeleproductTypeViewController.h"
#import "CSchangeNewViewController.h"
#import "CSChangeTwoTableViewCell.h"
#import "HWScanViewController.h"
#import "ListenListViewController.h"

#import "CSnewchangeTableViewCell.h"
#import "CSnewsViewController.h"
#import "CSMaintenanceViewController.h"
#import "NSString+AttributedString.h"

@interface CSchangeErciViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property (weak, nonatomic) IBOutlet UILabel *orderLB;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UIView *tabFootView;
@property(nonatomic,strong)UILabel *tatolLB;
@property(strong, nonatomic)NSMutableArray *qrCodeStrArr;
@property(nonatomic,strong)UILabel *upShowLB;
@property(nonatomic,strong)UIView *bgViewsec;
@property(nonatomic,strong)UIView *bgViewsecjiedan;

@end

@implementation CSchangeErciViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"更换配件";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.qrCodeStrArr = [NSMutableArray array];
//    [self.rightbutton setImage:imgname(@"Csaoyosao") forState:(UIControlStateNormal)];
    
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerNib:[UINib nibWithNibName:@"CSnewchangeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSnewchangeTableViewCell"];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.orderLB.text = self.newid;
    
    [self showtabFootView];
    
    [self shwoBgviewsec];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)showtabFootView{
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    self.tatolLB = [[UILabel alloc]initWithFrame:CGRectMake(0, 36, KWIDTH, 18)];
    self.tatolLB .font = FontSize(18);
    self.tatolLB.textAlignment = NSTextAlignmentCenter;
    //self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥0.00"];
    [self.tabFootView addSubview: self.tatolLB ];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"提交" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 68, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    self.mytableView.tableFooterView = self.tabFootView;
}

-(void)tuisongAction:(UIButton *)but{
    //推送账单
    // 订单状态(0生成订单（已接单）//生成检测报告
    // ，1已检测，//生成账单
    //2生成账单（未支付），//去支付
    // 3已支付（已完成），//去评价
    //4已评价，
    //  5已取消)',//先不管
    //http://192.168.2.114:8094/cs_order/addRepProduct?data={"orderId":"001","products":[{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321","qrcode":"01"},{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321","qrcode":"01"},{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321",,"qrcode":"01"}],"peopleCost":"50","up_products":[{"price": "253", "type_id": "12313","type_name": "波片","count": "1","name": "波片","qrcode": "01","image","12313213543434"}]}
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] =_orderNumberStr;
    
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *oldmuarr = [NSMutableArray arrayWithCapacity:1];
    
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];
        
        //if ([model.isSaoyisao integerValue] == 2) {
        
            if (strIsEmpty(model.ne_type_id)) {
                ShowToastWithText(@"旧配件配件品类不能为空");
                return;
            }
            if (strIsEmpty(model.old_type_id)) {
                ShowToastWithText(@"旧配件配件名称不能为空");
                return;
            }
            
            if (strIsEmpty(model.ne_name)) {
                ShowToastWithText(@"新配件配件品类不能为空");
                return;
            }
            if (strIsEmpty(model.old_name)) {
                ShowToastWithText(@"新配件配件名称不能为空");
                return;
            }
            
        //设计更换配件 暂不操作
            
            NSMutableDictionary *oldmuDic = [NSMutableDictionary dictionaryWithCapacity:1];
            
            muDic[@"price"] = model.total_price;
            muDic[@"type_id"] = model.ne_type_id;
            muDic[@"count"] = @"1";
            muDic[@"name"] = model.ne_name;
            muDic[@"qrcode"] = model.ne_qrCode;
            [muarr addObject:muDic];
            
            oldmuDic[@"price"] = model.old_price;
            oldmuDic[@"type_id"] = model.old_type_id;
            oldmuDic[@"count"] = @"1";
            oldmuDic[@"name"] = model.old_name;
            oldmuDic[@"qrcode"] = model.old_qrCode;
            [oldmuarr addObject:oldmuDic];
        //}
    }
    
    param[@"products"] =muarr;
    param[@"up_products"] =oldmuarr;
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    allDic[@"data"] = json;
    [NetWorkTool POST:weixuishouhouchenpru param:allDic success:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];

        /*ZSTipsView *view = [ZSTipsView new];
        view.upLable.text = @"信息提交成功";
        view.btnBlock = ^{

            if (self.mybleoc) {
                self.mybleoc(@"");
            }
            [self returnaction];
        };
        [view show];*/
        
//        ShowToastWithText(@"添加成功");
//        if (self.mybleoc) {
//            self.mybleoc(@"");
//        }
//        [self returnaction];
    
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)returnaction{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSnewsViewController class]]) {
            CSnewsViewController *vc =(CSnewsViewController *)controller;
            [self.navigationController popToViewController:vc animated:YES];
            return ;
        }
    }
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSMaintenanceViewController class]]) {
            CSMaintenanceViewController *vc =(CSMaintenanceViewController *)controller;
            [self.navigationController popToViewController:vc animated:YES];
            return ;
        }
    }
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[ListenListViewController class]]) {
            ListenListViewController *vc =(ListenListViewController *)controller;
            [self.navigationController popToViewController:vc animated:YES];
            return ;
        }
    }
}

- (void)rightClick{
//    HWScanViewController *vc = [[HWScanViewController alloc]init];
//    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
//    [self presentViewController:na animated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 345;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    CSnewchangeTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:@"CSnewchangeTableViewCell" forIndexPath:indexPath];
        mycell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!strIsEmpty(model.old_type_name)) {
        [mycell.oldBut setTitleColor:K333333 forState:(UIControlStateNormal)];
        [mycell.oldBut setTitle:model.old_type_name forState:(UIControlStateNormal)];

    } else {
        [mycell.oldBut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
        [mycell.oldBut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
    }
    if (!strIsEmpty(model.ne_type_name)) {
        [mycell.nebut setTitleColor:K333333 forState:(UIControlStateNormal)];
        [mycell.nebut setTitle:model.ne_type_name forState:(UIControlStateNormal)];

    } else {
        [mycell.nebut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
        [mycell.nebut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
    }
   
    mycell.old_nameTF.text = model.old_name;
    mycell.ne_nameTF.text = model.ne_name;
    mycell.rowLB.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
//    mycell.old_nameTF.userInteractionEnabled = YES;
//    mycell.oldBut.userInteractionEnabled = YES;
//    mycell.ne_nameTF.userInteractionEnabled = YES;
//    mycell.nebut.userInteractionEnabled = YES;
    if ([model.old_isSaoyisao integerValue] == 1) {
        mycell.old_nameTF.userInteractionEnabled = NO;
        mycell.oldBut.userInteractionEnabled = NO;
    } else {
        mycell.old_nameTF.userInteractionEnabled = YES;
        mycell.oldBut.userInteractionEnabled = YES;
    }
    
    if ([model.ne_isSaoyisao integerValue] == 1){
        mycell.ne_nameTF.userInteractionEnabled = NO;
        mycell.nebut.userInteractionEnabled = NO;
    } else {
        mycell.ne_nameTF.userInteractionEnabled = YES;
        mycell.nebut.userInteractionEnabled = YES;
    }
    
//NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([self.mytableView numberOfRowsInSection:0]-1) inSection:0];
//[self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    kWeakSelf;
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
      
        switch (ind) {
            case 0:
            {//上边的扫一扫
                HWScanViewController *vc = [[HWScanViewController alloc]init];
                vc.myblock = ^(NSString *str) {
                    [self requesrtWith:str withind:indexPath.row isup:1];
                };
                UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                [self presentViewController:na animated:YES completion:nil];
            }
                break;
            case 1:
            {//上边的选择品类
//                SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
//                seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
//                    model.old_type_name = str;
//                    model.old_type_id = idstr;
//                    [weakSelf.mytableView reloadData];
//                };
//                [self.navigationController pushViewController:seleVc animated:YES];
            }
                break;
            case 2:
            {//上边的输入名称
                model.old_name = str;
                [weakSelf.mytableView reloadData];
            }
                break;
            case 3:
            {//下边的扫一扫
                HWScanViewController *vc = [[HWScanViewController alloc]init];
                vc.myblock = ^(NSString *str) {
                    [self requesrtWith:str withind:indexPath.row isup:0];
                };
                UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                [self presentViewController:na animated:YES completion:nil];
            }
                break;
            case 4:
            {//上边的选择品类
//                SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
//                seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
//                    model.ne_type_name= str;
//                    model.ne_type_id = idstr;
//                    [weakSelf.mytableView reloadData];
//                };
//                [self.navigationController pushViewController:seleVc animated:YES];
            }
                break;
            case 5:
            {//下边的输入名称
                model.ne_name = str;
                [weakSelf.mytableView reloadData];
            }
                break;
            case 6:
            {///删除
                [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                [weakSelf.mytableView reloadData];
            }
                break;
            default:
                break;
        }
    };
        return mycell;
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind isup:(NSInteger )index{
    
    if (self.qrCodeStrArr.count == 0) {
        [self.qrCodeStrArr addObject:strr];
    } else {
//        NSMutableArray *tempArr = self.qrCodeStrArr;
        NSArray * tempArr = [NSArray arrayWithArray: self.qrCodeStrArr];
        for (NSString *str in tempArr) {
            if ([str isEqualToString:strr]) {
                ShowToastWithText(@"该配件二维码已被扫描");
                return;
            }else {
                [self.qrCodeStrArr addObject:strr];
            }
        }
    }
    
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        
        
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = weakSelf.mydateSource[ind];
        if (index == 1) {
            //旧配件
            mymodel.old_type_name =[mudic objectForKey:@"type_name"];
            mymodel.old_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.old_name =[mudic objectForKey:@"parts_name"];
            mymodel.old_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.old_isSaoyisao = @"1";

        }else{
            //新配件
            mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
            mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.ne_name =[mudic objectForKey:@"parts_name"];
            mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.ne_isSaoyisao = @"1";
        }
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind{
    kWeakSelf;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
        mymodel.qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.type_name =[mudic objectForKey:@"type_name"];
        mymodel.price =[mudic objectForKey:@"price"];
        mymodel.type_id =[mudic objectForKey:@"type_id"];
        mymodel.count =[mudic objectForKey:@"count"];
        mymodel.name =[mudic objectForKey:@"name"];
        mymodel.isSaoyisao = @"0";
        
        [weakSelf.mydateSource removeObjectAtIndex:ind];
        [weakSelf.mydateSource insertObject:mymodel atIndex:ind];
        [weakSelf.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}

- (IBAction)addaction:(UIButton *)sender {

    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    [self.mydateSource addObject:model];
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];

    
}

- (IBAction)rightAddaction:(UIButton *)sender {
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
}

/**
 弹出框的背景图  提交成功
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"信息提交成功";
    
//    NSMutableAttributedString *dowStr = [NSString getAttributedStringWithOriginalString:@"*可在我的订单查看此单" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
//    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom + 16, KWIDTH-44, 21)];
//    [whiteBGView addSubview:DowLable];
//    DowLable.font = FontSize(16);
//    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
//    DowLable.attributedText = dowStr;
//    DowLable.textAlignment = NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    //[self.navigationController popViewControllerAnimated:YES];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
