//
//  CSnewJieGuoViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSSHmainLIstModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewJieGuoViewController : BaseViewController
@property(nonatomic,strong)CSSHmainLIstModel *model;
@property(nonatomic,copy)void (^myblock)(NSString *str);

@end

NS_ASSUME_NONNULL_END
