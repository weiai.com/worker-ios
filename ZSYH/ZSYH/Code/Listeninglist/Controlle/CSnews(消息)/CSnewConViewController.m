//
//  CSnewConViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修详情

#import "CSnewConViewController.h"
#import "CSnewPTViewController.h"
#import "CSnewListDetalieViewController.h"
#import "CSnewJieGuoViewController.h"
#import "ZYPersonServiceOrderDetailsVC.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"


@interface CSnewConViewController ()
@property (nonatomic, strong) UIView *bgView;

@end

@implementation CSnewConViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"售后维修详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self showdetaile];
    [self shwoBgview];
    
    //1  5 2  3
    //.更换配件2
    //2 去更换配件
    //3 客户下单给我
    //4 更换配件
    //5 不处理
}

-(void)showdetaile{
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight+ 10, KWIDTH, 83)];
    [self.view addSubview:bgview];
    bgview. backgroundColor = [UIColor whiteColor];
    
    UILabel *yuyue = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 40)];
    yuyue.font = KFontPingFangSCMedium(14);
    yuyue.textColor = K666666;
    yuyue.text = @"上门日期";
    [bgview addSubview:yuyue];
    
    UILabel *yuyuelb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, 0, KWIDTH-yuyue.right-16, 40)];
    yuyuelb.font = KFontPingFangSCMedium(14);
    yuyuelb.textColor = K666666;
    NSString *ssdfdsf = @" ";
    yuyuelb.text = [NSString stringWithFormat:@"%@ %@%@",_model.orderDate,ssdfdsf,_model.orderTime];
    [bgview addSubview:yuyuelb];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(16, yuyue.bottom, KWIDTH-32, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [bgview addSubview:line];

    UILabel *liyou = [[UILabel alloc]initWithFrame:CGRectMake(16, line.bottom, 83, 40)];
    liyou.font = KFontPingFangSCMedium(14);
    liyou.textColor = K666666;
    liyou.text = @"申请理由";
    [bgview addSubview:liyou];
    
    CGFloat fff = [NSString heightWithWidth:KWIDTH-16-16-83 font:14 text:_model.account];

    UILabel *liyoulb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, line.bottom+12, KWIDTH-yuyue.right-16, fff)];
    liyoulb.font = KFontPingFangSCMedium(14);
    liyoulb.textColor = K666666;
    liyoulb.text = _model.account;
    liyoulb.numberOfLines = 0;
    [bgview addSubview:liyoulb];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(16, liyoulb.bottom, KWIDTH-32, 1)];
    //line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [bgview addSubview:line1];
    bgview.height =line1.bottom+7;
    
    UIView *bgview2 = [[UIView alloc]initWithFrame:CGRectMake(0, bgview.bottom+10, KWIDTH, 43)];
    [self.view addSubview:bgview2];
    bgview2. backgroundColor = [UIColor whiteColor];
    
    UILabel *fuwudan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 44, 43)];
    fuwudan.font = KFontPingFangSCMedium(14);
    fuwudan.textColor = K666666;
    fuwudan.text = @"服务单";
    [bgview2 addSubview:fuwudan];
    
    UIImageView *myinage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH-22, 15, 7, 13)];
    myinage.image = imgname(@"Cjinru");
    [bgview2 addSubview:myinage];
    
    bgview2.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(servicactionddd:)];
    [bgview2 addGestureRecognizer:ges];
    
    NSUInteger ind = [_model.state integerValue];

    if (ind == 0) {

    UIButton *quebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [quebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [quebut setTitle:@"已知晓，准时上门" forState:(UIControlStateNormal)];
    [quebut setBackgroundColor:zhutiColor];
    quebut.layer.masksToBounds = YES;
    quebut.layer.cornerRadius = 4;
    [quebut addTarget:self action:@selector(queryActin:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:quebut];

    [quebut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(28);
        make.right.offset(-28);
        make.height.offset(48);
        make.bottom.offset(-130);
    }];
  
    }else if (ind == 2) {
        //更换配件
        UILabel *jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:jieguoBg];
        jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [jieguoBg addSubview:chuli];

        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [jieguoBg addSubview:nameLbBG];

        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        namedow.text = [_model.handleDate substringToIndex:16];
        [nameLbBG addSubview:namedow];
        
    }else  if (ind == 3) {
        //更换配件
        UILabel *jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:jieguoBg];
        jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        namedow.text = [_model.handleDate substringToIndex:16];

        [nameLbBG addSubview:namedow];
        name.text = @"客户下单给我";
        
    }else  if (ind == 5) {
        //更换配件
        UILabel *jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:jieguoBg];
        jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        //namedow.text = _model.createDate;
        namedow.text = [_model.handleDate substringToIndex:16];

        [nameLbBG addSubview:namedow];
        name.text = @"不处理，售后结束";
        
    }else  if (ind == 4) {
        //更换配件
        UILabel *jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:jieguoBg];
        jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        //namedow.text = _model.createDate;
        namedow.text = [_model.handleDate substringToIndex:16];

        [nameLbBG addSubview:namedow];
        name.text = @"客户下单给我";
        
    } else  if (ind == -2) {
        
        //更换配件
        UILabel *jieguoBg = [[UILabel alloc]initWithFrame:CGRectMake(0, bgview2.bottom+20, KWIDTH, KHEIGHT)];
        [self.view addSubview:jieguoBg];
        jieguoBg.backgroundColor = [UIColor whiteColor];
        UILabel  *chuli = [[UILabel alloc]initWithFrame:CGRectMake(16, 11, 100, 22)];
        chuli.font = FontSize(16);
        chuli.textColor = [UIColor colorWithHexString:@"#232620"];
        chuli.text = @"处理结果:";
        [jieguoBg addSubview:chuli];
        
        UILabel *nameLbBG = [[UILabel  alloc]initWithFrame:CGRectMake(16, chuli.bottom+15, KWIDTH-32, 72)];
        nameLbBG.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [jieguoBg addSubview:nameLbBG];
        
        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(16, 12, KWIDTH-32-32, 22)];
        name.font = FontSize(16);
        name.textColor = zhutiColor;
        name.text = @"更换配件";
        [nameLbBG addSubview:name];
        
        UILabel *namedow = [[UILabel alloc]initWithFrame:CGRectMake(16, name.bottom+6, KWIDTH-32-32, 22)];
        namedow.font = FontSize(16);
        namedow.textColor = zhutiColor;
        //namedow.text = _model.createDate;
        namedow.text = [_model.handleDate substringToIndex:16];

        [nameLbBG addSubview:namedow];
        name.text = @"已取消";
    }
}

-(void)queryActin:(UIButton *)mybut{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param [@"id"] =NOTNIL(_model.id);
    param [@"state"] =@"1";

    [NetWorkTool POST:changeErci param:param success:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgView];

    } other:^(id dic) {

    } fail:^(NSError *error) {

    } needUser:YES];
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = self.view.bounds;
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"发送成功";
    //self.bgUPLable =UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"请准时上门处理！";
    //self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    if (self.myblock) {
        self.myblock(@"");
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)servicactionddd:(UITapGestureRecognizer *)ges{
    
    if ([_model.user_type isEqualToString:@"0"]||[_model.usertype isEqualToString:@"0"]) { //个人订单
        //跳转到个人订单 服务单详情
        ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
        vc.orderID = self.model.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([_model.user_type isEqualToString:@"2"]||[_model.usertype isEqualToString:@"2"]) { //电器厂家订单
        //跳转到电器厂家 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
        vc.orderID = self.model.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    }

}
@end
