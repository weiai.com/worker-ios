//
//  ZYTJXPJHandLoseTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTJXPJHandLoseTableViewCell.h"

@implementation ZYTJXPJHandLoseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    frame.origin.y += 10;
    frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 132);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        UIButton *closeBtn = [[UIButton alloc] init];
        closeBtn.frame = CGRectMake(10, 10, 34, 38);
        [closeBtn setImage:imgname(@"cellCha") forState:(UIControlStateNormal)];
        [closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:closeBtn];
        self.closeBtn = closeBtn;
    
        UILabel *AddTitLab = [[UILabel alloc] init];
        AddTitLab.frame = CGRectMake(KWIDTH/2-67, 20, 134, 30);
        AddTitLab.textAlignment = NSTextAlignmentCenter;
        AddTitLab.text = @"新增配件";
        AddTitLab.font = FontSize(16);
        AddTitLab.textColor = K666666;
        [bottomV addSubview:AddTitLab];
        self.AddTitLab = AddTitLab;
        
        UILabel *pjmcTitLab = [[UILabel alloc] init];
        pjmcTitLab.frame = CGRectMake(16, closeBtn.bottom + 21, 70, 16);
        pjmcTitLab.text = @"配件名称:";
        pjmcTitLab.font = FontSize(14);
        pjmcTitLab.textColor = K666666;
        [bottomV addSubview:pjmcTitLab];
        self.pjmcTitLab = pjmcTitLab;
        
        UITextField *pjmcConTF = [[UITextField alloc] init];
        pjmcConTF.frame = CGRectMake(pjmcTitLab.right + 8, closeBtn.bottom + 21, KWIDTH-36-80, 16);
        pjmcConTF.placeholder = @"请输入配件名称";
        pjmcConTF.font = FontSize(14);
        pjmcConTF.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
        [bottomV addSubview:pjmcConTF];
        pjmcConTF.delegate = self;
        self.pjmcConTF = pjmcConTF;
        
        UIView *line = [[UIView alloc] init];
        line.frame = CGRectMake(10, pjmcTitLab.bottom +17, KWIDTH-20, 1);
        line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line];
    }
    return self;
}
#pragma UITextFieldDelegate
- (void)textFieldEditChanged:(UITextField *)textField
{
    if (self.textEndHandle) {
        self.textEndHandle(textField.text);
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.textEndHandle) {
        
        self.textEndHandle(textField.text);
    }
}
- (void)closeBtnAction:(UIButton *)button {
    NSLog(@"您点击了 右上角 删除按钮");
    if (self.myblock) {
        self.myblock(0, @"");
    }
}

@end
