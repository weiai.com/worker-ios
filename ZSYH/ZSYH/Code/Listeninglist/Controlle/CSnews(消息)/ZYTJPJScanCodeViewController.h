//
//  ZYTJPJScanCodeViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSHmainLIstModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZYTJPJScanCodeViewController : BaseViewController
@property(nonatomic, copy)void (^myblock)(NSUInteger ind,NSString *str);
@property(nonatomic, strong) NSString *orderId;
@property(nonatomic, strong) NSString *repProId;
@property(nonatomic, strong) NSString *idStr;
@property(nonatomic, strong) CSSHmainLIstModel *model;
@property(nonatomic, strong) NSString *apporderid;

@end

NS_ASSUME_NONNULL_END
