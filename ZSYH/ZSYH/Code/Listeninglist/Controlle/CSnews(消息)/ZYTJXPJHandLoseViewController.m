//
//  ZYTJXPJHandLoseViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTJXPJHandLoseViewController.h"
#import "ZYTJXPJHandLoseTableViewCell.h"
#import "CSmakeAccAddmodel.h"
#import "HWScanViewController.h"
#import "ZYHouBaoOrderDetailViewController.h"
#import "ZYPersonServiceOrderDetailsVC.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"

@interface ZYTJXPJHandLoseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) NSMutableSet <NSString *>*saoYiSaoStrSet;
@property (nonatomic, strong) UIView *bgViewsec;

@end

@implementation ZYTJXPJHandLoseViewController

- (NSMutableSet <NSString *>*)saoYiSaoStrDic {
    if (!_saoYiSaoStrSet) {
        _saoYiSaoStrSet = [NSMutableSet setWithCapacity:1];
    }
    return _saoYiSaoStrSet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加新配件";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [self showdetaile];
    [self shwoBgviewsec];
}

-(void)showdetaile {
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight+ 10, KWIDTH, 55)];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    bgview.backgroundColor = [UIColor whiteColor];
    
    UILabel *dingdan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 55)];
    dingdan.font = FontSize(16);
    dingdan.textColor = K666666;
    dingdan.text = @"订单编号:";
    [bgview addSubview:dingdan];
    
    UILabel *dingdanlb = [[UILabel alloc]initWithFrame:CGRectMake(99, 0, KWIDTH/2, 55)];
    dingdanlb.font = FontSize(16);
    dingdanlb.textColor = K666666;
    dingdanlb.text = self.apporderid;
    [bgview addSubview:dingdanlb];
    
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(dingdanlb.right, 0, 100, 55);
    [rightBtn setImage:imgname(@"newaddjinru") forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -40)];
    [bgview addSubview:rightBtn];
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rightBtnAction:)];
    bgview.userInteractionEnabled =  YES;
    [bgview addGestureRecognizer:ges];
    
    UIView *btview = [[UIView alloc]initWithFrame:CGRectMake(0,bgview.bottom +2, KWIDTH, 55)];
    [self.scrollView addSubview:btview];
    [self.view addSubview:self.scrollView];
    btview.backgroundColor = [UIColor whiteColor];
    
    UIButton *addPjBtn = [[UIButton alloc] init];
    addPjBtn.frame = CGRectMake(56, 2, KWIDTH - 56 -55, 48);
    [addPjBtn setImage:imgname(@"组 3181") forState:(UIControlStateNormal)];
    [addPjBtn addTarget:self action:@selector(addNewPjBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btview addSubview:addPjBtn];
    
    UILabel *tipsLab = [[UILabel alloc]initWithFrame:CGRectMake(16, btview.bottom +10, KWIDTH -32, 15)];
    tipsLab.font = FontSize(14);
    tipsLab.textColor = K666666;
    tipsLab.text = @"请您输入新配件的名称";
    tipsLab.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    tipsLab.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:tipsLab];
    [self.view addSubview:self.scrollView];
    tipsLab.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, tipsLab.bottom +10, KWIDTH, KHEIGHT - kNaviHeight -20-55-56);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    //新增配件 手输
    [_mytableView registerClass:[ZYTJXPJHandLoseTableViewCell class] forCellReuseIdentifier:@"ZYTJXPJHandLoseTableViewCell"];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    [_scrollView addSubview:_mytableView];
}

-(void)showtabFootView {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"提交" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 48, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _mytableView.tableFooterView = self.tabFootView;
}

#pragma mark - 提交
-(void)tuisongAction:(UIButton *)but{
    //推送账单
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    //param[@"orderId"] = NOTNIL(self.orderStr);
    param[@"orderId"] = NOTNIL(self.idStr);
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    if (_mydateSource.count == 0) {
        ShowToastWithText(@"请先添加配件");
        return;
    }
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];
        
        if (strIsEmpty(model.name)) {
            ShowToastWithText(@"请输入配件名称");
            return;
        }

        muDic[@"price"] = @"0";
        muDic[@"type_id"] = NOTNIL(model.type_id);
        muDic[@"count"] = NOTNIL(model.count);
        muDic[@"name"] = NOTNIL(model.name);
        muDic[@"productId"] = NOTNIL(model.type_id);
        muDic[@"qrcode"] = @"";
        muDic[@"product_model"] = @"1";
        [muarr addObject:muDic];
    }
    param[@"products"] = muarr;
    param[@"repProId"] = NOTNIL(self.repProId);

    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] = json;
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool POST:updateRepProduct param:allDic success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"请求成功 %@", dic);
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        //weakSelf.bgViewsec.center = self.view.center;
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

- (void)rightBtnAction:(UIButton *)button {

    if ([_CSSHmainLIstModel.user_type isEqualToString:@"0"]||[_CSSHmainLIstModel.usertype isEqualToString:@"0"]) { //个人订单
        //跳转到个人订单 服务单详情
        ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
        vc.orderID = _CSSHmainLIstModel.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([_CSSHmainLIstModel.user_type isEqualToString:@"2"]||[_CSSHmainLIstModel.usertype isEqualToString:@"2"]) { //电器厂家订单
        //跳转到电器厂家 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
        vc.orderID = _CSSHmainLIstModel.orderId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    NSLog(@"您点击了最上边的按钮 %@", self.orderStr);
}

- (void)addNewPjBtnAction:(UIButton *)button {
    
    [self showtabFootView]; //点击添加配件按钮之后 显示提交按钮
    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
    
    mymodel.row = @"";              mymodel.type_name = @"";
    mymodel.type_id = @"";          mymodel.name = @"";
    mymodel.price = @"";            mymodel.count = @"1";
    mymodel.total_price = @"";      mymodel.qrCode = @"";
    mymodel.isSaoyisao = @"2";      mymodel.typeProduct = @"2";
    mymodel.old_type_name = @"";    mymodel.old_type_id = @"";
    mymodel.old_name = @"";         mymodel.old_price = @"1";
    mymodel.old_count = @"1";       mymodel.old_total_price = @"";
    
    mymodel.old_isSaoyisao = @"2";  mymodel.ne_type_name = @"";
    mymodel.ne_type_id = @"";       mymodel.ne_name = @"";
    mymodel.ne_price = @"2";        mymodel.ne_count = @"";
    mymodel.ne_total_price = @"";   mymodel.ne_qrCode = @"";
    mymodel.ne_isSaoyisao = @"2";
    
    model.typeProduct = @"2";
    model.ne_count = @"1";
    model.old_count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    NSLog(@"您点击了 添加新配件按钮");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 159;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",(long)indexPath.section, (long)indexPath.row];
    //通过唯一标识创建Cell实例
    ZYTJXPJHandLoseTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYTJXPJHandLoseTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    __weak typeof(mycell) weakCell = mycell;
    
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if (model.name) {
        mycell.pjmcConTF.text = model.name;
    } else {
        mycell.pjmcConTF.text = @"";
    }
    
    mycell.textEndHandle = ^(NSString *text) {
        weakCell.pjmcConTF.text = text;
        model.name = text;
    };
    mycell.pjmcConTF.textColor = K666666;
    kWeakSelf;
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
        
        switch (ind) {
            case 0: {//删除
                [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                [weakSelf.mytableView reloadData];
                
                CGFloat ffftotle = 0;
                for (CSmakeAccAddmodel *model in self.mydateSource) {
                    CGFloat motltlt = [model.total_price floatValue];
                    ffftotle = ffftotle +motltlt;
                }
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //KMyLog(@"您点击了第 %ld 个分区第 %ld行", indexPath.section, indexPath.row);
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"信息已提交";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.numberOfLines = 0;
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"可在我的订单或候保产品订单页面中\n查看配件详情";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-14);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}
@end
