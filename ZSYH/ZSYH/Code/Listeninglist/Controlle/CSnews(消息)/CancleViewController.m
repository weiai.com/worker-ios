//
//  CancleViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修详情

#import "CancleViewController.h"
#import "CSWXPersonDetaileViewController.h"

@interface CancleViewController ()
@property(nonatomic,strong)UIScrollView *scrollView;

@end

@implementation CancleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"售后维修详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self showdetaile];
}

-(void)showdetaile{
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    UILabel *firstbgLable = [[UILabel alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 44)];
    firstbgLable.backgroundColor = [UIColor colorWithHexString:@"#979797"];
    [self.scrollView addSubview:firstbgLable];
    UILabel *firstbgLabletext = [[UILabel alloc]initWithFrame:CGRectMake(16, kNaviHeight, KWIDTH, 44)];
    firstbgLabletext.font = FontSize(16);
    firstbgLabletext.text = @"客户已取消";
    [self.scrollView addSubview:firstbgLabletext];

    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,firstbgLable.bottom, KWIDTH, 83)];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    bgview. backgroundColor = [UIColor whiteColor];
    
    UILabel *yuyue = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 40)];
    yuyue.font = KFontPingFangSCMedium(14);
    yuyue.textColor = K666666;
    yuyue.text = @"上门日期";
    [bgview addSubview:yuyue];
    
    UILabel *yuyuelb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, 0, KWIDTH-100, 40)];
    yuyuelb.font = KFontPingFangSCMedium(14);
    yuyuelb.textColor = K666666;
    NSString *ssdfdsf = @" ";
    yuyuelb.text = [NSString stringWithFormat:@"%@ %@%@",_model.orderDate,ssdfdsf,_model.orderTime];
    [bgview addSubview:yuyuelb];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(16, yuyue.bottom, KWIDTH-32, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [bgview addSubview:line];
    
    UILabel *liyou = [[UILabel alloc]initWithFrame:CGRectMake(16, line.bottom, 83, 40)];
    liyou.font = KFontPingFangSCMedium(14);
    liyou.textColor = K666666;
    liyou.text = @"申请理由";
    [bgview addSubview:liyou];
    
    CGFloat fff = [NSString heightWithWidth:KWIDTH-16-16-83 font:14 text:_model.account];
    
    UILabel *liyoulb = [[UILabel alloc]initWithFrame:CGRectMake(yuyue.right, line.bottom+12, KWIDTH-16-16-83, fff)];
    liyoulb.font = KFontPingFangSCMedium(14);
    liyoulb.textColor = K666666;
    liyoulb.text = _model.account;
    [bgview addSubview:liyoulb];
    liyoulb.numberOfLines = 0;
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(16, liyoulb.bottom, KWIDTH-32, 1)];
    [bgview addSubview:line1];
    
    bgview.height = line1.bottom+7;
    
    UIView *bgview2 = [[UIView alloc]initWithFrame:CGRectMake(0, bgview.bottom+10, KWIDTH, 43)];
    [self.scrollView addSubview:bgview2];
    bgview2. backgroundColor = [UIColor whiteColor];
    
    UILabel *fuwudan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 44, 43)];
    fuwudan.font = KFontPingFangSCMedium(14);
    fuwudan.textColor = K666666;
    fuwudan.text = @"服务单";
    [bgview2 addSubview:fuwudan];
    
    UIImageView *myinage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH-22, 15, 7, 13)];
    myinage.image = imgname(@"Cjinru");
    [bgview2 addSubview:myinage];
    bgview2.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(servicactionddd:)];
    [bgview2 addGestureRecognizer:ges];
    
    UIView *bgview3 = [[UIView alloc]initWithFrame:CGRectMake(0, bgview2.bottom+10, KWIDTH, 43)];
    [self.scrollView addSubview:bgview3];

}

-(void)servicactionddd:(UITapGestureRecognizer *)ges{
    
    CSWXPersonDetaileViewController *detVC1 =[[CSWXPersonDetaileViewController alloc]init];
    detVC1.orderID = _model.orderId;
    [self.navigationController pushViewController:detVC1 animated:YES];
   
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}
@end
