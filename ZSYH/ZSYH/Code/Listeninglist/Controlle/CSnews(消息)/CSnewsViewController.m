//
//  CSnewsViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//  我的消息

#import "CSnewsViewController.h"
#import "CSnewsModel.h"
#import "CSnewsTableViewCell.h"
#import "CSnewPTViewController.h"
#import "CSnewListDetalieViewController.h"
#import "CSDQCViewController.h"
#import "CSnewConViewController.h"
#import "CSnewJieGuoViewController.h"
#import "CSlistModel.h"
#import "CancleViewController.h"
#import "ZYPersonServiceOrderDetailsVC.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"

@interface CSnewsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,assign)NSInteger index;
@end

@implementation CSnewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"我的消息";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    //[self.rightbutton setImage:imgname(@"C添加") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    [self.myTableView reloadData];
    self.index = 1;
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_myTableView) {
    [self request];
    }
}

-(void)request{
    
    [self.mydateSource removeAllObjects];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [NetWorkTool POST:shouHouList param:param success:^(id dic) {
        [self.mydateSource removeAllObjects];
        KMyLog(@"售后维修%@",dic);
        self.mydateSource = [CSSHmainLIstModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.myTableView reloadData];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 122;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSnewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSnewsTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CSSHmainLIstModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    cell.myblcok = ^(NSString * _Nonnull str) {
        
        if ([model.user_type isEqualToString:@"0"]||[model.usertype isEqualToString:@"0"]) { //个人订单
            //跳转到个人订单 服务单详情
            ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
            vc.orderID = model.orderId;
            [self.navigationController pushViewController:vc animated:YES];
        } else if ([model.user_type isEqualToString:@"2"]||[model.usertype isEqualToString:@"2"]) { //电器厂家订单
            //跳转到电器厂家 服务单详情
            ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
            vc.orderID = model.orderId;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        [tableView reloadData];
    };
    
    [cell reashWith:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSSHmainLIstModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(model.Id);
    [NetWorkTool POST:isreadNewsapi param:param success:^(id dic) {
        
        if ([model.state integerValue ]  == 1) {
            CSnewJieGuoViewController *vc = [[CSnewJieGuoViewController alloc]init];
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if ([model.state integerValue ]  == -2) {
            CancleViewController *vccc = [[CancleViewController alloc]init];
            vccc.model = model;
            [self.navigationController pushViewController:vccc animated:YES];
            
        }else{
            CSnewConViewController *vccc = [[CSnewConViewController alloc]init];
            vccc.model = model;
            vccc.myblock = ^(NSString * _Nonnull str) {
            };
            [self.navigationController pushViewController:vccc animated:YES];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [self->_myTableView.mj_header beginRefreshing];
        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSnewsTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSnewsTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
