//
//  BannerDetailsViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/11/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BannerDetailsViewController : BaseViewController
@property (nonatomic, strong) NSString *bannerId; //轮播图Id
@property (nonatomic, strong) NSString *detailsStr; //details
@property (nonatomic, strong) NSString *titleStr; //页面标题
@property (nonatomic, strong) NSString *partsId;

@end

NS_ASSUME_NONNULL_END
