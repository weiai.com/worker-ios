//
//  ListenListViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ListenListViewController.h"
#import "ListeningListTableViewCell.h"
#import "CSListDetalieViewController.h"
#import "CSlistModel.h"
#import "CSnewListeningListTableViewCell.h"
#import "CSnewListDetalieViewController.h"
#import "CSnewPTViewController.h"
#import "CSDQCViewController.h"
#import "CSPersonModel.h"
#import "JPUSHService.h"
#import "CSnewsViewController.h"
#import "CSSHmainLIstModel.h"
#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"
#import "iflyMSC/IFlySpeechSynthesizer.h"
#import <iflyMSC/iflyMSC.h>
#import "CSsecviceViewController.h"
#import "CSaddPerCardViewController.h"
#import "CSIdAuthViewController.h"
#import "JSHAREService.h"
#import <AdSupport/AdSupport.h>
#import "CSSHlistTableViewCell.h"
#import "CSnewJieGuoViewController.h"
#import "CancleViewController.h"
#import "CSnewConViewController.h"
#import "CSSHmainLIstModel.h"
#import "PerfectingInfoViewController.h"
//#import "CSSHlistModel.h"
#import "CSnewDQCViewController.h"
#import "CSBannerModel.h"
#import "HomeTableHeaderView.h"

@interface ListenListViewController ()<UITableViewDelegate,UITableViewDataSource,IFlySpeechSynthesizerDelegate>{
    IFlySpeechSynthesizer * _iFlySpeechSynthesizer;
}
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UIButton *ReceiptBut;
@property(nonatomic,strong)UIView *bgView;
@property(nonatomic,strong)UILabel *numberLB;
@property(nonatomic,strong)UILabel *bgUPLable;
@property(nonatomic,strong)UILabel *bgdowLable;
@property(nonatomic,strong)CSPersonModel *model;
@property(nonatomic,strong)NSMutableArray *mydateUPSource;
@property(nonatomic,assign)BOOL isforst;
@property(nonatomic,strong)NSString *eqTimeStr;
@property(nonatomic,strong)NSMutableArray *bannerArr;
@property(nonatomic,strong)UILabel *tipsLab;

@end

@implementation ListenListViewController

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:YES];
    if (IPHONE_X) {
        if (@available(iOS 11.0, *)){
            self.tabBarController.tabBar.frame = CGRectMake(0, ScreenH-kTabbarHeight, ScreenW, kTabbarHeight);
        }
    }
    [_ReceiptBut removeFromSuperview];
}

-(void)requeetRight{
    [NetWorkTool POST:shouHouList param:nil success:^(id dic) {
        //KMyLog(@"售后维修%@",dic);
        NSMutableArray *numberArr = [NSMutableArray arrayWithCapacity:2];
        for (NSMutableDictionary *mudic in [dic objectForKey:@"data"]) {
            
            CSSHmainLIstModel *model = [[CSSHmainLIstModel alloc]init];
            [model setValuesForKeysWithDictionary:mudic];
            if ([model.isread integerValue]  == 0) {
                [numberArr addObject:model];
            }
        }
        
        if (numberArr.count >0) {
            //已读 未读做判断
            [self.rightbutton setImage:imgname(@"newyou") forState:(UIControlStateNormal)];
        } else {
            [self.rightbutton setImage:imgname(@"newmeiyou") forState:(UIControlStateNormal)];
        }
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)rightClick {
    [self.navigationController pushViewController:[CSnewsViewController new] animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    if (IPHONE_X) {
        if (@available(iOS 11.0, *)){
            self.tabBarController.tabBar.frame = CGRectMake(0, ScreenH-kTabbarHeight, ScreenW, kTabbarHeight);
        }
    }
    
    [singlTool shareSingTool].index = 0;
    [_ReceiptBut removeFromSuperview];
    self.isforst =  NO;
    if (self.mydateSource) {
        [self showBtton];
    }
    [self requeetRight];
    [self loadBanner];
    self.bgView.alpha = 1;
    [self.myTableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"听单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.mydateUPSource = [NSMutableArray arrayWithCapacity:1];
    self.bannerArr = [NSMutableArray arrayWithCapacity:1];
    
    self.bgView.alpha = 1;
    
    [self.rightbutton setImage:imgname(@"newmeiyou") forState:(UIControlStateNormal)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tuisonhg:) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestDetaile) name:@"applicationWillEnterForeground" object:nil];
}

#pragma mark - 请求轮播图数据
-(void)loadBanner {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = @"1";
    [self.bannerArr removeAllObjects];
    [NetWorkTool POST:Mbannerapi param:param success:^(id dic) {
        //KMyLog(@"听单页 轮播图 数据请求 %@", dic);
        if (weakSelf.bannerArr.count > 0) {
            return ;
        }

        KMyLog(@"听单页 轮播图数据 %@", dic[@"data"]);
        
        self.bannerArr = [CSBannerModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        HomeTableHeaderView *tableHeaerView = (HomeTableHeaderView *)self.myTableView.tableHeaderView;
        tableHeaerView.data = weakSelf.bannerArr;
        [tableHeaerView loadContent];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSString *)localizedStringTime{
    NSDateFormatter*formatter = [[NSDateFormatter alloc]init];[formatter setDateFormat:@"yyy-MM-dd HH:mm:ss"];
    NSString*dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}

-(void)tuisonhg:(NSNotification *)no{
    NSString *code = [NSString stringWithFormat:@"%@",[no.userInfo objectForKey:@"textstr"]];
    
    NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@",@"5cb580e1"];
   
    [self loadBanner];
    [self requeetRight];
    [self requestDetaile];
   
   
    return;
    
    [IFlySpeechUtility createUtility:initString];
    
    // 创建合成对象，为单例模式
    _iFlySpeechSynthesizer = [IFlySpeechSynthesizer sharedInstance];
    
    _iFlySpeechSynthesizer.delegate = self;
    //设置语音合成的参数
    //语速,取值范围 0~100
    [_iFlySpeechSynthesizer setParameter:@"50" forKey:[IFlySpeechConstant SPEED]];
    //音量;取值范围 0~100
    [_iFlySpeechSynthesizer setParameter:@"50" forKey: [IFlySpeechConstant VOLUME]];
    //发音人,默认为”xiaoyan”;可以设置的参数列表可参考个 性化发音人列表
    [_iFlySpeechSynthesizer setParameter:@" xiaoyan " forKey: [IFlySpeechConstant VOICE_NAME]];
    //音频采样率,目前支持的采样率有 16000 和 8000
    [_iFlySpeechSynthesizer setParameter:@"8000" forKey: [IFlySpeechConstant SAMPLE_RATE]];
    //asr_audio_path保存录音文件路径，如不再需要，设置value为nil表示取消，默认目录是documents
    [_iFlySpeechSynthesizer setParameter:@" tts.pcm" forKey: [IFlySpeechConstant TTS_AUDIO_PATH]];
    //启动合成会话
    [_iFlySpeechSynthesizer startSpeaking:code];
    
    //    *  |  小燕     |   xiaoyan        |
    //    *  |  小宇     |   xiaoyu         |
    //    *  |  凯瑟琳   |   catherine      |
    //    *  |  亨利     |   henry          |
    //    *  |  玛丽     |   vimary         |
    //    *  |  小研     |   vixy           |
    //    *  |  小琪     |   vixq           |
    //    *  |  小峰     |   vixf           |
    //    *  |  小梅     |   vixl           |
    //    *  |  小莉     |   vixq           |
    //    *  |  小蓉     |   vixr           |
    //    *  |  小芸     |   vixyun         |
    //    *  |  小坤     |   vixk           |
    //    *  |  小强     |   vixqa          |
    //    *  |  小莹     |   vixyin         |
    //    *  |  小新     |   vixx           |
    //    *  |  楠楠     |   vinn           |
    //    *  |  老孙     |   vils           |
    
}
//合成结束，此代理必须要实现
- (void) onCompleted:(IFlySpeechError *) error{
    KMyLog(@"myerror%@",error);
}

//合成开始
- (void) onSpeakBegin{}
//合成缓冲进度
- (void) onBufferProgress:(int) progress message:(NSString *)msg{}
//合成播放进度
- (void) onSpeakProgress:(int) progress{}  

#pragma mark - 请求订单
/**
 网络请求
 */
-(void)requestDetaile{
  
    [NetWorkTool POST:LlistenList param:nil success:^(id dic) {
        //KMyLog(@"___________ 接单 __________%@",[HFTools toJSONString:dic]);
        
        [self.mydateSource removeAllObjects];
        
        NSArray *arr  = [NSArray arrayWithArray:[[dic objectForKey:@"data"] objectForKey:@"afterOrders"]];
        if (arr.count >0) {
            for (NSDictionary *mydic in [[dic objectForKey:@"data"] objectForKey:@"afterOrders"]) {
                [DYModelMaker DY_makeModelWithDictionary:mydic modelKeyword:@"" modelName:@""];
                CSlistModel *mdeol = [[CSlistModel alloc]init];
                [mdeol setValuesForKeysWithDictionary:mydic];
                mdeol.isup = 1;
                [self.mydateSource addObject:mdeol];
            }
        }
        
        for (NSDictionary *mydic in [[dic objectForKey:@"data"] objectForKey:@"appOrders"]) {
            CSlistModel *mdeol = [[CSlistModel alloc]init];
            mdeol.isup = 0;
            [mdeol setValuesForKeysWithDictionary:mydic];
            [self.mydateSource addObject:mdeol];
        }
        self.eqTimeStr = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"data"] objectForKey:@"limitTime"]];
        [self.myTableView reloadData];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
    } needUser:NO];
}

-(void)removeusstate{
    kWeakSelf;
    
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        self.model = nil;
        self.model = [[CSPersonModel alloc]init];
        [weakSelf.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        
        if (strIsEmpty(weakSelf.model.id_card_no)) {
            //选择服务范围
            UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"请先完善您的个人信息" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            
            [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                KMyLog(@"取消");
            }]];
            [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                KMyLog(@"确定");
                PerfectingInfoViewController *VC = [PerfectingInfoViewController new];
                VC.phone = weakSelf.model.phone;
                [self.navigationController pushViewController:VC animated:YES];
            }]];
            [self presentViewController:alertCon animated:YES completion:nil];
            return ;
        }
        
        if ([self->_model.workstate integerValue] == 1) {
            [self requestDetaile];
            if (weakSelf.isforst) {
                [[UIApplication sharedApplication] registerForRemoteNotifications];//开启远程推送
                [self.myTableView reloadData];
            }
            //打卡成功 空状态页图片
            self.tipsLab.text = @"手慢了，订单已被抢走……";
            
        } else {
            if (weakSelf.isforst) {
                [self.mydateSource removeAllObjects];
                [self.myTableView reloadData];
            }
            [self.mydateSource removeAllObjects];
            [self.myTableView reloadData];
            [[UIApplication sharedApplication] unregisterForRemoteNotifications];//关闭远程推送
        }
        self.isforst = YES;
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

/**
 创建接单按钮
 */
-(void)showBtton{
    
    if (!_ReceiptBut) {
        self.ReceiptBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    }
    CGFloat fff = kTabbarHeight+10;
    [[UIApplication sharedApplication].keyWindow addSubview:_ReceiptBut];
    [_ReceiptBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-fff);
        make.left.offset(KWIDTH/2-40);
        make.width.offset(80);
        make.height.offset(80);
    }];
    [self removeusstate];
}

/**
 弹出框的背景图
 */
-(UIView *)bgView{
    
    if (!_bgView) {
        _bgView = [[UIView alloc]init];
        _bgView.frame = self.view.bounds;
        _bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
        
        UIView *whiteBGView = [[UIView alloc]init];
        whiteBGView.backgroundColor = [UIColor whiteColor];
        whiteBGView.layer.masksToBounds = YES;
        whiteBGView.layer.cornerRadius = 6;
        [_bgView addSubview:whiteBGView];
        [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.offset(0);
            make.height.offset(240);
            make.left.offset(22);
            make.right.offset(-22);
        }];
        
        UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
        upImage.layer.masksToBounds = YES;
        upImage.layer.cornerRadius = 20;
        [whiteBGView addSubview:upImage];
        upImage.image = [UIImage imageNamed:@"组732"];
        
        UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
        [whiteBGView addSubview:UpLable];
        UpLable.font = FontSize(16);
        UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
        UpLable.textAlignment =  NSTextAlignmentCenter;
        UpLable.text = @"结束接单";
        self.bgUPLable = UpLable;
        
        UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
        [whiteBGView addSubview:DowLable];
        DowLable.font = FontSize(14);
        DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
        DowLable.textAlignment =  NSTextAlignmentCenter;
        DowLable.text = @"明天继续努力";
        self.bgdowLable = DowLable;
        
        UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
        [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        iKnowBut.layer.masksToBounds = YES;
        iKnowBut.layer.cornerRadius = 4;
        [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [whiteBGView addSubview:iKnowBut];
        [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.offset(48);
            make.left.offset(28);
            make.right.offset(-28);
            make.bottom.offset(-39);
        }];
    }
    return _bgView;
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
}

-(void)workOrnotWork:(UIButton *)but{
    
    kWeakSelf;
    NSString *str =  @"1";
    if ( [_model.workstate integerValue] == 1) {
        str = @"0";
    }
    
    NSMutableDictionary *muarr = [NSMutableDictionary dictionaryWithCapacity:1];
    muarr[@"state"] = str;
    
    [NetWorkTool POST:LendBegin param:muarr success:^(id dic) {
        [weakSelf removeusstate];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  _mydateUPSource.count>0?2:1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_mydateUPSource.count>0) {
        return section == 0?_mydateUPSource.count:_mydateSource.count;
    } else {
        return _mydateSource.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSlistModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if (model.isup == 1)
    {
         return 108;
    }else
    {
        if ([model.user_type integerValue] == 2)
        {
            return UITableViewAutomaticDimension;
        }else
        {
             return 108;
        }
    }
    return 108;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [UIView new];
    return view;
    view.frame = CGRectMake(0, 0, KWIDTH, 194);
    
    dispatch_queue_t serialQueue = dispatch_queue_create("com.SerialQueue", DISPATCH_QUEUE_SERIAL);
    NSLog(@"1");
    dispatch_sync(serialQueue, ^{
        NSLog(@"2");
        BOOL isMainT = [NSThread isMainThread];
        NSLog(@"isMainThread = %d",isMainT);
    });
    NSLog(@"3");
    
    NSMutableArray *mu = [NSMutableArray arrayWithCapacity:1];
    for (CSBannerModel *model in _bannerArr) {
        [mu addObject:model.pic_url];
    }
    
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSlistModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if (model.isup == 1) {
        //售后
        CSSHlistTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"CSSHlistTableViewCell" forIndexPath:indexPath];
        cell2.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell2 refash:model];
        return cell2;
    } else {
        if ([model.user_type integerValue] == 2) {
            //电器厂
            ListeningListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListeningListTableViewCell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell refash:model];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else {
            //个人用户
            CSnewListeningListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSnewListeningListTableViewCell" forIndexPath:indexPath];
            cell.backgroundColor = [UIColor clearColor];
            [cell refash:model];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSlistModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    if (model.isup == 1){ //售后维修订单
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
        param[@"id"] = model.Id;
        
            CSSHmainLIstModel *mymodel = [[CSSHmainLIstModel alloc]init];
            mymodel.id = model.Id;
            mymodel.orderDate = model.orderDate;
            mymodel.orderTime = model.orderTime;
            mymodel.state = model.state;
            mymodel.handleDate = model.handleDate;
            mymodel.createDate = model.createDate;
            mymodel.isread = model.isread;
            mymodel.repairUserId = model.repairUserId;
            mymodel.user_type = model.user_type;
            mymodel.userId = model.userId;
            mymodel.orderId = model.orderId;
            mymodel.account = model.account;
            mymodel.app_order_id = model.app_order_id;
            
            if ([model.state integerValue] == 1) {
                CSnewJieGuoViewController *vc = [[CSnewJieGuoViewController alloc]init];
                vc.model = mymodel;
                [self.navigationController pushViewController:vc animated:YES];
            } else if([model.state integerValue] == -2) {
                CancleViewController *vccc = [[CancleViewController alloc]init];
                vccc.model = mymodel;
                [self.navigationController pushViewController:vccc animated:YES];
            } else {
                CSnewConViewController *vccc = [[CSnewConViewController alloc]init];
                vccc.model = mymodel;
                vccc.myblock = ^(NSString * _Nonnull str) {
                };
                [self.navigationController pushViewController:vccc animated:YES];
            }
    } else if (model.isup == 0) { //预约单
        BOOL istime = NO;
        for ( CSlistModel *model33 in _mydateSource) { //售后维修
            if (model33.isup == 1 ) {
                NSInteger indtime = [_eqTimeStr integerValue];
                NSInteger newind =[[[singlTool shareSingTool] currentTimeStr] integerValue];
                KMyLog(@"dsfasdsf-----%@",_eqTimeStr);
                istime = indtime < newind ?YES:NO;
            }
        }
        
        if (istime) {
            //预约时间小
            ShowToastWithText(@"请先处理您的售后维修单");
            return;
        }
        
        if ([model.user_type integerValue] == 0) {
            //普通用户
            CSnewPTViewController *detVC1 =[[CSnewPTViewController alloc]init];
            detVC1.mymodel = model;
            [self.navigationController pushViewController:detVC1 animated:YES];
            KMyLog(@"个人订单 跳转 订单详情页面");
            [_myTableView reloadData];
        } else if ([model.user_type integerValue] == 1) {
            CSnewListDetalieViewController *detVC =[[CSnewListDetalieViewController alloc]init];
            //企业用户
            detVC.mymodel = model;
            [self.navigationController pushViewController:detVC animated:YES];
            [_myTableView reloadData];
        } else if ([model.user_type integerValue] == 2) {
            //保内
            if ([model.is_protect boolValue]) {
                CSnewDQCViewController *detVC2 =[[CSnewDQCViewController alloc]init];
                detVC2.mymodel = model;
                detVC2.userType = model.user_type;
                [self.navigationController pushViewController:detVC2 animated:YES];
                [_myTableView reloadData];
            } else {
                //保外
                CSDQCViewController *detVC2 =[[CSDQCViewController alloc]init];
                detVC2.mymodel = model;
                detVC2.userType = model.user_type;
                detVC2.order_type_id = model.order_type_id; //新添加的
                [self.navigationController pushViewController:detVC2 animated:YES];
                [_myTableView reloadData];
            }
        }
    }
}

-(UITableView *)myTableView{
    KMyLog(@"dayin%f",self.navigationController.navigationBar.height);
    
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight-kTabbarHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        [_myTableView registerNib:[UINib nibWithNibName:@"ListeningListTableViewCell" bundle:nil] forCellReuseIdentifier:@"ListeningListTableViewCell"];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSnewListeningListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSnewListeningListTableViewCell"];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSSHlistTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSSHlistTableViewCell"];
        kWeakSelf;
        
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(KWIDTH/2-106, KHEIGHT/2-36-60, 213, 158) image:imgname(@"组 3259") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        _myTableView.placeHolderView.backgroundColor = [UIColor clearColor];
        
        UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 138, 213, 20)];
        tipsLab.text = @"不要挂机了,快点打卡接单吧";
        tipsLab.textAlignment = NSTextAlignmentCenter;
        tipsLab.textColor = [UIColor colorWithHexString:@"#777777"];
        tipsLab.font = FontSize(16);
        [_myTableView.placeHolderView addSubview:tipsLab];
        self.tipsLab = tipsLab;
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
        
        HomeTableHeaderView *tableHeaderView = [[HomeTableHeaderView alloc] init];
        tableHeaderView.frame = CGRectMake(0, 0, KWIDTH, 224);
        _myTableView.tableHeaderView = tableHeaderView;
    }
    return _myTableView;
}

- (NSString *)currentdateInterval{
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)([datenow timeIntervalSince1970]*1000)];
    return timeSp;
}

@end
