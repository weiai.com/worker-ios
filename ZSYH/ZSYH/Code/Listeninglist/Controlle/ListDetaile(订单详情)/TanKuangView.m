
//
//  TanKuangView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "TanKuangView.h"

@implementation TanKuangView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
       
        [self showui];
    }
    return self;
}
-(void)showui{
   
    self.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(154);
        make.width.offset(235);
        
    }];
    
    UILabel *titile = [[UILabel alloc]init];
    titile.font = FontSize(16);
    titile.text = @"您正在进行忽略订单操作,";
    titile.textColor = [UIColor colorWithHexString:@"#333333"];
    titile.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:titile];
    self.upLable =titile;
    UILabel *msglb = [[UILabel alloc]init];
    msglb.font = FontSize(16);
    msglb.text = @"确定忽略订单？";
    msglb.textColor = [UIColor colorWithHexString:@"#333333"];
    msglb.textAlignment = NSTextAlignmentCenter;
    self.dowLable =msglb;
    [whiteBGView addSubview:msglb];
    
    [titile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(17);
        make.top.offset(20);
        
    }];
    [msglb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(20);
        make.top.offset(47);
        
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(quxiaocaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(yescaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(35);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-35);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    
    
}
-(void)quxiaocaozuoAction:(UIButton *)but{
    if (self.myblcok) {
        self.myblcok(0);
    }
    
}
-(void)yescaozuoAction:(UIButton *)but{
    if (self.myblcok) {
        self.myblcok(1);
    }
    
}
@end
