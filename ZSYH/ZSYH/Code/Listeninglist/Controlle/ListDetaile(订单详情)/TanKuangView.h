//
//  TanKuangView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TanKuangView : UIView
@property(nonatomic,strong)UILabel *upLable;
@property(nonatomic,strong)UILabel *dowLable;
@property(nonatomic,copy)void (^myblcok)(NSInteger ind);
@end

NS_ASSUME_NONNULL_END
