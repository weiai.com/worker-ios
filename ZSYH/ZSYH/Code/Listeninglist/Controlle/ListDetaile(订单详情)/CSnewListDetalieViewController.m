//
//  CSnewListDetalieViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//  个人订单 订单详情

#import "CSnewListDetalieViewController.h"
#import "NSString+AttributedString.h"

@interface CSnewListDetalieViewController ()
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *showbgView;//提示是否取消
@property(nonatomic,assign)BOOL isec;//是否二次上门
@property(nonatomic,strong)UIView *bgViewsec;

@end

@implementation CSnewListDetalieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isec = NO;
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self showloadingview];
    if (!_mymodel) {
        self.isec = YES;
        
        [self shodetalils];
    } else {
        [self showui];
    }
    [self shwoBgviewsec];
    KMyLog(@"是不是个人订单 订单详情");
}

-(void)shodetalils{
    if (!_mymodel) {
        _mymodel = [[CSlistModel alloc]init];
        NSMutableDictionary *param  = [NSMutableDictionary dictionaryWithCapacity:1];
        param[@"orderId"] = _urls;
        [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
            [self->_mymodel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
            [self showui];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }
}

-(void)showui{
    [self.view addSubview:self.scrollView];
    CGFloat wei = 60;
    CGFloat left1 = 26;
    CGFloat left2 = 14;
    
    UIView *bbgvieww = [[UIView alloc]init];
    [self.scrollView addSubview:bbgvieww];
    CGFloat hh = 20;
    if (_mymodel.user_type) {
        //企业用户
        UILabel *complay = [[UILabel alloc]initWithFrame:CGRectMake(left1, 20, wei, 14)];
        complay.font = FontSize(14);
        complay.textColor = K999999;
        complay.text = @"单位名称:";
        [self.scrollView addSubview:complay];
        
        UILabel *complaylb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, 20, KWIDTH-left2-left1-wei-left1-20, 14)];
        complaylb.font = KFontPingFangSCMedium(14);
        complaylb.textColor = KshiYellow;
        complaylb.text = _mymodel.company_name;
        [self.scrollView addSubview:complaylb];
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(left1, complay.bottom+10, KWIDTH-left1-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line1];
        hh = line1.bottom+8;
    }
    
    UILabel *secvice = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
    secvice.font = FontSize(14);
    secvice.textColor = K999999;
    secvice.text = @"服务类型";
    [self.scrollView addSubview:secvice];
    
    UILabel *secvicelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
    secvicelb.font = KFontPingFangSCMedium(14);
    secvicelb.textColor = K666666;
    secvicelb.text = _mymodel.service_name;
    [self.scrollView addSubview:secvicelb];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, secvicelb.bottom+10, KWIDTH-left1-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line2];
    
    UILabel *produrt = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+8, wei, 14)];
    produrt.font = FontSize(14);
    produrt.textColor = K999999;
    produrt.text = @"电器品类";
    [self.scrollView addSubview:produrt];
    
    UILabel *produrtlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line2.bottom+8, 150, 14)];
    produrtlb.font = KFontPingFangSCMedium(14);
    produrtlb.textColor = K666666;
    produrtlb.text = _mymodel.fault_name;
    [self.scrollView addSubview:produrtlb];

    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left1, produrt.bottom+10, KWIDTH-left1-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line3];
    
    UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
    number.font = FontSize(14);
    number.textColor = K999999;
    number.text = @"报修数量";
    [self.scrollView addSubview:number];
    
    UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
    numberlb.font = KFontPingFangSCMedium(14);
    numberlb.textColor = K666666;
    numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
    [self.scrollView addSubview:numberlb];
    
    UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
    line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line44];
    
    //维修增加故障原因
    if ([self.mymodel.fault_type isEqualToString:@"3"]) {
        
        UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
        faultReasonL.font = FontSize(14);
        faultReasonL.textColor = K999999;
        faultReasonL.text = @"故障原因";
        [self.scrollView addSubview:faultReasonL];
        
        UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        faultReason.font = KFontPingFangSCMedium(14);
        faultReason.textColor = K666666;
        faultReason.text = _mymodel.common_failures;
        [self.scrollView addSubview:faultReason];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
        line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line];
        
        UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
        guzhang.font = FontSize(14);
        guzhang.textColor = K999999;
        guzhang.text = @"备注信息";
        [self.scrollView addSubview:guzhang];
        
        CGFloat heigbz = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
        
        UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heigbz)];
        guzhanglb.font = FontSize(14);
        guzhanglb.textColor = K999999;
        guzhanglb.text = _mymodel.fault_comment;
        guzhanglb.numberOfLines = 0;
        [self.scrollView addSubview:guzhanglb];
        
        heigbz = 14;
        UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
        line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line4];
        
        UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
        yuyueTime.font = FontSize(14);
        yuyueTime.textColor = K999999;
        yuyueTime.text = @"上门日期";
        [self.scrollView addSubview:yuyueTime];
        
        UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        yuyueTimelb.font = KFontPingFangSCMedium(14);
        yuyueTimelb.textColor = K333333;
        yuyueTimelb.text = _mymodel.order_date;
        
        NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
        NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
        NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
        NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
        yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
        [self.scrollView addSubview:yuyueTimelb];
        
        UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
        line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line5];
        
        UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
        yuyueTime2.font = FontSize(14);
        yuyueTime2.textColor = K999999;
        yuyueTime2.text = @"上门时间";
        [self.scrollView addSubview:yuyueTime2];
        
        UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        yuyueTimelb2.font = KFontPingFangSCMedium(14);
        yuyueTimelb2.textColor = K333333;
        NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
        [self.scrollView addSubview:yuyueTimelb2];
        
        UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
        line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line6];
        
        UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
        address.font = FontSize(14);
        address.textColor = K999999;
        address.text = @"地址";
        [self.scrollView addSubview:address];
        
        CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
        
        UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
        addresslb.font = KFontPingFangSCMedium(14);
        addresslb.textColor = K999999;
        addresslb.numberOfLines = 0;
        addresslb.text = _mymodel.user_address;
        [self.scrollView addSubview:addresslb];
        
        
        UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
        line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line11];
        
        
        UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+7, 30, 14)];
        dianti.font = FontSize(14);
        dianti.textColor = K999999;
        dianti.text = @"电梯";
        [self.scrollView addSubview:dianti];
        
        UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+7, 30, 14)];
        diantilb.font = KFontPingFangSCMedium(14);
        diantilb.textColor = K666666;
        diantilb.text = @"无";
        if ([_mymodel.has_elevator integerValue] == 1) {
            diantilb.text = @"有";
        }
        [self.scrollView addSubview:diantilb];
        
        UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line11.bottom+7, 30, 14)];
        louceng.font = FontSize(14);
        louceng.textColor = K999999;
        louceng.text = @"楼层";
        [self.scrollView addSubview:louceng];
        
        UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line11.bottom+7, 35, 14)];
        loucenglb.font = KFontPingFangSCMedium(14);
        loucenglb.textColor = K666666;
        loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
        [self.scrollView addSubview:loucenglb];
        loucenglb.textAlignment = NSTextAlignmentRight;
        
        UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
        line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line8];
        
        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, line8.bottom+20);
        bbgvieww.backgroundColor = [UIColor whiteColor];
        //bbgvieww.layer.masksToBounds = YES;
        //bbgvieww.layer.cornerRadius = 12;
        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
        
        UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(25, bbgvieww.bottom+10, KWIDTH-50, 17)];
        ordernumLB.font = FontSize(12);
        ordernumLB.textColor = K999999;
        ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
        [self.scrollView addSubview:ordernumLB];
        
        CGFloat  wwww = (KWIDTH-160-160)/3;
        UIButton *leftbutto = [UIButton buttonWithType:(UIButtonTypeCustom)];
        leftbutto.frame = CGRectMake(wwww, ordernumLB.bottom+30, 160, 48);
        [leftbutto setTitle:@"接单" forState:(UIControlStateNormal)];
        [leftbutto setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [leftbutto setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [leftbutto addTarget:self action:@selector(jiedanction:) forControlEvents:(UIControlEventTouchUpInside)];
        leftbutto.layer.masksToBounds = YES;
        leftbutto.layer.cornerRadius = 4;
        [self.scrollView addSubview:leftbutto];
        
        UIButton *rightbutto = [UIButton buttonWithType:(UIButtonTypeCustom)];
        rightbutto.frame = CGRectMake(wwww*2+160, ordernumLB.bottom+30, 160, 48);
        [rightbutto setTitle:@"忽略" forState:(UIControlStateNormal)];
        [rightbutto setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [rightbutto setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [rightbutto addTarget:self action:@selector(rightbuttobuttoaction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:rightbutto];
        rightbutto.layer.masksToBounds = YES;
        rightbutto.layer.cornerRadius = 4;
        
        _scrollView.contentSize = CGSizeMake(KWIDTH, rightbutto.bottom+10);
        
        if (self.isec) {
            leftbutto.hidden = YES;
            rightbutto.hidden = YES;
        }
        
    } else {
        
        UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
        guzhang.font = FontSize(14);
        guzhang.textColor = K999999;
        guzhang.text = @"备注信息";
        [self.scrollView addSubview:guzhang];
        
        CGFloat heigbz = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
        
        UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heigbz)];
        guzhanglb.font = FontSize(14);
        guzhanglb.textColor = K999999;
        guzhanglb.text = _mymodel.fault_comment;
        
        guzhanglb.numberOfLines = 0;
        [self.scrollView addSubview:guzhanglb];
        heigbz = 14;
        UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
        line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line4];
        
        UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
        yuyueTime.font = FontSize(14);
        yuyueTime.textColor = K999999;
        yuyueTime.text = @"上门日期";
        [self.scrollView addSubview:yuyueTime];
        
        UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        yuyueTimelb.font = KFontPingFangSCMedium(14);
        yuyueTimelb.textColor = K333333;
        yuyueTimelb.text = _mymodel.order_date;
        
        NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
        NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
        NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
        NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
        yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
        [self.scrollView addSubview:yuyueTimelb];
        
        UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
        line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line5];
        
        UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
        yuyueTime2.font = FontSize(14);
        yuyueTime2.textColor = K999999;
        yuyueTime2.text = @"上门时间";
        [self.scrollView addSubview:yuyueTime2];
        
        UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        yuyueTimelb2.font = KFontPingFangSCMedium(14);
        yuyueTimelb2.textColor = K666666;
        NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
        [self.scrollView addSubview:yuyueTimelb2];
        
        UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
        line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line6];
        
        UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
        address.font = FontSize(14);
        address.textColor = K999999;
        address.text = @"地址";
        [self.scrollView addSubview:address];
        
        CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
        UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
        addresslb.font = KFontPingFangSCMedium(14);
        addresslb.textColor = K999999;
        addresslb.numberOfLines = 0;
        addresslb.text = _mymodel.user_address;
        [self.scrollView addSubview:addresslb];
        
        
        UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
        line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line11];
        
        
        UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+7, 30, 14)];
        dianti.font = FontSize(14);
        dianti.textColor = K999999;
        dianti.text = @"电梯";
        [self.scrollView addSubview:dianti];
        
        UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+7, 30, 14)];
        diantilb.font = KFontPingFangSCMedium(14);
        diantilb.textColor = K666666;
        diantilb.text = @"无";
        if ([_mymodel.has_elevator integerValue] == 1) {
            diantilb.text = @"有";
        }
        [self.scrollView addSubview:diantilb];
        
        UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line11.bottom+7, 30, 14)];
        louceng.font = FontSize(14);
        louceng.textColor = K999999;
        louceng.text = @"楼层";
        [self.scrollView addSubview:louceng];
        
        UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line11.bottom+7, 35, 14)];
        loucenglb.font = KFontPingFangSCMedium(14);
        loucenglb.textColor = K666666;
        loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
        [self.scrollView addSubview:loucenglb];
        loucenglb.textAlignment = NSTextAlignmentRight;
        
        UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
        line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line8];
        
        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, line8.bottom+20);
        bbgvieww.backgroundColor = [UIColor whiteColor];
        //bbgvieww.layer.masksToBounds = YES;
        //bbgvieww.layer.cornerRadius = 12;
        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
        
        UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(25, bbgvieww.bottom+10, KWIDTH-50, 17)];
        ordernumLB.font = FontSize(12);
        ordernumLB.textColor = K999999;
        ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
        [self.scrollView addSubview:ordernumLB];
        
        CGFloat  wwww = (KWIDTH-160-160)/3;
        UIButton *leftbutto = [UIButton buttonWithType:(UIButtonTypeCustom)];
        leftbutto.frame = CGRectMake(wwww, ordernumLB.bottom+30, 160, 48);
        [leftbutto setTitle:@"接单" forState:(UIControlStateNormal)];
        [leftbutto setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [leftbutto setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [leftbutto addTarget:self action:@selector(jiedanction:) forControlEvents:(UIControlEventTouchUpInside)];
        leftbutto.layer.masksToBounds = YES;
        leftbutto.layer.cornerRadius = 4;
        [self.scrollView addSubview:leftbutto];
        
        UIButton *rightbutto = [UIButton buttonWithType:(UIButtonTypeCustom)];
        rightbutto.frame = CGRectMake(wwww*2+160, ordernumLB.bottom+30, 160, 48);
        [rightbutto setTitle:@"忽略" forState:(UIControlStateNormal)];
        [rightbutto setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [rightbutto setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [rightbutto addTarget:self action:@selector(rightbuttobuttoaction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:rightbutto];
        rightbutto.layer.masksToBounds = YES;
        rightbutto.layer.cornerRadius = 4;
        
        _scrollView.contentSize = CGSizeMake(KWIDTH, rightbutto.bottom+10);
        
        if (self.isec) {
            leftbutto.hidden = YES;
            rightbutto.hidden = YES;
        }
    }
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)jiedanction:(UIButton *)but{
    
    KMyLog(@"leftbuttoaction");
    KMyLog(@"leftbuttoaction");
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"appId"] = NOTNIL(_mymodel.Id);
    kWeakSelf;
    [NetWorkTool POST:LlistGrab param:param success:^(id dic) {
   
        [singlTool shareSingTool].needReasfh = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)rightbuttobuttoaction:(UIButton *)but{
    
    if ([self.mymodel.isprivate_work integerValue]  == 1) {
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"appId"] = NOTNIL(self.mymodel.Id);
        [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
        KMyLog(@"rightbuttobuttoaction");
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

-(void)showloadingview{
    
    self.showbgView = [[UIView alloc]init];
    self.showbgView.frame = self.view.bounds;
    self.showbgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.showbgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(154);
        make.width.offset(235);
    }];
    
    UILabel *titile = [[UILabel alloc]init];
    titile.font = FontSize(16);
    titile.text = @"您正在进行忽略订单操作,";
    titile.textColor = [UIColor colorWithHexString:@"#333333"];
    titile.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:titile];
    
    UILabel *msglb = [[UILabel alloc]init];
    msglb.font = FontSize(16);
    msglb.text = @"确定忽略订单？";
    msglb.textColor = [UIColor colorWithHexString:@"#333333"];
    msglb.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:msglb];
    
    [titile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(17);
        make.top.offset(20);
    }];
    
    [msglb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(20);
        make.top.offset(47);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(quxiaocaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(yescaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(50);
        make.width.offset(64);
        make.bottom.offset(-32);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-50);
        make.width.offset(64);
        make.bottom.offset(-32);
    }];
}

-(void)quxiaocaozuoAction:(UIButton *)but{
    
    
    if ([self.mymodel.isprivate_work integerValue]  == 1) {
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"appId"] = NOTNIL(self.mymodel.Id);
        [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
        KMyLog(@"rightbuttobuttoaction");
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)yescaozuoAction:(UIButton *)but{
    [_showbgView removeFromSuperview];
    
    if ([self.mymodel.isprivate_work integerValue]  == 1) {
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"appId"] = NOTNIL(self.mymodel.Id);
        [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
        KMyLog(@"rightbuttobuttoaction");
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"接单成功";
    
    NSMutableAttributedString *dowStr = [NSString getAttributedStringWithOriginalString:@"*可在我的订单查看此单" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom + 16, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.attributedText = dowStr;
    DowLable.textAlignment = NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];;
    
}

@end

