//
//  CSDQCViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSDQCViewController.h"
#import "TanKuangView.h"
#import "NSString+AttributedString.h"
#import "ZYTestOrderDetailViewController.h"          //检测单详情
#import "ZYElectricalFactoryServiceOrderDetailsVC.h" //电器厂家 服务单详情

@interface CSDQCViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSArray *_nameArray;
}
@property (strong, nonatomic) UIPickerView *pickerView;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *ordernumLB;
@property(nonatomic,strong)UILabel *riqiLB;
@property(nonatomic,strong)UILabel *timeLB;
@property(nonatomic,strong)UILabel *tishilb;
@property(nonatomic,strong)UIButton *jiedanBUt;//接单
@property(nonatomic,strong)UIButton *huluebut;//忽略
@property(nonatomic,strong)UIView *bgViewRiqi;//选择日期的背景
@property(nonatomic,strong)UIButton *oldbut;//选择日期的but
@property(nonatomic,strong)UIView *bgViewtime;//选择时间的背景
@property(nonatomic,strong)UIButton *oldbuttime;//选中时间的but
@property(nonatomic,assign)NSInteger sencion0row;// uipickerview选中行
@property(nonatomic,strong)NSString *seleTime;//时间选择
@property(nonatomic,strong)UIView *showbgView;//提示是否取消
@property(nonatomic,assign)BOOL isec;//是否二次上门
@property(nonatomic,strong)UIView *bgViewsecjiedan;
@property(nonatomic,strong)NSString *timeStr;
@property(nonatomic,strong)UILabel *upShowLB;
@property(nonatomic,strong)UIView *bgViewsec;
@property(nonatomic,assign)BOOL istime;
@property(nonatomic,strong)TanKuangView *zhijiehuluo;
@property(nonatomic,strong)UIImageView *sanjiaoImage;
@end

@implementation CSDQCViewController
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_showbgView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isec = NO;
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self showui];
    
    if (!_mymodel) {
        self.isec = YES;
        [self shodetalils];
    } else {
        [self showui];
        [self shwoBgview];
        [self shwosecBgview];
        [self showloadingview];
    }
    [self shwoBgviewsec];
    [self shwoBgviewjiedan];
    self.istime = NO;
    // Do any additional setup after loading the view.
}

-(void)shodetalils{
    if (!_mymodel) {
        _mymodel = [[CSlistModel alloc]init];
        NSMutableDictionary *param  = [NSMutableDictionary dictionaryWithCapacity:1];
        param[@"orderId"] = _urls;
        [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
            KMyLog(@"ssssssssssss%@",dic);
            [self->_mymodel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
            [self showui];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }
}

- (void)leftClick{
    
    if (self.istime) {
        
        self.zhijiehuluo = [[TanKuangView alloc]initWithFrame:self.view.bounds];
        self.zhijiehuluo.upLable.text = @"您正在填写客户的预约信息，";
        //self.zhijiehuluo.dowLable.text = @"确定忽略订单？";
        self.zhijiehuluo.dowLable.text = @"确定返回？";
        
        //取消订单
        kWeakSelf;
        [[UIApplication sharedApplication].keyWindow addSubview: self.zhijiehuluo];
        self.zhijiehuluo.myblcok = ^(NSInteger ind) {
            if (ind == 0) {
                [weakSelf.zhijiehuluo removeFromSuperview];
            }else{
                [weakSelf.zhijiehuluo removeFromSuperview];
                
                if ([weakSelf.mymodel.isprivate_work integerValue]  == 1) {
                    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
                    mudic[@"appId"] = NOTNIL(weakSelf.mymodel.Id);
                    [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                        
                    } other:^(id dic) {
                        
                    } fail:^(NSError *error) {
                        
                    } needUser:YES];
                    
                    KMyLog(@"rightbuttobuttoaction");
                } else {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }
            }
        };
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showui{
    [self.view addSubview:self.scrollView];
    CGFloat wei = 60;
    CGFloat left1 = 26;
    CGFloat left2 = 14;
    
    UIView *bbgvieww = [[UIView alloc]init];
    [self.scrollView addSubview:bbgvieww];
    [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
    
    UIImageView *img = [[UIImageView alloc]init];
    img.image = imgname(@"cdianqichalogo");
    [bbgvieww addSubview:img];
    [img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.right.offset(-24);
        make.width.offset(24);
        make.height.offset(31);
    }];
    
    CGFloat hh = 20;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH / 2 - 111/2, 24, 111, 23)];
    if ([_mymodel.fault_type isEqualToString:@"3"]) { //fault_type 3 维修类型
        if ([_mymodel.is_protect boolValue]) {
            if ([_mymodel.order_type_id isEqualToString:@"1"]) { //检测单
                [imgView setImage:imgname(@"JiancedanImg-1")]; //检测单
            } else {
                [imgView setImage:imgname(@"baoneiimg")]; //保内
            }
        }else {
            if ([_mymodel.order_type_id isEqualToString:@"1"]) {
                [imgView setImage:imgname(@"JiancedanImg-1")]; //检测单
            } else {
                [imgView setImage:imgname(@"baowaiimg")]; //保外
            }
        }
    }else {
        [imgView setImage:imgname(@"")];
    }
    [self.scrollView addSubview:imgView];
    
    UILabel *complay = [[UILabel alloc]initWithFrame:CGRectMake(left1, 59, wei, 14)];
    complay.font = FontSize(14);
    complay.textColor = K999999;
    complay.text = @"厂商名称";
    [self.scrollView addSubview:complay];
    
    UILabel *complaylb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, 59, KWIDTH-left2-left1-wei-left1-20, 14)];
    complaylb.font = KFontPingFangSCMedium(14);
    //complaylb.textColor = [UIColor colorWithHexString:@"#CA9F61"]; //高亮
    complaylb.textColor = K666666;
    complaylb.text = _mymodel.company_name;
    [self.scrollView addSubview:complaylb];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(left1, complay.bottom+10, KWIDTH-left1-20, 1)];
    line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line1];
    hh = line1.bottom+8;
    
    UILabel *secvice = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
    secvice.font = FontSize(14);
    secvice.textColor = K999999;
    secvice.text = @"服务类型";
    [self.scrollView addSubview:secvice];
    
    UILabel *secvicelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
    secvicelb.font = KFontPingFangSCMedium(14);
    secvicelb.textColor = K666666;
    secvicelb.text = _mymodel.service_name;
    [self.scrollView addSubview:secvicelb];
    
    //厂家报修
    //    UILabel *baonei = [[UILabel alloc]initWithFrame:CGRectMake(secvicelb.right-5, hh, 100, 14)];
    //    baonei.font = FontSize(14);
    //    baonei.textColor = [UIColor colorWithHexString:@"#CA9F61"];
    //    baonei.text = _mymodel.service_name;
    //    if ([_mymodel.fault_type isEqualToString:@"3"]) { //fault_type 3 维修类型
    //        if ([_mymodel.is_protect boolValue]) {
    //            baonei.text = @"保内";
    //        }else {
    //            baonei.text = @"保外";
    //        }
    //    }else {
    //        baonei.text = @"";
    //    }
    //    [self.scrollView addSubview:baonei];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, secvicelb.bottom+10, KWIDTH-left1-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line2];
    
    UILabel *produrt = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+8, wei, 14)];
    produrt.font = FontSize(14);
    produrt.textColor = K999999;
    produrt.text = @"电器品类";
    [self.scrollView addSubview:produrt];
    
    UILabel *produrtlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line2.bottom+8, 180, 14)];
    produrtlb.font = KFontPingFangSCMedium(14);
    produrtlb.textColor = K666666;
    produrtlb.text = _mymodel.fault_name;
    [self.scrollView addSubview:produrtlb];
    
    //    UILabel *weixiuline = [[UILabel alloc]initWithFrame:CGRectMake(produrtlb.right+4, line2.bottom+11,1, 10)];
    //    weixiuline.backgroundColor = K666666;
    //    [self.scrollView addSubview:weixiuline];
    //
    //    UILabel *weixiu = [[UILabel alloc]initWithFrame:CGRectMake(weixiuline.right, line2.bottom+8,68, 14)];
    //    weixiu.font = FontSize(14);
    //    weixiu.textAlignment = NSTextAlignmentCenter;
    //    weixiu.textColor = K666666;
    //    weixiu.text = _mymodel.service_name;
    //    [self.scrollView addSubview:weixiu];
    
    //    UILabel *baonei = [[UILabel alloc]initWithFrame:CGRectMake(weixiu.right, line2.bottom+8, 100, 14)];
    //    baonei.font = FontSize(14);
    //    baonei.textColor = [UIColor colorWithHexString:@"#CA9F61"];;
    //    baonei.text = _mymodel.service_name;
    //    if ([_mymodel.is_protect integerValue] == 1) {
    //        baonei.text = @"保内";
    //    }else{
    //        baonei.text = @"保外";
    //    }
    //    [self.scrollView addSubview:baonei];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left1, produrt.bottom+10, KWIDTH-left1-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line3];
    
    //维修增加故障原因
    if ([self.mymodel.fault_type isEqualToString:@"3"]) {
        
        UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
        faultReasonL.font = FontSize(14);
        faultReasonL.textColor = K999999;
        faultReasonL.text = @"故障原因";
        [self.scrollView addSubview:faultReasonL];
        
        UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
        faultReason.font = KFontPingFangSCMedium(14);
        faultReason.textColor = K666666;
        faultReason.text = _mymodel.common_failures;
        [self.scrollView addSubview:faultReason];
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
        line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line];
        
        UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
        guzhang.font = FontSize(14);
        guzhang.textColor = K999999;
        guzhang.text = @"备注信息";
        [self.scrollView addSubview:guzhang];
        
        CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
        
        UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
        guzhanglb.font = FontSize(14);
        guzhanglb.textColor = K999999;
        guzhanglb.text = _mymodel.fault_comment;
        guzhanglb.numberOfLines = 0;
        [self.scrollView addSubview:guzhanglb];
        
        heig = 14;
        UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
        line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line6];
        
        UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
        address.font = FontSize(14);
        address.textColor = K999999;
        address.text = @"地址";
        [self.scrollView addSubview:address];
        
        CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
        
        UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
        addresslb.font = KFontPingFangSCMedium(14);
        addresslb.textColor = K999999;
        addresslb.text = _mymodel.user_address;
        addresslb.numberOfLines = 0;
        [self.scrollView addSubview:addresslb];
        
        UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
        line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line11];
        
        //        UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+12, 30, 14)];
        //        userLab.font = FontSize(14);
        //        userLab.textColor = K999999;
        //        userLab.text = @"用户";
        //        [self.scrollView addSubview:userLab];
        //
        //        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+12, 60, 14)];
        //        name.font = FontSize(14);
        //        name.textColor = K666666;
        //        name.text = _mymodel.user_name;
        //        [self.scrollView addSubview:name];
        //
        //        UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+10, line11.bottom+12, 75, 14)];
        //        phoneA.font = FontSize(14);
        //        phoneA.textColor = K999999;
        //        phoneA.text = @"用户电话";
        //        [self.scrollView addSubview:phoneA];
        //
        //        UIButton *phoneB = [UIButton new];
        //        //phoneB.frame = CGRectMake(phone.right+5, phone.centerY-30/2, 30, 30);
        //        phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
        //        [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
        //        [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
        //        [self.scrollView addSubview:phoneB];
        //
        //        //UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(name.right, addresslb.bottom+12, KWIDTH-name.right-16-10, 14)];
        //        UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+12, KWIDTH-20-67-60-70-30-10-15, 14)];
        //        phone.font = FontSize(14);
        //        phone.textColor = K666666;
        //        phone.textAlignment = NSTextAlignmentLeft;
        //        phone.text = _mymodel.user_phone;
        //        [self.scrollView addSubview:phone];
        //
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
        //        phone.userInteractionEnabled = YES;
        //        [phone addGestureRecognizer:tap];
        
        //        UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+10, KWIDTH-left1-20, 1)];
        //        line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        //        [self.scrollView addSubview:line7];
        
        UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+7, 30, 14)];
        dianti.font = FontSize(14);
        dianti.textColor = K999999;
        dianti.text = @"电梯";
        [self.scrollView addSubview:dianti];
        
        UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+7, 30, 14)];
        diantilb.font = KFontPingFangSCMedium(14);
        diantilb.textColor = K666666;
        diantilb.text = @"无";
        if ([_mymodel.has_elevator integerValue] == 1) {
            diantilb.text = @"有";
        }
        [self.scrollView addSubview:diantilb];
        
        //UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-100, line7.bottom+7, 30, 14)];
        UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line11.bottom+7, 30, 14)];
        louceng.font = FontSize(14);
        louceng.textColor = K999999;
        louceng.text = @"楼层";
        [self.scrollView addSubview:louceng];
        
        //UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-55, line7.bottom+7, 30, 14)];
        UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line11.bottom+7, 35, 14)];
        loucenglb.font = KFontPingFangSCMedium(14);
        loucenglb.textColor = K666666;
        loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
        [self.scrollView addSubview:loucenglb];
        loucenglb.textAlignment = NSTextAlignmentCenter;
        
        UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
        line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line8];
        
        if ([_mymodel.order_type_id isEqualToString:@"1"]) {
            
            /*
             UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
             faultReasonL.font = FontSize(14);
             faultReasonL.textColor = K999999;
             faultReasonL.text = @"故障原因";
             [self.scrollView addSubview:faultReasonL];
             
             UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
             faultReason.font = FontSize(14);
             faultReason.textColor = K666666;
             faultReason.text = _mymodel.common_failures;
             [self.scrollView addSubview:faultReason];
             
             UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
             line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
             [self.scrollView addSubview:line];
             */
            UILabel *smfTitLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+8, 90, 14)];
            smfTitLab.text = @"上门费";
            smfTitLab.font = FontSize(14);
            smfTitLab.textColor = K999999;
            [self.scrollView addSubview:smfTitLab];
            
            UILabel *smfConLab = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
            smfConLab.font = KFontPingFangSCMedium(14);
            smfConLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
            NSString *fixedPriceStr = [NSString stringWithFormat:@"%.2f",[_mymodel.fixed_price floatValue]];
            smfConLab.text = [NSString stringWithFormat:@"%@%@", @"¥", fixedPriceStr];
            [self.scrollView addSubview:smfConLab];
            
            //            UILabel *line21 = [[UILabel alloc]initWithFrame:CGRectMake(left1, smfTitLab.bottom+10, KWIDTH-left1-20, 1)];
            //            line21.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            //            [self.scrollView addSubview:line21];
            
            bbgvieww.frame = CGRectMake(10, 11, KWIDTH-20, smfConLab.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
        } else {
            bbgvieww.frame = CGRectMake(10, 11, KWIDTH-20, line8.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
        }
        
        //        bbgvieww.frame = CGRectMake(10, 11, KWIDTH-20, line8.bottom+20);
        //        bbgvieww.backgroundColor = [UIColor whiteColor];
        
        self.ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(25, bbgvieww.bottom+10, KWIDTH-50, 17)];
        _ordernumLB.font = FontSize(12);
        _ordernumLB.textColor = K999999;
        _ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
        [self.scrollView addSubview:_ordernumLB];
        
        UILabel *wenxintishi = [[UILabel alloc]initWithFrame:CGRectMake(26,  self.ordernumLB.bottom+20, KWIDTH-52, 40)];
        wenxintishi.font = FontSize(14);
        wenxintishi.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        wenxintishi.numberOfLines = 0;
        wenxintishi.text = @"温馨提示:此单是厂商为客户下的维修单，具体维修事宜请您电话联系客户了解！";
        [self.scrollView addSubview:wenxintishi];
        
        CGFloat  wwww = (KWIDTH-160-160)/3;
        self.jiedanBUt = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.jiedanBUt.frame = CGRectMake(wwww, wenxintishi.bottom+34, 160, 48);
        //[self.jiedanBUt setTitle:@"填写预约信息" forState:(UIControlStateNormal)];
        [self.jiedanBUt setTitle:@"接单" forState:(UIControlStateNormal)];
        [self.jiedanBUt setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [self.jiedanBUt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.jiedanBUt addTarget:self action:@selector(jiedanction:) forControlEvents:(UIControlEventTouchUpInside)];
        self.jiedanBUt.layer.masksToBounds = YES;
        self.jiedanBUt.tag = 999;
        self.jiedanBUt.layer.cornerRadius = 4;
        [self.scrollView addSubview:self.jiedanBUt];
        
        self.huluebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.huluebut.frame = CGRectMake(wwww*2+160, wenxintishi.bottom+34, 160, 48);
        [self.huluebut setTitle:@"忽略" forState:(UIControlStateNormal)];
        [self.huluebut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [self.huluebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.huluebut addTarget:self action:@selector(rightbuttoNoaction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:self.huluebut];
        self.huluebut.layer.masksToBounds = YES;
        self.huluebut.layer.cornerRadius = 4;
        self.huluebut.tag = 888;
        
        if (self.isec) {
            self.jiedanBUt.hidden = YES;
            self.huluebut.hidden = YES;
        }
    }else {
        UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
        guzhang.font = FontSize(14);
        guzhang.textColor = K999999;
        guzhang.text = @"备注信息";
        [self.scrollView addSubview:guzhang];
        
        CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
        
        UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
        guzhanglb.font = FontSize(14);
        guzhanglb.textColor = K999999;
        guzhanglb.text = _mymodel.fault_comment;
        guzhanglb.numberOfLines = 0;
        [self.scrollView addSubview:guzhanglb];
        
        heig = 14;
        UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
        line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line6];
        
        UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
        address.font = FontSize(14);
        address.textColor = K999999;
        address.text = @"地址";
        [self.scrollView addSubview:address];
        
        CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
        UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
        addresslb.font = KFontPingFangSCMedium(14);
        addresslb.textColor = K999999;
        addresslb.text = _mymodel.user_address;
        addresslb.numberOfLines = 0;
        [self.scrollView addSubview:addresslb];
        
        UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
        line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line11];
        
        //        UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+12, 30, 14)];
        //        userLab.font = FontSize(14);
        //        userLab.textColor = K999999;
        //        userLab.text = @"用户";
        //        [self.scrollView addSubview:userLab];
        //
        //        UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+8, 60, 14)];
        //        name.font = FontSize(14);
        //        name.textColor = K666666;
        //        name.text = _mymodel.user_name;
        //        [self.scrollView addSubview:name];
        //
        //        UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+10, line11.bottom+8, 50, 14)];
        //        phoneA.font = FontSize(14);
        //        phoneA.textColor = K999999;
        //        phoneA.text = @"用户电话";
        //        [self.scrollView addSubview:phoneA];
        //
        //        UIButton *phoneB = [UIButton new];
        //        //phoneB.frame = CGRectMake(phone.right+5, phone.centerY-30/2, 30, 30);
        //        //phoneB.frame = CGRectMake(name.right+80, name.centerY-30/2, 30, 30);
        //        phoneB.frame = CGRectMake(phoneA.right+10, line11.bottom + 5, 30, 30);
        //        [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
        //        [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
        //        [self.scrollView addSubview:phoneB];
        //
        //        //UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(name.right, addresslb.bottom+12, KWIDTH-name.right-16-10, 14)];
        //        UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+10, line11.bottom+12, KWIDTH-20-67-60-70-30-10-15, 14)];
        //        phone.font = FontSize(14);
        //        phone.textColor = K666666;
        //        phone.textAlignment = NSTextAlignmentRight;
        //        phone.text = _mymodel.user_phone;
        //        [self.scrollView addSubview:phone];
        //
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
        //        phone.userInteractionEnabled = YES;
        //        [phone addGestureRecognizer:tap];
        //
        //        UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+10, KWIDTH-left1-20, 1)];
        //        line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        //        [self.scrollView addSubview:line7];
        
        
        UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+7, 30, 14)];
        dianti.font = FontSize(14);
        dianti.textColor = K999999;
        dianti.text = @"电梯";
        [self.scrollView addSubview:dianti];
        
        UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line11.bottom+7, 30, 14)];
        diantilb.font = KFontPingFangSCMedium(14);
        diantilb.textColor = K666666;
        diantilb.text = @"无";
        if ([_mymodel.has_elevator integerValue] == 1) {
            diantilb.text = @"有";
        }
        [self.scrollView addSubview:diantilb];
        
        //UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-100, line7.bottom+7, 30, 14)];
        UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line11.bottom+7, 30, 14)];
        louceng.font = FontSize(14);
        louceng.textColor = K999999;
        louceng.text = @"楼层";
        [self.scrollView addSubview:louceng];
        
        //UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-55, line7.bottom+7, 30, 14)];
        UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line11.bottom+7, 35, 14)];
        loucenglb.font = KFontPingFangSCMedium(14);
        loucenglb.textColor = K666666;
        loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
        [self.scrollView addSubview:loucenglb];
        loucenglb.textAlignment = NSTextAlignmentCenter;
        
        UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
        line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line8];
        
        bbgvieww.frame = CGRectMake(10, 11, KWIDTH-20, line8.bottom+20);
        bbgvieww.backgroundColor = [UIColor whiteColor];
        
        self.ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(25, bbgvieww.bottom+10, KWIDTH-50, 17)];
        _ordernumLB.font = FontSize(12);
        _ordernumLB.textColor = K999999;
        _ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
        [self.scrollView addSubview:_ordernumLB];
        
        UILabel *wenxintishi = [[UILabel alloc]initWithFrame:CGRectMake(26,  self.ordernumLB.bottom+20, KWIDTH-52, 40)];
        wenxintishi.font = FontSize(14);
        wenxintishi.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        wenxintishi.numberOfLines = 0;
        wenxintishi.text = @"温馨提示:此单是厂商为客户下的维修单，具体维修事宜请您电话联系客户了解！";
        [self.scrollView addSubview:wenxintishi];
        
        CGFloat  wwww = (KWIDTH-160-160)/3;
        self.jiedanBUt = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.jiedanBUt.frame = CGRectMake(wwww, wenxintishi.bottom+34, 160, 48);
        //[self.jiedanBUt setTitle:@"填写预约信息" forState:(UIControlStateNormal)];
        [self.jiedanBUt setTitle:@"接单" forState:(UIControlStateNormal)];
        [self.jiedanBUt setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [self.jiedanBUt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.jiedanBUt addTarget:self action:@selector(jiedanction:) forControlEvents:(UIControlEventTouchUpInside)];
        self.jiedanBUt.layer.masksToBounds = YES;
        self.jiedanBUt.tag = 999;
        self.jiedanBUt.layer.cornerRadius = 4;
        [self.scrollView addSubview:self.jiedanBUt];
        
        self.huluebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.huluebut.frame = CGRectMake(wwww*2+160, wenxintishi.bottom+34, 160, 48);
        [self.huluebut setTitle:@"忽略" forState:(UIControlStateNormal)];
        [self.huluebut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        [self.huluebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.huluebut addTarget:self action:@selector(rightbuttoNoaction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:self.huluebut];
        self.huluebut.layer.masksToBounds = YES;
        self.huluebut.layer.cornerRadius = 4;
        self.huluebut.tag = 888;
        
        
        if (self.isec) {
            self.jiedanBUt.hidden = YES;
            self.huluebut.hidden = YES;
        }
    }
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)rightbuttoNoaction:(UIButton *)but{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)jiedanction:(UIButton *)but{
    KMyLog(@"接单");
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"appId"] = NOTNIL(_mymodel.Id);
    param[@"user_type"] = NOTNIL(_userType);
    
    if ([self.mymodel.order_type_id isEqualToString:@"1"]) {
        param[@"order_type_id"] = @"1";
    } else {
        param[@"order_type_id"] = @"2";
    }
    kWeakSelf;
    
    [NetWorkTool POST:LlistGrab param:param success:^(id dic) {
        //ShowToastWithText(@"抢单成功! 请填写用户的预约信息");
        [self shwoBgviewsec];
        [singlTool shareSingTool].needReasfh = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsecjiedan];
        UIButton *buttt = (UIButton *)[self.view viewWithTag:999];
        UIButton *buttt88 = (UIButton *)[self.view viewWithTag:888];
        buttt88.hidden =YES;
        buttt.hidden = YES;
        if (!self->_timeLB) {
            [weakSelf showseleui];
        }
        
    } other:^(id dic) {
        ShowToastWithText(dic[@"msg"]);
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showseleui{
    self.istime = YES;
    self.jiedanBUt.hidden = YES;
    [self.jiedanBUt removeFromSuperview];
    self.huluebut.hidden = YES;
    [self.huluebut removeFromSuperview];
    
    UILabel *winingLB = [[UILabel alloc]initWithFrame:CGRectMake(26, _ordernumLB.bottom+20, KWIDTH-52, 38)];
    winingLB.font = FontSize(14);
    winingLB.textColor = KshiYellow;
    winingLB.numberOfLines = 2;
    //    winingLB.text = @"温馨提示:此单是厂商为客户下的维修单，具体维修事宜请您电话联系客户了解！";
    [self.scrollView addSubview:winingLB];
    
    UIView *cenbbgvieww = [[UIView alloc]initWithFrame:CGRectMake(10, winingLB.bottom+10, KWIDTH-20, 48)];
    cenbbgvieww.backgroundColor = [UIColor whiteColor];
    [cenbbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
    [self.scrollView addSubview:cenbbgvieww];
    
    UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(16, 17, 30, 14)];
    userLab.font = FontSize(14);
    userLab.textColor = K999999;
    userLab.text = @"用户";
    [cenbbgvieww addSubview:userLab];
    
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(67, 17, 60, 14)];
    name.font = FontSize(14);
    name.textColor = K666666;
    name.text = _mymodel.user_name;
    [cenbbgvieww addSubview:name];
    
    UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+5, 17, 75, 14)];
    phoneA.font = FontSize(14);
    phoneA.textColor = K999999;
    phoneA.text = @"用户手机号";
    [cenbbgvieww addSubview:phoneA];
    
    UIButton *phoneB = [UIButton new];
    //phoneB.frame = CGRectMake(phone.right+5, phone.centerY-30/2, 30, 30);
    phoneB.frame = CGRectMake(phoneA.right, 9, 30, 30);
    [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
    [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
    [cenbbgvieww addSubview:phoneB];
    
    //UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(name.right, addresslb.bottom+12, KWIDTH-name.right-16-10, 14)];
    UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, 17, KWIDTH-20-67-60-70-30-10-15, 14)];
    phone.font = FontSize(14);
    phone.textColor = K666666;
    phone.textAlignment = NSTextAlignmentLeft;
    phone.text = _mymodel.user_phone;
    [cenbbgvieww addSubview:phone];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
    phone.userInteractionEnabled = YES;
    [phone addGestureRecognizer:tap];
    
    UILabel *redLB = [[UILabel alloc]initWithFrame:CGRectMake(26, cenbbgvieww.bottom+20, KWIDTH-52, 38)];
    redLB.font = FontSize(12);
    redLB.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    redLB.numberOfLines = 2;
    redLB.text = @"请填写您与客户电话沟通的具体预约信息并点击提交!";
    [self.scrollView addSubview:redLB];
    
    UIView *dowbbgvieww = [[UIView alloc]initWithFrame:CGRectMake(10, redLB.bottom+10, KWIDTH-20, 130)];
    dowbbgvieww.backgroundColor = [UIColor whiteColor];
    [dowbbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
    [self.scrollView addSubview:dowbbgvieww];
    
    //    self.jiedanBUt.y = dowbbgvieww.bottom+27;
    //    [self.jiedanBUt setTitle:@"接单" forState:(UIControlStateNormal)];
    //    self.huluebut.y = dowbbgvieww.bottom+27;;
    
    CGFloat  wwww = (KWIDTH-160-160)/3;
    UIButton*leftbutu = [UIButton buttonWithType:(UIButtonTypeCustom)];
    leftbutu.frame = CGRectMake(wwww, dowbbgvieww.bottom+27, 160, 48);
    [leftbutu setTitle:@"接单" forState:(UIControlStateNormal)];
    [leftbutu setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [leftbutu setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftbutu addTarget:self action:@selector(jiedanction:) forControlEvents:(UIControlEventTouchUpInside)];
    leftbutu.layer.masksToBounds = YES;
    leftbutu.layer.cornerRadius = 4;
    [self.scrollView addSubview:leftbutu];
    
    UIButton*rightbutu  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    rightbutu.frame = CGRectMake(wwww*2+160, dowbbgvieww.bottom+27, 160, 48);
    [rightbutu setTitle:@"忽略" forState:(UIControlStateNormal)];
    [rightbutu setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [rightbutu setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [rightbutu addTarget:self action:@selector(rightbuttobuttoaction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.scrollView addSubview:rightbutu];
    rightbutu.layer.masksToBounds = YES;
    rightbutu.layer.cornerRadius = 4;
    
    UILabel *yuyuetiitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 17, KWIDTH-20, 16)];
    yuyuetiitle.font = FontSize(14);
    yuyuetiitle.textColor = K333333;
    yuyuetiitle.text = @"预约信息";
    yuyuetiitle.textAlignment = NSTextAlignmentCenter;
    [dowbbgvieww addSubview:yuyuetiitle];
    
    UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(15, 53, 60, 14)];
    yuyueTime.font = FontSize(14);
    yuyueTime.textColor = K666666;
    yuyueTime.text = @"上门日期";
    [dowbbgvieww addSubview:yuyueTime];
    
    UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(yuyueTime.right+14, 53, KWIDTH-85, 14)];
    yuyueTimelb.font = FontSize(14);
    yuyueTimelb.textColor = K999999;
    yuyueTimelb.text = @"请选择上门日期";
    self.riqiLB = yuyueTimelb;
    [dowbbgvieww addSubview:yuyueTimelb];
    
    UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(15, yuyueTimelb.bottom+10, KWIDTH-16-20, 1)];
    line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [dowbbgvieww addSubview:line5];
    
    UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(15, line5.bottom+8, 60, 14)];
    yuyueTime2.font = FontSize(14);
    yuyueTime2.textColor = K666666;
    yuyueTime2.text = @"上门时间";
    [dowbbgvieww addSubview:yuyueTime2];
    
    UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(yuyueTime2.right+14, line5.bottom+8, KWIDTH-16-20, 14)];
    yuyueTimelb2.font = FontSize(14);
    yuyueTimelb2.textColor = K999999;
    yuyueTimelb2.text = @"请选择上门时间";
    self.timeLB = yuyueTimelb2;
    [dowbbgvieww addSubview:yuyueTimelb2];
    
    UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(15, yuyueTime2.bottom+10, KWIDTH-16-20, 1)];
    line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [dowbbgvieww addSubview:line6];
    
    //    UILabel *beizhu = [[UILabel alloc]initWithFrame:CGRectMake(15, line6.bottom+10, 60, 16)];
    //    beizhu.font = FontSize(14);
    //    beizhu.textColor = K999999;
    //    beizhu.text = @"备注信息";
    ////    beizhu.textAlignment = NSTextAlignmentCenter;
    //    [dowbbgvieww addSubview:beizhu];
    //
    //    UITextView *beizhuTV = [[UITextView alloc]initWithFrame:CGRectMake(beizhu.right+14, line6.bottom+10, KWIDTH-beizhu.right-14-25, 78)];
    //
    //    beizhuTV.backgroundColor = [UIColor colorWithHexString:@"#E9E9E9"];
    //    beizhuTV.placeholder = @"请填写备注信息";
    //    beizhuTV.layer.masksToBounds = YES;
    //    beizhuTV.layer.cornerRadius = 4;
    //    [dowbbgvieww addSubview:beizhuTV];
    
    UIButton *upbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [dowbbgvieww addSubview:upbut];
    [upbut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [upbut setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -130)];
    [upbut addTarget:self action:@selector(seleriqi:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIButton *dowbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [dowbbgvieww addSubview:dowbut];
    [dowbut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [dowbut setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -130)];
    [dowbut addTarget:self action:@selector(seletime:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [upbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.width.offset(150);
        make.height.offset(30);
        make.top.offset(48);
    }];
    [dowbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.width.offset(150);
        make.height.offset(30);
        make.top.offset(80);
    }];
    
    //CGFloat  wwww = (KWIDTH-160-160)/3;
    UIButton*leftbutu1 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    leftbutu1.frame = CGRectMake(20, dowbbgvieww.bottom+27, KWIDTH-40, 48);
    [leftbutu1 setTitle:@"提交" forState:(UIControlStateNormal)];
    [leftbutu1 setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [leftbutu1 setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftbutu1 addTarget:self action:@selector(commit:) forControlEvents:(UIControlEventTouchUpInside)];
    leftbutu1.layer.masksToBounds = YES;
    leftbutu1.layer.cornerRadius = 4;
    [self.scrollView addSubview:leftbutu1];
    _scrollView.contentSize = CGSizeMake(KWIDTH, dowbbgvieww.bottom+40+kNaviHeight);
}

-(void)seleriqi:(UIButton *)but{
    
    KMyLog(@"选择日期");
    //[[UIApplication sharedApplication].keyWindow addSubview:self.bgViewRiqi];
    //新建日期选择器
    MHDatePicker *datePicker = [[MHDatePicker alloc] init];
    datePicker.isBeforeTime = NO;
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [datePicker didFinishSelectedDate:^(NSDate *selectedDate) {
        NSString *selectedDateStr = [formatter stringFromDate:selectedDate];
        self.timeStr = selectedDateStr;
        NSArray *myarray = [selectedDateStr componentsSeparatedByString:@"-"];
        self.riqiLB.text = [NSString stringWithFormat:@"%@年%@月%@日",myarray[0],myarray[1],myarray[2]];
        self.riqiLB.textColor = K333333;
        
        if ([self.timeLB.text isEqualToString:@""]) { //如果时间为空
            KMyLog(@"如果时间为空,则不做处理");
        } else { //如果时间不为空
            self.timeLB.text = @"请选择上门时间";
            self.timeLB.textColor = K999999;
        }
    }];
}

-(void)seletime:(UIButton *)but{
    KMyLog(@"选择日期");
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewtime];
}

-(void)rightbuttobuttoaction:(UIButton *)but {
    self.zhijiehuluo = [[TanKuangView alloc]initWithFrame:self.view.bounds];
    self.zhijiehuluo.upLable.text = @"您正在填写客户的预约信息，";
    self.zhijiehuluo.dowLable.text = @"确定返回？";
    
    //取消订单
    kWeakSelf;
    [[UIApplication sharedApplication].keyWindow addSubview: self.zhijiehuluo];
    self.zhijiehuluo.myblcok = ^(NSInteger ind) {
        if (ind == 0) {
            [weakSelf.zhijiehuluo removeFromSuperview];
        }else{
            [weakSelf.zhijiehuluo removeFromSuperview];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    };
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgViewRiqi = [[UIView alloc]init];
    self.bgViewRiqi.frame = self.view.bounds;
    self.bgViewRiqi.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewRiqi addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(211);
        make.width.offset(283);
        
    }];
    
    UIButton *jintianbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [jintianbut setTitle:@"今天" forState:(UIControlStateNormal)];
    [jintianbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [jintianbut setBackgroundColor:[UIColor whiteColor]];
    [jintianbut addTarget:self action:@selector(jintianAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:jintianbut];
    UIButton *mingtianbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [mingtianbut setTitle:@"明天" forState:(UIControlStateNormal)];
    [mingtianbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [mingtianbut setBackgroundColor:[UIColor whiteColor]];
    [mingtianbut addTarget:self action:@selector(mingtianbutAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:mingtianbut];
    self.oldbut = mingtianbut;
    [self.oldbut setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    
    UIButton *houtianbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [houtianbut setTitle:@"后天" forState:(UIControlStateNormal)];
    [houtianbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [houtianbut setBackgroundColor:[UIColor whiteColor]];
    [houtianbut addTarget:self action:@selector(houtianbutAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:houtianbut];
    [jintianbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(25);
        make.height.offset(16);
        make.left.right.offset(0);
        
    }];
    
    [mingtianbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(jintianbut.mas_bottom).offset(31);
        make.height.offset(16);
        make.left.right.offset(0);
    }];
    
    [houtianbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(mingtianbut.mas_bottom).offset(31);
        make.height.offset(16);
        make.left.right.offset(0);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(yesButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(50);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-50);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
}

-(void)jintianAction:(UIButton *)but{
    [self.oldbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    self.oldbut = but;
}

-(void)mingtianbutAction:(UIButton *)but{
    [self.oldbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    self.oldbut = but;
}

-(void)houtianbutAction:(UIButton *)but{
    [self.oldbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    self.oldbut = but;
}

-(void)ikenowAction:(UIButton *)but{
    KMyLog(@"取消");
    [_bgViewRiqi removeFromSuperview];
}

-(void)yesButAction:(UIButton *)but{
    KMyLog(@"确定");
    
    [_bgViewRiqi removeFromSuperview];
    _riqiLB.text = _oldbut.titleLabel.text;
    
    NSString *jintian = [self getCurrentTime];
    NSString *mingtian = [self GetTomorrowDay:[self dateFromString:jintian]];
    
    NSString *houtian = [self GetTomorrowDay:[self dateFromString:mingtian]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    KMyLog(@"打印一下日期~~~~~~~~~~ %@", dateTime);
    NSString *nowstr =[dateTime substringFromIndex:11];
    KMyLog(@"mmmmmmmmmmm%@",nowstr);
    nowstr = [nowstr substringToIndex:2];
    KMyLog(@"mmmmmmmmmmm%@",nowstr);
    KMyLog(@"mmmmmmmmmmm%@",_timeLB.text);
    _timeStr = _riqiLB.text;
    _riqiLB.textColor = K333333;
    if ([_riqiLB.text isEqualToString:@"今天"]) {
        NSInteger rimeind =[[_timeLB.text substringToIndex:2] integerValue];
        
        _riqiLB.text = jintian;
        if (![_timeLB.text isEqualToString:@"请选择上门时间"]) {
            if (rimeind < [nowstr integerValue]) {
                ShowToastWithText(@"上门时间必须大于当前时间");
                _riqiLB.text = @"请选择上门日期";
                _riqiLB.textColor = K999999;
            }
        }
    }else if ([_riqiLB.text isEqualToString:@"明天"]){
        _riqiLB.text = mingtian;
    }else{
        _riqiLB.text = houtian;
    }
    NSArray  *myarray = [_riqiLB.text componentsSeparatedByString:@"-"];
    _riqiLB.text =[NSString stringWithFormat:@"%@年%@月%@日",myarray[0],myarray[1],myarray[2]];
}

//获取当地时间
- (NSString *)newGetCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}

//获取当地时间
- (NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}

//将字符串转成NSDate类型
- (NSDate *)dateFromString:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

//传入今天的时间，返回明天的时间
- (NSString *)GetTomorrowDay:(NSDate *)aDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:aDate];
    [components setDay:([components day]+1)];
    
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd"];
    return [dateday stringFromDate:beginningOfWeek];
}

-(void)shwosecBgview{
    self.bgViewtime = [[UIView alloc]init];
    self.bgViewtime.frame = self.view.bounds;
    self.bgViewtime.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewtime addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(271);
        make.width.offset(317);
        
    }];
    
    //    UIButton *amBUt = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [amBUt setTitle:@"上午" forState:(UIControlStateNormal)];
    //    [amBUt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [amBUt setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //
    //    self.sanjiaoImage = [[UIImageView alloc]init];
    //    self.sanjiaoImage.frame = CGRectMake(KWIDTH/4-8.5, 51, 17, 10);
    //    self.sanjiaoImage.image = imgname(@"snajiaoWhite");
    //    [amBUt addTarget:self action:@selector(amBUtAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [whiteBGView addSubview:amBUt];
    //    self.oldbuttime = amBUt;
    //    [self.oldbuttime setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    //
    //    UIButton *pmBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [pmBut setTitle:@"下午" forState:(UIControlStateNormal)];
    //    [pmBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [pmBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //    [pmBut addTarget:self action:@selector(pmButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [whiteBGView addSubview:pmBut];
    //    [whiteBGView addSubview:self.sanjiaoImage];
    //
    //    [amBUt mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.offset(61);
    //        make.left.offset(0);
    //        make.width.offset(158.5);
    //        make.top.offset(0);
    //    }];
    //
    //    [pmBut mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.offset(61);
    //        make.right.offset(0);
    //        make.width.offset(158.5);
    //        make.top.offset(0);
    //    }];
    
    UILabel *topTitLab = [[UILabel alloc] init];
    topTitLab.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    topTitLab.text = @"点击时间段进行选择";
    topTitLab.textColor = [UIColor whiteColor];
    topTitLab.textAlignment = NSTextAlignmentCenter;
    topTitLab.font = FontSize(16);
    [whiteBGView addSubview:topTitLab];
    
    [topTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(61);
        make.left.offset(0);
        make.width.offset(317);
        make.top.offset(0);
    }];
    
    self.pickerView = [[UIPickerView alloc]init];
    self.pickerView.backgroundColor = [UIColor whiteColor];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    
    [whiteBGView addSubview:self.pickerView];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        //make.top.mas_equalTo(amBUt.mas_bottom).offset(15);
        make.top.mas_equalTo(topTitLab.mas_bottom).offset(15);
        make.height.offset(135);
    }];
    
    _nameArray = [NSArray arrayWithObjects:@"8:00 - 9:00",@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",@"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",nil];
    
    [self.pickerView reloadAllComponents];//刷新UIPickerView
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(timeikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(timeyesButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(61);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-61);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
}

//-(void)amBUtAction:(UIButton *)but{
//    //上午
////    _nameArray = [NSArray arrayWithObjects:@"0:00 - 1:00",@"1:00 - 2:00",@"2:00 - 3:00",@"3:00 - 4:00",@"4:00 - 5:00",@"5:00 - 6:00",@"6:00 - 7:00",@"7:00 - 8:00",@"8:00 - 9:00",@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",nil];
//
//    _nameArray = [NSArray arrayWithObjects:@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",nil];
//    [self.pickerView reloadAllComponents];//刷新UIPickerView
//    [self.oldbuttime setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
//    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
//    self.oldbut = but;
//    self.oldbuttime  = but;
//    self.sanjiaoImage.centerX = self.oldbuttime.centerX;
//}

//-(void)pmButAction:(UIButton *)but{
//    //下午
////    _nameArray = [NSArray arrayWithObjects: @"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",@"20:00 - 21:00",@"21:00 - 22:00",@"22:00 - 23:00",@"23:00 - 24:00",nil];
//
//     _nameArray = [NSArray arrayWithObjects: @"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",nil];
//
//    [self.pickerView reloadAllComponents];//刷新UIPickerView
//
//    [self.oldbuttime setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
//    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
//    self.oldbut = but;
//    self.oldbuttime  = but;
//    self.sanjiaoImage.centerX = self.oldbuttime.centerX;
//}

-(void)timeikenowAction:(UIButton *)but{
    //取消
    [_bgViewtime removeFromSuperview];
}

-(void)timeyesButAction:(UIButton *)but{
    //确定
    _timeLB.text = _seleTime;
    if (!_seleTime) {
        _timeLB.text = _nameArray[0];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    KMyLog(@"打印一下日期~~~~~~~~~~ %@", dateTime);
    NSString *nowstr =[dateTime substringFromIndex:11];
    KMyLog(@"mmmmmmmmmmm%@",nowstr);
    nowstr = [nowstr substringToIndex:2];
    KMyLog(@"mmmmmmmmmmm%@",nowstr);
    KMyLog(@"mmmmmmmmmmm%@",_timeLB.text);
    _timeLB.textColor = K333333;
    
    NSString *jintian = [self newGetCurrentTime];
    if ([self.riqiLB.text isEqualToString:jintian]) {
        NSInteger rimeind =[[_timeLB.text substringToIndex:2] integerValue];
        if (rimeind < [nowstr integerValue]) {
            ShowToastWithText(@"上门时间必须大于当前时间");
            _timeLB.text = @"请选择上门时间";
            _timeLB.textColor = K999999;
        }
    }
    [_bgViewtime removeFromSuperview];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//返回指定列的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_nameArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.0f;
}

- (CGFloat) pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return  317;
}

//显示的标题
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *str = [_nameArray objectAtIndex:row];
    return str;
}

//显示的标题字体、颜色等属性
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString *str = [_nameArray objectAtIndex:row];
    
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#999999"]} range:NSMakeRange(0, [AttributedString  length])];
    if (self.sencion0row == row) {
        [AttributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#F9AC19"]} range:NSMakeRange(0, [AttributedString  length])];
    }
    return AttributedString;
    
}//NS_AVAILABLE_IOS(6_0);
//被选择的行

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.sencion0row = row;
    [_pickerView reloadAllComponents];
    self.seleTime =[_nameArray objectAtIndex:row];
    NSLog(@"HANG%@",[_nameArray objectAtIndex:row]);
}

-(void)showloadingview{
    
    self.showbgView = [[UIView alloc]init];
    self.showbgView.frame = self.view.bounds;
    self.showbgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.showbgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(154);
        make.width.offset(235);
        
    }];
    
    UILabel *titile = [[UILabel alloc]init];
    titile.font = FontSize(16);
    titile.text = @"您正在进行忽略订单操作,";
    titile.textColor = [UIColor colorWithHexString:@"#333333"];
    titile.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:titile];
    self.upShowLB =titile;
    UILabel *msglb = [[UILabel alloc]init];
    msglb.font = FontSize(16);
    msglb.text = @"确定忽略订单？";/////
    msglb.textColor = [UIColor colorWithHexString:@"#333333"];
    msglb.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:msglb];
    
    [titile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(17);
        make.top.offset(20);
        
    }];
    [msglb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(20);
        make.top.offset(47);
        
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(quxiaocaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(yescaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(50);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-50);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
}

-(void)quxiaocaozuoAction:(UIButton *)but {
    [self.showbgView removeFromSuperview];
}

-(void)yescaozuoAction:(UIButton *)but{
    
    if ([_mymodel.isprivate_work integerValue]  == 1) {
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"appId"] = NOTNIL(_mymodel.Id);
        [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
            [self.navigationController popViewControllerAnimated:YES];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        KMyLog(@"rightbuttobuttoaction");
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/**
 弹出框的背景图  接单成功
 */
-(void)shwoBgviewjiedan {
    self.bgViewsecjiedan = [[UIView alloc]init];
    self.bgViewsecjiedan.frame = self.view.bounds;
    self.bgViewsecjiedan.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecjiedan addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"接单成功";
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*请滑动至底部联系用户并填写预约信息" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:topLable];
    topLable.font = FontSize(14);
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.attributedText = attStr;
    topLable.textAlignment = NSTextAlignmentCenter;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"如暂不填写之后请在我的订单处查看填写";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikeno:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikeno:(UIButton *)but{
    [_bgViewsecjiedan removeFromSuperview];
    //[self.navigationController popViewControllerAnimated:YES];
}

/**
 弹出框的背景图  提交
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"提交成功";
    
    NSMutableAttributedString *dowStr = [NSString getAttributedStringWithOriginalString:@"*可在我的订单查看此单" originalColor:K333333 originalFont:FontSize(14) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(14)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom + 16, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.attributedText = dowStr;
    DowLable.textAlignment = NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    if ([_mymodel.order_type_id isEqualToString:@"1"]) { //检测单
        //检测单详情
        ZYTestOrderDetailViewController *vc = [[ZYTestOrderDetailViewController alloc] init];
        NSString *strPush = @"1";
        vc.strPush = strPush;
        vc.cslistModel = self.mymodel;
        vc.idStr = self.mymodel.Id;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        //电器厂订单 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
        vc.orderID = self.mymodel.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    //[self.navigationController popViewControllerAnimated:YES];
    //self.navigationController.tabBarController.selectedIndex = 1;
    //[self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)commit:(UIButton *)but {
    if ([_riqiLB.text isEqualToString:@"请选择上门日期"]  ) {
        ShowToastWithText(@"请选择上门日期");
        return;
    }else if([_timeLB.text isEqualToString:@"请选择上门时间"]){
        ShowToastWithText(@"请选择上门时间");
        return;
    }
    
    KMyLog(@"提交");
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(_mymodel.Id);
    param[@"order_date"] = _timeStr;
    param[@"order_time"] = _timeLB.text;
    
    if ([self.mymodel.order_type_id isEqualToString:@"1"]) { //检测单
        param[@"order_type_id"] = NOTNIL(self.mymodel.order_type_id);
    } else { //普通单
        param[@"order_type_id"] = NOTNIL(self.mymodel.order_type_id);
    }
    
    kWeakSelf;
    [NetWorkTool POST:dqcUpdateOrderTime param:param success:^(id dic) {
        
        [singlTool shareSingTool].needReasfh = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}


@end
