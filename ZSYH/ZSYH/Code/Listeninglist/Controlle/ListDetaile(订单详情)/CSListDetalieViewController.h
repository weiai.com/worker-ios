//
//  CSListDetalieViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSListDetalieViewController : BaseViewController
@property(nonatomic,strong)CSlistModel *mymodel;
@end

NS_ASSUME_NONNULL_END
