//
//  CSListDetalieViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSListDetalieViewController.h"
#import "CSListDetaileTableViewCell.h"
#import "CSListDetaileModel.h"

@interface CSListDetalieViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSArray *titArr;
@property(nonatomic,strong)CSListDetaileModel *model;

@property(nonatomic,strong)NSDictionary *condic;

@end

@implementation CSListDetalieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    // Do any additional setup after loading the view.
    [self showBtton];
    [self request];
}

-(void)request{
    
    NSMutableDictionary *mu = [NSMutableDictionary dictionaryWithCapacity:1];
    mu[@"appOrderId"]= _mymodel.Id;
    
    kWeakSelf;
    [NetWorkTool POST:LlistenDetaile param:mu success:^(id dic) {
        KMyLog(@"__%@",dic);
        self.model = [[CSListDetaileModel alloc]init];
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        NSMutableDictionary *mydic =[[dic objectForKey:@"data"] mutableCopy];
        KMyLog(@"dic 有没有数据 %@ dic 有没有数据", dic);

        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"company_name"]];
        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"address_content"]];
        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"userPhone"]];
        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"fault_name"]];
        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"fault_comment"]];

        [weakSelf.mydateSource addObject: [mydic objectForKey:@"has_elevator"]];
        [weakSelf.mydateSource addObject: [mydic  objectForKey:@"order_date"]];

        self.condic = mydic;
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

/**
 创建接单n按钮
 */
-(void)showBtton{
    UIButton *ReceiptBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [ReceiptBut setTitle:@"接单" forState:(UIControlStateNormal)];
    [ReceiptBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [ReceiptBut setBackgroundColor:[UIColor colorWithHexString:@"#F9AC19"]];
    [self.view addSubview:ReceiptBut];
    [ReceiptBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-100);
        make.left.offset(KWIDTH/2-103);
        make.width.offset(206);
        make.height.offset(50);
    }];
    
    [ReceiptBut addTarget:self action:@selector(jiadenAxction:) forControlEvents:(UIControlEventTouchUpInside)];
}

-(void)jiadenAxction:(UIButton *)but{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"appId"] = [self.condic objectForKey:@"id"];
    [NetWorkTool POST:LlistGrab param:param success:^(id dic) {
        ShowToastWithText(@"抢单成功");
        [singlTool shareSingTool].needReasfh = YES;
        [self.navigationController popViewControllerAnimated:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSListDetaileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSListDetaileTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.leftLB.text = [self.titArr safeObjectAtIndex:indexPath.row];
    cell.rightLB.text = [self.mydateSource safeObjectAtIndex:indexPath.row];
    cell.userImage.hidden = YES;
    return cell;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSListDetaileTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSListDetaileTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

-(NSArray *)titArr{
    if (!_titArr) {
        _titArr = @[@"姓名",@"地址",@"电话",@"故障类型",@"故障详情说明",@"有无电梯",@"维修时间"];
    }
    return _titArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
