//
//  CSnewDQCViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/19.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewDQCViewController : BaseViewController
@property(nonatomic,strong)CSlistModel *mymodel;
@property(nonatomic,strong)NSString *urls;
@property(nonatomic,strong)NSString *userType;


@end

NS_ASSUME_NONNULL_END
