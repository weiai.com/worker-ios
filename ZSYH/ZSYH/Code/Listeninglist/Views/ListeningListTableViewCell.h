//
//  ListeningListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListeningListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UILabel *gander;

@property (weak, nonatomic) IBOutlet UILabel *guzhangType;
@property (weak, nonatomic) IBOutlet UILabel *addresslb;

@property (weak, nonatomic) IBOutlet UILabel *typeLB;
@property (weak, nonatomic) IBOutlet UIImageView *isdianiqc;
@property (weak, nonatomic) IBOutlet UILabel *userType;
@property (weak, nonatomic) IBOutlet UILabel *guaranteeTypeL;
@property (weak, nonatomic) IBOutlet UIImageView *yikoujiaImg;
@property (weak, nonatomic) IBOutlet UILabel *serviceType;
@property (weak, nonatomic) IBOutlet UILabel *elecCateLab;

-(void)refash:(CSlistModel *)model;

@end

NS_ASSUME_NONNULL_END
