//
//  HomeTableHeaderView.m
//  ZSYH
//
//  Created by 李英杰 on 2019/8/30.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "HomeTableHeaderView.h"
#import <SCAdView.h>
#import "CSBannerModel.h"
#import "BannerDetailsViewController.h"

@interface HomeTableHeaderView ()<SCAdViewDelegate>
@property (nonatomic, strong) SCAdView *scAdView;
@property (nonatomic, strong) UIView *pageControlBackView;
@property (nonatomic, strong) UIView *currentPageView;
@property (nonatomic, strong) NSMutableArray *bannerArr;
@property (nonatomic, strong) NSMutableArray *mu;
@property (nonatomic, assign) NSInteger index;
@end

@implementation HomeTableHeaderView

- (void)buildSubview {
        
    UIView *pageControlBackView = [[UIView alloc] init];
    pageControlBackView.frame = CGRectMake(0, 164, KWIDTH, 10);
    pageControlBackView.backgroundColor = [UIColor clearColor];
    self.pageControlBackView = pageControlBackView;
    [self addSubview:pageControlBackView];
    
    self.backgroundColor = [UIColor whiteColor];
    UIImageView *tipsImg = [[UIImageView alloc] initWithFrame:CGRectMake(16, 189, KWIDTH - 32, 25)];
    [tipsImg setImage:imgname(@"组 3200")];
    [self addSubview:tipsImg];
}

- (void)reloadPageControlViewWithCount:(NSInteger)count {
    [self.pageControlBackView removeAllSubviews];
    CGFloat viewWidth = 18;
    CGFloat viewSpace = 5;
    CGFloat viewHeight = 4;
    CGFloat viewTop = 10;
    CGFloat allWidth = viewWidth * count + (count - 1) * viewSpace;
    CGFloat left = KWIDTH / 2 - allWidth / 2;
    for (int i = 0; i < count; i++) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(left + (viewWidth + viewSpace) * i, viewTop, viewWidth, viewHeight);
        view.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
        view.layer.cornerRadius = viewHeight / 2;
        view.layer.masksToBounds = YES;
        [self.pageControlBackView addSubview:view];
        if (i == 0) {
            self.currentPageView = view;
            view.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
        }
    }
}

//加载网络数据
- (void)loadContent {
    self.bannerArr = [NSMutableArray arrayWithCapacity:1];
    self.bannerArr = self.data;
    self.mu = [NSMutableArray arrayWithCapacity:1];
    if (self.bannerArr.count == 0) {
        return;
    }
    for (CSBannerModel *model in self.bannerArr) {
        [self.mu addObject:model.pic_url];
    }
    [self.scAdView reloadWithDataArray:self.mu];
    [self reloadPageControlViewWithCount:self.bannerArr.count];
}

- (SCAdView *)scAdView {
    if (!_scAdView) {
        SCAdView *adView = [[SCAdView alloc] initWithBuilder:^(SCAdViewBuilder *builder) {
            builder.viewFrame = (CGRect){0, 0, KWIDTH, 174};
            builder.adItemSize = (CGSize){(KWIDTH - 60 * 2), 120};
            builder.minimumLineSpacing = 25;
            builder.secondaryItemMinAlpha = 1;
            builder.threeDimensionalScale = 1.2;
            builder.infiniteCycle = 2.f;//无线轮播时间
            builder.itemCellNibName = @"SCAdDemoCollectionViewCell";
        }];
        adView.backgroundColor = [UIColor clearColor];
        adView.delegate = self;
        [adView play];
        _scAdView = adView;
        [self addSubview:adView];
    }
    return _scAdView;
}

#pragma mark - 轮播代理
-(void)sc_didClickAd:(id)adModel{
    
    CSBannerModel *model = self.bannerArr[self.index];
    NSLog(@"听单页点击轮播图-->%ld",self.index);
    
    if (model.details.length == 0) {
        
    } else {
        BannerDetailsViewController *vc = [[BannerDetailsViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        vc.bannerId = model.id;
        vc.titleStr = model.title;
        vc.partsId = model.partsId;
        [[self viewController].navigationController pushViewController:vc animated:YES];
    }
}

- (void)sc_scrollToIndex:(NSInteger)index{
    //NSLog(@"sc_scrollToIndex-->%ld",index);
    self.index = index;
    NSArray *viewArray = self.pageControlBackView.subviews;
    self.currentPageView.backgroundColor = [UIColor colorWithHexString:@"#DFDFDF"];
    UIView *currentView = viewArray[index];
    currentView.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    self.currentPageView = currentView;
}

- (UIViewController *)viewController {
    for (UIView *next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end
