//
//  CSSHlistTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSSHlistTableViewCell.h"

@implementation CSSHlistTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgimage.layer.cornerRadius = 8;
    self.bgimage.layer.masksToBounds = YES;
    
    self.username.font = KFontPingFangSCMedium(9);
    self.ganderLb.font = KFontPingFangSCMedium(10);
    self.timeLB.font = KFontPingFangSCMedium(14);
    self.liyouLB.font = KFontPingFangSCMedium(14);
    self.serviceTimeTit.font = KFontPingFangSCMedium(14);
    self.serviceReasTit.font =  KFontPingFangSCMedium(14);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refash:(CSlistModel *)model{
    
    self.bgimage.backgroundColor = [UIColor colorWithHexString:@"#1AB394"];
    NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[model.limitTime componentsSeparatedByString:@"-"]];
    NSString *yesr =[NSString stringWithFormat:@"%ld",(long)[muarrt[0] integerValue]];
    NSString *mouth =[NSString stringWithFormat:@"%ld",(long)[muarrt[1] integerValue]];
    NSString *day =[NSString stringWithFormat:@"%ld",(long)[muarrt[2] integerValue]];
    
    self.timeLB.text = [NSString stringWithFormat:@"%@月%@日 %@",mouth,day,model.orderTime];
    self.liyouLB.text = [NSString stringWithFormat:@"%@",model.account];
    
    if ([model.user_type  integerValue] == 2) {
        //电器厂
        self.userImage.image = imgname(@"putongyonghu");
        self.username.text = @"电器厂";
    } else if ([model.user_type integerValue] == 1){
        //企业用户
        self.userImage.image = imgname(@"cQiyeyonghu");
        self.username.text = @"企业用户";
    } else {
        self.userImage.image = imgname(@"putongyonghu");
        self.username.text = @"个人用户";
    }
    //heweshouhouweixu 售后维修  henindekehu  你的客户
}

@end
