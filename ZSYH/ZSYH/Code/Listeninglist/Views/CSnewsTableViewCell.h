//
//  CSnewsTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSSHmainLIstModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSnewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myimage;

@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *yuyueTime;
@property (weak, nonatomic) IBOutlet UILabel *miaoshuLB;
@property (weak, nonatomic) IBOutlet UILabel *biaotiLB;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeTit;
@property (weak, nonatomic) IBOutlet UILabel *serviceQuerTit;

@property(nonatomic,copy)void (^myblcok)(NSString *str);
-(void)reashWith:(CSSHmainLIstModel *)model;


@end

NS_ASSUME_NONNULL_END
