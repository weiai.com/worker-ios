
//
//  CSnewsTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewsTableViewCell.h"

@implementation CSnewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.biaotiLB.font = KFontPingFangSCMedium(16);

    self.timeLB.font = KFontPingFangSCMedium(14);
    self.yuyueTime.font = KFontPingFangSCMedium(14);
    self.miaoshuLB.font = KFontPingFangSCMedium(14);
    self.serviceTimeTit.font = KFontPingFangSCMedium(14);
    self.serviceQuerTit.font = KFontPingFangSCMedium(14);

  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)lookcentent:(UIButton *)sender {
    if (self.myblcok) {
        self.myblcok(@"");
    }
}
-(void)reashWith:(CSSHmainLIstModel *)model{


//    NSString *timestr  = [self dateConversionTimeStamp:[self nsstringConversionNSDate:model.createDate]];
    
    
//    self.timeLB.text = [NSString timeAgoWithDate:timestr];


    self.yuyueTime.text = [NSString stringWithFormat:@"%@ %@",model.orderDate,model.orderTime];
    self.miaoshuLB.text = model.account;
    self.biaotiLB.text = @"售后维修消息";

    if ([model.isread integerValue] == 1) {
        self.myimage.image = imgname(@"isreadNo");
    }else{
        self.myimage.image = imgname(@"isreadyes");

        
    }
    
    NSString *timestr  = [self dateConversionTimeStamp:[self nsstringConversionNSDate:model.createDate]];
    
    
    self.timeLB.text = [NSString timeAgoWithDate:timestr];
    
    
}
-(NSDate *)nsstringConversionNSDate:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *datestr = [dateFormatter dateFromString:dateStr];
    return datestr;
}
-(NSString *)dateConversionTimeStamp:(NSDate *)date
{
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]*1000];
    return timeSp;
}



@end
