//
//  CSSHlistTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSSHlistTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *username;
@property (weak, nonatomic) IBOutlet UILabel *ganderLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *liyouLB;
@property (weak, nonatomic) IBOutlet UIImageView *bgimage;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeTit;
@property (weak, nonatomic) IBOutlet UILabel *serviceReasTit;

-(void)refash:(CSlistModel *)model;

@end

NS_ASSUME_NONNULL_END
