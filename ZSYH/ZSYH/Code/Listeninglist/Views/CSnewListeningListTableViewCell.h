//
//  CSnewListeningListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewListeningListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bgimage;
@property (weak, nonatomic) IBOutlet UIImageView *shouhou;

@property (weak, nonatomic) IBOutlet UIImageView *myimage;
@property (weak, nonatomic) IBOutlet UILabel *userType;
@property (weak, nonatomic) IBOutlet UILabel *gander;
@property (weak, nonatomic) IBOutlet UILabel *serviceType;
@property (weak, nonatomic) IBOutlet UILabel *prudoutType;

@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *namelb;

@property (weak, nonatomic) IBOutlet UILabel *phonelb;
@property (weak, nonatomic) IBOutlet UILabel *addressLB;
@property (weak, nonatomic) IBOutlet UILabel *serviceTypeTit;
@property (weak, nonatomic) IBOutlet UILabel *elecCateTit;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeTit;
-(void)refash:(CSlistModel *)model;

@end

NS_ASSUME_NONNULL_END
