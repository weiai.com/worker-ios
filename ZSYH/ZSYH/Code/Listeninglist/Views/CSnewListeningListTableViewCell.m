//
//  CSnewListeningListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewListeningListTableViewCell.h"

@implementation CSnewListeningListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bgimage.layer.cornerRadius = 8;
    self.bgimage.layer.masksToBounds = YES;
    
    self.userType.font = KFontPingFangSCMedium(9);
    self.gander.font = KFontPingFangSCMedium(9);
    self.serviceType.font = KFontPingFangSCMedium(12);
    
    self.timeLB.font = KFontPingFangSCMedium(12);
    self.namelb.font = KFontPingFangSCMedium(12);
    self.phonelb.font = KFontPingFangSCMedium(12);
    self.addressLB.font = KFontPingFangSCMedium(12);
    self.prudoutType.font = KFontPingFangSCMedium(12);
    
    self.serviceTypeTit.font = KFontPingFangSCMedium(12);
    self.serviceTimeTit.font = KFontPingFangSCMedium(12);
    self.elecCateTit.font = KFontPingFangSCMedium(12);

    /*
     @property (weak, nonatomic) IBOutlet UILabel *userType;
     @property (weak, nonatomic) IBOutlet UILabel *gander;
     @property (weak, nonatomic) IBOutlet UILabel *serviceType;
     @property (weak, nonatomic) IBOutlet UILabel *prudoutType;
     
     @property (weak, nonatomic) IBOutlet UILabel *timeLB;
     @property (weak, nonatomic) IBOutlet UILabel *namelb;
     
     @property (weak, nonatomic) IBOutlet UILabel *phonelb;
     @property (weak, nonatomic) IBOutlet UILabel *addressLB;
     */
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)refash:(CSlistModel *)model{
    
    self.addressLB.text = [NSString stringWithFormat:@"%@",model.user_address];
    self.namelb.text = [NSString stringWithFormat:@"%@",model.user_name];
    self.phonelb.text = [NSString stringWithFormat:@"%@",model.user_phone];
    //self.phonelb.text = [NSString stringWithFormat:@"%@*****%@",[model.user_phone substringToIndex:3],[model.user_phone substringFromIndex:7]];
    
    self.gander.text = [NSString stringWithFormat:@"信用分:%@",model.credit_score];
    self.serviceType.text = [NSString stringWithFormat:@"%@",model.service_name];
    self.prudoutType.text = [NSString stringWithFormat:@"%@",model.fault_name];
    self.timeLB.text = [NSString stringWithFormat:@"%@ %@",model.order_date,model.order_time];
    
    NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[model.order_date componentsSeparatedByString:@"-"]];
    NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
    NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
    
    self.timeLB.text = [NSString stringWithFormat:@"%@月%@日 %@",mouth,day,model.order_time];
    
    if ([model.user_type  integerValue] == 2) {
        //电器厂
        self.myimage.image = imgname(@"putongyonghu");
        self.userType.text = @"电器厂";
        
    } else if ([model.user_type integerValue] == 1){
        //企业用户
        self.myimage.image = imgname(@"cQiyeyonghu");
        self.userType.text = @"企业用户";

    } else {
        self.myimage.image = imgname(@"putongyonghu");
        self.userType.text = @"个人用户";
    }
    //heweshouhouweixu 售后维修  henindekehu  你的客户
    
    self.shouhou.hidden = YES;
    
    if ([model.is_again_order integerValue] == 1 ) {
        self.shouhou.hidden = NO;
        self.shouhou.image = imgname(@"heweshouhouweixu");
    }
    
    if ([model.isprivate_work integerValue]  == 1) {
        self.shouhou.hidden = NO;
        
        self.shouhou.image = imgname(@"henindekehu");
    }
}

@end
