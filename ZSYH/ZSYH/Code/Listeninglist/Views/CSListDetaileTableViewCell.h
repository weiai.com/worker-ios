//
//  CSListDetaileTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSListDetaileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLB;
@property (weak, nonatomic) IBOutlet UILabel *rightLB;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myImage;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@end

NS_ASSUME_NONNULL_END
