//
//  ListeningListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ListeningListTableViewCell.h"

@implementation ListeningListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.bgView.layer.cornerRadius = 8;
    self.bgView.layer.masksToBounds = YES;
   // 2.如果是四个角中的某几个角,一个,两个,或者3个,代码示例(切的左下,和右下):
   /*UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bgView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bgView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.bgView.layer.mask = maskLayer;*/

    self.userName.font = KFontPingFangSCMedium(12);
    self.phone.font = KFontPingFangSCMedium(12);
    self.gander.font = KFontPingFangSCMedium(9);
    self.guzhangType.font = KFontPingFangSCMedium(12);
    self.addresslb.font = KFontPingFangSCMedium(12);
    self.typeLB.font = KFontPingFangSCMedium(12);
    self.userType.font = KFontPingFangSCMedium(9);
    self.guaranteeTypeL.font = KFontPingFangSCMedium(12);
    self.serviceType.font = KFontPingFangSCMedium(12);
    self.elecCateLab.font = KFontPingFangSCMedium(12);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refash:(CSlistModel *)model{
    
    self.addresslb.text = [NSString stringWithFormat:@"%@",model.user_address];
    self.userName.text = [NSString stringWithFormat:@"%@",model.company_name];
    //self.phone.text = [NSString stringWithFormat:@"%@*****%@",[model.user_phone substringToIndex:3],[model.user_phone substringFromIndex:7]];
    self.gander.text = [NSString stringWithFormat:@"信用分:%@",model.credit_score];
    self.guzhangType.text = [NSString stringWithFormat:@"%@",model.service_name];
    self.typeLB.text = [NSString stringWithFormat:@"%@",model.fault_name];
    
    if ([model.order_type_id isEqualToString:@"1"]) { //检测单
        self.isdianiqc.image = [UIImage imageNamed:@"jiancedanlogo"];
        self.userImage.image = [UIImage imageNamed:@"jiancedanimg"];
    } else { //电器厂家订单
        self.isdianiqc.image = [UIImage imageNamed:@"Cdianqichang"];
        self.userImage.image = [UIImage imageNamed:@"LisIHeader"];
    }
    
    if ([model.fault_type isEqualToString:@"1"]) {
        self.yikoujiaImg.image = [UIImage imageNamed:@"组 2957"];
    } else {
        self.yikoujiaImg.image = [UIImage imageNamed:@""];
    }
    
    if ([model.fault_type isEqualToString:@"3"]) {
        if ([model.is_protect boolValue]) {
            if ([model.order_type_id isEqualToString:@"1"]) { //检测单
                self.guaranteeTypeL.hidden = YES;
                self.yikoujiaImg.image = [UIImage imageNamed:@"shangmenfei"];
            } else {
                self.guaranteeTypeL.text = @"保内";
                self.yikoujiaImg.image = [UIImage imageNamed:@"组 2957"];
            }
        } else {
            if ([model.order_type_id isEqualToString:@"1"]) {
                self.guaranteeTypeL.hidden = YES;
                self.yikoujiaImg.image = [UIImage imageNamed:@"shangmenfei"];
            } else {
                self.guaranteeTypeL.text = @"保外";
                self.yikoujiaImg.image = [UIImage imageNamed:@""];
            }
        }
    }else {
        self.guaranteeTypeL.text = @"";
    }
//    if ([model.is_protect boolValue]) {
//        self.guaranteeTypeL.text = @"保内";
//    }else {
//        self.guaranteeTypeL.text = @"保外";
//    }
    self.userType.text = @"电器厂家";
}

@end
