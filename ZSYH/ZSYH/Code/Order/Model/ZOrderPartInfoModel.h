//
//  ZOrderPartInfoModel.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//
// 我的订单 -> 配件信息model

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZOrderPartInfoModel : BaseModel
@property (nonatomic, copy) NSString *type_name;//配件品类
@property (nonatomic, copy) NSString *parts_name;//配件名称
@property (nonatomic, copy) NSString *productionDate;//有效期开始
@property (nonatomic, copy) NSString *termValidity;//有效期结束
@property (nonatomic, copy) NSString *install;//没有有效期开始和结束 使用该字段

@property (nonatomic, copy) NSString *warranty;//质保期
@property (nonatomic, copy) NSString *state;//1正常使用2损坏3隐藏添加配件按钮
@property (nonatomic, copy) NSString *quantity;//
@property (nonatomic, copy) NSString *salePrice;//
@property (nonatomic, copy) NSString *qrcode;//

@property (nonatomic, copy) NSString *partsId;//
@property (nonatomic, copy) NSString *count;//
@property (nonatomic, copy) NSString *product_type_id;//
@property (nonatomic, copy) NSString *type;//
@property (nonatomic, copy) NSString *compensationPrice;//

@property (nonatomic, copy) NSString *contractPrice;//
@property (nonatomic, copy) NSString *isaddnew;//
@property (nonatomic, copy) NSString *name;//
@property (nonatomic, copy) NSString *order_id;//
@property (nonatomic, copy) NSString *product_model;//

@property (nonatomic, copy) NSString *createDate;//
@property (nonatomic, copy) NSString *soft;//

@property (nonatomic, copy) NSString *parts_brand;
@property (nonatomic, copy) NSString *lower_user;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *parts_details;
@property (nonatomic, copy) NSString *parts_number;

@property (nonatomic, copy) NSString *azTime;
@property (nonatomic, copy) NSString *lower_agentB;
@property (nonatomic, copy) NSString *repProId;
@property (nonatomic, copy) NSString *is_today_hot;
@property (nonatomic, copy) NSString *is_hot_push;

@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *classification;
@property (nonatomic, copy) NSString *lower_agentA;
@property (nonatomic, copy) NSString *factory_id;
@property (nonatomic, copy) NSString *parts_model;

@property (nonatomic, copy) NSString *agent_user_price;
@property (nonatomic, copy) NSString *parts_quantity;
@property (nonatomic, copy) NSString *parts_place;
@property (nonatomic, copy) NSString *shTime;
@property (nonatomic, copy) NSString *parts_type;

@property (nonatomic, copy) NSString *is_good_choice;
@property (nonatomic, copy) NSString *is_new_push;
@property (nonatomic, copy) NSString *modifyDate;
@property (nonatomic, copy) NSString *parts_price;
@property (nonatomic, copy) NSString *repair_user_price;

@property (nonatomic, copy) NSString *parts_remarks;

@end

NS_ASSUME_NONNULL_END
