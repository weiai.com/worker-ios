//
//  ZOrderAfterSalePartInfoModel.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 售后维修 -> 配件信息model

#import "BaseModel.h"
#import "ZOrderPartInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZOrderAfterSalePartInfoModel : BaseModel
@property (nonatomic, copy) NSString *prodcutComment;//配件名称字段(顶部展示)
@property (nonatomic, strong) NSArray <ZOrderPartInfoModel *> *pros;//该订单使用的配件列表
@end

NS_ASSUME_NONNULL_END
