//
//  ZOrderAfterSalePartInfoModel.m
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderAfterSalePartInfoModel.h"

@implementation ZOrderAfterSalePartInfoModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"pros":[ZOrderPartInfoModel class]
             };
}
@end
