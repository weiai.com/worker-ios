//
//  OrderViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderViewController : UIViewController

- (void)change:(UISegmentedControl *)sender;

@end

NS_ASSUME_NONNULL_END
