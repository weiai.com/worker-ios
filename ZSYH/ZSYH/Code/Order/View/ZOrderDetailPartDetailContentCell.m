//
//  ZOrderDetailPartDetailContentCell.m
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderDetailPartDetailContentCell.h"
#import "CSmaintenanceListModel.h"

@interface ZOrderDetailPartDetailContentCell ()
@property (nonatomic, strong) UILabel *lblDate;//添加时间
@property (nonatomic, strong) UILabel *lblContent;//内容
@end

@implementation ZOrderDetailPartDetailContentCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    [self.contentView addSubview:self.lblDate];
    [self.lblDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(16);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-16);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.height.mas_equalTo(20);
    }];
    
    UIView *viewBg = [UIView new];
    viewBg.layer.cornerRadius = 12;
    viewBg.layer.masksToBounds = YES;
    viewBg.layer.borderWidth = 1;
    viewBg.layer.borderColor = kColorWithHex(0x70be68).CGColor;
    [self.contentView addSubview:viewBg];
    [viewBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(10);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-10);
        make.top.equalTo(self.lblDate.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-11);
    }];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = @"配件名称";
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblTitle];
    [lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewBg.mas_top).offset(24);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.height.mas_equalTo(20);
    }];
    
    UIView *viewBgSmall = [UIView new];
    viewBgSmall.layer.cornerRadius = 8;
    viewBgSmall.layer.masksToBounds = YES;
    viewBgSmall.layer.borderWidth = 1;
    viewBgSmall.layer.borderColor = kColorWithHex(0xdbdbdb).CGColor;
    [self.contentView addSubview:viewBgSmall];
    [viewBgSmall mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(viewBg.mas_leading).offset(34);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-31);
        make.top.equalTo(lblTitle.mas_bottom).offset(5);
        make.bottom.equalTo(viewBg.mas_bottom).offset(-51);
    }];
    
    [self.contentView addSubview:self.lblContent];
    [self.lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(viewBgSmall.mas_leading).offset(15);
        make.trailing.equalTo(viewBgSmall.mas_trailing).offset(-15);
        make.top.equalTo(viewBgSmall.mas_top).offset(12);
        make.bottom.equalTo(viewBgSmall.mas_bottom).offset(-32);
    }];
    
    UIView *viewLine = [UIView new];
    viewLine.backgroundColor = kColorWithHex(0xf2f2f2);
    [self.contentView addSubview:viewLine];
    [viewLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
}
#pragma mark - 数据更新

-(void)setInfoPartInfoModel:(ZOrderInfoPartInfoModel *)infoPartInfoModel
{
    if (infoPartInfoModel) {
        _infoPartInfoModel = infoPartInfoModel;
        self.lblDate.text = [NSString stringWithFormat:@"添加时间 %@",_infoPartInfoModel.create_time];
        self.lblContent.text = _infoPartInfoModel.product_name;
    }
}

- (void)setOrderModel:(CSmaintenanceListModel *)orderModel{
    if (orderModel) {
        _orderModel = orderModel;
        self.lblDate.text = [NSString stringWithFormat:@"添加时间 %@",orderModel.createTime];
        self.lblContent.text = orderModel.prodcutComment;
    }
}
#pragma mark - 懒加载
- (UILabel *)lblDate{
    if (!_lblDate) {
        _lblDate = [UILabel new];
        _lblDate.font = [UIFont boldSystemFontOfSize:14];
        _lblDate.textColor = kColorWithHex(0x333333);
    }
    return _lblDate;
}
- (UILabel *)lblContent{
    if (!_lblContent) {
        _lblContent = [UILabel new];
        _lblContent.font = [UIFont systemFontOfSize:12];
        _lblContent.textColor = kColorWithHex(0x999999);
        _lblContent.preferredMaxLayoutWidth = KScreenWidth - 115;
        _lblContent.numberOfLines = 0;
    }
    return _lblContent;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
