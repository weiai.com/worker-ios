//
//  ZOrderAfterSalePartAddAlertView.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 二次维修 -> 更换配件 -> 弹窗提示view

typedef NS_ENUM(NSUInteger, AlertViewType) {
    AlertViewTypeMakeSure = 0,      //确认是否损坏
    AlertViewTypeAddPart            //添加配件
};
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol ZOrderAfterSalePartAddAlertViewDelegate <NSObject>
- (void)partAddAlertViewBtnClickWith:(AlertViewType)viewType withAction:(NSInteger)action;
@end

@interface ZOrderAfterSalePartAddAlertView : UIView
@property (nonatomic, assign) AlertViewType viewType;
@property (nonatomic, weak) id <ZOrderAfterSalePartAddAlertViewDelegate> delegate;
@property (nonatomic, strong) NSString *repProId;
- (void)showView;//显示
- (void)dismissView;//消失
@end

NS_ASSUME_NONNULL_END
