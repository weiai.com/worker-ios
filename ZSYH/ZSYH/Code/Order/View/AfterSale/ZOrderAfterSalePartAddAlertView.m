//
//  ZOrderAfterSalePartAddAlertView.m
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderAfterSalePartAddAlertView.h"

@interface ZOrderAfterSalePartAddAlertView ()
@property (nonatomic, strong) UILabel *lblContent;
@property (nonatomic, strong) UIButton *btnLeft;
@property (nonatomic, strong) UIButton *btnRight;
@end

@implementation ZOrderAfterSalePartAddAlertView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViewAction];
        self.backgroundColor = RGBA(1, 1, 1, 0.5);
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    UIView *viewCenter = [UIView new];
    viewCenter.backgroundColor = kColorWithHex(0xffffff);
    viewCenter.layer.cornerRadius = 6.0;
    viewCenter.layer.masksToBounds = YES;
    [self addSubview:viewCenter];
    [viewCenter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kScaleNum(331), kScaleNum(239)));
    }];
    
    UIImageView *ivNotice = [UIImageView new];
    ivNotice.image = imgname(@"order_notice_icon");
    [viewCenter addSubview:ivNotice];
    [ivNotice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewCenter);
        make.top.equalTo(viewCenter.mas_top).offset(34);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    
    [viewCenter addSubview:self.lblContent];
    [self.lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewCenter);
        make.top.equalTo(ivNotice.mas_bottom).offset(19);
    }];
    
    [viewCenter addSubview:self.btnLeft];
    [self.btnLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(viewCenter.mas_leading).offset(30);
        make.bottom.equalTo(viewCenter.mas_bottom).offset(-20);
        make.size.mas_equalTo(CGSizeMake(110, 36));
    }];
    
    [viewCenter addSubview:self.btnRight];
    [self.btnRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(viewCenter.mas_trailing).offset(-30);
        make.bottom.equalTo(viewCenter.mas_bottom).offset(-20);
        make.size.mas_equalTo(CGSizeMake(110, 36));
    }];
}
- (void)setViewType:(AlertViewType)viewType{
    _viewType = viewType;
    if (viewType == AlertViewTypeMakeSure) {
        self.backgroundColor = [kColorWithHex(0xb1b0af) colorWithAlphaComponent:0.85];
        
        [self.btnLeft setBackgroundColor:kColorWithHex(0xf2f2f2)];
        [self.btnLeft setTitle:@"取消" forState:UIControlStateNormal];
        [self.btnLeft setTitleColor:kColorWithHex(0x999999) forState:UIControlStateNormal];
        
        [self.btnRight setBackgroundColor:kColorWithHex(0x70be68)];
        [self.btnRight setTitle:@"确定" forState:UIControlStateNormal];
        [self.btnRight setTitleColor:kColorWithHex(0xffffff) forState:UIControlStateNormal];
        
        self.lblContent.text = @"确认该候保配件已损坏？";
        
    }else if (viewType == AlertViewTypeAddPart){
        self.backgroundColor = kColorWithHex(0xf2f2f2);
        
        [self.btnLeft setBackgroundColor:kColorWithHex(0xffffff)];
        [self.btnLeft setTitle:@"不是" forState:UIControlStateNormal];
        [self.btnLeft setTitleColor:kColorWithHex(0x70be68) forState:UIControlStateNormal];
        self.btnLeft.layer.borderColor = kColorWithHex(0x70be68).CGColor;
        self.btnLeft.layer.borderWidth = 1;
        
        [self.btnRight setBackgroundColor:kColorWithHex(0x70be68)];
        [self.btnRight setTitle:@"是" forState:UIControlStateNormal];
        [self.btnRight setTitleColor:kColorWithHex(0xffffff) forState:UIControlStateNormal];
        
        self.lblContent.text = @"新配件是否为候保配件？";
    }
    
    
}
#pragma mark - 交互事件
//显示
- (void)showView{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    self.frame = window.bounds;
    [window addSubview:self];
}
//关闭
- (void)dismissView{
    [self removeFromSuperview];
}
- (void)btnClickAction:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(partAddAlertViewBtnClickWith:withAction:)]) {
        [self.delegate partAddAlertViewBtnClickWith:self.viewType withAction:tag];
    }
    [self dismissView];
}
#pragma mark - 懒加载
- (UILabel *)lblContent{
    if (!_lblContent) {
        _lblContent = [UILabel new];
        _lblContent.font = [UIFont systemFontOfSize:16];
        _lblContent.textColor = kColorWithHex(0x333333);
        _lblContent.textAlignment = NSTextAlignmentCenter;
    }
    return _lblContent;
}
- (UIButton *)btnLeft{
    if (!_btnLeft) {
        _btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnLeft.layer.cornerRadius = 4;
        _btnLeft.layer.masksToBounds = YES;
        _btnLeft.tag = 200;
        _btnLeft.titleLabel.font = [UIFont systemFontOfSize:14];
        [_btnLeft addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnLeft;
}
- (UIButton *)btnRight{
    if (!_btnRight) {
        _btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
        _btnRight.layer.cornerRadius = 4;
        _btnRight.layer.masksToBounds = YES;
        _btnRight.tag = 201;
        _btnRight.titleLabel.font = [UIFont systemFontOfSize:14];
        [_btnRight addTarget:self action:@selector(btnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnRight;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
