//
//  ZOrderAfterSaleChangePartNoticeCell.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 二次维修 -> 更换配件 -> 质保提示cell

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZOrderAfterSaleChangePartNoticeCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
