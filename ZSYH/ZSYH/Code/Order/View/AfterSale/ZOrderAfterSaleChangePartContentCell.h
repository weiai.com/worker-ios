//
//  ZOrderAfterSaleChangePartContentCell.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 二次维修 -> 更换配件 -> 维修内容cell

#import <UIKit/UIKit.h>
#import "ZOrderAfterSalePartInfoModel.h"

NS_ASSUME_NONNULL_BEGIN
@protocol ZOrderAfterSaleChangePartContentCellDelegate <NSObject>
//添加配件按钮
- (void)orderAfterSaleChangePartContentCellBtnClick;
@end

@interface ZOrderAfterSaleChangePartContentCell : UITableViewCell
@property (nonatomic, weak) id <ZOrderAfterSaleChangePartContentCellDelegate> delegate;
@property (nonatomic, strong) ZOrderAfterSalePartInfoModel *partInfoModel;
@end

NS_ASSUME_NONNULL_END
