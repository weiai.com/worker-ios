//
//  ZOrderAfterSaleChangePartPartInfoCell.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 二次维修 -> 更换配件 -> 配件信息cell

#import <UIKit/UIKit.h>
#import "ZOrderPartInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZOrderAfterSaleChangePartPartInfoCellDelegate <NSObject>
//按钮点击事件
- (void)orderAfterSaleChangePartPartInfoCellBtnClick:(ZOrderPartInfoModel *)model index:(NSIndexPath *)index;
@end

@interface ZOrderAfterSaleChangePartPartInfoCell : UITableViewCell
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) ZOrderPartInfoModel *partModel;
@property (nonatomic, weak) id <ZOrderAfterSaleChangePartPartInfoCellDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
