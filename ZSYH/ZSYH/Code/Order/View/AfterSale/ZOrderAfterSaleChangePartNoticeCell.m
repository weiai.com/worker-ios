//
//  ZOrderAfterSaleChangePartNoticeCell.m
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderAfterSaleChangePartNoticeCell.h"

@implementation ZOrderAfterSaleChangePartNoticeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    UILabel *lblNotice = [UILabel new];
    lblNotice.text = @"温馨提示：已损坏的配件如在质保期内，请携带损坏配件到购买该配件的配件点，进行更换并申请补助。";
    lblNotice.font = [UIFont systemFontOfSize:14];
    lblNotice.textColor = kColorWithHex(0xd84b4a);
    lblNotice.numberOfLines = 0;
    [self.contentView addSubview:lblNotice];
    [lblNotice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.leading.equalTo(self.contentView.mas_leading).offset(20);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-20);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
