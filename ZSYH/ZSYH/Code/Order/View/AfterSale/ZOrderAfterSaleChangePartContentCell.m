//
//  ZOrderAfterSaleChangePartContentCell.m
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderAfterSaleChangePartContentCell.h"
@interface ZOrderAfterSaleChangePartContentCell ()
@property (nonatomic, strong) UILabel *lblContent;//内容
@property (nonatomic, strong) UIButton *btnAdd;//添加配件按钮
@end

@implementation ZOrderAfterSaleChangePartContentCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    
    UIView *viewBg = [UIView new];
    viewBg.backgroundColor = kColorWithHex(0xffffff);
    viewBg.layer.cornerRadius = 12;
    viewBg.layer.masksToBounds = YES;
    viewBg.layer.borderWidth = 1;
    viewBg.layer.borderColor = kColorWithHex(0x70be68).CGColor;
    [self.contentView addSubview:viewBg];
    [viewBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.contentView.mas_leading).offset(10);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-10);
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = @"配件名称";
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblTitle];
    [lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewBg.mas_top).offset(24);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.height.mas_equalTo(20);
    }];
    
    UIView *viewBgSmall = [UIView new];
    viewBgSmall.layer.cornerRadius = 8;
    viewBgSmall.layer.masksToBounds = YES;
    viewBgSmall.layer.borderWidth = 1;
    viewBgSmall.layer.borderColor = kColorWithHex(0xdbdbdb).CGColor;
    [self.contentView addSubview:viewBgSmall];
    [viewBgSmall mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(viewBg.mas_leading).offset(34);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-31);
        make.top.equalTo(lblTitle.mas_bottom).offset(5);
        make.bottom.equalTo(viewBg.mas_bottom).offset(-51);
    }];
    
    [self.contentView addSubview:self.lblContent];
    [self.lblContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(viewBgSmall.mas_leading).offset(15);
        make.trailing.equalTo(viewBgSmall.mas_trailing).offset(15);
        make.top.equalTo(viewBgSmall.mas_top).offset(12);
        make.bottom.equalTo(viewBgSmall.mas_bottom).offset(-32);
    }];
    
    [self.contentView addSubview:self.btnAdd];
    [self.btnAdd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewBgSmall.mas_bottom).offset(10);
        make.bottom.equalTo(viewBg.mas_bottom).offset(-10);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-14);
        make.height.mas_equalTo(32);
    }];
    
}
#pragma mark - 数据更新
- (void)setPartInfoModel:(ZOrderAfterSalePartInfoModel *)partInfoModel{
    if (partInfoModel) {
        _partInfoModel = partInfoModel;
        self.lblContent.text = partInfoModel.prodcutComment;
    }
}
#pragma mark - 懒加载
- (UILabel *)lblContent{
    if (!_lblContent) {
        _lblContent = [UILabel new];
        _lblContent.font = [UIFont systemFontOfSize:12];
        _lblContent.textColor = kColorWithHex(0x999999);
        _lblContent.preferredMaxLayoutWidth = KScreenWidth - 115;
        _lblContent.numberOfLines = 0;
    }
    return _lblContent;
}
- (UIButton *)btnAdd{
    if (!_btnAdd) {
        _btnAdd = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnAdd setImage:imgname(@"shouhouaddnew") forState:UIControlStateNormal];
        [_btnAdd addTarget:self action:@selector(btnAddClickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnAdd;
}
#pragma mark - 交互事件
- (void)btnAddClickAction:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(orderAfterSaleChangePartContentCellBtnClick)]) {
        [self.delegate orderAfterSaleChangePartContentCellBtnClick];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
