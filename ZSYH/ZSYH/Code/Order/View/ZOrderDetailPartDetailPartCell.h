//
//  ZOrderDetailPartDetailPartCell.h
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 订单详情 -> 配件详情 -> 配件内容cell

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZOrderDetailPartDetailPartCell : UITableViewCell
@property (nonatomic, strong) ZOrderInfoPartInfoModel *partModel;
@end

NS_ASSUME_NONNULL_END
