//
//  ZOrderDetailPartDetailContentCell.h
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 订单详情 -> 配件详情 -> 配件名称cell(顶部)

#import <UIKit/UIKit.h>

@class CSmaintenanceListModel;
@class ZOrderInfoPartInfoModel;

NS_ASSUME_NONNULL_BEGIN

@interface ZOrderDetailPartDetailContentCell : UITableViewCell
@property (nonatomic, strong) CSmaintenanceListModel *orderModel;
@property (nonatomic, strong) ZOrderInfoPartInfoModel *infoPartInfoModel;

@end

NS_ASSUME_NONNULL_END
