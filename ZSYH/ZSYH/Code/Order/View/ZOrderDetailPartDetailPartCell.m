//
//  ZOrderDetailPartDetailPartCell.m
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderDetailPartDetailPartCell.h"

@interface ZOrderDetailPartDetailPartCell ()
@property (nonatomic, strong) UILabel *lblDate;//日期
@property (nonatomic, strong) UILabel *lblCategory;//品类
@property (nonatomic, strong) UILabel *lblName;//名称
@property (nonatomic, strong) UILabel *lblValidity;//有效期
@property (nonatomic, strong) UILabel *lblQuality;//质保期

@end

@implementation ZOrderDetailPartDetailPartCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}

#pragma mark - 界面布局
- (void)setupViewAction{
    [self.contentView addSubview:self.lblDate];
    
    UIView *viewBg = [UIView new];
    viewBg.layer.cornerRadius = 12;
    viewBg.layer.masksToBounds = YES;
    viewBg.layer.borderColor = kColorWithHex(0x70be68).CGColor;
    viewBg.layer.borderWidth = 1;
    [self.contentView addSubview:viewBg];
    
    UILabel *lblNoticeCategory = [UILabel new];
    lblNoticeCategory.text = @"配件品类";
    lblNoticeCategory.font = [UIFont systemFontOfSize:14];
    lblNoticeCategory.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblNoticeCategory];
    [self.contentView addSubview:self.lblCategory];
    
    UIView *viewLineOne = [UIView new];
    viewLineOne.backgroundColor = kColorWithHex(0xdbdbdb);
    [self.contentView addSubview:viewLineOne];
    
    UILabel *lblNoticeName = [UILabel new];
    lblNoticeName.text = @"配件名称";
    lblNoticeName.font = [UIFont systemFontOfSize:14];
    lblNoticeName.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblNoticeName];
    [self.contentView addSubview:self.lblName];
    
    UIView *viewLineTwo = [UIView new];
    viewLineTwo.backgroundColor = kColorWithHex(0xdbdbdb);
    [self.contentView addSubview:viewLineTwo];
    
    UILabel *lblNoticeValidity = [UILabel new];
    lblNoticeValidity.text = @"有效期";
    lblNoticeValidity.font = [UIFont systemFontOfSize:14];
    lblNoticeValidity.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblNoticeValidity];
    [self.contentView addSubview:self.lblValidity];
    
    UIView *viewLineThree = [UIView new];
    viewLineThree.backgroundColor = kColorWithHex(0xdbdbdb);
    [self.contentView addSubview:viewLineThree];
    
    UILabel *lblNoticeQuality = [UILabel new];
    lblNoticeQuality.text = @"质保期";
    lblNoticeQuality.font = [UIFont systemFontOfSize:14];
    lblNoticeQuality.textColor = kColorWithHex(0x666666);
    [self.contentView addSubview:lblNoticeQuality];
    [self.contentView addSubview:self.lblQuality];
    
    UIView *viewLineFour = [UIView new];
    viewLineFour.backgroundColor = kColorWithHex(0xdbdbdb);
    [self.contentView addSubview:viewLineFour];
    
    UIView *viewLineFive = [UIView new];
    viewLineFive.backgroundColor = kColorWithHex(0xf2f2f2);
    [self.contentView addSubview:viewLineFive];
    
    //布局约束
    [self.lblDate mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.leading.equalTo(self.contentView.mas_leading).offset(16);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-16);
        make.height.mas_equalTo(20);
    }];
    
    [viewBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lblDate.mas_bottom).offset(10);
        make.leading.equalTo(self.contentView.mas_leading).offset(10);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-11);
    }];
    
    [lblNoticeCategory mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewBg.mas_top).offset(29);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    
    [self.lblCategory mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblNoticeCategory);
        make.leading.equalTo(lblNoticeCategory.mas_trailing).offset(26);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-16);
    }];
    
    [viewLineOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblNoticeCategory.mas_bottom).offset(5);
        make.leading.equalTo(lblNoticeCategory);
        make.trailing.equalTo(viewBg);
        make.height.mas_equalTo(0.5);
    }];
    
    [lblNoticeName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewLineOne.mas_bottom).offset(5);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    
    [self.lblName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblNoticeName);
        make.leading.equalTo(lblNoticeCategory.mas_trailing).offset(26);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-16);
    }];
    
    [viewLineTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblNoticeName.mas_bottom).offset(5);
        make.leading.equalTo(lblNoticeCategory);
        make.trailing.equalTo(viewBg);
        make.height.mas_equalTo(0.5);
    }];
    
    [lblNoticeValidity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewLineTwo.mas_bottom).offset(5);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    
    [self.lblValidity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblNoticeValidity);
        make.leading.equalTo(lblNoticeCategory.mas_trailing).offset(26);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-16);
    }];
    
    [viewLineThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblNoticeValidity.mas_bottom).offset(5);
        make.leading.equalTo(lblNoticeCategory);
        make.trailing.equalTo(viewBg);
        make.height.mas_equalTo(0.5);
    }];
    
    [lblNoticeQuality mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewLineThree.mas_bottom).offset(5);
        make.leading.equalTo(viewBg.mas_leading).offset(16);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    
    [self.lblQuality mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(lblNoticeQuality);
        make.leading.equalTo(lblNoticeCategory.mas_trailing).offset(26);
        make.trailing.equalTo(viewBg.mas_trailing).offset(-16);
    }];
    
    [viewLineFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lblNoticeQuality.mas_bottom).offset(5);
        make.leading.equalTo(lblNoticeCategory);
        make.trailing.equalTo(viewBg);
        make.height.mas_equalTo(0.5);
        make.bottom.equalTo(viewBg.mas_bottom).offset(-49);
    }];
    
    [viewLineFive mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
}
#pragma mark - 数据更新
- (void)setPartModel:(ZOrderInfoPartInfoModel *)partModel{
    if (partModel) {
        _partModel = partModel;
        self.lblDate.text = [NSString stringWithFormat:@"添加时间 %@",partModel.create_time];
        self.lblCategory.text = partModel.type_name;
        self.lblName.text = partModel.product_name;
        if (partModel.productionDate.length > 0 && partModel.termValidity.length > 0) {
            self.lblValidity.text = [NSString stringWithFormat:@"%@至%@",partModel.productionDate,partModel.termValidity];
        }else{
            self.lblValidity.text = [NSString stringWithFormat:@"%@月",partModel.install];
        }
        self.lblQuality.text = [NSString stringWithFormat:@"%@个月",partModel.warranty];
    }
}
#pragma mark - 懒加载
- (UILabel *)lblDate{
    if (!_lblDate) {
        _lblDate = [UILabel new];
        _lblDate.font = [UIFont boldSystemFontOfSize:14];
        _lblDate.textColor = kColorWithHex(0x333333);
    }
    return _lblDate;
}

- (UILabel *)lblCategory{
    if (!_lblCategory) {
        _lblCategory = [UILabel new];
        _lblCategory.font = [UIFont boldSystemFontOfSize:14];
        _lblCategory.textColor = kColorWithHex(0x333333);
    }
    return _lblCategory;
}

- (UILabel *)lblName{
    if (!_lblName) {
        _lblName = [UILabel new];
        _lblName.font = [UIFont boldSystemFontOfSize:14];
        _lblName.textColor = kColorWithHex(0x333333);
    }
    return _lblName;
}

- (UILabel *)lblValidity{
    if (!_lblValidity) {
        _lblValidity = [UILabel new];
        _lblValidity.font = [UIFont boldSystemFontOfSize:14];
        _lblValidity.textColor = kColorWithHex(0x333333);
    }
    return _lblValidity;
}

- (UILabel *)lblQuality{
    if (!_lblQuality) {
        _lblQuality = [UILabel new];
        _lblQuality.font = [UIFont boldSystemFontOfSize:14];
        _lblQuality.textColor = kColorWithHex(0x333333);
    }
    return _lblQuality;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
