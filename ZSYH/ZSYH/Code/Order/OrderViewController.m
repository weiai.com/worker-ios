//
//  OrderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "OrderViewController.h"
#import "CSMyorderViewController.h"
#import "CSMaintenanceViewController.h"
#import "ZYTestOrderViewController.h"
#import "UISegmentedControl+ChangeColor.h"

@interface OrderViewController ()

@property (nonatomic , retain) UISegmentedControl *segmentedC;
@property (nonatomic , retain) CSMyorderViewController *myorderVC; //我的订单 服务单
@property (nonatomic , retain) CSMaintenanceViewController *maintenanceVC; //我的订单 二次保修
@property (nonatomic , retain) ZYTestOrderViewController *testorderVC; //我的订单 检测单

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}

- (void)setUpView {
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.myorderVC = [CSMyorderViewController new];
    self.myorderVC.from = @"tabbar";
    self.maintenanceVC = [CSMaintenanceViewController new];
    self.testorderVC = [ZYTestOrderViewController new];
    
    [self addChildViewController:self.myorderVC];
    [self addChildViewController:self.maintenanceVC];
    [self addChildViewController:self.testorderVC];
    [self.view addSubview:self.myorderVC.view];
    
    self.segmentedC = [[UISegmentedControl alloc] initWithItems:@[@"服务单", @"二次保修", @"检测单"]];
    self.segmentedC.frame = CGRectMake(0, 0, 218, 30);
    self.segmentedC.selectedSegmentIndex = 0;
    [self.segmentedC addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
    self.segmentedC.tintColor = zhutiColor;
    if (@available(iOS 13, *)) {
        [self.segmentedC ensureiOS12Style];
        
    }else
    {
        UIFont *font = [UIFont boldSystemFontOfSize:14.0f];   // 设置字体大小
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        [self.segmentedC setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
    self.navigationItem.titleView = self.segmentedC;
}

- (void)change:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.view.subviews[0] removeFromSuperview];
            [self.view insertSubview:self.myorderVC.view atIndex:0];
            KMyLog(@"您点击了 服务单 按钮");
            break;
        case 1:
            [self.view.subviews[0] removeFromSuperview];
            [self.view insertSubview:self.maintenanceVC.view atIndex:0];
            [[NSNotificationCenter defaultCenter] postNotificationName:WODEDINGDAN_REQUEST_ERCIBAOXIU_LISTDATA  object:nil userInfo:nil];
            KMyLog(@"您点击了 二次维修 按钮");
            break;
        case 2:
            [self.view.subviews[0] removeFromSuperview];
            [self.view insertSubview:self.testorderVC.view atIndex:0];[[NSNotificationCenter defaultCenter] postNotificationName:WODEDINGDAN_REQUEST_JIANCEDAN_LISTDATA object:nil userInfo:nil];
            KMyLog(@"您点击了 检测单 按钮");
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

