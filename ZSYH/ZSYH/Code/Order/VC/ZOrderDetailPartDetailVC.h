//
//  ZOrderDetailPartDetailVC.h
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 订单详情 -> 配件详情VC

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZOrderDetailPartDetailVC : BaseViewController
@property (nonatomic, strong) CSmaintenanceListModel *orderModel;
@end

NS_ASSUME_NONNULL_END
