//
//  ZOrderAfterSaleChangePartVC.h
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 我的订单 -> 二次保修 -> 更换配件VC

#import <UIKit/UIKit.h>
#import "CSSHmainLIstModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ZOrderAfterSaleChangePartVC : BaseViewController
@property (nonatomic, copy) NSString *orderID;//订单ID
@property (nonatomic, copy) NSString *idStr;
@property (nonatomic, strong) CSSHmainLIstModel *model;
@property (nonatomic, copy) NSString *apporderid;

@end

NS_ASSUME_NONNULL_END
