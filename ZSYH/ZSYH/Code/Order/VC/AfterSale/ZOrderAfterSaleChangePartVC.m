//
//  ZOrderAfterSaleChangePartVC.m
//  ZSYH
//
//  Created by LZY on 2019/10/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修详情-更换配件

#import "ZOrderAfterSaleChangePartVC.h"
#import "ZOrderAfterSalePartInfoModel.h"
#import "ZOrderAfterSaleChangePartNoticeCell.h"
#import "ZOrderAfterSaleChangePartContentCell.h"
#import "ZOrderAfterSaleChangePartPartInfoCell.h"
#import "ZOrderAfterSalePartAddAlertView.h"//弹窗
#import "ZYTJXPJHandLoseViewController.h"//添加非候保配件
#import "ZYTJPJScanCodeViewController.h"//添加候保配件
#import "CSnewJieGuoViewController.h" //售后维修详情
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"
#import "ZYPersonServiceOrderDetailsVC.h"

@interface ZOrderAfterSaleChangePartVC () <UITableViewDelegate,UITableViewDataSource,ZOrderAfterSalePartAddAlertViewDelegate,ZOrderAfterSaleChangePartContentCellDelegate,ZOrderAfterSaleChangePartPartInfoCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZOrderAfterSalePartInfoModel *partInfoModel;
@property (nonatomic, strong) NSMutableArray *partArray;
@property (nonatomic, assign) BOOL isShowNotice;//是否展示提示文案
@property (nonatomic, strong) ZOrderPartInfoModel *changePartModel;//被修改的配件model
@property (nonatomic, strong) NSIndexPath *changePartIndexPath;//被修改的配件下标
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) NSString *repProId;
@property (nonatomic, strong) NSString *idString;

@end

@implementation ZOrderAfterSaleChangePartVC

- (void)viewWillAppear:(BOOL)animated {
    [self requestData];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"更换配件";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.isShowNotice = NO;
    
    [self setupViewAction];
    [self showtabFootView];
    
    //更换配件成功通知监听 (在更换配件成功后发送这个通知即可)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"CHNAGE_PART_SUCCESS" object:nil];
}

#pragma mark - 界面布局
- (void)setupViewAction{
    [self.view addSubview:self.tableView];
    
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, KNavHeight, KScreenWidth, 65)];
    
    UIView *viewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 10, KScreenWidth, 55)];
    viewBg.backgroundColor = kColorWithHex(0xffffff);
    [viewHeader addSubview:viewBg];
    
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 70, 55)];
    lblTitle.font = [UIFont systemFontOfSize:16];
    lblTitle.textColor = kColorWithHex(0x666666);
    lblTitle.text = @"订单编号:";
    [viewBg addSubview:lblTitle];
    
    UILabel *lblOrderID = [[UILabel alloc]initWithFrame:CGRectMake(99, 0, KScreenWidth - 130, 55)];
    lblOrderID.font = FontSize(16);
    lblOrderID.textColor = K666666;
    lblOrderID.text = self.apporderid;
    [viewBg addSubview:lblOrderID];
    
    UIImageView *ivArrow = [[UIImageView alloc] initWithFrame:CGRectMake(KScreenWidth - 28, 17, 12, 21)];
    ivArrow.image = imgname(@"newaddjinru");
    [viewBg addSubview:ivArrow];
    
    UIButton *btnOrder = [UIButton buttonWithType:UIButtonTypeCustom];
    btnOrder.frame = CGRectMake(16, 0, KScreenWidth - 32, 55);
    [btnOrder addTarget:self action:@selector(btnOrderClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewBg addSubview:btnOrder];
    
    self.tableView.tableHeaderView = viewHeader;
}

-(void)showtabFootView {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    UIButton *changeEndBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [changeEndBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [changeEndBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [changeEndBut setTitle:@"配件已更换结束" forState:(UIControlStateNormal)];
    changeEndBut.frame = CGRectMake(28, 48, KWIDTH-28-28, 48);
    changeEndBut.titleLabel.font = FontSize(16);
    changeEndBut.layer.masksToBounds = YES;
    changeEndBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview:changeEndBut];
    
    [changeEndBut addTarget:self action:@selector(changeEndButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _tableView.tableFooterView = self.tabFootView;
}

#pragma mark - 配件已更换结束
-(void)changeEndButAction:(UIButton *)but {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param [@"orderId"] = NOTNIL(self.idStr) ;
    
    [NetWorkTool POST:endAfterOrder param:param success:^(id dic) {
        KMyLog(@"配件已更换结束 %@", dic);

        [self.navigationController popViewControllerAnimated:YES];
        [singlTool shareSingTool].needReasfh = YES;

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    KMyLog(@"您点击了 配件已更换结束 按钮");
}

#pragma mark - 数据请求
- (void)requestData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectIfNotNil:self.orderID forKey:@"id"];
    kWeakSelf;
    [NetWorkTool POST:getRepPros param:param success:^(id dic) {
        [weakSelf handleDataWith:dic];
        NSDictionary *dataDic = dic[@"data"];
        self.idString = dataDic[@"id"];
        KMyLog(@"售后维修 更换配件页面 所有数据 %@", dic);

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

//数据处理
- (void)handleDataWith:(id)dic{
    kWeakSelf;
    self.partInfoModel = [ZOrderAfterSalePartInfoModel mj_objectWithKeyValues:[dic objectForKeyNotNil:@"data"]];
    self.partArray = self.partInfoModel.pros.mutableCopy;
    [self.partArray enumerateObjectsUsingBlock:^(ZOrderPartInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![obj.state isEqualToString:@"1"]) {
            //1正常使用2损坏3更换了配件
            weakSelf.isShowNotice = YES;
            *stop = YES;//停止遍历
        }
    }];
    [self.tableView reloadData];
}

#pragma mark - 交互事件
//点击订单编号
- (void)btnOrderClickAction:(UIButton *)sender{
    if ([_model.user_type isEqualToString:@"0"]||[_model.usertype isEqualToString:@"0"]) { //个人订单
        //跳转到个人订单 服务单详情
        ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
        vc.orderID = self.partInfoModel.Id;
        [self.navigationController pushViewController:vc animated:YES];
    } else if ([_model.user_type isEqualToString:@"2"]||[_model.usertype isEqualToString:@"2"]) { //电器厂家订单
        //跳转到电器厂家 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
        vc.orderID = self.partInfoModel.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        if (self.isShowNotice) {
            return 1;
        }else{
            return 0;
        }
    } else if (section == 1){
        if (self.partInfoModel.prodcutComment.length > 0) {
            return 1;
        }else{
            return 0;
        }
    } else{
        NSMutableArray *array = [NSMutableArray array];
        for (ZOrderPartInfoModel *model in self.partArray) {
            if (![model.isaddnew isEqualToString:@"1"]) {
                [array addObject:model];
            }
        }
        return array.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        ZOrderAfterSaleChangePartNoticeCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartNoticeCell class]) forIndexPath:indexPath];
        return cell;
    } else if (indexPath.section == 1){
        ZOrderAfterSaleChangePartContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartContentCell class]) forIndexPath:indexPath];
        cell.partInfoModel = self.partInfoModel;
        cell.delegate = self;
        return cell;
    } else {
        ZOrderPartInfoModel *model = [self.partArray objectAtIndexSafe:indexPath.row];
        ZOrderAfterSaleChangePartPartInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartPartInfoCell class]) forIndexPath:indexPath];
        cell.partModel = model;
        cell.indexPath = indexPath;
        cell.delegate = self;
        
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - ZOrderAfterSaleChangePartContentCellDelegate
- (void)orderAfterSaleChangePartContentCellBtnClick{
    ZOrderAfterSalePartAddAlertView *viewAlert = [ZOrderAfterSalePartAddAlertView new];
    viewAlert.viewType = AlertViewTypeAddPart;
    viewAlert.delegate = self;
    [viewAlert showView];
}

#pragma mark - ZOrderAfterSaleChangePartPartInfoCellDelegate
- (void)orderAfterSaleChangePartPartInfoCellBtnClick:(ZOrderPartInfoModel *)model index:(NSIndexPath *)index{
    if ([model.state isEqualToString:@"1"]) {
        //申请补助
        ZOrderAfterSalePartAddAlertView *viewAlert = [ZOrderAfterSalePartAddAlertView new];
        viewAlert.viewType = AlertViewTypeMakeSure;
        viewAlert.delegate = self;
        [viewAlert showView];
    } else if ([model.state isEqualToString:@"2"]){
        //添加新配件
        ZOrderAfterSalePartAddAlertView *viewAlert = [ZOrderAfterSalePartAddAlertView new];
        viewAlert.viewType = AlertViewTypeAddPart;
        viewAlert.delegate = self;
        [viewAlert showView];
        viewAlert.repProId = model.repProId;
        self.repProId = model.repProId;
        KMyLog(@"您点击了 cell上面的<添加新配件>按钮 %ld ------- %@", (long)index.row, model.repProId);
    }
    self.changePartModel = model;
    self.changePartIndexPath = index;
}

#pragma mark - ZOrderAfterSalePartAddAlertViewDelegate
- (void)partAddAlertViewBtnClickWith:(AlertViewType)viewType withAction:(NSInteger)action{
    if (viewType == AlertViewTypeMakeSure) {
        //确认候保配件是否损坏 200取消 201确认
        if (action == 200) {
            //弹窗消失
        } else if (action == 201){
            //申请质保
            [self requestApplyQuality];
        }
    } else if (viewType == AlertViewTypeAddPart){
        //新增配件是否为候保配件 200不是 201是
        if (action == 200) {
            ZYTJXPJHandLoseViewController *vc = [[ZYTJXPJHandLoseViewController alloc] init];
            vc.apporderid = self.apporderid;
            vc.orderStr = self.orderID;
            vc.idStr = self.idString;
            vc.repProId = self.repProId;
            vc.CSSHmainLIstModel = self.model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"添加新配件->手输");
        }else if (action == 201){
            //扫码添加新配件
            ZYTJPJScanCodeViewController *vc = [[ZYTJPJScanCodeViewController alloc] init];
            vc.apporderid = self.apporderid;
            vc.orderId = self.orderID;
            vc.idStr = self.idString;
            vc.repProId = self.repProId;
            vc.model = self.model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"添加新配件->扫码");
        }
    }
}

//申请质保
- (void)requestApplyQuality{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObjectIfNotNil:self.changePartModel.Id forKey:@"id"];//配件ID
    kWeakSelf;
    [NetWorkTool POST:afterAppGuar param:param success:^(id dic) {
        weakSelf.changePartModel.state = @"2";//已损坏
        //刷新指定cell
        //[weakSelf.tableView reloadRowsAtIndexPaths:@[weakSelf.changePartIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        weakSelf.isShowNotice = YES;
        [weakSelf.tableView reloadData];
    } other:^(id dic) {
    } fail:^(NSError *error) {
    } needUser:YES];
}

#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KNavHeight, KScreenWidth, KScreenHeight - KNavHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = UITableViewAutomaticDimension;//默认值
        _tableView.estimatedRowHeight = 200;//预估高
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        
        [_tableView registerClass:[ZOrderAfterSaleChangePartNoticeCell class] forCellReuseIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartNoticeCell class])];
        [_tableView registerClass:[ZOrderAfterSaleChangePartContentCell class] forCellReuseIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartContentCell class])];
        [_tableView registerClass:[ZOrderAfterSaleChangePartPartInfoCell class] forCellReuseIdentifier:NSStringFromClass([ZOrderAfterSaleChangePartPartInfoCell class])];
    }
    return _tableView;
}

- (NSMutableArray *)partArray{
    if (!_partArray) {
        _partArray = [NSMutableArray array];
    }
    return _partArray;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
