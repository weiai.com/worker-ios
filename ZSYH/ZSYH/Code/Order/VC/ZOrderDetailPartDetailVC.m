//
//  ZOrderDetailPartDetailVC.m
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZOrderDetailPartDetailVC.h"
#import "ZOrderDetailPartDetailContentCell.h"
#import "ZOrderDetailPartDetailPartCell.h"

@interface ZOrderDetailPartDetailVC () <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *partArray;

@end

@implementation ZOrderDetailPartDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配件详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = kColorWithHex(0xffffff);
    
    [self setupViewAction];
    // Do any additional setup after loading the view.
}

#pragma mark - 界面布局
- (void)setupViewAction{
    [self.view addSubview:self.tableView];
}

#pragma mark - 数据更新
- (void)setOrderModel:(CSmaintenanceListModel *)orderModel{
    if (orderModel) {
        _orderModel = orderModel;
        [self.partArray addObjectsFromArray:orderModel.pros];
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        if (self.orderModel.prodcutComment.length > 0) {
            return 1;
        }else{
            return 0;
        }
    }else{
        return self.partArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        ZOrderDetailPartDetailContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderDetailPartDetailContentCell class]) forIndexPath:indexPath];
        cell.orderModel = self.orderModel;
        return cell;
    } else {
        ZOrderInfoPartInfoModel *partModel = [self.partArray objectAtIndexSafe:indexPath.row];
        
        if (partModel.product_type_id.length == 0) {
            ZOrderDetailPartDetailContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderDetailPartDetailContentCell class]) forIndexPath:indexPath];
            cell.infoPartInfoModel = partModel;
            return cell;
        } else {
            
            ZOrderDetailPartDetailPartCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZOrderDetailPartDetailPartCell class]) forIndexPath:indexPath];
            cell.partModel = partModel;
            return cell;
        }
    }
    return [UITableViewCell new];
}

#pragma mark - 懒加载
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KNavHeight, KScreenWidth, KScreenHeight - KNavHeight) style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.rowHeight = UITableViewAutomaticDimension;//默认值
        _tableView.estimatedRowHeight = 250;//预估高
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerClass:[ZOrderDetailPartDetailContentCell class] forCellReuseIdentifier:NSStringFromClass([ZOrderDetailPartDetailContentCell class])];
        [_tableView registerClass:[ZOrderDetailPartDetailPartCell class] forCellReuseIdentifier:NSStringFromClass([ZOrderDetailPartDetailPartCell class])];
    }
    return _tableView;
}

- (NSMutableArray *)partArray{
    if (!_partArray) {
        _partArray = [NSMutableArray array];
    }
    return _partArray;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
