//
//  ZBaseWebVC.h
//  ZSYH
//
//  Created by LZY on 2019/10/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

// 网页基类VC

#import "BaseViewController.h"
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface ZBaseWebVC : BaseViewController
@property (nonatomic, strong) WKWebView *webView;//网页
@property (nonatomic, copy) NSString *webUrl;//网页地址
@end

NS_ASSUME_NONNULL_END
