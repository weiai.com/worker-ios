//
//  ZSAlertView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/5.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZSAlertView.h"

@interface ZSAlertView ()

@end

@implementation ZSAlertView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)setUI {
    
    self.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    _upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    [whiteBGView addSubview:_upImage];
    _upImage.layer.masksToBounds = YES;
    _upImage.layer.cornerRadius = 20;
    _upImage.image = [UIImage imageNamed:@"组732"];
    
    _upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:_upLable];
    _upLable.font = FontSize(16);
    _upLable.textColor = [UIColor colorWithHexString:@"#333333"];
    _upLable.textAlignment =  NSTextAlignmentCenter;
    
    _dowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:_dowLable];
    _dowLable.font = FontSize(14);
    _dowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    _dowLable.textAlignment =  NSTextAlignmentCenter;
    
    _btn1 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_btn1 setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    _btn1.layer.masksToBounds = YES;
    _btn1.layer.cornerRadius = 4;
    _btn1.layer.borderColor = [zhutiColor CGColor];
    _btn1.layer.borderWidth = 1.0f;
    [_btn1 addTarget:self action:@selector(btn1Click) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:_btn1];
    [_btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_dowLable.mas_bottom).with.offset(30);
        make.left.mas_equalTo((ScreenW-120*2-44)/3);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(45);
    }];
    
    _btn2 = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_btn2 setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    _btn2.layer.masksToBounds = YES;
    _btn2.layer.cornerRadius = 4;
    _btn2.layer.borderColor = [zhutiColor CGColor];
    _btn2.layer.borderWidth = 1.0f;
    [_btn2 addTarget:self action:@selector(btn2Click) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:_btn2];
    [_btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_dowLable.mas_bottom).with.offset(30);
        make.right.mas_equalTo(-(ScreenW-120*2-44)/3);
        make.width.mas_equalTo(120);
        make.height.mas_equalTo(45);
    }];
    
    [self addSubview:self.bgView];
    
}



- (void)show {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
}


- (void)btn1Click {
    if (self.btn1Block) {
        self.btn1Block();
    }
    [self removeFromSuperview];
}

- (void)btn2Click {
    if (self.btn2Block) {
        self.btn2Block();
    }
    [self removeFromSuperview];
}


@end
