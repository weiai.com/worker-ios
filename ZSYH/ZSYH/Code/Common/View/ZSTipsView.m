//
//  ZSTipsView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/5.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZSTipsView.h"

@interface ZSTipsView ()

@end

@implementation ZSTipsView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)setUI {
    
    self.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    _upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    [whiteBGView addSubview:_upImage];
    _upImage.layer.masksToBounds = YES;
    _upImage.layer.cornerRadius = 20;
    _upImage.image = [UIImage imageNamed:@"组732"];
    
    _upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:_upLable];
    _upLable.font = FontSize(16);
    _upLable.textColor = [UIColor colorWithHexString:@"#333333"];
    _upLable.textAlignment =  NSTextAlignmentCenter;
    
    _dowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:_dowLable];
    _dowLable.font = FontSize(14);
    _dowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    _dowLable.textAlignment =  NSTextAlignmentCenter;
    
    
    _btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [_btn setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [_btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [_btn setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    _btn.layer.masksToBounds = YES;
    _btn.layer.cornerRadius = 4;
    [_btn addTarget:self action:@selector(btnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:_btn];
    [_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
    [self addSubview:self.bgView];
    
}



- (void)show {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
}


- (void)btnClick {
    if (self.btnBlock) {
        self.btnBlock();
    }
    [self removeFromSuperview];
}




@end

