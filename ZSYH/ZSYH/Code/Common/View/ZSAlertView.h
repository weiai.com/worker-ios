//
//  ZSAlertView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/5.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZSAlertView : UIView

@property(nonatomic,strong)UIView *bgView;
@property(nonatomic,strong)UIImageView *upImage;
@property(nonatomic,strong)UILabel *upLable;
@property(nonatomic,strong)UILabel *dowLable;
@property(nonatomic,strong)UIButton *btn1;
@property(nonatomic,strong)UIButton *btn2;

- (void)show;
@property (nonatomic, copy) void(^btn1Block)();
@property (nonatomic, copy) void(^btn2Block)();

@end

NS_ASSUME_NONNULL_END
