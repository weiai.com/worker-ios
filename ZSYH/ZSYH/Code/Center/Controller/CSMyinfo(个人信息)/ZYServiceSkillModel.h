//
//  ZYServiceSkillModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/11/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYServiceSkillModel : NSObject
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *fault_id;
@property (nonatomic, copy) NSString *service_name;
@property (nonatomic, copy) NSString *certificates_type;

@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *fault_name;

@end

NS_ASSUME_NONNULL_END
