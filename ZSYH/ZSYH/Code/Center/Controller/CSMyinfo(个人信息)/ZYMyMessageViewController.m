//
//  ZYMyMessageViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyMessageViewController.h"
#import "ZYMyMessageTableViewCell.h"
#import "ZYMessageModel.h"

@interface ZYMyMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;

@end

@implementation ZYMyMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的留言";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];

    [self requestData];
    [self showdetaile];
  
}

-(void)showdetaile {
//    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight+ 10, KWIDTH, 55)];
//    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
//    bgview.backgroundColor = [UIColor whiteColor];

//    UILabel *dingdan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 55)];
//    dingdan.font = FontSize(16);
//    dingdan.textColor = K666666;
//    dingdan.text = @"订单编号:";
//    [bgview addSubview:dingdan];

//    UILabel *dingdanlb = [[UILabel alloc]initWithFrame:CGRectMake(99, 0, KWIDTH/2, 55)];
//    dingdanlb.font = FontSize(16);
//    dingdanlb.textColor = K666666;
//    dingdanlb.text = _idStr;
//    [bgview addSubview:dingdanlb];

//    UIButton *rightBtn = [[UIButton alloc] init];
//    rightBtn.frame = CGRectMake(dingdanlb.right, 0, 100, 55);
//    [rightBtn setImage:imgname(@"newaddjinru") forState:(UIControlStateNormal)];
//    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -40)];
//    [bgview addSubview:rightBtn];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, KNavHeight, KWIDTH, KHEIGHT - kNaviHeight);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerClass:[ZYMyMessageTableViewCell class] forCellReuseIdentifier:@"ZYMyMessageTableViewCell"];
    _mytableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_mytableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self->_mytableView.mj_header beginRefreshing];
    }];
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    [_scrollView addSubview:_mytableView];
}

//我的留言 数据请求
- (void)requestData {
    
    [NetWorkTool POST:myPartsMessage param:nil success:^(id dic) {
        KMyLog(@"我的留言 %@", dic);
        
        self.mydateSource = [ZYMessageModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.mytableView reloadData];
    } other:^(id dic) {

    } fail:^(NSError *error) {

    } needUser:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 269;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    //通过唯一标识创建Cell实例
    ZYMyMessageTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYMyMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    self.mytableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    //数据赋值
    ZYMessageModel *model = self.mydateSource[indexPath.row];

    [mycell.headerImg sd_setImageWithURL:[NSURL URLWithString:_mymodel.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
    mycell.nameLab.text = _mymodel.nickname;
    mycell.dateLab.text = model.createTime;
    mycell.textView.text = model.message;
    [mycell.productImg sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil];
    mycell.proInfoLab.text = model.parts_details;
    
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //KMyLog(@"您点击了第 %ld 个分区第 %ld 行", indexPath.section, indexPath.row);
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
