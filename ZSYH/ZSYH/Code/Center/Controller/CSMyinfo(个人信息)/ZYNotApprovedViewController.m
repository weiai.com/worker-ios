//
//  ZYNotApprovedViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/2.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYNotApprovedViewController.h"
#import "ZYBusinessLicenseViewController.h"

@interface ZYNotApprovedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *reasonLab;

@end

@implementation ZYNotApprovedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"审核未通过";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.reasonLab.text = _model.refusalReasons;

   
}

//重新提交按钮点击事件处理
- (IBAction)resubmitBtnAction:(id)sender {
    ZYBusinessLicenseViewController *bvc = [[ZYBusinessLicenseViewController alloc] init];
    [self.navigationController pushViewController:bvc animated:YES];
}


@end
