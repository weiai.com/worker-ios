//
//  CSXGphoneViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSXGphoneViewController.h"

@interface CSXGphoneViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet YZMButton *yzmBut;
@property (weak, nonatomic) IBOutlet UILabel *bglaii;

@property (weak, nonatomic) IBOutlet UILabel *bgli22;
@property (weak, nonatomic) IBOutlet UITextField *nephontTF;
@property (weak, nonatomic) IBOutlet UILabel *bgdfdf;
@property (weak, nonatomic) IBOutlet UILabel *bgdfdfds;
@property (weak, nonatomic) IBOutlet UITextField *neYZmTF;



@property (weak, nonatomic) IBOutlet UITextField *oldYzmTF;
@property (weak, nonatomic) IBOutlet UILabel *oldPhoneLB;
@property(nonatomic,strong)NSString *yzmAcode;

@property (weak, nonatomic) IBOutlet UIView *neBGvview;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contttt;
@property (weak, nonatomic) IBOutlet UITextField *olddddddtf;

@end

@implementation CSXGphoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改绑定手机号";
    [self.leftbutton setTitle:@"取消" forState:(UIControlStateNormal)];

    self.yzmBut.layer.masksToBounds = YES;
    self.yzmBut.layer.cornerRadius = 4;
    self.yzmBut.layer.borderWidth = 1;
    self.yzmBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    self.olddddddtf.delegate = self;
    self.neYZmTF.delegate = self;

    if (_model.phone.length < 10) {
        
        self.title = @"绑定手机号";

    }else{
        self.oldPhoneLB.text = [NSString stringWithFormat:@"当前绑定手机号 %@****%@",[_model.phone substringToIndex:4],[_model.phone substringFromIndex:8]];

    }
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
 
    self.neBGvview.hidden = YES;
    


    // Do any additional setup after loading the view from its nib.
}
- (IBAction)oldyzm:(YZMButton *)sender {
    [sender timeFailBeginFrom:60];

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =NOTNIL(_oldPhoneLB.text);
    
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        self.olddddddtf.keyboardType = UIKeyboardTypeNumberPad;
        [self.olddddddtf becomeFirstResponder];
        
        self.yzmAcode = [dic objectForKey:@"data"];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
    
}

- (IBAction)senmasff:(UIButton *)sender {
    [_yzmBut timeFailBeginFrom:60];

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =NOTNIL(_nephontTF.text);
    
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        self.neYZmTF.keyboardType = UIKeyboardTypeNumberPad;
        [self.neYZmTF becomeFirstResponder];
        self.yzmAcode = [dic objectForKey:@"data"];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
   
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    KMyLog(@"%@",textField.text);
    NSString *str = [NSString stringWithFormat:@"%@%@",textField.text,string];
    if (str.length == 6) {
        
        if ([str isEqualToString:_yzmAcode]) {
            if (textField == _neYZmTF) {
                ShowToastWithText(@"验证码正确");
                [self changePhoneactin];
            }else{
                [textField resignFirstResponder];
                self.neBGvview.hidden = NO;
                
                self.contttt.constant = kNaviHeight+0;
                [_nephontTF becomeFirstResponder];
                _nephontTF.keyboardType = UIKeyboardTypeNumberPad;
            }
        }else{
            ShowToastWithText(@"验证码错误");
        }
    }
    return YES;
}

-(void)changePhoneactin{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] = NOTNIL(_nephontTF.text);
    param[@"yzCode"] = NOTNIL(_neYZmTF.text);

    [NetWorkTool POST:changePhone param:param success:^(id dic) {
        ShowToastWithText(@"修改成功");
        
        
        [NetWorkTool POST:Login param:param success:^(id dic) {
            NSString *toStr = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"data"] objectForKey:@"token"]];
            ShowToastWithText(@"修改成功");

            [USER_DEFAULT setObject:toStr forKey:@"Token"];
            
            
            [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
                NSString *userID = [[dic objectForKey:@"data"] objectForKey:@"id"];
                
                [USER_DEFAULT setObject:userID forKey:@"user_id"];
                [self dismissViewControllerAnimated:YES completion:nil];

            } other:^(id dic) {
                
            } fail:^(NSError *error) {
                
            } needUser:YES];
            
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:NO];
        
        
        
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}


@end
