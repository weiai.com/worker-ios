//
//  CSsecviceViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSsecviceViewController.h"
#import "CSServiceFWModel.h"
#import "CSserviceFWCollectionViewCell.h"
#import "CSLoginViewController.h"
#import "TabBarViewController.h"
#import "SkillsCerViewController.h"

@interface CSsecviceViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)XSQBanner *banner;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray *modelArr;
@property(nonatomic,strong)NSMutableArray *headerArr;
@property(nonatomic,strong)NSMutableArray <CSServiceFWModel *>*selectedModelArr;
@property(nonatomic,strong)UIView *collectHeaderView;
@property (nonatomic, strong) UIView *relatedCertificateAlertView;//相关证书弹框
@end

@implementation CSsecviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"服务范围";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self.rightbutton setTitle:@"提交" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    self.rightbutton.hidden = YES;//不要右上角提交按钮,提交按钮展示在界面最下面
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    self.selectedModelArr = [NSMutableArray arrayWithCapacity:1];
    self.headerArr = [NSMutableArray arrayWithCapacity:1];
    if (!_seleArr) {
        _seleArr = [NSMutableArray arrayWithCapacity:1];
    }
    [self requestGoods];
    [self shwoUploadRelatedCertificateAlertView];
    // Do any additional setup after loading the view.
}

#pragma mark - 提交
- (void)submitClick{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (_seleArr.count == 0) {
        ShowToastWithText(@"请至少选择一个服务类型");
        return;
    }

    NSString *string = [_seleArr componentsJoinedByString:@","];
    
    if ([self.isrgister isEqualToString:@"zhuce"]) {
        NSString *string = [_seleArr componentsJoinedByString:@","];
        if (self.commitBlock) {
            self.commitBlock(string, _seleArr);
        }
        [self.navigationController popViewControllerAnimated:YES];
        return;

    }
    

    param[@"faultType"] = string;
    [NetWorkTool POST:setUserInfo param:param success:^(id dic) {

            ShowToastWithText(@"修改成功");
            [self.navigationController popViewControllerAnimated:YES];

    } other:^(id dic) {
        
        //需要上传证件
       if ([dic[@"errorCode"] intValue] == 005) {
           
           ZSYHAlertView *view = [ZSYHAlertView new];
           view.upImage.image = [UIImage imageNamed:@"组 1461"];
           view.upLable.text = @"您选择的服务范围";
           view.dowLable.text = @"需要上传您本人的技能证件！";
           [view.btn1 setTitle:@"取消" forState:(UIControlStateNormal)];
           [view.btn2 setTitle:@"去上传" forState:(UIControlStateNormal)];
           view.btn1Block = ^{
                [self.navigationController popViewControllerAnimated:YES];
           };
           view.btn2Block = ^{
               SkillsCerViewController *VC = [SkillsCerViewController new];
               VC.from = @"secvice";
               [self.navigationController pushViewController:VC animated:YES];
           };
           [view show];
        }
    } fail:^(NSError *error) {
        
    } needUser:NO];
}


- (void)requestGoods {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [NetWorkTool POST:getsServiceFW param:param success:^(id dic) {
        KMyLog(@"getfanwei%@",dic);
        NSArray *myarr = [dic objectForKey:@"data"];
        for (NSDictionary *mydic in myarr) {
            [self.headerArr addObject:[mydic objectForKey:@"service_name"]];
            NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
            for (NSDictionary *conDic in [mydic objectForKey:@"second"]) {
                CSServiceFWModel *model = [[CSServiceFWModel alloc]init];
                [model setValuesForKeysWithDictionary:conDic];
                [muarr addObject:model];
            }
            [self.modelArr addObject:muarr];;
        }
        [self.collectionView reloadData];
    } other:^(id dic) {
        
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
   
}
-(void)showcollectHeader{
    
    self.collectHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 40)];
   
}

#pragma mark - 弹框
-(void)shwoUploadRelatedCertificateAlertView {
    self.relatedCertificateAlertView = [[UIView alloc]init];
    self.relatedCertificateAlertView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.relatedCertificateAlertView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.relatedCertificateAlertView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"请上传相关证件";
    
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(28, 110, KWIDTH - 100, 34)];
    [whiteBGView addSubview:topLable];
    topLable.font = FontSize(14);
    topLable.numberOfLines = 0;
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.textAlignment =  NSTextAlignmentCenter;
    topLable.text = @"服务范围需要上传相关证件";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"";
    
    UIButton *leftButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [leftButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:0.5]];
    leftButton.layer.masksToBounds = YES;
    leftButton.layer.cornerRadius = 4;
    [leftButton addTarget:self action:@selector(cancleAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.offset(28);
        make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.bottom.offset(-19);
    }];
    
    UIButton *rightButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [rightButton setTitle:@"去上传" forState:(UIControlStateNormal)];
    [rightButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [rightButton setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    rightButton.layer.masksToBounds = YES;
    rightButton.layer.cornerRadius = 4;
    [rightButton addTarget:self action:@selector(uploadAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:rightButton];
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)cancleAction:(UIButton *)sender{
    [_relatedCertificateAlertView removeFromSuperview];
    NSLog(@"您点击了 左侧按钮");
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)uploadAction:(UIButton *)sender{
    [_relatedCertificateAlertView removeFromSuperview];
    NSLog(@"您点击了 右侧按钮");
    NSString *string = [_seleArr componentsJoinedByString:@","];
    if (self.commitBlock) {
        self.commitBlock(string, _seleArr);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - CollectionView -

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"head" forIndexPath:indexPath];
        header.backgroundColor = [UIColor whiteColor];
        [header removeAllSubviews];
//        [header addSubview: self.collectHeaderView];
        UILabel *bgLbv = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 5)];
        bgLbv.backgroundColor = [UIColor colorWithHexString:@"#FFF2F2F2"];
        [header addSubview:bgLbv];

        UILabel *leftLB = [[UILabel alloc]initWithFrame:CGRectMake(16, 21, KWIDTH/2, 16)];
        leftLB.font = FontSize(16);
        leftLB.textColor = [UIColor colorWithHexString:@"#5D5D5D"];
        leftLB.text = self.headerArr[indexPath.section];
        [header addSubview:leftLB];
        
        UIButton *quanxuanbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        
        BOOL ddd = YES;
        [quanxuanbut setTitle:@"全不选" forState:(UIControlStateNormal)];

        for (CSServiceFWModel *model in _modelArr[indexPath.section]) {
            if ([_seleArr containsObject:model.Id]) {

            }else{
                [quanxuanbut setTitle:@"全选" forState:(UIControlStateNormal)];
                break;

            }
        }
        
 
        [quanxuanbut setTitleColor:zhutiColor forState:(UIControlStateNormal)];
        quanxuanbut.frame = CGRectMake(KWIDTH-16-40, 20, 50, 16);
        quanxuanbut.titleLabel.font = FontSize(14);
        [header addSubview:quanxuanbut];
        quanxuanbut.tag = indexPath.section+556;
        [quanxuanbut addTarget:self action:@selector(quanxuanaction:) forControlEvents:(UIControlEventTouchUpInside)];
        return header;
    } else {
        UICollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
        UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        loginBtn.adjustsImageWhenDisabled = NO;
        loginBtn.adjustsImageWhenHighlighted = NO;
        loginBtn.layer.masksToBounds = YES;
        loginBtn.layer.cornerRadius = kScaleNum(4);
        loginBtn.backgroundColor = zhutiColor;
        loginBtn.userInteractionEnabled = YES;
        [loginBtn setTitle:@"提交" forState:UIControlStateNormal];
        [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        loginBtn.titleLabel.font = FontSize(16);
        [loginBtn addTarget:self action:@selector(submitClick) forControlEvents:UIControlEventTouchUpInside];
        loginBtn.frame = CGRectMake(kScaleNum(28), kScaleNum(25), kScreen_Width - kScaleNum(28 * 2), kScaleNum(48));
        [footerView addSubview:loginBtn];
        footerView.backgroundColor = [UIColor colorWithHex:0xf2f2f2];
        if (indexPath.section == _headerArr.count - 1) {//最后一个分区
            return footerView;
        } else {
            return [UIView new];
        }
    }
   
    return nil;
    
}
-(void)quanxuanaction:(UIButton *)but{
    NSInteger ind = but.tag -556;
    
    for (CSServiceFWModel *model in _modelArr[ind]) {
        if ([but.titleLabel.text isEqualToString:@"全选"]) {
            [_seleArr addObject:model.Id];

        }else{
            [_seleArr removeObject:model.Id];

        }
    }
    [_collectionView reloadData];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
  
    return CGSizeMake(KWIDTH, 40);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if (section == _headerArr.count - 1) {
        return CGSizeMake(KWIDTH, 100);
    }
    return CGSizeZero;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //[_modelArr[section] count]
    NSArray *array = _modelArr[section];
    return array.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _headerArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CSserviceFWCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CSserviceFWCollectionViewCell" forIndexPath:indexPath];
//
    CSServiceFWModel *model = _modelArr[indexPath.section][indexPath.row];
    cell.mylable.text = model.fault_name;
    if ([_seleArr containsObject:model.Id]) {
        cell.mimage.image = imgname(@"serviceseleIm");
    }else{
        cell.mimage.image = imgname(@"usseleima");

    }
    
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    CSServiceFWModel *model = _modelArr[indexPath.section][indexPath.row];

    if ([_seleArr containsObject:model.Id]) {
        [_seleArr removeObject:model.Id];
        [_selectedModelArr removeObject:model];
    }else{
        
        [_seleArr addObject:model.Id];
        [_selectedModelArr addObject:model];
        NSLog(@"选中%@", model);
    }
    [_collectionView  reloadData];
}



-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake((KWIDTH-62)/4 , 33);
        //        layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.footerReferenceSize = CGSizeMake(KWIDTH, kScaleNum(100));
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        layout.minimumLineSpacing = 10;
        layout.minimumInteritemSpacing = 10;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        
        [_collectionView registerNib:[UINib nibWithNibName:@"CSserviceFWCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CSserviceFWCollectionViewCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        [self.view addSubview:_collectionView];
        _collectionView.backgroundColor = [UIColor whiteColor];
        
    }
    return _collectionView;
}

@end

