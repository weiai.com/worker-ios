//
//  PerfectingInfoViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PerfectingInfoViewController : BaseViewController

@property (nonatomic, strong) NSMutableDictionary *stepOneDict;
@property (nonatomic, copy) NSString *phone;

@end

NS_ASSUME_NONNULL_END
