//
//  PerfectingInfoViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "PerfectingInfoViewController.h"
#import "PerfectingInfoCell.h"
#import "CSquyuViewController.h"
#import "CSsecviceViewController.h"
#import "CSIdAuthViewController.h"
#import "CSLoginViewController.h"
#import "TabBarViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "NSString+AttributedString.h"
#import "ZYServiceSkillModel.h"

@interface PerfectingInfoViewController ()<UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *imageNameArr;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) UIImageView *skillIV;
@property (nonatomic, strong) UIButton *skillB;
@property (nonatomic, strong) NSMutableArray *areaArr;
@property (nonatomic, strong) NSMutableArray *secviceArr;
@property (nonatomic, strong) NSMutableArray *imageArr;
@property (nonatomic, strong) NSMutableArray *cerImageArr;
@property (nonatomic, strong) NSMutableDictionary *paramDict;
//@property (nonatomic,strong) NSMutableArray *imageArr;//
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) UIView *addView;//
@property (nonatomic, assign) CGFloat bottomf;//
@property (nonatomic, strong) UIView *footerV;
@property (nonatomic, strong) UIView *butBg;
@property (nonatomic, strong) NSMutableArray *killArray;
@property (nonatomic, strong) NSMutableArray *selectArr;
@property (nonatomic, strong) NSMutableArray *fwfwArray;
@end

@implementation PerfectingInfoViewController

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"Rectangle 2"] forBarMetrics:UIBarMetricsDefault];
    self.imageNameArr = @[@"组 2871", @"组 2872", @"组 2870"];
    self.titleArr = @[@"接单区域", @"服务范围", @"身份认证"];
    self.paramDict = [NSMutableDictionary dictionary];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"xiangGuanZhengjian");
    [self.imageArr addObject:_myimage];
    
    self.killArray = [NSMutableArray arrayWithCapacity:1];
    self.selectArr = [NSMutableArray arrayWithCapacity:1];
    self.fwfwArray = [NSMutableArray arrayWithCapacity:1];

    [self setUI];
    [self getCertificateListByFaultIdData]; //查询有没有选择空调
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"Rectangle 2"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forBarMetrics:UIBarMetricsDefault];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - 构建视图
- (void)setUI {
    
    [self.view addSubview:self.scrollView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"完善信息";
    titleLabel.font = FontSize(17);
    titleLabel.frame = CGRectMake(0, 0, kScaleNum(200), kScaleNum(44));
    self.navigationItem.titleView = titleLabel;
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon_white") forState:(UIControlStateNormal)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreen_Width, kScaleNum(160)) style:UITableViewStyleGrouped];
    self.edgesForExtendedLayout = UIRectEdgeBottom;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = DefaultColor;
    self.tableView.bounces = NO;
    [self.scrollView addSubview:self.tableView];
    
    UIImageView *imageV = [UIImageView new];
    imageV.frame = CGRectMake(16, self.tableView.bottom +18, 18, 18);
    imageV.image = [UIImage imageNamed:@"组 2873"];
    [self.scrollView addSubview:imageV];
    
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(imageV.right +13, self.tableView.bottom +18, 60, 20);
    label.text = @"相关证件";
    label.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:label];
    
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(16, imageV.bottom +18, KWIDTH-32, 100)];
    [self.scrollView addSubview: self.addView];
    CGFloat www = (KWIDTH -62)/4;
    self.bottomf = label.bottom;
    [self showinageWirhArr:_imageArr];
    
    CGRect ffff = self.addView.frame;
    ffff.size.height = www +10;
    self.addView.frame = ffff;
    
    CGFloat heigehtbg = KHEIGHT -self.addView.bottom - 16;
    
    if (heigehtbg < 147) {
        heigehtbg = 147;
    }
    
    UIView *butBg = [[UIView alloc]initWithFrame:CGRectMake(0, self.addView.bottom, KWIDTH, heigehtbg)];
    self.butBg = butBg;
    [self.scrollView addSubview:butBg];
    butBg.userInteractionEnabled = YES;
    butBg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIButton *doneB = [UIButton new];
    doneB.frame = CGRectMake(28, 42, KWIDTH-28-28, 48);
    [doneB setTitle:@"完成注册" forState:(UIControlStateNormal)];
    [doneB addTarget:self action:@selector(done:) forControlEvents:(UIControlEventTouchUpInside)];
    [doneB setBackgroundImage:[UIImage imageNamed:@"路径 2065"] forState:(UIControlStateNormal)];
    [butBg addSubview:doneB];
}

#pragma mark - tableView代理方法
/**
 * tableView的分区数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/**
 * tableView分区里的行数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArr.count;
}

/**
 * tableViewCell的相关属性
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"PerfectingInfoCell";
    //出列可重用的cell
    PerfectingInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell= [[[NSBundle mainBundle] loadNibNamed:@"PerfectingInfoCell" owner:nil options:nil] firstObject];
    }
    
    //修改cell属性，使其在选中时无色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImage *image = [UIImage imageNamed:self.imageNameArr[indexPath.row]];
    cell.imageV.image = image;
    NSString *title = self.titleArr[indexPath.row];
    cell.titleL.text = title;

    if (indexPath.row == 0) {
        cell.contentL.text = @"请选择接单地区";
    } else if (indexPath.row == 1) {
        cell.contentL.text = @"请选择您的服务范围";
    } else if (indexPath.row == 2) {
        cell.contentL.text = @"请进行身份认证";
    }
    
    return cell;
}

/**
 * tableViewCell的高度
 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

/**
 * 分区头的高度
 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

/**
 * 分区脚的高度
 */
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

/**
 * 分区头
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

/**
 * 分区脚
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}


/**
 * tableViewCell的点击事件
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@", indexPath);
    PerfectingInfoCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    __weak typeof(cell) weakCell = cell;
    
    if ([cell.titleL.text isEqualToString:@"接单区域"]) {
        CSquyuViewController *VC = [CSquyuViewController new];
        VC.islogin = @"login";
        VC.commitBlock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr, NSMutableArray * _Nonnull muarr) {
            self.areaArr = muarr;
            NSArray *stringArray = [str componentsSeparatedByString:@"/"];
            if (stringArray.count > 0) {
                NSMutableAttributedString *attString = [NSString getAttributedStringWithOriginalString:str originalColor:[UIColor blackColor] originalFont:FontSize(14) originalLineSpacing:0 changeString:[stringArray firstObject]  changeColor:zhutiColor changeFont:FontSize(14)];
                weakCell.contentL.attributedText = attString;
                weakCell.contentL.textAlignment = NSTextAlignmentRight;
            }
            NSLog(@"%@", str);
            NSLog(@"%@", idstr);
            NSLog(@"%@", muarr);
            [self.paramDict setValue:idstr forKey:@"area"];
        };
        if (self.areaArr.count > 0) {
            VC.muarr = self.areaArr;
        }
        
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    if ([cell.titleL.text isEqualToString:@"服务范围"]) {
        CSsecviceViewController *VC = [[CSsecviceViewController alloc]init];
        VC.isrgister = @"zhuce";
        VC.commitBlock = ^(NSString * _Nonnull str, NSMutableArray * _Nonnull seleArr) {
            self.secviceArr = seleArr;
            weakCell.contentL.textColor = [UIColor blackColor];
            weakCell.contentL.text = @"已提交";
            [self.paramDict setValue:str forKey:@"faultType"];
            NSLog(@"-------------111%@111-------------", str);
            NSLog(@"-------------222%@222-------------", seleArr);
            self.fwfwArray = seleArr;
        };
        if (self.secviceArr.count > 0) {
            VC.seleArr = self.secviceArr;
        }
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    if ([cell.titleL.text isEqualToString:@"身份认证"]) {
        CSIdAuthViewController *VC = [CSIdAuthViewController new];
        VC.from = @"perfectingInfo";
        VC.commitBlock = ^(NSMutableDictionary * _Nonnull dict, NSMutableArray * _Nonnull muarr) {
            weakCell.contentL.textColor = [UIColor blackColor];
            weakCell.contentL.text = @"已提交";
            self.cerImageArr = muarr;
            //self.imageArr = muarr;
            [self.paramDict setValue:dict[@"idCard"] forKey:@"idCard"];
            [self.paramDict setValue:dict[@"name"] forKey:@"name"];
            NSLog(@"完善信息-身份认证 输出字典数据 %@", dict);
            NSLog(@"完善信息-身份认证 输出数组数据 %@", muarr);
        };
        if ([weakCell.contentL.text isEqualToString:@"已提交"]) {
            KMyLog(@"身份认证 状态为已提交的话 不做页面跳转");
        } else {
            [self.navigationController pushViewController:VC animated:YES];
        }
    }
}

#pragma mark - 高空作业证
//高空作业证
- (void)skillBClick:(UIButton *)sender {
    if (self.cerImageArr.count == 3) {
        UIImagePickerController *pick = [[UIImagePickerController alloc]init];
        pick.delegate = self;
        [UIAlertController showPhotoAlerControllerPicker:pick controller:self sourceView:nil];
    } else {
        ShowToastWithText(@"请先进行身份认证");
    }
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind] removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];

    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    CGFloat www = (KWIDTH -62)/4;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(16+(www+10)* (i % 4), self.bottomf +15 + (www + 10) * (i / 4), www, www);
        [self.scrollView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            
            CGFloat height = KHEIGHT - (image.bottom + 20) - 16;
            if (height < 147) {
                height = 147;
            }
            self.butBg.frame = CGRectMake(0, image.bottom + 20, KWIDTH, height);
            self.scrollView.contentSize = CGSizeMake(kScreen_Width, self.butBg.bottom);
            //最大的图片数量
            NSInteger maxImageNum = 100;
            
            if (i == maxImageNum) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        } else {
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

//查询上传服务范围的所有选项
- (void)getCertificateListByFaultIdData {
    
    [NetWorkTool POST:getCertificateListByFaultId param:nil success:^(id dic) {
        KMyLog(@"输出一下看有没有值 %@", dic);
        for (NSDictionary *moDic in [dic objectForKey:@"data"]) {
            [DYModelMaker DY_makeModelWithDictionary:moDic modelKeyword:@"" modelName:@""];
            ZYServiceSkillModel *model = [ZYServiceSkillModel mj_objectWithKeyValues:moDic];
            [self.killArray addObject:model];
            [self.selectArr addObject:model.fault_id];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark - 完成注册
//完成注册
- (void)done:(UIButton *)sender {
    
    if (!self.paramDict[@"area"]) {
        ShowToastWithText(@"请选择接单区域");
        return;
    }
    if (!self.paramDict[@"faultType"]) {
        ShowToastWithText(@"请选择服务范围");
        return;
    }
    if (!self.paramDict[@"idCard"]) {
        ShowToastWithText(@"请进行身份认证");
        return;
    }
    if (!self.paramDict[@"name"]) {
        ShowToastWithText(@"请进行身份认证");
        return;
    }
    
    //非注册页过来的
    if (self.phone.length > 0) {
        
        NSPredicate *filterPredicate_same = [NSPredicate predicateWithFormat:@"SELF IN %@",self.fwfwArray];
        NSArray *filter_no = [self.selectArr filteredArrayUsingPredicate:filterPredicate_same];
        NSLog(@"%@",filter_no);
        
        if (filter_no != nil && ![filter_no isKindOfClass:[NSNull class]] && filter_no.count != 0){
            //执行array不为空时的操作
            if (self.imageArr.count <= 1) {
                ShowToastWithText(@"请上传相关证件");
                return;
            }
            [self.paramDict setValue:self.phone forKey:@"phone"];
            [self.paramDict setValue:self.stepOneDict[@"invitationCode"] forKey:@"invitationCode"];

        } else {
            
            [self.paramDict setValue:self.phone forKey:@"phone"];
        }
        
    } else {
        //注册页过来的
        if (self.imageArr.count <= 1) {
            ShowToastWithText(@"请上传相关证件");
            return;
        }
        [self.paramDict setValue:self.stepOneDict[@"phone"] forKey:@"phone"];
        [self.paramDict setValue:self.stepOneDict[@"invitationCode"] forKey:@"invitationCode"];
    }
    
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.cerImageArr.count)];
    [self.imageArr insertObjects:self.cerImageArr atIndexes:indexSet];
    
    NSLog(@"%@", self.paramDict);
    NSLog(@"%@", self.imageArr);
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [NetWorkTool registerUploadPicWithUrl:LRegin param:self.paramDict key:@"image" image:self.imageArr withSuccess:^(id dic) {
        NSLog(@"注册结果:%@", dic);
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if ([[dic objectForKey:@"errorCode"] integerValue] == 0) {
            //非注册页过来的
            if (self.phone.length > 0) {
                TabBarViewController *tbc = [TabBarViewController new];
                [self presentViewController:tbc animated:YES completion:nil];
            } else {
                //注册页过来的
                ShowToastWithText(@"恭喜您，注册成功");
                CSLoginViewController *VC = [CSLoginViewController new];
                [self.navigationController pushViewController:VC animated:YES];
            }

        } else if ([[dic objectForKey:@"errorCode"] integerValue] == 2) {

            ShowToastWithText(dic[@"msg"]);
        } else if ([[dic objectForKey:@"errorCode"] integerValue] == 5) {

            ShowToastWithText(dic[@"msg"]);
        }
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        ShowToastWithText(@"网络不好,请重试");
        NSLog(@"失败的原因是什么");
    }];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
