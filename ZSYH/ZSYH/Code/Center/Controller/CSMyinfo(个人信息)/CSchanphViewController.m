//
//  CSchanphViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/7.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSchanphViewController.h"
#import "DPSendCodeButton.h"

@interface CSchanphViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *yzmTF;
@property (weak, nonatomic) IBOutlet YZMButton *yzmBut;

@end

@implementation CSchanphViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改绑定手机号";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    [self.rightbutton setTitle:@"确定" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    
    DPSendCodeButton *sendCodeButton = [[DPSendCodeButton alloc]initWithFrame:_yzmBut.bounds identify:@"LoginViewButton"];
    //    sendCodeButton.backgroundColor = [UIColor redColor];
    [sendCodeButton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    sendCodeButton.titleLabel.font = FontSize(13);
    [sendCodeButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [_yzmBut addSubview:sendCodeButton];
    // Do any additional setup after loading the view from its nib.
}

- (void)rightClick{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] = NOTNIL(_phoneTF.text);
    param[@"yzCode"] = NOTNIL(_yzmTF.text);
    
    [NetWorkTool POST:changePhone param:param success:^(id dic) {
        ShowToastWithText(@"修改成功");
        [self.navigationController popViewControllerAnimated:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (IBAction)yzmaAction:(YZMButton *)sender {
    return;
//    __weak typeof(self) weakSelf = self;
//    if (![self isMobileNumberOnly:_phoneTF.text]) {
//        ShowToastWithText(@"请输入正确电话号码");
//        return;
//    }
//    [sender timeFailBeginFrom:60];
//
//    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
//    param[@"phone"] =_phoneTF.text;
//
//    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
//        ShowToastWithText(@"发送成功");
//    } other:^(id dic) {
//
//    } fail:^(NSError *error) {
//
//    } needUser:YES];
}

-(void)click{
    
    //去除字符串中空格
    _phoneTF.text = [_phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_phoneTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =_phoneTF.text;
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        ShowToastWithText(@"验证码发送成功");
        [self.phoneTF resignFirstResponder];
        [self.yzmTF becomeFirstResponder];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

///判断是否为电话号码

- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
        
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}
    
@end
