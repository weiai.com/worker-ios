//
//  CSNewChangePhoneViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSNewChangePhoneViewController.h"

@interface CSNewChangePhoneViewController ()
@property (weak, nonatomic) IBOutlet UITextField *myyzmTF;
@property (weak, nonatomic) IBOutlet UITextField *myphoneTF;

@end

@implementation CSNewChangePhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改绑定手机号";
    [self.leftbutton setTitle:@"取消" forState:(UIControlStateNormal)];
    [self.rightbutton setTitle:@"确定" forState:(UIControlStateNormal)];
    self.myphoneTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
   
}

- (IBAction)sendMAgAction:(YZMButton *)sender {
    
    //去除字符串中空格
    _myphoneTF.text = [_myphoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_myphoneTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    [sender timeFailBeginFrom:60];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =NOTNIL(_myphoneTF.text) ;
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        ShowToastWithText(@"验证码发送成功");
        [self.myphoneTF resignFirstResponder];
        [self.myyzmTF becomeFirstResponder];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (void)rightClick{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] = NOTNIL(_myphoneTF.text);
    param[@"yzCode"] = NOTNIL(_myyzmTF.text);
    
    [NetWorkTool POST:changePhone param:param success:^(id dic) {
        ShowToastWithText(@"修改成功");
        [self.navigationController popViewControllerAnimated:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

///判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

@end
