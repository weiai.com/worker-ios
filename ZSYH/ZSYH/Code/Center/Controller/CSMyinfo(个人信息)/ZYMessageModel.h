//
//  ZYMessageModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYMessageModel : NSObject
@property (nonatomic, strong) NSString *guaranteePeriod;
@property (nonatomic, strong) NSString *factory_id;
@property (nonatomic, strong) NSString *parts_remarks;
@property (nonatomic, strong) NSString *parts_details;
@property (nonatomic, strong) NSString *parts_name;

@property (nonatomic, strong) NSString *modifyDate;
@property (nonatomic, strong) NSString *is_good_choice;
@property (nonatomic, strong) NSString *is_new_push;
@property (nonatomic, strong) NSString *parts_price;
@property (nonatomic, strong) NSString *repair_user_price;

@property (nonatomic, strong) NSString *parts_type;
@property (nonatomic, strong) NSString *parts_brand;
@property (nonatomic, strong) NSString *lower_agentB;
@property (nonatomic, strong) NSString *lower_agentA;
@property (nonatomic, strong) NSString *parts_number;

@property (nonatomic, strong) NSString *soft;
@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *is_hot_push;
@property (nonatomic, strong) NSString *parts_place;
@property (nonatomic, strong) NSString *lower_user;

@property (nonatomic, strong) NSString *parts_quantity;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *cellCount;
@property (nonatomic, strong) NSString *is_today_hot;
@property (nonatomic, strong) NSString *createDate;

@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *agent_user_price;
@property (nonatomic, strong) NSString *parts_model;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) NSString *classification;

@end

NS_ASSUME_NONNULL_END
