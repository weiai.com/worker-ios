//
//  CSsecviceViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_OPTIONS(NSInteger, FromViewControllerType) {
    FromPerfectInfoVCType = 0,      //完善信息
    FromPersonInfoVCType = 1 << 0,  //个人中心
    FromUnknowVCType = 1 << 1       //未知
};
@interface CSsecviceViewController : BaseViewController
@property(nonatomic,copy)void (^commitBlock)(NSString *str, NSMutableArray *seleArr);
@property(nonatomic,strong)NSMutableArray *seleArr;
@property(nonatomic,strong)NSString *isrgister;
@end

NS_ASSUME_NONNULL_END
