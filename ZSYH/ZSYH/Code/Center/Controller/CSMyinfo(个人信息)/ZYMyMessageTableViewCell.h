//
//  ZYMyMessageTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyMessageTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *headerImg; //头像
@property (nonatomic, strong) UILabel *nameLab; //姓名
@property (nonatomic, strong) UILabel *dateLab; //日期
@property (nonatomic, strong) UITextView *textView; //留言文本
@property (nonatomic, strong) UIImageView *productImg; //产品描述图片
@property (nonatomic, strong) UILabel *proInfoLab; //产品描述信息
@property (nonatomic, strong) UILabel *hbspdgLab; //侯保产品订购 提示文本
@property (nonatomic, strong) UILabel *orderNumLab; //产品订购人数

@end

NS_ASSUME_NONNULL_END
