//
//  CSidraceInfomationViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSidraceInfomationViewController.h"

@interface CSidraceInfomationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLb;
@property (weak, nonatomic) IBOutlet UILabel *idcardLb;

@end

@implementation CSidraceInfomationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.title = @"身份证信息";
    NSString *str1 = [_model.id_card_no substringFromIndex:14];
    NSString *str2 = [_model.id_card_no substringToIndex:10];
    
    self.nameLb.text = _model.personName;

    self.idcardLb.text = [NSString stringWithFormat:@"%@****%@",str2,str1];


 
}

@end
