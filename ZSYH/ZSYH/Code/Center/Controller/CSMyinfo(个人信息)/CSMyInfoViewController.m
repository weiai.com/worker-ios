//
//  CSMyInfoViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyInfoViewController.h"
#import "CSListDetaileTableViewCell.h"
#import "HFAddressViewController.h"
#import "CSCmyIdcardViewController.h"
#import "CSniChengViewController.h"
#import "CSPersonModel.h"
#import "CSNewChangePhoneViewController.h"
#import "MOFSPickerManager.h"
#import "CSsecviceViewController.h"
#import "CSchanphViewController.h"
#import "CSquyumodel.h"

#import "CSXGphoneViewController.h"
#import "CSquyuViewController.h"
#import "CSidraceInfomationViewController.h"
#import "SkillsCerViewController.h"
#import "UserAgreementViewController.h"

@interface CSMyInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSArray *titArr;
@property(nonatomic,strong)UIImage *headerImage;

@end

@implementation CSMyInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_myTableView) {
        [self request];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
 
    self.title = @"个人信息";
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    
    [self request];
}

-(void)request{
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        [self.myTableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.titArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSListDetaileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSListDetaileTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.leftLB.text = self.titArr[indexPath.row];
    cell.leftLB.font = KFontPingFangSCRegular(14);
    cell.leftLB.textColor = K666666;
    cell.userImage.hidden = YES;
    cell.rightLB.hidden = YES;
    
    if (indexPath.row == 1) {
        cell.userImage.hidden = NO;
        
        if (_model.image_url.length <= 0) {
            cell.userImage.image = [UIImage imageNamed:@"userImage"];
        }else {
            if ([_model.image_url hasPrefix:@"http"]) {
                [cell.userImage sd_setImageWithURL:[NSURL URLWithString:_model.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
            }else{
                sdimg(cell.userImage, _model.image_url);
            }
        }
        cell.userImage.userInteractionEnabled = YES;
        UITapGestureRecognizer *ges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seleImage:)];
        [cell.userImage addGestureRecognizer:ges];
    }
    if (indexPath.row == 5) {
        cell.rightLB.text = _model.area_name;
        cell.rightLB.hidden = NO;
    }
    
    if (indexPath.row == 0) {
        cell.rightLB.text = _model.nickname;
        cell.rightLB.hidden = NO;
    }
    
    if (indexPath.row == 2) {
        cell.rightLB.text = _model.phone;
        cell.rightLB.hidden = NO;
    }
    return cell;
}

-(void)seleImage:(UITapGestureRecognizer *)sender{
    UIImagePickerController *pick = [[UIImagePickerController alloc]init];
    pick.allowsEditing = YES;
    pick.delegate = self;
    [UIAlertController showPhotoAlerControllerPicker:pick controller:self sourceView:nil];
}

- (void)upLoadHeadIcon:(UIImage *)image {
    self.headerImage = image;
    [MBProgressHUD showHUDAddedTo:self.view msg:@"上传头像..." animated:YES];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    //param[@"type"] = @"logo";
    
    [NetWorkTool UploadPicWithUrl:setUpUserImage param:param key:@"images" image:image withSuccess:^(id dic) {
        [self request];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = info[UIImagePickerControllerEditedImage];
    [self upLoadHeadIcon:image];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 3) {
        [self.navigationController pushViewController:[HFAddressViewController new] animated:YES];
    } else if (indexPath.row == 4){
        
        NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
        for (NSDictionary * mudic in _model.area) {
            CSquyumodel *model = [[CSquyumodel alloc]init];
            [model setValuesForKeysWithDictionary:mudic];
            model.idstr = [mudic objectForKey:@"id"];
            model.isdelde = @"0";
            [muarr addObject:model];
        }
        CSquyuViewController *dsaff = [[CSquyuViewController alloc]init];
        dsaff.muarr = muarr;
        dsaff.mynblco = ^(NSString * _Nonnull idstr) {
        };
        [self.navigationController pushViewController:dsaff animated:YES];
        
    } else if (indexPath.row == 0){
        
        CSniChengViewController *cell = [[CSniChengViewController alloc]init];
        cell.model = self.model;
        [self.navigationController pushViewController:cell animated:YES];
    } else if (indexPath.row == 2){
        
        CSchanphViewController *cell = [[CSchanphViewController alloc]init];
        //cell.model = self.model;
        
        [self.navigationController pushViewController:cell animated:YES];
    } else if (indexPath.row == 5){
        
        CSsecviceViewController *seVC = [[CSsecviceViewController  alloc]init];
        NSArray  *array = [_model.faultType componentsSeparatedByString:@","];//其中"-"号为分隔符
        seVC.seleArr = [array mutableCopy];
        [self.navigationController pushViewController:seVC animated:YES];
        
    } else if (indexPath.row == 6){
        //身份证信息
        CSidraceInfomationViewController *seVC = [[CSidraceInfomationViewController  alloc]init];
        seVC.model = _model;
        [self.navigationController pushViewController:seVC animated:YES];
        
    } else if (indexPath.row == 7){
        
        //相关证件
        SkillsCerViewController *seVC = [[SkillsCerViewController alloc]init];
        [self.navigationController pushViewController:seVC animated:YES];
        
    } else if (indexPath.row == 8){
        
        //用户协议
        UserAgreementViewController *agreeVC = [[UserAgreementViewController alloc] init];
        [self.navigationController pushViewController:agreeVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 48;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSListDetaileTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSListDetaileTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

-(NSArray *)titArr{
    if (!_titArr) {
        _titArr = @[@"昵称",@"头像",@"修改绑定手机号",@"收货地址",@"接单区域",@"我的服务范围",@"身份证信息",@"相关证件",@"用户协议"];
        
    }
    return _titArr;
}


@end
