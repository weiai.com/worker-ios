//
//  ZYMyQRCodeViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSPersonModel.h"
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyQRCodeViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *model;

@end

NS_ASSUME_NONNULL_END
