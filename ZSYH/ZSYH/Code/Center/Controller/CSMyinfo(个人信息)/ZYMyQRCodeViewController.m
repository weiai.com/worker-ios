//
//  ZYMyQRCodeViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyQRCodeViewController.h"
#import <CoreImage/CoreImage.h>

@interface ZYMyQRCodeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *backView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UIImageView *QRCodeImg;

@end

@implementation ZYMyQRCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"专属二维码";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.backView.layer.cornerRadius = 10;
    self.backView.layer.masksToBounds = YES;
    
    self.headerImg.layer.cornerRadius = self.headerImg.bounds.size.width/2;
    self.headerImg.layer.masksToBounds = YES;
    if (_model.image_url.length <= 0) {
        self.headerImg.image = [UIImage imageNamed:@"userImage"];
    } else {
        if ([_model.image_url hasPrefix:@"http"]) {
            [self.headerImg sd_setImageWithURL:[NSURL URLWithString:_model.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
        } else {
            sdimg(self.headerImg, _model.image_url);
        }
    }
    
    self.nameLab.text = _model.nickname;
    
    [self loadCodeImg];

}

- (void)loadCodeImg {
    //1.创建一个二维码滤镜实例(CIFilter)
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // 滤镜恢复默认设置
    [filter setDefaults];
    //2.给滤镜添加数据
    NSString *string1 = myCodeImage;
    NSString *string2 = _model.Id;
    NSString *resaultStr = [NSString stringWithFormat:@"%@%@%@",HOST_LINK,string1,string2];
    NSLog(@"拼接之后的字符串是 %@", resaultStr);
    NSData *data = [resaultStr dataUsingEncoding:NSUTF8StringEncoding];
    //使用KVC的方式给filter赋值
    [filter setValue:data forKeyPath:@"inputMessage"];
    //3.生成二维码
    
    if (self.model.Id == nil && self.model.Id == NULL) {
        
    } else {
        CIImage *image = [filter outputImage];
        
        //4.在中心增加一张图片
        UIImage *img = [self createNonInterpolatedUIImageFormCIImage:image withSize:KWIDTH];
        //5.把中央图片划入二维码里面
        //5.1开启图形上下文
        UIGraphicsBeginImageContext(img.size);
        //5.2将二维码的图片画入
        [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
        //UIImage *centerImg = [UIImage imageNamed:@"putongyonghu"];
        
        UIImageView *centerImg = [[UIImageView alloc] init];
        if (_model.image_url.length <= 0) {
            centerImg.image = [UIImage imageNamed:@"userImage"];
        } else {
            if ([_model.image_url hasPrefix:@"http"]) {
                [centerImg sd_setImageWithURL:[NSURL URLWithString:_model.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
            } else {
                sdimg(centerImg, _model.image_url);
            }
        }
        
        CGFloat centerW = 80;
        CGFloat centerH = 80;
        CGFloat centerX = img.size.width / 2 - centerW / 2;
        CGFloat centerY = img.size.height / 2 - centerH / 2;
        
        [centerImg.image drawInRect:CGRectMake(centerX, centerY, centerW, centerH)];
        
        //5.3获取绘制好的图片
        UIImage *finalImg = UIGraphicsGetImageFromCurrentImageContext();
        //5.4关闭图像上下文
        UIGraphicsEndImageContext();
        //6.显示最终二维码
        self.QRCodeImg.image = finalImg;
    }
//    //4.在中心增加一张图片
//    UIImage *img = [self createNonInterpolatedUIImageFormCIImage:image withSize:KWIDTH];
//    //5.把中央图片划入二维码里面
//    //5.1开启图形上下文
//    UIGraphicsBeginImageContext(img.size);
//    //5.2将二维码的图片画入
//    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
//    //UIImage *centerImg = [UIImage imageNamed:@"putongyonghu"];
    
//    UIImageView *centerImg = [[UIImageView alloc] init];
//    if (_model.image_url.length <= 0) {
//        centerImg.image = [UIImage imageNamed:@"userImage"];
//    } else {
//        if ([_model.image_url hasPrefix:@"http"]) {
//            [centerImg sd_setImageWithURL:[NSURL URLWithString:_model.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
//        } else {
//            sdimg(centerImg, _model.image_url);
//        }
//    }
//
//    CGFloat centerW = 80;
//    CGFloat centerH = 80;
//    CGFloat centerX = img.size.width / 2 - centerW / 2;
//    CGFloat centerY = img.size.height / 2 - centerH / 2;
//
//    [centerImg.image drawInRect:CGRectMake(centerX, centerY, centerW, centerH)];
//
//    //5.3获取绘制好的图片
//    UIImage *finalImg = UIGraphicsGetImageFromCurrentImageContext();
//    //5.4关闭图像上下文
//    UIGraphicsEndImageContext();
//    //6.显示最终二维码
//    self.QRCodeImg.image = finalImg;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //    // 1. 创建一个二维码滤镜实例(CIFilter)
    //    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    //    // 滤镜恢复默认设置
    //    [filter setDefaults];
    //
    //    // 2. 给滤镜添加数据
    //    NSString *string = @"学号:3180807001 \n  姓名:黄振锋";
    //    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    //    // 使用KVC的方式给filter赋值
    //    [filter setValue:data forKeyPath:@"inputMessage"];
    //
    //    // 3. 生成二维码
    //    CIImage *image = [filter outputImage];
    //
    //    //4.在中心增加一张图片
    //    UIImage *img = [self createNonInterpolatedUIImageFormCIImage:image withSize:screenW];
    //
    //    //5.把中央图片划入二维码里面
    //    //5.1开启图形上下文
    //    UIGraphicsBeginImageContext(img.size);
    //    //5.2将二维码的图片画入
    //    [img drawInRect:CGRectMake(0, 0, img.size.width, img.size.height)];
    //    UIImage *centerImg = [UIImage imageNamed:@"HZhenF"];
    //    CGFloat centerW=img.size.width*0.3;
    //    CGFloat centerH=centerW;
    //    CGFloat centerX=(img.size.width-centerW)*0.5;
    //    CGFloat centerY=(img.size.height -centerH)*0.5;
    //    [centerImg drawInRect:CGRectMake(centerX, centerY, centerW, centerH)];
    //    //5.3获取绘制好的图片
    //    UIImage *finalImg=UIGraphicsGetImageFromCurrentImageContext();
    //    //5.4关闭图像上下文
    //    UIGraphicsEndImageContext();
    //
    //    //6.显示最终二维码
    //    self.imageView.image = finalImg;
}

/**
 *  调用该方法处理图像变清晰
 *  根据CIImage生成指定大小的UIImage
 *
 *  @param image CIImage
 *  @param size  图片宽度以及高度
 */
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    //1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    //2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

@end
