//
//  CSniChengViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSPersonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSniChengViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *model;

@end

NS_ASSUME_NONNULL_END
