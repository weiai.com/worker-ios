
//
//  SkillsCerViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "SkillsCerViewController.h"
#import "SkillsCerModel.h"
#import "SkillsCerCell.h"
#import "CSMyInfoViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
@interface SkillsCerViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *addImageArr;
@property (nonatomic,strong) NSMutableArray *imageArr;
@property (nonatomic,strong) UIImageView *myimage;
@property (nonatomic,strong) UIView *addView;
@property (nonatomic,assign) CGFloat bottomf;
@property (nonatomic, strong) UIView *butBg;
@property (nonatomic, assign) BOOL showData;//数据展示初始数据
@property (nonatomic,strong) NSMutableArray *deleteIdsArr;//删除的图片id数组
@end

@implementation SkillsCerViewController

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageArr  = [NSMutableArray array];
    self.deleteIdsArr = [NSMutableArray array];
    self.addImageArr  = [NSMutableArray array];
  
   
    [self setUI];
    [self loadData];
    
    self.title = @"相关证件";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}

- (void)loadData {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [NetWorkTool POST:getCertificateImgs param:param success:^(id dic) {
        KMyLog(@"获取相关证件 数据请求 %@", dic);
        
        NSArray *arr = dic[@"data"];
        self.dataArr = [NSMutableArray array];
        [self.dataArr addObjectsFromArray:[SkillsCerModel mj_objectArrayWithKeyValuesArray:arr]];
        for (int i=0; i<self.dataArr.count; i++) {
            UIImageView *imageView = [[UIImageView alloc]init];
            imageView.image = [UIImage imageNamed:@""];
            [self.imageArr addObject:imageView];
        }
        for (SkillsCerModel *model in self.dataArr) {
            model.isOriginalData = @"1";
        }
        self.myimage = [[UIImageView alloc]init];
        self->_myimage.image = imgname(@"xiangGuanZhengjian");
        [self.imageArr addObject:self->_myimage];
        
        self->_showData = YES;
        [self showinageWirhArr:self->_imageArr];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        adjustInset(_scrollView);
    }
    return _scrollView;
}
-(void)setUI
{
    
    [self.view addSubview:self.scrollView];
    
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(16, kNaviHeight+10, KWIDTH-32, 100)];
    [self.scrollView addSubview: self.addView];
    CGFloat www = (KWIDTH -62)/4;
    self.bottomf = 0;
    
    CGRect ffff = self.addView.frame;
    ffff.size.height = www +10;
    self.addView.frame = ffff;
    
    CGFloat heigehtbg = KHEIGHT -self.addView.bottom - 16;
    
    if (heigehtbg < 147) {
        heigehtbg = 147;
    }
    
    UIView *butBg = [[UIView alloc]initWithFrame:CGRectMake(0, self.addView.bottom, KWIDTH, heigehtbg)];
    self.butBg = butBg;
    [self.scrollView addSubview:butBg];
    butBg.userInteractionEnabled = YES;
    butBg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UIButton *doneB = [UIButton new];
    doneB.frame = CGRectMake(28, 42, KWIDTH-28-28, 48);
    doneB.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    doneB.titleLabel.font = [UIFont systemFontOfSize:15];
    [doneB setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneB setTitle:@"提交" forState:(UIControlStateNormal)];
    [doneB addTarget:self action:@selector(submit) forControlEvents:(UIControlEventTouchUpInside)];
    [butBg addSubview:doneB];
}
-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    if (ind< _imageArr.count-1) {
        SkillsCerModel *model = self.dataArr[ind];
        if ([model.isOriginalData isEqualToString:@"1"]) {
            [self.deleteIdsArr addObject:model.id];
        }else
        {
            [self.addImageArr removeObjectAtIndex:ind];
        }
        
    [_imageArr[ind] removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    [self.dataArr removeObjectAtIndex:ind];
    [self showinageWirhArr:_imageArr];
 
    }
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    [self.addImageArr addObject:image];//新加的 放入提交接口上送参数中
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
    
    //添加的新图片
    SkillsCerModel *model = [[SkillsCerModel alloc]init];
    model.isOriginalData = @"0";
    [self.dataArr insertObject:model atIndex:0];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    CGFloat www = (KWIDTH -62)/4;
    
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(16+(www+10)* (i % 4), self.bottomf +15 + (www + 10) * (i / 4), www, www);
        if (_showData == YES) {
            if (i < self.dataArr.count) {
                SkillsCerModel *model = self.dataArr[i];
                [image sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:nil];
            }
            
        }
        [self.scrollView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            
            CGFloat height = KHEIGHT - (image.bottom + 20) - 16;
            if (height < 147) {
                height = 147;
            }
            self.butBg.frame = CGRectMake(0, image.bottom + 20, KWIDTH, height);
            self.scrollView.contentSize = CGSizeMake(kScreen_Width, self.butBg.bottom);
            //最大的图片数量
            NSInteger maxImageNum = 100;
            
            if (i == maxImageNum) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        } else {
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
    _showData = NO;
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

- (void)submit {
    
    if (self.imageArr.count==1) {
        ShowToastWithText(@"请先选择图片");
        return;
    }
    
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    if (self.deleteIdsArr.count>0) {
        NSString *str = [self.deleteIdsArr componentsJoinedByString:@","];
        param[@"ids"] = NOTNIL(str);
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    kWeakSelf;
    [NetWorkTool UploadPicWithUrl:updateCertificateImgs param:param key:@"image" image:self.addImageArr withSuccess:^(id dic) {

        NSString *errorCodeStr = [NSString stringWithFormat:@"%@",[dic objectForKeyNotNil:@"errorCode"]];
        if ([errorCodeStr isEqualToString:@"0"]) {
            [weakSelf.navigationController  popViewControllerAnimated:YES];
            ShowToastWithText(@"修改成功");
        }else{
            NSString *msg = [NSString stringWithFormat:@"%@",[dic objectForKeyNotNil:@"msg"]];
            ShowToastWithText(msg);
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } failure:^(NSError *error) {

    }];
}

@end

