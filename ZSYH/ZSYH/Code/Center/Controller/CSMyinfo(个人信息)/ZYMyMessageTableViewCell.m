//
//  ZYMyMessageTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYMyMessageTableViewCell.h"

@implementation ZYMyMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

  
}

- (void)setFrame:(CGRect)frame{
    //frame.origin.x += 10;
    frame.origin.y += 10;
    frame.size.height -= 10;
    //frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 269);
        //bottomV.layer.cornerRadius = 10;
        //bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        //用户头像
        UIImageView *headerImg = [[UIImageView alloc] init];
        headerImg.frame = CGRectMake(13, 10, 50, 50);
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:headerImg.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:headerImg.bounds.size];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
        //设置大小
        maskLayer.frame = headerImg.bounds;
        //设置图形样子
        maskLayer.path = maskPath.CGPath;
        headerImg.layer.mask = maskLayer;
        [bottomV addSubview:headerImg];
        self.headerImg = headerImg;
        
        //用户昵称
        UILabel *nameLab = [[UILabel alloc] init];
        nameLab.frame = CGRectMake(headerImg.right +13, 18, KWIDTH/2, 16);
        nameLab.textAlignment = NSTextAlignmentLeft;
        nameLab.font = FontSize(14);
        nameLab.textColor = K333333;
        [bottomV addSubview:nameLab];
        self.nameLab = nameLab;
        
        //留言日期/时间
        UILabel *dateLab = [[UILabel alloc] init];
        dateLab.frame = CGRectMake(headerImg.right + 13, nameLab.bottom +10, KWIDTH/2, 16);
        dateLab.font = FontSize(14);
        dateLab.textColor = K666666;
        [bottomV addSubview:dateLab];
        self.dateLab = dateLab;
        
        //留言内容
        UITextView *textView = [[UITextView alloc] init];
        textView.frame = CGRectMake(13, headerImg.bottom +10, KWIDTH - 13*2, 67);
        textView.font = FontSize(14);
        textView.editable = NO;
        [bottomV addSubview:textView];
        self.textView = textView;
        
        //产品详情
        UILabel *bgLab = [[UILabel alloc] init];
        bgLab.frame = CGRectMake(13, textView.bottom +10, KWIDTH - 26, 106);
        bgLab.backgroundColor = [UIColor colorWithHexString:@"#FAFAFA"];
        [bottomV addSubview:bgLab];
        
        //产品图片
        UIImageView *productImg = [[UIImageView alloc] init];
        productImg.frame = CGRectMake(0, 0, 106, 106);
        [bgLab addSubview:productImg];
        self.productImg = productImg;
        
        //产品信息
        UILabel *proInfoLab = [[UILabel alloc] init];
        proInfoLab.frame = CGRectMake(productImg.right +10, 10, KWIDTH - 26 - 20 - 106, 50);
        proInfoLab.font = FontSize(14);
        proInfoLab.textColor = K666666;
        proInfoLab.numberOfLines = 0;
        [bgLab addSubview:proInfoLab];
        self.proInfoLab = proInfoLab;
        
        //候保产品订购
        UILabel *hbspdgLab = [[UILabel alloc] init];
        hbspdgLab.frame = CGRectMake(productImg.right + 10, proInfoLab.bottom + 15, KWIDTH/3, 16);
        hbspdgLab.text = @"候保产品订购";
        hbspdgLab.font = FontSize(14);
        hbspdgLab.textColor = [UIColor colorWithHexString:@"#70BE68"];
        //[bgLab addSubview:hbspdgLab];
        self.hbspdgLab = hbspdgLab;
    }
    return self;
}

@end
