//
//  CSidraceInfomationViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSPersonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSidraceInfomationViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *model;

@end

NS_ASSUME_NONNULL_END
