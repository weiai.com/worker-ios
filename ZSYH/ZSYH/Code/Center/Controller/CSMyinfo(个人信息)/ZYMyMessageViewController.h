//
//  ZYMyMessageViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSPersonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYMyMessageViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *mymodel;

@end

NS_ASSUME_NONNULL_END
