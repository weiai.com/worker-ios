//
//  CSMyAccConViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyAccConViewController.h"
#import "CSGanderdrHomeTableViewCell.h"
#import "CSMyDayViewController.h"

@interface CSMyAccConViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *dataSource;

@end

@implementation CSMyAccConViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的账户";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    [self.myTableView registerNib:[UINib nibWithNibName:@"CSGanderdrHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSGanderdrHomeTableViewCell"];
    self.myTableView.tableFooterView = [UIView new];
    self.dataSource = [NSMutableArray arrayWithCapacity:1];
    [self.dataSource addObject:@"当日收益"];
    [self.dataSource addObject:@"收益查询"];
    [self.dataSource addObject:@"收益说明"];

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSGanderdrHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSGanderdrHomeTableViewCell" forIndexPath:indexPath];
    cell.leftLB.text = [NSString stringWithFormat:@"%@",[_dataSource safeObjectAtIndex:indexPath.row]];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        //当日收益
        [self.navigationController pushViewController:[CSMyDayViewController  new] animated:YES];
    }
}

@end
