//
//  CSNewACcontViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSNewACcontViewController.h"
#import "CSsetWAnewtlementViewController.h"
#import "CSmyIncomeFormViewController.h"
#import "CSmyGrandViewController.h"
#import "MyacontViewController.h"

@interface CSNewACcontViewController ()
@property (weak, nonatomic) IBOutlet UILabel *totalLB;
@property (weak, nonatomic) IBOutlet UILabel *canReflectLB;
@property (strong, nonatomic) UILabel *ruleLable;
@property (nonatomic,strong) NSMutableDictionary *mydic;
@property (weak, nonatomic) IBOutlet UILabel *zongshouyiLB;
@property (weak, nonatomic) IBOutlet UILabel *zhanghuLab;
@property (nonatomic, strong) UIView *bgView;

@end

@implementation CSNewACcontViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的账户";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
    [self showMyzhanghuUI];
    [self shwoBgview];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self request];
}

- (void)showMyzhanghuUI {

    //订单收益 按钮
    UIButton *ddsyBtn = [[UIButton alloc] init];
    ddsyBtn.frame = CGRectMake(19, 339, (KWIDTH-47)/2, 86);
    [ddsyBtn setImage:imgname(@"dingdanshouyi-1") forState:UIControlStateNormal];
    [ddsyBtn addTarget:self action:@selector(myMoneyAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:ddsyBtn];
    
    //结算历史 按钮
    UIButton *jslsBtn = [[UIButton alloc] init];
    jslsBtn.frame = CGRectMake(ddsyBtn.right +9, 339, (KWIDTH-47)/2, 86);
    [jslsBtn setImage:imgname(@"jiesuanlishi-1") forState:UIControlStateNormal];
    [jslsBtn addTarget:self action:@selector(accontHestoryAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:jslsBtn];
    
    //补助金  按钮
    UIButton *bzjBtn = [[UIButton alloc] init];
    bzjBtn.frame = CGRectMake(19, ddsyBtn.bottom +10, (KWIDTH-47)/2, 86);
    [bzjBtn setImage:imgname(@"buzhujin-1") forState:UIControlStateNormal];
    [bzjBtn addTarget:self action:@selector(myGrandAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bzjBtn];
    
    //我的账户  按钮
    UIButton *wdzhBtn = [[UIButton alloc] init];
    wdzhBtn.frame = CGRectMake(bzjBtn.right +9, ddsyBtn.bottom +10, (KWIDTH-47)/2, 86);
    [wdzhBtn setImage:imgname(@"wodezhanghu-1") forState:UIControlStateNormal];
    [wdzhBtn addTarget:self action:@selector(myZhanghuBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:wdzhBtn];

    //结算信息
    UILabel *jiesuanLab = [[UILabel alloc] init];
    jiesuanLab.frame = CGRectMake(16, bzjBtn.bottom +10, 70, 22);
    jiesuanLab.text = @"结算政策";
    jiesuanLab.font = FontSize(16);
    jiesuanLab.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.view addSubview:jiesuanLab];
    
    //结算政策内容
    UILabel *ruleLabel = [[UILabel alloc] init];
    ruleLabel.frame = CGRectMake(16, jiesuanLab.bottom +15, KWIDTH-32, 90);
    ruleLabel.font = FontSize(14);
    ruleLabel.numberOfLines = 0;
    ruleLabel.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.view addSubview:ruleLabel];
    self.ruleLable = ruleLabel;
}

-(void)request{
    
    [NetWorkTool POST:myAccont param:nil success:^(id dic) {
        
      
        self.mydic = [[dic objectForKey:@"data"] mutableCopy];
        self.totalLB.text =[NSString stringWithFormat:@"%@",[self.mydic objectForKey:@"todayEan"]];
        self.canReflectLB.text=[NSString stringWithFormat:@"%@",[self.mydic objectForKey:@"allEarn"]];
        NSMutableString *ruleStr = [NSMutableString stringWithFormat:@"%@",[self.mydic objectForKey:@"polComment"]];
        [ruleStr insertString:@"\n" atIndex:28];
        [ruleStr insertString:@"\n" atIndex:52];
        self.ruleLable.text = ruleStr;
        self.zongshouyiLB.text=[NSString stringWithFormat:@"%@",[self.mydic objectForKey:@"balance"]];
        if (strIsEmpty(self.totalLB.text)) {
            self.totalLB.text = @"0.00";
        }
        if (strIsEmpty(self.canReflectLB.text)) {
            self.canReflectLB.text = @"0.00";
        }
        if (strIsEmpty(self.zongshouyiLB.text)) {
            self.zongshouyiLB.text = @"0.00";
        }
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } other:^(id dic) {
        
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    //[0]    (null)    @"allEarn" : (no summary)//历史总收益
    //[2]    (null)    @"todayEan" : (no summary)//今日收益
    //[3]    (null)    @"balance" : @"0"
}

- (IBAction)shenqingjiesuanAction:(UIButton *)sender {
    //申请结算
    
    [NetWorkTool POST:getCanAccont param:nil success:^(id dic) {
        
        CSsetWAnewtlementViewController *dfdf = [[CSsetWAnewtlementViewController alloc]init];
        dfdf.moneyStr = [dic objectForKey:@"data"];
        [self.navigationController pushViewController:dfdf animated:YES];
        
    } other:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgView];

    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    KMyLog(@"您点击了 申请结算 按钮");
}

//订单收益 按钮 点击事件处理
- (void)myMoneyAction:(UIButton *)sender {
    //订单收益
    CSmyIncomeFormViewController *cell = [[CSmyIncomeFormViewController alloc]init];
    cell.titleStr = @"订单收益";
    [singlTool shareSingTool].titlsr = @"订单收益";
    [self.navigationController pushViewController:cell animated:YES];
}

//补助金 按钮 点击事件处理
- (void)myGrandAction:(UIButton *)sender {
    //补助金
    CSmyGrandViewController *grandVC = [[CSmyGrandViewController alloc]init];
    //seveVC.view.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    grandVC.titleStr = @"补助金";
    [self.navigationController pushViewController:grandVC animated:YES];
}

//结算历史 按钮 点击事件处理
- (void)accontHestoryAction:(UIButton *)sender {
    //结算历史
    CSmyIncomeFormViewController *cell2 = [[CSmyIncomeFormViewController alloc]init];
    cell2.titleStr = @"结算历史";
    [singlTool shareSingTool].titlsr = @"结算历史";
    [self.navigationController pushViewController:cell2 animated:YES];
}

//我的账户 按钮 点击事件s处理
- (void)myZhanghuBtnAction:(UIButton *)sender {
    //我的账户
    MyacontViewController *vc = [[MyacontViewController alloc] init];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(154);
        make.left.offset(70);
        make.right.offset(-70);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    //upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 37, KWIDTH-140, 42)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.numberOfLines = 0;
    UpLable.text = @"亲,今天不是结算日哦,请到\n结算日申请结算!";
    //self.bgUPLable =UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"请到结算日申请结算";
    //self.bgdowLable = DowLable;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#CA9F61"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-22);
    }];
}

-(void)ikenowAction:(UIButton *)but{
    [_bgView removeFromSuperview];
    //if (self.myblock) {
        //self.myblock(@"");
    //}
    //[self.navigationController popViewControllerAnimated:YES];
}


@end
