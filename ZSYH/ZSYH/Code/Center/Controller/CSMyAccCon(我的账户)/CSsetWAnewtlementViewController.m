//
//  CSsetWAnewtlementViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSsetWAnewtlementViewController.h"
#import <WXApi.h>
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthInfo.h"
#import "APRSASigner.h"
#import "CSPersonModel.h"

@interface CSsetWAnewtlementViewController ()
@property (weak, nonatomic) IBOutlet UILabel *moneyLB;
@property (weak, nonatomic) IBOutlet UIButton *wxBut;
@property (weak, nonatomic) IBOutlet UIButton *alipayBut;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftLayout;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *leftLB;
@property (weak, nonatomic) IBOutlet UILabel *goshouquanlB;

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) CSPersonModel *model;

@property (nonatomic, strong) NSString *ALIPAY;

@property (nonatomic, assign) NSInteger indx;

@end

@implementation CSsetWAnewtlementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请结算";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [Center addObserver:self selector:@selector(wechataction:) name:@"wechatshouquanSuccess" object:nil];
    
    self.model = [[CSPersonModel alloc]init];

    self.phone = [USER_DEFAULT objectForKey:@"phone"];
    self.moneyLB.text = [NSString stringWithFormat:@"%@%@",@"￥" ,_moneyStr] ;
    [self requsert];
    
    
}

-(void)requsert{
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        [DYModelMaker DY_makeModelWithDictionary:[dic objectForKey:@"data"] modelKeyword:@"" modelName:@""];
    
        [weakSelf.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        
        if (!strIsEmpty(weakSelf.model.alipay_name)) {
            [weakSelf.wxBut setImage:imgname(@"newawachat") forState:(UIControlStateNormal)];
            [weakSelf.alipayBut setImage:imgname(@"seleAlipay") forState:(UIControlStateNormal)];
            weakSelf.leftLayout.constant = 66;
            mysdimg(weakSelf.userImage, weakSelf.model.alipay_image_url);
            
            weakSelf.leftLB.text = weakSelf.model.alipay_name;
            weakSelf.goshouquanlB.text = @"切换账号";
            self->_indx = 1;
            
        } else if (!strIsEmpty(weakSelf.model.wx_image_url)) {
            [weakSelf.wxBut setImage:imgname(@"seleWX") forState:(UIControlStateNormal)];
            [weakSelf.alipayBut setImage:imgname(@"newalpiay") forState:(UIControlStateNormal)];
            weakSelf.leftLayout.constant = 66;
            mysdimg(weakSelf.userImage, weakSelf.model.wx_image_url);
            
            weakSelf.leftLB.text = weakSelf.model.wx_name;
            weakSelf.goshouquanlB.text = @"切换账号";
            self->_indx = 0;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

-(void)wechataction:(NSNotification *)no{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *code = [NSString stringWithFormat:@"%@",[no.userInfo objectForKey:@"code"]];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"code"] =code;
    param[@"phone"] = self.phone;
    kWeakSelf;
    [NetWorkTool POST:wxAppLogin param:param success:^(id dic) {
        KMyLog(@"sfas超市%@",dic);
        [weakSelf requsert];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

- (IBAction)wxAction:(UIButton *)sender {
    self.indx = 0;
    [self.wxBut setImage:imgname(@"seleWX") forState:(UIControlStateNormal)];
    [self.alipayBut setImage:imgname(@"newalpiay") forState:(UIControlStateNormal)];
    
    kWeakSelf;
    if (!strIsEmpty(weakSelf.model.wx_image_url)) {
        weakSelf.leftLayout.constant = 66;
        mysdimg(weakSelf.userImage, weakSelf.model.wx_image_url);
        
        weakSelf.leftLB.text = weakSelf.model.wx_name;
        weakSelf.goshouquanlB.text = @"切换账号";
        weakSelf.userImage.hidden = NO;
    } else {
        weakSelf.leftLayout.constant = 16;
        weakSelf.leftLB.text = @"您尚未绑定微信账户";
        weakSelf.userImage.hidden = YES;
        weakSelf.goshouquanlB.text = @"去授权";
    }
    
    KMyLog(@"您点击了 微信 按钮");
}

- (IBAction)alipayaction:(UIButton *)sender {
    self.indx = 1;
    [self.wxBut setImage:imgname(@"newawachat") forState:(UIControlStateNormal)];
    [self.alipayBut setImage:imgname(@"seleAlipay") forState:(UIControlStateNormal)];
    kWeakSelf;
    if (!strIsEmpty(weakSelf.model.alipay_name)) {
        weakSelf.leftLayout.constant = 66;
        mysdimg(weakSelf.userImage, weakSelf.model.alipay_image_url);
        weakSelf.leftLB.text = weakSelf.model.alipay_name;
        weakSelf.goshouquanlB.text = @"切换账号";
        weakSelf.userImage.hidden = NO;
    } else {
        weakSelf.leftLayout.constant = 16;
        weakSelf.leftLB.text = @"您尚未绑定支付宝账户";
        weakSelf.userImage.hidden = YES;

        weakSelf.goshouquanlB.text = @"去授权";
    }
    KMyLog(@"您点击了 支付宝 按钮");
}

-(void)zhifubaosucess:(NSString *)aucode{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"code"] =aucode;
    param[@"phone"] = self.phone;
    kWeakSelf;
    [NetWorkTool POST:aliPayAppLogin param:param success:^(id dic) {
        KMyLog(@"sfas超市%@",dic);
        [weakSelf requsert];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

- (IBAction)GOShouquan:(id)sender {

    if (self.indx == 0) {
        if ([WXApi isWXAppInstalled]) {
            SendAuthReq* req =[[SendAuthReq alloc ] init];
            req.scope = @"snsapi_userinfo";
            req.state = @"wx_oauth2_authorization_state";
            
            [WXApi sendReq:req];
        }else{
            //没安装微信
        }
    }else{
        //
        //[NetWorkTool POST:@"http://192.168.2.205:8088/cs_pay/agentAlipayToLogin" param:nil
        //          success:^(id dic) {
        //            KMyLog(@"dasf%@",dic);
        //          } other:^(id dic) {
        //
        //          } fail:^(NSError *error) {
        //
        //          } needUser:YES];
        //    return;
        
        // 重要说明
        // 这里只是为了方便直接向商户展示支付宝的整个支付流程；所以Demo中加签过程直接放在客户端完成；
        // 真实App里，privateKey等数据严禁放在客户端，加签过程务必要放在服务端完成；
        // 防止商户私密数据泄露，造成不必要的资金损失，及面临各种安全风险；
        /*============================================================================*/
        /*=======================需要填写商户app申请的===================================*/
        /*============================================================================*/
        NSString *pid = @"2088431820896382";
        NSString *appID = @"2019032163612874";
        
        // 如下私钥，rsa2PrivateKey 或者 rsaPrivateKey 只需要填入一个
        // 如果商户两个都设置了，优先使用 rsa2PrivateKey
        // rsa2PrivateKey 可以保证商户交易在更加安全的环境下进行，建议使用 rsa2PrivateKey
        // 获取 rsa2PrivateKey，建议使用支付宝提供的公私钥生成工具生成，
        // 工具地址：https://doc.open.alipay.com/docs/doc.htm?treeId=291&articleId=106097&docType=1
        NSString *rsa2PrivateKey = @"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCrzYXaHYxnmEZJe/lWWxvJ7XB96z/kEPV5nnGhWDZa2ZVni7a/Q8EBnlLX72d19ZN21BLDzCXhl9EV2YpAN41i8zrIQXvtcM1Vpsp/LlpayjOIjqg+JsLLX6E+4+ThSY/ZXsTHYWJDnPdHvvbsLRxfDgneOxL8arV1Gxm4POSU0yJDygdXkob7PWqDak5rcJceq8xsZ42s6FtzHshahN1S79gZTAqbl5DmBOnUdZLVxaUYHn8571q50s29S4bD92BAmMrtYZ5xDSGvPGj39W8OUv/nVxcoMhXLhWuhjVnMpVJZq6OgCZ1EtQcqDAxpScbqdIoix4c6wGjKg4pXsYpHAgMBAAECggEAY0Wt23wJfm7Z5mpRiBm7M/IUd14xG+rEvVLVnAJp3hMuJpfvsBNJelLu0cGPSfTuEdK3SRY0qWddNL/wB+1RsGSVidN09Z2huKVZCyZQpXeDUZwFEWy6UGPgu4NZ5OfpzBPbps3ZxvHzXSy2Z4AljbOB9jyyWtKaKaHQOcpN1sQ6Pe3EziodOYlzIjHSOgm5C+r0wUx0fRRAu8Ivb5yiyNvBt496srQ/31yZQ4NCV8i8OGIBmQJfbo8DQ6lfan2jcknYTOWKY5E2GMTUlzJ+8BRyxl1e4CkSQ8YsuX4Mqzpb1AvgKa93D13WV+Zk7IKznRxj9Vo/JX1kaq/O9P9UwQKBgQDjEiyDZl9gkiDRFL6sjgniQBIRsWbX+hHB87KBfdNBA5aEVR8t/D1gxPsxlKp6GgR8qMBxRRgJsxZ7mkarAsFqzsBl8MklTp9KZEeG/hfH+bZ1AjL+k5D+85FKGhm7cGY96fPKmy9GoBxy/ofdqEdT29xWAN41Aq7uEMYilODp5wKBgQDBsMz1vHblyTPgczasER3diMRTysCjdn4AnDVNTk3oxUhlWqQpwuJMtq1pll30fDci0QlWJCKg2cAnsSkIDFKIyhOmYtgJcwotrJEbdtHeakl31S3nQwA3E7XaZq4SjwIW4zlZtv+wopo68BU5B67DxkESVxlO3iXeXpyPZ2AQoQKBgFZ825uI6Jcd0YYRgrayuy0D/l3i1is5Qn4/ViRgSpCezzvEOEYJp08ueAbgezZapo7cr7/08zzui4e0sWn9eLI34axyVArzsVRicaQHMAOpJ0fV+Jpiln2lRBeXbWmpXCgE7iijhxQ9c9iQ/ir9J33XZksTtY83YG2o0e7mCezNAoGAffg9pMh4z4Om8KAd+5R6Rv1PKmvgE6/ZzQ3LAMcflnbQWdK0l9B+m4PnjpdT7dDnjwZCuzpvJwvXlDj9RZW2C+7ZDsqUVLjz8rab/S+u2Kdptrz3yMNjnW59RFipd7p4kmgfgYCqFLf79L72Hownln4XDhWFiSyjllcUHZQO/2ECgYBd47FNgO0aIa7L9IYX4YYtAO+mB0uJjvM7EtmIq5WndNJ4EsCK0P+uykmQ72j/SwtjcB0/qVo0XS3Y4Mfpk+e8/I2Zyx+zMpe30wtki8IRLoPIjmhUYA+O5oEYlGZ2WT6CisvTWX7HgbxJRdGRhq1XeaADIQ6ICVvHy5Uht1jZCA==";
        NSString *rsaPrivateKey = @"";
        /*============================================================================*/
        /*============================================================================*/
        /*============================================================================*/
        //pid和appID获取失败,提示
        if ([pid length] == 0 ||
            [appID length] == 0 ||
            ([rsa2PrivateKey length] == 0 && [rsaPrivateKey length] == 0)) {
            UIAlertController *alert = [UIAlertController
                                        alertControllerWithTitle:@"提示"
                                        message:@"缺少pid或者appID或者私钥,请检查参数设置"
                                        preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"知道了"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action){
                                                               
                                                           }];
            [alert addAction:action];
            [self presentViewController:alert animated:YES completion:^{ }];
            return;
        }
        
        //生成 auth info 对象
        APAuthInfo *authInfo = [APAuthInfo new];
        authInfo.pid = pid;
        authInfo.appID = appID;
        //auth type
        NSString *authType = [[NSUserDefaults standardUserDefaults] objectForKey:@"authType"];
        if (authType) {
            authInfo.authType = authType;
        }
        //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
        NSString *appScheme = @"zhushiyahuan";
        // 将授权信息拼接成字符串
        NSString *authInfoStr = [authInfo description];
        NSLog(@"authInfoStr = %@",authInfoStr);
        
     //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
        NSString *signedString = nil;
        APRSASigner* signer = [[APRSASigner alloc] initWithPrivateKey:((rsa2PrivateKey.length > 1)?rsa2PrivateKey:rsaPrivateKey)];
        if ((rsa2PrivateKey.length > 1)) {
            signedString = [signer signString:authInfoStr withRSA2:YES];
        } else {
            signedString = [signer signString:authInfoStr withRSA2:NO];
        }
        kWeakSelf;
        // 将签名成功字符串格式化为订单字符串,请严格按照该格式
        if (signedString.length > 0) {
            authInfoStr = [NSString stringWithFormat:@"%@&sign=%@&sign_type=%@", authInfoStr, signedString, ((rsa2PrivateKey.length > 1)?@"RSA2":@"RSA")];
            [[AlipaySDK defaultService] auth_V2WithInfo:authInfoStr
                                             fromScheme:appScheme
                                               callback:^(NSDictionary *resultDic) {
                                                   NSLog(@"result = %@",resultDic);
                                                   // 解析 auth code
                                                   NSString *result = resultDic[@"result"];
                                                   NSString *authCode = nil;
                                                   
                                                   if (result.length>0) {
                                                       NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                                                       for (NSString *subResult in resultArr) {
                                                           if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                                                               authCode = [subResult substringFromIndex:10];
                                                               break;
                                                           }
                                                       }
                                                   }
                                                   NSLog(@"授权结果 authCode = %@", authCode?:@"");
                                                   weakSelf.ALIPAY =[NSString stringWithFormat:@"%@",authCode];
                                                   [weakSelf zhifubaosucess:authCode];
                                               }];
        }
    }
}

- (IBAction)quedingaCTION:(UIButton *)sender {
    if ([_moneyStr integerValue]   ==  0) {
        ShowToastWithText(@"结算金额必须大于0");
        return;
    }
    
    sender.userInteractionEnabled = NO;
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = _indx == 0 ?@"1":@"2";
    [NetWorkTool POST:tixianAccont param:param success:^(id dic) {
       
       ZSTipsView *view = [ZSTipsView new];
       view.upImage.image = [UIImage imageNamed:@"accyes"];
       view.dowLable.text = @"结算申请已提交，等待平台处理";
       [view.btn setTitle:@"完成" forState:(UIControlStateNormal)];
       [view.btn setBackgroundColor:[UIColor colorWithHexString:@"#CA9F61"]];
       view.btnBlock = ^{
            [self.navigationController popViewControllerAnimated:YES];
       };
       [view show];
     
       
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

@end
