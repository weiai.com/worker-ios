//
//  CSMyDayViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyDayViewController.h"
#import "CSMyAccDayTableViewCell.h"

@interface CSMyDayViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSArray *titArr;

@end

@implementation CSMyDayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"当日收益";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
   
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSMyAccDayTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSMyAccDayTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight+5, KWIDTH, KHEIGHT-kNaviHeight-5) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSMyAccDayTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSMyAccDayTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}
-(NSArray *)titArr{
    if (!_titArr) {
        _titArr = @[@"昵称",@"头像",@"电话",@"地址",@"银行卡信息"];
        
    }
    return _titArr;
    
}


@end
