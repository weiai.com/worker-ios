//
//  CSpolicListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSpolicListTableViewCell.h"

@implementation CSpolicListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}
-(void)setModel:(PolicyListModel *)model
{
   
    _model = model;
    if (_model) {
        self.numLab.text = _model.id;
        self.titleLab.text = _model.pol_name;
        self.contentLab.text = _model.pol_comment;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
