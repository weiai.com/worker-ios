//
//  CSpolicListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PolicyListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSpolicListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property(nonatomic,strong)PolicyListModel *model;
@end

NS_ASSUME_NONNULL_END
