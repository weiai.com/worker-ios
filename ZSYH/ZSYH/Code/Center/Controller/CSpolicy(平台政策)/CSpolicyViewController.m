//
//  CSpolicyViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSpolicyViewController.h"
#import "CSpolicListTableViewCell.h"
#import "PolicyListModel.h"

@interface CSpolicyViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;

@end

@implementation CSpolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"平台政策";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self requesrt];
   
    
}
-(void)requesrt{
    kWeakSelf;
    [NetWorkTool POST:cs_userGetPolicy param:nil success:^(id dic) {
        
        self.mydateSource = [PolicyListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [weakSelf.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSpolicListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSpolicListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    PolicyListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSpolicListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSpolicListTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}
@end
