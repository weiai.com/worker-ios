//
//  MyacontViewController.h
//  Distributor
//
//  Created by 主事丫环 on 2019/5/15.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "BaseViewController.h"
#import "CSPersonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyacontViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *model;

@end

NS_ASSUME_NONNULL_END
