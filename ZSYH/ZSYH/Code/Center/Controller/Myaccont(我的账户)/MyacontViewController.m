//
//  MyacontViewController.m
//  Distributor
//
//  Created by 主事丫环 on 2019/5/15.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "MyacontViewController.h"
#import <WXApi.h>
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthInfo.h"
#import "APRSASigner.h"
#import "DISaccmodel.h"

@interface MyacontViewController ()
@property(nonatomic,strong)DISaccmodel *alipayModel;
@property (weak, nonatomic) IBOutlet UILabel *wechatGoshouquan;
@property (weak, nonatomic) IBOutlet UILabel *weibangdingWechat;
@property (weak, nonatomic) IBOutlet UILabel *weibandingAlipay;
@property (weak, nonatomic) IBOutlet UILabel *alipayGoshouquna;
@property (weak, nonatomic) IBOutlet UISwitch *wechaSwith;
@property (weak, nonatomic) IBOutlet UISwitch *alipaySwith;
@property (weak, nonatomic) IBOutlet UIImageView *wechatImage;
@property (weak, nonatomic) IBOutlet UILabel *wechatName;
@property (weak, nonatomic) IBOutlet UIImageView *alipayImage;
@property (weak, nonatomic) IBOutlet UILabel *alipayname;
@property (nonatomic,assign) BOOL issuccess;

@end

@implementation MyacontViewController

- (void)viewWillAppear:(BOOL)animated {
    [self request];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的账户";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.alipayModel = [[DISaccmodel alloc]init];
    self.wechatName.hidden = YES;
    self.wechatImage.hidden = YES;
    self.alipayname.hidden = YES;
    self.alipayImage.hidden = YES;
    self.issuccess = YES;
    
    /*kWeakSelf;
    
    if (!strIsEmpty(_model.wx_image_url)) {
        self.wechatGoshouquan.text = @"切换账号";
        self.weibangdingWechat.hidden = YES;
        self.wechatName.hidden = NO;
        self.wechatImage.hidden = NO;
        [self.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.wx_image_url]]];
        self.wechatName.text = [NSString stringWithFormat:@"%@",self.model.wx_name];
    }
    
    if (!strIsEmpty(_model.alipay_image_url)) {
        self.alipayGoshouquna.text = @"切换账号";
        self.weibandingAlipay.hidden = YES;
        self.alipayname.hidden = NO;
        self.alipayImage.hidden = NO;
        [self.alipayImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.alipay_image_url]]];
        self.alipayname.text = [NSString stringWithFormat:@"%@",weakSelf.model.alipay_name];
    }
    
    if (!strIsEmpty(_model.type)) {
        if ([_model.type integerValue] == 1) {
            self.wechaSwith.on = YES;
            self.alipaySwith.on = NO;
            
            if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                [self.wechaSwith setEnabled:YES];
                [self.alipaySwith setEnabled:YES];
            } else {
                [self.wechaSwith setEnabled:NO];
                [self.alipaySwith setEnabled:NO];
            }
            
        } else if ([_model.type integerValue] == 2){
            self.alipaySwith.on = YES;
            self.wechaSwith.on = NO;
            
//            if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
//                [self.wechaSwith setEnabled:YES];
//                [self.alipaySwith setEnabled:YES];
//            } else {
//                [self.wechaSwith setEnabled:NO];
//                [self.alipaySwith setEnabled:NO];
//            }
        }
    } else {
        self.wechaSwith.on = NO;
        self.alipaySwith.on = NO;
        [self.wechaSwith setEnabled:NO];
        [self.alipaySwith setEnabled:NO];
    }*/
    
    //if (!self.model) {
        //self.model = [[CSPersonModel alloc]init];
        //[self request];
    //}
    
    [Center addObserver:self selector:@selector(zhifubao:) name:@"shouquanSuccess" object:nil];
    
    [Center addObserver:self selector:@selector(wechataction:) name:@"wechatshouquanSuccess" object:nil];
}

-(void)request{
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        
        if (!strIsEmpty(self->_model.wx_image_url)) {
            self.wechatGoshouquan.text = @"切换账号";
            self.weibangdingWechat.hidden = YES;
            self.wechatName.hidden = NO;
            self.wechatImage.hidden = NO;
            [self.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.wx_image_url]]];
            self.wechatName.text = [NSString stringWithFormat:@"%@",self.model.wx_name];
            
            self.wechaSwith.on = YES;
            self.alipaySwith.on = NO;
        }
        
        if (!strIsEmpty(self->_model.alipay_image_url)) {
            self.alipayGoshouquna.text = @"切换账号";
            self.weibandingAlipay.hidden = YES;
            self.alipayname.hidden = NO;
            self.alipayImage.hidden = NO;
            [self.alipayImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.alipay_image_url]]];
            self.alipayname.text = [NSString stringWithFormat:@"%@",weakSelf.model.alipay_name];
            
            self.alipaySwith.on = YES;
            self.wechaSwith.on = NO;
        }
        
        //if (!strIsEmpty(self->_model.type)) {
            //if ([self->_model.type integerValue] == 1) {
            if ([self->_model.type isEqualToString:@"1"]) {
                self.wechaSwith.on = YES;
                self.alipaySwith.on = NO;
                
                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {

                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                    //微信 开关 添加点击事件
                    [self.wechaSwith addTarget:self action:@selector(wechaSwithChange:)forControlEvents:UIControlEventTouchUpInside];
                    //支付宝 开关 添加点击事件
                    [self.alipaySwith addTarget:self action:@selector(alipaySwithChange:)forControlEvents:UIControlEventTouchUpInside];
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
            //} else if ([self->_model.type integerValue] == 2){
                } else if ([self->_model.type isEqualToString:@"2"]){
                self.alipaySwith.on = YES;
                self.wechaSwith.on = NO;
                
                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                    //微信 开关 添加点击事件
                    [self.wechaSwith addTarget:self action:@selector(wechaSwithChange:)forControlEvents:UIControlEventTouchUpInside];
                    //支付宝 开关 添加点击事件
                    [self.alipaySwith addTarget:self action:@selector(alipaySwithChange:)forControlEvents:UIControlEventTouchUpInside];
                    
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
            //}
        } else {
            self.wechaSwith.on = NO;
            self.alipaySwith.on = NO;
            [self.wechaSwith setEnabled:YES];
            [self.alipaySwith setEnabled:YES];
        }
        
        //[singlTool shareSingTool].needReasfh = YES;

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

//微信   设置默认账号 开关切换事件处理
- (void)wechaSwithChange:(UISwitch *)swt {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = @"1"; //type 1微信2 支付宝
    
    [MBProgressHUD showHUDAddedTo: self.view animated:YES];

    [NetWorkTool POST:setDefaultTX param:param success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        KMyLog(@"微信设置默认账号 切换成功 %@", dic);
        self.wechaSwith.on = YES;
        self.alipaySwith.on = NO;
        
        
        [self.wechaSwith setEnabled:YES];
        [self.alipaySwith setEnabled:YES];
        
        [self showOnleText:dic[@"msg"] delay:2];

    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showOnleText:dic[@"msg"] delay:2];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

//支付宝 设置默认账号 开关切换事件处理
- (void)alipaySwithChange:(UISwitch *)swt {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = @"2"; //type 1微信2 支付宝
    
    [MBProgressHUD showHUDAddedTo: self.view animated:YES];
    [NetWorkTool POST:setDefaultTX param:param success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        KMyLog(@"支付宝设置默认账号 切换成功 %@", dic);
        self.wechaSwith.on = NO;
        self.alipaySwith.on = YES;
        
        [self.wechaSwith setEnabled:YES];
        [self.alipaySwith setEnabled:YES];
        
        [self showOnleText:dic[@"msg"] delay:2];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showOnleText:dic[@"msg"] delay:2];

    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    } needUser:YES];
}

-(void)wechataction:(NSNotification *)no{
    NSString *code = [NSString stringWithFormat:@"%@",[no.userInfo objectForKey:@"code"]];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"code"] = code;
    param[@"phone"] =_model.phone;
    kWeakSelf;
    [NetWorkTool POST:wxAppLogin param:param success:^(id dic) {
        KMyLog(@"sfas超市%@",dic);
//        [weakSelf.alipayModel setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
//        weakSelf.wechatGoshouquan.text = @"切换账号";
//        weakSelf.weibangdingWechat.hidden = YES;
//        weakSelf.wechatName.hidden = NO;
//        weakSelf.wechatImage.hidden = NO;
//        [weakSelf.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.alipayModel.wx_image_url]]];
//        weakSelf.wechatName.text = weakSelf.alipayModel.wx_name;
//        //self.wechaSwith.on = NO;
//        //self.alipaySwith.on = NO;
        
//        //if ([weakSelf.model.type integerValue] == 1) {
//        if ([weakSelf.model.type isEqualToString:@"1"]) {
//            self.wechaSwith.on = YES;
//            self.alipaySwith.on = NO;
//        //} else if ([weakSelf.model.type integerValue] == 2){
//            } else if ([weakSelf.model.type isEqualToString:@"2"]){
//            self.wechaSwith.on = NO;
//            self.alipaySwith.on = YES;
//        }
        
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        
        if (!strIsEmpty(self->_model.wx_image_url)) {
            self.wechatGoshouquan.text = @"切换账号";
            self.weibangdingWechat.hidden = YES;
            self.wechatName.hidden = NO;
            self.wechatImage.hidden = NO;
            [self.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.wx_image_url]]];
            self.wechatName.text = [NSString stringWithFormat:@"%@",self.model.wx_name];
            
            self.wechaSwith.on = YES;
            self.alipaySwith.on = NO;
        }
        
        if (!strIsEmpty(self->_model.alipay_image_url)) {
            self.alipayGoshouquna.text = @"切换账号";
            self.weibandingAlipay.hidden = YES;
            self.alipayname.hidden = NO;
            self.alipayImage.hidden = NO;
            [self.alipayImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.alipay_image_url]]];
            self.alipayname.text = [NSString stringWithFormat:@"%@",weakSelf.model.alipay_name];
            
            self.alipaySwith.on = YES;
            self.wechaSwith.on = NO;
        }
        
        if (!strIsEmpty(self->_model.type)) {
            if ([self->_model.type isEqualToString:@"1"]) {
                self.wechaSwith.on = YES;
                self.alipaySwith.on = NO;

                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
            } else if ([self->_model.type isEqualToString:@"2"]){
                self.alipaySwith.on = YES;
                self.wechaSwith.on = NO;

                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
            }
        } else {
            self.wechaSwith.on = NO;
            self.alipaySwith.on = NO;
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (IBAction)wechatswith:(UISwitch *)sender {
    self.alipaySwith.on = !self.wechaSwith.on;
    NSString *str = @"1";
    if (self.alipaySwith.on) {
        str = @"2";
    }
    [self shwotiixnawith:str];
}

- (IBAction)alipayAcrti:(UISwitch *)sender {
    self.wechaSwith.on = !self.alipaySwith.on;
    NSString *str = @"1";
    if (self.alipaySwith.on) {
        str = @"2";
    }
    [self shwotiixnawith:str];
}

-(void)zhifubao:(NSNotification *)no{
    NSString *code = [NSString stringWithFormat:@"%@",[no.userInfo objectForKey:@"result"]];
    
    NSArray  *array = [code componentsSeparatedByString:@"="];
    NSString *sfdsfs = [NSString stringWithFormat:@"%@",array[4]];
    NSString *codelst = [sfdsfs substringToIndex:sfdsfs.length - 6];
    KMyLog(@"支付宝 ---111111---%@",codelst);
    KMyLog(@"支付宝 ---222222---%@",code);
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"code"] = codelst;
    param[@"phone"] =_model.phone;
    kWeakSelf;
    [NetWorkTool POST:aliPayAppLogin param:param success:^(id dic) {
        KMyLog(@"支付宝 回调 返回数据 %@",dic);
        [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
            
            [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
            
            if (!strIsEmpty(self->_model.wx_image_url)) {
                self.wechatGoshouquan.text = @"切换账号";
                self.weibangdingWechat.hidden = YES;
                self.wechatName.hidden = NO;
                self.wechatImage.hidden = NO;
                [self.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.wx_image_url]]];
                self.wechatName.text = [NSString stringWithFormat:@"%@",self.model.wx_name];
                
                self.wechaSwith.on = YES;
                self.alipaySwith.on = NO;
            }
            
            if (!strIsEmpty(self->_model.alipay_image_url)) {
                self.alipayGoshouquna.text = @"切换账号";
                self.weibandingAlipay.hidden = YES;
                self.alipayname.hidden = NO;
                self.alipayImage.hidden = NO;
                [self.alipayImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.alipay_image_url]]];
                self.alipayname.text = [NSString stringWithFormat:@"%@",weakSelf.model.alipay_name];
                
                self.alipaySwith.on = YES;
                self.wechaSwith.on = NO;
            }
            
            //if (!strIsEmpty(self->_model.type)) {
                //if ([self->_model.type integerValue] == 1) {
                if ([self->_model.type isEqualToString:@"1"]) {
                    self.wechaSwith.on = YES;
                    self.alipaySwith.on = NO;

                    if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                        [self.wechaSwith setEnabled:YES];
                        [self.alipaySwith setEnabled:YES];
                    } else {
                        [self.wechaSwith setEnabled:NO];
                        [self.alipaySwith setEnabled:NO];
                    }
                //} else if ([self->_model.type integerValue] == 2){
                    } else if ([self->_model.type isEqualToString:@"2"]){
                    self.alipaySwith.on = YES;
                    self.wechaSwith.on = NO;
                    
                    if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                        [self.wechaSwith setEnabled:YES];
                        [self.alipaySwith setEnabled:YES];
                    } else {
                        [self.wechaSwith setEnabled:NO];
                        [self.alipaySwith setEnabled:NO];
                    }
                //}
            } else {
                self.wechaSwith.on = NO;
                self.alipaySwith.on = NO;
            }
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)shwotiixnawith:(NSString *)type{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = type;
//    [NetWorkTool POST:setDefaultTX param:param success:^(id dic) {
//        ShowToastWithText(@"修改成功");
//        weakSelf.issuccess = NO;
//
//    } other:^(id dic) {
//        if (weakSelf.issuccess) {
//            weakSelf.wechaSwith.on = !weakSelf.wechaSwith.on;
//            //self.alipaySwith.on = !self.alipaySwith.on;
//        }
//    } fail:^(NSError *error) {
//        if (weakSelf.issuccess) {
//            weakSelf.wechaSwith.on = !weakSelf.wechaSwith.on;
//            //self.alipaySwith.on = !self.alipaySwith.on;
//        }
//    } needUser:YES];
}

- (IBAction)wechatAction:(UIButton *)sender {
    if ([WXApi isWXAppInstalled]) {
        SendAuthReq* req =[[SendAuthReq alloc ] init];
        req.scope = @"snsapi_userinfo";
        req.state = @"wx_oauth2_authorization_state";
        
        [WXApi sendReq:req];
    } else {
        //没安装维修
    }
}

-(void)tencentDidLogin {
    NSLog(@"yes");
}

- (IBAction)alipayAction:(UIButton *)sender {
    //    [NetWorkTool POST:@"http://192.168.2.205:8088/cs_pay/agentAlipayToLogin" param:nil
    //              success:^(id dic) {
    //                KMyLog(@"dasf%@",dic);
    //              } other:^(id dic) {
    //
    //              } fail:^(NSError *error) {
    //
    //              } needUser:YES];
    //    return;
    
    // 重要说明
    // 这里只是为了方便直接向商户展示支付宝的整个支付流程；所以Demo中加签过程直接放在客户端完成；
    // 真实App里，privateKey等数据严禁放在客户端，加签过程务必要放在服务端完成；
    // 防止商户私密数据泄露，造成不必要的资金损失，及面临各种安全风险；
    /*============================================================================*/
    /*=======================需要填写商户app申请的===================================*/
    /*============================================================================*/
    NSString *pid = @"2088431820896382";
    NSString *appID = @"2019032163612874";
    
    // 如下私钥，rsa2PrivateKey 或者 rsaPrivateKey 只需要填入一个
    // 如果商户两个都设置了，优先使用 rsa2PrivateKey
    // rsa2PrivateKey 可以保证商户交易在更加安全的环境下进行，建议使用 rsa2PrivateKey
    // 获取 rsa2PrivateKey，建议使用支付宝提供的公私钥生成工具生成，
    // 工具地址：https://doc.open.alipay.com/docs/doc.htm?treeId=291&articleId=106097&docType=1
    NSString *rsa2PrivateKey = @"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCrzYXaHYxnmEZJe/lWWxvJ7XB96z/kEPV5nnGhWDZa2ZVni7a/Q8EBnlLX72d19ZN21BLDzCXhl9EV2YpAN41i8zrIQXvtcM1Vpsp/LlpayjOIjqg+JsLLX6E+4+ThSY/ZXsTHYWJDnPdHvvbsLRxfDgneOxL8arV1Gxm4POSU0yJDygdXkob7PWqDak5rcJceq8xsZ42s6FtzHshahN1S79gZTAqbl5DmBOnUdZLVxaUYHn8571q50s29S4bD92BAmMrtYZ5xDSGvPGj39W8OUv/nVxcoMhXLhWuhjVnMpVJZq6OgCZ1EtQcqDAxpScbqdIoix4c6wGjKg4pXsYpHAgMBAAECggEAY0Wt23wJfm7Z5mpRiBm7M/IUd14xG+rEvVLVnAJp3hMuJpfvsBNJelLu0cGPSfTuEdK3SRY0qWddNL/wB+1RsGSVidN09Z2huKVZCyZQpXeDUZwFEWy6UGPgu4NZ5OfpzBPbps3ZxvHzXSy2Z4AljbOB9jyyWtKaKaHQOcpN1sQ6Pe3EziodOYlzIjHSOgm5C+r0wUx0fRRAu8Ivb5yiyNvBt496srQ/31yZQ4NCV8i8OGIBmQJfbo8DQ6lfan2jcknYTOWKY5E2GMTUlzJ+8BRyxl1e4CkSQ8YsuX4Mqzpb1AvgKa93D13WV+Zk7IKznRxj9Vo/JX1kaq/O9P9UwQKBgQDjEiyDZl9gkiDRFL6sjgniQBIRsWbX+hHB87KBfdNBA5aEVR8t/D1gxPsxlKp6GgR8qMBxRRgJsxZ7mkarAsFqzsBl8MklTp9KZEeG/hfH+bZ1AjL+k5D+85FKGhm7cGY96fPKmy9GoBxy/ofdqEdT29xWAN41Aq7uEMYilODp5wKBgQDBsMz1vHblyTPgczasER3diMRTysCjdn4AnDVNTk3oxUhlWqQpwuJMtq1pll30fDci0QlWJCKg2cAnsSkIDFKIyhOmYtgJcwotrJEbdtHeakl31S3nQwA3E7XaZq4SjwIW4zlZtv+wopo68BU5B67DxkESVxlO3iXeXpyPZ2AQoQKBgFZ825uI6Jcd0YYRgrayuy0D/l3i1is5Qn4/ViRgSpCezzvEOEYJp08ueAbgezZapo7cr7/08zzui4e0sWn9eLI34axyVArzsVRicaQHMAOpJ0fV+Jpiln2lRBeXbWmpXCgE7iijhxQ9c9iQ/ir9J33XZksTtY83YG2o0e7mCezNAoGAffg9pMh4z4Om8KAd+5R6Rv1PKmvgE6/ZzQ3LAMcflnbQWdK0l9B+m4PnjpdT7dDnjwZCuzpvJwvXlDj9RZW2C+7ZDsqUVLjz8rab/S+u2Kdptrz3yMNjnW59RFipd7p4kmgfgYCqFLf79L72Hownln4XDhWFiSyjllcUHZQO/2ECgYBd47FNgO0aIa7L9IYX4YYtAO+mB0uJjvM7EtmIq5WndNJ4EsCK0P+uykmQ72j/SwtjcB0/qVo0XS3Y4Mfpk+e8/I2Zyx+zMpe30wtki8IRLoPIjmhUYA+O5oEYlGZ2WT6CisvTWX7HgbxJRdGRhq1XeaADIQ6ICVvHy5Uht1jZCA==";
    NSString *rsaPrivateKey = @"";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    kWeakSelf;
    //pid和appID获取失败,提示
    if ([pid length] == 0 ||
        [appID length] == 0 ||
        ([rsa2PrivateKey length] == 0 && [rsaPrivateKey length] == 0)) {
        UIAlertController *alert = [UIAlertController
                                    alertControllerWithTitle:@"提示"
                                    message:@"缺少pid或者appID或者私钥,请检查参数设置"
                                    preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction
                                 actionWithTitle:@"知道了"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action){
                                 }];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:^{ }];
        return;
    }
    
    //生成 auth info 对象
    APAuthInfo *authInfo = [APAuthInfo new];
    authInfo.pid = pid;
    authInfo.appID = appID;
    
    //auth type
    NSString *authType = [[NSUserDefaults standardUserDefaults] objectForKey:@"authType"];
    if (authType) {
        authInfo.authType = authType;
    }
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"zhushiyahuan";
    
    // 将授权信息拼接成字符串
    NSString *authInfoStr = [authInfo description];
    NSLog(@"authInfoStr = %@",authInfoStr);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并
    //将签名字符串base64编码和UrlEncode
    NSString *signedString = nil;
    APRSASigner* signer = [[APRSASigner alloc] initWithPrivateKey:((rsa2PrivateKey.length > 1)?rsa2PrivateKey:rsaPrivateKey)];
    if ((rsa2PrivateKey.length > 1)) {
        signedString = [signer signString:authInfoStr withRSA2:YES];
    } else {
        signedString = [signer signString:authInfoStr withRSA2:NO];
    }
    
    // 将签名成功字符串格式化为订单字符串,请严格按照该格式
    if (signedString.length > 0) {
        authInfoStr = [NSString stringWithFormat:@"%@&sign=%@&sign_type=%@", authInfoStr, signedString, ((rsa2PrivateKey.length > 1)?@"RSA2":@"RSA")];
        [[AlipaySDK defaultService] auth_V2WithInfo:authInfoStr
                                         fromScheme:appScheme
                                           callback:^(NSDictionary *resultDic) {
                                               NSLog(@"result = %@",resultDic);
                                               // 解析 auth code
                                               NSString *result = resultDic[@"result"];
                                               NSString *authCode = nil;
                                               
                                               if (result.length>0) {
                                                   NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                                                   for (NSString *subResult in resultArr) {
                                                       if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                                                           authCode = [subResult substringFromIndex:10];
                                                           break;
                                                       }
                                                   }
                                               }
                                               NSLog(@"授权结果 authCode = %@", authCode?:@"");
                                               //weakSelf.ALIPAY =[NSString stringWithFormat:@"%@",authCode];
                                               [weakSelf zhifubaosucess:authCode];
                                           }];
    }
}

-(void)processAuth_V2Result:(NSURL*)resultUrl standbyCallback:(CompletionBlock)completionBlock{
    KMyLog(@"dsfsfsd%@",resultUrl);
}

//支付宝 绑定账户
-(void)zhifubaosucess:(NSString *)aucode{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"code"] = aucode;
    param[@"phone"] = self.model.phone;
    kWeakSelf;
    [NetWorkTool POST:aliPayAppLogin param:param success:^(id dic) {
        KMyLog(@"sfas超市%@",dic);
        [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
            
            [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
            
            if (!strIsEmpty(self->_model.wx_image_url)) {
                self.wechatGoshouquan.text = @"切换账号";
                self.weibangdingWechat.hidden = YES;
                self.wechatName.hidden = NO;
                self.wechatImage.hidden = NO;
                [self.wechatImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.wx_image_url]]];
                self.wechatName.text = [NSString stringWithFormat:@"%@",self.model.wx_name];
                
                self.wechaSwith.on = YES;
                self.alipaySwith.on = NO;
            }
            
            if (!strIsEmpty(self->_model.alipay_image_url)) {
                self.alipayGoshouquna.text = @"切换账号";
                self.weibandingAlipay.hidden = YES;
                self.alipayname.hidden = NO;
                self.alipayImage.hidden = NO;
                [self.alipayImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",weakSelf.model.alipay_image_url]]];
                self.alipayname.text = [NSString stringWithFormat:@"%@",weakSelf.model.alipay_name];
                
                self.alipaySwith.on = YES;
                self.wechaSwith.on = NO;
            }
            
            /*if (!strIsEmpty(self->_model.type)) {
                if ( [_model.type integerValue] == 1) {
                    self.wechaSwith.on = YES;
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }else if ([self->_model.type integerValue] == 2){
                    self.alipaySwith.on = YES;
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
            }else{
                self.wechaSwith.on = NO;
                self.alipaySwith.on = NO;
            }*/
            
            //if (!strIsEmpty(self->_model.type)) {
            //if ([self->_model.type integerValue] == 1) {
            if ([self->_model.type isEqualToString:@"1"]) {
                self.wechaSwith.on = YES;
                self.alipaySwith.on = NO;
                
                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
                //} else if ([self->_model.type integerValue] == 2){
            } else if ([self->_model.type isEqualToString:@"2"]){
                self.alipaySwith.on = YES;
                self.wechaSwith.on = NO;
                
                if (self.model.alipay_name.length > 0 && self.model.wx_name.length > 0) {
                    [self.wechaSwith setEnabled:YES];
                    [self.alipaySwith setEnabled:YES];
                } else {
                    [self.wechaSwith setEnabled:NO];
                    [self.alipaySwith setEnabled:NO];
                }
                //}
            } else {
                self.wechaSwith.on = NO;
                self.alipaySwith.on = NO;
            }
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

@end
