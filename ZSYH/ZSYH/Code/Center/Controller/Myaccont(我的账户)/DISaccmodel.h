//
//  DISaccmodel.h
//  Distributor
//
//  Created by 主事丫环 on 2019/5/17.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DISaccmodel : BaseModel
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, assign) double modifyDate;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *alipay_image_url;
@property (nonatomic, copy) NSString *openid;
@property (nonatomic, copy) NSString *invitation_code;
@property (nonatomic, copy) NSString *type_id;
@property (nonatomic, copy) NSString *id_card;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *person;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *plan_id;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *valid_state;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *wx_image_url;
@property (nonatomic, copy) NSString *alipay_userid;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, assign) double createDate;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *parent_invitation_code;

@property (nonatomic, copy) NSString *alipay_name;
@property (nonatomic, copy) NSString *wx_name;


@end

NS_ASSUME_NONNULL_END
