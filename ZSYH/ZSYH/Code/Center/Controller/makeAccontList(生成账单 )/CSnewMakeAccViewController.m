//
//  CSnewMakeAccViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewMakeAccViewController.h"
#import "CSmakeAccontListcell.h"
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"
#import "SeleproductTypeViewController.h"
#import "CSchangeNewViewController.h"
#import "CSChangeTwoTableViewCell.h"
#import "HWScanViewController.h"

#import "CSPJFnewChangeTableViewCell.h"

#import "CSnewchangeTableViewCell.h"
@interface CSnewMakeAccViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong)NSMutableArray *mydateSource;

@property(nonatomic,strong)UIView *tabFootView;
@property(nonatomic,strong)UILabel *tatolLB;
@property(nonatomic,strong)UIView *bgViewsec;
@property(nonatomic,strong)UITableView *myTableView;

@property(nonatomic,strong)UITextField *mytf;
@property (strong, nonatomic)UILabel *zongfeiLB;

@property (strong, nonatomic)NSString *fuwufei;
@property (strong, nonatomic)NSString *peijianzongfei;

@property (copy, nonatomic)NSString *qrCodeStr;
@property (strong, nonatomic)NSMutableArray *qrCodeStrArr;
@end
@implementation CSnewMakeAccViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.qrCodeStrArr = [NSMutableArray array];

    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self.view addSubview:self.myTableView ];

    [self showtabFootView];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view from its nib.
}

/**
 此类 祖传代码 勿动
 */
-(void)showtabFootView{
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    self.tatolLB = [[UILabel alloc]initWithFrame:CGRectMake(0, 36, KWIDTH, 18)];
    self.tatolLB .font = FontSize(18);
    self.tatolLB.textAlignment = NSTextAlignmentCenter;
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥0.00"];
    [self.tabFootView addSubview: self.tatolLB ];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 68, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _myTableView.tableFooterView = self.tabFootView;
}



-(void)tuisongAction:(UIButton *)but{
    //推送账单
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (strIsEmpty(_mytf.text)) {
        ShowToastWithText(@"请输入工时费");
        return;
    }
    
    param[@"peopleCost"] = _mytf.text;
    param[@"orderId"] =_orderNumberStr;
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *oldmuarr = [NSMutableArray arrayWithCapacity:1];
    
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];
        
        if ([model.typeProduct integerValue] == 2) {
            //设计更换配件 暂不操作
            
            NSMutableDictionary *oldmuDic = [NSMutableDictionary dictionaryWithCapacity:1];
            if (strIsEmpty(model.ne_name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.ne_type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            if (strIsEmpty(model.old_name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.old_type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            
            if ([model.ne_qrCode isEqualToString:model.old_qrCode]) {
                ShowToastWithText(@"新旧配件不能扫描同一个二维码");
                return;
            }
            
            if (strIsEmpty(model.old_price)) {
                ShowToastWithText(@"配件费不能为空");
                return;
            }
            if ([model.old_price integerValue] == 0) {
                ShowToastWithText(@"配件费不能为0");
                return;
            }
            
            muDic[@"price"] = model.total_price;
            muDic[@"type_id"] = model.ne_type_id;
            muDic[@"count"] = @"1";
            muDic[@"name"] = model.ne_name;
            muDic[@"qrcode"] = model.ne_qrCode;
            muDic[@"product_model"] = @"0";
            [muarr addObject:muDic];
            
            oldmuDic[@"price"] = model.old_price;
            oldmuDic[@"type_id"] = model.old_type_id;
            oldmuDic[@"count"] = @"1";
            oldmuDic[@"name"] = model.old_name;
            oldmuDic[@"qrcode"] = model.old_qrCode;
            [oldmuarr addObject:oldmuDic];
            
            
            
        }else{
            if (strIsEmpty(model.name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            if (strIsEmpty(model.price)) {
                ShowToastWithText(@"配件费不能为空");
                return;
            }
            if ([model.price integerValue] == 0) {
                ShowToastWithText(@"配件费不能为0");
                return;
            }
            muDic[@"price"] = model.price;
            muDic[@"type_id"] = model.type_id;
            muDic[@"count"] = model.count;
            muDic[@"name"] = model.name;
            muDic[@"productId"] = model.type_id;
            muDic[@"qrcode"] = model.qrCode;
            muDic[@"product_model"] = @"1";
            
            
            [muarr addObject:muDic];
            
        }
        
    }
    
    param[@"products"] =muarr;
    param[@"up_products"] =oldmuarr;
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] =json;
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSLog(@"%@", param);
    NSLog(@"%@", allDic);
    
    
    
    [NetWorkTool POST:addPeiJian param:allDic success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        weakSelf.bgViewsec.center = self.view.center;

        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
    
    
    
}




- (void)rightClick{
    //    HWScanViewController *vc = [[HWScanViewController alloc]init];
    //    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    //    [self presentViewController:na animated:YES completion:nil];
}
/**
 更换配件
 
 @param sender 更换配件
 */
- (IBAction)changeProductAction:(UIButton *)sender {
    _fuwufei = self.mytf.text;

    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.typeProduct = @"2";
    model.ne_count = @"1";
    model.old_count = @"1";
    [self.mydateSource addObject:model];
    
    [self.myTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    

    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 164;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 164)];
    
    [self.view addSubview:headerView];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *dingdan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 55)];
    dingdan.font = FontSize(16);
    dingdan.textColor = K666666;
    dingdan.text = @"订单编号:";
    [headerView addSubview:dingdan];
    
    UILabel *dingdanlb = [[UILabel alloc]initWithFrame:CGRectMake(99, 0, KWIDTH-99, 55)];
    dingdanlb.font = FontSize(16);
    dingdanlb.textColor = K666666;
    dingdanlb.text =  self.newid;
    [headerView addSubview:dingdanlb];
    
    UILabel *bglable1 = [[UILabel alloc]initWithFrame:CGRectMake(0, dingdan.bottom, KWIDTH, 10)];
    bglable1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [headerView addSubview:bglable1];

    UILabel *fuwu = [[UILabel alloc]initWithFrame:CGRectMake(16, bglable1.bottom, 83, 48)];
    fuwu.font = FontSize(16);
    fuwu.textColor =[UIColor colorWithHexString:@"#111111"];
    fuwu.text = @"工时费:";
    [headerView addSubview:fuwu];
    
    
    UILabel *fuwu1 = [[UILabel alloc]initWithFrame:CGRectMake(fuwu.right, bglable1.bottom, 12, 48)];
    fuwu1.font = FontSize(16);
    fuwu1.textColor = K333333;
    fuwu1.text = @"¥";
    [headerView addSubview:fuwu1];
    UITextField  *mytf33 = [[UITextField alloc]initWithFrame:CGRectMake(fuwu1.right, bglable1.bottom, KWIDTH-95, 48)];
    mytf33.keyboardType = UIKeyboardTypeNumberPad;
    [mytf33 addRules];
    mytf33.delegate = self;
    mytf33.textColor = K333333;
    mytf33.placeholder = @"请输入工时费";
    mytf33.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:mytf33];
    self.mytf = mytf33;

    if (!strIsEmpty(_fuwufei)) {
        self.mytf.text = _fuwufei;
    }
    UILabel *bglable2 = [[UILabel alloc]initWithFrame:CGRectMake(0, fuwu1.bottom, KWIDTH, 2)];
    bglable2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [headerView addSubview:bglable2];
    
    
    UILabel *peojian = [[UILabel alloc]initWithFrame:CGRectMake(16, bglable2.bottom, 83, 48)];
    peojian.font = FontSize(16);
    peojian.textColor =[UIColor colorWithHexString:@"#111111"];
    peojian.text = @"配件总费:";
    [headerView addSubview:peojian];
    
    UILabel *peojian1 = [[UILabel alloc]initWithFrame:CGRectMake(peojian.right, bglable2.bottom, 12, 48)];
    peojian1.font = FontSize(16);
    peojian1.textColor = K333333;
    peojian1.text = @"¥";
    //[headerView addSubview:peojian1];
    
    UILabel *peojian2 = [[UILabel alloc]initWithFrame:CGRectMake(peojian.right, bglable2.bottom, KWIDTH - 214-99-12+20, 48)];
    peojian2.font = FontSize(16);
    peojian2.textColor = K333333;
    peojian2.text = @"¥0";
    [headerView addSubview:peojian2];
    self.zongfeiLB =peojian2;
    if (!strIsEmpty(self.peijianzongfei)) {
        self.zongfeiLB.text = self.peijianzongfei;
    }
    
    UIButton *addpic = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [addpic setImage:imgname(@"Caddbut") forState:(UIControlStateNormal)];
    addpic.frame = CGRectMake(KWIDTH-20-14, bglable2.bottom+14, 20, 20);
    [headerView addSubview:addpic];
    [addpic addTarget:self action:@selector(addpejjian:) forControlEvents:(UIControlEventTouchUpInside)];
    UIButton *addbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [addbut setTitle:@"添加配件" forState:(UIControlStateNormal)];
    [addbut setTitleColor:[UIColor colorWithHexString:@"#707070"] forState:(UIControlStateNormal)];
    addbut.titleLabel.font = FontSize(14);
    addbut.frame = CGRectMake(KWIDTH-20-14-62, bglable2.bottom+14, 62, 20);
    [headerView addSubview:addbut];
    [addbut addTarget:self action:@selector(addpejjian:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    UIButton *changepic = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [changepic setImage:imgname(@"Caddbut") forState:(UIControlStateNormal)];
    changepic.frame = CGRectMake(KWIDTH-122, bglable2.bottom+14, 20, 20);
    [headerView addSubview:changepic];
    [changepic addTarget:self action:@selector(changePeijian:) forControlEvents:(UIControlEventTouchUpInside)];
    UIButton *changePeijianbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [changePeijianbut setTitle:@"更换配件" forState:(UIControlStateNormal)];
    [changePeijianbut setTitleColor:[UIColor colorWithHexString:@"#707070"] forState:(UIControlStateNormal)];
    changePeijianbut.titleLabel.font = FontSize(14);
    changePeijianbut.frame = CGRectMake(KWIDTH-122-62, bglable2.bottom+14, 62, 20);
    [headerView addSubview:changePeijianbut];
    [changePeijianbut addTarget:self action:@selector(changePeijian:) forControlEvents:(UIControlEventTouchUpInside)];
    return headerView;
}

-(void)changePeijian:(UIButton *)but{
    _fuwufei = self.mytf.text;

    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    
    model.typeProduct = @"2";
    model.ne_count = @"1";
    model.old_count = @"1";
    [self.mydateSource addObject:model];
    [self.myTableView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

-(void)addpejjian:(UIButton *)but{
    _fuwufei = self.mytf.text;

    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.isSaoyisao = @"1";
    model.count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.myTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([model.typeProduct integerValue] == 2) {
        return 461;
    }else{
        return 240;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];

    model.row = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    if ([model.typeProduct integerValue] == 2) {
        CSPJFnewChangeTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:@"CSPJFnewChangeTableViewCell" forIndexPath:indexPath];
        mycell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (!strIsEmpty(model.old_type_name)) {
            [mycell.oldBut setTitleColor:K333333 forState:(UIControlStateNormal)];
            [mycell.oldBut setTitle:model.old_type_name forState:(UIControlStateNormal)];
        }else{
            [mycell.oldBut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
            [mycell.oldBut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        if (!strIsEmpty(model.ne_type_name)) {
            [mycell.nebut setTitleColor:K333333 forState:(UIControlStateNormal)];
            [mycell.nebut setTitle:model.ne_type_name forState:(UIControlStateNormal)];
        }else{
            [mycell.nebut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
            [mycell.nebut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        
        mycell.old_nameTF.text =model.old_name;
        mycell.ne_nameTF.text = model.ne_name;
        mycell.PeiJianFeiTF.text = model.total_price;
        mycell.rowLB.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        //    mycell.old_nameTF.userInteractionEnabled = YES;
        //    mycell.oldBut.userInteractionEnabled = YES;
        //    mycell.ne_nameTF.userInteractionEnabled = YES;
        //    mycell.nebut.userInteractionEnabled = YES;
        
        if ([model.old_isSaoyisao integerValue] == 1) {
            mycell.old_nameTF.userInteractionEnabled = NO;
            mycell.oldBut.userInteractionEnabled = NO;
        }else {
            mycell.old_nameTF.userInteractionEnabled = YES;
            mycell.oldBut.userInteractionEnabled = YES;
        }
        
        if ([model.ne_isSaoyisao integerValue] == 1){
            mycell.ne_nameTF.userInteractionEnabled = NO;
            mycell.nebut.userInteractionEnabled = NO;
        }else {
            mycell.ne_nameTF.userInteractionEnabled = YES;
            mycell.nebut.userInteractionEnabled = YES;
        }
        
        kWeakSelf;
        mycell.myblock = ^(NSUInteger ind,NSString *str) {
            
            switch (ind) {
                case 0:
                {//上边的扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row isup:1];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 1:
                {//上边的选择品类
                    
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.old_type_name = str;
                        model.old_type_id = idstr;
                        [weakSelf.myTableView reloadData];
                        
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                    
                }
                    break;
                    
                case 2:
                {//上边的输入名称
                    model.old_name = str;
                    [weakSelf.myTableView reloadData];
                    
                    
                }
                    break;
                case 3:
                {//下边的扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row isup:0];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                    
                    
                }
                    break;
                case 4:
                {//上边的选择品类
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.ne_type_name= str;
                        model.ne_type_id = idstr;
                        [weakSelf.myTableView reloadData];
                        
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                    
                }
                    break;
                case 5:
                {//下边的输入名称
                    model.ne_name = str;
                    
                    [weakSelf.myTableView reloadData];
                    
                }
                    break;
                case 6:
                {///删除
                    
                    
                    [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                    [weakSelf.myTableView reloadData];
                    
                    
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.fuwufei = self.mytf.text;
                    
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    
                }
                    break;
                case 10:
                {///配件费
                    //                    CGFloat ffftotle = 0;
                    //                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                    //                            CGFloat motltlt = [model.total_price floatValue];
                    //                                    ffftotle = ffftotle +motltlt;
                    //                        }
                    //                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    //                    CGFloat sevrice = [self->_mytf.text floatValue];
                    //                    ffftotle = ffftotle +[self.mytf.text floatValue] +sevrice ;
                    //                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    //
                    model.old_price = str;
                    model.total_price = str;
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.fuwufei = self.mytf.text;
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    
                }
                    break;
                default:
                    break;
            }
            
        };
        
        
        return mycell;
    }else{
        
        
        
        CSmakeAccontListcell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmakeAccontListcell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell refashWithmodel:model];
        //self.myblock(0, @""); 0配件单价 1 配件名称2删除 3配件数量 4扫一扫 5选择配件种类 6 7
        kWeakSelf;
        cell.myblock = ^(NSInteger stute, NSString * _Nonnull coun) {
            switch (stute) {
                case 0:
                {//配件单价
                    
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.count floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];                CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.fuwufei = self.mytf.text;
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    cell.totalPrice.text =[NSString stringWithFormat:@"%@", model.total_price];
                    
                    //                model.isSaoyisao = @"1";
                    model.price = coun;
                }
                    break;
                case 1:
                {//配件名称
                    model.name = coun;
                    
                }
                    break;
                case 2:
                {//删除
                    [weakSelf.mydateSource removeObject:model];
                    [weakSelf.myTableView reloadData];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.fuwufei = self.mytf.text;
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 3:
                {//配件数量
                    model.count = coun;
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.price floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.fuwufei = self.mytf.text;
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 4:
                {//扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 5:
                {//选择配件种类
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.type_name = str;
                        model.type_id = idstr;
                        [weakSelf.myTableView reloadData];
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                }
                    break;
                default:
                    break;
            }
        };
        
        if ((indexPath.row+1) == _mydateSource.count) {
            //[self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
        return cell;
    }
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind isup:(NSInteger )index{
    
    NSLog(@"%@",self.qrCodeStrArr);
    NSLog(@"%@",strr);
    
    if (self.qrCodeStrArr.count == 0) {
        [self.qrCodeStrArr addObject:strr];
    }else {
        //NSMutableArray *tempArr = self.qrCodeStrArr;
        NSArray * tempArr = [NSArray arrayWithArray: self.qrCodeStrArr];
        for (NSString *str in tempArr) {
            if ([str isEqualToString:strr]) {
                ShowToastWithText(@"该配件二维码已被扫描");
                return;
            }else {
                [self.qrCodeStrArr addObject:strr];
            }
        }
    }
    
    NSLog(@"%@",self.qrCodeStrArr);
    NSLog(@"%@",strr);
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = weakSelf.mydateSource[ind];
        if (index == 1) {
            
            if ([[mudic objectForKey:@"qrcode"] isEqualToString:mymodel.ne_qrCode]) {
                ShowToastWithText(@"新旧配件不能扫描同一个二维码");
                return;
            }
            
            //旧配件
            mymodel.old_type_name =[mudic objectForKey:@"type_name"];
            mymodel.old_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.old_name =[mudic objectForKey:@"parts_name"];
            mymodel.old_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.old_isSaoyisao = @"1";
            
        }else{
            
            if ([[mudic objectForKey:@"qrcode"] isEqualToString:mymodel.old_qrCode]) {
                ShowToastWithText(@"新旧配件不能扫描同一个二维码");
                return;
            }
            
            //新配件
            mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
            mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.ne_name =[mudic objectForKey:@"parts_name"];
            mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.ne_isSaoyisao = @"1";
            
        }
        
        
        [self.myTableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}


- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind{
    
    NSLog(@"%@",self.qrCodeStrArr);
    NSLog(@"%@",strr);
    
    if (self.qrCodeStrArr.count == 0) {
        [self.qrCodeStrArr addObject:strr];
    }else {
//        NSMutableArray *tempArr = self.qrCodeStrArr;
        NSArray * tempArr = [NSArray arrayWithArray: self.qrCodeStrArr];
        for (NSString *str in tempArr) {
            if ([str isEqualToString:strr]) {
                ShowToastWithText(@"该配件二维码已被扫描");
                return;
            }else {
                [self.qrCodeStrArr addObject:strr];
            }
        }
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
        mymodel.qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.type_name =[mudic objectForKey:@"type_name"];
        mymodel.price =[mudic objectForKey:@"parts_price"];
        mymodel.price = @"0";
        mymodel.type_id =[mudic objectForKey:@"parts_type"];
        mymodel.Id =[mudic objectForKey:@"id"];
        
        //        mymodel.count =[mudic objectForKey:@"count"];
        mymodel.count  = @"1";
        mymodel.name =[mudic objectForKey:@"parts_name"];
        mymodel.isSaoyisao = @"0";
        
        [self->_mydateSource removeObjectAtIndex:ind];
        [self->_mydateSource insertObject:mymodel atIndex:ind];
        [self.myTableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    
    CGFloat ffftotle = 0;
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        CGFloat motltlt = [model.total_price floatValue];
        ffftotle = ffftotle +motltlt;
    }
    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
    self.peijianzongfei = [NSString stringWithFormat:@"¥%.2f",ffftotle];
    
    
    ffftotle = ffftotle +[self.mytf.text floatValue];
    self.fuwufei = self.mytf.text;
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
    
    return YES;
}
/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"推送成功";
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //    DowLable.text = @"明天继续努力";
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.mytf) {
        
        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
            ShowToastWithText(@"只能输入数字");
            return NO;
            
        }else{
            return YES;
            
        }
    }else{
        return YES;
    }
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 461;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
//        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
//            [_myTableView.mj_header beginRefreshing];
//        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSmakeAccontListcell" bundle:nil] forCellReuseIdentifier:@"CSmakeAccontListcell"];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSPJFnewChangeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSPJFnewChangeTableViewCell"];

        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}
@end

