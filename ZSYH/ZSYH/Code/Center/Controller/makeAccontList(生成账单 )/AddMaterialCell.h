//
//  AddMaterialCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddMaterialModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddMaterialCell : UITableViewCell

@property (strong, nonatomic) AddMaterialModel *model;
@property (weak, nonatomic) IBOutlet UILabel *rowL;
@property (weak, nonatomic) IBOutlet UIButton *closeB;
@property (weak, nonatomic) IBOutlet UIButton *chooseMaterialB;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *costTF;

@property(nonatomic,copy)void (^myblock)(NSInteger stute,NSString *coun);
- (void)refashWithmodel:(AddMaterialModel *)model;

@end

NS_ASSUME_NONNULL_END
