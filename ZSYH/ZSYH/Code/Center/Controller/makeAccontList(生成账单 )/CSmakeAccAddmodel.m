//
//  CSmakeAccAddmodel.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmakeAccAddmodel.h"

@implementation CSmakeAccAddmodel

- (NSMutableArray *)imageArray {
    if (!_imageArray) {
        _imageArray = [NSMutableArray arrayWithCapacity:1];
    }
    return _imageArray;
}
@end
