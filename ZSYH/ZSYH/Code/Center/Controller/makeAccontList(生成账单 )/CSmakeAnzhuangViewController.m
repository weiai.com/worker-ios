//
//  CSmakeAnzhuangViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmakeAnzhuangViewController.h"
#import "TanKuangNoseleview.h"

@interface CSmakeAnzhuangViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;
@property (weak, nonatomic) IBOutlet UITextField *mytf;
@property (weak, nonatomic) IBOutlet UILabel *tatolLB;
@property(nonatomic,strong)TanKuangNoseleview *tankiuiang;
@property (nonatomic,strong) UIView *bgViewsec;

@end

@implementation CSmakeAnzhuangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.title = @"生成账单";
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mytf.keyboardType = UIKeyboardTypeNumberPad;
    self.mytf.placeholder = @"请输入工时费";
    self.mytf.font = [UIFont systemFontOfSize:14];
    self.mytf.delegate = self;
    [self.mytf becomeFirstResponder];
    [self.mytf addRules];

//    self.orderNumber.text = _orderNumberStr;
    self.orderNumber.text = _neworderid;
    
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥0.00"];
    
    [self shwoBgviewsec];
    
//    kWeakSelf;
//    self.tankiuiang = [[TanKuangNoseleview alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
//    self.tankiuiang.titleLable.text = @"账单推送成功";
//    self.tankiuiang.msgLable.hidden = YES;
//
//    self.tankiuiang.myblcok = ^(NSInteger ind) {
//        
//        if (weakSelf.myblockj) {
//            weakSelf.myblockj(@"");
//        }
//        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
//        [weakSelf.navigationController popViewControllerAnimated:YES];
//    };
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)tuiAction:(UIButton *)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (strIsEmpty(_mytf.text)) {
        ShowToastWithText(@"请输入工时费");
        return;
    }
    if (_mytf.text.floatValue < 30) {
        ShowToastWithText(@"当前费用合计小于30元");
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    param[@"peopleCost"] = _mytf.text;
    param[@"orderId"] =_orderNumberStr;
  
//    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
//    param[@"products"] =muarr;
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] =json;
    kWeakSelf;
    [NetWorkTool POST:addPeiJian param:allDic success:^(id dic) {
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];

        [MBProgressHUD hideHUDForView:self.view animated:YES];


    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",[textField.text floatValue]];
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//    if (textField == self.mytf) {
//
//        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
//        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
//            ShowToastWithText(@"只能输入数字");
//            return NO;
//        }else{
//            return YES;
//        }
//    }else{
//        return YES;
//    }
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == self.mytf) {
        if(string.length == 0)
            return YES;
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if(existedLength - selectedLength + replaceLength > 5) {
            return NO;
        }
    }
    return YES;
}

/**
 弹出框的背景图 推送成功
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*为保障您的收益\n请您确认用户支付费用后离开" originalColor:K333333 originalFont:FontSize(16) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(16)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.numberOfLines = 0;
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.attributedText = attStr;
    DowLable.textAlignment =  NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikenow:(UIButton *)but{
    [self.bgViewsec removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
    [singlTool shareSingTool].needReasfh = YES;
    //    for (UIViewController *controller in self.navigationController.viewControllers) {
    //        if ([controller isKindOfClass:[CSOrderContentViewController class]]) {
    //            CSOrderContentViewController *vc = (CSOrderContentViewController *)controller;
    //            [self.navigationController popToViewController:vc animated:YES];
    //        }
    //    }
    
    //        if (self.myblock) {
    //            self.myblock(@"");
    //        }
    //        [self.navigationController popViewControllerAnimated:YES];
    
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


@end
