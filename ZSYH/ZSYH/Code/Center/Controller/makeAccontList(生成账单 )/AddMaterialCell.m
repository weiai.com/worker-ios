//
//  AddMaterialCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "AddMaterialCell.h"
#import "SeleproductTypeViewController.h"

@interface AddMaterialCell ()<UITextFieldDelegate>

@property(nonatomic,strong)UIView *bgViewsec;

@end

@implementation AddMaterialCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rowL.layer.cornerRadius = 9;
    self.rowL.layer.masksToBounds = YES;
    self.rowL.layer.borderColor = zhutiColor.CGColor;
    self.rowL.layer.borderWidth = 1;
    
    self.costTF.delegate = self;
    [self.costTF addRules];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)refashWithmodel:(AddMaterialModel *)model {
    
    self.model = model;
    self.rowL.text = model.row;
    self.nameTF.text = model.type_name;
    self.costTF.text = model.cost;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (self.myblock) {
        self.myblock(0, @"");
    }
    return YES;
}

- (BOOL)textFieldDidEndEditing:(UITextField *)textField{
    if ([self.costTF.text hasPrefix:@"0"]) {
        ShowToastWithText(@"价格不能是0开头");
        textField.text = @"";
    }
    if (self.myblock) {
        self.myblock(0, textField.text);
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
        ShowToastWithText(@"只能输入数字");
        return NO;
        
    }else{
        return YES;
        
    }
    
}


- (IBAction)close:(id)sender {
    if (self.myblock) {
        self.myblock(2, @"");
    }
}

- (IBAction)chooseMaterial:(id)sender {
    if (self.myblock) {
        self.myblock(5, @"");
    }
}

@end
