//
//  CSchangeNewViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSchangeNewViewController : BaseViewController
@property(nonatomic,copy)void (^myblock)(CSmakeAccAddmodel *model);
@end

NS_ASSUME_NONNULL_END
