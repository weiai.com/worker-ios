//
//  AddMaterialModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/9.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddMaterialModel : BaseModel

@property(nonatomic,copy)NSString *row;
@property(nonatomic,copy)NSString *cost;
@property(nonatomic,copy)NSString *type_name;
@property(nonatomic,copy)NSString *type_id;

@end

NS_ASSUME_NONNULL_END
