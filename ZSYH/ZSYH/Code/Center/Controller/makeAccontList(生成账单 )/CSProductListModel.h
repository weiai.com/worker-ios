//
//  CSProductListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSProductListModel : BaseModel
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, strong) id updateTime;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, copy) NSString *typeLevel;
@property (nonatomic, copy) NSString *typeCode;
@property (nonatomic, strong) id createTime;
@property (nonatomic, copy) NSString *parentCode;
@property (nonatomic, copy) NSString *isele;



@end

NS_ASSUME_NONNULL_END
