//
//  CSmakeAccLostViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmakeAccLostViewController.h"
#import "CSmakeAccontListcell.h"
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"
#import "SeleproductTypeViewController.h"
#import "CSchangeNewViewController.h"
#import "CSChangeTwoTableViewCell.h"
#import "HWScanViewController.h"
#import "CSPJFnewChangeTableViewCell.h"
#import "CSnewchangeTableViewCell.h"

@interface CSmakeAccLostViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UIView *tabFootView;
@property(nonatomic,strong)UILabel *tatolLB;
@property (weak, nonatomic) IBOutlet UITextField *mytf;
@property (weak, nonatomic) IBOutlet UILabel *zongfeiLB;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLB;
@property(nonatomic,strong)UIView *bgViewsec;

@end

@implementation CSmakeAccLostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    //[self.rightbutton setImage:imgname(@"Csaoyosao") forState:(UIControlStateNormal)];
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    [_mytableView registerNib:[UINib nibWithNibName:@"CSmakeAccontListcell" bundle:nil] forCellReuseIdentifier:@"CSmakeAccontListcell"];
    
    [_mytableView registerNib:[UINib nibWithNibName:@"CSPJFnewChangeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSPJFnewChangeTableViewCell"];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    self.mytf.keyboardType = UIKeyboardTypeNumberPad;
    self.orderNumberLB.text = self.newid;
    [self.mytf addRules];
    
    self.mytf.delegate = self;
    [self showtabFootView];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view from its nib.
}

-(void)showtabFootView{
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    self.tatolLB = [[UILabel alloc]initWithFrame:CGRectMake(0, 36, KWIDTH, 18)];
    self.tatolLB .font = FontSize(18);
    self.tatolLB.textAlignment = NSTextAlignmentCenter;
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥0.00"];
    [self.tabFootView addSubview: self.tatolLB ];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 68, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _mytableView.tableFooterView = self.tabFootView;
}

-(void)tuisongAction:(UIButton *)but{
    //推送账单
    // 订单状态(0生成订单（已接单）//生成检测报告
    // ，1已检测，//生成账单
    //2生成账单（未支付），//去支付
    // 3已支付（已完成），//去评价
    //4已评价，
    //  5已取消)',//先不管
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (strIsEmpty(_mytf.text)) {
        ShowToastWithText(@"请输入工时费");
        return;
    }
    
    param[@"peopleCost"] = _mytf.text;
    param[@"orderId"] =_orderNumberStr;
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *oldmuarr = [NSMutableArray arrayWithCapacity:1];
    
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];
        
        if ([model.typeProduct integerValue] == 2) {
            //设计更换配件 暂不操作
            
            NSMutableDictionary *oldmuDic = [NSMutableDictionary dictionaryWithCapacity:1];
            if (strIsEmpty(model.ne_name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.ne_type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            if (strIsEmpty(model.old_name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.old_type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            
            muDic[@"price"] = model.total_price;
            if (strIsEmpty( model.old_price)) {
                ShowToastWithText(@"配件费不能为空");
                return;
            }
            if ([model.old_price integerValue] == 0) {
                ShowToastWithText(@"配件费不能为0");
                return;
            }
            muDic[@"type_id"] = model.ne_type_id;
            muDic[@"count"] = @"1";
            muDic[@"name"] = model.ne_name;
            muDic[@"qrcode"] = model.qrCode;
            muDic[@"product_model"] = @"0";
            
            [muarr addObject:muDic];
            
            oldmuDic[@"price"] = model.old_price;
            oldmuDic[@"type_id"] = model.old_type_id;
            oldmuDic[@"count"] = @"1";
            oldmuDic[@"name"] = model.old_name;
            oldmuDic[@"qrcode"] = model.old_qrCode;
            [oldmuarr addObject:oldmuDic];
        } else {
            if (strIsEmpty(model.name)) {
                ShowToastWithText(@"配件名字不能为空");
                return;
            }
            if (strIsEmpty(model.type_id)) {
                ShowToastWithText(@"配件类型不能为空");
                return;
            }
            if (strIsEmpty(model.price)) {
                ShowToastWithText(@"配件费不能为空");
                return;
            }
            if ([model.price integerValue] == 0) {
                ShowToastWithText(@"配件费不能为0");
                return;
            }
            muDic[@"price"] = model.price;
            muDic[@"type_id"] = model.type_id;
            muDic[@"count"] = model.count;
            muDic[@"name"] = model.name;
            muDic[@"productId"] = model.type_id;
            muDic[@"qrcode"] = model.qrCode;
            muDic[@"product_model"] = @"1";
            
            [muarr addObject:muDic];
        }
    }
    
    param[@"products"] =muarr;
    param[@"up_products"] =oldmuarr;
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] = json;
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool POST:addPeiJian param:allDic success:^(id dic) {
        //ShowToastWithText(@"生成成功");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //[self.view addSubview:weakSelf.bgViewsec];
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        weakSelf.bgViewsec.center = self.view.center;
        
        //NSMutableDictionary *muarr = [NSMutableDictionary dictionaryWithCapacity:1];
        //muarr[@"repOrderId"] = weakSelf.orderNumberStr;
        //[NetWorkTool POST:tuisongzhanbgd param:muarr success:^(id dic) {
        //    ShowToastWithText(@"生成成功");
        //
        //    if (self.mybleoc) {
        //        self.mybleoc(@"");
        //    }
        //    [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        ////    [self.navigationController popViewControllerAnimated:YES];
        //
        ////    [MBProgressHUD hideHUDForView:self.view animated:YES];
        //
        //} other:^(id dic) {
        //
        //} fail:^(NSError *error) {
        //
        //} needUser:YES];
        //[self.navigationController popViewControllerAnimated:YES];
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

/**
 更换配件
 
 @param sender 更换配件
 */
- (IBAction)changeProductAction:(UIButton *)sender {
    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    
    CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
    //    mymodel.qrCode =@"";
    //    mymodel.type_name =@"";
    //    mymodel.price =@"";
    //    mymodel.price = @"0";
    //    mymodel.type_id =@"";
    //    mymodel.Id =@"";
    //    mymodel.typeProduct = @"2";
    //    mymodel.total_price = @"";
    //    mymodel.ne_name =@"";
    //    mymodel.old_name =@"";
    //    mymodel.isSaoyisao =@"2";
    //
    //    mymodel.type_name =@"";
    //
    //    //mymodel.count =[mudic objectForKey:@"count"];
    //    mymodel.count  = @"";
    //    mymodel.name = @"";
    //    mymodel.old_type_name = @"";
    //    mymodel.ne_type_name = @"";
    
    mymodel.row = @"";    mymodel.type_name = @"";    mymodel.type_id = @"";    mymodel.name = @"";
    mymodel.price = @"";    mymodel.count = @"1";    mymodel.total_price = @"";    mymodel.qrCode = @"";
    mymodel.isSaoyisao = @"2";    mymodel.typeProduct = @"2";    mymodel.old_type_name = @"";    mymodel.old_type_id = @"";
    mymodel.old_name = @"";     mymodel.old_price = @"1";    mymodel.old_count = @"1";    mymodel.old_total_price = @"";
    
    mymodel.old_isSaoyisao = @"2";    mymodel.ne_type_name = @"";    mymodel.ne_type_id = @"";    mymodel.ne_name = @"";
    mymodel.ne_price = @"2";    mymodel.ne_count = @"";    mymodel.ne_total_price = @"";    mymodel.ne_qrCode = @"";mymodel.ne_isSaoyisao = @"2";
    
    model.typeProduct = @"2";
    model.ne_count = @"1";
    model.old_count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    
    //    CSchangeNewViewController *changeVc = [[CSchangeNewViewController alloc]init];
    //    kWeakSelf;
    //    changeVc.myblock = ^(CSmakeAccAddmodel * _Nonnull model) {
    //        if (model) {
    //            model.total_price = @"";
    //            [self.mydateSource addObject:model];
    //            CGFloat ffftotle = 0;
    //            for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
    //                CGFloat motltlt = [model.total_price floatValue];
    //                ffftotle = ffftotle +motltlt;
    //            }
    //            self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
    //
    //            CGFloat sevrice = [self->_mytf.text floatValue];
    //            ffftotle = ffftotle +[self.mytf.text floatValue] +sevrice ;
    //
    //            self.tatolLB.text = [NSString stringWithFormat:@"合计%.2f",ffftotle];
    //            [self.mytableView reloadData];
    //        }
    //    };
    //    [self.navigationController pushViewController:changeVc animated:YES];
}
/**
 
 @param sender 添加配件
 */
- (IBAction)AddProductAction:(UIButton *)sender {
    //    @property(nonatomic,strong)NSString *row;
    //    @property(nonatomic,strong)NSString *product_category;
    //    @property(nonatomic,strong)NSString *product_categoryid;
    //    @property(nonatomic,strong)NSString *product_name;
    //    @property(nonatomic,strong)NSString *product_price;
    //    @property(nonatomic,strong)NSString *product_number;
    //    @property(nonatomic,strong)NSString *total_price;
    //
    //    @property(nonatomic,strong)NSString *isSaoyisao;//0是扫一扫的 不展示加减 // 1是正常手输的 // 2是更换配件
    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.isSaoyisao = @"1";
    model.count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([model.typeProduct integerValue] == 2) {
        return 461;
    }else{
        return 240;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    model.row = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    if ([model.typeProduct integerValue] == 2) {
        CSPJFnewChangeTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:@"CSPJFnewChangeTableViewCell" forIndexPath:indexPath];
        mycell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (!strIsEmpty(model.old_type_name)) {
            [mycell.oldBut setTitleColor:K333333 forState:(UIControlStateNormal)];
            [mycell.oldBut setTitle:model.old_type_name forState:(UIControlStateNormal)];
        }else{
            [mycell.oldBut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
            [mycell.oldBut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        if (!strIsEmpty(model.ne_type_name)) {
            [mycell.nebut setTitleColor:K333333 forState:(UIControlStateNormal)];
            [mycell.nebut setTitle:model.ne_type_name forState:(UIControlStateNormal)];
        }else{
            [mycell.nebut setTitleColor:[UIColor colorWithHexString:@"#B7B7B7"] forState:(UIControlStateNormal)];
            [mycell.nebut setTitle:@"请选择配件品类" forState:(UIControlStateNormal)];
        }
        
        mycell.old_nameTF.text = model.old_name;
        mycell.ne_nameTF.text = model.ne_name;
        mycell.PeiJianFeiTF.text = model.total_price;
        mycell.rowLB.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
        //    mycell.old_nameTF.userInteractionEnabled = YES;
        //    mycell.oldBut.userInteractionEnabled = YES;
        //    mycell.ne_nameTF.userInteractionEnabled = YES;
        //    mycell.nebut.userInteractionEnabled = YES;
        
        if ([model.old_isSaoyisao integerValue] == 1) {
            mycell.old_nameTF.userInteractionEnabled = NO;
            mycell.oldBut.userInteractionEnabled = NO;
        }
        if ([model.ne_isSaoyisao integerValue] == 1){
            mycell.ne_nameTF.userInteractionEnabled = NO;
            mycell.nebut.userInteractionEnabled = NO;
        }
        kWeakSelf;
        mycell.myblock = ^(NSUInteger ind,NSString *str) {
            
            switch (ind) {
                case 0:
                {//上边的扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row isup:1];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 1:
                {//上边的选择品类
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.old_type_name = str;
                        model.old_type_id = idstr;
                        [weakSelf.mytableView reloadData];
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                }
                    break;
                    
                case 2:
                {//上边的输入名称
                    model.old_name = str;
                    [weakSelf.mytableView reloadData];
                }
                    break;
                case 3:
                {//下边的扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row isup:0];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 4:
                {//上边的选择品类
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.ne_type_name= str;
                        model.ne_type_id = idstr;
                        [weakSelf.mytableView reloadData];
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                }
                    break;
                case 5:
                {//下边的输入名称
                    model.ne_name = str;
                    [weakSelf.mytableView reloadData];
                }
                    break;
                case 6:
                {///删除
                    [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                    [weakSelf.mytableView reloadData];
                    
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 10:
                {///配件费
                    //                    CGFloat ffftotle = 0;
                    //                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                    //                            CGFloat motltlt = [model.total_price floatValue];
                    //                                    ffftotle = ffftotle +motltlt;
                    //                        }
                    //                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    //                    CGFloat sevrice = [self->_mytf.text floatValue];
                    //                    ffftotle = ffftotle +[self.mytf.text floatValue] +sevrice ;
                    //                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    //
                    model.old_price = str;
                    model.total_price = str;
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                default:
                    break;
            }
        };
        return mycell;
    }else{
        
        CSmakeAccontListcell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmakeAccontListcell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell refashWithmodel:model];
        //self.myblock(0, @""); 0配件单价 1 配件名称2删除 3配件数量 4扫一扫 5选择配件种类 6 7
        kWeakSelf;
        cell.myblock = ^(NSInteger stute, NSString * _Nonnull coun) {
            switch (stute) {
                case 0:
                {//配件单价
                    
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.count floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];                CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                    cell.totalPrice.text =[NSString stringWithFormat:@"%@", model.total_price];
                    
                    //                model.isSaoyisao = @"1";
                    model.price = coun;
                }
                    break;
                case 1:
                {//配件名称
                    model.name = coun;
                }
                    break;
                case 2:
                {//删除
                    [weakSelf.mydateSource removeObject:model];
                    [weakSelf.mytableView reloadData];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in self.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 3:
                {//配件数量
                    model.count = coun;
                    NSInteger ind = [coun integerValue];
                    CGFloat fff = [model.price floatValue] * ind;
                    model.total_price =[NSString stringWithFormat:@"%.2f",fff];
                    CGFloat ffftotle = 0;
                    for (CSmakeAccAddmodel *model in weakSelf.mydateSource) {
                        CGFloat motltlt = [model.total_price floatValue];
                        ffftotle = ffftotle +motltlt;
                    }
                    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
                    
                    ffftotle = ffftotle +[self.mytf.text floatValue];
                    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
                }
                    break;
                case 4:
                {//扫一扫
                    HWScanViewController *vc = [[HWScanViewController alloc]init];
                    vc.myblock = ^(NSString *str) {
                        [self requesrtWith:str withind:indexPath.row];
                    };
                    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                    [self presentViewController:na animated:YES completion:nil];
                }
                    break;
                case 5:
                {//选择配件种类
                    SeleproductTypeViewController *seleVc = [[SeleproductTypeViewController alloc]init];
                    seleVc.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
                        model.type_name = str;
                        model.type_id = idstr;
                        [weakSelf.mytableView reloadData];
                    };
                    [self.navigationController pushViewController:seleVc animated:YES];
                }
                    break;
                default:
                    break;
            }
        };
        if ((indexPath.row+1) == _mydateSource.count) {
            [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
        return cell;
    }
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind isup:(NSInteger )index{
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = weakSelf.mydateSource[ind];
        if (index == 1) {
            //旧配件
            mymodel.old_type_name =[mudic objectForKey:@"type_name"];
            mymodel.old_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.old_name =[mudic objectForKey:@"parts_name"];
            mymodel.old_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.old_isSaoyisao = @"1";
            
        }else{
            //新配件
            mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
            mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.ne_name =[mudic objectForKey:@"parts_name"];
            mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.ne_isSaoyisao = @"1";
        }
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind{

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
        mymodel.qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.type_name =[mudic objectForKey:@"type_name"];
        mymodel.price =[mudic objectForKey:@"parts_price"];
        mymodel.price = @"0";
        mymodel.type_id =[mudic objectForKey:@"parts_type"];
        mymodel.Id =[mudic objectForKey:@"id"];
        
        mymodel.count  = @"1";
        mymodel.name =[mudic objectForKey:@"parts_name"];
        mymodel.isSaoyisao = @"0";
        
        [self->_mydateSource removeObjectAtIndex:ind];
        [self->_mydateSource insertObject:mymodel atIndex:ind];
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    CGFloat ffftotle = 0;
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        CGFloat motltlt = [model.total_price floatValue];
        ffftotle = ffftotle +motltlt;
    }
    self.zongfeiLB.text = [NSString stringWithFormat:@"¥%.2f",ffftotle];
    ffftotle = ffftotle +[self.mytf.text floatValue];
    self.tatolLB.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
    
    return YES;
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"推送成功";
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //    DowLable.text = @"明天继续努力";
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    
    //    if (self.mybleoc) {
    //        self.mybleoc(@"");
    //    }
    //    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.mytf) {
        
        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
            ShowToastWithText(@"只能输入数字");
            return NO;
            
        }else{
            return YES;
        }
    }else{
        return YES;
    }
}

@end
