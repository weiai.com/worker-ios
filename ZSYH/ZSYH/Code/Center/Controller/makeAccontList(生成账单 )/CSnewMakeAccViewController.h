//
//  CSnewMakeAccViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewMakeAccViewController : BaseViewController
@property(nonatomic,strong)NSString *orderNumberStr;
@property(nonatomic,copy)void (^mybleoc)(NSString *str);

@property(nonatomic,strong)NSString *newid;
@end

NS_ASSUME_NONNULL_END
