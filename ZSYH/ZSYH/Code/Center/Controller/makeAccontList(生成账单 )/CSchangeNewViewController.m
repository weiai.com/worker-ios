
//
//  CSchangeNewViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSchangeNewViewController.h"
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"

@interface CSchangeNewViewController ()
@property(nonatomic,strong)UIView *oldBgView;
@property(nonatomic,strong)UILabel *oldtitleLB;
@property(nonatomic,strong)UILabel *oldcateGoryLB;
@property(nonatomic,strong)UILabel *oldpruNameLB;
@property(nonatomic,strong)UILabel *oldprupriceLB;
@property(nonatomic,strong)UILabel *oldpruTotalpriceLB;

@property(nonatomic,strong)UIView *newgView;
@property(nonatomic,strong)UILabel *newtitleLB;

@property(nonatomic,strong)UILabel *newcateGoryLB;
@property(nonatomic,strong)UILabel *newpruNameLB;
@property(nonatomic,strong)UILabel *newprupriceLB;
@property(nonatomic,strong)UILabel *newpruTotalpriceLB;

@property(nonatomic,strong)CSmakeAccAddmodel *mymodel;

@end

@implementation CSchangeNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"更换配件";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    // Do any additional setup after loading the view from its nib.
    self.mymodel = [[CSmakeAccAddmodel alloc]init];
    [self showUI];
}
-(void)showUI{
    
    self.oldBgView = [[UIView alloc]initWithFrame:CGRectMake(10, kNaviHeight+10, KWIDTH-20, 46)];
    [self.view addSubview:_oldBgView];
    _oldBgView.layer.cornerRadius = 8;
    _oldBgView.layer.masksToBounds = YES;
    self.oldBgView.backgroundColor = [UIColor whiteColor];
    
    CGFloat ssss = (KWIDTH-106)/2;
    
    UIButton *oldsaoyiso = [UIButton buttonWithType:(UIButtonTypeCustom)];
    oldsaoyiso.frame = CGRectMake(ssss+106-20, 16, 20, 18);
    [oldsaoyiso setImage:imgname(@"saoyisao") forState:(UIControlStateNormal)];
    [self.oldBgView addSubview:oldsaoyiso];
    [oldsaoyiso addTarget:self action:@selector(oldAcrtion:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    self.oldtitleLB = [[UILabel alloc]init];
    self.oldtitleLB.text = @"扫码添加平台旧配件";
    self.oldtitleLB.textAlignment = NSTextAlignmentRight;
    self.oldtitleLB.font = FontSize(16);
    self.oldtitleLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [self.oldBgView addSubview:self.oldtitleLB];
    self.oldtitleLB.frame = CGRectMake(20, 16, ssss+106-20-8-20, 16);

    
    
    self.newgView = [[UIView alloc]initWithFrame:CGRectMake(10, self.oldBgView.bottom+10, KWIDTH-20, 46)];
    [self.view addSubview:_newgView];
    _newgView.layer.cornerRadius = 8;
    _newgView.layer.masksToBounds = YES;
    self.newgView.backgroundColor = [UIColor whiteColor];
    
    UIButton *newsaoyiso = [UIButton buttonWithType:(UIButtonTypeCustom)];
    newsaoyiso.frame = CGRectMake(ssss+106-20, 16, 20, 18);
    [newsaoyiso setImage:imgname(@"saoyisao") forState:(UIControlStateNormal)];
    [_newgView addSubview:newsaoyiso];
    [newsaoyiso addTarget:self action:@selector(newsaoyiso:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    self.newtitleLB = [[UILabel alloc]init];
    self.newtitleLB.text = @"扫码添加平台新配件";
    self.newtitleLB.textAlignment = NSTextAlignmentRight;
    self.newtitleLB.font = FontSize(16);
    self.newtitleLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [_newgView addSubview:self.newtitleLB];
    self.newtitleLB.frame = CGRectMake(20, 16, ssss+106-20-8-20, 16);
    
    
    
    
    [self.rightbutton setTitle:@"完成" forState:(UIControlStateNormal)];
}
-(void)rightClick{
    
    if (strIsEmpty(_oldcateGoryLB.text)  || strIsEmpty(_newcateGoryLB.text)) {
        ShowToastWithText(@"您没有完善信息,添加失败");
        [self.navigationController popViewControllerAnimated:YES];

    }else{
        if (self.myblock) {
            self.myblock(_mymodel);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)oldAcrtion:(UIButton *)but{
    
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        [self requesrtWith:str];
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
    
    

}
- (void)requesrtWith:(NSString *)strr {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        self.mymodel.old_qrCode =[mudic objectForKey:@"qrcode"];
        self.mymodel.old_type_name =[mudic objectForKey:@"type_name"];
        self.mymodel.old_price =[mudic objectForKey:@"price"];
        self.mymodel.old_type_id =[mudic objectForKey:@"type_id"];
        self.mymodel.old_count =[mudic objectForKey:@"count"];
        self.mymodel.old_name =[mudic objectForKey:@"name"];

        if (!weakSelf.oldcateGoryLB) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showUpOldui];

            });

        }else{
            [self oldRefashwhimNodel];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}
-(void)oldRefashwhimNodel{
    self.oldtitleLB.text = @"旧配件";
    self.oldcateGoryLB.text = _mymodel.old_type_name;
    self.oldpruNameLB.text = _mymodel.old_name;
    self.oldprupriceLB.text = _mymodel.old_price;
    self.oldpruTotalpriceLB.text = _mymodel.old_price;
    
}
-(void)showUpOldui{
         self.oldBgView.height = 231;

    
    CGFloat heii = 46;
    if (self.newpruTotalpriceLB) {
        heii = 231;
    }
        self.newgView.frame = CGRectMake(10, self.oldBgView.bottom+10, KWIDTH-20, heii);
    
    UILabel *categotr = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 83, 14)];
    categotr.font = FontSize(14);
    categotr.text = @"配件品类:";
    categotr.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.oldBgView addSubview:categotr];
    self.oldcateGoryLB = [[UILabel alloc]initWithFrame:CGRectMake(categotr.right, 50, KWIDTH, 14)];
    self.oldcateGoryLB.font = FontSize(14);
    self.oldcateGoryLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.oldBgView  addSubview: self.oldcateGoryLB];
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, categotr.bottom+17, KWIDTH-20, 1)];
    [self.oldBgView addSubview:line];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    
    
    
    UILabel *oldpruName = [[UILabel alloc]initWithFrame:CGRectMake(10, line.bottom, 83, 47)];
    oldpruName.font = FontSize(14);
    oldpruName.text = @"配件名称:";
    oldpruName.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.oldBgView addSubview:oldpruName];
    self.oldpruNameLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruName.right, line.bottom, KWIDTH, 47)];
    self.oldpruNameLB.font = FontSize(14);
    self.oldpruNameLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.oldBgView  addSubview: self.oldpruNameLB];
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, oldpruName.bottom, KWIDTH-20, 1)];
    [self.oldBgView addSubview:line1];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    
    UILabel *oldpruprice = [[UILabel alloc]initWithFrame:CGRectMake(10, line1.bottom, 83, 47)];
    oldpruprice.font = FontSize(14);
    oldpruprice.text = @"配件单价:";
    oldpruprice.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.oldBgView addSubview:oldpruprice];
    self.oldprupriceLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruprice.right, line1.bottom, KWIDTH, 47)];
    self.oldprupriceLB.font = FontSize(14);
    self.oldprupriceLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.oldBgView  addSubview: self.oldprupriceLB];
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, oldpruprice.bottom, KWIDTH-20, 1)];
    [self.oldBgView addSubview:line2];
    line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    UILabel *oldpruTotalprice = [[UILabel alloc]initWithFrame:CGRectMake(10, line2.bottom, 83, 47)];
    oldpruTotalprice.font = FontSize(14);
    oldpruTotalprice.text = @"配件费:";
    oldpruTotalprice.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.oldBgView addSubview:oldpruTotalprice];
    self.oldpruTotalpriceLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruTotalprice.right, line2.bottom, KWIDTH, 47)];
    self.oldpruTotalpriceLB.font = FontSize(14);
    self.oldpruTotalpriceLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.oldBgView  addSubview: self.oldpruTotalpriceLB];
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, oldpruTotalprice.bottom, KWIDTH-20, 1)];
    [self.oldBgView addSubview:line3];
    line3.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    [self oldRefashwhimNodel];

}



-(void)newsaoyiso:(UIButton *)but{
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self newrequesrtWith:str];

        });
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
    
}
- (void)newrequesrtWith:(NSString *)strr {
 
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        self.mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
        self.mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
        self.mymodel.ne_price =[mudic objectForKey:@"price"];
        self.mymodel.ne_type_id =[mudic objectForKey:@"type_id"];
        self.mymodel.ne_count =[mudic objectForKey:@"count"];
        self.mymodel.ne_name =[mudic objectForKey:@"name"];
        self.mymodel.isSaoyisao = @"2";
        if (!weakSelf.newpruTotalpriceLB) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self shownewui];

            });

            
        }else{
            [self newRefashwhimNodel];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
}
-(void)newRefashwhimNodel{
    self.newtitleLB.text = @"新更换配件";
    self.newcateGoryLB.text = _mymodel.ne_type_name;
    self.newpruNameLB.text = _mymodel.ne_name;
    self.newprupriceLB.text = _mymodel.ne_price;
    self.newpruTotalpriceLB.text = _mymodel.ne_price;
    
}
-(void)shownewui{
    
    self.newgView.height = 231;
    CGFloat heii = 46;
    if (self.oldpruTotalpriceLB) {
        heii = 231;
    }
    self.oldBgView.height = heii;


    UILabel *mycategory = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 83, 14)];
    mycategory.font = FontSize(14);
    mycategory.text = @"配件品类:";
    mycategory.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.newgView addSubview:mycategory];
    self.newcateGoryLB = [[UILabel alloc]initWithFrame:CGRectMake(mycategory.right, 50, KWIDTH, 14)];
    self.newcateGoryLB.font = FontSize(14);
    self.newcateGoryLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.newgView  addSubview: self.newcateGoryLB];
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, mycategory.bottom+17, KWIDTH-20, 1)];
    [self.newgView addSubview:line];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    
    
    
    UILabel *oldpruName = [[UILabel alloc]initWithFrame:CGRectMake(10, line.bottom, 83, 47)];
    oldpruName.font = FontSize(14);
    oldpruName.text = @"配件名称:";
    oldpruName.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.newgView addSubview:oldpruName];
    self.newpruNameLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruName.right, line.bottom, KWIDTH, 47)];
    self.newpruNameLB.font = FontSize(14);
    self.newpruNameLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.newgView  addSubview: self.newpruNameLB];
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, oldpruName.bottom, KWIDTH-20, 1)];
    [self.newgView addSubview:line1];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    
    UILabel *oldpruprice = [[UILabel alloc]initWithFrame:CGRectMake(10, line1.bottom, 83, 47)];
    oldpruprice.font = FontSize(14);
    oldpruprice.text = @"配件单价:";
    oldpruprice.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.newgView addSubview:oldpruprice];
    self.newprupriceLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruprice.right, line1.bottom, KWIDTH, 47)];
    self.newprupriceLB.font = FontSize(14);
    self.newprupriceLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.newgView  addSubview: self.newprupriceLB];
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, oldpruprice.bottom, KWIDTH-20, 1)];
    [self.newgView addSubview:line2];
    line2.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    
    UILabel *oldpruTotalprice = [[UILabel alloc]initWithFrame:CGRectMake(10, line2.bottom, 83, 47)];
    oldpruTotalprice.font = FontSize(14);
    oldpruTotalprice.text = @"配件费:";
    oldpruTotalprice.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.newgView addSubview:oldpruTotalprice];
    self.newpruTotalpriceLB = [[UILabel alloc]initWithFrame:CGRectMake(oldpruTotalprice.right, line2.bottom, KWIDTH, 47)];
    self.newpruTotalpriceLB.font = FontSize(14);
    self.newpruTotalpriceLB.textColor = [UIColor colorWithHexString:@"#666666"];
    [self.newgView  addSubview: self.newpruTotalpriceLB];
    
    [self newRefashwhimNodel];
}


@end
