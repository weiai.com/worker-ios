//
//  SeleproductTypeViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "SeleproductTypeViewController.h"
#import "CSproductTypeTableViewCell.h"
#import "CSProductListModel.h"

@interface SeleproductTypeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *leftTableview;
@property (nonatomic, strong) UITableView *centerTableview;
@property (nonatomic, strong) UITableView *rightterTableview;
@property (nonatomic, strong) NSMutableArray *leftDateSource;
@property (nonatomic, strong) NSMutableArray *centerDateSource;
@property (nonatomic, strong) NSMutableArray *rightDateSource;
@property (nonatomic, strong) CSProductListModel *selemodel;

@end

@implementation SeleproductTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.title) {
        
    } else {
        self.title = @"配件品类";
    }
    
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.leftDateSource = [NSMutableArray arrayWithCapacity:1];
    self.centerDateSource = [NSMutableArray arrayWithCapacity:1];
    self.rightDateSource = [NSMutableArray arrayWithCapacity:1];
    [self.rightbutton setTitle:@"完成" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [self showui];
    [self requestleft];
    // Do any additional setup after loading the view.
}

-(void)requestleft{
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pId"] = @"0";
    [NetWorkTool POST:getfenlei param:mudic success:^(id dic) {
        
        for (NSMutableDictionary *mudoc in [dic objectForKey:@"data"]) {
            CSProductListModel *model = [[CSProductListModel alloc]init];
            model.isele = @"0";
            [model setValuesForKeysWithDictionary:mudoc];
            [self.leftDateSource addObject:model];
        }
        [self.leftTableview reloadData];
        
    } other:^(id dic) {
        [self.leftTableview reloadData];
        
    } fail:^(NSError *error) {
        [self.leftTableview reloadData];
        
    } needUser:YES];
}

- (void)rightClick{
    if (!self.selemodel) {
        ShowToastWithText(@"请选择配件品类");
        return;
    }
    if (self.myblock) {
        self.myblock(self.selemodel.typeName, self.selemodel.Id);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showui{
    CGFloat  leftright = 30;
    CGFloat  tabWith  = (KWIDTH - leftright - leftright-10)/3;
    
    self.leftTableview  = [[UITableView alloc]initWithFrame:CGRectMake(leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
    _leftTableview.delegate = self;
    _leftTableview.dataSource = self;
    _leftTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_leftTableview];
    
    _leftTableview.backgroundColor = [UIColor whiteColor];
    self.centerTableview  = [[UITableView alloc]initWithFrame:CGRectMake(tabWith+5+leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
    _centerTableview.delegate = self;
    _centerTableview.dataSource = self;
    [self.view addSubview:_centerTableview];
    _centerTableview.backgroundColor = [UIColor whiteColor];
    _centerTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.rightterTableview  = [[UITableView alloc]initWithFrame:CGRectMake(tabWith+tabWith+10+leftright, kNaviHeight, tabWith, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
    _rightterTableview.delegate = self;
    _rightterTableview.dataSource = self;
    [self.view addSubview:_rightterTableview];
    _rightterTableview.backgroundColor = [UIColor whiteColor];
    _rightterTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.leftTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
    [self.centerTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
    [self.rightterTableview  registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableview) {
        
        CSProductListModel *model = [_leftDateSource safeObjectAtIndex:indexPath.row];
        for (CSProductListModel *mymodel in _leftDateSource) {
            mymodel.isele =@"0";
        }
        
        model.isele = @"1";
        [self.leftTableview reloadData];
        [self showcenterWithid:model.Id];
        
    } else if (tableView  == _centerTableview){
        
        CSProductListModel *model = [_centerDateSource safeObjectAtIndex:indexPath.row];
        
        for (CSProductListModel *mymodel in _centerDateSource) {
            mymodel.isele =@"0";
        }
        
        model.isele = @"1";
        [self.centerTableview reloadData];
        
        [self showrightWithid:model.Id];
    } else {
        CSProductListModel *model = [_rightDateSource safeObjectAtIndex:indexPath.row];
        self.selemodel = model;
        
        for (CSProductListModel *mymodel in _rightDateSource) {
            mymodel.isele =@"0";
        }
        model.isele = @"1";
        [self.rightterTableview reloadData];
        //if (self.myblock) {
        //    self.myblock(model.typeName, model.Id);
        //}
    }
}

-(void)showcenterWithid:(NSString *)str{
    [self.centerDateSource removeAllObjects];
    [self.rightDateSource removeAllObjects];
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pid"] = str;
    [NetWorkTool POST:getfenleiWithID param:mudic success:^(id dic) {
        
        self.centerDateSource = [CSProductListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.centerTableview reloadData];
        [self.rightDateSource removeAllObjects];
        [self.rightterTableview reloadData];
        
    } other:^(id dic) {
        [self.centerTableview reloadData];
        [self.rightDateSource removeAllObjects];
        [self.rightterTableview reloadData];
        
    } fail:^(NSError *error) {
        [self.centerTableview reloadData];
        [self.rightDateSource removeAllObjects];
        [self.rightterTableview reloadData];
        
    } needUser:YES];
}

-(void)showrightWithid:(NSString *)str{
    [self.rightDateSource removeAllObjects];
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"pid"] = str;
    [NetWorkTool POST:getfenleiWithID param:mudic success:^(id dic) {
        
        self.rightDateSource = [CSProductListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.rightterTableview reloadData];
        
    } other:^(id dic) {
        [self.rightterTableview reloadData];
        
    } fail:^(NSError *error) {
        [self.rightterTableview reloadData];
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableview) {
        return _leftDateSource.count;
    } else if (tableView  == _centerTableview){
        return _centerDateSource.count;
    } else {
        return _rightDateSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSproductTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSproductTypeTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (tableView == _leftTableview) {
        
        CSProductListModel *model = [_leftDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        } else {
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
        }
    } else if (tableView  == _centerTableview){
        
        CSProductListModel *model = [_centerDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        } else {
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
        }
        
    } else {
        CSProductListModel *model = [_rightDateSource safeObjectAtIndex:indexPath.row];
        cell.mycontentLB.text = model.typeName;
        
        if ([model.isele integerValue] == 1) {
            cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        } else {
            cell.mycontentLB.textColor =[UIColor colorWithHexString:@"#5D5D5D"];
        }
    }
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
