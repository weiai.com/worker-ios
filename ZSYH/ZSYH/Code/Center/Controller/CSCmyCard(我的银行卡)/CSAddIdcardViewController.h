//
//  CSAddIdcardViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSPersonModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSAddIdcardViewController : BaseViewController
@property(nonatomic,strong)CSPersonModel *model;

@end

NS_ASSUME_NONNULL_END
