//
//  CSIdAuthViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/8.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSIdAuthViewController.h"
#import "JYBDBankCardVC.h"
#import "MBProgressHUD.h"
#import "JYBDIDCardVC.h"
#import "CSsecviceViewController.h"
#import <AipOcrSdk/AipOcrSdk.h>

@interface CSIdAuthViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    // 默认的识别成功的回调
    void (^_successHandler)(id);
    // 默认的识别失败的回调
    void (^_failHandler)(NSError *);
}

@property(nonatomic,strong)UIScrollView *scrollV;
@property(nonatomic,strong)UILabel *tipsL;
@property(nonatomic,strong)UIImageView *upImage;
@property(nonatomic,strong)UIImageView *dowImage;
//@property(nonatomic,strong)UIImageView *holdIdIV;
@property (nonatomic, strong) UIImage *cardUpImage;     //身份证正面
@property (nonatomic, strong) UIImage *cardDownImage;   //身份证背面
@property (nonatomic, strong) UIImage *holdCardImage;   //手持身份证
@property(nonatomic,strong)UIButton *upBut;
@property(nonatomic,strong)UIButton *dowBUt;
//@property(nonatomic,strong)UIButton *holdIdB;
@property(nonatomic,strong)UIButton *nextB;
@property(nonatomic,strong)NSString *cardstr;
@property(nonatomic,strong)NSString *cardname;

@end

@implementation CSIdAuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUI];
    
    //授权方法1：在此处填写App的Api Key/Secret Key
    [[AipOcrService shardService] authWithAK:@"ZWe40BWSGFyWuLMzjAQI7MNx" andSK:@"LHzxsEzGqhtpgFNykN8fkmP3O9njQEIp"];
    
    [self configCallback];
}

- (void)setUI {
    
    self.title = @"身份认证";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    _scrollV = [UIScrollView new];
    _scrollV.frame = CGRectMake(0, 0, ScreenW, ScreenH);
    _scrollV.contentSize = CGSizeMake(ScreenW, ScreenH+100+107);
    [self.view addSubview:_scrollV];
    
    _tipsL = [UILabel new];
    _tipsL.text = @"请上传您的中国第二代居民身份证原件";
    _tipsL.textColor = [UIColor grayColor];
    _tipsL.font = [UIFont systemFontOfSize:15];
    _tipsL.textAlignment = NSTextAlignmentCenter;
    [_scrollV addSubview:_tipsL];
    [_tipsL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.width.mas_equalTo(ScreenW-40);
        make.height.mas_equalTo(40);
    }];
    
    _upImage = [UIImageView new];
    _upImage.userInteractionEnabled = YES;
    _upImage.image = [UIImage imageNamed:@"shenfenzhengZM"];
    [_scrollV addSubview:_upImage];
    [_upImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_tipsL.mas_bottom).with.offset(20);
        make.width.mas_equalTo(312);
        make.left.mas_equalTo((ScreenW-312)/2);
        make.height.mas_equalTo(175);
    }];
    
    _upBut = [UIButton new];
    [_upBut setBackgroundImage:[UIImage imageNamed:@"shenfenzhengAction"] forState:(UIControlStateNormal)];
    [_upBut addTarget:self action:@selector(upAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [_upImage addSubview:_upBut];
    [_upBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.centerX.mas_equalTo(self->_upImage.centerX);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(72);
    }];
    
    _dowImage = [UIImageView new];
    _dowImage.userInteractionEnabled = YES;
    _dowImage.image = [UIImage imageNamed:@"shenfenzhengFM"];
    [_scrollV addSubview:_dowImage];
    [_dowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_upImage.mas_bottom).with.offset(20);
        make.width.mas_equalTo(312);
        make.left.mas_equalTo((ScreenW-312)/2);
        make.height.mas_equalTo(175);
    }];
    
    _dowBUt = [UIButton new];
    [_dowBUt setBackgroundImage:[UIImage imageNamed:@"shenfenzhengAction"] forState:(UIControlStateNormal)];
    [_dowBUt addTarget:self action:@selector(dowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [_dowImage addSubview:_dowBUt];
    [_dowBUt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.centerX.mas_equalTo(self->_dowImage.centerX);
        make.width.mas_equalTo(72);
        make.height.mas_equalTo(72);
    }];
    
    _nextB = [UIButton new];
    [_nextB setTitle:@"提交" forState:(UIControlStateNormal)];
    [_nextB addTarget:self action:@selector(baocunAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _nextB.layer.cornerRadius = 3;
    [_nextB setBackgroundColor:zhutiColor];
    [_scrollV addSubview:_nextB];
    [_nextB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self->_dowImage.mas_bottom).with.offset(20);
        make.left.mas_equalTo(30);
        make.width.mas_equalTo(ScreenW-60);
        make.height.mas_equalTo(50);
    }];
    
    self.dowBUt.hidden = YES;
    //self.holdIdB.hidden = YES;
}

- (void)configCallback {
    __weak typeof(self) weakSelf = self;
    
    // 这是默认的识别成功的回调
    _successHandler = ^(id result){
        NSLog(@"%@", result);
        NSString *title = @"识别结果";
        NSMutableString *message = [NSMutableString string];
        
        if(result[@"words_result"]){
            if([result[@"words_result"] isKindOfClass:[NSDictionary class]]){
                [result[@"words_result"] enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                    if([obj isKindOfClass:[NSDictionary class]] && [obj objectForKey:@"words"]){
                        [message appendFormat:@"%@: %@\n", key, obj[@"words"]];
                    }else{
                        [message appendFormat:@"%@: %@\n", key, obj];
                    }
                    
                }];
            }else if([result[@"words_result"] isKindOfClass:[NSArray class]]){
                for(NSDictionary *obj in result[@"words_result"]){
                    if([obj isKindOfClass:[NSDictionary class]] && [obj objectForKey:@"words"]){
                        [message appendFormat:@"%@\n", obj[@"words"]];
                    } else {
                        [message appendFormat:@"%@\n", obj];
                    }
                }
            }
        } else {
            [message appendFormat:@"%@", result];
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:weakSelf cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }];
    };
    
    _failHandler = ^(NSError *error){
        NSLog(@"%@", error);
        NSString *msg = [NSString stringWithFormat:@"%li:%@", (long)[error code], [error localizedDescription]];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[[UIAlertView alloc] initWithTitle:@"识别失败" message:msg delegate:weakSelf cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
        }];
    };
}


//身份证正面
- (void)upAction:(UIButton *)sender {
    
    kWeakSelf;
    UIViewController * vc =
    [AipCaptureCardVC ViewControllerWithCardType:CardTypeLocalIdCardFont
                                 andImageHandler:^(UIImage *image) {
                                     [[AipOcrService shardService]
                                      detectIdCardFrontFromImage:image
                                      withOptions:nil
                                      successHandler:^(id result){
                                          NSLog(@"%@", result);
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              weakSelf.upImage.image = image;
                                              weakSelf.cardUpImage = image;             weakSelf.cardstr = result[@"words_result"][@"公民身份号码"][@"words"];
                                              weakSelf.cardname = result[@"words_result"][@"姓名"][@"words"];
                                              weakSelf.upBut.hidden = YES;
                                              weakSelf.dowBUt.hidden = NO;
                                              [weakSelf dismissViewControllerAnimated:YES completion:^{}];
                                          });
                                      }
                                      failHandler:self->_failHandler];
                                 }];
    [self presentViewController:vc animated:YES completion:nil];
}

//身份证反面
- (void)dowAction:(UIButton *)sender {
    
    kWeakSelf;
    UIViewController * vc =
    [AipCaptureCardVC ViewControllerWithCardType:CardTypeLocalIdCardBack
                                 andImageHandler:^(UIImage *image) {
                                     [[AipOcrService shardService]
                                      detectIdCardFrontFromImage:image
                                      withOptions:nil
                                      successHandler:^(id result){
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              weakSelf.dowImage.image = image;
                                              weakSelf.cardDownImage = image;
                                              weakSelf.dowBUt.hidden = YES;
                                              //weakSelf.holdIdB.hidden = NO;
                                              [weakSelf dismissViewControllerAnimated:YES completion:^{}];
                                          });
                                      }
                                      failHandler:self->_failHandler];
                                 }];
    [self presentViewController:vc animated:YES completion:nil];
}

//手持身份证
- (void)holdIdBClick:(UIButton *)sender {
    UIImagePickerController *pick = [[UIImagePickerController alloc]init];
    pick.delegate = self;
    [UIAlertController showPhotoAlerControllerPicker:pick controller:self sourceView:nil];
}

//imagePicker代理事件
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    dispatch_async(dispatch_get_main_queue(), ^{
        //self.holdIdIV.image = image;
        self.holdCardImage = image;
        self.upBut.hidden = YES;
        self.dowBUt.hidden = YES;
        //self.holdIdB.hidden = NO;
    });
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 提交按钮
- (void)baocunAction:(UIButton *)sender {
    
    if (!_cardUpImage) {
        ShowToastWithText(@"请扫描身份证正面照");
        return;
    }
    if (!_cardDownImage) {
        ShowToastWithText(@"请扫描身份证反面照");
        return;
    }
    //if (!_holdCardImage) {
        //ShowToastWithText(@"请拍摄手持身份证照");
        //return;
    //}
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    [muarr addObject:_upImage];
    [muarr addObject:_dowImage];
    //[muarr addObject:_holdIdIV];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"idCard"] = _cardstr;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    kWeakSelf;
    [NetWorkTool UploadPicWithUrl:checkIdCard param:param key:@"image" image:muarr withSuccess:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([[dic objectForKey:@"errorCode"] integerValue] == 0) {
            ShowToastWithText(@"上传成功");
            if (weakSelf.commitBlock) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:1];
                [dict setObject:[NSString stringWithFormat:@"%@", weakSelf.cardstr] forKey:@"idCard"];
                [dict setObject:[NSString stringWithFormat:@"%@", weakSelf.cardname] forKey:@"name"];
                weakSelf.commitBlock(dict, muarr);
                [self.navigationController popViewControllerAnimated:YES];
                return ;
            }
            CSsecviceViewController *dfdsfdsf = [[CSsecviceViewController alloc]init];
            dfdsfdsf.isrgister = @"zhuce";
            [self.navigationController pushViewController:dfdsfdsf animated:YES];
        } else {
            ShowToastWithText(dic[@"msg"]);
        }
    } failure:^(NSError *error) {

    }];
}

@end
