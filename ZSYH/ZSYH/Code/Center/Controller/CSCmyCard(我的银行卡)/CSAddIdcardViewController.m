
//
//  CSAddIdcardViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSAddIdcardViewController.h"
#import "CSproductTypeTableViewCell.h"
#import "CSCbankCardListModel.h"
#import "CSaddPerCardViewController.h"
#import "CSIdAuthViewController.h"
#import "JYBDBankCardVC.h"


@interface CSAddIdcardViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)UITextField *cardName;
@property(nonatomic,strong)UITextField *cardNumber;
@property(nonatomic,strong)UITextField *name;
@property(nonatomic,strong)UITextField *nameNumber;
@property(nonatomic,strong)CSCbankCardListModel *seleModel;

@property(nonatomic,strong)UIView *bgViewtime;
@end
@implementation CSAddIdcardViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = @"添加银行卡";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.mydateSource  = [NSMutableArray arrayWithCapacity:1];
    [self showdeteile];
    [self shwosecBgview];
}
- (void)rightClick{
    
}
-(void)showdeteile{
    CGFloat left = 16;
    CGFloat lbWith = 140-left;
    CGFloat lbHei = 55;
    UILabel *yinhangkaLB = [[UILabel alloc]initWithFrame:CGRectMake(left, kNaviHeight, lbWith, lbHei)];
    yinhangkaLB.textColor = [UIColor colorWithHexString:@"#333333"];
    yinhangkaLB.font = FontSize(16);
    yinhangkaLB.text = @"银行卡";
    [self.view addSubview:yinhangkaLB];
    
    self.cardName = [[UITextField alloc]initWithFrame:CGRectMake(lbWith-left, kNaviHeight, KWIDTH-60-lbWith-left, lbHei)];
    [self.view addSubview:self.cardName];
    self.cardName.placeholder = @"选择银行卡所属银行";
    self.cardName.delegate = self;
    UIButton *seleButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    seleButton.frame = CGRectMake(KWIDTH-60, kNaviHeight, 40, lbHei);
    [seleButton setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [self.view addSubview:seleButton];
    [seleButton addTarget:self action:@selector(seleYinName:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left, yinhangkaLB.bottom, KWIDTH-left-left, 1)];
    [self.view addSubview:line];
    line.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    
    UILabel *kaHaoLB = [[UILabel alloc]initWithFrame:CGRectMake(left, line.bottom, lbWith, lbHei)];
    kaHaoLB.textColor = [UIColor colorWithHexString:@"#333333"];
    kaHaoLB.font = FontSize(16);
    kaHaoLB.text = @"卡 号";
    [self.view addSubview:kaHaoLB];
    
    self.cardNumber = [[UITextField alloc]initWithFrame:CGRectMake(lbWith-left, line.bottom, KWIDTH-left-left-60, lbHei)];
    [self.view addSubview:self.cardNumber];
    self.cardNumber.placeholder = @"请输入或扫描银行卡号码";
    self.cardNumber.userInteractionEnabled = NO;
    UIButton *saoyisao = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [saoyisao setImage:imgname(@"erweimaLv") forState:(UIControlStateNormal)];
    [self.view addSubview:saoyisao];
    saoyisao.frame = CGRectMake(KWIDTH-55, line.bottom, 40, lbHei);
    [saoyisao addTarget:self action:@selector(saoyosao:) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(left, kaHaoLB.bottom, KWIDTH-left-left, 1)];
    [self.view addSubview:line1];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    
    
    UILabel *nameLB = [[UILabel alloc]initWithFrame:CGRectMake(left, line1.bottom, lbWith, lbHei)];
    nameLB.textColor = [UIColor colorWithHexString:@"#333333"];
    nameLB.font = FontSize(16);
    nameLB.text = @"姓 名";
    [self.view addSubview:nameLB];
    
    self.name = [[UITextField alloc]initWithFrame:CGRectMake(lbWith-left, line1.bottom, KWIDTH-left-left, lbHei)];
    [self.view addSubview:self.name];
    self.name.placeholder = @"请输入持卡人姓名";
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left, nameLB.bottom, KWIDTH-left-left, 1)];
    [self.view addSubview:line2];
    line2.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    
    UILabel *idnumerLB = [[UILabel alloc]initWithFrame:CGRectMake(left, line2.bottom, lbWith, lbHei)];
    idnumerLB.textColor = [UIColor colorWithHexString:@"#333333"];
    idnumerLB.font = FontSize(16);
    idnumerLB.text = @"身份证号";
    [self.view addSubview:idnumerLB];
    
    self.nameNumber = [[UITextField alloc]initWithFrame:CGRectMake(lbWith-left, line2.bottom, KWIDTH-left-left, lbHei)];
    [self.view addSubview:self.nameNumber];
    self.nameNumber.placeholder = @"请上传身份证照片";
    self.nameNumber.userInteractionEnabled = NO;
    
    UIButton *camerabut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [camerabut setImage:imgname(@"Camerapic") forState:(UIControlStateNormal)];
    camerabut.frame =CGRectMake(KWIDTH-55, line2.bottom, 40, lbHei);
    
    [camerabut addTarget:self action:@selector(seleidcard:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:camerabut];

    
    
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left, idnumerLB.bottom, KWIDTH-left-left, 1)];
    [self.view addSubview:line3];
    line3.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    
    UILabel *bgLable = [[UILabel alloc]initWithFrame:CGRectMake(0, line3.bottom, KWIDTH, KHEIGHT)];
    bgLable.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:bgLable];

    
    UIButton *buildingButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    buildingButton.frame = CGRectMake(30, KHEIGHT-140-48, KWIDTH-60, 48);
    [buildingButton setTitle:@"立即绑定" forState:(UIControlStateNormal)];
    [self.view addSubview:buildingButton];
    [buildingButton addTarget:self action:@selector(buildingButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    buildingButton.layer.masksToBounds = YES;
    buildingButton.layer.cornerRadius = 4;
    [buildingButton setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [buildingButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    
    if (!strIsEmpty(_model.id_card_no)) {
        self.nameNumber.text =_model.id_card_no;
        camerabut.hidden = YES;
    }
    
    
}
-(void)saoyosao:(UIButton *)but{
    if (_cardName.text.length  == 0) {
        ShowToastWithText(@"请先选择银行卡所属银行");
        return;
    }
    
    JYBDBankCardVC *AVCaptureVC = [[JYBDBankCardVC alloc] init];
    
    AVCaptureVC.finish = ^(JYBDBankCardInfo *info, UIImage *image) {
        
        self->_cardNumber.text = info.bankNumber;
//        if ([info.bankName substringFromIndex:(info.bankName.length-4)] isEqualToString:[]) {
//
//        }
        //返回数据不规范 没法判断
        
    };
    [self.navigationController pushViewController:AVCaptureVC animated:YES];
    

}
-(void)seleidcard:(UIButton *)but{
    
    KMyLog(@"ddddd");
    CSIdAuthViewController *ddvc = [[CSIdAuthViewController alloc]init];
    ddvc.mybliock = ^(NSString * _Nonnull num) {
        if (!strIsEmpty(num)) {
            
        self.nameNumber.text = num;
            
        }
    };
    
    [self.navigationController pushViewController:ddvc animated:YES];
    
}
-(void)buildingButtonAction:(UIButton *)but{
    if (_cardName.text.length  == 0) {
        ShowToastWithText(@"请先选择银行卡所属银行");
        return;
    }
    
    if (_cardNumber.text.length  == 0) {
        ShowToastWithText(@"银行卡号不能为空");
        return;
    }
    if (_name.text.length  == 0) {
        ShowToastWithText(@"持卡人姓名不能为空");
        return;
    }
    
    
    KMyLog(@"立即绑定");
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"bank_card"] = _cardNumber.text;
    param[@"band_card_type"] = _seleModel.band_card_type;
    param[@"bank_card_belong"] = _seleModel.Id;
    param[@"bankCard"] = _cardName.text;

    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *mu = [NSMutableDictionary dictionaryWithCapacity:1];
    mu[@"bankCard"] = json;
    [NetWorkTool POST:CmyAddBankCard param:mu success:^(id dic) {
        KMyLog(@"添加银行卡成功%@",dic);
        [self.navigationController popViewControllerAnimated:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
    
    
    
}
-(void)seleYinName:(UIButton *)but{
    
    KMyLog(@"选择银行名字");
    [[UIApplication  sharedApplication].keyWindow addSubview:self.bgViewtime];
    
}
-(void)shwosecBgview{
    self.bgViewtime = [[UIView alloc]init];
    self.bgViewtime.frame = self.view.bounds;
    self.bgViewtime.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
//    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewtime addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(0);
        make.height.offset(308);
        make.left.right.offset(0);
        
    }];
    
    UIView *headerview =[[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 50)];
    headerview.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    [whiteBGView addSubview:headerview];
    UIButton *amBUt = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [amBUt setTitle:@"取消" forState:(UIControlStateNormal)];
    [amBUt setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
    [amBUt addTarget:self action:@selector(amBUtAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [headerview addSubview:amBUt];
    UIButton *pmBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [pmBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [pmBut setTitleColor:[UIColor colorWithHexString:@"#666666"] forState:(UIControlStateNormal)];
    [pmBut addTarget:self action:@selector(pmButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [headerview addSubview:pmBut];
    
    [amBUt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.left.offset(20);
        make.width.offset(57);
    }];
    [pmBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.right.offset(-20);
        make.width.offset(57);
    }];
    
    [whiteBGView addSubview:   self.myTableView];
    
    [self.myTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.top.mas_equalTo(headerview.mas_bottom).offset(0);
    }];
    [self request];
    
}
-(void)request{
    
    [NetWorkTool POST:getCardtype param:nil success:^(id dic) {

        self.mydateSource = [CSCbankCardListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
-(void)amBUtAction:(UIButton *)but{
    //取消
    [self.bgViewtime removeFromSuperview];
}
-(void)pmButAction:(UIButton *)but{
    //确定
    [self.bgViewtime removeFromSuperview];
    self.cardName.text = _seleModel.type_name;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSproductTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSproductTypeTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CSCbankCardListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if (self.seleModel == model) {
        cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#70BE68"];

    }else{
        cell.mycontentLB.textColor = [UIColor colorWithHexString:@"#999999"];

    }
    cell.mycontentLB.text = model.type_name;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.seleModel = [_mydateSource safeObjectAtIndex:indexPath.row];
    [self.myTableView reloadData];
}
-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]init];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _myTableView.tableFooterView = [UIView new];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSproductTypeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSproductTypeTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
