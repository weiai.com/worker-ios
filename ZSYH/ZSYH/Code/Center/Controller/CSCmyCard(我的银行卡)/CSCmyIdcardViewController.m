//
//  CSCmyIdcardViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSCmyIdcardViewController.h"
#import "CSBankCDTableViewCell.h"
#import "CSAddIdcardViewController.h"

@interface CSCmyIdcardViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;


@end
@implementation CSCmyIdcardViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self request];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"我的银行卡";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self.rightbutton setImage:imgname(@"C添加") forState:(UIControlStateNormal)];

    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    
    // Do any additional setup after loading the view.
    [self request];
}
- (void)rightClick{
    CSAddIdcardViewController *addvc = [[CSAddIdcardViewController alloc]init];
    addvc.model = self.model;
    [self.navigationController pushViewController:addvc animated:YES];
}

-(void)request{
    [self.mydateSource removeAllObjects];
    [NetWorkTool POST:CmyBankidList param:nil success:^(id dic) {
        [self.mydateSource removeAllObjects];
        
        self.mydateSource = [CSCbankCardListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];

    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];


        
    } needUser:YES];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return self.mydateSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 122;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSBankCDTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSBankCDTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSCbankCardListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell refash:model];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CSCbankCardListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if (self.myblcok) {
        self.myblcok(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [self->_myTableView.mj_header beginRefreshing];
        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSBankCDTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSBankCDTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}







@end
