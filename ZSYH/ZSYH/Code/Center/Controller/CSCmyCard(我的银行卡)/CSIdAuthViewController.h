//
//  CSIdAuthViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/8.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSIdAuthViewController : BaseViewController
@property(nonatomic,copy)void (^commitBlock)(NSMutableDictionary *dict, NSMutableArray *muarr);
@property(nonatomic,copy)void (^mybliock)(NSString *num);
@property(nonatomic,copy) NSString *from;
@end

NS_ASSUME_NONNULL_END
