//
//  CSMyTouSuViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyTouSuViewController.h"
#import "CSTousuTableViewCell.h"
#import "CStousuModel.h"
#import "CStousuCOnViewController.h"

@interface CSMyTouSuViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;

@end

@implementation CSMyTouSuViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"我的投诉";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    
    [self request];
    // Do any additional setup after loading the view.
}

-(void)request{
    [self.mydateSource removeAllObjects];
    [NetWorkTool POST:getTousu param:nil success:^(id dic) {
        
        self.mydateSource = [CStousuModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        
    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 122;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSTousuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSTousuTableViewCell" forIndexPath:indexPath];
    [_myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; //取消cell的下划线
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CStousuModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell reasfd:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CStousuModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    CStousuCOnViewController *conVC = [[CStousuCOnViewController alloc]init];
    conVC.idStr = model.Id;
    [self.navigationController pushViewController:conVC animated:YES];
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [_myTableView.mj_header beginRefreshing];
        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSTousuTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSTousuTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
