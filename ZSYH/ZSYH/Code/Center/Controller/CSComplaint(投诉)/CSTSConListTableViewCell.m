//
//  CSTSConListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSTSConListTableViewCell.h"

@implementation CSTSConListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)reash:(CStousuConLisModel *)model{
    
    self.timeLB.text = model.del_time;
    if ([model.state integerValue] == 0) {
        self.stateLB.text = @"未处理";
        self.cenLb.text = @"提交投诉";
    }else  if ([model.state integerValue] == 1) {
        self.stateLB.text = @"处理中";
        self.cenLb.text = @"区域负责人已知晓投诉";

    }else  if ([model.state integerValue] == 2) {
        self.stateLB.text = @"已处理";
        self.cenLb.text = @"区域负责人给出处理结果";

    }
    
    self.contentLb.text = model.del_content;

    
}

@end
