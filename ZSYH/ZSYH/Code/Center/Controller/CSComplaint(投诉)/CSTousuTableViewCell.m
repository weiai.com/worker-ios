//
//  CSTousuTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSTousuTableViewCell.h"

@implementation CSTousuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.timeLb.font = KFontPingFangSCMedium(12);
    self.stauLB.font = KFontPingFangSCMedium(12);
    
    self.tsfsTitLab.font = KFontPingFangSCMedium(14);
    self.tsztTitLab.font = KFontPingFangSCMedium(14);
    self.tswtTitLab.font = KFontPingFangSCMedium(14);
    
    self.fangshiLB.font = KFontPingFangSCMedium(14);
    self.zhutiLB.font = KFontPingFangSCMedium(14);
    self.wentiLb.font = KFontPingFangSCMedium(14);

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)reasfd:(CStousuModel *)model{
    
    self.timeLb.text = [model.update_time substringToIndex:16];
    if ([model.state integerValue] == 0) {
        self.stauLB.text = @"未处理";
    } else if ([model.state integerValue] == 1) {
        self.stauLB.text = @"处理中";
    } else if ([model.state integerValue] == 2) {
        self.stauLB.text = @"已处理";
    }
    self.fangshiLB.text = @"电话投诉";
    self.zhutiLB.text = model.title;
    self.wentiLb.text = model.content;
}

@end
