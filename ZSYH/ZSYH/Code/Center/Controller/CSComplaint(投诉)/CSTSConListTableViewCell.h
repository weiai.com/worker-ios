//
//  CSTSConListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CStousuConLisModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSTSConListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *myimage;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *stateLB;
@property (weak, nonatomic) IBOutlet UILabel *cenLb;
@property (weak, nonatomic) IBOutlet UILabel *contentLb;
@property (weak, nonatomic) IBOutlet UILabel *huifuLB;

-(void)reash:(CStousuConLisModel *)model;
@end

NS_ASSUME_NONNULL_END
