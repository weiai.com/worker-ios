
//
//  CStousuCOnViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CStousuCOnViewController.h"
#import "CSTSConListTableViewCell.h"
#import "CStousuConLisModel.h"

@interface CStousuCOnViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSDictionary *mydic;


@end

@implementation CStousuCOnViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"投诉详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    
    // Do any additional setup after loading the view.
    [self request];
}
-(void)shottabfoot{
    
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 10, KWIDTH, 106)];
    header.backgroundColor = [UIColor whiteColor];
    
    UILabel *left0 = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 71, 40)];
    left0.font = FontSize(14);
    left0.textColor = K666666;
    left0.text = @"投诉方式";
    [header addSubview:left0];
    UILabel *center0 = [[UILabel alloc]initWithFrame:CGRectMake(87, 0, KWIDTH-100, 40)];
    center0.font = FontSize(14);
    center0.textColor = K666666;
    center0.text = @"电话投诉";
    [header addSubview:center0];
    UILabel *timeLB0 = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-160, 0, 140, 40)];
    timeLB0.font = FontSize(12);
    timeLB0.textAlignment = NSTextAlignmentRight;
    timeLB0.textColor = K666666;
    timeLB0.text = [[_mydic objectForKey:@"create_time"] substringToIndex:16];
    [header addSubview:timeLB0];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(15, left0.bottom, KWIDTH-32, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [header addSubview:line];
    
    UILabel *left1 = [[UILabel alloc]initWithFrame:CGRectMake(16, line.bottom, 71, 40)];
    left1.font = FontSize(14);
    left1.textColor = K666666;
    left1.text = @"投诉主题";
    [header addSubview:left1];
    UILabel *center1 = [[UILabel alloc]initWithFrame:CGRectMake(87, line.bottom, KWIDTH-100, 40)];
    center1.font = FontSize(14);
    center1.textColor = K666666;
    center1.text = [_mydic objectForKey:@"title"];;
    [header addSubview:center1];
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(15, left1.bottom, KWIDTH-32, 1)];
    line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    [header addSubview:line1];
    
    
    UILabel *left2 = [[UILabel alloc]initWithFrame:CGRectMake(16, line1.bottom, 71, 40)];
    left2.font = FontSize(14);
    left2.textColor = K666666;
    left2.text = @"投诉问题";
    [header addSubview:left2];
    
    CGFloat fffh = [NSString heightWithWidth:KWIDTH-100 font:14 text:[_mydic objectForKey:@"content"]];
    if (fffh < 40) {
        fffh = 40;
    }
    UILabel *center3 = [[UILabel alloc]initWithFrame:CGRectMake(87, line1.bottom, KWIDTH-100, fffh)];
    center3.font = FontSize(14);
    center3.textColor = K666666;
    center3.text = [_mydic objectForKey:@"content"];;
    [header addSubview:center3];
    center3.numberOfLines = 0;

    header.height = center3.bottom+16;
    self.myTableView.tableHeaderView = header;
    
    
    
}

-(void)request{
    [self.mydateSource removeAllObjects];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = NOTNIL(_idStr);
    [NetWorkTool POST:cousuContentlist param:param success:^(id dic) {
        self.mydic = [NSDictionary dictionaryWithDictionary:[dic objectForKey:@"data"]];
        [self shottabfoot];
        
        for (NSMutableDictionary *mudic in [[dic objectForKey:@"data"] objectForKey:@"nodes"]) {
            CStousuConLisModel *model = [[CStousuConLisModel alloc]init];
            [model setValuesForKeysWithDictionary:mudic];
            [self.mydateSource addObject:model];
        }
        
        CStousuConLisModel *model = [[CStousuConLisModel alloc]init];
        model.del_time = self.mydic[@"create_time"];
        model.del_content = @"提交投诉";
        model.state = @"0";
        [self.mydateSource addObject:model];
        
        //添加时间线
        UIView *footerV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-106-self.mydateSource.count*90-kNaviHeight)];
        UIView *lineV = [UIView new];
        lineV.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        lineV.frame = CGRectMake(24, 0, 1, footerV.size.height);
        [footerV addSubview:lineV];
        self.myTableView.tableFooterView = footerV;
        
        [self.myTableView reloadData];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        
    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        
        
        
    } needUser:YES];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSTSConListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSTSConListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CStousuConLisModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell reash:model];
    
    if ([model.state isEqualToString:@"2"]) {
        cell.huifuLB.hidden = NO;
        cell.contentLb.hidden = NO;
        
    }else {
        cell.huifuLB.hidden = YES;
        cell.contentLb.hidden = YES;
    }
    
    if (indexPath.row == 0) {
        cell.timeLB.textColor = cell.cenLb.textColor = cell.stateLB.textColor = cell.contentLb.textColor = cell.huifuLB.textColor = [UIColor colorWithHexString:@"#70BE68"];
        cell.myimage.image = imgname(@"heidian");
        cell.stateLB.hidden = NO;

    }else{
         cell.timeLB.textColor = cell.cenLb.textColor = cell.stateLB.textColor = cell.contentLb.textColor = cell.huifuLB.textColor = K666666;
        cell.stateLB.hidden = YES;
        cell.myimage.image = imgname(@"heidian");
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.bounces = NO;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.tableFooterView = [UIView new];
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [_myTableView.mj_header beginRefreshing];
        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSTSConListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSTSConListTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}







@end
