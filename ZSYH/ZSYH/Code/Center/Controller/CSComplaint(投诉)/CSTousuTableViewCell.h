//
//  CSTousuTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CStousuModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSTousuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLb;
@property (weak, nonatomic) IBOutlet UILabel *stauLB;
@property (weak, nonatomic) IBOutlet UILabel *fangshiLB;
@property (weak, nonatomic) IBOutlet UILabel *zhutiLB;
@property (weak, nonatomic) IBOutlet UILabel *wentiLb;
@property (weak, nonatomic) IBOutlet UILabel *tsfsTitLab;
@property (weak, nonatomic) IBOutlet UILabel *tsztTitLab;
@property (weak, nonatomic) IBOutlet UILabel *tswtTitLab;

-(void)reasfd:(CStousuModel *)model;

@end

NS_ASSUME_NONNULL_END
