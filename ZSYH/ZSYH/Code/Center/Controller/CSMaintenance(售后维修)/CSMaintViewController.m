//
//  CSMaintViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修

#import "CSMaintViewController.h"
#import "CSnewsTableViewCell.h"
#import "CSlistModel.h"
#import "CSnewsTableViewCell.h"
#import "CSnewPTViewController.h"
#import "CSnewListDetalieViewController.h"
#import "CSDQCViewController.h"
#import "CSnewConViewController.h"
#import "CSmianListTableViewCell.h"
#import "CSSHmainLIstModel.h"
#import "CSnewJieGuoViewController.h"
#import "CSmaintenanceListModel.h"
#import "ZYPersonServiceOrderDetailsVC.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h"


@interface CSMaintViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;

@end

@implementation CSMaintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self request];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(request) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(request) name:WODEDINGDAN_REQUEST_ERCIBAOXIU_LISTDATA object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(request) name:@"applicationWillEnterForeground" object:nil];
}

#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)request{
    
    [self.mydateSource removeAllObjects];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    if ([_type isEqualToString:@"0"]) {
        
    } else {
        NSInteger iiidd = [_type integerValue];
        iiidd -=1;
        NSString *strtate = [NSString stringWithFormat:@"%ld",(long)iiidd];
        param[@"state"] = strtate;
    }
    [NetWorkTool POST:shouHouList param:param success:^(id dic) {
        [self.mydateSource removeAllObjects];
        KMyLog(@"售后维修%@",dic);
        self.mydateSource = [CSSHmainLIstModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
    } other:^(id dic) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
    } fail:^(NSError *error) {
        [self.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 130;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

/**
 * 分区头
 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

/**
 * 分区脚
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmianListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmianListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CSSHmainLIstModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    kWeakSelf;
    
    cell.myblcok = ^(NSString * _Nonnull str) {
        
        if ([model.user_type isEqualToString:@"0"]||[model.usertype isEqualToString:@"0"]) { //个人订单
            //跳转到个人订单 服务单详情
            ZYPersonServiceOrderDetailsVC *vc = [[ZYPersonServiceOrderDetailsVC alloc] init];
            vc.orderID = model.orderId;
            [self.navigationController pushViewController:vc animated:YES];
        } else if ([model.user_type isEqualToString:@"2"]||[model.usertype isEqualToString:@"2"]) { //电器厂家订单
            //跳转到电器厂家 服务单详情
            ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
            vc.orderID = model.orderId;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
        [weakSelf.myTableView reloadData];
       
    };
    //状态 -2已取消(维修工知晓后取消) -1已取消(直接取消) 0未接单 1维修工知晓 2已处理（更换配件） 3已处理（去下单） 4已处理（已下单）5售后结束
    if ([model.state integerValue]  == 0) {
        cell.stuteLB.text = @"未处理";
    }else  if ([model.state integerValue]  == 1) {
        cell.stuteLB.text = @"已知晓";
    }else   if ([model.state integerValue]  == -2) {
        cell.stuteLB.text = @"已取消";
    }else {
        cell.stuteLB.text = @"已处理";
    }
    
    cell.uptimeLB.text = [model.createDate substringToIndex:16];
    cell.yuyueTimeLb.text = [NSString stringWithFormat:@"%@ %@",model.orderDate,model.orderTime];
    cell.wentiLb.text = model.account;
    return cell;
}

-(NSDate *)nsstringConversionNSDate:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *datestr = [dateFormatter dateFromString:dateStr];
    return datestr;
}

-(NSString *)dateConversionTimeStamp:(NSDate *)date{
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]*1000];
    return timeSp;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CSSHmainLIstModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    if ([model.state integerValue ]  == 1) {
        CSnewJieGuoViewController *vc = [[CSnewJieGuoViewController alloc]init];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        CSnewConViewController *vccc = [[CSnewConViewController alloc]init];
        vccc.model = model;
        vccc.myblock = ^(NSString * _Nonnull str) {
            [self request];
        };
        [self.navigationController pushViewController:vccc animated:YES];
        
    }
    
    //1  5 2  3
    //.更换配件2
    //2 去更换配件
    //3 客户下单给我
    //4 更换配件
    //5 不处理
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-44-kTabbarHeight) style:(UITableViewStyleGrouped)];
        _myTableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self request];
        }];
        
        [_myTableView registerNib:[UINib nibWithNibName:@"CSmianListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSmianListTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end


