//
//  CSMaintenanceViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMaintenanceViewController.h"
#import "CSMaintViewController.h"
#import "SGPagingView.h"

@interface CSMaintenanceViewController ()<SGPageTitleViewDelegate, SGPageContentScrollViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;
@property (nonatomic, strong) NSMutableArray *titArr;

@end

@implementation CSMaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"售后维修";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.titArr = [NSMutableArray arrayWithCapacity:1];
    
    [self showNav];
    //[self requestTitle];
    
    //[self.rightbutton setTitle:@"生成订单" forState:(UIControlStateNormal)];
    // Do any additional setup after loading the view.
}

- (void)rightClick{
    
}

-(void)showNav{
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.indicatorAdditionalWidth = 10; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
    configure.titleGradientEffect = YES;
    
    configure.titleSelectedColor= [UIColor colorWithHexString:@"#70BE68"];
    configure.titleColor= [UIColor colorWithHexString:@"#999999"];
    configure.indicatorColor = zhutiColor;
    configure.bottomSeparatorColor= [UIColor whiteColor];
    
    NSMutableArray *titleArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *childVCS = [NSMutableArray arrayWithCapacity:1];
    [titleArr addObject:@"全部"];
    [titleArr addObject:@"未处理"];
    [titleArr addObject:@"已知晓"];
    [titleArr addObject:@"已处理"];
    
    for (int i = 0; i<titleArr.count; i++) {
        CSMaintViewController *oneVC = [[CSMaintViewController alloc] init];
        oneVC.type = [NSString stringWithFormat:@"%d",i];
        
        [childVCS addObject:oneVC];
    }
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 44) delegate:self titleNames:titleArr configure:configure];
    //CGFloat ContentCollectionViewHeight = self.view.frame.size.height - CGRectGetMaxY(_pageTitleView.frame);
    //self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_pageTitleView.frame), KWIDTH, ContentCollectionViewHeight) parentVC:self childVCs:childVCS];
    self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, kNaviHeight+44, KWIDTH, KHEIGHT-kNaviHeight-44) parentVC:self childVCs:childVCS];
    _pageContentScrollView.delegatePageContentScrollView = self;
    [self.view addSubview:self.pageTitleView ];
    
    [self.view addSubview:_pageContentScrollView];
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [super textFieldShouldReturn:textField];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [super textFieldShouldReturn:textField];
    
    //    SearchResultController *vc = [SearchResultController new];
    //    vc.keyword = textField.text;
    //    vc.Ctype = @"0";
    //    [self.navigationController pushViewController:vc animated:YES];
    return YES;
}
- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 1 || index == 5) {
        [_pageTitleView removeBadgeForIndex:index];
    }
}


@end
