//
//  CSSHmainLIstModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSSHmainLIstModel : BaseModel
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *orderDate;
@property (nonatomic, copy) NSString *orderTime;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *handleDate; //已知晓时间

@property (nonatomic, copy) NSString *cancelDate; //取消时间
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *isread;
@property (nonatomic, copy) NSString *repairUserId;
@property (nonatomic, copy) NSString *userId;

@property (nonatomic, copy) NSString *repairUserDate;
@property (nonatomic, copy) NSString *orderId;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *user_type;
@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *app_order_id;
@property (nonatomic, copy) NSString *usertype;
@property (nonatomic, copy) NSString *afterOrderState;

@property (nonatomic, copy) NSString *afterOrderDate;
@property (nonatomic, copy) NSString *afterOrderTime;
@property (nonatomic, copy) NSString *afterAccount;
@end

NS_ASSUME_NONNULL_END
