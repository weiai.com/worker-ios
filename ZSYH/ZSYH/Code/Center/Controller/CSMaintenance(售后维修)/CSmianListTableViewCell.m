
//
//  CSmianListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmianListTableViewCell.h"

@implementation CSmianListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.uptimeLB.font = KFontPingFangSCRegular(12);
    self.yuyueTimeLb.font = KFontPingFangSCMedium(13);
    self.wentiLb.font = KFontPingFangSCMedium(13);
    self.serviceTimeTit.font = KFontPingFangSCMedium(13);
    self.problemDescTit.font = KFontPingFangSCMedium(13);

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)myaction:(UIButton *)sender {
    if (self.myblcok) {
        self.myblcok(@"");
    }
}

@end
