//
//  CSmianListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSmianListTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblcok)(NSString *str);
@property (weak, nonatomic) IBOutlet UILabel *uptimeLB;

@property (weak, nonatomic) IBOutlet UILabel *yuyueTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *wentiLb;
@property (weak, nonatomic) IBOutlet UILabel *stuteLB;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeTit;
@property (weak, nonatomic) IBOutlet UILabel *problemDescTit;


@end

NS_ASSUME_NONNULL_END
