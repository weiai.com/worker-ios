//
//  CSBuyDetaileViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSBuyDetaileViewController : BaseViewController
@property(nonatomic,strong)NSString *orderstr ;
@property(nonatomic,copy)NSString *from ;
@property(nonatomic,copy)NSString *payType ;
@property(nonatomic,strong)NSMutableArray *mydateSource ;
@property(nonatomic,strong)NSMutableArray *myHeaderdate ;
@property(nonatomic,assign)NSInteger ind ;


@end


NS_ASSUME_NONNULL_END
