//
//  CSorderHeaderModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSorderHeaderModel : BaseModel
@property(nonatomic,strong)NSString * address;
@property(nonatomic,strong)NSString * orderNumber;
@property(nonatomic,strong)NSString * sendTypeId;
@property(nonatomic,strong)NSString * repUserId;
@property(nonatomic,strong)NSString * countMoney;
@property(nonatomic,strong)NSString * updateTime;
@property(nonatomic,strong)NSString * carId;
@property(nonatomic,strong)NSString * state;
@property(nonatomic,strong)NSString * createTime;
@property(nonatomic,strong)NSString * phone;




@end

NS_ASSUME_NONNULL_END
