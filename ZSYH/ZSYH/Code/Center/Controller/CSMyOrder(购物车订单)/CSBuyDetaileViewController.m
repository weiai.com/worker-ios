//
//  CSBuyDetaileViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSBuyDetaileViewController.h"
#import "CSBuyOrderTableViewCell.h"
#import "CSMallOrderModel.h"
#import "CSorderHeaderModel.h"
#import "CSBuyOrderTableViewCell.h"
#import "CSMallOrderModel.h"
#import "CSorderHeaderModel.h"
#import "CSnewBuyOrderTableViewCell.h"
#import "CSpayViewController.h"
#import "CSBuyDetaileViewController.h"
#import "CSgoCommitViewController.h"
#import "CSlookMailCimmitViewController.h"
#import "CancelOrderView.h"
#import "CSShopViewController.h"
#import "TabBarViewController.h"

@interface CSBuyDetaileViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;
@property (weak, nonatomic) IBOutlet UILabel *ordertime;
@property (weak, nonatomic) IBOutlet UILabel *nameAndPhone;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (nonatomic,strong) NSMutableDictionary *hedledic;
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property (nonatomic,strong) CSorderHeaderModel *model;
@property (weak, nonatomic) IBOutlet UILabel *stateLB;
@property (weak, nonatomic) IBOutlet UIButton *leftBut;
@property (weak, nonatomic) IBOutlet UIButton *rightBut;
@property (weak, nonatomic) IBOutlet UIButton *shopNameBut;
@property (weak, nonatomic) IBOutlet UILabel *lable24h;

@end

@implementation CSBuyDetaileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mytableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.title = @"订单详情";
    
    //如果是从支付成功跳转过来的,重写订单详情页面
    if ([self.from isEqualToString:@"paySuc"]) {
        
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"返回商城" style:(UIBarButtonItemStyleDone) target:self action:@selector(backMall)];
        leftItem.tintColor = zhutiColor;
        [leftItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:14],NSFontAttributeName, nil] forState:UIControlStateNormal];
        self.navigationItem.leftBarButtonItem = leftItem;
        
    } else {
        [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    }
    
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    [_mytableView registerNib:[UINib nibWithNibName:@"CSBuyOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSBuyOrderTableViewCell"];
    adjustInset(_mytableView);
    
    self.hedledic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    self.leftBut.layer.borderWidth = 1;
    self.leftBut.layer.borderColor = zhutiColor.CGColor;

    [self relodate];
}

//返回商城
- (void)backMall {
    TabBarViewController *tbc = [TabBarViewController new];
    tbc.selectedIndex = 2;
    [self presentViewController:tbc animated:YES completion:nil];
}

-(void)relodate{
    kWeakSelf;
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithCapacity:1];
    parm[@"orderId"] = _orderstr;
    
    [NetWorkTool POST:lookshopOrderDet param:parm success:^(id dic) {
        KMyLog(@"听到赣州%@",dic);
        self.hedledic = [[dic objectForKey:@"data"] objectForKey:@"order"];
        self.orderNumber.text =[self.hedledic  objectForKey:@"id"];
        self.ordertime.text =[self.hedledic  objectForKey:@"create_time"];
        self.nameAndPhone.text =[NSString stringWithFormat:@"%@ %@",[self.hedledic  objectForKey:@"user_name"],[self.hedledic  objectForKey:@"phone"]];
        self.address.text = dic[@"data"][@"address"][@"rep_address"];
        CSorderHeaderModel  *model = [[CSorderHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:self.hedledic];
        self.model =model;
        self.lable24h.hidden = YES;
        if ([model.state integerValue] == 0) {
            //待付款
            weakSelf.leftBut.hidden = NO;
            [weakSelf.leftBut setTitle:@"取消订单" forState:(UIControlStateNormal)];
            weakSelf.rightBut.hidden = NO;
            [weakSelf.rightBut setTitle:@"去付款" forState:(UIControlStateNormal)];
            self.lable24h.hidden = NO;

        }else if([model.state integerValue] == 1){
            //已付款
            weakSelf.leftBut.hidden = YES;
            weakSelf.rightBut.hidden = YES;
            //[rightBut setTitle:@"确认收货" forState:(UIControlStateNormal)];
            
        }else if([model.state integerValue] == 2){
            //待评价
            //去评价
            weakSelf.leftBut.hidden = YES;
            weakSelf.rightBut.hidden = NO;
            [weakSelf.rightBut setTitle:@"确认收货" forState:(UIControlStateNormal)];
        
        }else if([model.state integerValue] == 3){
            
            weakSelf.leftBut.hidden = YES;
            weakSelf.leftBut.hidden = NO;
            [weakSelf.rightBut setTitle:@"去评价" forState:(UIControlStateNormal)];
            
            //0代付款1待发货2待收货3待评价4已评价
        }else if([model.state integerValue] == 4){
            ////查看评价
            weakSelf.leftBut.hidden = YES;
            weakSelf.rightBut.hidden = NO;
            [weakSelf.rightBut setTitle:@"查看评价" forState:(UIControlStateNormal)];
            
        }else if([model.state integerValue] == -1){
            //已取消
            weakSelf.leftBut.hidden = YES;
            weakSelf.rightBut.hidden = YES;
        }
        
        if ([model.state integerValue] == 0) {
            //待付款
            //去付款
            weakSelf.stateLB.text = @"待付款";
        }else if([model.state integerValue] == 1){
            //已付款
            //去评价
             weakSelf.stateLB.text = @"待发货";
            
        }else if([model.state integerValue] == 2){
            //待评价
            //去评价
             weakSelf.stateLB.text = @"待收货";
            
        }else if([model.state integerValue] ==3){
            //待评价
            //去评价
             weakSelf.stateLB.text = @"待评价";
            
        }else if([model.state integerValue] == 4){
            //待评价
            //去评价
             weakSelf.stateLB.text = @"已评价";
        
        }else if([model.state integerValue] == -1){
            //待评价
            //去评价
             weakSelf.stateLB.text = @"已取消";
        }
        
        NSDictionary *products = [[dic objectForKey:@"data"] objectForKey:@"products"];
        for (NSDictionary *mydicccc in products) {
            CSMallOrderModel *model = [[CSMallOrderModel alloc]init];
            [model setValuesForKeysWithDictionary:mydicccc];
            [self.mydateSource addObject:model];
            
            [self.shopNameBut setTitle:model.shop_name forState:(UIControlStateNormal)];
            if (self.shopNameBut.frame.size.width > (KWIDTH- 120)) {
                self.shopNameBut.width  =KWIDTH- 120;
            }
        }
        [self.mytableView reloadData];
        
        //如果是从支付成功跳转过来的,重写订单详情页面
        if ([self.from isEqualToString:@"paySuc"]) {
            [self newTabfootvciew];
        }else {
            [self tabfootvciew];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (IBAction)shopnameAction:(UIButton *)sender {
    [self jinruAction];
}

- (IBAction)shopjinruAction:(UIButton *)sender {
    [self jinruAction];
}

-(void)jinruAction{
    CSMallOrderModel *mdeol = [_mydateSource firstObject];
    CSShopViewController *jin = [[CSShopViewController alloc]init];
    jin.shopid = mdeol.shop_id;
    [self.navigationController pushViewController:jin animated:YES];
}

- (IBAction)cellAction:(UIButton *)sender {
    [HFTools callMobilePhone:_model.phone];
}

- (IBAction)leftAction:(UIButton *)sender {
    
    CSorderHeaderModel *model = _model;
    if ([model.state integerValue] == 0) {
        //待付款  -  取消订单
//        //ShowToastWithText(@"取消订单");
//        CancelOrderView *cancel = [[CancelOrderView alloc]initWithFrame:[UIScreen mainScreen].bounds array:@[@"我不想买了",@"信息填写错误，重新拍",@"卖家缺货",@"其他原因"] block:^(NSString *index) {
//            [self quxiaodingdan:model with:index];
//            [cancel removeFromSuperview];
//        }];
//        [[UIApplication sharedApplication].keyWindow addSubview:cancel];
        
        UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"确定取消订单?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"取消");
        }]];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"确定");
            [self quxiaodingdan:model with:0];
        }]];
        
        [self presentViewController:alertCon animated:YES completion:nil];
        
    }else if([model.state integerValue] == 1){
        //已付款--
        
    }else if([model.state integerValue] == 2){
        //待评价
        [self goCimmef:0];
    }
}

- (IBAction)rightActon:(UIButton *)sender {
    //    0代付款1待发货2待收货3待评价4已评价
    
    CSorderHeaderModel *model = _model;
    if ([model.state integerValue] == 0) {
        //待付款  -  去付款
        
        [self gopayWithmodel:model];
    }else if([model.state integerValue] == 1){
        //已付款--认收货
        //        [self querenshouhuo:ind];
        
    }else if([model.state integerValue] == 2){
        //已付款--认收货
        [self querenshouhuo:0];
        
    }else if([model.state integerValue] == 3){
        //待评价
        //去评价
        [self goCimmef:0];
        //待评价
        //查看评价
        
    }else if([model.state integerValue] == 4){
        CSlookMailCimmitViewController *cimm = [[CSlookMailCimmitViewController alloc]init];
        cimm.orderid = model.Id;
        [self.navigationController pushViewController:cimm animated:YES];
    }
}

-(void)querenshouhuo:(NSInteger )ind{
    CSorderHeaderModel *model = _model;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(model.Id);
    [NetWorkTool POST:querenShouHuo param:param success:^(id dic) {
        ShowToastWithText(@"确认成功");
        [self->_mydateSource removeAllObjects];
        [self->_myHeaderdate removeAllObjects];
        [self relodate];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)goCimmef:(NSInteger )ind{
    CSgoCommitViewController *commit = [[CSgoCommitViewController alloc]init];
    
//    CSMallOrderModel *mod = [_mydateSource[ind] firstObject];
//    CSorderHeaderModel *hea = _myHeaderdate[ind];
//    commit.proModel = mod;
    commit.headerModel = _model;
    commit.myblock = ^(NSString * _Nonnull str) {
        [self relodate];
    };
    [self.navigationController pushViewController:commit animated:YES];
}

-(void)quxiaodingdan:(CSorderHeaderModel *)model with:(NSString *)str{
    //mailquxiaoOrder
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(model.Id);
    param[@"reason"] = str;
    
    [NetWorkTool POST:mailquxiaoOrder param:param success:^(id dic) {
        ShowToastWithText(@"取消成功");
        [self relodate];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)gopayWithmodel:(CSorderHeaderModel *)model{
    CSpayViewController *vc = [[CSpayViewController alloc]init];
    vc.orderSon =model.Id;
    vc.myblockj = ^(NSString * _Nonnull srr) {
        [self relodate];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSBuyOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSBuyOrderTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSMallOrderModel *mdeol = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell refashWithModel:mdeol];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(void)tabfootvciew{
    UIView *tabFoodView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 140)];
    tabFoodView.backgroundColor = [UIColor whiteColor];
    
    UILabel *bgLale = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 7)];
    bgLale.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabFoodView addSubview:bgLale];
    UILabel *peisiong = [[UILabel alloc]initWithFrame:CGRectMake(16, bgLale.bottom+12, 90, 20)];
    peisiong.font = FontSize(14);
    peisiong.textColor = K999999;
    peisiong.text = @"配送方式";
    [tabFoodView addSubview:peisiong];

    UILabel *peisiongLB = [[UILabel alloc]initWithFrame:CGRectMake(peisiong.right, bgLale.bottom+12, 90, 20)];
    peisiongLB.font = FontSize(14);
    peisiongLB.textColor = K666666;
    
    peisiongLB.text = @"物流";
    [tabFoodView addSubview:peisiongLB];
    NSArray *titleArr = @[@"自提",@"物流",@"滴滴配送"];
    if ([[_hedledic objectForKey:@"send_type_id"] integerValue] <titleArr.count) {
        peisiongLB.text = titleArr[[[_hedledic objectForKey:@"send_type_id"] integerValue]];
    }
    
    UILabel *bgLale2 = [[UILabel alloc]initWithFrame:CGRectMake(0, peisiong.bottom+12, KWIDTH, 7)];
    bgLale2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabFoodView addSubview:bgLale2];
    
    UILabel *jine = [[UILabel alloc]initWithFrame:CGRectMake(16, bgLale2.bottom+12, 90, 20)];
    jine.font = FontSize(14);
    jine.textColor = K999999;
    jine.text = @"产品金额";
    [tabFoodView addSubview:jine];
    
    UILabel *jinelb = [[UILabel alloc]initWithFrame:CGRectMake(jine.right, bgLale2.bottom+12, 90, 20)];
    jinelb.font = FontSize(14);
    jinelb.textColor = [UIColor colorWithHexString:@"#EC4900"];
    jinelb.text =[NSString stringWithFormat:@"¥%@",[_hedledic objectForKey:@"count_money"]];
    [tabFoodView addSubview:jinelb];
    CGFloat wwww = [NSString widthWithFont:14 text:jinelb.text];
    jinelb.frame = CGRectMake(KWIDTH-15-wwww, bgLale2.bottom+12, wwww, 20);
    
    UILabel *jinelblrft = [[UILabel alloc]initWithFrame:CGRectMake(70, bgLale2.bottom+12, KWIDTH-70-16-wwww-6, 20)];
    jinelblrft.font = FontSize(14);
    jinelblrft.textAlignment = NSTextAlignmentRight;
    jinelblrft.textColor = [UIColor colorWithHexString:@"#484848"];
    jinelblrft.text =@"(不含运费)";
    [tabFoodView addSubview:jinelblrft];
    
    UILabel *yfjine = [[UILabel alloc]initWithFrame:CGRectMake(16, jine.bottom+12, 90, 20)];
    yfjine.font = FontSize(14);
    yfjine.textColor = K999999;
    yfjine.text = @"应付金额";
    [tabFoodView addSubview:yfjine];
    
    UILabel *yfjinelb = [[UILabel alloc]initWithFrame:CGRectMake(jine.right, jine.bottom+12, 90, 20)];
    yfjinelb.font = FontSize(14);
    yfjinelb.textColor = [UIColor colorWithHexString:@"#EC4900"];
    yfjinelb.text =[NSString stringWithFormat:@"¥%@",[_hedledic objectForKey:@"count_money"]];
    [tabFoodView addSubview:yfjinelb];
    CGFloat yfwwww = [NSString widthWithFont:14 text:jinelb.text];
    yfjinelb.frame = CGRectMake(KWIDTH-15-yfwwww, jine.bottom+12, yfwwww, 20);
    
    UILabel *yfjinelblrft = [[UILabel alloc]initWithFrame:CGRectMake(70, jine.bottom+12, KWIDTH-70-16-wwww-6, 20)];
    yfjinelblrft.font = FontSize(14);
    yfjinelblrft.textAlignment = NSTextAlignmentRight;
    yfjinelblrft.textColor = [UIColor colorWithHexString:@"#484848"];
    yfjinelblrft.text =@"(不含运费)";
    [tabFoodView addSubview:yfjinelblrft];
    
//    CGFloat wwww = 0;
//    for (CSMallOrderModel *mode in _mydateSource) {
//        CGFloat fff =[mode.product_price floatValue];
//        NSInteger ind = [mode.pro_count integerValue];
//     wwww = fff * ind +wwww;
//    }
//    jinelb.text =[NSString stringWithFormat:@"%.2f",wwww];

    self.mytableView.tableFooterView = tabFoodView;
}

-(void)newTabfootvciew{
    UIView *tabFoodView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 170)];
    tabFoodView.backgroundColor = [UIColor whiteColor];
    
    UILabel *bgLale = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 7)];
    bgLale.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabFoodView addSubview:bgLale];
    UILabel *peisiong = [[UILabel alloc]initWithFrame:CGRectMake(16, bgLale.bottom+12, 90, 20)];
    peisiong.font = FontSize(14);
    peisiong.textColor = K999999;
    peisiong.text = @"配送方式";
    [tabFoodView addSubview:peisiong];
    
    UILabel *peisiongLB = [[UILabel alloc]initWithFrame:CGRectMake(peisiong.right, bgLale.bottom+12, 90, 20)];
    peisiongLB.font = FontSize(14);
    peisiongLB.textColor = K666666;
    
    peisiongLB.text = @"物流";
    [tabFoodView addSubview:peisiongLB];
    NSArray *titleArr = @[@"自提",@"物流",@"滴滴配送"];
    if ([[_hedledic objectForKey:@"send_type_id"] integerValue] <titleArr.count) {
        peisiongLB.text = titleArr[[[_hedledic objectForKey:@"send_type_id"] integerValue]];
    }
    
    UILabel *bgLale2 = [[UILabel alloc]initWithFrame:CGRectMake(0, peisiong.bottom+12, KWIDTH, 7)];
    bgLale2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabFoodView addSubview:bgLale2];
    
    UILabel *jine = [[UILabel alloc]initWithFrame:CGRectMake(16, bgLale2.bottom+12, 90, 20)];
    jine.font = FontSize(14);
    jine.textColor = K999999;
    jine.text = @"产品金额";
    [tabFoodView addSubview:jine];
    
    UILabel *jinelb = [[UILabel alloc]initWithFrame:CGRectMake(jine.right, bgLale2.bottom+12, 90, 20)];
    jinelb.font = FontSize(14);
    jinelb.textColor = [UIColor colorWithHexString:@"#EC4900"];
    jinelb.text =[NSString stringWithFormat:@"¥%@",[_hedledic objectForKey:@"count_money"]];
    [tabFoodView addSubview:jinelb];
    CGFloat wwww = [NSString widthWithFont:14 text:jinelb.text];
    jinelb.frame = CGRectMake(KWIDTH-15-wwww, bgLale2.bottom+12, wwww, 20);
    
    UILabel *jinelblrft = [[UILabel alloc]initWithFrame:CGRectMake(70, bgLale2.bottom+12, KWIDTH-70-16-wwww-6, 20)];
    jinelblrft.font = FontSize(14);
    jinelblrft.textAlignment = NSTextAlignmentRight;
    jinelblrft.textColor = [UIColor colorWithHexString:@"#484848"];
    jinelblrft.text =@"(不含运费)";
    [tabFoodView addSubview:jinelblrft];
    
    UILabel *yfjine = [[UILabel alloc]initWithFrame:CGRectMake(16, jine.bottom+12, 90, 20)];
    yfjine.font = FontSize(14);
    yfjine.textColor = K999999;
    yfjine.text = @"应付金额";
    [tabFoodView addSubview:yfjine];
    
    UILabel *yfjinelb = [[UILabel alloc]initWithFrame:CGRectMake(jine.right, jine.bottom+12, 90, 20)];
    yfjinelb.font = FontSize(14);
    yfjinelb.textColor = [UIColor colorWithHexString:@"#EC4900"];
    yfjinelb.text =[NSString stringWithFormat:@"¥%@",[_hedledic objectForKey:@"count_money"]];
    [tabFoodView addSubview:yfjinelb];
    CGFloat yfwwww = [NSString widthWithFont:14 text:jinelb.text];
    yfjinelb.frame = CGRectMake(KWIDTH-15-yfwwww, jine.bottom+12, yfwwww, 20);
    
    UILabel *yfjinelblrft = [[UILabel alloc]initWithFrame:CGRectMake(70, jine.bottom+12, KWIDTH-70-16-wwww-6, 20)];
    yfjinelblrft.font = FontSize(14);
    yfjinelblrft.textAlignment = NSTextAlignmentRight;
    yfjinelblrft.textColor = [UIColor colorWithHexString:@"#484848"];
    yfjinelblrft.text =@"(不含运费)";
    [tabFoodView addSubview:yfjinelblrft];
    
    UILabel *payTypeL = [[UILabel alloc]initWithFrame:CGRectMake(16, yfjine.bottom+12, 90, 20)];
    payTypeL.font = FontSize(14);
    payTypeL.textColor = K999999;
    payTypeL.text = @"支付方式";
    [tabFoodView addSubview:payTypeL];
    
    UILabel *payType = [[UILabel alloc]initWithFrame:CGRectMake(yfjine.right, yfjine.bottom+12, 90, 20)];
    payType.font = FontSize(14);
    payType.textColor = K666666;
    payType.text =self.payType;
    [tabFoodView addSubview:payType];
    CGFloat yfwwww2 = [NSString widthWithFont:14 text:yfjinelb.text];
    payType.frame = CGRectMake(KWIDTH-15-yfwwww2-20, yfjine.bottom+12, yfwwww2+20, 20);
    
    self.mytableView.tableFooterView = tabFoodView;
}

@end
