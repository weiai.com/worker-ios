//
//  CSBuyOrderDeViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSBuyOrderDeViewController.h"
#import "CSBuyOrderTableViewCell.h"
#import "CSMallOrderModel.h"
#import "CSorderHeaderModel.h"
#import "CSnewBuyOrderTableViewCell.h"
#import "CSpayViewController.h"
#import "CSBuyDetaileViewController.h"
#import "CSgoCommitViewController.h"
#import "CSlookMailCimmitViewController.h"
#import "CancelOrderView.h"
#import "CSShopViewController.h"

@interface CSBuyOrderDeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSMutableArray *myHeaderdate;

@end

@implementation CSBuyOrderDeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.myHeaderdate = [NSMutableArray arrayWithCapacity:1];
    
    [self.myTableView.mj_header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    
    if (_myTableView) {
        [self.myTableView.mj_header beginRefreshing];
    }
}

-(void)requeset{
    kWeakSelf;
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSInteger index  = [_type integerValue];
    
    if (index== 0) {
        param[@"state"] = @"";
    } else if (index== 1){
        index =0;
        NSString *sttt = [NSString stringWithFormat:@"%ld",(long)index];
        param[@"state"] = sttt;
    } else {
        NSString *sttt = [NSString stringWithFormat:@"%ld",(long)index];
        param[@"state"] = sttt;
    }
    [NetWorkTool POST:CmyOrderAll param:param success:^(id dic) {
        KMyLog(@"__%@",dic);
        [self.mydateSource  removeAllObjects];
        [weakSelf.myHeaderdate removeAllObjects];
        
        for (NSDictionary *moDic in [dic objectForKey:@"data"] ) {
            
            NSArray *modelDic = [moDic objectForKey:@"products"];
            if (modelDic.count >0 ) {
                CSorderHeaderModel *header = [[CSorderHeaderModel alloc]init];
                [header setValuesForKeysWithDictionary:[moDic objectForKey:@"order"]];
                
                [weakSelf.myHeaderdate addObject:header];
                NSMutableArray *cellMuarr = [NSMutableArray arrayWithCapacity:1];
                
                for (NSDictionary *pruDic in modelDic) {
                    CSMallOrderModel *model = [[CSMallOrderModel alloc]init];
                    [model setValuesForKeysWithDictionary:pruDic];
                    model.state = header.state;
                    [cellMuarr addObject:model];
                }
                [self.mydateSource addObject:cellMuarr];
            }
            [self.myTableView reloadData];
            //[MBProgressHUD hideHUDForView:self.view animated:YES];
            [weakSelf.myTableView.mj_header endRefreshing];
        }
    } other:^(id dic) {
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        [weakSelf.myTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        [weakSelf.myTableView.mj_header endRefreshing];
        
    } needUser:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 83;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CSorderHeaderModel *model = _myHeaderdate[section];
    UIView *tabHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 83)];
    
    tabHeaderView.backgroundColor = [UIColor whiteColor];
    UILabel *upLB = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 5)];
    upLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabHeaderView addSubview:upLB];
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(16, 19, 75, 17)];
    order.font = FontSize(14);
    order.textColor = K333333;
    order.text = @"订单编号：";
    [tabHeaderView addSubview:order];
    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(16+75, 19, 200, 17)];
    orderlb.font = FontSize(14);
    orderlb.textColor = K333333;
    [tabHeaderView addSubview:orderlb];
    orderlb.text = model.Id;
    
    UILabel *orderRQ = [[UILabel alloc]initWithFrame:CGRectMake(16, 61-23, 75, 17)];
    orderRQ.font = FontSize(14);
    orderRQ.textColor = K333333;
    orderRQ.text = @"订单日期：";
    [tabHeaderView addSubview:orderRQ];
    UILabel *orderRQlb = [[UILabel alloc]initWithFrame:CGRectMake(16+75, 61-23, 200, 17)];
    orderRQlb.font = FontSize(14);
    orderRQlb.textColor = K333333;
    [tabHeaderView addSubview:orderRQlb];
    orderRQlb.text = model.createTime;
    
    UILabel *stutelb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-16-50, 20, 50, 25)];
    stutelb.font = FontSize(14);
    stutelb.textColor = [UIColor colorWithHexString:@"#70BE68"];
    [tabHeaderView addSubview:stutelb];
    stutelb.text = model.createTime;
    UILabel *lineLB = [[UILabel alloc]initWithFrame:CGRectMake(16, 60, KWIDTH-32 ,1)];
    lineLB.font = FontSize(14);
    lineLB.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [tabHeaderView addSubview:lineLB];
    UILabel *lineLB2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 83, KWIDTH ,1)];
    lineLB2.backgroundColor = [UIColor whiteColor];
    [tabHeaderView addSubview:lineLB2];
    
    CSMallOrderModel *mdeole = _mydateSource[section][0];
    
    CGFloat www = [NSString widthWithFont:14 text:mdeole.shop_name];
    UILabel *shopname = [[UILabel alloc]initWithFrame:CGRectMake(16, 69, www, 14)];
    shopname.font = FontSize(14);
    shopname.textColor = K333333;
    [tabHeaderView addSubview:shopname];
    shopname.text = mdeole.shop_name;
    
    UIImageView *jintu = [[UIImageView alloc]initWithFrame:CGRectMake(shopname.right+16, 71, 6, 12)];
    jintu.image = imgname(@"Cjinru");
    [tabHeaderView addSubview:jintu];
    
    
    UIButton *jiruAction = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [jiruAction addTarget:self action:@selector(jinruAction:) forControlEvents:(UIControlEventTouchUpInside)];
    jiruAction.tag = 909+section;
    jiruAction.frame = CGRectMake(0, 68, jintu.right, 15);
    [tabHeaderView addSubview:jiruAction];
    
    //0代付款1待发货2待收货3待评价4已评价 -1已取消
    if ([model.state integerValue] == 0) {
        //待付款
        //去付款
        stutelb.text = @"待付款";
    }else if([model.state integerValue] == 1){
        //已付款
        //去评价
        stutelb.text = @"待发货";
    }else if([model.state integerValue] == 2){
        //待评价
        //去评价
        stutelb.text = @"待收货";
    }else if([model.state integerValue] ==3){
        //待评价
        //去评价
        stutelb.text = @"待评价";
    }else if([model.state integerValue] == 4){
        //待评价
        //去评价
        stutelb.text = @"已评价";
    }else if([model.state integerValue] == -1){
        //待评价
        //去评价
        stutelb.text = @"已取消";
    }
    return tabHeaderView;
}

-(void)jinruAction:(UIButton *)but{
    NSInteger indd = but.tag-909;
    CSMallOrderModel *mdeole = _mydateSource[indd][0];
    CSShopViewController *jin = [[CSShopViewController alloc]init];
    jin.shopid = mdeole.shop_id;
    [self.navigationController pushViewController:jin animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    CSorderHeaderModel *model = _myHeaderdate[section];
    if([model.state integerValue] == -1){
        return 0.01;
    }else if([model.state integerValue] == 1){
        return 0.01;
    }
    return 80;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    CSorderHeaderModel *model = _myHeaderdate[section];
    UIView *tabFoodView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 80)];
    tabFoodView.backgroundColor = [UIColor whiteColor];
    
    UILabel *total = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-16-40, 10, 40, 14)];
    total.font = FontSize(14);
    total.textColor = K666666;
    [tabFoodView addSubview:total];
    total.text = [NSString stringWithFormat:@"产品总价：¥%@",model.countMoney];
    CGFloat ww = [NSString widthWithFont:14 text:total.text];
    total.frame = CGRectMake(KWIDTH-16-ww, 10, ww, 14);
    NSInteger indd = 0;
    for (CSMallOrderModel *mdeol in _mydateSource[section]) {
        NSInteger dsfdsf = [[NSString stringWithFormat:@"%@",mdeol.pro_count] integerValue];
        indd += dsfdsf;
    }
    
    UILabel *gongji = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, KWIDTH-32-ww-9, 14)];
    gongji.font = FontSize(14);
    gongji.textColor = [UIColor colorWithHexString:@"#4A4A4A"];
    gongji.textAlignment = NSTextAlignmentRight;
    [tabFoodView addSubview:gongji];
    gongji.text = [NSString stringWithFormat:@"共计%ld件产品",indd];
    
    UIButton *rightBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    rightBut.frame = CGRectMake(KWIDTH-16-75, 36, 75, 32);
    [rightBut setBackgroundColor:zhutiColor];
    rightBut.layer.masksToBounds  = YES;
    rightBut.layer.cornerRadius = 4;
    [rightBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tabFoodView addSubview:rightBut];
    
    [rightBut addTarget:self action:@selector(rightAction:) forControlEvents:(UIControlEventTouchUpInside)];
    rightBut.tag = section+9527;
    
    UIButton *leftBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    leftBut.frame = CGRectMake(KWIDTH-16-75-32-75, 36, 75, 32);
    [leftBut setBackgroundColor:[UIColor whiteColor]];
    leftBut.layer.masksToBounds  = YES;
    leftBut.layer.cornerRadius = 4;
    leftBut.layer.borderWidth = 1;
    leftBut.layer.borderColor = zhutiColor.CGColor;
    [leftBut setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [tabFoodView addSubview:leftBut];
    rightBut.titleLabel.font = FontSize(12)    ;
    leftBut.titleLabel.font =FontSize(12)  ;
    [leftBut addTarget:self action:@selector(rleftButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    leftBut.tag = section+957;
    
    if ([model.state integerValue] == 0) {
        //待付款
        leftBut.hidden = NO;
        [leftBut setTitle:@"取消订单" forState:(UIControlStateNormal)];
        rightBut.hidden = NO;
        [rightBut setTitle:@"去付款" forState:(UIControlStateNormal)];
    }else if([model.state integerValue] == 1){
        //已付款
        leftBut.hidden = YES;
        rightBut.hidden = YES;
        //[rightBut setTitle:@"确认收货" forState:(UIControlStateNormal)];
        return [UIView new];
    }else if([model.state integerValue] == 2){
        //待评价
        //去评价
        leftBut.hidden = YES;
        rightBut.hidden = NO;
        [rightBut setTitle:@"确认收货" forState:(UIControlStateNormal)];
    }else if([model.state integerValue] == 3){
        leftBut.hidden = YES;
        rightBut.hidden = NO;
        [rightBut setTitle:@"去评价" forState:(UIControlStateNormal)];
        //0代付款1待发货2待收货3待评价4已评价
    }else if([model.state integerValue] == 4){
        ////查看评价
        leftBut.hidden = YES;
        rightBut.hidden = NO;
        [rightBut setTitle:@"查看评价" forState:(UIControlStateNormal)];
    }else if([model.state integerValue] == -1){
        //已取消
        leftBut.hidden = YES;
        rightBut.hidden = YES;
        return [UIView new];
    }
    return tabFoodView;
}

-(void)rightAction:(UIButton *)but{
    
    NSInteger ind = but.tag-9527;
    //0代付款1待发货2待收货3待评价4已评价
    
    CSorderHeaderModel *model = _myHeaderdate[ind];
    if ([model.state integerValue] == 0) {
        //待付款  -  去付款
        [self gopayWithmodel:model];
    }else if([model.state integerValue] == 1){
        //已付款--认收货
        //[self querenshouhuo:ind];
    }else if([model.state integerValue] == 2){
        //已付款--认收货
        [self querenshouhuo:ind];
    }else if([model.state integerValue] == 3){
        //待评价
        //去评价
        [self goCimmef:ind];
        //待评价
        //查看评价
        
    }else if([model.state integerValue] == 4){
        CSlookMailCimmitViewController *cimm = [[CSlookMailCimmitViewController alloc]init];
        cimm.orderid = model.Id;
        [self.navigationController pushViewController:cimm animated:YES];
    }
}

-(void)querenshouhuo:(NSInteger )ind{
    CSorderHeaderModel *model = _myHeaderdate[ind];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(model.Id);
    [NetWorkTool POST:querenShouHuo param:param success:^(id dic) {
        ShowToastWithText(@"确认成功");
        [self->_mydateSource removeAllObjects];
        [self->_myHeaderdate removeAllObjects];
        [self requeset];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)goCimmef:(NSInteger )ind{
    CSgoCommitViewController *commit = [[CSgoCommitViewController alloc]init];
    
    CSMallOrderModel *mod = [_mydateSource[ind] firstObject];
    CSorderHeaderModel *hea = _myHeaderdate[ind];
    commit.proModel = mod;
    commit.headerModel = hea;
    commit.myblock = ^(NSString * _Nonnull str) {
        [self requeset];
    };
    [self.navigationController pushViewController:commit animated:YES];
}

-(void)rleftButAction:(UIButton *)but{
    
    NSInteger ind = but.tag-957;
    
    CSorderHeaderModel *model = _myHeaderdate[ind];
    if ([model.state integerValue] == 0) {
        //待付款  -  取消订单
        //ShowToastWithText(@"取消订单");
        //CancelOrderView *cancel = [[CancelOrderView alloc]initWithFrame:[UIScreen mainScreen].bounds array:@[@"我不想买了",@"信息填写错误，重新拍",@"卖家缺货",@"其他原因"] block:^(NSString *index) {
        //    [self quxiaodingdan:model with:index];
        //    [cancel removeFromSuperview];
        //}];
        //[[UIApplication sharedApplication].keyWindow addSubview:cancel];
        UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"确定取消订单?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"取消");
        }]];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"确定");
            [self quxiaodingdan:model with:0];
        }]];
        
        [self presentViewController:alertCon animated:YES completion:nil];
    }else if([model.state integerValue] == 1){
        //已付款--
        
    }else if([model.state integerValue] == 2){
        //待评价
        [self goCimmef:ind];
    }
}

-(void)quxiaodingdan:(CSorderHeaderModel *)model with:(NSString *)str{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(model.Id);
    param[@"reason"] = str;
    
    [NetWorkTool POST:mailquxiaoOrder param:param success:^(id dic) {
        ShowToastWithText(@"取消成功");
        [self requeset];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)gopayWithmodel:(CSorderHeaderModel *)model{
    
    CSpayViewController *vc = [[CSpayViewController alloc]init];
    vc.orderSon = model.Id;
    vc.myblockj = ^(NSString * _Nonnull srr) {
        [self requeset];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray *arr = [_mydateSource safeObjectAtIndex:section];
    return arr.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _myHeaderdate.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSnewBuyOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSnewBuyOrderTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSMallOrderModel *mdeol = _mydateSource[indexPath.section][indexPath.row];
    
    [cell refashWithModel:mdeol];
    //cell.orderBlock = ^(NSString * _Nonnull ind) {
    //    ShowToastWithText(@"生成订单");
    //};
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CSMallOrderModel *mdeol = _myHeaderdate[indexPath.section];
    CSBuyDetaileViewController *deVc = [[CSBuyDetaileViewController alloc]init];
    deVc.orderstr = mdeol.Id;
    [self.navigationController pushViewController:deVc animated:YES];
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49) style:(UITableViewStyleGrouped)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSnewBuyOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSnewBuyOrderTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
        
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requeset];//请求数据
        }];
    }
    return _myTableView;
}

@end
