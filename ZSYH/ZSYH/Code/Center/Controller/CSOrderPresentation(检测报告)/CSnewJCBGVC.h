//
//  CSnewJCBGVC.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewJCBGVC : BaseViewController
@property(nonatomic,strong)CSmaintenanceListModel *model;

@end

NS_ASSUME_NONNULL_END
