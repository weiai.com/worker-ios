//
//  CSorderPresentationViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSorderPresentationViewController.h"
#import "SYBigImage.h"
@interface CSorderPresentationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *orderLB;
@property (weak, nonatomic) IBOutlet UILabel *guzhnagPruLB;
@property (weak, nonatomic) IBOutlet UILabel *guzhuangContentLB;
@property (weak, nonatomic) IBOutlet UILabel *bgimage;
@property (weak, nonatomic) IBOutlet UIImageView *myuimage;
@property (weak, nonatomic) IBOutlet UILabel *yugufeiyongLB;

@end

@implementation CSorderPresentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"检测报告";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.bgimage.userInteractionEnabled = YES;
    self.myuimage.userInteractionEnabled = YES;
    [self request];
   
}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"orderId"] = NOTNIL(_model.orderId);
    [NetWorkTool POST:lookbaogao param:param success:^(id dic) {
        KMyLog(@"查看结果%@",dic);
        NSDictionary *mudic = [dic objectForKey:@"data"];
        self.orderLB.text = [NSString stringWithFormat:@"订单编号:%@",[mudic objectForKey:@"app_order_id"]];
        self.guzhnagPruLB.text = [mudic objectForKey:@"product_name"];
        self.guzhuangContentLB.text = [mudic objectForKey:@"fault_comment"];
        self.yugufeiyongLB.text =[NSString stringWithFormat:@"%@元",[mudic objectForKey:@"cost"]];

        NSMutableArray *imagearr = [[mudic objectForKey:@"images"] mutableCopy];
    
        [self showinageWith:imagearr];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showinageWith:(NSArray *)arr{
    
    CGFloat wei = (self.bgimage.frame.size.width - 25)/4;
    
    switch (arr.count) {
        case 0:
            {
                
            }
            break;
        case 1:
        {
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[arr[0] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.centerX.offset(0);
            }];
            
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
        }
            break;
        case 2:
        {
            CGFloat fengxi = 15;
            //CGFloat wei = (self.bgimage.width - 45)/2;
            
            UIImageView *myimage = [[UIImageView alloc]init];
            myimage.userInteractionEnabled = YES;
            [self.bgimage addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[arr[0] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi);
            }];
            
            UIImageView *myimage1 = [[UIImageView alloc]init];
            myimage1.userInteractionEnabled = YES;
            [self.bgimage addSubview:myimage1];
            [myimage1 sd_setImageWithURL:[NSURL URLWithString:[arr[1] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.right.offset(-fengxi);
            }];
            SYBigImage * bigI0 = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI0];
            SYBigImage * bigI1 = [[SYBigImage alloc]init];
            [myimage1 addGestureRecognizer:bigI1];
        }
            break;
        case 3:
        {
            //CGFloat wei = (self.bgimage.frame.size.width - 20)/3;
            CGFloat fengxi = 5;
            
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[arr[0] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi);
            }];
            
            UIImageView *myimage1 = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage1];
            [myimage1 sd_setImageWithURL:[NSURL URLWithString:[arr[1] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi*2+wei*1);
            }];
            
            UIImageView *myimage2 = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage2];
            [myimage2 sd_setImageWithURL:[NSURL URLWithString:[arr[2] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi*3+wei*2);
            }];
           
            myimage.userInteractionEnabled = YES;
            myimage1.userInteractionEnabled = YES;
            myimage2.userInteractionEnabled = YES;
            
            SYBigImage * bigI0 = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI0];
            SYBigImage * bigI1 = [[SYBigImage alloc]init];
            [myimage1 addGestureRecognizer:bigI1];
            SYBigImage * bigI2 = [[SYBigImage alloc]init];
            [myimage2 addGestureRecognizer:bigI2];
        }
            break;
        case 4:
        {
            //CGFloat wei = (self.bgimage.frame.size.width - 25)/4;
            CGFloat fengxi = 5;
            
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[arr[0] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi);
            }];
            UIImageView *myimage1 = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage1];
            [myimage1 sd_setImageWithURL:[NSURL URLWithString:[arr[1] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage1 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi*2+wei);
            }];
            
            UIImageView *myimage2 = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage2];
            [myimage2 sd_setImageWithURL:[NSURL URLWithString:[arr[2] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi*3+wei*2);
            }];
            
            UIImageView *myimage3 = [[UIImageView alloc]init];
            [self.bgimage addSubview:myimage3];
            [myimage3 sd_setImageWithURL:[NSURL URLWithString:[arr[3] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            [myimage3 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.offset(0);
                make.width.height.offset(wei);
                make.left.offset(fengxi*4+wei*3);
            }];
            
            myimage.userInteractionEnabled = YES;
            myimage1.userInteractionEnabled = YES;
            myimage2.userInteractionEnabled = YES;
            myimage3.userInteractionEnabled = YES;

            SYBigImage * bigI0 = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI0];
            SYBigImage * bigI1 = [[SYBigImage alloc]init];
            [myimage1 addGestureRecognizer:bigI1];
            SYBigImage * bigI2 = [[SYBigImage alloc]init];
            [myimage2 addGestureRecognizer:bigI2];
            SYBigImage * bigI3 = [[SYBigImage alloc]init];
            [myimage3 addGestureRecognizer:bigI3];
        }
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
