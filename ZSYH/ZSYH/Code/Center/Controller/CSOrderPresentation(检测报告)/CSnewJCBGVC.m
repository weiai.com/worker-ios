//
//  CSnewJCBGVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewJCBGVC.h"
#import "SYBigImage.h"

@interface CSnewJCBGVC ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *orderLB;
@property(nonatomic,strong)UILabel *guzhnagPruLB;
@property(nonatomic,strong)UILabel *guzhuangContentLB;
@property(nonatomic,strong)UILabel *yugufeiyongLB;

@property(nonatomic,strong)NSMutableDictionary *myDic;

@end

@implementation CSnewJCBGVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"检测报告";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.myDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [self request];
    
}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"orderId"] = _model.orderId;
    [NetWorkTool POST:lookbaogao param:param success:^(id dic) {
        KMyLog(@"查看结果%@",dic);
        NSDictionary *mudic = [dic objectForKey:@"data"];
        self.myDic = (NSMutableDictionary *)mudic;
        
//        self.orderLB.text = [NSString stringWithFormat:@"订单编号:%@",[mudic objectForKey:@"app_order_id"]];
//        self.guzhnagPruLB.text = [mudic objectForKey:@"product_name"];
//        self.guzhuangContentLB.text = [mudic objectForKey:@"fault_comment"];
//        self.yugufeiyongLB.text =[NSString stringWithFormat:@"%@元",[mudic objectForKey:@"cost"]];
//
//        NSMutableArray *imagearr = [[mudic objectForKey:@"images"] mutableCopy];
//
//        [self showinageWith:imagearr];
        [self showui];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showui{
   
    [self.view addSubview:self.scrollView];
    CGFloat lef = 46;
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(16, 5, KWIDTH, 17)];
    order.text =[NSString stringWithFormat:@"订单编号:%@",[_myDic objectForKey:@"app_order_id"]];
    order.font = FontSize(12);
    order.textColor = [UIColor colorWithHexString:@"#979797"];
    [self.scrollView addSubview:order];
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 27, KWIDTH-20, KHEIGHT-27-kNaviHeight)];
    bgimage.image = imgname(@"jiancebaogaoBgView");
    [self.scrollView addSubview:bgimage];
    
    UIImageView *bgimageup = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-170)/2, order.bottom+61, 170, 146)];
    bgimageup.image = imgname(@"jiancebaogaoupview");
    [self.scrollView addSubview:bgimageup];
    
    UILabel *guzhangh = [[UILabel alloc]initWithFrame:CGRectMake(lef, bgimageup.bottom+29, 67, 14)];
    guzhangh.text =@"故障配件:";
    guzhangh.font = FontSize(12);
    guzhangh.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:guzhangh];
    
    UILabel *guzhanghlb = [[UILabel alloc]initWithFrame:CGRectMake(guzhangh.right, bgimageup.bottom+29, 67, 14)];
    guzhanghlb.text =[_myDic objectForKey:@"product_name"];
    guzhanghlb.font = FontSize(12);
    guzhanghlb.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:guzhanghlb];
    
    UILabel *guzhangms = [[UILabel alloc]initWithFrame:CGRectMake(lef, guzhangh.bottom+10, 67, 14)];
    guzhangms.text =@"故障描述:";
    guzhangms.font = FontSize(12);
    guzhangms.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:guzhangms];
    
    CGFloat fff = [NSString heightWithWidth:KWIDTH-115-46 font:12 text:[_myDic objectForKey:@"fault_comment"]];
    UILabel *guzhangmslb = [[UILabel alloc]initWithFrame:CGRectMake(guzhangh.right, guzhanghlb.bottom+10, KWIDTH-115-46, fff)];
    guzhangmslb.text =[_myDic objectForKey:@"fault_comment"];
    guzhangmslb.font = FontSize(12);
    guzhangmslb.numberOfLines = 0;
    guzhangmslb.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:guzhangmslb];
    
    UILabel *yugu = [[UILabel alloc]initWithFrame:CGRectMake(lef, guzhangms.bottom+10, 67, 14)];
    yugu.text =@"预估费用:";
    yugu.font = FontSize(12);
    yugu.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:yugu];

    UILabel *yugulb = [[UILabel alloc]initWithFrame:CGRectMake(guzhangh.right, guzhangms.bottom+10, 67, 14)];
    yugulb.text =[NSString stringWithFormat:@"%@元", [_myDic objectForKey:@"cost"]];
    yugulb.font = FontSize(12);
    yugulb.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:yugulb];
    
    UILabel *checkFeeL = [[UILabel alloc]initWithFrame:CGRectMake(lef, yugu.bottom+10, 67, 14)];
    checkFeeL.text =@"检测费用:";
    checkFeeL.font = FontSize(12);
    checkFeeL.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:checkFeeL];

    UILabel *checkFee = [[UILabel alloc]initWithFrame:CGRectMake(checkFeeL.right, yugu.bottom+10, 67, 14)];
    checkFee.text =[NSString stringWithFormat:@"%@元", [_myDic objectForKey:@"testCost"]];
    checkFee.font = FontSize(12);
    checkFee.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:checkFee];
    
    //取消是否二次上门
    /*UILabel *isSecondL = [[UILabel alloc]initWithFrame:CGRectMake(lef, checkFeeL.bottom+10, 67+20, 14)];
    isSecondL.text =@"是否二次上门:";
    isSecondL.font = FontSize(12);
    isSecondL.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:isSecondL];
    
    UILabel *isSecond = [[UILabel alloc]initWithFrame:CGRectMake(isSecondL.right, checkFeeL.bottom+10, 67, 14)];
    if ([[_myDic objectForKey:@"is_secode_service"] boolValue]) {
        isSecond.text = @"是";
    }else {
        isSecond.text = @"否";
    }
    isSecond.font = FontSize(12);
    isSecond.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:isSecond];*/
    
    NSMutableArray *imagearr = [[_myDic objectForKey:@"images"] mutableCopy];
    CGFloat wei = (self.scrollView.frame.size.width - 25-46-46)/4;
    CGFloat bot = checkFee.bottom+38;
    if (imagearr.count > 0) {
        for (int i = 0; i < imagearr.count; i++) {
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.scrollView addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[imagearr[i] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
            myimage.frame = CGRectMake(46+5+(wei+5)*i, checkFee.bottom+30, wei, wei);
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
            bot =myimage.bottom+38;
        }
    }
    
    if (bot <( KHEIGHT - 20-27-kNaviHeight)) {
        bot = KHEIGHT - 20-27-kNaviHeight;
    }
    
    //bgimage.bottom = bot;
    CGRect ffframe = bgimage.frame;
    ffframe.size.height= bot;
    bgimage.frame = ffframe;
    
    self.scrollView.contentSize = CGSizeMake(KWIDTH, bgimage.bottom + 20);
    
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
