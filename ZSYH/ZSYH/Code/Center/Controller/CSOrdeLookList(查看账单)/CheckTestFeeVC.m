//
//  CheckTestFeeVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CheckTestFeeVC.h"

@interface CheckTestFeeVC ()
@property(nonatomic,strong)UIView *bgView;
@property(nonatomic,strong)NSDictionary *mydic;
@property(nonatomic,strong)UIScrollView *scrollView;

@end

@implementation CheckTestFeeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.title = @"检测费";
    
    [self request];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    // Do any additional setup after loading the view.
}

-(void)request{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(_model.orderId);
    kWeakSelf;
    [NetWorkTool POST:lookListacc param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
        
        [weakSelf.model setValuesForKeysWithDictionary:[weakSelf.mydic objectForKey:@"order"]];
        [self showDetaileUi];
        KMyLog(@"群里发的%@",dic);
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showDetaileUi{
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, KHEIGHT-20)];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview: self.bgView];
    self.bgView.backgroundColor = zhutiColor;
    
    [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];
    
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
    order.textColor = [UIColor whiteColor];
    order.font = FontSize(16);
    order.text = @"订单编号";
    [self.bgView addSubview:order];
    
    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
    orderlb.textColor = [UIColor whiteColor];
    orderlb.font = FontSize(16);
    orderlb.text = [[_mydic objectForKey:@"order"] objectForKey:@"app_order_id"];
    [self.bgView addSubview:orderlb];
    
    UILabel *upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 100)];
    upLable.backgroundColor = [UIColor whiteColor];
    [self.bgView addSubview:upLable];
    
    UILabel *peijian = [[UILabel alloc]initWithFrame:CGRectMake(24, 32, 100, 14)];
    peijian.textColor = K666666;
    peijian.font = FontSize(14);
    peijian.text = @"配件费";
    [upLable addSubview:peijian];
    
    UILabel *peijianlb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, 32, 100, 14)];
    peijianlb.textColor = K666666;
    peijianlb.textAlignment = NSTextAlignmentRight;
    peijianlb.font = FontSize(14);
    peijianlb.text = [NSString stringWithFormat:@"¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"proCost"]];
    [upLable addSubview:peijianlb];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, peijianlb.bottom+10, KWIDTH-20, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [upLable addSubview:line];
    CGFloat h = line.bottom;
    NSArray *pruArr = [_mydic objectForKey:@"products"];
    if (pruArr.count >0) {
        
        for (int i = 0; i<pruArr.count; i++) {
            UILabel *leftLbnumber = [[UILabel alloc]initWithFrame:CGRectMake(24, h+13+(25*i), 12, 12)];
            leftLbnumber.layer.masksToBounds = YES;
            leftLbnumber.layer.cornerRadius = 6;
            leftLbnumber.layer.borderWidth = 1;
            leftLbnumber.layer.borderColor = K999999.CGColor;
            leftLbnumber.textColor =K999999;
            leftLbnumber.textAlignment = NSTextAlignmentCenter;
            leftLbnumber.font = FontSize(8);
            int tt = i+1;
            leftLbnumber.text = [NSString stringWithFormat:@"%d",tt];
            [upLable addSubview:leftLbnumber];
            
            NSDictionary *mydic = pruArr[i];
            
            UILabel *leftLb = [[UILabel alloc]initWithFrame:CGRectMake(24+17, h+13+(25*i), KWIDTH-144, 12)];
            leftLb.textColor = K999999;
            leftLb.font = FontSize(12);
            NSString *str = @"更换配件";
            if ([[mydic objectForKey:@"product_model"] integerValue] == 1) {
                str = @"添加配件";
            }
            leftLb.text =[NSString stringWithFormat:@"%@ %@ 数量*%@",str,[mydic objectForKey:@"product_name"],[mydic objectForKey:@"count"]];
            [upLable addSubview:leftLb];
            
            NSString *coun = [mydic objectForKey:@"count"];
            NSInteger indcoun = [coun integerValue];
            NSInteger pri = [[mydic objectForKey:@"product_price"] integerValue];
            NSInteger ddddd = indcoun*pri;
            UILabel *rightlb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, h+13+(25*i), 100, 12)];
            rightlb.textAlignment = NSTextAlignmentRight;
            rightlb.textColor = K999999;
            rightlb.font = FontSize(12);
            rightlb.text = [NSString stringWithFormat:@"¥ %ld",ddddd];
            [upLable addSubview:rightlb];
            
            if (i == (pruArr.count-1)) {
                h = leftLb.bottom+13;
            }
        }
    }
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, h, KWIDTH-20, 1)];
    line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [upLable addSubview:line1];
    
    UILabel *service = [[UILabel alloc]initWithFrame:CGRectMake(24, line1.bottom+16, 100, 14)];
    service.textColor = K666666;
    service.font = FontSize(14);
    service.text = @"工时费";
    [upLable addSubview:service];
    
    UILabel *servicelb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line1.bottom+16, 100, 14)];
    servicelb.textColor = K666666;
    servicelb.textAlignment = NSTextAlignmentRight;
    servicelb.font = FontSize(14);
    servicelb.text =  [NSString stringWithFormat:@"¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"peopleCost"]];
    [upLable addSubview:servicelb];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, service.bottom+10, KWIDTH-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [upLable addSubview:line2];
    
    UILabel *feiyong = [[UILabel alloc]initWithFrame:CGRectMake(24, line2.bottom+16, 100, 14)];
    feiyong.textColor = K666666;
    feiyong.font = FontSize(14);
    feiyong.text = @"费用合计";
    [upLable addSubview:feiyong];
    
    UILabel *feiyonglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line2.bottom+16, 100, 14)];
    feiyonglb.textColor = K666666;
    feiyonglb.textAlignment = NSTextAlignmentRight;
    feiyonglb.font = FontSize(14);
    feiyonglb.text =  [NSString stringWithFormat:@"¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"all_cost"]];
    [upLable addSubview:feiyonglb];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, feiyong.bottom+10, KWIDTH-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [upLable addSubview:line3];
    
    
    UILabel *myshouyi = [[UILabel alloc]initWithFrame:CGRectMake(24, line3.bottom+16, 100, 14)];
    myshouyi.textColor = zhutiColor;
    myshouyi.font = FontSize(14);
    myshouyi.text = @"本单收益";
    [upLable addSubview:myshouyi];
    
    UILabel *myshouyilb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line3.bottom+16, 100, 14)];
    myshouyilb.textColor = zhutiColor;
    myshouyilb.textAlignment = NSTextAlignmentRight;
    myshouyilb.font = FontSize(14);
    myshouyilb.text =[NSString stringWithFormat:@"¥ %.2f",[[_mydic  objectForKey:@"earning"] floatValue]];
    [upLable addSubview:myshouyilb];
    upLable.height = myshouyilb.bottom+100;
    self.bgView.height = upLable.bottom;
    
    UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(22, self.bgView.bottom+6, KWIDTH-44, 13)];
    payTime.text = [NSString stringWithFormat:@"支付时间: %@",_model.pay_time];
    payTime.textColor = [UIColor colorWithHexString:@"#232620"];
    payTime.font =FontSize(12);
    payTime.textAlignment = NSTextAlignmentRight;
    [self.scrollView addSubview:payTime];
    self.scrollView.contentSize = CGSizeMake(KWIDTH,  payTime.bottom);
    
#pragma mark ***查看收益
    if ([_model.order_state integerValue] < 3) {
        //myshouyi.hidden = YES;
        //myshouyilb.hidden = YES;
        payTime.hidden = YES;
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end

