//
//  ZYTestLookZDViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYTestLookZDViewController : BaseViewController
@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
