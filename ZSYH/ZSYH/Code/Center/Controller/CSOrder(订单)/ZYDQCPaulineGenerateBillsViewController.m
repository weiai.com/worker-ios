//
//  ZYDQCPaulineGenerateBillsViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYDQCPaulineGenerateBillsViewController.h"
#import "CSOrderContentViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "ZYAddAccessoriesViewController.h" //添加配件

@interface ZYDQCPaulineGenerateBillsViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,UITextFieldDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) BOOL isfour;//
@property (nonatomic, strong) NSString *issecont;//二次上门
@property (nonatomic, strong) NSMutableArray *imageArr;//
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) UILabel *plLB;//
@property (nonatomic, strong) UIView *addView;//
@property (nonatomic, assign) CGFloat bottomf;//
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UIButton *oldbut;//
@property (nonatomic, strong) UITextView *mytextView;//请填写产品具体故障
@property (nonatomic, strong) UILabel *billCostlb; //本单费用
@property (nonatomic, strong) UIView *butBg;
@property (nonatomic, strong) UIButton *upBut;//提交账单
@property (nonatomic, strong) UIView *bgViewsecHoubao;
@property (nonatomic, strong) UILabel *stirngLenghLabel;
@property (nonatomic, strong) UILabel *zbqConLab;

@end

@implementation ZYDQCPaulineGenerateBillsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    self.isfour = NO;
    self.issecont = @"0";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"addpicimage");
    [self.imageArr addObject:_myimage];
    
    [self showui];
    
    [self shwoBgviewsec];
    [self shwoBgviewsecHoubao];
   
}

-(void)showui{
    [self.view addSubview:self.scrollView];
    CGFloat left1 = 16;
    
    if ([_mymodel.product_source isEqualToString:@"1"]) {
        //厂商提供
        UILabel *ornumber = [[UILabel alloc]initWithFrame:CGRectMake(left1, 0, 70, 55)];
        ornumber.text = @"订单编号:";
        ornumber.textColor = K666666;
        ornumber.font = FontSize(16);
        [self.scrollView addSubview:ornumber];
        
        UILabel *ornumberlb = [[UILabel alloc]initWithFrame:CGRectMake(ornumber.right+13, 0, KWIDTH, 55)];
        ornumberlb.text = _mymodel.Id;
        ornumberlb.textColor = K666666;
        ornumberlb.font = FontSize(16);
        [self.scrollView addSubview:ornumberlb];
        
        UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, ornumber.bottom, KWIDTH, 13)];
        line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line0];
        
        UILabel *bendan = [[UILabel alloc]initWithFrame:CGRectMake(left1, line0.bottom+16, 70, 16)];
        bendan.text = @"本单费用:";
        bendan.font = FontSize(16);
        bendan.textColor = K333333;
        [self.scrollView addSubview:bendan];
        
        UILabel *billCostlb = [[UILabel alloc]initWithFrame:CGRectMake(bendan.right+13, line0.bottom+16, KWIDTH, 16)];
        NSString *fixedPStr = [NSString stringWithFormat:@"¥%.2f", [self.mymodel.fixed_price floatValue]];
        billCostlb.text = fixedPStr;
        billCostlb.textColor = [UIColor colorWithHexString:@"#70BE68"];
        billCostlb.font = FontSize(16);
        [self.scrollView addSubview:billCostlb];
        self.billCostlb = billCostlb;
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0,  billCostlb.bottom+16, KWIDTH, 2)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line1];
        
        UILabel *starLb = [[UILabel alloc]init];
        starLb.frame = CGRectMake(left1, line1.bottom + 16, 8, 14);
        [self.scrollView addSubview:starLb];
        starLb.font = FontSize(16);
        starLb.textColor = [UIColor redColor];
        starLb.textAlignment = NSTextAlignmentCenter;
        starLb.text = @"*";
        
        UILabel *winding = [[UILabel alloc]initWithFrame:CGRectMake(starLb.right+6, line1.bottom+10, KWIDTH-32, 16)];
        [self.scrollView addSubview:winding];
        winding.textColor = [UIColor colorWithHexString:@"#333333"];
        winding.font = FontSize(16);
        winding.text = @"请添加完工照片";
        
        //--------------------------------厂家提供 添加完工照片 ---------------------------------//
        self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, winding.bottom, KWIDTH, 100)];
        [self.scrollView addSubview: self.addView];
        CGFloat www = (KWIDTH -62)/4;
        self.bottomf = winding.bottom;
        [self showinageWirhArr:_imageArr];
        
        CGRect ffff = self.addView.frame;
        ffff.size.height = www +20;
        self.addView.frame = ffff;
        
        //--------------------------------厂家提供 添加完工照片 ---------------------------------//
        UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(0,self.addView.bottom, KWIDTH, 2)];
        line5.alpha = 0.f;
        line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line5];
        
        CGFloat heigehtbg = KHEIGHT -line5.bottom - 16;
        
        if (heigehtbg < 147) {
            heigehtbg = 147;
        }
        
        UIView *butBg = [[UIView alloc]initWithFrame:CGRectMake(0, line5.bottom, KWIDTH, heigehtbg)];
        butBg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        self.butBg = butBg;
        [self.scrollView addSubview:butBg];
        butBg.userInteractionEnabled = YES;
        
        UIButton *upBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [upBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
        [upBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [upBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        upBut.frame = CGRectMake(28, 42, KWIDTH-28-28, 48);
        upBut.layer.masksToBounds = YES;
        upBut.layer.cornerRadius = 4;
        [butBg addSubview:upBut];
        [upBut addTarget:self action:@selector(upAciotn:) forControlEvents:(UIControlEventTouchUpInside)];
        _scrollView.contentSize = CGSizeMake(KWIDTH, butBg.bottom);
        self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        
    } else if ([_mymodel.product_source isEqualToString:@"2"]) {
        //师傅自带
        UILabel *ornumber = [[UILabel alloc]initWithFrame:CGRectMake(left1, 0, 70, 55)];
        ornumber.text = @"订单编号:";
        ornumber.textColor = K666666;
        ornumber.font = FontSize(16);
        [self.scrollView addSubview:ornumber];
        
        UILabel *ornumberlb = [[UILabel alloc]initWithFrame:CGRectMake(ornumber.right+13, 0, KWIDTH, 55)];
        ornumberlb.text = _mymodel.Id;
        ornumberlb.textColor = K666666;
        ornumberlb.font = FontSize(16);
        [self.scrollView addSubview:ornumberlb];
        
        UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, ornumber.bottom, KWIDTH, 13)];
        line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line0];
        
        UILabel *bendan = [[UILabel alloc]initWithFrame:CGRectMake(left1, line0.bottom+16, 70, 16)];
        bendan.text = @"本单费用:";
        bendan.font = FontSize(16);
        bendan.textColor = K333333;
        [self.scrollView addSubview:bendan];
        
        UILabel *billCostlb = [[UILabel alloc]initWithFrame:CGRectMake(bendan.right+13, line0.bottom+16, KWIDTH, 16)];
        NSString *fixedPStr = [NSString stringWithFormat:@"¥%.2f", [self.mymodel.fixed_price floatValue]];
        billCostlb.text = fixedPStr;
        billCostlb.textColor = [UIColor colorWithHexString:@"#70BE68"];
        billCostlb.font = FontSize(16);
        [self.scrollView addSubview:billCostlb];
        self.billCostlb = billCostlb;
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0,  billCostlb.bottom+16, KWIDTH, 2)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line1];
        
        //质保期 标题
        UILabel *zbqTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, line1.bottom, 70, 48)];
        zbqTitLab.text = @"质保期";
        zbqTitLab.font = FontSize(16);
        zbqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
        [self.scrollView addSubview:zbqTitLab];
        
        //质保期 输入框
        UILabel *zbqConLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, line1.bottom +8, 60, 32)];
        zbqConLab.font = FontSize(16);
        zbqConLab.textColor = [UIColor colorWithHexString:@"#333333"];
        zbqConLab.textAlignment = NSTextAlignmentLeft;
        [self.scrollView addSubview:zbqConLab];
        self.zbqConLab = zbqConLab;
        //添加点击事件
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelClick)];
        [zbqConLab addGestureRecognizer:labelTapGestureRecognizer];
        zbqConLab.userInteractionEnabled = YES;
        
        //输入框 下划线
        UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, zbqConLab.bottom, 60, 1)];
        bottomLine.backgroundColor = [UIColor colorWithHexString:@"#BBBBBB"];
        [self.scrollView addSubview:bottomLine];
        
        //质保期 多少个月
        UILabel *monthLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqConLab.right, line1.bottom, 50, 48)];
        monthLab.text = @"个月";
        monthLab.font = FontSize(16);
        monthLab.textColor = [UIColor colorWithHexString:@"#666666"];
        [self.scrollView addSubview:monthLab];
        
        UILabel *line8 = [[UILabel alloc] initWithFrame:CGRectMake(0, zbqTitLab.bottom, KWIDTH, 2)];
        line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line8];
        
        UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+16, KWIDTH-32, 16)];
        guzhang.text = @"请输入配件名称 (如果没有请填写无)";
        guzhang.font = FontSize(16);
        guzhang.textColor = K333333;
        [self.scrollView addSubview:guzhang];
        
        self.mytextView = [[UITextView alloc]initWithFrame:CGRectMake(left1, guzhang.bottom+10, KWIDTH-16-16, 124)];
        self.mytextView.font = FontSize(14);
        self.mytextView.layer.masksToBounds = YES;
        self.mytextView.layer.cornerRadius = 4;
        self.mytextView.layer.borderWidth = 1;
        self.mytextView.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        //[self.mytextView becomeFirstResponder];
        [self.scrollView addSubview:self.mytextView];
        self.mytextView.delegate = self;
        
        UILabel *plLB = [[UILabel alloc]initWithFrame:CGRectMake(5, 11, KWIDTH-32-10, 14)];
        plLB.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
        plLB.text = @"请输入配件名称";
        plLB.font = FontSize(14);
        self.plLB = plLB;
        [self.mytextView addSubview:plLB];
        
        UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.mytextView.bottom+10, KWIDTH, 2)];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line2];
        
        UILabel *starLab2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+10, 8, 16)];
        starLab2.text = @"*";
        starLab2.font = FontSize(16);
        starLab2.textColor = [UIColor colorWithHexString:@"#FF000A"];
        [self.scrollView addSubview:starLab2];
        
        UILabel *winding = [[UILabel alloc]initWithFrame:CGRectMake(starLab2.right+1, line2.bottom+10, KWIDTH-32, 16)];
        [self.scrollView addSubview:winding];
        winding.textColor = [UIColor colorWithHexString:@"#333333"];
        winding.font = FontSize(16);
        winding.text = @"请添加完工照片";
        
        //--------------------------------师傅自带 添加完工照片 ---------------------------------//
        self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, winding.bottom, KWIDTH, 100)];
        [self.scrollView addSubview: self.addView];
        CGFloat www = (KWIDTH -62)/4;
        self.bottomf = winding.bottom;
        [self showinageWirhArr:_imageArr];
        
        CGRect ffff = self.addView.frame;
        ffff.size.height = www+10;
        self.addView.frame = ffff;
        
        //--------------------------------师傅自带 添加完工照片 ---------------------------------//
        UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(0,self.addView.bottom+10, KWIDTH, 2)];
        line5.alpha = 0.f;
        line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line5];
        
        CGFloat heigehtbg = KHEIGHT -line5.bottom - 16;
        
        if (heigehtbg < 147) {
            heigehtbg = 147;
        }
        
        UIView *butBg = [[UIView alloc]initWithFrame:CGRectMake(0, line5.bottom, KWIDTH, heigehtbg)];
        self.butBg = butBg;
        [self.scrollView addSubview:butBg];
        butBg.userInteractionEnabled = YES;
        butBg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        
        UIButton *upBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [upBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
        [upBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [upBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
        upBut.frame = CGRectMake(28, 42, KWIDTH-28-28, 48);
        upBut.layer.masksToBounds = YES;
        upBut.layer.cornerRadius = 4;
        [butBg addSubview:upBut];
        [upBut addTarget:self action:@selector(upAciotn:) forControlEvents:(UIControlEventTouchUpInside)];
        _scrollView.contentSize = CGSizeMake(KWIDTH, butBg.bottom);
        self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    }
}

-(void)labelClick {
   [self creatActionSheet];
}

-(void)creatActionSheet {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"选择月份" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"3" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"3";
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"6" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"6";

    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"12" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"12";

    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"取消");
    }];
    //把action添加到actionSheet里
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [actionSheet addAction:action4];
    //相当于之前的[actionSheet show];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - 提交账单
-(void)upAciotn:(UIButton *)but{
    [self updatedateile];
}

-(void)updatedateile{
    if ([_mymodel.product_source isEqualToString:@"1"]) {//厂家提供
        
        if (strIsEmpty(_zbqConLab.text)) {
            ShowToastWithText(@"请选择质保期");
            return;
        }
        
        if (self.imageArr.count - 1 != _mymodel.number.integerValue) {
            NSString *tip = [NSString stringWithFormat:@"请添加%ld张完工照片", _mymodel.number.integerValue];
            ShowToastWithText(tip);
            return;
        }
    }
    if ([_mymodel.product_source isEqualToString:@"2"]) { //师傅自带
        if (strIsEmpty(_zbqConLab.text)) {
            ShowToastWithText(@"请选择质保期");
            return;
        } else if (strIsEmpty(_mytextView.text)) {
            ShowToastWithText(@"请输入配件名称");
            return;
        }
    }
    UIImageView *myyy = [self.imageArr lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }
    
    //推送账单
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *string = self.billCostlb.text;
    NSString *string1 = [string substringFromIndex:1];
    param[@"peopleCost"] = string1; //工时费
    param[@"orderId"] = _mymodel.orderId; //维修订单id
    param[@"type"] = @"2";
    param[@"productCost"] = @"0"; //添加配件的信息集合
    param[@"prodcutComment"] = self.mytextView.text;
    
    if ([_mymodel.product_source isEqualToString:@"2"]) { //师傅自带
        param[@"warranty"] = self.zbqConLab.text;
    }
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    allDic[@"data"] = json;
    if (_imageArr.count>0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NetWorkTool UploadPicWithUrl:OrderCreateBill param:allDic key:@"images" image:_imageArr withSuccess:^(id dic) {
            KMyLog(@"______推送师傅自带生成账单______%@",dic);
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
            if ([dic[@"errorCode"] intValue]==0) {
                if ([self.mymodel.product_source isEqualToString:@"1"]) { //厂商提供
                    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];
                } else if ([self.mymodel.product_source isEqualToString:@"2"]) { //师傅自带
                    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecHoubao];
                }
            } else {
                [NetWorkTool showOnleText:dic[@"msg"] delay:3];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
        }];
        NSLog(@"%@", param);
        NSLog(@"%@", allDic);
    } else {
        ShowToastWithText(@"请添加完工照片");
        [self.imageArr addObject:self.myimage];
    }
}

/**
 弹出框的背景图 账单推送成功
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

/**
 弹出框的背景图 添加配件
 */
-(void)shwoBgviewsecHoubao{
    self.bgViewsecHoubao = [[UIView alloc]init];
    self.bgViewsecHoubao.frame = self.view.bounds;
    self.bgViewsecHoubao.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecHoubao addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
//    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 21, 40, 40)];
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 41, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
//    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 79, KWIDTH-44, 25)];
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 99, KWIDTH-44, 25)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, KWIDTH-22*2, 63)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.numberOfLines = 0;
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    //DowLable.text = @"如果您此次更换的有补贴工时费的候保配\n件，请点击“去安装”，没有更换候保配件请\n点击“结束”。";
    
    UIButton *leftButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftButton setTitle:@"结束" forState:(UIControlStateNormal)];
    [leftButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:1]];
    leftButton.layer.masksToBounds = YES;
    leftButton.layer.cornerRadius = 4;
    [leftButton addTarget:self action:@selector(leftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftButton];
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        //make.left.offset(28);
        //make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.left.offset(58);
        make.right.offset(-58);
        make.bottom.offset(-18);
    }];
    
    UIButton *rightButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [rightButton setTitle:@"去安装" forState:(UIControlStateNormal)];
    [rightButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [rightButton setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    rightButton.layer.masksToBounds = YES;
    rightButton.layer.cornerRadius = 4;
    [rightButton addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //[whiteBGView addSubview:rightButton];
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-18);
    }];
}

//结束 按钮点击事件处理
-(void)leftBtnAction:(UIButton *)but{
    [_bgViewsecHoubao removeFromSuperview];
    
    [singlTool shareSingTool].needReasfh = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

//去安装 按钮点击事件处理
-(void)rightBtnAction:(UIButton *)but{
    [_bgViewsecHoubao removeFromSuperview];
    
    ZYAddAccessoriesViewController *vc = [[ZYAddAccessoriesViewController alloc] init];
    vc.mymodel = self.mymodel;
    vc.orderId = self.mymodel.orderId;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind] removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr {
    [self.addView removeAllSubviews];
    CGFloat www = (KWIDTH -62)/4;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(16+(www+10)* (i % 4), self.bottomf+10 + (www + 10) * (i / 4), www, www);
        [self.scrollView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            CGFloat height = KHEIGHT - (image.bottom + 20) - 16;
            if (height < 147) {
                height = 147;
            }
            self.butBg.frame = CGRectMake(0, image.bottom + 20, KWIDTH, height);
            self.scrollView.contentSize = CGSizeMake(kScreen_Width, self.butBg.bottom);
            //最大的图片数量
            NSInteger maxImageNum = _mymodel.number.integerValue;
            if ([_mymodel.product_source isEqualToString:@"2"]) {//师傅自带
                maxImageNum = 9;
            } else if ([_mymodel.product_source isEqualToString:@"1"]) {//厂家提供
                maxImageNum = _mymodel.number.integerValue;
            }
            if (i == maxImageNum) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        } else {
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

-(void)isAction:(UIButton *)button {
    [self.oldbut setImage:imgname(@"shopCar_normal") forState:(UIControlStateNormal)];
    [button setImage:imgname(@"xuanze") forState:(UIControlStateNormal)];
    self.oldbut  = button;
    self.issecont = @"1";
}

-(void)noAction:(UIButton *)button {
    [self.oldbut setImage:imgname(@"shopCar_normal") forState:(UIControlStateNormal)];
    [button setImage:imgname(@"xuanze") forState:(UIControlStateNormal)];
    self.oldbut  = button;
    self.issecont = @"0";
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.plLB.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length < 1) {
        self.plLB.hidden = NO;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    KMyLog(@"%@", textView.text);
    
    if (self.mytextView == textView) {
        self.plLB.hidden = YES;
        //实时显示字数
        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
        //字数限制操作
        if (textView.text.length >= 80) {
            textView.text = [textView.text substringToIndex:80];
            self.stirngLenghLabel.text = @"80/80";
        }
        //取消按钮点击权限，并显示提示文字
        if (textView.text.length == 0) {
            self.plLB.hidden = NO;
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
