//
//  ZYTestOrderViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYTestOrderViewController : BaseViewController
@property(nonatomic,copy)NSString *from;

@end

NS_ASSUME_NONNULL_END
