//
//  CSWXPersonDetaileViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//  售后维修单 查看服务单详情

#import "CSWXPersonDetaileViewController.h"
#import "CSmaintenanceListModel.h"
#import "CSWXPersonDetaileViewController.h"
#import "CSnewJCBGVC.h"
#import "LooksevCimmitViewController.h"
#import "CSmakeAnzhuangViewController.h"
#import "FillDateViewController.h"
#import "ZYGenerateBillsViewController.h"
#import "ZYDQCPaulineGenerateBillsViewController.h"
#import "ZYGenerateBillingDetailsController.h"
#import "ZYDQCBWCKZDViewController.h"         //电器厂保外查看账单
#import "ZYDQCBNSFZDCKZDViewController.h"     //电器厂保内师傅自带查看账单
#import "ZYDQCBNCJTGCKZDViewController.h"     //电器厂保内厂家提供查看账单
#import "ZYPersonServiceCKZDViewController.h" //个人维修订单查看账单
#import "ZOrderDetailPartDetailVC.h"          //配件详情
#import "ZYDQCDQAZCKZDViewController.h"       //电器厂电器安装查看账单

@interface CSWXPersonDetaileViewController ()
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIButton *onebut;
@property(nonatomic,strong)UIButton *twobut;
@property(nonatomic,strong)UIButton *threebut;
@property(nonatomic,strong)UIButton *fourbut;

@end

@implementation CSWXPersonDetaileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"服务单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    if (strIsEmpty(_orderID)) {
        [self showui];
    } else {
        [self requeest];
    }
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_scrollView) {
        [self requeest];
    }
}

-(void)requeest{
    
    _mymodel = [[CSmaintenanceListModel alloc]init];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:(NSUInteger)1];
    param[@"orderId"] = NOTNIL(_orderID);
    kWeakSelf;
    
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        KMyLog(@"售后维修 服务单详情 %@", dic);
        weakSelf.mymodel = [CSmaintenanceListModel mj_objectWithKeyValues:[dic objectForKeyNotNil:@"data"]];
        if (strIsEmpty(weakSelf.mymodel.orderId)) {
            weakSelf.mymodel.orderId = weakSelf.mymodel.rep_order_id;
        }
        [self showui];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showui{
    [self.view addSubview:self.scrollView];
    [self.scrollView removeAllSubviews];
    
    CGFloat wei = 60;
    CGFloat left1 = 26;
    CGFloat left2 = 14;
    
    UIView *bbgvieww = [[UIView alloc]init];
    [self.scrollView addSubview:bbgvieww];
    
    if ([self.mymodel.rep_order_state integerValue] == 6) {
        UIImageView *redIV = [UIImageView new];
        redIV.frame = CGRectMake(0, 0, ScreenW-20, 73);
        redIV.image = [UIImage imageNamed:@"路径 1829"];
        [bbgvieww addSubview:redIV];
    }
    
    CGFloat butwir = 79;
    CGFloat fengxi = (KWIDTH -20 - (butwir*4))/5;
    
    NSArray *titleArr = @[@"待评价",@"检查报告",@"查看账单",@"生成账单"];
    for (int i = 0; i<4; i++) {
        UIButton *mybut = [UIButton buttonWithType:UIButtonTypeCustom];
        mybut.frame = CGRectMake(fengxi+(butwir+fengxi)*i, 36, butwir, 26);
        [mybut setTitle:titleArr[i] forState:(UIControlStateNormal)];
        mybut.titleLabel.font = FontSize(12);
        mybut.tag = 9527+i;
        [mybut addTarget:self action:@selector(fourAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [bbgvieww addSubview:mybut];
        switch (i) {
            case 0:
            {
                self.onebut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#F88B1F"] forState:(UIControlStateNormal)];
                mybut.y = 10;
                mybut.x = 16;
                mybut.width = 60;
            }
                break;
            case 1:
            {
                self.twobut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 2:
            {
                self.threebut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 3:
            {self.fourbut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:(UIControlStateNormal)];
                [mybut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                //mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                //mybut.layer.borderWidth = 1;
            }
                break;
            default:
                break;
        }
    }
    
    [self returnisJCBG:_mymodel];
    
    UILabel *lable16 = [[UILabel alloc]initWithFrame:CGRectMake(0, 73, KWIDTH-20, 2)];
    [bbgvieww addSubview:lable16];
    lable16.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    CGFloat hh = 20+lable16.bottom;
    
    if ([_mymodel.user_type integerValue] != 0) {
        //厂商联系电话
        //        UILabel *cslxdhTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, hh, 90, 20)];
        //        cslxdhTitLab.text = @"厂商联系电话";
        //        cslxdhTitLab.font = FontSize(14);
        //        cslxdhTitLab.textColor = K999999;
        //        [self.scrollView addSubview:cslxdhTitLab];
        //
        //        UIImageView *tellPhone = [[UIImageView alloc] initWithFrame:CGRectMake(KWIDTH -163, hh-5, 30, 30)];
        //        [tellPhone setImage:imgname(@"orginphone")];
        //        [self.scrollView addSubview:tellPhone];
        //
        //        UILabel *phoneNumLab = [[UILabel alloc] initWithFrame:CGRectMake(tellPhone.right +5, hh, 105, 20)];
        //        phoneNumLab.text = _mymodel.company_phone;
        //        phoneNumLab.font = FontSize(14);
        //        phoneNumLab.textColor = K666666;
        //        phoneNumLab.textAlignment = NSTextAlignmentRight;
        //        [self.scrollView addSubview:phoneNumLab];
        //
        //        UILabel *line100 = [[UILabel alloc]initWithFrame:CGRectMake(0, cslxdhTitLab.bottom+11, KWIDTH-20, 2)];
        //        line100.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        //        [self.scrollView addSubview:line100];
        //
        //企业用户
        UILabel *complay = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
        complay.font = FontSize(14);
        complay.textColor = K999999;
        complay.text = @"单位名称";
        if ([_mymodel.user_type integerValue] == 2) {
            complay.text = @"厂商名称";
        }
        [self.scrollView addSubview:complay];
        
        UILabel *complaylb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
        complaylb.font = FontSize(14);
        complaylb.textColor = KshiYellow; //高亮
        complaylb.text = _mymodel.company_name;
        [self.scrollView addSubview:complaylb];
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(left1, complay.bottom+10, KWIDTH-left1-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line1];
        hh = line1.bottom+8;
    }
    
    UILabel *secvice = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
    secvice.font = FontSize(14);
    secvice.textColor = K999999;
    secvice.text = @"服务类型";
    [self.scrollView addSubview:secvice];
    
    UILabel *secvicelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
    secvicelb.font = FontSize(14);
    secvicelb.textColor = K666666;
    secvicelb.text = _mymodel.service_name;
    [self.scrollView addSubview:secvicelb];
    
    if ([_mymodel.user_type integerValue] == 2) {
        UILabel *baonei = [[UILabel alloc]initWithFrame:CGRectMake(secvicelb.right-5, hh, 100, 14)];
        baonei.font = FontSize(14);
        baonei.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        if ([_mymodel.fault_type isEqualToString:@"3"]) {
            if ([_mymodel.is_protect boolValue]) {
                baonei.text = @"保内";
            }else {
                baonei.text = @"保外";
            }
        }else {
            baonei.text = @"";
        }
        [self.scrollView addSubview:baonei];
    }
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, secvicelb.bottom+10, KWIDTH-left1-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line2];
    
    UILabel *produrt = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+8, wei, 14)];
    produrt.font = FontSize(14);
    produrt.textColor = K999999;
    produrt.text = @"电器品类";
    [self.scrollView addSubview:produrt];
    
    CGFloat widd = [NSString widthWithFont:14 text:_mymodel.fault_name];
    UILabel *produrtlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line2.bottom+8,widd, 14)];
    produrtlb.font = FontSize(14);
    produrtlb.textColor = K666666;
    produrtlb.text = _mymodel.fault_name;
    [self.scrollView addSubview:produrtlb];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left1, produrt.bottom+10, KWIDTH-left1-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line3];
    
    //电器厂
    if ([_mymodel.user_type integerValue] == 2) {
        if (_mymodel.order_date.length > 0) {
            //维修增加故障原因
            if ([self.mymodel.fault_type isEqualToString:@"3"]) { //电器厂维修订单
                UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
                faultReasonL.font = FontSize(14);
                faultReasonL.textColor = K999999;
                faultReasonL.text = @"故障原因";
                [self.scrollView addSubview:faultReasonL];
                
                UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
                faultReason.font = FontSize(14);
                faultReason.textColor = K666666;
                faultReason.text = _mymodel.common_failures;
                [self.scrollView addSubview:faultReason];
                
                UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
                line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                heig = 14;
                
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
                yuyueTime.font = FontSize(14);
                yuyueTime.textColor = K999999;
                yuyueTime.text = @"上门日期";
                [self.scrollView addSubview:yuyueTime];
                
                UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb.font = FontSize(14);
                yuyueTimelb.textColor = K666666;
                yuyueTimelb.text = _mymodel.order_date;
                
                NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
                if (muarrt.count>2) {
                    NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                    NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                    NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                    yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                }
                [self.scrollView addSubview:yuyueTimelb];
                
                UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line5];
                
                UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
                yuyueTime2.font = FontSize(14);
                yuyueTime2.textColor = K999999;
                yuyueTime2.text = @"上门时间";
                [self.scrollView addSubview:yuyueTime2];
                
                UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb2.font = FontSize(14);
                yuyueTimelb2.textColor = K666666;
                NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
                NSString *ap = indd < 12 ? @"上午":@"下午";
                yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
                [self.scrollView addSubview:yuyueTimelb2];
                
                UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line6];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = FontSize(14);
                addresslb.textColor = K999999;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
                name.font = FontSize(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
                phone.font = FontSize(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentLeft;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = FontSize(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = FontSize(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                if ([self.mymodel.is_protect isEqualToString:@"0"]) { //保外
                    UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, louceng.bottom+10, KWIDTH-left1-20, 1)];
                    line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line10];
                    
                    UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                    ordernumLB.font = FontSize(12);
                    ordernumLB.textColor = K999999;
                    ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                    [bbgvieww addSubview:ordernumLB];
                    
                    //配件是否到货
                    if ([_mymodel.confirmation_product boolValue]) {
                        
                        UILabel *pjdhLine = [[UILabel alloc]initWithFrame:CGRectMake(10, ordernumLB.bottom+20, KWIDTH-20, 2)];
                        pjdhLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhLine];
                        
                        UILabel *pjdh = [[UILabel alloc]initWithFrame:CGRectMake(left1+15, pjdhLine.bottom+20, 180, 14)];
                        pjdh.font = FontSize(12);
                        pjdh.textColor = [UIColor redColor];
                        pjdh.text = [NSString stringWithFormat:@"配件已到货"];
                        [self.scrollView addSubview:pjdh];
                        
                        UILabel *pjdhBotLine = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdh.bottom+10, KWIDTH-left1-20, 1)];
                        pjdhBotLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhBotLine];
                        
                        UILabel *nyuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdhBotLine.bottom+8, 90, 14)];
                        nyuyueTime.font = FontSize(14);
                        nyuyueTime.textColor = K999999;
                        nyuyueTime.text = @"新预约日期";
                        [self.scrollView addSubview:nyuyueTime];
                        
                        UILabel *nyuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, pjdhBotLine.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb.font = FontSize(14);
                        nyuyueTimelb.textColor = K666666;
                        nyuyueTimelb.text = _mymodel.subscribe_date;
                        
                        NSMutableArray *nmuarrt =[NSMutableArray arrayWithArray:[_mymodel.subscribe_date componentsSeparatedByString:@"-"]];
                        if (nmuarrt.count>2) {
                            NSString *yesr =[NSString stringWithFormat:@"%ld",[nmuarrt[0] integerValue]];
                            NSString *mouth =[NSString stringWithFormat:@"%ld",[nmuarrt[1] integerValue]];
                            NSString *day =[NSString stringWithFormat:@"%ld",[nmuarrt[2] integerValue]];
                            
                            nyuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                        }
                        [self.scrollView addSubview:nyuyueTimelb];
                        
                        UILabel *nline5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                        nline5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline5];
                        
                        UILabel *nyuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nline5.bottom+8, 90, 14)];
                        nyuyueTime2.font = FontSize(14);
                        nyuyueTime2.textColor = K999999;
                        nyuyueTime2.text = @"新预约时间";
                        [self.scrollView addSubview:nyuyueTime2];
                        
                        UILabel *nyuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, nline5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb2.font = FontSize(14);
                        nyuyueTimelb2.textColor = K666666;
                        NSInteger  nindd = [[_mymodel.subscribe_time substringToIndex:2] integerValue];
                        NSString *nap = nindd < 12 ? @"上午":@"下午";
                        nyuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", nap, _mymodel.subscribe_time];
                        [self.scrollView addSubview:nyuyueTimelb2];
                        
                        UILabel *nline6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                        nline6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline6];
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, nline6.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                        
                    } else {
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    }
                } else if ([self.mymodel.is_protect isEqualToString:@"1"]) { //保内
                    
                    UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
                    line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line8];
                    
                    UILabel *pjlyL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+7, wei, 14)];
                    pjlyL.font = FontSize(14);
                    pjlyL.textColor = K999999;
                    pjlyL.text = @"配件来源";
                    [self.scrollView addSubview:pjlyL];
                    
                    UILabel *pjly = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+7, 130, 14)];
                    pjly.font = FontSize(14);
                    pjly.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                    if ([_mymodel.product_source isEqualToString:@"1"]) {
                        pjly.text = @"厂商提供所需配件";
                    } else {
                        pjly.text = @"师傅自带所需配件";
                    }
                    [self.scrollView addSubview:pjly];
                    
                    UILabel *line9 = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjly.bottom+10, KWIDTH-left1-20, 1)];
                    line9.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line9];
                    
                    UILabel *ykjL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line9.bottom+7, wei, 14)];
                    ykjL.font = FontSize(14);
                    ykjL.textColor = K999999;
                    ykjL.text = @"一口价"; ///一口价///
                    [self.scrollView addSubview:ykjL];
                    
                    UILabel *ykj = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line9.bottom+7, 130, 14)];
                    ykj.font = FontSize(14);
                    ykj.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                    ykj.text = [NSString stringWithFormat:@"¥%.2f",[_mymodel.fixed_price floatValue]];
                    [self.scrollView addSubview:ykj];
                    
                    UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, ykj.bottom+10, KWIDTH-left1-20, 1)];
                    line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line10];
                    
                    UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                    ordernumLB.font = FontSize(12);
                    ordernumLB.textColor = K999999;
                    ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                    [bbgvieww addSubview:ordernumLB];
                    
                    //配件是否到货
                    if ([_mymodel.confirmation_product boolValue]) {
                        UILabel *pjdhLine = [[UILabel alloc]initWithFrame:CGRectMake(10, ordernumLB.bottom+20, KWIDTH-20, 2)];
                        pjdhLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhLine];
                        
                        UILabel *pjdh = [[UILabel alloc]initWithFrame:CGRectMake(left1+15, pjdhLine.bottom+20, 180, 14)];
                        pjdh.font = FontSize(12);
                        pjdh.textColor = [UIColor redColor];
                        pjdh.text = [NSString stringWithFormat:@"配件已到货"];
                        [self.scrollView addSubview:pjdh];
                        
                        UILabel *pjdhBotLine = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdh.bottom+10, KWIDTH-left1-20, 1)];
                        pjdhBotLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhBotLine];
                        
                        UILabel *nyuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdhBotLine.bottom+8, 90, 14)];
                        nyuyueTime.font = FontSize(14);
                        nyuyueTime.textColor = K999999;
                        nyuyueTime.text = @"新预约日期";
                        [self.scrollView addSubview:nyuyueTime];
                        
                        UILabel *nyuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, pjdhBotLine.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb.font = FontSize(14);
                        nyuyueTimelb.textColor = K666666;
                        nyuyueTimelb.text = _mymodel.subscribe_date;
                        
                        NSMutableArray *nmuarrt =[NSMutableArray arrayWithArray:[_mymodel.subscribe_date componentsSeparatedByString:@"-"]];
                        if (nmuarrt.count>2) {
                            NSString *yesr =[NSString stringWithFormat:@"%ld",[nmuarrt[0] integerValue]];
                            NSString *mouth =[NSString stringWithFormat:@"%ld",[nmuarrt[1] integerValue]];
                            NSString *day =[NSString stringWithFormat:@"%ld",[nmuarrt[2] integerValue]];
                            nyuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                        }
                        [self.scrollView addSubview:nyuyueTimelb];
                        
                        UILabel *nline5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                        nline5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline5];
                        
                        UILabel *nyuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nline5.bottom+8, 90, 14)];
                        nyuyueTime2.font = FontSize(14);
                        nyuyueTime2.textColor = K999999;
                        nyuyueTime2.text = @"新预约时间";
                        [self.scrollView addSubview:nyuyueTime2];
                        
                        UILabel *nyuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, nline5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb2.font = FontSize(14);
                        nyuyueTimelb2.textColor = K666666;
                        NSInteger  nindd = [[_mymodel.subscribe_time substringToIndex:2] integerValue];
                        NSString *nap = nindd < 12 ? @"上午":@"下午";
                        nyuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", nap, _mymodel.subscribe_time];
                        [self.scrollView addSubview:nyuyueTimelb2];
                        
                        UILabel *nline6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                        nline6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline6];
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, nline6.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    } else {
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    }
                }
            } else {
                
                //电器厂安装订单 服务单详情
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                heig = 14;
                
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
                yuyueTime.font = FontSize(14);
                yuyueTime.textColor = K999999;
                yuyueTime.text = @"上门日期";
                [self.scrollView addSubview:yuyueTime];
                
                UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb.font = FontSize(14);
                yuyueTimelb.textColor = K666666;
                yuyueTimelb.text = _mymodel.order_date;
                
                NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
                if (muarrt.count>2) {
                    NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                    NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                    NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                    yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                }
                [self.scrollView addSubview:yuyueTimelb];
                
                UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line5];
                
                UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
                yuyueTime2.font = FontSize(14);
                yuyueTime2.textColor = K999999;
                yuyueTime2.text = @"上门时间";
                [self.scrollView addSubview:yuyueTime2];
                
                UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb2.font = FontSize(14);
                yuyueTimelb2.textColor = K666666;
                NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
                NSString *ap = indd < 12 ? @"上午":@"下午";
                yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
                [self.scrollView addSubview:yuyueTimelb2];
                
                UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line6];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
                
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = FontSize(14);
                addresslb.textColor = K999999;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
                name.font = FontSize(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
                phone.font = FontSize(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentLeft;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = FontSize(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = FontSize(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
                line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line8];
                
                UILabel *ykjL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+7, wei, 14)];
                ykjL.font = FontSize(14);
                ykjL.textColor = K999999;
                ykjL.text = @"一口价"; ///一口价
                [self.scrollView addSubview:ykjL];
                
                UILabel *ykj = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+7, 130, 14)];
                ykj.font = FontSize(14);
                ykj.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                ykj.text = [NSString stringWithFormat:@"¥%.2f",[_mymodel.fixed_price floatValue]];
                [self.scrollView addSubview:ykj];
                
                UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, ykj.bottom+10, KWIDTH-left1-20, 1)];
                line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line10];
                
                UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                ordernumLB.font = FontSize(12);
                ordernumLB.textColor = K999999;
                ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                [bbgvieww addSubview:ordernumLB];
                
                bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                bbgvieww.backgroundColor = [UIColor whiteColor];
                [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            }
        } else {
            //维修增加故障原因
            if ([self.mymodel.fault_type isEqualToString:@"3"]) { //电器厂维修订单 服务单详情
                
                UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
                faultReasonL.font = FontSize(14);
                faultReasonL.textColor = K999999;
                faultReasonL.text = @"故障原因";
                [self.scrollView addSubview:faultReasonL];
                
                UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
                faultReason.font = FontSize(14);
                faultReason.textColor = K666666;
                faultReason.text = _mymodel.common_failures;
                [self.scrollView addSubview:faultReason];
                
                UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
                line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                
                heig = 14;
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
                
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line4.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = FontSize(14);
                addresslb.textColor = K999999;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
                name.font = FontSize(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
                phone.font = FontSize(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentLeft;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = FontSize(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = FontSize(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+9, KWIDTH-left1-20, 1)];
                line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line10];
                
                UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                ordernumLB.font = FontSize(12);
                ordernumLB.textColor = K999999;
                ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                [bbgvieww addSubview:ordernumLB];
                bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                bbgvieww.backgroundColor = [UIColor whiteColor];
                [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                
            } else {
                
                UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
                number.font = FontSize(14);
                number.textColor = K999999;
                number.text = @"报修数量";
                [self.scrollView addSubview:number];
                
                UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
                numberlb.font = FontSize(14);
                numberlb.textColor = K666666;
                numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
                [self.scrollView addSubview:numberlb];
                
                UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
                line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line44];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                
                heig = 14;
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
                
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line4.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = FontSize(14);
                addresslb.textColor = K999999;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
                name.font = FontSize(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
                phone.font = FontSize(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentLeft;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = FontSize(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = FontSize(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
                line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line8];
                
                UILabel *ykjL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+7, wei, 14)];
                ykjL.font = FontSize(14);
                ykjL.textColor = K999999;
                ykjL.text = @"一口价"; ///一口价///
                [self.scrollView addSubview:ykjL];
                
                UILabel *ykj = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+7, 130, 14)];
                ykj.font = FontSize(14);
                ykj.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                ykj.text = [NSString stringWithFormat:@"¥%.2f",[_mymodel.fixed_price floatValue]];
                [self.scrollView addSubview:ykj];
                
                UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, ykj.bottom+10, KWIDTH-left1-20, 1)];
                line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line10];
                
                UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                ordernumLB.font = FontSize(12);
                ordernumLB.textColor = K999999;
                ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                [bbgvieww addSubview:ordernumLB];
                
                bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                bbgvieww.backgroundColor = [UIColor whiteColor];
                [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            }
        }
        
    } else if ([_mymodel.user_type integerValue] == 1) {//家庭/企业报修
        
        UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
        number.font = FontSize(14);
        number.textColor = K999999;
        number.text = @"报修数量";
        [self.scrollView addSubview:number];
        UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
        numberlb.font = FontSize(14);
        numberlb.textColor = K666666;
        numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
        [self.scrollView addSubview:numberlb];
        
        UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
        line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line44];
        //维修增加故障原因
        if ([self.mymodel.fault_type isEqualToString:@"3"]) {
            
            UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
            faultReasonL.font = FontSize(14);
            faultReasonL.textColor = K999999;
            faultReasonL.text = @"故障原因";
            [self.scrollView addSubview:faultReasonL];
            
            UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
            faultReason.font = FontSize(14);
            faultReason.textColor = K666666;
            faultReason.text = _mymodel.common_failures;
            [self.scrollView addSubview:faultReason];
            
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
            line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line];
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = FontSize(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = FontSize(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = FontSize(14);
            addresslb.textColor = K999999;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
            name.font = FontSize(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
            phone.font = FontSize(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentLeft;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = FontSize(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = FontSize(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            
        } else {
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = FontSize(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = FontSize(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = FontSize(14);
            addresslb.textColor = K999999;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
            name.font = FontSize(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
            phone.font = FontSize(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentLeft;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = FontSize(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = FontSize(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        }
        
    } else {
        
        UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
        number.font = FontSize(14);
        number.textColor = K999999;
        number.text = @"报修数量";
        [self.scrollView addSubview:number];
        
        UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
        numberlb.font = FontSize(14);
        numberlb.textColor = K666666;
        numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
        [self.scrollView addSubview:numberlb];
        
        UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
        line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line44];
        
        //维修增加故障原因
        if ([self.mymodel.fault_type isEqualToString:@"3"]) {
            //个人维修
            UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
            faultReasonL.font = FontSize(14);
            faultReasonL.textColor = K999999;
            faultReasonL.text = @"故障原因";
            [self.scrollView addSubview:faultReasonL];
            
            UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
            faultReason.font = FontSize(14);
            faultReason.textColor = K666666;
            faultReason.text = _mymodel.common_failures;
            [self.scrollView addSubview:faultReason];
            
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
            line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line];
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = FontSize(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = FontSize(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = FontSize(14);
            addresslb.textColor = K999999;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
            name.font = FontSize(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
            phone.font = FontSize(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentLeft;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = FontSize(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = FontSize(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        } else {
            /////不知道是哪个单子
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = FontSize(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = FontSize(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 font:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = FontSize(14);
            addresslb.textColor = K999999;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 60, 14)];
            name.font = FontSize(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right+2, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(phoneA.right+5, line11.bottom + 3, 30, 30);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+5, line11.bottom+9, KWIDTH-20-67-60-70-30-10-15, 14)];
            phone.font = FontSize(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentLeft;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, phone.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = FontSize(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = FontSize(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        }
    }
}

/***
 服务单详情 检测报告 查看账单 查看评价 配件详情 按钮 点击跳转事件
 */
-(void)fourAction:(UIButton *)but{
    /*ind:0 是 第一个,表示状态
     1 是第一个可点击按钮
     2 是第二个可点击按钮
     3 是第三个可点击按钮
     */
    NSInteger ind = but.tag-9527;
    if (ind == 0) {
        return;
    }else{
        ind -= 1;
    }
    /*经过减1操作 ind
     0 是第一个可点击按钮
     1 是第二个可点击按钮
     2 是第三个可点击按钮
     */
    CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc]init];
    model = _mymodel;
    if ([model.rep_order_state integerValue] == 0) {
        //已接单
        if ([model.fault_type isEqualToString:@"3"]) {//维修
            if ([model.user_type isEqualToString:@"2"]) {//电器厂
                if (model.order_date.length == 0) {//无预约时间,需填写
                    kWeakSelf;
                    FillDateViewController *VC = [[FillDateViewController alloc]init];
                    VC.mymodel = model;
                    VC.myblock = ^(NSString * _Nonnull str) {
                        [self requeest];
                        [singlTool shareSingTool].needReasfh = YES;
                        //weakSelf.mymodel.order_date = @"1";
                        [self returnisJCBG:weakSelf.mymodel];
                    };
                    [self.navigationController pushViewController:VC animated:YES];
                } else {//有预约时间
                    if ([model.is_protect isEqualToString:@"0"]) {//保外
                        //生成账单
                        ZYGenerateBillsViewController *VC = [[ZYGenerateBillsViewController alloc] init];
                        VC.mymodel = model;
                        VC.myblock = ^(NSString * _Nonnull str) {
                            [self requeest];
                            [singlTool shareSingTool].needReasfh = YES;
                            self.mymodel.order_date = @"1";
                            [self returnisJCBG:self.mymodel];
                        };
                        [self.navigationController pushViewController:VC animated:YES];
                    } else {//保内
                        if ([model.product_source isEqualToString:@"1"]) {
                            //厂家提供配件
                            ZYDQCPaulineGenerateBillsViewController *Vc = [ZYDQCPaulineGenerateBillsViewController new];
                            Vc.mymodel = model;
                            Vc.myblock = ^(NSString * _Nonnull str) {
                                [self requeest];
                                [singlTool shareSingTool].needReasfh = YES;
                                self.mymodel.order_date = @"1";
                                [self returnisJCBG:self.mymodel];
                            };
                            [self.navigationController pushViewController:Vc animated:YES];
                        } else {
                            //师傅自带配件
                            ZYDQCPaulineGenerateBillsViewController *Vc = [ZYDQCPaulineGenerateBillsViewController new];
                            Vc.mymodel = model;
                            Vc.myblock = ^(NSString * _Nonnull str) {
                                [self requeest];
                                [singlTool shareSingTool].needReasfh = YES;
                                self.mymodel.order_date = @"1";
                                [self returnisJCBG:self.mymodel];
                            };
                            [self.navigationController pushViewController:Vc animated:YES];
                        }
                    }
                }
            }
            return;
            //电器厂维修和安装，需要判断是否填写预约日期和时间
            if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                //无预约日期
                //填写预约日期
                kWeakSelf;
                FillDateViewController *VC = [[FillDateViewController alloc]init];
                VC.mymodel = model;
                VC.myblock = ^(NSString * _Nonnull str) {
                    [self requeest];
                    [singlTool shareSingTool].needReasfh = YES;
                    //weakSelf.mymodel.order_date = @"1";
                    [self returnisJCBG:weakSelf.mymodel];
                };
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                //电器厂维修保内
                if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                    kWeakSelf;
                    ZYDQCPaulineGenerateBillsViewController *Vc = [ZYDQCPaulineGenerateBillsViewController new];
                    Vc.mymodel = model;
                    Vc.myblock = ^(NSString * _Nonnull str) {
                        [self requeest];
                        [singlTool shareSingTool].needReasfh = YES;
                        weakSelf.mymodel.order_date = @"1";
                        [self returnisJCBG:weakSelf.mymodel];
                    };
                    [self.navigationController pushViewController:Vc animated:YES];
                } else {
                    //生成账单
                    kWeakSelf;
                    ZYGenerateBillsViewController *VC = [[ZYGenerateBillsViewController alloc] init];
                    VC.mymodel = model;
                    VC.myblock = ^(NSString * _Nonnull str) {
                        [self requeest];
                        [singlTool shareSingTool].needReasfh = YES;
                        weakSelf.mymodel.order_date = @"1";
                        [self returnisJCBG:weakSelf.mymodel];
                    };
                    [self.navigationController pushViewController:VC animated:YES];
                }
            }
        } else {
            //电器厂维修和安装，需要判断是否填写预约日期和时间
            if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                //无预约日期
                //填写预约日期
                kWeakSelf;
                FillDateViewController *VC = [[FillDateViewController alloc]init];
                VC.mymodel = model;
                VC.myblock = ^(NSString * _Nonnull str) {
                    [self requeest];
                    [singlTool shareSingTool].needReasfh = YES;
                    //weakSelf.mymodel.order_date = @"1";
                    [self returnisJCBG:weakSelf.mymodel];
                };
                [self.navigationController pushViewController:VC animated:YES];
            } else {
                if ([model.fault_type isEqualToString:@"1"]) {
                    //电器厂安装 生成账单
                    if ([model.user_type isEqualToString:@"2"]) {
                        GenerateInstallBillForFactoryVC *zdVc = [[GenerateInstallBillForFactoryVC alloc]init];
                        zdVc.mymodel = model;
                        [self.navigationController pushViewController:zdVc animated:YES];
                    } else {
                        
                    }
                } else {
                    //生成账单
                    CSmakeAnzhuangViewController *zdVc = [[CSmakeAnzhuangViewController alloc]init];
                    zdVc.orderNumberStr = model.orderId;
                    zdVc.neworderid = model.Id;
                    zdVc.myblockj = ^(NSString * _Nonnull str) {
                        [singlTool shareSingTool].needReasfh = YES;
                    };
                    [self.navigationController pushViewController:zdVc animated:YES];
                }
            }
        }
    } else if ([model.rep_order_state integerValue] == 1) {
        //已检测
        if (ind == 1) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                //查看检测报告
                CheckTestReportForFactoryVC *bgVC = [[CheckTestReportForFactoryVC alloc]init];
                bgVC.model = model;
                [self.navigationController pushViewController:bgVC animated:YES];
            } else {
                //查看检测报告
                CSnewJCBGVC *bgVC = [[CSnewJCBGVC alloc]init];
                bgVC.model = model;
                
                [self.navigationController pushViewController:bgVC animated:YES];
            }
        } else if (ind == 2){
            //生成账单
            if ([model.fault_type isEqualToString:@"3"]) {
                //生成账单
                //电器厂维修保内
                if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                    //查看检测报告
                    //CheckTestReportForFactoryVC *bgVC = [[CheckTestReportForFactoryVC alloc]init];
                    //bgVC.model = model;
                    //[self.navigationController pushViewController:bgVC animated:YES];
                    //return;
                    
                    GenerateBillForFactoryStepOneVC *VC = [GenerateBillForFactoryStepOneVC new];
                    VC.mymodel = model;
                    [self.navigationController pushViewController:VC animated:YES];
                } else {
                    
                }
            } else {
                //电器厂保内
                if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                    //查看账单 (不是电器厂保内查看账单)
                    CheckBillForFactoryVC *zhangdna = [[CheckBillForFactoryVC alloc]init];
                    zhangdna.model = model;
                    [self.navigationController pushViewController:zhangdna animated:YES];
                    KMyLog(@"是不是 电器厂保内 厂家自带 未支付状态 查看账单-------------------");
                } else {
                    //电器厂保外 查看账单
                    
                }
            }
        }
    } else if ([model.rep_order_state integerValue] == 2) {
        //待支付
        if (ind == 1) {//中间的按钮
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                //查看检测报告
                CheckTestReportForFactoryVC *bgVC = [[CheckTestReportForFactoryVC alloc]init];
                bgVC.model = model;
                [self.navigationController pushViewController:bgVC animated:YES];
            } else {
                //查看检测报告
                //CSnewJCBGVC *bgVC = [[CSnewJCBGVC alloc]init];
                //bgVC.model = model;
                //[self.navigationController pushViewController:bgVC animated:YES];
                
                
                if ([model.user_type isEqualToString:@"0"] && [model.fault_type isEqualToString:@"3"]) { //个人维修订单
                    ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 查看账单 未支付 是不是这里");
                }
            }
        } else if(ind == 2) {//最右边的按钮
            if ([model.user_type isEqualToString:@"2"]) {//电器厂
                if ([model.fault_type isEqualToString:@"1"]) {
                    //电器厂 安装单 查看账单
                    CheckBillForFactoryVC *zhangdna = [[CheckBillForFactoryVC alloc]init];
                    zhangdna.model = model;
                    [self.navigationController pushViewController:zhangdna animated:YES];
                } else {//维修
                    //is_protect 0:(保外) 1:(保内)
                    if ([model.is_protect isEqualToString:@"1"]) {//保内
                        if ([model.product_source isEqualToString:@"1"]) {//厂家提供
                            //电器厂订单 保内 厂家提供 查看账单
                            //CheckBillForFactoryVC *zhangdna = [[CheckBillForFactoryVC alloc]init];
                            //zhangdna.model = model;
                            //[self.navigationController pushViewController:zhangdna animated:YES];
                            
                            KMyLog(@"是不是 电器厂 保内 厂家自带 查看账单8888888888888");
                            //ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                            //vc.model = model;
                            //[self.navigationController pushViewController:vc animated:YES];
                        } else { //师傅自带
                            //师傅自带 查看账单
                            //ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                            //vc.model = model;
                            //[self.navigationController pushViewController:vc animated:YES];
                            //NSLog(@"电器厂订单 师傅自带 未支付 查看账单 在这里哦");
                        }
                    } else {//保外
                        //电器厂保外 查看账单 也不是这里
                        //ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                        //vc.model = model;
                        //[self.navigationController pushViewController:vc animated:YES];
                        //KMyLog(@"电器厂订单 保外 未支付 正常流程 查看账单 在这里哦");
                    }
                }
            } else {//个人
                if ([model.fault_type isEqualToString:@"3"]) {//维修
                    //判断是否可以查看配件详情
                    if ([model.azNumber isEqualToString:@""]) { //不可以查看配件详情
                        //ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                        //vc.model = model;
                        //[self.navigationController pushViewController:vc animated:YES];
                        //KMyLog(@"个人维修订单 未支付 查看账单");
                    }
                }
            }
        }
    } else if ([model.rep_order_state integerValue] == 3) {
        //已支付
        if (ind == 1) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                
                ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器厂订单 保内 师傅自带 已支付 未评价 查看账单");
            } else {
                //电器厂维修订单 保外订单 查看账单
                ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器厂维修单 保外订单 已支付 未评价 查看账单");
            }
            
            if ([model.user_type isEqualToString:@"0"]) {
                ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人维修订单 查看账单 未支付 是不是这里");
            }
        } else if(ind == 2){
            if ([model.user_type isEqualToString:@"2"]) {//电器厂
                //电器厂保内
                if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                    //保内
                    if (model.product_source.integerValue == 1) {
                        //电器厂订单 保内 厂家提供 查看账单 已支付
                        ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 厂家提供 已支付 未评价 查看账单");
                    } else {
                        //判断是否有配件详情
                        if (model.azNumber == nil || model.azNumber == NULL) {
                            ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 保内 师傅自带 已支付 未评价 查看账单");
                        } else {
                            if ([model.azNumber isEqualToString:@"0"]) {
                                ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                                vc.model = model;
                                [self.navigationController pushViewController:vc animated:YES];
                                KMyLog(@"电器厂订单 保内 师傅自带 已支付 未评价 查看账单");
                            } else {
                                ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
                                vc.orderModel = model;
                                [self.navigationController pushViewController:vc animated:YES];
                                KMyLog(@"电器厂订单 保内 师傅自带 已支付 未评价 查看配件详情");
                            }
                        }
                    }
                } else {
                    //判断是否显示配件详情按钮
                    if (model.azNumber == nil || model.azNumber == NULL) {
                        //电器厂 保外 正常结束 已支付 未评价 查看账单
                        ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保外 正常流程 已支付 未评价 查看账单");
                    } else {
                        if ([model.azNumber isEqualToString:@"0"]) {
                            //电器厂 保外 正常结束 已支付 未评价 查看账单
                            ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 保外 正常流程 已支付 未评价 查看账单");
                        } else {
                            ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
                            vc.orderModel = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 保外 添加配件 已支付 未评价 查看配件详情");
                        }
                    }
                }
            } else {//个人
                if ([model.user_type isEqualToString:@"0"]) {
//                    if ([model.fault_type isEqualToString:@"3"]) {//维修
                        //判断是否可以查看配件详情
                        if ([model.azNumber isEqualToString:@""]) { //不可以查看配件详情
                            ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"个人维修订单 未支付 查看账单");
                        }
//                    }
                }
            }
        }
    } else if ([model.rep_order_state integerValue] == 4) {
        //已评价
        if (ind == 0) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                if (model.product_source.integerValue == 2) {//师傅自带
                    //判断是否显示配件详情
                    if (model.azNumber == nil || model.azNumber == NULL) {
                        
                    } else {
                        if ([model.azNumber isEqualToString:@"0"]) {
                            
                        } else {
                            //电器厂订单 师傅自带 已支付 已评价 查看账单
                            ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 师傅自带 已支付 已评价 查看账单");
                        }
                    }
                } else {//厂家提供
                    //查看检测报告
                    CheckTestReportForFactoryVC *bgVC = [[CheckTestReportForFactoryVC alloc]init];
                    bgVC.model = model;
                    [self.navigationController pushViewController:bgVC animated:YES];
                }
            } else {
                if (model.azNumber == nil || model.azNumber == NULL) {
                    //查看检测报告
                    CSnewJCBGVC *bgVC = [[CSnewJCBGVC alloc]init];
                    bgVC.model = model;
                    [self.navigationController pushViewController:bgVC animated:YES];
                    
                } else {
                    if ([model.azNumber isEqualToString:@"0"]) {
                        //查看检测报告
                        CSnewJCBGVC *bgVC = [[CSnewJCBGVC alloc]init];
                        bgVC.model = model;
                        [self.navigationController pushViewController:bgVC animated:YES];
                        
                    } else {
                        //电器厂订单 师傅自带 已支付 已评价 查看账单
                        ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 师傅自带 已支付 已评价 查看账单");
                    }
                }
            }
        } else if(ind  == 1){
            //电器厂保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                if (model.product_source.integerValue == 1) {
                    //厂家提供 查看账单
                    CheckBillForFactoryVC *vc = [[CheckBillForFactoryVC alloc]init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"电器厂订单 厂家提供 已支付 已评价 查看账单");
                } else {
                    if ([model.user_type isEqualToString:@"2"]) {//电器厂
                        //电器厂保内
                        if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                            //保内
                            if (model.product_source.integerValue == 1) {
                                //电器厂订单 保内 厂家提供 查看账单 已支付
                                ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                                vc.model = model;
                                [self.navigationController pushViewController:vc animated:YES];
                                KMyLog(@"电器厂订单 保内 厂家提供 已支付 已评价 查看账单");
                            } else {
                                //判断是否有配件详情
                                if (model.azNumber == nil || model.azNumber == NULL) {
                                    ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                                    vc.model = model;
                                    [self.navigationController pushViewController:vc animated:YES];
                                    KMyLog(@"电器厂订单 保内 师傅自带 已支付 已评价 查看账单");
                                } else {
                                    if ([model.azNumber isEqualToString:@"0"]) {
                                        ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                                        vc.model = model;
                                        [self.navigationController pushViewController:vc animated:YES];
                                        KMyLog(@"电器厂订单 保内 师傅自带 已支付 已评价 查看账单");
                                    } else {
                                        
                                        //查看评价
                                        LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
                                        cimmit.orderid = model.orderId;
                                        [self.navigationController pushViewController:cimmit animated:YES];
                                        KMyLog(@"电器厂订单 师傅自带 已支付 已评价 查看评价");
                                    }
                                }
                            }
                        } else {
                            //判断是否显示配件详情按钮
                            if (model.azNumber == nil || model.azNumber == NULL) {
                                //电器厂 保外 正常结束 已支付 未评价 查看账单
                                ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                                vc.model = model;
                                [self.navigationController pushViewController:vc animated:YES];
                                KMyLog(@"电器厂订单 保外 正常流程 已支付 已评价 查看账单");
                            } else {
                                if ([model.azNumber isEqualToString:@"0"]) {
                                    //电器厂 保外 正常结束 已支付 未评价 查看账单
                                    ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                                    vc.model = model;
                                    [self.navigationController pushViewController:vc animated:YES];
                                    KMyLog(@"电器厂订单 保外 正常流程 已支付 已评价 查看账单");
                                } else {
                                    ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
                                    vc.orderModel = model;
                                    [self.navigationController pushViewController:vc animated:YES];
                                    KMyLog(@"电器厂订单 保外 添加配件 已支付 已评价 查看配件详情");
                                }
                            }
                        }
                        
                    }
                }
            } else {
                if (model.azNumber == nil || model.azNumber == NULL) {
                    //电器厂保外 查看账单
                    ZYDQCBWCKZDViewController *baowaiLB = [[ZYDQCBWCKZDViewController alloc] init];
                    baowaiLB.model = model;
                    [self.navigationController pushViewController:baowaiLB animated:YES];
                    KMyLog(@"电器厂维修 保外订单 已支付 已评价 查看账单");
                } else {
                    if ([model.azNumber isEqualToString:@"0"]) {
                        //电器厂保外 查看账单
                        ZYDQCBWCKZDViewController *baowaiLB = [[ZYDQCBWCKZDViewController alloc] init];
                        baowaiLB.model = model;
                        [self.navigationController pushViewController:baowaiLB animated:YES];
                        KMyLog(@"电器厂维修 保外订单 已支付 已评价 查看账单");
                    } else {
                        //查看评价
                        LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
                        cimmit.orderid = model.orderId;
                        [self.navigationController pushViewController:cimmit animated:YES];
                        KMyLog(@"电器厂维修订单 已评价 查看账单");
                    }
                }
            }
        } else if(ind == 2){
            //判断是否显示配件详情按钮
            if (model.azNumber == nil || model.azNumber == NULL) {
                //查看评价
                LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
                cimmit.orderid = model.orderId;
                [self.navigationController pushViewController:cimmit animated:YES];
                KMyLog(@"电器厂维修订单 已评价 查看账单");
            } else {
                if ([model.azNumber isEqualToString:@"0"]) {
                    //查看评价
                    LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
                    cimmit.orderid = model.orderId;
                    [self.navigationController pushViewController:cimmit animated:YES];
                    KMyLog(@"电器厂维修订单 保外 已评价 查看账单");
                } else {
                    ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
                    vc.orderModel = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"电器厂维修订单 保内 师傅自带 已评价 查看配件详情");
                }
            }
        }
    } else if ([model.rep_order_state integerValue] == 5) {
        //已取消

    } else if ([model.rep_order_state integerValue] == 6) {
        if (ind == 1) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                //查看检测报告
                CheckTestReportForFactoryVC *bgVC = [[CheckTestReportForFactoryVC alloc]init];
                bgVC.model = model;
                [self.navigationController pushViewController:bgVC animated:YES];
            } else {
                //查看检测报告
                CSnewJCBGVC *bgVC = [[CSnewJCBGVC alloc]init];
                bgVC.model = model;
                [self.navigationController pushViewController:bgVC animated:YES];
            }
        } else if (ind == 2){
            //电器厂保内
            if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                if (model.product_source.integerValue == 1) {
                    //厂家提供 查看账单
                    CheckBillForFactoryVC *zhangdna = [[CheckBillForFactoryVC alloc]init];
                    zhangdna.model = model;
                    [self.navigationController pushViewController:zhangdna animated:YES];
                } else {
                    //电器厂 师傅自带 查看账单
                    //ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                    //vc.model = model;
                    //[self.navigationController pushViewController:vc animated:YES];
                    //KMyLog(@"这里是不是电器厂 保内 师傅自带 未支付 查看账单");
                }
            } else {
                //电器厂保外 查看账单 (不是电器厂保内 师傅自带 查看账单)
                //ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                //vc.model = model;
                //[self.navigationController pushViewController:vc animated:YES];
            }
        }
    }
}

#pragma mark - 修改按钮状态
-(void)returnisJCBG:(CSmaintenanceListModel *)model{
    
    if ([model.fault_type isEqualToString:@"3"]) { //维修订单
        
        if ([model.rep_order_state integerValue] == 0) {
            if ([model.user_type isEqualToString:@"2"]) {//电器厂
                if (model.order_date.length == 0) {//无预约时间,需填写
                    [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                    self.twobut.hidden = YES;
                    self.threebut.hidden = YES;
                    self.fourbut.hidden = YES;
                } else {//有预约时间
                    if ([model.is_protect isEqualToString:@"0"]) {//保外
                        //已接单
                        [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                        self.twobut.hidden = YES;
                        self.threebut.hidden = YES;
                        self.fourbut.hidden = YES;
                    } else {//保内
                        if ([model.product_source isEqualToString:@"1"]) {//厂家提供配件
                            //已接单
                            [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                            self.twobut.hidden = YES;
                            self.threebut.hidden = YES;
                            self.fourbut.hidden = YES;
                        } else {//师傅自带配件
                            //已接单
                            [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                            self.twobut.hidden = YES;
                            self.threebut.hidden = YES;
                            self.fourbut.hidden = YES;
                        }
                    }
                }
            } else { //家庭/企事业
                //已接单
                [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                self.twobut.hidden = YES;
                self.threebut.hidden = YES;
                self.fourbut.hidden = YES;
            }
        } else if ([model.rep_order_state integerValue] == 1) {
            //已检测 (只有电器厂-维修-保内-厂家提供配件 才有检测)
            [self.onebut setTitle:@"已检测" forState:(UIControlStateNormal)];
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
            
        } else if ([model.rep_order_state integerValue] == 2) {
            //待支付
            [self.onebut setTitle:@"待支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            
            if ([model.azNumber isEqualToString:@""]) {
                //配件详情 按钮
                self.threebut.hidden = YES;
                [self.threebut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            } else {
                
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            }
            
            if (model.user_type.integerValue == 2 && model.is_protect.integerValue == 1 && model.product_source.integerValue == 1) {
                //维修-电器厂-保内-厂家提供
                self.threebut.hidden = NO;
                [self.threebut setTitle:@"检测报告" forState:UIControlStateNormal];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.layer.borderWidth = 1;
                self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                [self.threebut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
            
        } else if ([model.rep_order_state integerValue] == 3) {
            //已支付
            [self.onebut setTitle:@"已支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            
            if (model.azNumber == nil || model.azNumber == NULL) {
                self.threebut.hidden = YES;
                
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            } else {
                
                if ([model.azNumber isEqualToString:@"0"]) {
                    self.threebut.hidden = YES;
                    
                    [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.fourbut.layer.borderWidth = 1;
                } else {
                    
                    [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.threebut.layer.borderWidth = 1;
                    
                    [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.fourbut.layer.borderWidth = 1;
                }
            }
        } else if ([model.rep_order_state integerValue] == 4) {
            //4已评价，
            [self.onebut setTitle:@"已评价" forState:(UIControlStateNormal)];
            //self.twobut.hidden = YES;
            self.threebut.hidden = NO;
            if (model.is_protect.integerValue == 1) {//保内
                if (model.product_source.integerValue == 1) {//厂家提供配件
                    self.twobut.hidden = NO;
                    [self.twobut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.twobut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.twobut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.twobut.layer.borderWidth = 1;
                }
            }
            
            //判断是否显示配件详情按钮
            if (model.azNumber == nil || model.azNumber == NULL) {
                self.twobut.hidden = YES;
                
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            } else {
                if ([model.azNumber isEqualToString:@"0"]) {
                    self.twobut.hidden = YES;
                    
                    [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.threebut.layer.borderWidth = 1;
                    
                    [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.fourbut.layer.borderWidth = 1;
                } else {
                    
                    [self.twobut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.twobut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.twobut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.twobut.layer.borderWidth = 1;
                    
                    [self.threebut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.threebut.layer.borderWidth = 1;
                    
                    [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                    self.fourbut.layer.borderWidth = 1;
                }
            }
        } else if ([model.rep_order_state integerValue] == 5) {
            // 5已取消
            [self.onebut setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        } else if ([model.rep_order_state integerValue] == 6) {
            //订单中止
            [self.onebut setTitle:@"订单中止" forState:(UIControlStateNormal)];
            self.onebut.titleLabel.font = FontSize(14);
            [self.onebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor whiteColor].CGColor;
            [self.fourbut setBackgroundColor:[UIColor clearColor]];
            self.fourbut.layer.borderWidth = 1;
        }
    } else {
        if ([model.rep_order_state integerValue] == 0) {
            //电器厂维修和安装，需要判断是否填写预约日期和时间
            if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                //无预约日期
                //已接单
                [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                
                self.twobut.hidden = YES;
                self.threebut.hidden = YES;
                
                [self.fourbut setTitle:@"填写预约信息" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            } else {
                //已接单
                [self.onebut setTitle:@"已接单" forState:(UIControlStateNormal)];
                self.twobut.hidden = YES;
                self.threebut.hidden = YES;
                self.fourbut.hidden = YES;
                [self.fourbut setTitle:@"生成账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                self.fourbut.layer.borderWidth = 1;
            }
        } else if ([model.rep_order_state integerValue] == 1) {
            //已检测
            [self.onebut setTitle:@"已检测" forState:(UIControlStateNormal)];
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
            self.fourbut.layer.borderWidth = 1;
        } else if ([model.rep_order_state integerValue] == 2) {
            //待支付
            [self.onebut setTitle:@"待支付" forState:(UIControlStateNormal)];
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
            self.fourbut.layer.borderWidth = 1;
        } else if ([model.rep_order_state integerValue] == 3) {
            //已完成
            [self.onebut setTitle:@"已支付" forState:(UIControlStateNormal)];
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
            self.fourbut.layer.borderWidth = 1;
        } else if ([model.rep_order_state integerValue] == 4){
            //4已评价，
            [self.onebut setTitle:@"已评价" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            
            [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.threebut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];/////
            self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;/////
            self.threebut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];/////
            self.threebut.layer.borderWidth = 1;
            
            [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
            self.fourbut.layer.borderWidth = 1;
        } else if ([model.rep_order_state integerValue] == 5){
            // 5已取消
            [self.onebut setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

@end
