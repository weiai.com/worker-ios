//
//  FillDateViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "FillDateViewController.h"
#import "TanKuangView.h"
#import "ZYElectricalFactoryServiceOrderDetailsVC.h" //电器厂订单 服务单详情
#import "ZYTestOrderDetailViewController.h" //检测单详情

@interface FillDateViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSArray *_nameArray;
}
@property (strong, nonatomic) UIPickerView *pickerView;
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *riqiLB;
@property(nonatomic,strong)UILabel *timeLB;
@property(nonatomic,strong)UIButton *jiedanBUt;//接单
@property(nonatomic,strong)UIButton *huluebut;//忽略
@property(nonatomic,strong)UIView *bgViewRiqi;//选择日期的背景
@property(nonatomic,strong)UIButton *oldbut;//选择日期的but
@property(nonatomic,strong)UIView *bgViewtime;//选择时间的背景
@property(nonatomic,strong)UIButton *oldbuttime;//选中时间的but
@property(nonatomic,assign)NSInteger sencion0row;// uipickerview选中行
@property(nonatomic,strong)NSString *seleTime;//时间选择
@property(nonatomic,strong)NSString *timeStr;
@property(nonatomic,strong)UIView *bgViewsec;
@property(nonatomic,assign)BOOL istime;
@property(nonatomic,strong)TanKuangView *zhijiehuluo;
@property(nonatomic,strong)UIImageView *sanjiaoImage;

@end

@implementation FillDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"填写预约信息";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self showseleui];
    [self shwosecBgview];
}

-(void)showseleui{
    
    
    UILabel *winingLB = [[UILabel alloc]initWithFrame:CGRectMake(26, 20, KWIDTH-52, 38)];
    winingLB.font = FontSize(14);
    winingLB.textColor = KshiYellow;
    winingLB.numberOfLines = 2;
    //winingLB.text = @"温馨提示:此单是厂商为客户下的维修单，具体维修事宜请您电话联系客户了解！";
    [self.view addSubview:winingLB];
    
    UILabel *redLB = [[UILabel alloc]initWithFrame:CGRectMake(26, winingLB.bottom+20, KWIDTH-52, 38)];
    redLB.font = FontSize(12);
    redLB.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    redLB.numberOfLines = 2;
    redLB.text = @"请填写您与客户电话沟通的具体预约信息并点击提交!";
    [self.view addSubview:redLB];
    
    UIView *dowbbgvieww = [[UIView alloc]initWithFrame:CGRectMake(10, redLB.bottom+10, KWIDTH-20, 130)];
    dowbbgvieww.backgroundColor = [UIColor whiteColor];
    [dowbbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
    
    [self.view addSubview:dowbbgvieww];
    
    //self.jiedanBUt.y = dowbbgvieww.bottom+27;
    //[self.jiedanBUt setTitle:@"接单" forState:(UIControlStateNormal)];
    //self.huluebut.y = dowbbgvieww.bottom+27;;

    UIButton*leftbutu = [UIButton buttonWithType:(UIButtonTypeCustom)];
    leftbutu.frame = CGRectMake(20, dowbbgvieww.bottom+27, KWIDTH-40, 48);
    [leftbutu setTitle:@"提交" forState:(UIControlStateNormal)];
    [leftbutu setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [leftbutu setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftbutu addTarget:self action:@selector(commit) forControlEvents:(UIControlEventTouchUpInside)];
    leftbutu.layer.masksToBounds = YES;
    leftbutu.layer.cornerRadius = 4;
    [self.view addSubview:leftbutu];
    
    //    UIButton*rightbutu  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    rightbutu.frame = CGRectMake(wwww*2+160, dowbbgvieww.bottom+27, 160, 48);
    //    [rightbutu setTitle:@"忽略" forState:(UIControlStateNormal)];
    //    [rightbutu setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //    [rightbutu setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [rightbutu addTarget:self action:@selector(rightbuttobuttoaction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [self.view addSubview:rightbutu];
    //    rightbutu.layer.masksToBounds = YES;
    //    rightbutu.layer.cornerRadius = 4;

    UILabel *yuyuetiitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 17, KWIDTH-20, 16)];
    yuyuetiitle.font = FontSize(14);
    yuyuetiitle.textColor = K333333;
    yuyuetiitle.text = @"预约信息";
    yuyuetiitle.textAlignment = NSTextAlignmentCenter;
    [dowbbgvieww addSubview:yuyuetiitle];
    
    UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(15, 53, 60, 14)];
    yuyueTime.font = FontSize(14);
    yuyueTime.textColor = K666666;
    yuyueTime.text = @"上门日期";
    [dowbbgvieww addSubview:yuyueTime];
    
    UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(yuyueTime.right+14, 53, KWIDTH-119, 14)];
    yuyueTimelb.font = FontSize(14);
    yuyueTimelb.textColor = K999999;
    yuyueTimelb.text = @"请选择上门日期";
    self.riqiLB = yuyueTimelb;
    [dowbbgvieww addSubview:yuyueTimelb];
    
    UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(15, yuyueTimelb.bottom+10, KWIDTH-16-20, 1)];
    line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [dowbbgvieww addSubview:line5];
    
    UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(15, line5.bottom+8, 60, 14)];
    yuyueTime2.font = FontSize(14);
    yuyueTime2.textColor = K666666;
    yuyueTime2.text = @"上门时间";
    [dowbbgvieww addSubview:yuyueTime2];
    
    UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(yuyueTime2.right+14, line5.bottom+8, KWIDTH-16-20, 14)];
    yuyueTimelb2.font = FontSize(14);
    yuyueTimelb2.textColor = K999999;
    yuyueTimelb2.text = @"请选择上门时间";
    self.timeLB = yuyueTimelb2;
    [dowbbgvieww addSubview:yuyueTimelb2];
    
    UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(15, yuyueTime2.bottom+10, KWIDTH-16-20, 1)];
    line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [dowbbgvieww addSubview:line6];
    
    //    UILabel *beizhu = [[UILabel alloc]initWithFrame:CGRectMake(15, line6.bottom+10, 60, 16)];
    //    beizhu.font = FontSize(14);
    //    beizhu.textColor = K999999;
    //    beizhu.text = @"备注信息";
    ////    beizhu.textAlignment = NSTextAlignmentCenter;
    //    [dowbbgvieww addSubview:beizhu];
    //
    //    UITextView *beizhuTV = [[UITextView alloc]initWithFrame:CGRectMake(beizhu.right+14, line6.bottom+10, KWIDTH-beizhu.right-14-25, 78)];
    //    beizhuTV.backgroundColor = [UIColor colorWithHexString:@"#E9E9E9"];
    //    beizhuTV.placeholder = @"请填写备注信息";
    //    beizhuTV.layer.masksToBounds = YES;
    //    beizhuTV.layer.cornerRadius = 4;
    //    [dowbbgvieww addSubview:beizhuTV];
    
    UIButton *upbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [dowbbgvieww addSubview:upbut];
    [upbut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [upbut setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -130)];
    [upbut addTarget:self action:@selector(seleriqi:) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIButton *dowbut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [dowbbgvieww addSubview:dowbut];
    [dowbut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    [dowbut setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -130)];
    [dowbut addTarget:self action:@selector(seletime:) forControlEvents:(UIControlEventTouchUpInside)];
    
    [upbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.width.offset(150);
        make.height.offset(30);
        make.top.offset(45);
    }];
    
    [dowbut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.width.offset(150);
        make.height.offset(30);
        make.top.offset(77);
    }];
    //_scrollView.contentSize = CGSizeMake(KWIDTH, dowbbgvieww.bottom+40+kNaviHeight);
}

-(void)seleriqi:(UIButton *)but{
    
    //新建日期选择器
    MHDatePicker *datePicker = [[MHDatePicker alloc] init];
    datePicker.isBeforeTime = NO;
    datePicker.datePickerMode = UIDatePickerModeDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [datePicker didFinishSelectedDate:^(NSDate *selectedDate) {
        NSString *selectedDateStr = [formatter stringFromDate:selectedDate];
        self.timeStr = selectedDateStr;
        NSArray *myarray = [selectedDateStr componentsSeparatedByString:@"-"];
        self.riqiLB.text = [NSString stringWithFormat:@"%@年%@月%@日",myarray[0],myarray[1],myarray[2]];
        self.riqiLB.textColor = K333333;
        
        if ([self.timeLB.text isEqualToString:@""]) { //如果时间为空
            KMyLog(@"如果时间为空,则不做处理");
        } else { //如果时间不为空
            self.timeLB.text = @"请选择上门时间";
            self.timeLB.textColor = K999999;
        }
    }];
  
}

-(void)seletime:(UIButton *)but{
    KMyLog(@"选择日期");
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewtime];
}

-(void)shwosecBgview{
    self.bgViewtime = [[UIView alloc]init];
    self.bgViewtime.frame = self.view.bounds;
    self.bgViewtime.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewtime addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(271);
        make.width.offset(317);
    }];
    
    //    UIButton *amBUt = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [amBUt setTitle:@"上午" forState:(UIControlStateNormal)];
    //    [amBUt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [amBUt setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //    self.sanjiaoImage = [[UIImageView alloc]init];
    //    self.sanjiaoImage.frame = CGRectMake(KWIDTH/4-8.5, 51, 17, 10);
    //
    //    self.sanjiaoImage.image = imgname(@"snajiaoWhite");
    //    [amBUt addTarget:self action:@selector(amBUtAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [whiteBGView addSubview:amBUt];
    //    self.oldbuttime = amBUt;
    //
    //    [self.oldbuttime setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
    //    UIButton *pmBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [pmBut setTitle:@"下午" forState:(UIControlStateNormal)];
    //    [pmBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [pmBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //    [pmBut addTarget:self action:@selector(pmButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [whiteBGView addSubview:pmBut];
    //    [whiteBGView addSubview:self.sanjiaoImage];
    //
    //    [amBUt mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.offset(61);
    //        make.left.offset(0);
    //        make.width.offset(158.5);
    //        make.top.offset(0);
    //    }];
    //
    //    [pmBut mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.offset(61);
    //        make.right.offset(0);
    //        make.width.offset(158.5);
    //        make.top.offset(0);
    //    }];
    
    UILabel *topTitLab = [[UILabel alloc] init];
    topTitLab.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    topTitLab.text = @"点击时间段进行选择";
    topTitLab.textAlignment = NSTextAlignmentCenter;
    topTitLab.textColor = [UIColor whiteColor];
    topTitLab.font = FontSize(16);
    [whiteBGView addSubview:topTitLab];
    
    [topTitLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(61);
        make.left.offset(0);
        make.width.offset(317);
        make.top.offset(0);
    }];
    
    self.pickerView = [[UIPickerView alloc]init];
    self.pickerView.backgroundColor = [UIColor whiteColor];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    [whiteBGView addSubview:self.pickerView];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        //make.top.mas_equalTo(amBUt.mas_bottom).offset(15);
        make.top.mas_equalTo(topTitLab.mas_bottom).offset(15);
        make.height.offset(135);
    }];
    
    _nameArray = [NSArray arrayWithObjects:@"8:00 - 9:00",@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",@"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",nil];
    
    [self.pickerView reloadAllComponents];//刷新UIPickerView
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(timeikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(timeyesButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(61);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-61);
        make.width.offset(64);
        make.bottom.offset(-14);
    }];
}

//-(void)amBUtAction:(UIButton *)but{
//    //上午
//    //_nameArray = [NSArray arrayWithObjects:@"0:00 - 1:00",@"1:00 - 2:00",@"2:00 - 3:00",@"3:00 - 4:00",@"4:00 - 5:00",@"5:00 - 6:00",@"6:00 - 7:00",@"7:00 - 8:00",@"8:00 - 9:00",@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",nil];
//
//    _nameArray = [NSArray arrayWithObjects:@"9:00 - 10:00",@"10:00 - 11:00",@"11:00 - 12:00",nil];
//
//    [self.pickerView reloadAllComponents];//刷新UIPickerView
//    [self.oldbuttime setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
//    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
//    self.oldbut = but;
//    self.oldbuttime  = but;
//    self.sanjiaoImage.centerX = self.oldbuttime.centerX;
//}

//-(void)pmButAction:(UIButton *)but{
//    //下午
//    //_nameArray = [NSArray arrayWithObjects: @"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",@"20:00 - 21:00",@"21:00 - 22:00",@"22:00 - 23:00",@"23:00 - 24:00",nil];
//    
//    _nameArray = [NSArray arrayWithObjects: @"12:00 - 13:00",@"13:00 - 14:00",@"14:00 - 15:00",@"15:00 - 16:00",@"16:00 - 17:00",@"17:00 - 18:00",@"18:00 - 19:00",@"19:00 - 20:00",nil];
//
//    [self.pickerView reloadAllComponents];//刷新UIPickerView
//
//    [self.oldbuttime setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
//    [but setTitleColor:[UIColor colorWithHexString:@"#F9AC19"] forState:(UIControlStateNormal)];
//    self.oldbut = but;
//    self.oldbuttime  = but;
//    self.sanjiaoImage.centerX = self.oldbuttime.centerX;
//}

-(void)timeikenowAction:(UIButton *)but{
    //取消
    [_bgViewtime removeFromSuperview];
}

-(void)timeyesButAction:(UIButton *)but{
    //确定
    _timeLB.text = _seleTime;
    if (!_seleTime) {
        _timeLB.text = _nameArray[0];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    NSString *nowstr = [dateTime substringFromIndex:11];
    nowstr = [nowstr substringToIndex:2];

    _timeLB.textColor = K333333;
    
    NSString *jintian = [self newGetCurrentTime];
    if ([self.riqiLB.text isEqualToString:jintian]) {
        NSInteger rimeind =[[_timeLB.text substringToIndex:2] integerValue];
        if (rimeind < [nowstr integerValue]) {
            ShowToastWithText(@"预约时间必须大于当前时间");
            _timeLB.text = @"请选择上门时间";
            _timeLB.textColor = K999999;
        }
    }
    [_bgViewtime removeFromSuperview];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//返回指定列的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_nameArray count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.0f;
}

- (CGFloat) pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return  317;
}

//显示的标题
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *str = [_nameArray objectAtIndex:row];
    return str;
}

//显示的标题字体、颜色等属性
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    NSString *str = [_nameArray objectAtIndex:row];
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc]initWithString:str];
    
    [AttributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#999999"]} range:NSMakeRange(0, [AttributedString  length])];
    if (self.sencion0row == row) {
        [AttributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#F9AC19"]} range:NSMakeRange(0, [AttributedString  length])];
    }
    
    return AttributedString;
    
}//NS_AVAILABLE_IOS(6_0);

//被选择的行
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.sencion0row = row;
    [_pickerView reloadAllComponents];
    self.seleTime =[_nameArray objectAtIndex:row];
}

//获取当地时间
- (NSString *)newGetCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy年MM月dd日"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    return dateTime;
}

#pragma mark 提交
- (void)commit {
    
    //    if (self.myblock) {
    //        self.myblock(@"");
    //    }
    //    [self.navigationController popViewControllerAnimated:YES];
    //    return;
    
    if ([_riqiLB.text isEqualToString:@"请选择上门日期"]  ) {
        ShowToastWithText(@"请选择上门日期");
        return;
    }else if( [_timeLB.text isEqualToString:@"请选择上门时间"]){
        ShowToastWithText(@"请选择上门时间");
        return;
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    if ([_mymodel.order_type_id isEqualToString:@"1"]) {
        param[@"order_type_id"] = _mymodel.order_type_id;
    } else {
        param[@"order_type_id"] = @"";
    }
    param[@"id"] = _mymodel.Id;
    //param[@"order_date"] = @"";
    //param[@"order_time"] = @"";
    param[@"order_date"] = _timeStr;
    param[@"order_time"] = _timeLB.text;
    
    [NetWorkTool POST:dqcUpdateOrderTime param:param success:^(id dic) {
        
        if (self.myblock) {
            self.myblock(@"");
        }
        
        /***
         *test_order_id
         *不为空 根据检测单下的订单
         *空    电器厂正常下的订单
         */
        
        if ([self.mymodel.order_type_id isEqualToString:@"1"]) { //检测单
            ZYTestOrderDetailViewController *vc = [[ZYTestOrderDetailViewController alloc] init];
            NSString *strPush = @"2";
            vc.strPush = strPush;
            vc.mymodel = self.mymodel;
            vc.idStr = self.mymodel.Id;
            [self.navigationController pushViewController:vc animated:YES];
        } else { //非检测单
            //电器厂正常下单 填写完预约信息之后 返回服务单列表
            if ([self.mymodel.test_order_id isEqualToString:@""]) {
                [self.navigationController popViewControllerAnimated:YES]; //11.20注释
            } else {
                ZYElectricalFactoryServiceOrderDetailsVC *vc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc] init];
                vc.mymodel = self.mymodel;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

@end
