//
//  ZYTestOrderContentTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestOrderContentTableViewCell.h"

@implementation ZYTestOrderContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 169);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        bottomV.layer.borderWidth = 1;
        bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        //最上面 日期时间
        UILabel *dateLab = [[UILabel alloc] init];
        dateLab.frame = CGRectMake(15, 14, KWIDTH/2, 17);
        dateLab.font = KFontPingFangSCRegular(12);
        dateLab.textColor = K999999;
        [bottomV addSubview:dateLab];
        self.dateLab = dateLab;
        //右上角 图片
        UIImageView *tipsImg = [[UIImageView alloc] init];
        tipsImg.frame = CGRectMake(bottomV.right-79, 0, 15, 20);
        [tipsImg setImage:imgname(@"cdianqichalogo")];
        [bottomV addSubview:tipsImg];
        //右上角 状态文本
        UILabel *stateLab = [[UILabel alloc] init];
        stateLab.frame = CGRectMake(tipsImg.right+4, 14, 45, 11);
        stateLab.text = @"已接单";
        stateLab.textAlignment = NSTextAlignmentRight;
        stateLab.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        stateLab.font = KFontPingFangSCLight(11);
        [bottomV addSubview:stateLab];
        self.stateLab = stateLab;
        //右箭头 图片
        UIImageView *leftImg = [[UIImageView alloc] init];
        leftImg.frame = CGRectMake(12, dateLab.bottom+22, 48, 48);
        [leftImg setImage:imgname(@"jiancedanleftimg")];
        [bottomV addSubview:leftImg];
        //检测单 提示文字
        UILabel *tipsTitLab = [[UILabel alloc] init];
        tipsTitLab.frame = CGRectMake(12, leftImg.bottom +8, 48, 17);
        tipsTitLab.text = @"检测单";
        tipsTitLab.textAlignment = NSTextAlignmentCenter;
        tipsTitLab.font = KFontPingFangSCMedium(12);
        tipsTitLab.textColor = K666666;
        [bottomV addSubview:tipsTitLab];
        //服务类型 标题
        UILabel *fwlxTitLab = [[UILabel alloc] init];
        fwlxTitLab.frame = CGRectMake(leftImg.right+12, dateLab.bottom +7, 70, 20);
        fwlxTitLab.text = @"服务类型:";
        fwlxTitLab.font = KFontPingFangSCMedium(13);
        fwlxTitLab.textColor = K666666;
        [bottomV addSubview:fwlxTitLab];
        self.fwlxTitLab = fwlxTitLab;
        //服务类型 内容
        UILabel *fwlxConLab = [[UILabel alloc] init];
        fwlxConLab.frame = CGRectMake(fwlxTitLab.right, dateLab.bottom +7, KWIDTH-20-12-48-12-70-12, 20);
        fwlxConLab.font = KFontPingFangSCMedium(13);
        fwlxConLab.textColor = K666666;
        [bottomV addSubview:fwlxConLab];
        self.fwlxConLab = fwlxConLab;
        //电器品类 标题
        UILabel *dqplTitLab = [[UILabel alloc] init];
        dqplTitLab.frame = CGRectMake(leftImg.right+12, fwlxTitLab.bottom +2, 70, 20);
        dqplTitLab.text = @"电器品类:";
        dqplTitLab.font = KFontPingFangSCMedium(13);
        dqplTitLab.textColor = K666666;
        [bottomV addSubview:dqplTitLab];
        self.dqplTitLab = dqplTitLab;
        //电器品类 内容
        UILabel *dqplConLab = [[UILabel alloc] init];
        dqplConLab.frame = CGRectMake(fwlxTitLab.right, fwlxTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        dqplConLab.font = KFontPingFangSCMedium(13);
        dqplConLab.textColor = K666666;
        [bottomV addSubview:dqplConLab];
        self.dqplConLab = dqplConLab;
        //预约时间 标题
        UILabel *yysjTitLab = [[UILabel alloc] init];
        yysjTitLab.frame = CGRectMake(leftImg.right+12, dqplTitLab.bottom +2, 70, 20);
        yysjTitLab.text = @"上门时间:";
        yysjTitLab.font = KFontPingFangSCMedium(13);
        yysjTitLab.textColor = K666666;
        [bottomV addSubview:yysjTitLab];
        self.yysjTitLab = yysjTitLab;
        //预约时间 内容
        UILabel *yysjConLab = [[UILabel alloc] init];
        yysjConLab.frame = CGRectMake(yysjTitLab.right, dqplTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        yysjConLab.font = KFontPingFangSCMedium(13);
        yysjConLab.textColor = K666666;
        [bottomV addSubview:yysjConLab];
        self.yysjConLab = yysjConLab;
        //服务地址 标题
        UILabel *fwdzTitLab = [[UILabel alloc] init];
        fwdzTitLab.frame = CGRectMake(leftImg.right+12, yysjTitLab.bottom +2, 70, 20);
        fwdzTitLab.text = @"服务地址:";
        fwdzTitLab.font = KFontPingFangSCMedium(13);
        fwdzTitLab.textColor = K666666;
        [bottomV addSubview:fwdzTitLab];
        self.fwdzTitLab = fwdzTitLab;
        //服务地址 内容
        UILabel *fwdzConLab = [[UILabel alloc] init];
        fwdzConLab.frame = CGRectMake(fwlxTitLab.right, yysjTitLab.bottom +2, KWIDTH-20-12-48-12-70-12, 20);
        fwdzConLab.font = KFontPingFangSCMedium(13);
        fwdzConLab.textColor = K666666;
        [bottomV addSubview:fwdzConLab];
        self.fwdzConLab = fwdzConLab;
        
        //填写预约信息 按钮
        UIButton *txyyxxBtn = [[UIButton alloc] init];
        txyyxxBtn.frame = CGRectMake(bottomV.right - 116, fwdzTitLab.bottom +10, 112, 28);
        [txyyxxBtn setImage:[UIImage imageNamed:@"tianxieyuyuexinxi"] forState:UIControlStateNormal];
        [txyyxxBtn addTarget:self action:@selector(tianxieyuyuexinxi:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:txyyxxBtn];
        self.txyyxxBtn = txyyxxBtn;

        //生成检测报告 按钮
        UIButton *scjcbgBtn = [[UIButton alloc] init];
        scjcbgBtn.frame = CGRectMake(bottomV.right - 116, fwdzTitLab.bottom +10, 112, 28);
        [scjcbgBtn setImage:[UIImage imageNamed:@"shengchengjiancebaogao"] forState:UIControlStateNormal];
        [scjcbgBtn addTarget:self action:@selector(shengchengjiancebaogao:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:scjcbgBtn];
        self.scjcbgBtn = scjcbgBtn;
    }
    return self;
}

- (void)refasf:(CSmaintenanceListModel *)model{
    self.dateLab.text = model.create_time;        //日期时间
    self.fwlxConLab.text = model.service_name;    //服务类型
    self.dqplConLab.text = model.fault_name;      //电器品类
   
    if (model.order_time == nil && model.order_date == nil) {
       
        self.yysjConLab.hidden = YES;
    } else {
        NSInteger  indd = [[model.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        
        NSArray  *myarray = [model.order_date
                             componentsSeparatedByString:@"-"];
        NSString *ordertiame =[NSString stringWithFormat:@"%@月%@日 %@%@",myarray[1],myarray[2],ap,model.order_time];
        self.yysjConLab.text = ordertiame;
        self.yysjConLab.hidden = NO;
    }
    self.fwdzConLab.text = model.user_address;    //地址

    if ([model.order_state isEqualToString:@"1"]) {   //已接单
        self.stateLab.text = @"已接单";
        self.scjcbgBtn.hidden = NO;
    } else if ([model.order_state isEqualToString:@"2"] ) { //已检测
        self.stateLab.text = @"已检测";
        self.scjcbgBtn.hidden = YES;
        self.txyyxxBtn.hidden = YES;
    } else if ([model.order_state isEqualToString:@"3"] ) { //已支付
        self.stateLab.text = @"已支付";
        self.scjcbgBtn.hidden = YES;
        self.txyyxxBtn.hidden = YES;
    } else if ([model.order_state isEqualToString:@"4"] ) { //已取消
        self.stateLab.text = @"已取消";
        self.scjcbgBtn.hidden = YES;
        self.txyyxxBtn.hidden = YES;
    } else { //已损坏
        KMyLog(@"暂时先不管");
    }
    
    if (!model.order_date && !model.order_time) {
        self.txyyxxBtn.hidden = NO;
        self.scjcbgBtn.hidden = YES;
    } else {
        self.txyyxxBtn.hidden = YES;
        if ([model.order_state isEqualToString:@"1"]) {
            self.scjcbgBtn.hidden = NO;
        } else {
            self.scjcbgBtn.hidden = YES;
        }
    }
}

//生成检测报告 按钮 点击事件
- (void)shengchengjiancebaogao:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");//生成检测报告
    }
    NSLog(@"您点击了 检测单 生成检测报告 按钮");
}

//填写预约信息 按钮 点击事件
- (void)tianxieyuyuexinxi:(UIButton *)button {
    if (self.myblock) {
        self.myblock(1, @"");//填写预约信息
    }
    NSLog(@"您点击了 检测单 填写预约信息 按钮");
}

@end
