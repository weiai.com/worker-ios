//
//  GenerateBillForFactoryStepTwoVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "GenerateBillForFactoryStepTwoVC.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "SeleproductTypeViewController.h"
#import "NewSelenameViewController.h"
#import "CSOrderContentViewController.h"

@interface GenerateBillForFactoryStepTwoVC ()<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate
>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *myPuCategorylb;//故障配件品类:
@property (nonatomic, strong) UITextField *myPuCName;//故障配件名称:
@property (nonatomic, strong) UITextView *mytextView;//请填写产品具体故障
@property (nonatomic, strong) UITextField *myyuge;//预估费用:
@property (nonatomic, strong) UITextField *checkMoneyTF;//检测费
@property (nonatomic, strong) UILabel *placeLab;//
@property (nonatomic, strong) UIView *addView;//
@property (nonatomic, strong) UIButton *oldbut;//
@property (nonatomic, strong) NSMutableArray *imageArr;//
@property (nonatomic, assign) BOOL isfour;//
@property (nonatomic, assign) CGFloat bottomf;//
@property (nonatomic, strong) NSString *myPuCategorID;//
@property (nonatomic, strong) NSString *issecont;//二次上门
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) UIView *bgViewsec;

@end

@implementation GenerateBillForFactoryStepTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    self.isfour = NO;
    self.issecont = @"0";

    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"addpicimage");
    [self.imageArr addObject:_myimage];
    [self showui];
    
    [self shwoBgviewsec];
    
}

-(void)showui{
    [self.view addSubview:self.scrollView];
    CGFloat left1 = 16;
    
    UILabel *ornumber = [[UILabel alloc]initWithFrame:CGRectMake(left1, 0, 70, 55)];
    ornumber.text = @"订单编号:";
    ornumber.textColor = K666666;
    ornumber.font = FontSize(16);
    [self.scrollView addSubview:ornumber];
    
    UILabel *ornumberlb = [[UILabel alloc]initWithFrame:CGRectMake(ornumber.right+13, 0, KWIDTH, 55)];
    ornumberlb.text = _mymodel.Id;
    ornumberlb.textColor = K666666;
    ornumberlb.font = FontSize(16);
    [self.scrollView addSubview:ornumberlb];
    
    UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, ornumber.bottom, KWIDTH, 13)];
    line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line0];
    
    //    UILabel *productCategory = [[UILabel alloc]initWithFrame:CGRectMake(left1, ornumber.bottom+13, 105, 48)];
    //    productCategory.text = @"产品品类:";
    //    productCategory.textColor = K333333;
    //    productCategory.font = FontSize(16);
    //    [self.scrollView addSubview:productCategory];
    //    UILabel *proctCategorylb = [[UILabel alloc]initWithFrame:CGRectMake(productCategory.right+left1, ornumber.bottom    +13, KWIDTH, 48)];
    //    proctCategorylb.textColor = K333333;
    //    proctCategorylb.font = FontSize(16);
    //    [self.scrollView addSubview:proctCategorylb];
    //    proctCategorylb.text = _mymodel.fault_name;
    //    self.myPuCategorylb =proctCategorylb;
    //
    //    UIButton *morebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    //    [self.scrollView addSubview:morebut];
    //    morebut.frame  =  CGRectMake(KWIDTH-50, ornumber.bottom+13, 40, 48);
    //    [morebut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    //    [morebut addTarget:self action:@selector(moreAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, proctCategorylb.bottom, KWIDTH, 2)];
    //    line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //    [self.scrollView addSubview:line1];
    //
    //    UIButton *newmorebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [self.scrollView addSubview:newmorebut];
    //    newmorebut.frame  =  CGRectMake(KWIDTH-40, productCategory.bottom, 40, 48);
    //    [newmorebut setImage:imgname(@"Cjinru") forState:(UIControlStateNormal)];
    //    //    [newmorebut addTarget:self action:@selector(newmorebutAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    //    UILabel *productnameStar = [[UILabel alloc]initWithFrame:CGRectMake(left1-10, productCategory.bottom+2, 10, 48)];
    //    productnameStar.text = @"*";
    //    productnameStar.textColor = [UIColor redColor];
    //    [self.scrollView addSubview:productnameStar];
    //    UILabel *productname = [[UILabel alloc]initWithFrame:CGRectMake(left1, productCategory.bottom+2, 105, 48)];
    //    productname.text = @"故障配件:";
    //    productname.textColor = K333333;
    //    productname.font = FontSize(16);
    //    [self.scrollView addSubview:productname];
    //
    //    self.myPuCName = [[UITextField alloc]initWithFrame:CGRectMake(productname.right+left1, productCategory.bottom+2, KWIDTH-134, 48)];
    //    self.myPuCName.placeholder = @"请选择故障配件";
    //    self.myPuCName.font = FontSize(16);
    //    [self.scrollView addSubview: self.myPuCName];
    //
    //    UIButton *reanewmorebut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [self.scrollView addSubview:reanewmorebut];
    //    reanewmorebut.frame  =  CGRectMake(productname.right+left1, productCategory.bottom+2, KWIDTH-102, 48);
    //    [reanewmorebut addTarget:self action:@selector(reanewmorebutAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //
    //    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, productname.bottom, KWIDTH, 2)];
    //    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //    [self.scrollView addSubview:line2];
    
//    UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line0.bottom+16, 70, 16)];
//    guzhang.text = @"故障描述:";
//    guzhang.font = FontSize(16);
//    guzhang.textColor = K333333;
//    [self.scrollView addSubview:guzhang];
//
//    self.mytextView = [[UITextView alloc]initWithFrame:CGRectMake(guzhang.right+16, line0.bottom+16, KWIDTH-guzhang.right-16-16, 78)];
//    self.mytextView.layer.masksToBounds = YES;
//    self.mytextView.layer.cornerRadius = 4;
//    [self.scrollView addSubview:self.mytextView];
//    self.mytextView.backgroundColor = [UIColor colorWithHexString:@"#E9E9E9"];
//    self.mytextView.delegate = self;
//
//    UILabel *plLB = [[UILabel alloc]initWithFrame:CGRectMake(15, 11, KWIDTH, 12)];
//    plLB.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
//    plLB.text = @"请填写产品具体故障";
//    plLB.font = FontSize(12);
//    self.plLB = plLB;
//    [self.mytextView addSubview:plLB];
//
//    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0,  self.mytextView.bottom+16, KWIDTH, 2)];
//    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
//    [self.scrollView addSubview:line3];
    
    //星星
    UILabel *starlab = [[UILabel alloc] init];
    starlab.frame = CGRectMake(left1,line0.bottom,9,32);
    starlab.numberOfLines = 0;
    starlab.text = @"*";
    starlab.font = FontSize(16);
    starlab.textColor = [UIColor colorWithHexString:@"#FF000A"];
    [self.scrollView addSubview:starlab];
    
    UILabel *winding = [[UILabel alloc]initWithFrame:CGRectMake(starlab.right+6, line0.bottom, KWIDTH-32, 32)];
    [self.scrollView addSubview:winding];
    //winding.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    winding.font = FontSize(16);
    winding.text = @"请添加完工照片";
    
    self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, winding.bottom, KWIDTH, 90)];
    [self.scrollView addSubview: self.addView];
    CGFloat www = (KWIDTH -(8*7))/4;
    self.bottomf = winding.bottom;
    [self showinageWirhArr:_imageArr];
    
    CGRect ffff = self.addView.frame;
    ffff.size.height = www +10;
    self.addView.frame = ffff;
    
    UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(0,  self.addView.bottom+16, KWIDTH, 2)];
    line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line4];
    
//    UILabel *iserciStar = [[UILabel alloc]initWithFrame:CGRectMake(left1-10, line4.bottom, 10, 48)];
//    iserciStar.text = @"*";
//    iserciStar.textColor = [UIColor redColor];
//    [self.scrollView addSubview:iserciStar];
//    UILabel *iserci = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom, 105, 48)];
//    [self.scrollView addSubview:iserci];
//    iserci.textColor = K333333;//[UIColor colorWithHexString:@"#033333"];
//    iserci.font = FontSize(16);
//    iserci.text = @"是否二次上门:";
//    UIButton *isercibut = [UIButton buttonWithType:(UIButtonTypeCustom)];
//    isercibut.frame = CGRectMake(iserci.right+10, line4.bottom, 50, 48);
//    [isercibut setTitle:@" 是" forState:(UIControlStateNormal)];
//    [isercibut setTitleColor:K666666 forState:(UIControlStateNormal)];
//    [isercibut setImage:imgname(@"shopCar_normal") forState:(UIControlStateNormal)];
//    [self.scrollView addSubview:isercibut];
//    [isercibut addTarget:self action:@selector(isAction:) forControlEvents:(UIControlEventTouchUpInside)];
//    UIButton *ercibut = [UIButton buttonWithType:(UIButtonTypeCustom)];
//    ercibut.frame = CGRectMake(isercibut.right+10, line4.bottom, 50, 48);
//    [ercibut setTitleColor:K666666 forState:(UIControlStateNormal)];
//    [ercibut setTitle:@" 否" forState:(UIControlStateNormal)];
//    [ercibut setImage:imgname(@"xuanze") forState:(UIControlStateNormal)];
//    [self.scrollView addSubview:ercibut];
//    [ercibut addTarget:self action:@selector(noAction:) forControlEvents:(UIControlEventTouchUpInside)];
//    self.oldbut = ercibut;
    
//    UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(0,iserci.bottom, KWIDTH, 2)];
//    line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
//    [self.scrollView addSubview:line5];
    
    //    UILabel *yuguStar = [[UILabel alloc]initWithFrame:CGRectMake(left1-10, iserci.bottom+2, 10, 48)];
    //    yuguStar.text = @"*";
    //    yuguStar.textColor = [UIColor redColor];
    //    [self.scrollView addSubview:yuguStar];
    //    UILabel *yugu = [[UILabel alloc]initWithFrame:CGRectMake(left1, iserci.bottom+2, 102, 48)];
    //    yugu.textColor = K333333;
    //    yugu.text = @"预估费用:";
    //    yugu.font = FontSize(16);
    //    [self.scrollView addSubview:yugu];
    //    UILabel *yugu1 = [[UILabel alloc]initWithFrame:CGRectMake(102, iserci.bottom+2, 12, 48)];
    //    yugu1.textColor = K666666;
    //    yugu1.text = @"¥";
    //    yugu1.font = FontSize(14);
    //    [self.scrollView addSubview:yugu1];
    //
    //    self.myyuge = [[UITextField alloc]initWithFrame:CGRectMake(yugu1.right, iserci.bottom+2, KWIDTH-134, 48)];
    //    self.myyuge.placeholder = @"请输入预估费用";
    //    [self.myyuge addRules];
    //    [self.scrollView addSubview: self.myyuge];
    //    self.myyuge.keyboardType = UIKeyboardTypeNumberPad;
    //    self.myyuge.delegate = self;
    //
    //    UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(0,yugu.bottom, KWIDTH, 2)];
    //    line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //    [self.scrollView addSubview:line6];
    //
    //    UILabel *checkLStar = [[UILabel alloc]initWithFrame:CGRectMake(left1-10, yugu.bottom+2, 10, 48)];
    //    checkLStar.text = @"*";
    //    checkLStar.textColor = [UIColor redColor];
    //    [self.scrollView addSubview:checkLStar];
    //    UILabel *checkL = [[UILabel alloc]initWithFrame:CGRectMake(left1, yugu.bottom+2, 102, 48)];
    //    checkL.textColor = K333333;
    //    checkL.text = @"检测费用:";
    //    checkL.font = FontSize(16);
    //    [self.scrollView addSubview:checkL];
    //    UILabel *checkUnitL = [[UILabel alloc]initWithFrame:CGRectMake(102, yugu.bottom+2, 12, 48)];
    //    checkUnitL.textColor = K666666;
    //    checkUnitL.text = @"¥";
    //    checkUnitL.font = FontSize(14);
    //    [self.scrollView addSubview:checkUnitL];
    //
    //    self.checkMoneyTF = [[UITextField alloc]initWithFrame:CGRectMake(checkUnitL.right, yugu.bottom+2, KWIDTH-134, 48)];
    //    self.checkMoneyTF.placeholder = @"请输入检测费";
    //    [self.checkMoneyTF addRules];
    //    [self.scrollView addSubview: self.checkMoneyTF];
    //    self.checkMoneyTF.keyboardType = UIKeyboardTypeNumberPad;
    //    self.checkMoneyTF.delegate = self;
    
    CGFloat heigehtbg = KHEIGHT -line4.bottom - 16;
    
    if (heigehtbg < 147) {
        heigehtbg = 147;
    }
    
    UIView *butBg = [[UIView alloc]initWithFrame:CGRectMake(0,  line4.bottom, KWIDTH, heigehtbg)];
    [self.scrollView addSubview:butBg];butBg.userInteractionEnabled = YES;
    butBg.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    //    //提示
    //    UIImageView *tipsIV = [[UIImageView alloc] initWithFrame:CGRectMake(left1, checkL.bottom+5, 16, 16)];
    //    tipsIV.image = [UIImage imageNamed:@"组 2812"];
    //    [self.scrollView addSubview:tipsIV];
    //
    //    UILabel *tipsL = [[UILabel alloc]initWithFrame:CGRectMake(left1+20, checkL.bottom+5, 200, 16)];
    //    tipsL.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    //    tipsL.font = FontSize(12);
    //    tipsL.text = @"用户停止维修，需支付检测费用。";
    //    [self.scrollView addSubview:tipsL];
    
    UIButton *upBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [upBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
    [upBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [upBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    upBut.frame = CGRectMake(28, 42, KWIDTH-28-28, 48);
    upBut.layer.masksToBounds = YES;
    upBut.layer.cornerRadius = 4;
    [butBg addSubview:upBut];
    [upBut addTarget:self action:@selector(upAciotn:) forControlEvents:(UIControlEventTouchUpInside)];
    _scrollView.contentSize = CGSizeMake(KWIDTH, butBg.bottom);
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
}

- (void)reanewmorebutAction:(UIButton *)but{
    //选择三级配件
    //    _mymodel.fault_type
    NewSelenameViewController *new = [[NewSelenameViewController alloc]init];
    new.type = _mymodel.fault_category;
    new.myblock = ^(NSString * _Nonnull str) {
        self.myPuCName.text = str;
    };
    [self.navigationController pushViewController:new animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField ==  self.myPuCName) {
        return NO;
    }
    if (textField == self.myyuge) {
        
        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
            ShowToastWithText(@"只能输入数字");
            return NO;
            
        }else{
            return YES;
        }
    }else{
        return YES;
    }
}

-(void)upAciotn:(UIButton *)but{
    //    if (strIsEmpty(_myPuCategorylb.text)) {
    //        ShowToastWithText(@"请选择故障配件品类");
    //        return;
    //    }
    //    if (strIsEmpty(_myPuCName.text)) {
    //        ShowToastWithText(@"请输入故障配件名称");
    //        return;
    //    }
    //    if (strIsEmpty(_mytextView.text)) {
    //        ShowToastWithText(@"请填写产品具体故障");
    //        return;
    //    }
    //    if (strIsEmpty(_myyuge.text)) {
    //        ShowToastWithText(@"请填写预估费用");
    //        return;
    //    }
    //    if (strIsEmpty(_checkMoneyTF.text)) {
    //        ShowToastWithText(@"请填写检测费");
    //        return;
    //    }
    [self updatedateile];
}

-(void)updatedateile{
    
    UIImageView *myyy = [self.imageArr  lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }
    
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic = self.paramDic;
    mudic[@"orderId"] =NOTNIL(self.mymodel.orderId);

    
    NSString *str  = [HFTools toJSONString:mudic];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = str;
    
    NSLog(@"%@", mudic);
    NSLog(@"%@", param);
    
    if (_imageArr.count>0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NetWorkTool UploadPicWithUrl:addPeiJian param:param key:@"images" image:_imageArr withSuccess:^(id dic) {
            KMyLog(@"____________%@",dic);
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
            if ([dic[@"errorCode"] intValue]==0) {
                ZSTipsView *view = [ZSTipsView new];
                view.upLable.text = @"账单推送成功";
                view.btnBlock = ^{
                    [singlTool shareSingTool].needReasfh = YES;
                    UIViewController *controller = self.navigationController.viewControllers[0];
                    [self.navigationController popToViewController:controller animated:YES];
                };
                [view show];
                //[[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];
            } else {
                [NetWorkTool showOnleText:dic[@"msg"] delay:3];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
        }];
    } else {
        ShowToastWithText(@"请添加完工照片");
        [self.imageArr addObject:self.myimage];
    }
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = self.view.bounds;
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"明天继续努力";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [self.bgViewsec removeFromSuperview];
    
    [singlTool shareSingTool].needReasfh = YES;
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[CSOrderContentViewController class]]) {
            CSOrderContentViewController *vc = (CSOrderContentViewController *)controller;
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
    //    if (self.myblock) {
    //        self.myblock(@"");
    //    }
    //    [self.navigationController popViewControllerAnimated:YES];
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind]  removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}
-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
        
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    //    self.bottomf = 0;
    CGFloat   www = (KWIDTH -(8*7))/4;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(8+(www+8)*i, self.bottomf+10, www, www);
        [self.scrollView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 4) {
                [image removeFromSuperview];
                //                UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scanBigImageClick1:)];
                //                [image addGestureRecognizer:tapGestureRecognizer1];
                
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        } else {
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

-(void)isAction:(UIButton *)button{
    
    [self.oldbut setImage:imgname(@"shopCar_normal") forState:(UIControlStateNormal)];
    
    [button setImage:imgname(@"xuanze") forState:(UIControlStateNormal)];
    self.oldbut  = button;
    self.issecont = @"1";
    
}

-(void)noAction:(UIButton *)button{
    [self.oldbut setImage:imgname(@"shopCar_normal") forState:(UIControlStateNormal)];
    
    [button setImage:imgname(@"xuanze") forState:(UIControlStateNormal)];
    self.oldbut  = button;
    self.issecont = @"0";
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.placeLab.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length < 1) {
        self.placeLab.hidden = NO;
    }
}

-(void)moreAction:(UIButton *)but{
    KMyLog(@"选择配件");
    SeleproductTypeViewController *seleVC = [[SeleproductTypeViewController alloc]init];
    seleVC.myblock = ^(NSString * _Nonnull str, NSString * _Nonnull idstr) {
        self.myPuCategorylb.text = str;
        self.myPuCategorID = idstr;
    };
    [self.navigationController pushViewController:seleVC animated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}


@end


