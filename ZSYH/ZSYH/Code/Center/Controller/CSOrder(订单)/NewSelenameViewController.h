//
//  NewSelenameViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewSelenameViewController : BaseViewController
@property(nonatomic,strong)NSString *type;
@property(nonatomic,copy)void (^myblock)(NSString *str);
@property(nonatomic,copy)void (^selblock)(NSString *str, NSString *idstr);
@end

NS_ASSUME_NONNULL_END
