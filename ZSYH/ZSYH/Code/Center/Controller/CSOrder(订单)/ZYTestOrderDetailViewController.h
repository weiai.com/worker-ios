//
//  ZYTestOrderDetailViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYTestOrderDetailViewController : BaseViewController
@property (nonatomic, strong) CSmaintenanceListModel *mymodel;
@property (nonatomic, strong) CSlistModel *cslistModel;
@property (nonatomic, strong) NSString *idStr;
@property (nonatomic, strong) NSString *strPush;

@end

NS_ASSUME_NONNULL_END
