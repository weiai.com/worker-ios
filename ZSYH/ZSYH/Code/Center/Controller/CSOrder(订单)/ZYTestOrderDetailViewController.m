//
//  ZYTestOrderDetailViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestOrderDetailViewController.h"
#import "CSmaintenanceListModel.h"
#import "ZYTestLookJCBGViewController.h" //检测单 检测报告
#import "ZYTestLookZDViewController.h"   //检测单 查看账单

#import "OrderViewController.h"

@interface ZYTestOrderDetailViewController ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;

@property (nonatomic, strong) UILabel *stateLab;    //状态
@property (nonatomic, strong) UILabel *cslxdhCon;   //厂家联系电话
@property (nonatomic, strong) UILabel *csmcConLab;  //厂家名称
@property (nonatomic, strong) UILabel *fwlxConLab;  //服务类型
@property (nonatomic, strong) UILabel *dqplConLab;  //电器品类
@property (nonatomic, strong) UILabel *gzyyConLab;  //故障原因
@property (nonatomic, strong) UILabel *bzxxConLab;  //备注信息
@property (nonatomic, strong) UILabel *yyrqConLab;  //预约日期
@property (nonatomic, strong) UILabel *yysjConLab;  //预约时间
@property (nonatomic, strong) UILabel *addrConLab;  //地址
@property (nonatomic, strong) UILabel *name;        //名字
@property (nonatomic, strong) UILabel *phone;       //手机
@property (nonatomic, strong) UILabel *diantilb;    //是否有电梯
@property (nonatomic, strong) UILabel *loucenglb;   //楼层
@property (nonatomic, strong) UILabel *smfConLab;   //上门费
@property (nonatomic, strong) UILabel *orderNumLab; //订单编号
@property (nonatomic, strong) UIButton *jcbgBtn;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) UIButton *ckzdBtn;
@end

@implementation ZYTestOrderDetailViewController
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //禁用侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"检测单详情";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //从检测单列表跳转进来 返回按钮
    //[self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    //接完单之后跳转进来 返回按钮
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    leftBtn.frame = CGRectMake(0, 0, 30,30);
    UIImage *img = [[UIImage imageNamed:@"left_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];

    [self requestData];
}

//接完单之后跳转到检测单详情页面 再返回 回到检测单列表页面
- (void)leftBarBtnClicked:(UIButton *)button {
    
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)requestData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = self.idStr;
    
    [weakSelf.mydateSource removeAllObjects];
    [NetWorkTool POST:getTestOrderById param:param success:^(id dic) {
        NSDictionary *dataDic = dic[@"data"];
        CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc]init];
        [model setValuesForKeysWithDictionary:dataDic];
        [self testOrderDetailUI:model];
        self.orderId = model.Id;
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)testOrderDetailUI:(CSmaintenanceListModel *)model {
    
    [self.view addSubview:self.scrollView];

    //顶部 背景View
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    topView.frame = CGRectMake(10, kNaviHeight +10, KWIDTH-20, kScaleNum(72));
    [self.scrollView addSubview:topView];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect: topView.bounds byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii:CGSizeMake(12,12)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init]; //创建 layer
    maskLayer.frame = topView.bounds;
    maskLayer.path = maskPath.CGPath;//赋值
    topView.layer.mask = maskLayer;
    //状态 Label
    UILabel *stateLab = [[UILabel alloc] init];
    stateLab.frame = CGRectMake(16, 10, 52, 20);
    //订单状态 0未接单 1已接单 2已检测 3已支付 4已取消
    if ([model.order_state isEqualToString:@"0"]) {
        stateLab.text = @"未接单";
    } else if ([model.order_state isEqualToString:@"1"]) {
        stateLab.text = @"已接单";
    } else if ([model.order_state isEqualToString:@"2"]) {
        stateLab.text = @"已检测";
    } else if ([model.order_state isEqualToString:@"3"]) {
        stateLab.text = @"已支付";
    } else if ([model.order_state isEqualToString:@"4"]) {
        stateLab.text = @"已取消";
    }
    stateLab.font = FontSize(14);
    stateLab.textColor = [UIColor colorWithHexString:@"#F88B1F"];
    [topView addSubview:stateLab];
    self.stateLab = stateLab;
    //检测报告 按钮
    UIButton *jcbgBtn = [[UIButton alloc] init];
    jcbgBtn.frame = CGRectMake(KWIDTH/2 +10, 40, 69, 26);
    [jcbgBtn setImage:imgname(@"testjcbg") forState:UIControlStateNormal];
    [jcbgBtn addTarget:self action:@selector(chakanjiancebaogao:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:jcbgBtn];
    self.jcbgBtn = jcbgBtn;
    //查看账单 按钮
    UIButton *ckzdBtn = [[UIButton alloc] init];
    ckzdBtn.frame = CGRectMake(jcbgBtn.right +12, 40, 69, 26);
    [ckzdBtn setImage:imgname(@"chakanzhangdan-two") forState:UIControlStateNormal];
    [ckzdBtn addTarget:self action:@selector(chakanzhangdan:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:ckzdBtn];
    self.ckzdBtn = ckzdBtn;
    
    if ([model.order_state isEqualToString:@"0"]) {
        self.jcbgBtn.hidden = YES;
        self.ckzdBtn.hidden = YES;
    } else if ([model.order_state isEqualToString:@"1"]) {
        self.jcbgBtn.hidden = YES;
        self.ckzdBtn.hidden = YES;
    } else if ([model.order_state isEqualToString:@"2"]) {
        self.jcbgBtn.hidden = NO;
        self.ckzdBtn.hidden = NO;
    } else if ([model.order_state isEqualToString:@"3"]) {
        self.jcbgBtn.hidden = NO;
        self.ckzdBtn.hidden = NO;
    } else if ([model.order_state isEqualToString:@"4"]) {
        self.jcbgBtn.hidden = NO;
        self.ckzdBtn.hidden = NO;
    }
    
    //中部 背景View
    UIView *centerView = [[UIView alloc] init];
    centerView.backgroundColor = [UIColor whiteColor];
    centerView.frame = CGRectMake(10, topView.bottom+2, KWIDTH-20, 44);
    [self.scrollView addSubview:centerView];
    //厂商联系电话
    UILabel *cslxdhLab = [[UILabel alloc] init];
    cslxdhLab.frame = CGRectMake(16, 13, 94, 20);
    cslxdhLab.text = @"厂商联系电话";
    cslxdhLab.font = FontSize(14);
    cslxdhLab.textColor = K666666;
    [centerView addSubview:cslxdhLab];
    //厂商联系电话
    UIButton *cslxdhBtn = [UIButton new];
    cslxdhBtn.frame = CGRectMake(centerView.right-169, 7, 30, 30);
    [cslxdhBtn setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
    [cslxdhBtn addTarget:self action:@selector(csCallBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [centerView addSubview:cslxdhBtn];
    //厂商联系电话
    UILabel *cslxdhCon = [[UILabel alloc] init];
    cslxdhCon.frame = CGRectMake(cslxdhBtn.right +10, 13, 102, 20);
    cslxdhCon.text = model.company_phone;
    cslxdhCon.font = KFontPingFangSCMedium(14);
    cslxdhCon.textColor = K666666;
    [centerView addSubview:cslxdhCon];
    UITapGestureRecognizer *tapg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(csCallLab:)];
    cslxdhCon.userInteractionEnabled = YES;
    [cslxdhCon addGestureRecognizer:tapg];
    self.cslxdhCon = cslxdhCon;
    
    //下面 背景View
    UIView *botView = [[UIView alloc] init];
    botView.backgroundColor = [UIColor whiteColor];
    botView.frame = CGRectMake(10, centerView.bottom+2, KWIDTH-20, kScaleNum(502));
    [self.scrollView addSubview:botView];

    //厂商名称 标题
    UILabel *csmcTitLab = [[UILabel alloc] init];
    csmcTitLab.frame = CGRectMake(16, 17, 70, 20);
    csmcTitLab.text = @"厂商名称";
    csmcTitLab.font = FontSize(14);
    csmcTitLab.textColor = K666666;
    [botView addSubview:csmcTitLab];
    //厂商名称 内容
    UILabel *csmcConLab = [[UILabel alloc] init];
    csmcConLab.frame = CGRectMake(csmcTitLab.right +5, 17, KWIDTH-20-32-70-5, 20);
    csmcConLab.text = model.company_name;
    csmcConLab.textColor = [UIColor colorWithHexString:@"#F88B1F"];
    csmcConLab.font = KFontPingFangSCMedium(14);
    [botView addSubview:csmcConLab];
    self.csmcConLab = csmcConLab;
    //下划线
    UILabel *line1 = [[UILabel alloc] init];
    line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line1.frame = CGRectMake(16, csmcTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line1];
    
    //服务类型 标题
    UILabel *fwlxTitLab = [[UILabel alloc] init];
    fwlxTitLab.frame = CGRectMake(16, line1.bottom +5, 70, 20);
    fwlxTitLab.text = @"服务类型";
    fwlxTitLab.font = FontSize(14);
    fwlxTitLab.textColor = K666666;
    [botView addSubview:fwlxTitLab];
    //服务类型 内容
    UILabel *fwlxConLab = [[UILabel alloc] init];
    fwlxConLab.frame = CGRectMake(csmcTitLab.right +5, line1.bottom +5, KWIDTH-20-32-70-5, 20);
    fwlxConLab.text = model.service_name;
    fwlxConLab.font = KFontPingFangSCMedium(14);
    fwlxConLab.textColor = K333333;
    [botView addSubview:fwlxConLab];
    self.fwlxConLab = fwlxConLab;
    //下划线
    UILabel *line2 = [[UILabel alloc] init];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line2.frame = CGRectMake(16, fwlxTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line2];
    
    //电器品类 标题
    UILabel *dqplTitLab = [[UILabel alloc] init];
    dqplTitLab.frame = CGRectMake(16, line2.bottom +5, 70, 20);
    dqplTitLab.text = @"电器品类";
    dqplTitLab.font = FontSize(14);
    dqplTitLab.textColor = K666666;
    [botView addSubview:dqplTitLab];
    //电器品类 内容
    UILabel *dqplConLab = [[UILabel alloc] init];
    dqplConLab.frame = CGRectMake(dqplTitLab.right +5, line2.bottom +5, KWIDTH-20-32-70-5, 20);
    dqplConLab.text = model.fault_name;
    dqplConLab.font = KFontPingFangSCMedium(14);
    dqplConLab.textColor = K333333;
    [botView addSubview:dqplConLab];
    //下划线
    UILabel *line3 = [[UILabel alloc] init];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line3.frame = CGRectMake(16, dqplTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line3];
    
    //故障原因 标题
    UILabel *gzyyTitLab = [[UILabel alloc] init];
    gzyyTitLab.frame = CGRectMake(16, line3.bottom +5, 70, 20);
    gzyyTitLab.text = @"故障原因";
    gzyyTitLab.font = FontSize(14);
    gzyyTitLab.textColor = K666666;
    [botView addSubview:gzyyTitLab];
    //电器品类 内容
    UILabel *gzyyConLab = [[UILabel alloc] init];
    gzyyConLab.frame = CGRectMake(gzyyTitLab.right +5, line3.bottom +5, KWIDTH-20-32-70-5, 20);
    gzyyConLab.text = model.common_failures;
    gzyyConLab.font = KFontPingFangSCMedium(14);
    gzyyConLab.textColor = K333333;
    [botView addSubview:gzyyConLab];
    self.gzyyConLab = gzyyConLab;

    //下划线
    UILabel *line4 = [[UILabel alloc] init];
    line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line4.frame = CGRectMake(16, gzyyTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line4];
    
    //备注信息 标题
    UILabel *bzxxTitLab = [[UILabel alloc] init];
    bzxxTitLab.frame = CGRectMake(16, line4.bottom +5, 70, 20);
    bzxxTitLab.text = @"备注信息";
    bzxxTitLab.font = FontSize(14);
    bzxxTitLab.textColor = K666666;
    [botView addSubview:bzxxTitLab];
    //备注信息 内容
    CGFloat height1 = [NSString heightWithWidth:KWIDTH-20-32-70-5 font:14 text:model.fault_comment];
    UILabel *bzxxConLab = [[UILabel alloc] init];
    bzxxConLab.frame = CGRectMake(bzxxTitLab.right +5, line4.bottom +5, KWIDTH-20-32-70-5, height1);
    bzxxConLab.text = model.fault_comment;
    bzxxConLab.font = FontSize(14);
    bzxxConLab.textColor = K999999;
    bzxxConLab.numberOfLines = 0;
    [botView addSubview:bzxxConLab];
    self.bzxxConLab = bzxxConLab;
    
    //下划线
    UILabel *line5 = [[UILabel alloc] init];
    line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line5.frame = CGRectMake(16, bzxxConLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line5];
    
    //预约日期 标题
    UILabel *yyrqTitLab = [[UILabel alloc] init];
    yyrqTitLab.frame = CGRectMake(16, line5.bottom +5, 70, 20);
    yyrqTitLab.text = @"上门日期";
    yyrqTitLab.font = FontSize(14);
    yyrqTitLab.textColor = K666666;
    [botView addSubview:yyrqTitLab];
    //上门日期 内容
    UILabel *yyrqConLab = [[UILabel alloc] init];
    yyrqConLab.frame = CGRectMake(bzxxTitLab.right +5, line5.bottom +5, KWIDTH-20-32-70-5, 20);
    yyrqConLab.font = KFontPingFangSCMedium(14);
    yyrqConLab.textColor = K666666;
    [botView addSubview:yyrqConLab];
    self.yyrqConLab = yyrqConLab;
    
    if (model.order_date == nil) {
        self.yyrqConLab.hidden = YES;
    } else {
        NSMutableArray *muarrt = [NSMutableArray arrayWithArray:[model.order_date componentsSeparatedByString:@"-"]];
        if (muarrt.count>2) {
            NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
            NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
            NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
            yyrqConLab.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
        }
    }
    
    //下划线
    UILabel *line6 = [[UILabel alloc] init];
    line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line6.frame = CGRectMake(16, yyrqTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line6];
    
    //预约时间 标题
    UILabel *yysjTitLab = [[UILabel alloc] init];
    yysjTitLab.frame = CGRectMake(16, line6.bottom +5, 70, 20);
    yysjTitLab.text = @"上门时间";
    yysjTitLab.font =  FontSize(14);
    yysjTitLab.textColor = K666666;
    [botView addSubview:yysjTitLab];
    //预约时间 内容
    UILabel *yysjConLab = [[UILabel alloc] init];
    yysjConLab.frame = CGRectMake(bzxxTitLab.right +5, line6.bottom +5, KWIDTH-20-32-70-5, 20);
    yysjConLab.font = KFontPingFangSCMedium(14);
    yysjConLab.textColor = K666666;
    [botView addSubview:yysjConLab];
    self.yysjConLab = yysjConLab;
    
    if (model.order_time == nil) {
        self.yysjConLab.hidden = YES;
    } else {
        NSInteger indd = [[model.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        yysjConLab.text = [NSString stringWithFormat:@"%@ %@", ap, model.order_time];
    }
    //下划线
    UILabel *line7 = [[UILabel alloc] init];
    line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line7.frame = CGRectMake(16, yysjTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line7];
    
    //地址 标题
    UILabel *addrTitLab = [[UILabel alloc] init];
    addrTitLab.frame = CGRectMake(16, line7.bottom +5, 70, 20);
    addrTitLab.text = @"地址";
    addrTitLab.font = FontSize(14);
    addrTitLab.textColor = K666666;
    [botView addSubview:addrTitLab];
    //地址 内容
    CGFloat height2 = [NSString heightWithWidth:KWIDTH-20-32-70-5 font:14 text:model.user_address];
    UILabel *addrConLab = [[UILabel alloc] init];
    addrConLab.frame = CGRectMake(addrTitLab.right +5, line7.bottom +5, KWIDTH-20-32-70-5, height2);
    addrConLab.text = model.user_address;
    addrConLab.font = FontSize(14);
    addrConLab.textColor = K666666;
    addrConLab.numberOfLines = 0;
    [botView addSubview:addrConLab];
    self.addrConLab = addrConLab;
    //下划线
    UILabel *line8 = [[UILabel alloc] init];
    line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line8.frame = CGRectMake(16, addrConLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line8];
    
    //用户名字 标题
    UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(16, line8.bottom+5, 30, 20)];
    userLab.font = FontSize(14);
    userLab.textColor = K333333;
    userLab.text = @"用户";
    [botView addSubview:userLab];

    //用户名字 内容
    UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line8.bottom+5, 60, 20)];
    name.font = KFontPingFangSCMedium(14);
    name.textColor = K333333;
    name.text = model.user_name;
    [botView addSubview:name];
    self.name = name;
    //手机 标题
    UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(botView.right-246, line8.bottom+5, 75, 20)];
    phoneA.font = FontSize(14);
    phoneA.textColor = K999999;
    phoneA.text = @"用户手机号";
    [botView addSubview:phoneA];
    //手机 图片
    UIButton *phoneB = [UIButton new];
    phoneB.frame = CGRectMake(botView.right-169, line8.bottom +1, 30, 30);
    [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
    [phoneB addTarget:self action:@selector(callUserBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [botView addSubview:phoneB];
    //手机号 内容
    UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right+10, line8.bottom+5, 102, 20)];
    phone.font = KFontPingFangSCMedium(14);
    phone.textColor = K333333;
    phone.textAlignment = NSTextAlignmentLeft;
    phone.text = model.user_phone;
    [botView addSubview:phone];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callUserLab:)];
    phone.userInteractionEnabled = YES;
    [phone addGestureRecognizer:tap];
    self.phone = phone;

    UILabel *line9 = [[UILabel alloc]initWithFrame:CGRectMake(16, phone.bottom+7, KWIDTH-16-20, 1)];
    line9.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [botView addSubview:line9];
    //电梯 标题
    UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(16, line9.bottom+7, 30, 20)];
    dianti.font = FontSize(14);
    dianti.textColor = K999999;
    dianti.text = @"电梯";
    [botView addSubview:dianti];
    //电梯 内容
    UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line9.bottom+7, 30, 20)];
    diantilb.font = KFontPingFangSCMedium(14);
    diantilb.textColor = K333333;
    diantilb.text = @"无";
    if ([model.has_elevator integerValue] == 1) {
        diantilb.text = @"有";
    }
    [botView addSubview:diantilb];
    self.diantilb = diantilb;
    //楼层 标题
    UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-115, line9.bottom+7, 30, 20)];
    louceng.font = FontSize(14);
    louceng.textColor = K999999;
    louceng.text = @"楼层";
    [botView addSubview:louceng];
    //楼层 内容
    UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-70, line9.bottom+7, 35, 20)];
    loucenglb.font = KFontPingFangSCMedium(14);
    loucenglb.textColor = K333333;
    loucenglb.text = [NSString stringWithFormat:@"%@楼",model.floorNum];
    [botView addSubview:loucenglb];
    self.loucenglb = loucenglb;
    
    UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(16, dianti.bottom+7, KWIDTH-16-20, 1)];
    line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [botView addSubview:line10];
    
    //上门费
    UILabel *smfTitLab = [[UILabel alloc] init];
    smfTitLab.frame = CGRectMake(16, line10.bottom +5, 70, 20);
    smfTitLab.text = @"上门费";
    smfTitLab.font = FontSize(14);
    smfTitLab.textColor = K666666;
    [botView addSubview:smfTitLab];
    //上门费 内容
    UILabel *smfConLab = [[UILabel alloc] init];
    smfConLab.frame = CGRectMake(smfTitLab.right +5, line10.bottom +5, KWIDTH-20-32-70-5, 20);
    smfConLab.text = [NSString stringWithFormat:@"%@%@", @"¥", model.fixed_price];
    smfConLab.font = KFontPingFangSCMedium(14);
    smfConLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
    [botView addSubview:smfConLab];
    self.smfConLab = smfConLab;
    //下划线
    UILabel *line11 = [[UILabel alloc] init];
    line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    line11.frame = CGRectMake(16, smfTitLab.bottom +7, KWIDTH-20-16, 1);
    [botView addSubview:line11];
    
    //订单编号
    UILabel *orderNumLab = [[UILabel alloc] init];
    orderNumLab.frame = CGRectMake(16, line11.bottom +5, KWIDTH-20-32, 20);
    orderNumLab.text = [NSString stringWithFormat:@"%@%@", @"订单编号: ", model.Id];
    orderNumLab.font = FontSize(12);
    orderNumLab.textColor = [UIColor colorWithHexString:@"#979797"];
    [botView addSubview:orderNumLab];
    self.orderNumLab = orderNumLab;
    
    botView.frame = CGRectMake(10, centerView.bottom+2, KWIDTH-20, orderNumLab.bottom +30);
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect: botView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(12,12)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init]; //创建 layer
    maskLayer1.frame = botView.bounds;
    maskLayer1.path = maskPath1.CGPath;//赋值
    botView.layer.mask = maskLayer1;
}

//查看检测报告 按钮 点击事件处理
- (void)chakanjiancebaogao:(CSmaintenanceListModel *)model {
    ZYTestLookJCBGViewController *vc = [[ZYTestLookJCBGViewController alloc] init];
    vc.model = model;
    vc.orderId = self.orderId;
    KMyLog(@"有没有把订单id传过去 %@", self.orderId);
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"您点击了 查看检测报告 按钮");
}

//查看账单 按钮 点击事件处理
- (void)chakanzhangdan:(UIButton *)button {
    ZYTestLookZDViewController *vc = [[ZYTestLookZDViewController alloc] init];
    vc.orderId = self.orderId;
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"您点击了 查看账单 按钮");
}

- (void)csCallBtn:(UIButton *)botton {
    if ([self.strPush isEqualToString:@"1"]) { //接完单之后Push过来
        [HFTools callMobilePhone:_cslistModel.company_phone];
    } else if ([self.strPush isEqualToString:@"2"]) {
        [HFTools callMobilePhone:_mymodel.company_phone];
    }
    KMyLog(@"您点击了 拨打 厂商电话按钮");
}

- (void)csCallLab:(UIButton *)botton {
    if ([self.strPush isEqualToString:@"1"]) { //接完单之后Push过来
        [HFTools callMobilePhone:_cslistModel.company_phone];
    } else if ([self.strPush isEqualToString:@"2"]) {
        [HFTools callMobilePhone:_mymodel.company_phone];
    }    KMyLog(@"您点击了 拨打 厂商电话Label");
}

- (void)callUserBtn:(UIButton *)button {
    if ([self.strPush isEqualToString:@"1"]) { //接完单之后Push过来
        [HFTools callMobilePhone:_cslistModel.company_phone];
    } else if ([self.strPush isEqualToString:@"2"]) {
        [HFTools callMobilePhone:_mymodel.user_phone];
    }    KMyLog(@"您点击了 拨打 用户电话按钮");
}

-(void)callUserLab:(UIButton *)button {
    if ([self.strPush isEqualToString:@"1"]) { //接完单之后Push过来
        [HFTools callMobilePhone:_cslistModel.company_phone];
    } else if ([self.strPush isEqualToString:@"2"]) {
        [HFTools callMobilePhone:_mymodel.user_phone];
    }    KMyLog(@"您点击了 拨打 用户手机号Label");
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT+100);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
