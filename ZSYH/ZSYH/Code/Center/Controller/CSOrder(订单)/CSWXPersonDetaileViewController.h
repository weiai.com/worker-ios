//
//  CSWXPersonDetaileViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSWXPersonDetaileViewController : BaseViewController
@property(nonatomic,strong)CSmaintenanceListModel *mymodel;
@property(nonatomic,strong)NSString *orderID;

@property(nonatomic,strong)NSString *shouhou;


@end

NS_ASSUME_NONNULL_END
