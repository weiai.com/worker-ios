//
//  ZYDQCPaulineGenerateBillsViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYDQCPaulineGenerateBillsViewController : BaseViewController

@property(nonatomic,strong)NSMutableDictionary *paramDic;
@property(nonatomic,strong)CSmaintenanceListModel *mymodel;
@property(nonatomic,copy)void (^myblock)(NSString *str);

@end

NS_ASSUME_NONNULL_END
