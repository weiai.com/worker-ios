//
//  ZYElectricalFactoryServiceOrderDetailsVC.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/30.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"
#import "CSlistModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYElectricalFactoryServiceOrderDetailsVC : BaseViewController
@property(nonatomic,strong)CSmaintenanceListModel *mymodel;
@property(nonatomic,strong)CSlistModel *listmodel;

@property(nonatomic,strong)NSString *orderID;

@property(nonatomic,strong)NSString *shouhou;

@end

NS_ASSUME_NONNULL_END
