//
//  ZYTestLookZDViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestLookZDViewController.h"
#import "SYBigImage.h"

@interface ZYTestLookZDViewController ()
@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) UIView *whiteBackView;
@property(nonatomic,strong) UILabel *peijian;//本单费用-标题
@property(nonatomic,strong) UILabel *peijianlb;//本单费用-内容
@property(nonatomic,strong) UITextView *pjNameCTV;
@property(nonatomic,strong) UILabel *pjNameTLab;

@property(nonatomic,strong) UIView *peijianLine;
@property(nonatomic,strong) UILabel *firstOrderFeiTitleLabel;//本日首单-标题
@property(nonatomic,strong) UILabel *firstOrderFeiContentLabel;//本日首单-内容
@property(nonatomic,strong) UILabel *firstOrderLine;//本日首单-分割线
@property(nonatomic,strong) UIView *imageBackView;//放图片的背景
@property(nonatomic,strong) UILabel *imageViewLine;
@property(nonatomic,strong) UILabel *myshouyi;//本单收益-标题
@property(nonatomic,strong) UILabel *myshouyilb;//本单收益-内容
@property(nonatomic,strong) UILabel *myShouYiLine;
@property(nonatomic,strong) UILabel *payTime;
@property(nonatomic,strong) NSDictionary *mydic;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) UILabel *line;

@end

@implementation ZYTestLookZDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单信息";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
  
}

-(void)request{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"id"] = self.orderId;
    [NetWorkTool POST:getTestOrderById param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
  
        [self showDetaileUi];
        KMyLog(@"检测单查看账单%@",dic);
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showDetaileUi{
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, KHEIGHT-20)];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview: self.bgView];
    self.bgView.backgroundColor = zhutiColor;
    
    [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];
    
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
    order.textColor = [UIColor whiteColor];
    order.font = FontSize(16);
    order.text = @"订单编号";
    [self.bgView addSubview:order];
    
    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
    orderlb.textColor = [UIColor whiteColor];
    orderlb.font = FontSize(16);
    orderlb.text = [_mydic objectForKey:@"id"];
    [self.bgView addSubview:orderlb];
    
    UILabel *peijian = [[UILabel alloc]initWithFrame:CGRectMake(24, 16, 100, 14)];
    peijian.textColor = K666666;
    peijian.font = FontSize(14);
    peijian.text = @"上门费";
    self.peijian = peijian;
    [self.whiteBackView addSubview:peijian];
    
    UILabel *peijianlb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, peijian.top, 100, 14)];
    peijianlb.textColor = K666666;
    peijianlb.textAlignment = NSTextAlignmentRight;
    peijianlb.font = FontSize(14);
    peijianlb.text = [NSString stringWithFormat:@"¥ %.2f",[[_mydic objectForKey:@"fixed_price"] floatValue]];
    self.peijianlb = peijianlb;
    [self.whiteBackView addSubview:peijianlb];
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, peijianlb.bottom+10, KWIDTH-20, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.whiteBackView addSubview:line];
    self.line = line;

#pragma mark ***查看收益
    if ([_mydic[@"order_state"] integerValue] < 3) {//未支付
        self.whiteBackView.height = self.peijianlb.bottom + 20;
        self.bgView.height = self.whiteBackView.bottom;
        self.line.hidden = YES;
        //设置所需的圆角位置以及大小
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.whiteBackView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.whiteBackView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.whiteBackView.layer.mask = maskLayer;
    } else {//已支付
        NSString *insurance_premium = [_mydic objectForKey:@"insurance_premium"];
        //insurance_premium = @"1";
        if ([insurance_premium floatValue] > 0) {//如果是本日首单
            [self firstOrderFeiContentLabel];//加载出来
            [self firstOrderLine];
            [self myshouyilb];
            self.whiteBackView.height = self.myshouyi.bottom + 10 + self.imageBackView.height;
            self.bgView.height = self.whiteBackView.bottom;
            [self payTime];
        } else {//不是本日首单
            [self myshouyiOne];
            [self myshouyilb];
            self.whiteBackView.height = self.myshouyi.bottom + 20;
            self.bgView.height = self.whiteBackView.bottom;
            [self payTime];
        }
        
//        self.myshouyi.frame = CGRectMake(24, line.bottom + 10, 100, 14);
//        [self myshouyilb];
//        self.whiteBackView.height = self.myshouyi.bottom + 20;
//        self.bgView.height = self.whiteBackView.bottom;
//        [self payTime];
//        self.scrollView.contentSize = CGSizeMake(KWIDTH, self.bgView.height + 13);
        //设置所需的圆角位置以及大小
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.whiteBackView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.whiteBackView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.whiteBackView.layer.mask = maskLayer;
    }
}

#pragma mark - 订单收益-标题
- (UILabel *)myshouyi {
    if (!_myshouyi) {
        UILabel *myshouyi = [[UILabel alloc]initWithFrame:CGRectMake(24, self.firstOrderLine.bottom + 16, 100, 14)];
        myshouyi.textColor = zhutiColor;
        myshouyi.font = FontSize(14);
        myshouyi.text = @"本单收益";
        _myshouyi = myshouyi;
        [self.whiteBackView addSubview:myshouyi];
    }
    return _myshouyi;
}

#pragma mark - 订单收益-标题
- (UILabel *)myshouyiOne {
    if (!_myshouyi) {
        UILabel *myshouyi = [[UILabel alloc]initWithFrame:CGRectMake(24, self.line.bottom + 16, 100, 14)];
        myshouyi.textColor = zhutiColor;
        myshouyi.font = FontSize(14);
        myshouyi.text = @"本单收益";
        _myshouyi = myshouyi;
        [self.whiteBackView addSubview:myshouyi];
    }
    return _myshouyi;
}

#pragma mark - 订单收益-内容
- (UILabel *)myshouyilb {
    if (!_myshouyilb) {
        UILabel *myshouyilb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.myshouyi.top, 100, 14)];
        myshouyilb.textColor = zhutiColor;
        myshouyilb.textAlignment = NSTextAlignmentRight;
        myshouyilb.font = FontSize(14);
        myshouyilb.text =[NSString stringWithFormat:@"¥ %.2f",[[_mydic objectForKey:@"earning"] floatValue]];
        [self.whiteBackView addSubview:myshouyilb];
        self.whiteBackView.height = myshouyilb.bottom+100;
        _myshouyilb = myshouyilb;
    }
    return _myshouyilb;
}

#pragma mark - 订单收益-分割线
- (UILabel *)myShouYiLine {
    if (!_myShouYiLine) {
        UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.myshouyi.bottom+10, KWIDTH-20, 1)];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _myShouYiLine = line2;
        [self.whiteBackView addSubview:line2];
    }
    return _myShouYiLine;
}

#pragma mark - 本日首单-标题
- (UILabel *)firstOrderFeiTitleLabel {
    if (!_firstOrderFeiTitleLabel) {
        UILabel *firstOrderFeeL = [[UILabel alloc]initWithFrame:CGRectMake(24, self.peijian.bottom + 10 + 16, 150, 14)];
        firstOrderFeeL.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        firstOrderFeeL.font = FontSize(14);
        firstOrderFeeL.text = @"当日首单-为您投保";
        _firstOrderFeiTitleLabel = firstOrderFeeL;
        [self.whiteBackView addSubview:firstOrderFeeL];
    }
    return _firstOrderFeiTitleLabel;
}

#pragma mark - 本日首单-内容
- (UILabel *)firstOrderFeiContentLabel {
    if (!_firstOrderFeiContentLabel) {
        UILabel *firstOrderFee = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.firstOrderFeiTitleLabel.top, 100, 14)];
        firstOrderFee.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        firstOrderFee.textAlignment = NSTextAlignmentRight;
        firstOrderFee.font = FontSize(14);
        firstOrderFee.text = [NSString stringWithFormat:@"-￥ %.2f",[[_mydic objectForKey:@"insurance_premium"] floatValue]];;
        _firstOrderFeiContentLabel = firstOrderFee;
        [self.whiteBackView addSubview:firstOrderFee];
    }
    return _firstOrderFeiContentLabel;
}

#pragma mark - 本日首单-分割线
- (UILabel *)firstOrderLine {
    if (!_firstOrderLine) {
        UILabel *firstOrderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.firstOrderFeiTitleLabel.bottom + 10, KWIDTH-20, 1)];
        firstOrderLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _firstOrderLine = firstOrderLine;
        [self.whiteBackView addSubview:firstOrderLine];
    }
    return _firstOrderLine;
}


#pragma mark - 白色背景
- (UIView *)whiteBackView {
    if (!_whiteBackView) {
        UIView *upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 100)];
        upLable.backgroundColor = [UIColor whiteColor];
        upLable.userInteractionEnabled = YES;
        _whiteBackView = upLable;
        [self.bgView addSubview:upLable];
    }
    return _whiteBackView;
}

#pragma mark - 支付时间
- (UILabel *)payTime {
    if (!_payTime) {
        UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(20, self.bgView.bottom+6, KWIDTH-40, 13)];
        payTime.text = [NSString stringWithFormat:@"支付时间: %@",_mydic[@"pay_time"]];
        payTime.textColor = [UIColor colorWithHexString:@"#232620"];
        payTime.font = FontSize(12);
        payTime.textAlignment = NSTextAlignmentRight;
        _payTime = payTime;
        [self.scrollView addSubview:payTime];
    }
    return _payTime;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
