//
//  CheckBillForFactoryVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CheckBillForFactoryVC.h"
#import "SYBigImage.h"

@interface CheckBillForFactoryVC ()
@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) UIView *whiteBackView;
@property(nonatomic,strong) UILabel *peijian;//本单费用-标题
@property(nonatomic,strong) UILabel *peijianlb;//本单费用-内容
@property(nonatomic,strong) UIView *peijianLine;
@property(nonatomic,strong) UILabel *firstOrderFeiTitleLabel;//本日首单-标题
@property(nonatomic,strong) UILabel *firstOrderFeiContentLabel;//本日首单-内容
@property(nonatomic,strong) UILabel *firstOrderLine;//本日首单-分割线
@property(nonatomic,strong) UILabel *myshigong;//完工图片
@property(nonatomic,strong) UIView *imageBackView;//放图片的背景
@property(nonatomic,strong) UILabel *imageViewLine;
@property(nonatomic,strong) UILabel *myshouyi;//本单收益-标题
@property(nonatomic,strong) UILabel *myshouyilb;//本单收益-内容
@property(nonatomic,strong) UILabel *myShouYiLine;
@property(nonatomic,strong) UILabel *payTime;
@property(nonatomic,strong) NSDictionary *mydic;
@property(nonatomic,strong) UIScrollView *scrollView;

@end

@implementation CheckBillForFactoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单信息";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
}

-(void)request{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(_model.orderId);
    kWeakSelf;
    [NetWorkTool POST:lookListacc param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
        
        [weakSelf.model setValuesForKeysWithDictionary:[weakSelf.mydic objectForKey:@"order"]];
        [self showDetaileUi];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showDetaileUi{

    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, KHEIGHT-20)];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview: self.bgView];
    self.bgView.backgroundColor = zhutiColor;

    [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];

    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
    order.textColor = [UIColor whiteColor];
    order.font = FontSize(16);
    order.text = @"订单编号";
    [self.bgView addSubview:order];

    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
    orderlb.textColor = [UIColor whiteColor];
    orderlb.font = FontSize(16);
    orderlb.text = [[_mydic objectForKey:@"order"] objectForKey:@"app_order_id"];
    [self.bgView addSubview:orderlb];

    UILabel *peijian = [[UILabel alloc]initWithFrame:CGRectMake(24, 16, 100, 14)];
    peijian.textColor = K666666;
    peijian.font = FontSize(14);
    peijian.text = @"本单费用";
    self.peijian = peijian;
    [self.whiteBackView addSubview:peijian];

    UILabel *peijianlb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, peijian.top, 100, 14)];
    peijianlb.textColor = K666666;
    peijianlb.textAlignment = NSTextAlignmentRight;
    peijianlb.font = FontSize(14);
    peijianlb.text = [NSString stringWithFormat:@"¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"all_cost"] floatValue]];
    self.peijianlb = peijianlb;
    [self.whiteBackView addSubview:peijianlb];

    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, peijianlb.bottom+10, KWIDTH-20, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.whiteBackView addSubview:line];
    
    CGFloat h = line.bottom;
    NSArray *pruArr = [_mydic objectForKey:@"products"];
    if (pruArr.count > 0) {
        for (int i = 0; i<pruArr.count; i++) {
            UILabel *leftLbnumber = [[UILabel alloc]initWithFrame:CGRectMake(24, h+13+(25*i), 12, 12)];
            leftLbnumber.layer.masksToBounds = YES;
            leftLbnumber.layer.cornerRadius = 6;
            leftLbnumber.layer.borderWidth = 1;
            leftLbnumber.layer.borderColor = K999999.CGColor;
            leftLbnumber.textColor =K999999;
            leftLbnumber.textAlignment = NSTextAlignmentCenter;
            leftLbnumber.font = FontSize(8);
            int tt = i+1;
            leftLbnumber.text = [NSString stringWithFormat:@"%d",tt];
            [self.whiteBackView addSubview:leftLbnumber];
            
            NSDictionary *mydic = pruArr[i];
            
            UILabel *leftLb = [[UILabel alloc]initWithFrame:CGRectMake(24+17, h+13+(25*i), KWIDTH-144, 12)];
            leftLb.textColor = K999999;
            leftLb.font = FontSize(12);
            NSString *str = @"更换配件";
            if ([[mydic objectForKey:@"product_model"] integerValue] == 1) {
                if ([self.model.service_name
                     isEqualToString:@"电器安装"]) {
                    str = @"添加材料";
                }else {
                    str = @"添加配件";
                }
            }
            leftLb.text =[NSString stringWithFormat:@"%@ %@ 数量*%@",str,[mydic objectForKey:@"product_name"],[mydic objectForKey:@"count"]];
            [self.whiteBackView addSubview:leftLb];
            if (i == (pruArr.count-1)) {
                h = leftLb.bottom+13;
            }
            
        }
    }

#pragma mark ***查看收益
    if ([_model.order_state integerValue] < 3) {//未支付
        self.myshigong.frame = CGRectMake(24, line.bottom + 16, 100, 14);
        [self showImageView];
        self.whiteBackView.height = self.myshigong.bottom + 10 + self.imageBackView.height;
        self.bgView.height = self.whiteBackView.bottom;
    } else {//已支付
        NSString *insurance_premium = [[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"];
        //insurance_premium = @"1";
        if ([insurance_premium floatValue] > 0) {//如果是本日首单
            self.firstOrderFeiTitleLabel.frame = CGRectMake(24, line.bottom + 16, 200, 14);
            [self firstOrderFeiContentLabel];//加载出来
            self.firstOrderLine.frame = CGRectMake(0, self.firstOrderFeiTitleLabel.bottom+10, KWIDTH-20, 1);
            self.myshigong.frame = CGRectMake(24, self.firstOrderLine.bottom + 16, 100, 14);
            [self showImageView];
            self.whiteBackView.height = self.myshigong.bottom + 10 + self.imageBackView.height;
            self.bgView.height = self.whiteBackView.bottom;
            
        } else {//不是本日首单
            self.myshigong.frame = CGRectMake(24, line.bottom + 16, 100, 14);
            [self showImageView];
        }
        
        self.myshouyi.frame = CGRectMake(24, self.imageBackView.bottom + 10, 100, 14);
        [self myshouyilb];
        [self myShouYiLine];
        self.whiteBackView.height = self.myShouYiLine.bottom + 10;
        self.bgView.height = self.whiteBackView.bottom;
        [self payTime];
        self.scrollView.contentSize = CGSizeMake(KWIDTH,  self.bgView.height + 13);
    }
    
}

#pragma mark - 白色背景
- (UIView *)whiteBackView {
    if (!_whiteBackView) {
        UIView *upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 100)];
        upLable.backgroundColor = [UIColor whiteColor];
        upLable.userInteractionEnabled = YES;
        _whiteBackView = upLable;
        [self.bgView addSubview:upLable];
    }
    return _whiteBackView;
}

#pragma mark - 完工照片
- (UILabel *)myshigong {
    if (!_myshigong) {
        UILabel *myshigong = [[UILabel alloc]initWithFrame:CGRectMake(24, self.peijianLine.bottom + 16, 100, 14)];
        myshigong.textColor = K666666;
        myshigong.font = FontSize(14);
        myshigong.text = @"完工照片";
        [self.whiteBackView addSubview:myshigong];
        _myshigong = myshigong;
    }
    return _myshigong;
}

#pragma mark - 订单收益-标题
- (UILabel *)myshouyi {
    if (!_myshouyi) {
        UILabel *myshouyi = [[UILabel alloc]initWithFrame:CGRectMake(24, self.imageViewLine.bottom + 16, 100, 14)];
        myshouyi.textColor = zhutiColor;
        myshouyi.font = FontSize(14);
        myshouyi.text = @"本单收益";
        _myshouyi = myshouyi;
        [self.whiteBackView addSubview:myshouyi];
    }
    return _myshouyi;
}

#pragma mark - 订单收益-内容
- (UILabel *)myshouyilb {
    if (!_myshouyilb) {
        UILabel *myshouyilb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.myshouyi.top, 100, 14)];
        myshouyilb.textColor = zhutiColor;
        myshouyilb.textAlignment = NSTextAlignmentRight;
        myshouyilb.font = FontSize(14);
        myshouyilb.text =[NSString stringWithFormat:@"¥ %.2f",[[_mydic  objectForKey:@"earning"] floatValue]];
        [self.whiteBackView addSubview:myshouyilb];
        self.whiteBackView.height = myshouyilb.bottom+100;
        _myshouyilb = myshouyilb;
    }
    return _myshouyilb;
}

#pragma mark - 订单收益-分割线
- (UILabel *)myShouYiLine {
    if (!_myShouYiLine) {
        UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.myshouyi.bottom+10, KWIDTH-20, 1)];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _myShouYiLine = line2;
        [self.whiteBackView addSubview:line2];
    }
    return _myShouYiLine;
}

#pragma mark - 本日首单-标题
- (UILabel *)firstOrderFeiTitleLabel {
    if (!_firstOrderFeiTitleLabel) {
        UILabel *firstOrderFeeL = [[UILabel alloc]initWithFrame:CGRectMake(24, self.peijian.bottom + 10 + 16, 150, 14)];
        firstOrderFeeL.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        firstOrderFeeL.font = FontSize(14);
        firstOrderFeeL.text = @"当日首单-为您投保";
        _firstOrderFeiTitleLabel = firstOrderFeeL;
        [self.whiteBackView addSubview:firstOrderFeeL];
    }
    return _firstOrderFeiTitleLabel;
}

#pragma mark - 本日首单-内容
- (UILabel *)firstOrderFeiContentLabel {
    if (!_firstOrderFeiContentLabel) {
        UILabel *firstOrderFee = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.firstOrderFeiTitleLabel.top, 200, 14)];
        firstOrderFee.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        firstOrderFee.textAlignment = NSTextAlignmentRight;
        firstOrderFee.font = FontSize(14);
        firstOrderFee.text =  [NSString stringWithFormat:@"-¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"]];
        _firstOrderFeiContentLabel = firstOrderFee;
        [self.whiteBackView addSubview:firstOrderFee];
    }
    return _firstOrderFeiContentLabel;
}

#pragma mark - 本日首单-分割线
- (UILabel *)firstOrderLine {
    if (!_firstOrderLine) {
        UILabel *firstOrderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.firstOrderFeiTitleLabel.bottom + 10, KWIDTH-20, 1)];
        firstOrderLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _firstOrderLine = firstOrderLine;
        [self.whiteBackView addSubview:firstOrderLine];
    }
    return _firstOrderLine;
}

- (UIView *)imageBackView {
    if (!_imageBackView) {
        UIView *imageBackView = [[UIView alloc] init];
        imageBackView.frame = CGRectMake(0, self.myshigong.bottom + 10, kScreen_Width, 60);
        imageBackView.backgroundColor = [UIColor clearColor];
        _imageBackView = imageBackView;
        [self.whiteBackView addSubview:imageBackView];
    }
    return _imageBackView;
}

#pragma mark - 图片-分割线
- (UILabel *)imageViewLine {
    if (!_imageViewLine) {
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _imageViewLine = line1;
        [self.whiteBackView addSubview:line1];
    }
    return _imageViewLine;
}

#pragma mark - 支付时间
- (UILabel *)payTime {
    if (!_payTime) {
        UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(22, self.bgView.bottom+6, KWIDTH-44, 13)];
        payTime.text = [NSString stringWithFormat:@"支付时间: %@",_model.pay_time];
        payTime.textColor = [UIColor colorWithHexString:@"#232620"];
        payTime.font =FontSize(12);
        payTime.textAlignment = NSTextAlignmentRight;
        _payTime = payTime;
        [self.scrollView addSubview:payTime];
    }
    return _payTime;
}

#pragma mark - 显示图片
- (void)showImageView {
    NSMutableArray *imagearr = [[_mydic objectForKey:@"imgs"] mutableCopy];
    CGFloat wei = (self.scrollView.frame.size.width - 25-46-46)/4;
    
    if (imagearr.count > 0) {
        [self.imageBackView removeAllSubviews];
        for (int i = 0; i < imagearr.count; i++) {
            
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.imageBackView addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[imagearr[i] objectForKey:@"img"]] placeholderImage:defaultImg];
            myimage.frame = CGRectMake(24+(wei+10)*(i % 4), (wei + 10) * (i / 4), wei, wei);
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
            self.imageBackView.frame = CGRectMake(0, self.myshigong.bottom + 10, self.whiteBackView.width, (wei + 10) * (i / 4 + 1));
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end

