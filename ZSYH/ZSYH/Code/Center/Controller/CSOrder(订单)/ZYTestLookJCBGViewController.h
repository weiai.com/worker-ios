//
//  ZYTestLookJCBGViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYTestLookJCBGViewController : BaseViewController
@property (nonatomic, strong) CSmaintenanceListModel *model;
@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
