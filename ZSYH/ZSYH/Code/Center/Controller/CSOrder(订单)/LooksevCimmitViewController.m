//
//  LooksevCimmitViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "LooksevCimmitViewController.h"

@interface LooksevCimmitViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagemy;
@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *cimConLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet StarEvaluator *mystar;

@end

@implementation LooksevCimmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评价";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
}

-(void)request{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] =NOTNIL(_orderid);
    kWeakSelf;
    
    [NetWorkTool POST:lookcimmweixiu param:param success:^(id dic) {
        KMyLog(@"dddddd%@",dic);
        NSDictionary *mydic = [dic objectForKey:@"data"];
        
        self.nameLB.text = [mydic objectForKey:@"user_name"];
        self.timeLB.text = [[mydic objectForKey:@"create_time"] substringToIndex:10];
        self.cimConLb.text = [mydic objectForKey:@"content"];
        
        NSString *imag = [mydic objectForKey:@"headimg"];
        
        if ([imag hasPrefix:@"http"]) {
            [self.imagemy sd_setImageWithURL:[NSURL URLWithString:imag] placeholderImage:defaultImg];
        } else {
            sdimg(self.imagemy, imag);
        }
        CGFloat ffff = [[mydic objectForKey:@"score"] floatValue];
        weakSelf.mystar.BGViewWidth = 120;
        weakSelf.mystar.widthDistence = 8;
        weakSelf.mystar.gardernumber = ffff;
        weakSelf.mystar.isShow = YES;
        [weakSelf.mystar loadSubviews];
    
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

@end
