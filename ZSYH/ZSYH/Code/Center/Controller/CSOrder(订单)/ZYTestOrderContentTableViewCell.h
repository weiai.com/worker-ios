//
//  ZYTestOrderContentTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYTestOrderContentTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *dateLab;       //日期时间
@property (nonatomic, strong) UILabel *stateLab;      //状态
@property (nonatomic, strong) UILabel *nameLab;       //姓名 内容
@property (nonatomic, strong) UILabel *phoneLab;      //电话 内容
@property (nonatomic, strong) UILabel *addressLab;    //地址 内容
@property (nonatomic, strong) UILabel *fwlxTitLab;    //服务类型 标题
@property (nonatomic, strong) UILabel *fwlxConLab;    //服务类型 内容

@property (nonatomic, strong) UILabel *dqplTitLab;    //电器品类 标题
@property (nonatomic, strong) UILabel *dqplConLab;    //电器品类 内容

@property (nonatomic, strong) UILabel *yysjTitLab;    //预约时间 标题
@property (nonatomic, strong) UILabel *yysjConLab;    //预约时间 内容

@property (nonatomic, strong) UILabel *fwdzTitLab;    //服务地址 标题
@property (nonatomic, strong) UILabel *fwdzConLab;    //服务地址 内容
@property (nonatomic, strong) UIButton *txyyxxBtn;    //填写预约信息 按钮
@property (nonatomic, strong) UIButton *scjcbgBtn;    //生成检测报告 按钮

@property (nonatomic, strong) UIButton *cancleOrBtn;  //取消预订 按钮

-(void)refasf:(CSmaintenanceListModel *)model;
@end

NS_ASSUME_NONNULL_END
