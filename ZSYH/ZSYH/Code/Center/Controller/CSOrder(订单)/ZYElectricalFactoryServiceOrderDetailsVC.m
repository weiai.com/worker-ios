//
//  ZYElectricalFactoryServiceOrderDetailsVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/30.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYElectricalFactoryServiceOrderDetailsVC.h"
#import "CSmaintenanceListModel.h"
#import "CSmakeAccLostViewController.h"
#import "CSnewJCBGVC.h"
#import "LooksevCimmitViewController.h"
#import "CSmakeAnzhuangViewController.h"
#import "FillDateViewController.h"
#import "ZYGenerateBillsViewController.h"
#import "ZYDQCPaulineGenerateBillsViewController.h" //厂家提供/师傅自带 生成账单
#import "ZYGenerateBillingDetailsController.h"
#import "ZYDQCDQAZCKZDViewController.h"        //电器厂电器安装查看账单
#import "ZYDQCBWCKZDViewController.h"          //电器厂保外查看账单
#import "ZYDQCBNSFZDCKZDViewController.h"      //电器厂保内师傅自带查看账单
#import "ZYDQCBNCJTGCKZDViewController.h"      //电器厂保内厂家提供查看账单
#import "ZOrderDetailPartDetailVC.h"           //配件详情
#import "ZYRefuseToRepairCKZDViewController.h" //电器厂保外拒绝维修查看账单
#import "ZYTestLookJCBGViewController.h"
#import "ZYDQCCJTGSCZDViewController.h"        //电器厂厂家提供生成账单
#import "ZYDQCSFZDSCZDViewController.h"        //电器厂师傅自带生成账单

@interface ZYElectricalFactoryServiceOrderDetailsVC ()
@property(nonatomic, strong)UIScrollView *scrollView;
@property(nonatomic, strong)UIButton *upOneBtn;
//下面4个 按钮
@property(nonatomic, strong)UIButton *firstBtn;
@property(nonatomic, strong)UIButton *twobut;
@property(nonatomic, strong)UIButton *threebut;
@property(nonatomic, strong)UIButton *fourbut;
@property(nonatomic, strong)UILabel *phoneNumLab;
@end

@implementation ZYElectricalFactoryServiceOrderDetailsVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //禁用侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    if (_scrollView) {
        [self request];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"服务单详情";
    //接完单之后跳转进来 返回按钮
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    leftBtn.frame = CGRectMake(0, 0, 30,30);
    UIImage *img = [[UIImage imageNamed:@"left_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    if (strIsEmpty(_orderID)) {
        [self configViews];
    } else {
        [self request];
    }
}

//接完单之后跳转到检测单详情页面 再返回 回到检测单列表页面
- (void)leftBarBtnClicked:(UIButton *)button {
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)request{
    
    _mymodel = [[CSmaintenanceListModel alloc]init];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:(NSUInteger)1];
    param[@"orderId"] =  NOTNIL(_orderID);
    kWeakSelf;
    
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        KMyLog(@"电器厂订单 服务单详情 %@", dic);
        weakSelf.mymodel = [CSmaintenanceListModel mj_objectWithKeyValues:[dic objectForKeyNotNil:@"data"]];
        if (strIsEmpty(weakSelf.mymodel.orderId)) {
            weakSelf.mymodel.orderId = weakSelf.mymodel.rep_order_id;
        }
        [self configViews];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark-------------------点击不同的状态按钮点击事件-----------------------------
/***
 服务单详情 检测报告 查看账单 查看评价 配件详情 按钮 点击跳转事件
 */
-(void)fourAction:(UIButton *)but{
    /*
     0 是第一个可点击按钮
     1 是第二个可点击按钮
     2 是第三个可点击按钮
     3 是第四个可点击按钮
     */
    //    NSInteger ind = but.tag-9527;
    NSString *btnTitle = but.titleLabel.text;//填写预约信息 检测报告 查看账单 查看评价 配件详情
    
    CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc]init];
    model = _mymodel;
    //已接单
    if ([model.rep_order_state integerValue] == 0) {
        
        if ([model.fault_type isEqualToString:@"3"]) {//维修
            if ([model.user_type isEqualToString:@"2"]) {//电器厂
                if ([btnTitle isEqualToString:@"检测报告"]) {
                    if (model.test_order_id.length>0)
                    {
                        ZYTestLookJCBGViewController *VC = [[ZYTestLookJCBGViewController alloc]init];
                        VC.orderId = model.test_order_id;
                        [self.navigationController pushViewController:VC animated:YES];
                    }
                }
                
                if ([btnTitle isEqualToString:@"配件详情"]) {
                    if ([model.azNumber integerValue] > 0) {
                        ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
                        vc.orderModel = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单  查看配件详情");
                    }
                }
            }
        } else {
            if ([btnTitle isEqualToString:@"查看账单"])
            {
                //电器厂订单 电器安装 查看账单
                ZYDQCDQAZCKZDViewController *vc = [[ZYDQCDQAZCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器安装订单 查看账单");
            }
        }
    }
    //待支付
    else if ([model.rep_order_state integerValue] == 2) {
        
        if ([btnTitle isEqualToString:@"检测报告"]) {
            if (model.test_order_id.length>0)
            {
                ZYTestLookJCBGViewController *VC = [[ZYTestLookJCBGViewController alloc]init];
                VC.orderId = model.test_order_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
        if ([btnTitle isEqualToString:@"查看账单"]) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.fault_type isEqualToString:@"3"]) {
                
                if ([model.is_protect boolValue] ==YES) {
                    //厂家提供product_source ==1
                    if (model.product_source.integerValue == 1) {
                        //电器厂订单 保内 厂家提供 查看账单 已支付
                        ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 厂家提供 未支付查看账单");
                    }
                    else
                    {
                        ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 师傅自带 未支付 查看账单");
                    }
                }
                //保外
                else {
                    //电器厂订单 保外订单 查看账单
                    ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"电器厂维修订单 待支付 查看账单");
                }
            }
            //安装单
            else
            {
                //电器厂订单 电器安装 查看账单
                ZYDQCDQAZCKZDViewController *vc = [[ZYDQCDQAZCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器安装订单 查看账单");
            }
        }
        
        if([btnTitle isEqualToString:@"配件详情"]) {
            ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"电器厂订单 保外 添加配件 未支付 查看配件详情");
        }
    }
    //已支付 未评价
    else if ([model.rep_order_state integerValue] == 3) {
        if ([btnTitle isEqualToString:@"检测报告"]) {
            if (model.test_order_id.length>0)
            {
                ZYTestLookJCBGViewController *VC = [[ZYTestLookJCBGViewController alloc]init];
                VC.orderId = model.test_order_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
        if ([btnTitle isEqualToString:@"查看账单"]) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.fault_type isEqualToString:@"3"]) {
                
                if ([model.is_protect boolValue] ==YES) {
                    //厂家提供product_source ==1
                    if (model.product_source.integerValue == 1) {
                        //电器厂订单 保内 厂家提供 查看账单 已支付
                        ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 厂家提供 未支付查看账单");
                    }
                    else
                    {
                        ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 师傅自带 未支付 查看账单");
                    }
                }
                //保外
                else {
                    //电器厂订单 保外订单 查看账单
                    ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"电器厂维修订单 待支付 查看账单");
                }
            }
            //安装单
            else {
                //电器厂订单 电器安装 查看账单
                ZYDQCDQAZCKZDViewController *vc = [[ZYDQCDQAZCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器安装订单 查看账单");
            }
        }
        if([btnTitle isEqualToString:@"配件详情"]) {
            ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"电器厂订单 保外 添加配件 未支付 查看配件详情");
        }
    }
    //已评价
    else if ([model.rep_order_state integerValue] == 4)
    {
        if ([btnTitle isEqualToString:@"检测报告"]) {
            if (model.test_order_id.length>0)
            {
                ZYTestLookJCBGViewController *VC = [[ZYTestLookJCBGViewController alloc]init];
                VC.orderId = model.test_order_id;
                [self.navigationController pushViewController:VC animated:YES];
            }
        }
        if ([btnTitle isEqualToString:@"查看账单"]) {
            //电器厂维修保内
            if ([model.user_type isEqualToString:@"2"] && [model.fault_type isEqualToString:@"3"]) {
                
                if ([model.is_protect boolValue] ==YES) {
                    //厂家提供product_source ==1
                    if (model.product_source.integerValue == 1) {
                        //电器厂订单 保内 厂家提供 查看账单 已支付
                        ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 厂家提供 未支付查看账单");
                    }
                    else
                    {
                        ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                        vc.model = model;
                        [self.navigationController pushViewController:vc animated:YES];
                        KMyLog(@"电器厂订单 保内 师傅自带 未支付 查看账单");
                    }
                }
                //保外
                else
                {
                    //电器厂订单 保外订单 查看账单
                    ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"电器厂维修订单 待支付 查看账单");
                }
            }
            //安装单
            else
            {
                //电器厂订单 电器安装 查看账单
                ZYDQCDQAZCKZDViewController *vc = [[ZYDQCDQAZCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"电器安装订单 查看账单");
            }
        }
        //配件详情
        if([btnTitle isEqualToString:@"配件详情"]) {
            ZOrderDetailPartDetailVC *vc = [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"电器厂订单 保外 添加配件 未支付 查看配件详情");
        }
        //查看评价
        if([btnTitle isEqualToString:@"查看评价"]) {
            LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
            cimmit.orderid = model.orderId;
            [self.navigationController pushViewController:cimmit animated:YES];
            KMyLog(@"查看评价");
        }
    }
    //已取消
    else if ([model.rep_order_state integerValue] == 5) {
        
    }
    //订单中止
    else if ([model.rep_order_state integerValue] == 6)
    {
        //test_order_id存在，不显示查看账单
        if ( model.test_order_id.length>0) {
            
        }
        else
        {
            if ([btnTitle isEqualToString:@"查看账单"])
            {
                //电器厂维修保内
                if ([model.user_type isEqualToString:@"2"] && [model.fault_type isEqualToString:@"3"]) {
                    
                    if ([model.is_protect boolValue] == YES) {
                        //厂家提供product_source ==1
                        if (model.product_source.integerValue == 1) {
                            //电器厂订单 保内 厂家提供 查看账单 已支付
                            ZYDQCBNCJTGCKZDViewController *vc = [[ZYDQCBNCJTGCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 保内 厂家提供 未支付查看账单");
                        }
                        else
                        {
                            ZYDQCBNSFZDCKZDViewController *vc = [[ZYDQCBNSFZDCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂订单 保内 师傅自带 未支付 查看账单");
                        }
                    }
                    //保外
                    else {
                        //orderType 1提前终止 2正常结束
                        if ([model.orderType isEqualToString:@"1"]) {
                            //电器厂保外订单 提前中止
                            ZYRefuseToRepairCKZDViewController *vc = [[ZYRefuseToRepairCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂保外订单 提前中止 未支付 查看账单");
                        }
                        else
                        {
                            //电器厂订单 保外订单 查看账单
                            ZYDQCBWCKZDViewController *vc = [[ZYDQCBWCKZDViewController alloc] init];
                            vc.model = model;
                            [self.navigationController pushViewController:vc animated:YES];
                            KMyLog(@"电器厂维修订单  查看账单");
                        }
                    }
                }
            }
        }
    }
}

#pragma mark ----------------根据请求结果,刷新按钮状态---------------------------------
-(void)refreshBtnState:(CSmaintenanceListModel *)model{
    
    /*
     厂家
     维修单
     
     厂家提供
     已接单  检测报告  配件详情
     待支付  检测报告  查看账单  配件详情
     已支付  检测报告  查看账单  配件详情
     待评价  检测报告  查看账单  配件详情 查看评价
     
     师傅自带
     已接单   检测报告   配件详情
     待支付   检测报告  查看账单 配件详情
     已支付   检测报告  查看账单 配件详情
     已评价   检测报告  查看账单 配件详情  查看评价
     
     保外
     已接单  检测报告  配件详情
     待支付  检测报告  查看账单 配件详情
     已支付  检测报告  查看账单 配件详情
     待评价  检测报告  查看账单 配件详情   查看评价
     
     状态为6 订单中止时候  查看账单 有可能有  有testorderID时候，不显示查看账单，
     
     厂家
     安装单
     已接单
     待支付  查看账单
     已支付  查看账单
     待评价  查看账单   查看评价
     
     */
    //电器厂维修订单
    self.firstBtn.hidden = YES;
    self.twobut.hidden = YES;
    self.threebut.hidden = YES;
    self.fourbut.hidden = YES;
#pragma mark------------------电器厂维修订单------------------------
    if ([model.fault_type isEqualToString:@"3"]) {
        //已接单
        if ([model.rep_order_state integerValue] == 0)
        {
            //电器厂
            if ([model.user_type isEqualToString:@"2"])
            {
                [self.upOneBtn setTitle:@"已接单" forState:(UIControlStateNormal)];
                
                //可以查看检测报告 也可以查看配件详情 按钮3是检测报告 按钮4是配件详情
                if (model.test_order_id.length>0 && [model.azNumber integerValue] > 0)
                {
                    self.threebut.hidden = NO;
                    self.fourbut.hidden = NO;
                    
                    [self.threebut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor whiteColor];
                    self.threebut.layer.borderWidth = 1;
                    
                    [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor whiteColor];
                    self.fourbut.layer.borderWidth = 1;
                }
                else
                {
                    //有 检测报告
                    if (model.test_order_id.length>0)
                    {
                        self.fourbut.hidden = NO;
                        [self.fourbut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                        [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                        self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                        self.fourbut.backgroundColor = [UIColor whiteColor];
                        self.fourbut.layer.borderWidth = 1;
                    }
                    //有 配件详情
                    else if ([model.azNumber integerValue] > 0) {
                        self.fourbut.hidden = NO;
                        [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                        [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                        self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                        self.fourbut.backgroundColor = [UIColor whiteColor];
                        self.fourbut.layer.borderWidth = 1;
                    }
                }
            }
        }
        else if ([model.rep_order_state integerValue] == 1)
        {
            //已检测
            //只有电器厂-维修-保内-厂家提供配件 才有检测
            [self.upOneBtn setTitle:@"已检测" forState:(UIControlStateNormal)];
            if (model.test_order_id.length>0) {
                [self.fourbut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        else if ([model.rep_order_state integerValue] == 2)
        {
            //待支付
            [self.upOneBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            //无 配件详情 按钮
            if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                
                if (model.test_order_id.length>0) {
                    self.threebut.hidden = NO;
                    [self.threebut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor whiteColor];
                    self.threebut.layer.borderWidth = 1;
                    
                } else {
                    self.threebut.hidden = YES;
                }
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
            else
            {
                //判断是否显示配件详情按钮
                //不显示 配件详情
                if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                    
                    if (model.test_order_id.length>0) {
                        self.threebut.hidden = NO;
                        [self.threebut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                        [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                        self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                        self.threebut.backgroundColor = [UIColor whiteColor];
                        self.threebut.layer.borderWidth = 1;
                    } else {
                        self.threebut.hidden = YES;
                    }
                    
                    self.fourbut.hidden = NO;
                    [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor whiteColor];
                    self.fourbut.layer.borderWidth = 1;
                }
                //显示 配件详情
                else
                {
                    if (model.test_order_id.length>0) {
                        self.twobut.hidden = NO;
                        [self.twobut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                        [self.twobut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                        self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                        self.twobut.backgroundColor = [UIColor whiteColor];
                        self.twobut.layer.borderWidth = 1;
                    } else {
                        self.twobut.hidden = YES;
                    }
                    
                    self.threebut.hidden = NO;
                    [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor whiteColor];
                    self.threebut.layer.borderWidth = 1;
                    
                    self.fourbut.hidden = NO;
                    [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor whiteColor];
                    self.fourbut.layer.borderWidth = 1;
                }
            }
        }
        else if ([model.rep_order_state integerValue] == 3)
        {
            //已支付
            [self.upOneBtn setTitle:@"已支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            //无 配件详情
            if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                if (model.test_order_id.length>0) {
                    self.threebut.hidden = NO;
                    [self.threebut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.threebut.backgroundColor = [UIColor whiteColor];
                    self.threebut.layer.borderWidth = 1;
                } else {
                    self.threebut.hidden = YES;
                }
                
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            } else {
                //有 配件详情
                if (model.test_order_id.length>0) {
                    self.twobut.hidden = NO;
                    [self.twobut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.twobut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.twobut.backgroundColor = [UIColor whiteColor];
                    self.twobut.layer.borderWidth = 1;
                } else {
                    self.twobut.hidden = YES;
                }
                
                self.threebut.hidden = NO;
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        //4已评价
        else if ([model.rep_order_state integerValue] == 4)
        {
            [self.upOneBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
            //判断是否显示配件详情按钮
            //不显示 配件详情
            if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                if (model.test_order_id.length>0) {
                    self.twobut.hidden = NO;
                    [self.twobut setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.twobut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.twobut.backgroundColor = [UIColor whiteColor];
                    self.twobut.layer.borderWidth = 1;
                } else {
                    self.twobut.hidden = YES;
                }
                self.threebut.hidden = NO;
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
            else
            {
                //显示配件详情
                //显示 检测报告
                if (model.test_order_id.length>0) {
                    self.firstBtn.hidden = NO;
                    [self.firstBtn setTitle:@"检测报告" forState:(UIControlStateNormal)];
                    [self.firstBtn setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.firstBtn.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.firstBtn.backgroundColor = [UIColor whiteColor];
                    self.firstBtn.layer.borderWidth = 1;
                }
                else
                {
                    self.firstBtn.hidden = YES;
                }
                self.twobut.hidden = NO;
                [self.twobut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.twobut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.twobut.backgroundColor = [UIColor whiteColor];
                self.twobut.layer.borderWidth = 1;
                
                self.threebut.hidden = NO;
                [self.threebut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        //5已取消
        else if ([model.rep_order_state integerValue] == 5)
        {
            [self.upOneBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
        //6订单中止
        else if ([model.rep_order_state integerValue] == 6)
        {
            [self.upOneBtn setTitle:@"订单中止已支付" forState:(UIControlStateNormal)];
            self.upOneBtn.titleLabel.font = FontSize(14);
            [self.upOneBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.upOneBtn.width = 120;
            self.upOneBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
            
            self.twobut.hidden = YES;
            //test_order_id存在，不显示查看账单
            if (model.test_order_id.length>0) {
                self.threebut.hidden = YES;
                self.fourbut.hidden = YES;
                
            } else {
                self.fourbut.hidden = NO;
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor whiteColor].CGColor;
                [self.fourbut setBackgroundColor:[UIColor clearColor]];
                self.fourbut.layer.borderWidth = 1;
                
            }
        }
    } else {
#pragma mark------------------电器厂安装订单------------------------
        //已接单
        if ([model.rep_order_state integerValue] == 0) {
            
            [self.upOneBtn setTitle:@"已接单" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
        //待支付
        else if ([model.rep_order_state integerValue] == 2)
        {
            [self.upOneBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            self.fourbut.hidden = NO;
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        //已支付
        else if ([model.rep_order_state integerValue] == 3)
        {
            [self.upOneBtn setTitle:@"已支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            self.fourbut.hidden = NO;
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        //4已评价
        else if ([model.rep_order_state integerValue] == 4)
        {
            [self.upOneBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            
            self.threebut.hidden = NO;
            [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.threebut.backgroundColor = [UIColor whiteColor];
            self.threebut.layer.borderWidth = 1;
            
            self.fourbut.hidden = NO;
            [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        // 5已取消
        else if ([model.rep_order_state integerValue] == 5){
            
            [self.upOneBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
    }
}

#pragma mark ----------------界面初始化布局---------------------------------
-(void)configViews
{
    [self.view addSubview:self.scrollView];
    [self.scrollView removeAllSubviews];
    
    CGFloat wei = 60;
    CGFloat left1 = 26;
    CGFloat left2 = 14;
    
    UIView *bbgvieww = [[UIView alloc]init];
    [self.scrollView addSubview:bbgvieww];
    
    if ([self.mymodel.rep_order_state integerValue] == 6) {
        UIImageView *redIV = [UIImageView new];
        redIV.frame = CGRectMake(0, 0, ScreenW-20, 73);
        redIV.image = [UIImage imageNamed:@"路径 1829"];
        [bbgvieww addSubview:redIV];
    }
    
    self.upOneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.upOneBtn.frame = CGRectMake(16, 10, 60, 26);
    [self.upOneBtn setTitleColor:[UIColor colorWithHexString:@"#F88B1F"] forState:(UIControlStateNormal)];
    self.upOneBtn.titleLabel.font = FontSize(12);
    [bbgvieww addSubview:self.upOneBtn];
    
    CGFloat butwir = 79;
    CGFloat fengxi = (KWIDTH -20 - (butwir*4))/5;
    
    NSArray *titleArr = @[@"检测报告",@"查看账单",@"配件详情",@"查看评价"];
    for (int i = 0; i<4; i++) {
        UIButton *mybut = [UIButton buttonWithType:UIButtonTypeCustom];
        mybut.frame = CGRectMake(fengxi+(butwir+fengxi)*i, 36, butwir, 26);
        [mybut setTitle:titleArr[i] forState:(UIControlStateNormal)];
        mybut.titleLabel.font = FontSize(12);
        mybut.tag = 9527+i;
        [mybut addTarget:self action:@selector(fourAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [bbgvieww addSubview:mybut];
        switch (i) {
            case 0:
            {
                self.firstBtn = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
                mybut.hidden = YES;
            }
                break;
            case 1:
            {
                self.twobut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 2:
            {
                self.threebut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 3:
            {
                self.fourbut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:(UIControlStateNormal)];
                [mybut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
            }
                break;
            default:
                break;
        }
    }
    
    [self refreshBtnState:_mymodel];
    
    UILabel *lable16 = [[UILabel alloc]initWithFrame:CGRectMake(0, 73, KWIDTH-20, 2)];
    [bbgvieww addSubview:lable16];
    lable16.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    CGFloat hh = 20+lable16.bottom;
    
    if ([_mymodel.user_type integerValue] != 0) { //电器厂订单 服务单详情 UI
        
        //厂商联系电话
        UILabel *cslxdhTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, hh, 90, 20)];
        cslxdhTitLab.text = @"厂商联系电话";
        cslxdhTitLab.font = FontSize(14);
        cslxdhTitLab.textColor = K999999;
        [self.scrollView addSubview:cslxdhTitLab];
        
        UIButton *cslxdhBtn = [UIButton new];
        cslxdhBtn.frame = CGRectMake(KWIDTH -158, hh-6, 33, 33);
        [cslxdhBtn setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
        [cslxdhBtn addTarget:self action:@selector(csCallBtn:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.scrollView addSubview:cslxdhBtn];
        
        UILabel *phoneNumLab = [[UILabel alloc] initWithFrame:CGRectMake(cslxdhBtn.right, hh-6, 105, 33)];
        phoneNumLab.text = _mymodel.company_phone;
        phoneNumLab.font = FontSize(14);
        phoneNumLab.textColor = K666666;
        phoneNumLab.textAlignment = NSTextAlignmentRight;
        [self.scrollView addSubview:phoneNumLab];
        UITapGestureRecognizer *tapg = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(csCallLab:)];
        phoneNumLab.userInteractionEnabled = YES;
        [phoneNumLab addGestureRecognizer:tapg];
        self.phoneNumLab = phoneNumLab;
        
        UILabel *line100 = nil;
        //隐藏厂商联系电话
        if (_mymodel.test_order_id.length>0) {
            cslxdhTitLab.hidden = NO;
            cslxdhBtn.hidden = NO;
            phoneNumLab.hidden = NO;
            line100 = [[UILabel alloc]initWithFrame:CGRectMake(0, cslxdhTitLab.bottom+11, KWIDTH-20, 2)];
            line100.hidden = NO;
            
        } else {
            cslxdhTitLab.hidden = YES;
            cslxdhBtn.hidden = YES;
            phoneNumLab.hidden = YES;
            line100 = [[UILabel alloc]initWithFrame:CGRectMake(0, lable16.top+10, KWIDTH-20, 2)];
            line100.hidden = YES;
        }
        
        line100.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line100];
        
        ///企业用户
        UILabel *complay = [[UILabel alloc]initWithFrame:CGRectMake(left1, line100.bottom +10, wei, 14)];
        complay.font = FontSize(14);
        complay.textColor = K999999;
        complay.text = @"单位名称";
        if ([_mymodel.user_type integerValue]  ==  2) {
            complay.text = @"厂商名称";
        }
        [self.scrollView addSubview:complay];
        
        UILabel *complaylb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line100.bottom +10, KWIDTH-left2-left1-wei-left1-20, 14)];
        complaylb.font = KFontPingFangSCMedium(14);
        complaylb.textColor = KshiYellow; //高亮
        complaylb.text = _mymodel.company_name;
        [self.scrollView addSubview:complaylb];
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(left1, complay.bottom+10, KWIDTH-left1-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line1];
        hh = line1.bottom+8;
    }
    
    UILabel *secvice = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
    secvice.font = FontSize(14);
    secvice.textColor = K999999;
    secvice.text = @"服务类型";
    [self.scrollView addSubview:secvice];
    
    UILabel *secvicelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
    secvicelb.font = KFontPingFangSCMedium(14);
    secvicelb.textColor = K666666;
    secvicelb.text = _mymodel.service_name;
    [self.scrollView addSubview:secvicelb];
    
    if ([_mymodel.user_type integerValue] == 2) {
        UILabel *baonei = [[UILabel alloc]initWithFrame:CGRectMake(secvicelb.right-5, hh, 100, 14)];
        baonei.font = FontSize(14);
        baonei.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        if ([_mymodel.fault_type isEqualToString:@"3"]) {
            if ([_mymodel.is_protect boolValue]) {
                baonei.text = @"保内";
            } else {
                baonei.text = @"保外";
            }
        }else {
            baonei.text = @"";
        }
        [self.scrollView addSubview:baonei];
    }
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, secvicelb.bottom+10, KWIDTH-left1-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line2];
    
    UILabel *produrt = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+8, wei, 14)];
    produrt.font = FontSize(14);
    produrt.textColor = K999999;
    produrt.text = @"电器品类";
    [self.scrollView addSubview:produrt];
    
    CGFloat widd = [NSString widthWithFont:14 text:_mymodel.fault_name];
    UILabel *produrtlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line2.bottom+8,widd, 14)];
    produrtlb.font = KFontPingFangSCMedium(14);
    produrtlb.textColor = K666666;
    produrtlb.text = _mymodel.fault_name;
    [self.scrollView addSubview:produrtlb];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left1, produrt.bottom+10, KWIDTH-left1-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line3];
    
    //电器厂订单
    if ([_mymodel.user_type integerValue] == 2) { //电器厂订单
        if (_mymodel.order_date.length > 0) {//日期不为空
            //维修增加故障原因
            if ([self.mymodel.fault_type isEqualToString:@"3"]) { //电器厂维修
                
                UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
                number.font = FontSize(14);
                number.textColor = K999999;
                number.text = @"报修数量";
                [self.scrollView addSubview:number];
                
                UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
                numberlb.font = KFontPingFangSCMedium(14);
                numberlb.textColor = K666666;
                numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
                [self.scrollView addSubview:numberlb];
                
                UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, numberlb.bottom+10, KWIDTH-left1-20, 1)];
                line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line44];
                
                UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
                faultReasonL.font = FontSize(14);
                faultReasonL.textColor = K999999;
                faultReasonL.text = @"故障原因";
                [self.scrollView addSubview:faultReasonL];
                
                UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
                faultReason.font = KFontPingFangSCMedium(14);
                faultReason.textColor = K666666;
                faultReason.text = _mymodel.common_failures;
                [self.scrollView addSubview:faultReason];
                
                UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
                line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                heig = 14;
                
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
                yuyueTime.font = FontSize(14);
                yuyueTime.textColor = K999999;
                yuyueTime.text = @"上门日期";
                [self.scrollView addSubview:yuyueTime];
                
                UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb.font = KFontPingFangSCMedium(14);
                yuyueTimelb.textColor = K666666;
                yuyueTimelb.text = _mymodel.order_date;
                
                NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
                if (muarrt.count>2) {
                    NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                    NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                    NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                    yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                }
                [self.scrollView addSubview:yuyueTimelb];
                
                UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line5];
                
                UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
                yuyueTime2.font = FontSize(14);
                yuyueTime2.textColor = K999999;
                yuyueTime2.text = @"上门时间";
                [self.scrollView addSubview:yuyueTime2];
                
                UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb2.font = KFontPingFangSCMedium(14);
                yuyueTimelb2.textColor = K666666;
                NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
                NSString *ap = indd < 12 ? @"上午":@"下午";
                yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
                [self.scrollView addSubview:yuyueTimelb2];
                
                UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line6];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = KFontPingFangSCMedium(14);
                addresslb.textColor = K666666;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
                name.font = KFontPingFangSCMedium(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+10, line11.bottom, 33, 33);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, KWIDTH-20-26-30-10-70-75-10-33, 33)];
                phone.font = KFontPingFangSCMedium(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentRight;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = KFontPingFangSCMedium(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = KFontPingFangSCMedium(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                if ([self.mymodel.is_protect isEqualToString:@"0"]) { //保外
                    UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, louceng.bottom+10, KWIDTH-left1-20, 1)];
                    line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line10];
                    
                    UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                    ordernumLB.font = FontSize(12);
                    ordernumLB.textColor = K999999;
                    ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                    [bbgvieww addSubview:ordernumLB];
                    
                    //配件是否到货
                    if ([_mymodel.confirmation_product boolValue]) {
                        
                        UILabel *pjdhLine = [[UILabel alloc]initWithFrame:CGRectMake(10, ordernumLB.bottom+20, KWIDTH-20, 2)];
                        pjdhLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhLine];
                        
                        UILabel *pjdh = [[UILabel alloc]initWithFrame:CGRectMake(left1+15, pjdhLine.bottom+20, 180, 14)];
                        pjdh.font = FontSize(12);
                        pjdh.textColor = [UIColor redColor];
                        pjdh.text = [NSString stringWithFormat:@"配件已到货"];
                        [self.scrollView addSubview:pjdh];
                        
                        UILabel *pjdhBotLine = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdh.bottom+10, KWIDTH-left1-20, 1)];
                        pjdhBotLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhBotLine];
                        
                        UILabel *nyuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdhBotLine.bottom+8, 90, 14)];
                        nyuyueTime.font = FontSize(14);
                        nyuyueTime.textColor = K999999;
                        nyuyueTime.text = @"新预约日期";
                        [self.scrollView addSubview:nyuyueTime];
                        
                        UILabel *nyuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, pjdhBotLine.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb.font = FontSize(14);
                        nyuyueTimelb.textColor = K666666;
                        nyuyueTimelb.text = _mymodel.subscribe_date;
                        
                        NSMutableArray *nmuarrt =[NSMutableArray arrayWithArray:[_mymodel.subscribe_date componentsSeparatedByString:@"-"]];
                        if (nmuarrt.count>2) {
                            NSString *yesr =[NSString stringWithFormat:@"%ld",[nmuarrt[0] integerValue]];
                            NSString *mouth =[NSString stringWithFormat:@"%ld",[nmuarrt[1] integerValue]];
                            NSString *day =[NSString stringWithFormat:@"%ld",[nmuarrt[2] integerValue]];
                            nyuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                        }
                        [self.scrollView addSubview:nyuyueTimelb];
                        
                        UILabel *nline5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                        nline5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline5];
                        
                        UILabel *nyuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nline5.bottom+8, 90, 14)];
                        nyuyueTime2.font = FontSize(14);
                        nyuyueTime2.textColor = K999999;
                        nyuyueTime2.text = @"新预约时间";
                        [self.scrollView addSubview:nyuyueTime2];
                        
                        UILabel *nyuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, nline5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb2.font = FontSize(14);
                        nyuyueTimelb2.textColor = K666666;
                        NSInteger  nindd = [[_mymodel.subscribe_time substringToIndex:2] integerValue];
                        NSString *nap = nindd < 12 ? @"上午":@"下午";
                        nyuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", nap, _mymodel.subscribe_time];
                        [self.scrollView addSubview:nyuyueTimelb2];
                        
                        UILabel *nline6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                        nline6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline6];
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, nline6.bottom+20);
                        bbgvieww.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                        
                    } else {
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    }
                } else if ([self.mymodel.is_protect isEqualToString:@"1"]) { //电器厂保内
                    
                    UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
                    line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line8];
                    
                    UILabel *pjlyL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+7, wei, 14)];
                    pjlyL.font = FontSize(14);
                    pjlyL.textColor = K999999;
                    pjlyL.text = @"配件来源";
                    [self.scrollView addSubview:pjlyL];
                    
                    UILabel *pjly = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+7, 130, 14)];
                    pjly.font = KFontPingFangSCMedium(14);
                    pjly.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                    if ([_mymodel.product_source isEqualToString:@"1"]) {
                        pjly.text = @"厂商提供所需配件";
                    } else {
                        pjly.text = @"师傅自带所需配件";
                    }
                    [self.scrollView addSubview:pjly];
                    
                    UILabel *line9 = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjly.bottom+10, KWIDTH-left1-20, 1)];
                    line9.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line9];
                    
                    UILabel *ykjL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line9.bottom+7, wei, 14)];
                    ykjL.font = FontSize(14);
                    ykjL.textColor = K999999;
                    ykjL.text = @"一口价"; ///一口价///
                    [self.scrollView addSubview:ykjL];
                    
                    UILabel *ykj = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line9.bottom+7, 130, 14)];
                    ykj.font = KFontPingFangSCMedium(14);
                    ykj.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                    ykj.text = [NSString stringWithFormat:@"¥%.2f",[_mymodel.fixed_price floatValue]];
                    [self.scrollView addSubview:ykj];
                    
                    UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, ykj.bottom+10, KWIDTH-left1-20, 1)];
                    line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                    [self.scrollView addSubview:line10];
                    
                    UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                    ordernumLB.font = FontSize(12);
                    ordernumLB.textColor = K999999;
                    ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                    [bbgvieww addSubview:ordernumLB];
                    
                    //配件是否到货
                    if ([_mymodel.confirmation_product boolValue]) {
                        UILabel *pjdhLine = [[UILabel alloc]initWithFrame:CGRectMake(10, ordernumLB.bottom+20, KWIDTH-20, 2)];
                        pjdhLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhLine];
                        
                        UILabel *pjdh = [[UILabel alloc]initWithFrame:CGRectMake(left1+15, pjdhLine.bottom+20, 180, 14)];
                        pjdh.font = FontSize(12);
                        pjdh.textColor = [UIColor redColor];
                        pjdh.text = [NSString stringWithFormat:@"配件已到货"];
                        [self.scrollView addSubview:pjdh];
                        
                        UILabel *pjdhBotLine = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdh.bottom+10, KWIDTH-left1-20, 1)];
                        pjdhBotLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:pjdhBotLine];
                        
                        UILabel *nyuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, pjdhBotLine.bottom+8, 90, 14)];
                        nyuyueTime.font = FontSize(14);
                        nyuyueTime.textColor = K999999;
                        nyuyueTime.text = @"新预约日期";
                        [self.scrollView addSubview:nyuyueTime];
                        
                        UILabel *nyuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, pjdhBotLine.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb.font = FontSize(14);
                        nyuyueTimelb.textColor = K666666;
                        nyuyueTimelb.text = _mymodel.subscribe_date;
                        
                        NSMutableArray *nmuarrt =[NSMutableArray arrayWithArray:[_mymodel.subscribe_date componentsSeparatedByString:@"-"]];
                        if (nmuarrt.count>2) {
                            NSString *yesr =[NSString stringWithFormat:@"%ld",[nmuarrt[0] integerValue]];
                            NSString *mouth =[NSString stringWithFormat:@"%ld",[nmuarrt[1] integerValue]];
                            NSString *day =[NSString stringWithFormat:@"%ld",[nmuarrt[2] integerValue]];
                            nyuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                        }
                        [self.scrollView addSubview:nyuyueTimelb];
                        
                        UILabel *nline5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                        nline5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline5];
                        
                        UILabel *nyuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nline5.bottom+8, 90, 14)];
                        nyuyueTime2.font = FontSize(14);
                        nyuyueTime2.textColor = K999999;
                        nyuyueTime2.text = @"新预约时间";
                        [self.scrollView addSubview:nyuyueTime2];
                        
                        UILabel *nyuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, nline5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                        nyuyueTimelb2.font = FontSize(14);
                        nyuyueTimelb2.textColor = K666666;
                        NSInteger  nindd = [[_mymodel.subscribe_time substringToIndex:2] integerValue];
                        NSString *nap = nindd < 12 ? @"上午":@"下午";
                        nyuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", nap, _mymodel.subscribe_time];
                        [self.scrollView addSubview:nyuyueTimelb2];
                        
                        UILabel *nline6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, nyuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                        nline6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                        [self.scrollView addSubview:nline6];
                        
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, nline6.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    } else {
                        bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                        bbgvieww.backgroundColor = [UIColor whiteColor];
                        [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                        self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
                    }
                }
            } else {
                //电器厂安装单
                
                UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
                number.font = FontSize(14);
                number.textColor = K999999;
                number.text = @"报修数量";
                [self.scrollView addSubview:number];
                
                UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
                numberlb.font = KFontPingFangSCMedium(14);
                numberlb.textColor = K666666;
                numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
                [self.scrollView addSubview:numberlb];
                
                UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
                line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line44];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                heig = 14;
                
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
                yuyueTime.font = FontSize(14);
                yuyueTime.textColor = K999999;
                yuyueTime.text = @"上门日期";
                [self.scrollView addSubview:yuyueTime];
                
                UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb.font = KFontPingFangSCMedium(14);
                yuyueTimelb.textColor = K666666;
                yuyueTimelb.text = _mymodel.order_date;
                
                NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
                if (muarrt.count>2) {
                    NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                    NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                    NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                    yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
                }
                [self.scrollView addSubview:yuyueTimelb];
                
                UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
                line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line5];
                
                UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
                yuyueTime2.font = FontSize(14);
                yuyueTime2.textColor = K999999;
                yuyueTime2.text = @"上门时间";
                [self.scrollView addSubview:yuyueTime2];
                
                UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                yuyueTimelb2.font = KFontPingFangSCMedium(14);
                yuyueTimelb2.textColor = K666666;
                NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
                NSString *ap = indd < 12 ? @"上午":@"下午";
                yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
                [self.scrollView addSubview:yuyueTimelb2];
                
                UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
                line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line6];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = KFontPingFangSCMedium(14);
                addresslb.textColor = K666666;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
                name.font = KFontPingFangSCMedium(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+10, line11.bottom, 33, 33);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, KWIDTH-20-26-30-10-70-75-10-33, 33)];
                phone.font = KFontPingFangSCMedium(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentRight;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = KFontPingFangSCMedium(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = KFontPingFangSCMedium(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
                line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line8];
                
                UILabel *ykjL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line8.bottom+7, wei, 14)];
                ykjL.font = FontSize(14);
                ykjL.textColor = K999999;
                ykjL.text = @"一口价";
                [self.scrollView addSubview:ykjL];
                
                UILabel *ykj = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line8.bottom+7, 130, 14)];
                ykj.font = KFontPingFangSCMedium(14);
                ykj.textColor = [UIColor colorWithHexString:@"#CA9F61"];
                ykj.text = [NSString stringWithFormat:@"¥%.2f",[_mymodel.fixed_price floatValue]];
                [self.scrollView addSubview:ykj];
                
                UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, ykj.bottom+10, KWIDTH-left1-20, 1)];
                line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line10];
                
                UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                ordernumLB.font = FontSize(12);
                ordernumLB.textColor = K999999;
                ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                [bbgvieww addSubview:ordernumLB];
                
                bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                bbgvieww.backgroundColor = [UIColor whiteColor];
                [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            }
        }
        else
        {   //如果时间为空
            //维修增加故障原因
            if ([self.mymodel.fault_type isEqualToString:@"3"]) {
                
                UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, wei, 14)];
                faultReasonL.font = FontSize(14);
                faultReasonL.textColor = K999999;
                faultReasonL.text = @"故障原因";
                [self.scrollView addSubview:faultReasonL];
                
                UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
                faultReason.font = KFontPingFangSCMedium(14);
                faultReason.textColor = K666666;
                faultReason.text = _mymodel.common_failures;
                [self.scrollView addSubview:faultReason];
                
                UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
                line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line];
                
                UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
                guzhang.font = FontSize(14);
                guzhang.textColor = K999999;
                guzhang.text = @"备注信息";
                [self.scrollView addSubview:guzhang];
                
                CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
                
                UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
                guzhanglb.font = FontSize(14);
                guzhanglb.textColor = K999999;
                guzhanglb.text = _mymodel.fault_comment;
                guzhanglb.numberOfLines = 0;
                [self.scrollView addSubview:guzhanglb];
                
                heig = 14;
                UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
                line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line4];
                
                UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 30, 14)];
                address.font = FontSize(14);
                address.textColor = K999999;
                address.text = @"地址";
                [self.scrollView addSubview:address];
                
                CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
                UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line4.bottom+8, KWIDTH-80 , heig2)];
                addresslb.font = KFontPingFangSCMedium(14);
                addresslb.textColor = K666666;
                addresslb.text = _mymodel.user_address;
                addresslb.numberOfLines = 0;
                [self.scrollView addSubview:addresslb];
                
                UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
                line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line11];
                
                UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
                userLab.font = FontSize(14);
                userLab.textColor = K999999;
                userLab.text = @"用户";
                [self.scrollView addSubview:userLab];
                
                UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
                name.font = KFontPingFangSCMedium(14);
                name.textColor = K666666;
                name.text = _mymodel.user_name;
                [self.scrollView addSubview:name];
                
                UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(name.right, line11.bottom+9, 75, 14)];
                phoneA.font = FontSize(14);
                phoneA.textColor = K999999;
                phoneA.text = @"用户手机号";
                [self.scrollView addSubview:phoneA];
                
                UIButton *phoneB = [UIButton new];
                phoneB.frame = CGRectMake(phoneA.right+10, line11.bottom, 33, 33);
                [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
                [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
                [self.scrollView addSubview:phoneB];
                
                UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, KWIDTH-20-26-30-10-70-75-10-33, 33)];
                phone.font = KFontPingFangSCMedium(14);
                phone.textColor = K666666;
                phone.textAlignment = NSTextAlignmentRight;
                phone.text = _mymodel.user_phone;
                [self.scrollView addSubview:phone];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
                phone.userInteractionEnabled = YES;
                [phone addGestureRecognizer:tap];
                
                UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
                line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line7];
                
                UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
                dianti.font = FontSize(14);
                dianti.textColor = K999999;
                dianti.text = @"电梯";
                [self.scrollView addSubview:dianti];
                
                UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
                diantilb.font = KFontPingFangSCMedium(14);
                diantilb.textColor = K666666;
                diantilb.text = @"无";
                if ([_mymodel.has_elevator integerValue] == 1) {
                    diantilb.text = @"有";
                }
                [self.scrollView addSubview:diantilb];
                
                UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
                louceng.font = FontSize(14);
                louceng.textColor = K999999;
                louceng.text = @"楼层";
                [self.scrollView addSubview:louceng];
                
                UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
                loucenglb.font = KFontPingFangSCMedium(14);
                loucenglb.textColor = K666666;
                loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
                [self.scrollView addSubview:loucenglb];
                
                UILabel *line10 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+9, KWIDTH-left1-20, 1)];
                line10.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
                [self.scrollView addSubview:line10];
                
                UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line10.bottom+5, KWIDTH-50, 17)];
                ordernumLB.font = FontSize(12);
                ordernumLB.textColor = K999999;
                ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
                [bbgvieww addSubview:ordernumLB];
                
                bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
                bbgvieww.backgroundColor = [UIColor whiteColor];
                [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
                self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            }
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

- (void)csCallBtn:(UIButton *)botton {
    [HFTools callMobilePhone:_mymodel.company_phone];
    KMyLog(@"您点击了 拨打 厂商电话按钮");
}

- (void)csCallLab:(UIButton *)botton {
    [HFTools callMobilePhone:_mymodel.company_phone];
    KMyLog(@"您点击了 拨打 厂商电话按钮");
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

@end
