//
//  ZYTestLookJCBGViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestLookJCBGViewController.h"
#import "SYBigImage.h"

@interface ZYTestLookJCBGViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *orderNumLab; //订单编号
@property(nonatomic,strong)UILabel *cpplConLab;
@property(nonatomic,strong)UILabel *gzsmConLab;
@property(nonatomic,strong)UILabel *jjfaConLab;
@property(nonatomic,strong)UILabel *gzpjConLab;
@property(nonatomic,strong)NSMutableDictionary *myDic;
@property(nonatomic,strong)NSMutableDictionary *proDic;

@end

@implementation ZYTestLookJCBGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"检测报告";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.myDic = [NSMutableDictionary dictionaryWithCapacity:1];
    self.proDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [self request];
    
}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"orderId"] = self.orderId;
    [NetWorkTool POST:getTestReportByTestId param:param success:^(id dic) {
        KMyLog(@"查看结果%@",dic);
        NSMutableDictionary *mudic = [dic objectForKey:@"data"];
        self.myDic = [mudic mutableCopy];
        
        for (NSMutableDictionary *dataDic in mudic[@"products"]) {
            self.proDic = dataDic;
            self.gzpjConLab.text = [dataDic objectForKey:@"product_name"];
        }
        [self showui];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showui{
    
    CGFloat lef = 46;
    //订单编号
    UILabel *orderNumLab = [[UILabel alloc]initWithFrame:CGRectMake(16,kNaviHeight +5, KWIDTH, 17)];
    orderNumLab.text =[NSString stringWithFormat:@"订单编号:%@",self.orderId];
    orderNumLab.font = FontSize(12);
    orderNumLab.textColor = [UIColor colorWithHexString:@"#979797"];
    [self.view addSubview:orderNumLab];
    self.orderNumLab = orderNumLab;
    
    //背景图片 一
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:CGRectMake(10, kNaviHeight +27, KWIDTH-20, self.scrollView.height)];
    bgimage.image = imgname(@"jiancebaogaoBgView");
    bgimage.userInteractionEnabled = YES;
    [bgimage addSubview:self.scrollView];
    [self.view addSubview:bgimage];
    
    //背景图片 二
    UIImageView *bgimageup = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-170)/2, 56, 170, 146)];
    bgimageup.image = imgname(@"jiancebaogaoupview");
    [self.scrollView addSubview:bgimageup];
    
    //产品品类 标题
    UILabel *cpplTitLab = [[UILabel alloc]initWithFrame:CGRectMake(lef, bgimageup.bottom+29, 67, 14)];
    cpplTitLab.text = @"产品品类:";
    cpplTitLab.font = FontSize(12);
    cpplTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:cpplTitLab];
    //产品品类 内容
    UILabel *cpplConLab = [[UILabel alloc]initWithFrame:CGRectMake(cpplTitLab.right, bgimageup.bottom+29, 167, 14)];
    cpplConLab.text = [_myDic objectForKey:@"fault_name"];
    cpplConLab.font = FontSize(12);
    cpplConLab.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:cpplConLab];
    self.cpplConLab = cpplConLab;
    
    //故障说明 标题
    UILabel *guzhangsm = [[UILabel alloc]initWithFrame:CGRectMake(lef, cpplTitLab.bottom+10, 67, 14)];
    guzhangsm.text = @"故障说明:";
    guzhangsm.font = FontSize(12);
    guzhangsm.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:guzhangsm];
    //故障说明 内容
    CGFloat fff = [NSString heightWithWidth:KWIDTH-115-46 font:12 text:[_myDic objectForKey:@"fault_comment"]];
    UILabel *gzsmConlab = [[UILabel alloc]initWithFrame:CGRectMake(cpplTitLab.right, cpplConLab.bottom+10, KWIDTH-115-46, fff)];
    gzsmConlab.text =[_myDic objectForKey:@"fault_comment"];
    gzsmConlab.font = FontSize(12);
    gzsmConlab.numberOfLines = 0;
    gzsmConlab.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:gzsmConlab];
    self.gzsmConLab = gzsmConlab;
    
    //解决方案 标题
    UILabel *jjfaTitLab = [[UILabel alloc]initWithFrame:CGRectMake(lef, gzsmConlab.bottom+10, 67, 14)];
    jjfaTitLab.text = @"解决方案:";
    jjfaTitLab.font = FontSize(12);
    jjfaTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:jjfaTitLab];
    //解决方案 内容
    CGFloat jjj = [NSString heightWithWidth:KWIDTH-115-46 font:12 text:[_myDic objectForKey:@"solution"]];
    UILabel *jjfaConlab = [[UILabel alloc]initWithFrame:CGRectMake(jjfaTitLab.right, gzsmConlab.bottom+10, KWIDTH-115-46, jjj)];
    jjfaConlab.text = [_myDic objectForKey:@"solution"];
    jjfaConlab.font = FontSize(12);
    jjfaConlab.numberOfLines = 0;
    jjfaConlab.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:jjfaConlab];
    self.jjfaConLab = jjfaConlab;
    
    //故障配件 标题
    UILabel *gzpjTitLab = [[UILabel alloc]initWithFrame:CGRectMake(lef, jjfaConlab.bottom+10, 67, 14)];
    gzpjTitLab.text = @"故障配件:";
    gzpjTitLab.font = FontSize(12);
    gzpjTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:gzpjTitLab];
    
    CGFloat h = gzpjTitLab.bottom;
    NSArray *pruArr = [_myDic objectForKey:@"products"];
    CGFloat imageViewWidth = 65;
    CGFloat imageViewTopMargin = 15;
    CGFloat imageViewBottomMargin = 15;
    CGFloat imageViewSpace = 5;
    if (pruArr.count >0) {
        for (int i = 0; i<pruArr.count; i++) {
            UIView *leftV = [UIView new];
            leftV.frame = CGRectMake(lef, h + 10 + (12 + imageViewTopMargin + imageViewWidth + imageViewBottomMargin)*i, 12, 12);
            leftV.backgroundColor = [UIColor brownColor];
            [self.scrollView addSubview:leftV];
            
            NSDictionary *mydic = pruArr[i];
            UILabel *rightL = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100-50, leftV.top, 144, 12)];
            rightL.textColor = K999999;
            rightL.font = FontSize(12);
            rightL.textAlignment = NSTextAlignmentRight;
            rightL.text =[NSString stringWithFormat:@"%@ *%@",[mydic objectForKey:@"product_name"],[mydic objectForKey:@"count"]];
            [self.scrollView addSubview:rightL];
            
            //配件图片
            NSDictionary *products = pruArr[i];
            NSArray *imgs = products[@"imgs"];
            if (imgs.count > 0) {
                for (int j = 0; j < imgs.count; j++) {
                    UIImageView *myimage = [[UIImageView alloc]init];
                    [self.scrollView addSubview:myimage];
                    [myimage sd_setImageWithURL:[NSURL URLWithString:[imgs[j] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
                    myimage.frame = CGRectMake(lef + (imageViewWidth + imageViewSpace) * j, leftV.bottom + imageViewTopMargin, imageViewWidth, imageViewWidth);
                    myimage.userInteractionEnabled = YES;
                    SYBigImage * bigI = [[SYBigImage alloc]init];
                    [myimage addGestureRecognizer:bigI];
                }
            }
            if (i == (pruArr.count-1)) {
                h = leftV.bottom + imageViewTopMargin + imageViewWidth + imageViewBottomMargin;
            }
        }
    }
    
    UILabel *bottomLine = [[UILabel alloc]initWithFrame:CGRectMake(10, h, KWIDTH-20, 1)];
    //bottomLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    bottomLine.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    [self.scrollView addSubview:bottomLine];
    
    bgimage.frame = CGRectMake(10, kNaviHeight +27, KWIDTH-20, self.scrollView.height);
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH-20, KHEIGHT-27-kNaviHeight)];
        _scrollView.contentSize = CGSizeMake(KWIDTH -20, KHEIGHT +125);
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.bounces = NO;
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}
@end
