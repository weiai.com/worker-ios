//
//  ZYNewCheckReportTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "ZYNewCheckReportViewController.h"
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN
//声明cell类 以便协议中参数使用
@class ZYNewCheckReportTableViewCell;
///第一步 写个协议
@protocol  ZYNewCheckReportDelegate<NSObject>
@optional
//@required 默认必须遵守@optional可遵守
//协议方法
- (void)closeBtnClickAction:(ZYNewCheckReportTableViewCell *)cell; //删除按钮点击事件

- (void)lessBtnClickAction:(ZYNewCheckReportTableViewCell *)cell; //减少数量按钮点击事件

- (void)addBtnClickAction:(ZYNewCheckReportTableViewCell *)cell; //添加数量按钮点击事件

@end

@interface ZYNewCheckReportTableViewCell : UITableViewCell <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong) ZYNewCheckReportViewController *mainVC;
@property (nonatomic, strong) CSmakeAccAddmodel *makeAccAddmodel;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIImageView *circleImg;
@property (nonatomic, strong) UILabel *rowLab;
@property (nonatomic, strong) UILabel *numberLab;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *nameTLab;
@property (nonatomic, strong) UITextField *nameTF;
@property (nonatomic, strong) UILabel *numLab;

@property (nonatomic, strong) UILabel *numberTLab;

@property (nonatomic, strong) UILabel *imageTLab;
@property (nonatomic,strong) UIView *addView;//
@property (nonatomic,assign) CGFloat bottomf;//

@property (nonatomic,strong) NSMutableArray *imageArr;//
@property (nonatomic,strong) UIImageView *myimage;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);
@property (nonatomic, copy) void(^imageArrayChangeBlock)(NSArray *imageArray);

//第二步 代理属性 weakx修饰 id类型(解除耦合性)
@property (nonatomic, weak) id <ZYNewCheckReportDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *mydateSource;


- (void)loadContent;

@end

NS_ASSUME_NONNULL_END
