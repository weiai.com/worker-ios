//
//  GenerateInstallBillForFactoryVC.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/23.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GenerateInstallBillForFactoryVC : BaseViewController

@property(nonatomic,strong)CSmaintenanceListModel *mymodel;

@end

NS_ASSUME_NONNULL_END
