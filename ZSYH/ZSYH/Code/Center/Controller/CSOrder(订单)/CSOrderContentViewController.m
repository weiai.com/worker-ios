//
//  CSOrderContentViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSOrderContentViewController.h"
#import "CSmaintenanceListModel.h"
#import "CSOrderNewTableViewCell.h"
#import "CSmakeAccLostViewController.h"
#import "CSmakeAccLostViewController.h"
#import "CSorderPresentationViewController.h"
#import "CSnewJCBGVC.h"
#import "CSorderLookListViewController.h"
#import "LooksevCimmitViewController.h"
#import "CSmakeAnzhuangViewController.h"
#import "CSnewMakeAccViewController.h"
#import "CheckTestFeeVC.h"
#import "FillDateViewController.h"
#import "ZYGenerateBillsViewController.h" //生成账单
#import "ZYDQCBWCKZDViewController.h" //查看账单
#import "ZYNewCheckReportViewController.h"
#import "ZYGenerateBillingDetailsController.h"
#import "ZYLogisticsInfoViewController.h"
#import "CSScanCodeOrderTableViewCell.h" //扫码订单 Cell
#import "HouBaoAccessoryDetailVC.h" //扫码订单 跳转详情
#import "ZYPersonServiceOrderDetailsVC.h" //个人/家庭订单 服务单详情
#import "ZYElectricalFactoryServiceOrderDetailsVC.h" //电器厂家订单 服务单详情
#import "ZYDQCCJTGSCZDViewController.h" //电器厂家 保内 厂家提供 生成账单
#import "ZYDQCSFZDSCZDViewController.h" //电器厂家 保内 师傅自带 生成账单

@interface CSOrderContentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *bgview;
@property (nonatomic, assign) NSInteger page;

@end

@implementation CSOrderContentViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 1;
    //维修单
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requeset) name:@"applicationWillEnterForeground" object:nil];
}
#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    
    if ([singlTool shareSingTool].needReasfh) {
        [self.myTableView.mj_header beginRefreshing];
        [singlTool shareSingTool].needReasfh = NO;
    }
    [self requeset];
}

-(void)requeset{

    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSInteger ind = [_type integerValue];
    
    ind-=1;
    if (ind >1) {
        ind +=1;
    }
    NSString *stt  = [NSString stringWithFormat:@"%ld",ind];
    if (ind == -1) { //全部
        stt = @"";
        param[@"state"] = stt;
    } else if (ind == 0) { //已接单
        param[@"state"] = @"0";
    } else if (ind == 1) { //已支付
        param[@"state"] = @"3";
    } else if (ind == 3) { //已完成
        param[@"state"] = @"4";
    }
    
    NSString *pagg = [NSString stringWithFormat:@"%ld",(long)_page];
    param[@"pageNum"] = pagg;
    
    [NetWorkTool POST:ClistW param:param success:^(id dic) {
        KMyLog(@"服务单列表 做了分页 %@", dic);
        for (NSDictionary *moDic in [dic objectForKey:@"data"]) {
            [DYModelMaker DY_makeModelWithDictionary:moDic modelKeyword:@"" modelName:@""];
            CSmaintenanceListModel *model = [CSmaintenanceListModel mj_objectWithKeyValues:moDic];
            [weakSelf.mydateSource addObject:model];
        }
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshing];
        
    } other:^(id dic) {
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshingWithNoMoreData];
    } fail:^(NSError *error) {
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        [self.myTableView.mj_footer endRefreshingWithNoMoreData];
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([model.order_model isEqualToString:@"1"]) {
        //显示 普通服务单
        return 180;
    } else {
        //显示 扫码安装单
        return 164;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([model.order_model isEqualToString:@"1"]) {
        //显示 普通服务单
        return UITableViewAutomaticDimension;
    } else {
        //显示 扫码安装单
        return 164;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.bgview.hidden = scrollView.contentOffset.y>0 ? YES:NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    //model.order_model 1普通服务单 2扫码安装单
    if ([model.order_model isEqualToString:@"1"]) {
        //普通服务单 Cell
        CSOrderNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSOrderNewTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
        [cell refasf:model];
        
        cell.orderBlock = ^(NSString * _Nonnull ind) {
            //已接单
            if ([model.order_state integerValue] == 0) {
                //维修类型
                if ([model.fault_type isEqualToString:@"3"]) {
                    //电器厂维修
                    if ([model.user_type isEqualToString:@"2"]) {
                        //需要填写预约日期
                        if (model.order_date.length == 0) {
                            if (ind.integerValue == 0) {
                                //厂家提供 保内 有物流信息 可以查看物流 显示查看物流按钮
                                if ( model.courierNumber.length > 0) {
                                    ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                                    logisticsVC.model = model;
                                    [self.navigationController pushViewController:logisticsVC animated:YES];
                                    return;
                                }
                            } else if (ind.integerValue == 1) { //中间的按钮
                                //填写预约信息
                                FillDateViewController *VC = [[FillDateViewController alloc]init];
                                VC.mymodel = model;
                                VC.myblock = ^(NSString * _Nonnull str) {
                                    [self requeset];
                                };
                                [self.navigationController pushViewController:VC animated:YES];
                                KMyLog(@"订单没有预约信息 填写预约信息");
                            } else if (ind.integerValue == 2) { //最右边的按钮,查看详情
                                [self lookServerOrderDetailWithModel:model];
                                return ;
                            }
                        } else { //不需要填写预约日期
                            //最左侧按钮
                            if (ind.integerValue == 0) {
                                //保内 厂家提供 有物流信息 显示 查看物流按钮
                                if (model.courierNumber.length > 0) {
                                    ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                                    logisticsVC.model = model;
                                    [self.navigationController pushViewController:logisticsVC animated:YES];
                                    KMyLog(@"保内 厂家提供 有物流信息 显示 查看物流 111");
                                    return;
                                }
                            }
                            //中间的按钮
                            if (ind.integerValue == 1) {
                                //保外
                                if ([model.is_protect isEqualToString:@"0"]) {
                                    //跳转到生成账单页面
                                    ZYGenerateBillsViewController *generaVC = [[ZYGenerateBillsViewController alloc] init];
                                    generaVC.mymodel = model;
                                    generaVC.myblock = ^(NSString * _Nonnull str) {
                                        [self requeset];
                                    };
                                    [self.navigationController pushViewController:generaVC animated:YES];
                                    KMyLog(@"电器厂维修订单 保外 生成账单");
                                } else if ([model.is_protect isEqualToString:@"1"]){ //保内
                                    //电器厂提供配件,保内且厂家提供  test_order_id为空，显示 生成检测报告
                                    if (model.test_order_id.length ==0 && [model.product_source isEqualToString:@"1"]) {
                                        //厂家提供配件,生成检测报告
                                        ZYNewCheckReportViewController *VC = [ZYNewCheckReportViewController new];
                                        VC.mymodel = model;
                                        [self.navigationController pushViewController:VC animated:YES];
                                        return;
                                        KMyLog(@"电器厂维修订单 保内 厂家提供 生成账单");
                                        //电器厂提供配件,保内且厂家提供  test_order_id不为空，显示生成账单按钮
                                    } else if (model.test_order_id.length >0 && [model.product_source isEqualToString:@"1"]) {
                                        //电器厂订单 保内 厂家提供 生成账单
                                        ZYDQCCJTGSCZDViewController *vc = [[ZYDQCCJTGSCZDViewController alloc] init];
                                        vc.mymodel = model;
                                        [self.navigationController pushViewController:vc animated:YES];
                                        KMyLog(@"电器厂订单 保内 厂家提供 生成账单");
                                    } else if (![model.product_source isEqualToString:@"1"]) {
                                        //电器厂维修订单 保内 师傅自带 生成账单
                                        ZYDQCSFZDSCZDViewController *vc = [[ZYDQCSFZDSCZDViewController alloc] init];
                                        vc.mymodel = model;
                                        [self.navigationController pushViewController:vc animated:YES];
                                        KMyLog(@"电器厂维修订单 保内 师傅自带 生成账单");
                                    }
                                }
                            }
                            //右侧按钮
                            if (ind.integerValue == 2) {
                                //查看详情
                                [self lookServerOrderDetailWithModel:model];
                                return;
                            }
                        }
                    } else {  //家庭/企事业维修
                        //个人维修 生成账单
                        if (ind.integerValue == 1) {
                            ZYGenerateBillsViewController *generaVC = [[ZYGenerateBillsViewController alloc] init];
                            generaVC.mymodel = model;
                            generaVC.myblock = ^(NSString * _Nonnull str) {
                                [self requeset];
                            };
                            [self.navigationController pushViewController:generaVC animated:YES];
                            KMyLog(@"个人维修 生成账单 是不是这里");
                        } else { //查看详情
                            [self lookServerOrderDetailWithModel:model];
                            return;
                        }
                    }
                    return;
                } else {
#pragma mark------------------------------非维修类--------------------------------------------
                    //电器厂安装，需要判断是否填写预约日期和时间
                    if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                        //无预约日期
                        //最右边的按钮,查看详情
                        if (ind.integerValue == 2) {
                            [self lookServerOrderDetailWithModel:model];
                            return;
                        } else if (ind.integerValue == 1) { //中间的按钮,填写预约日期
                            FillDateViewController *VC = [[FillDateViewController alloc]init];
                            VC.mymodel = model;
                            VC.myblock = ^(NSString * _Nonnull str) {
                                [self requeset];
                            };
                            [self.navigationController pushViewController:VC animated:YES];
                        }
                    } else {
                        //电器厂安装
                        if ([model.fault_type isEqualToString:@"1"]) {
                            //电器厂安装订单 生成账单
                            if ([model.user_type isEqualToString:@"2"]) {
                                if (ind.intValue == 1) {
                                    GenerateInstallBillForFactoryVC *zdVc = [[GenerateInstallBillForFactoryVC alloc]init];
                                    zdVc.mymodel = model;
                                    [self.navigationController pushViewController:zdVc animated:YES];
                                    KMyLog(@"电器厂安装订单 生成账单 跳转事件在这里");
                                } else if (ind.intValue == 2) {
                                    [self lookServerOrderDetailWithModel:model];
                                    return;
                                }
                            } else {
                                //中间的按钮
                                //个人安装 生成账单
                                if (ind.intValue == 1) {
                                    ZYGenerateBillingDetailsController *zygVC = [[ZYGenerateBillingDetailsController alloc] init];
                                    zygVC.orderNumberStr = model.orderId;
                                    zygVC.newid = model.Id;
                                    zygVC.mymodel = model;
                                    zygVC.myblockj = ^(NSString * _Nonnull str) {
                                        [singlTool shareSingTool].needReasfh = YES;
                                    };
                                    [self.navigationController pushViewController:zygVC animated:YES];
                                }
                                //右侧的按钮
                                else if (ind.intValue == 2) {
                                    [self lookServerOrderDetailWithModel:model];
                                    return;
                                }
                            }
                        } else {
                            if (ind.intValue == 1) {
                                if ([model.fault_type isEqualToString:@"1"]) {
                                    //个人 电器安装
                                    ZYGenerateBillingDetailsController *zygVC = [[ZYGenerateBillingDetailsController alloc] init];
                                    zygVC.orderNumberStr = model.orderId;
                                    zygVC.newid = model.Id;
                                    zygVC.mymodel = model;
                                    zygVC.myblockj = ^(NSString * _Nonnull str) {
                                        [singlTool shareSingTool].needReasfh = YES;
                                    };
                                    [self.navigationController pushViewController:zygVC animated:YES];
                                } else if ([model.fault_type isEqualToString:@"2"]) { //个人 电器清洗
                                    CSmakeAnzhuangViewController *zdVc = [[CSmakeAnzhuangViewController alloc]init];
                                    zdVc.orderNumberStr = model.orderId;
                                    zdVc.neworderid = model.Id;
                                    zdVc.myblockj = ^(NSString * _Nonnull str) {
                                        [self requeset];
                                    };
                                    [self.navigationController pushViewController:zdVc animated:YES];
                                    KMyLog(@"个人清洗订单 生成账单");
                                } else if ([model.fault_type isEqualToString:@"4"]) { //个人 电器拆机
                                    CSmakeAnzhuangViewController *zdVc = [[CSmakeAnzhuangViewController alloc]init];
                                    zdVc.orderNumberStr = model.orderId;
                                    zdVc.neworderid = model.Id;
                                    zdVc.myblockj = ^(NSString * _Nonnull str) {
                                        [self requeset];
                                    };
                                    [self.navigationController pushViewController:zdVc animated:YES];
                                    KMyLog(@"个人拆机订单 生成账单");
                                }
                            } else {
                                [self lookServerOrderDetailWithModel:model];
                            }
                        }
                    }
                }
            } else if ([model.order_state integerValue] == 1) {
                //已检测,只有电器厂-维修-保内-厂家提供才有已检测状态
                //点击左边的按钮
                if ([ind integerValue] == 0) {
                    //电器厂维修保内 查看物流
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                        ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                        logisticsVC.model = model;
                        [self.navigationController pushViewController:logisticsVC animated:YES];
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    }
                } else if ([ind integerValue] == 1) {//点击中间的按钮
                    //生成账单
                    if ([model.fault_type isEqualToString:@"3"]) {//维修类型
                        if ([model.user_type isEqualToString:@"2"]) {//电器厂
                            if ([model.is_protect isEqualToString:@"1"]) {//保内
                                if ([model.product_source isEqualToString:@"1"]) {
                                    //电器厂订单 保内 厂家提供 生成账单
                                    ZYDQCCJTGSCZDViewController *vc = [[ZYDQCCJTGSCZDViewController alloc] init];
                                    vc.mymodel = model;
                                    [self.navigationController pushViewController:vc animated:YES];
                                    KMyLog(@"电器厂订单 保内 厂家提供 生成账单");
                                } else {//师傅自带
                                    ZYDQCSFZDSCZDViewController *vc = [[ZYDQCSFZDSCZDViewController alloc] init];
                                    vc.mymodel = model;
                                    [self.navigationController pushViewController:vc animated:YES];
                                    KMyLog(@"电器厂订单 保内 师傅自带 生成账单");
                                }
                            } else {//保外
                                
                            }
                        } else {//家庭/企事业
                            
                        }
                    } else {//其他类型
                        
                    }
                    if ([model.fault_type isEqualToString:@"3"]) {
                        //生成账单
                        //电器厂维修保内
                        if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                            GenerateBillForFactoryStepOneVC *VC = [GenerateBillForFactoryStepOneVC new];
                            VC.mymodel = model;
                            [self.navigationController pushViewController:VC animated:YES];
                        } else {
                            CSnewMakeAccViewController *zdVc = [[CSnewMakeAccViewController alloc]init];
                            zdVc.orderNumberStr = model.orderId;
                            zdVc.newid= model.Id;
                            zdVc.mybleoc = ^(NSString * _Nonnull str) {
                                [self requeset];
                            };
                            [self.navigationController pushViewController:zdVc animated:YES];
                        }
                    } else {
                        //电器厂保内
                        if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else {
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        }
                    }
                } else {//点击最右边按钮
                    [self lookServerOrderDetailWithModel:model];//查看详情
                    return;
                }
            } else if ([model.order_state integerValue] == 2) { //待支付
                if ([ind integerValue] == 1) {
                    if ([model.product_source isEqualToString:@"1"] && [model.is_protect isEqualToString:@"1"] && model.courierNumber.length > 0 && model.courierCompany.length > 0) {
                        ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                        logisticsVC.model = model;
                        [self.navigationController pushViewController:logisticsVC animated:YES];
                        return;
                    }
                }
                if ([ind integerValue] == 2) {//点击最右边的按钮
                    //电器厂维修保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]&& [model.fault_type isEqualToString:@"3"]) {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    } else {
                        if ([model.fault_type isEqualToString:@"1"]) { //个人安装
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else if ([model.fault_type isEqualToString:@"2"]) { //个人清洗
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else if ([model.fault_type isEqualToString:@"3"]) { //个人维修
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else if ([model.fault_type isEqualToString:@"4"]) { //个人拆机
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else {//其他情况
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        }
                    }
                }
            } else if ([model.order_state integerValue] == 3) {
                //已支付 已完成
                if (ind.integerValue == 1) {
                    //查看物流
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                        ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                        logisticsVC.model = model;
                        [self.navigationController pushViewController:logisticsVC animated:YES];
                        return;
                    }
                }
                if (ind.integerValue == 2) {
                    //电器厂维修保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                        [self lookServerOrderDetailWithModel:model];//查看详情
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看检测报告按钮改为了查看详情
                        return;
                    }
                } else if ([ind integerValue] == 1000) {
                    //更换配件
                    if ([model.fault_type isEqualToString:@"3"]) {
                        //更换配件
                        CSmakeAccLostViewController *zdVc = [[CSmakeAccLostViewController alloc]init];
                        zdVc.orderNumberStr = model.orderId;
                        zdVc.newid = model.Id;
                        zdVc.mybleoc = ^(NSString * _Nonnull str) {
                            [self requeset];
                        };
                        [self.navigationController pushViewController:zdVc animated:YES];
                    } else {
                        //电器厂保内
                        if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        } else {
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                            return;
                        }
                    }
                }
            } else if ([model.order_state integerValue] == 4) {
                //已评价
                if ([ind integerValue] == 0) {
                    //电器厂维修保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        
                        if ( model.courierNumber.length > 0) {
                            ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                            logisticsVC.model = model;
                            [self.navigationController pushViewController:logisticsVC animated:YES];
                            KMyLog(@"已评价 查看物流 按钮 111");
                            return;
                        }
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    }
                } else if ([ind integerValue] == 1) {
                    //电器厂保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                        
                        if ( model.courierNumber.length > 0) {
                            ZYLogisticsInfoViewController *logisticsVC = [[ZYLogisticsInfoViewController alloc] init];
                            logisticsVC.model = model;
                            [self.navigationController pushViewController:logisticsVC animated:YES];
                            KMyLog(@"已评价 查看物流 按钮 222");
                        } else {
                            [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        }
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    }
                } else if ([ind integerValue] == 2) {
                    [self lookServerOrderDetailWithModel:model];//查看评价按钮改为了查看详情
                    return;
                }
            } else if ([model.order_state integerValue] == 5) {//已取消,没有点击按钮
                //已取消
            } else if ([model.order_state integerValue] == 6) {
                //订单中止
                if ([ind integerValue] == 1) {
                    //电器厂维修保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue] && [model.fault_type isEqualToString:@"3"]) {
                        [self lookServerOrderDetailWithModel:model];//查看检测报告按钮改为了查看详情
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看检测报告按钮改为了查看详情
                        return;
                    }
                } else if ([ind integerValue] == 2) {
                    //电器厂保内
                    if ([model.user_type isEqualToString:@"2"] && [model.is_protect boolValue]) {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    } else {
                        [self lookServerOrderDetailWithModel:model];//查看账单按钮改为了查看详情
                        return;
                    }
                }
            }
        };
        return cell;
    } else {
        //扫码安装单 Cell
        CSScanCodeOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSScanCodeOrderTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
        [cell refasf:model];
        kWeakSelf;
        cell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
            switch (ind) {
                case 0: {
                    //跳转到配件详情 页面
                    HouBaoAccessoryDetailVC *detailVC = [[HouBaoAccessoryDetailVC alloc] init];
                    detailVC.model = model;
                    [weakSelf.navigationController pushViewController:detailVC animated:YES];
                }
                    break;
                default:
                    break;
            }
        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

/**
 * 分区头*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

/**
 * 分区脚*/
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = DefaultColor;
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    //model.order_model 1普通服务单 2扫码安装单
    if ([model.order_model isEqualToString:@"1"]) {
        //普通服务单
        [self lookServerOrderDetailWithModel:model];
        [_myTableView reloadData];
    } else {
        //扫码安装单
        [self lookScanCodeOrderDetailWithModel:model];
        [_myTableView reloadData];
    }
}

#pragma mark - 查看服务单详情界面
- (void)lookServerOrderDetailWithModel:(CSmaintenanceListModel *)model {
    
    if ([model.user_type isEqualToString:@"0"]) {
        //个人/家庭订单 服务单详情
        ZYPersonServiceOrderDetailsVC *personvc = [[ZYPersonServiceOrderDetailsVC alloc] init];
        personvc.mymodel = model;
        personvc.orderID = model.orderId;
        [self.navigationController pushViewController:personvc animated:YES];
    } else if ([model.user_type isEqualToString:@"2"]) {
        //电器厂订单 服务单详情
        ZYElectricalFactoryServiceOrderDetailsVC *electrvc = [[ZYElectricalFactoryServiceOrderDetailsVC alloc]init];
        electrvc.mymodel = model;
        electrvc.orderID = model.orderId;
        [self.navigationController pushViewController:electrvc animated:YES];
    }
}

#pragma mark - 查看扫码订单详情
- (void)lookScanCodeOrderDetailWithModel:(CSmaintenanceListModel *)model {
    HouBaoAccessoryDetailVC *vc = [[HouBaoAccessoryDetailVC alloc] init];
    vc.model = model;
    vc.orderId = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        
        if ([self.from isEqualToString:@"tabbar"]) {
            _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49-kTabbarHeight) style:(UITableViewStyleGrouped)];
        } else {
            _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49) style:(UITableViewStyleGrouped)];
        }
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSOrderNewTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSOrderNewTableViewCell"];
        adjustInset(_myTableView);
        [_myTableView registerClass:[CSScanCodeOrderTableViewCell class] forCellReuseIdentifier:@"CSScanCodeOrderTableViewCell"];
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            if (self.mydateSource.count>0) {
                [self.mydateSource removeAllObjects];
            }
            self.page = 1;
            [self requeset];//请求数据
        }];
        self.myTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            self.page += 1;
            [self requeset];//请求数据
        }];
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
