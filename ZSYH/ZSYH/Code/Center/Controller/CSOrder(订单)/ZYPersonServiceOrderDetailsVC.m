//
//  ZYPersonServiceOrderDetailsVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/30.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYPersonServiceOrderDetailsVC.h"
#import "CSmaintenanceListModel.h"
#import "CSmakeAccLostViewController.h"        //生成账单
#import "LooksevCimmitViewController.h"        //查看评价
#import "CSmakeAnzhuangViewController.h"       //生成账单
#import "FillDateViewController.h"             //填写预约信息
#import "ZYGenerateBillsViewController.h"      //生成账单
#import "ZYGenerateBillingDetailsController.h" //个人安装 生成账单
#import "ZYDQCBNSFZDCKZDViewController.h"      //电器厂保内师傅自带查看账单
#import "ZYDQCBNCJTGCKZDViewController.h"      //电器厂白内厂家提供查看账单
#import "ZYPersonInstallCKZDViewController.h"  //个人安装订单查看账单
#import "ZYPersonCleanCKZDViewController.h"    //个人清洗订单查看账单
#import "ZYPersonServiceCKZDViewController.h"  //个人维修订单查看账单
#import "ZYPersonUnloadCKZDViewController.h"   //个人拆机订单查看账单
#import "ZYRefuseToRepairCKZDViewController.h" //个人维修/电器厂保外 拒绝维修查看账单
#import "ZOrderDetailPartDetailVC.h" //配件详情

@interface ZYPersonServiceOrderDetailsVC ()
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIButton *upOneBtn;
//下面4个 按钮
@property(nonatomic,strong)UIButton *firstBtn;
@property(nonatomic,strong)UIButton *twobut;
@property(nonatomic,strong)UIButton *threebut;
@property(nonatomic,strong)UIButton *fourbut;
@end

@implementation ZYPersonServiceOrderDetailsVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"服务单详情";
    //接完单之后跳转进来 返回按钮
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    leftBtn.frame = CGRectMake(0, 0, 30,30);
    UIImage *img = [[UIImage imageNamed:@"left_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    [self requeest];
}

//接完单之后跳转到检测单详情页面 再返回 回到检测单列表页面
- (void)leftBarBtnClicked:(UIButton *)button {
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //禁用侧滑返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    if (_scrollView) {
        [self requeest];
    }
}

-(void)requeest{
    
    _mymodel = [[CSmaintenanceListModel alloc]init];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:(NSUInteger)1];
    param[@"orderId"] = NOTNIL(_orderID);
    kWeakSelf;
    
    [NetWorkTool POST:weixuidContent param:param success:^(id dic) {
        KMyLog(@"个人 服务单详情 %@", dic);
        weakSelf.mymodel = [CSmaintenanceListModel mj_objectWithKeyValues:[dic objectForKeyNotNil:@"data"]];
        if (strIsEmpty(weakSelf.mymodel.orderId)) {
            weakSelf.mymodel.orderId = weakSelf.mymodel.rep_order_id;
        }
        [self showui];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark ------------------- 刷新按钮状态---------------
//重写
-(void)refreshBtnWithModel:(CSmaintenanceListModel *)model
{
    /*
     下面四个按钮显示逻辑汇总
     个人维修单
     rep_order_state = 0 已接单  配件详情
     = 2 待支付  显示  查看账单  配件详情
     = 3已支付  显示  查看账单  配件详情
     = 4已评价 显示  查看账单  配件详情  查看评价
     
     个人拒绝维修
     订单中止 显示 查看账单
     
     个人安装单
     已接单  不显示按钮
     待支付  显示  查看账单
     已支付  显示  查看账单
     已评价 显示   查看账单  查看评价
     
     个人清洗单/拆机
     已接单  不显示按钮
     待支付  显示  查看账单
     已支付  显示  查看账单
     已评价 显示   查看账单  查看评价
     */
    
    self.firstBtn.hidden = YES;
    //个人维修类订单
    if ([model.fault_type isEqualToString:@"3"]) {
        //0 已接单
        if ([model.rep_order_state integerValue] == 0) {
            //家庭/企事业
            if (![model.user_type isEqualToString:@"2"])
            {
                //已接单
                [self.upOneBtn setTitle:@"已接单" forState:(UIControlStateNormal)];
                self.twobut.hidden = YES;
                self.threebut.hidden = YES;
                self.fourbut.hidden = YES;
                //维修 有配件详情 才显示配件详情按钮
                if ([model.azNumber integerValue] > 0) {
                    self.fourbut.hidden = NO;
                    
                    [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                    [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                    self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                    self.fourbut.backgroundColor = [UIColor whiteColor];
                    self.fourbut.layer.borderWidth = 1;
                }
                else
                {
                    self.fourbut.hidden = YES;
                }
            }
        }
        // 2 待支付
        else if ([model.rep_order_state integerValue] == 2) {
            [self.upOneBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = NO;
            self.fourbut.hidden = NO;
            
            //不能查看配件详情
            if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                //查看账单 按钮
                self.threebut.hidden = YES;
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            } else {
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        //已支付
        else if ([model.rep_order_state integerValue] == 3) {
            [self.upOneBtn setTitle:@"已支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = NO;
            self.fourbut.hidden = NO;
            
            if (model.azNumber == nil || model.azNumber == NULL||[model.azNumber isEqualToString:@"0"]) {
                self.threebut.hidden = YES;
                
                [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            } else {
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        //4已评价
        else if ([model.rep_order_state integerValue] == 4) {
            [self.upOneBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
            self.twobut.hidden = NO;
            self.threebut.hidden = NO;
            self.fourbut.hidden = NO;
            
            //没有 配件详情 不显示配件详情按钮
            if (model.azNumber == nil || model.azNumber == NULL ||[model.azNumber isEqualToString:@"0"]) {
                self.twobut.hidden = YES;
                [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            } else {
                [self.twobut setTitle:@"查看账单" forState:(UIControlStateNormal)];
                [self.twobut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.twobut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.twobut.backgroundColor = [UIColor whiteColor];
                self.twobut.layer.borderWidth = 1;
                
                [self.threebut setTitle:@"配件详情" forState:(UIControlStateNormal)];
                [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.threebut.backgroundColor = [UIColor whiteColor];
                self.threebut.layer.borderWidth = 1;
                
                [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
                [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
                self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
                self.fourbut.backgroundColor = [UIColor whiteColor];
                self.fourbut.layer.borderWidth = 1;
            }
        }
        // 5已取消
        else if ([model.rep_order_state integerValue] == 5) {
            [self.upOneBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
        //订单中止已支付
        else if ([model.rep_order_state integerValue] == 6) {
            [self.upOneBtn setTitle:@"订单中止已支付" forState:(UIControlStateNormal)];
            self.upOneBtn.titleLabel.font = FontSize(14);
            [self.upOneBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.upOneBtn.width = 120;
            self.upOneBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
            
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor whiteColor].CGColor;
            [self.fourbut setBackgroundColor:[UIColor clearColor]];
            self.fourbut.layer.borderWidth = 1;
        }
    }
    //个人安装 清洗、拆机类订单
    else {
        //已接单
        if ([model.rep_order_state integerValue] == 0) {
            [self.upOneBtn setTitle:@"已接单" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
        //待支付
        else if ([model.rep_order_state integerValue] == 2) {
            [self.upOneBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        //已支付
        else if ([model.rep_order_state integerValue] == 3) {
            
            [self.upOneBtn setTitle:@"已支付" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            
            [self.fourbut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        //4已评价，
        else if ([model.rep_order_state integerValue] == 4){
            
            [self.upOneBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            
            [self.threebut setTitle:@"查看账单" forState:(UIControlStateNormal)];
            [self.threebut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.threebut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.threebut.backgroundColor = [UIColor whiteColor];
            self.threebut.layer.borderWidth = 1;
            
            [self.fourbut setTitle:@"查看评价" forState:(UIControlStateNormal)];
            [self.fourbut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
            self.fourbut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
            self.fourbut.backgroundColor = [UIColor whiteColor];
            self.fourbut.layer.borderWidth = 1;
        }
        //5已取消
        else if ([model.rep_order_state integerValue] == 5){
            [self.upOneBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
            self.twobut.hidden = YES;
            self.threebut.hidden = YES;
            self.fourbut.hidden = YES;
        }
    }
}

#pragma mark ------- 服务单详情 查看账单  配件详情  查看评价 按钮 点击跳转事件---------
-(void)fourAction:(UIButton *)but{
    /*ind:0 是 第一个,表示状态
     0 是第一个可点击按钮
     1 是第二个可点击按钮
     2 是第三个可点击按钮
     3 是第四个可点击按钮
     */
    //NSInteger ind = but.tag-9527;
    
    NSString *btnTitle = but.titleLabel.text;//查看账单  配件详情  查看评价
    CSmaintenanceListModel *model = [[CSmaintenanceListModel alloc]init];
    model = _mymodel;
    //已接单
    if ([model.rep_order_state integerValue] == 0)
    {   //维修
        if ([model.fault_type isEqualToString:@"3"])
        {
            if ([model.azNumber integerValue] > 0)
            {
                if ([btnTitle isEqualToString:@"配件详情"]) {
                    //可以查看配件详情
                    ZOrderDetailPartDetailVC *vc= [[ZOrderDetailPartDetailVC alloc] init];
                    vc.orderModel = model;
                    [self.navigationController pushViewController:vc animated: YES];
                    KMyLog(@"个人维修订单 已接单 查看配件详情");
                }
            }
        }
    }
    //待支付
    else if ([model.rep_order_state integerValue] == 2)
    {
        if ([btnTitle isEqualToString:@"查看账单"]) {
            //安装
            if ([model.fault_type isEqualToString:@"1"]) {
                ZYPersonInstallCKZDViewController *vc = [[ZYPersonInstallCKZDViewController alloc]init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人安装订单 查看账单 是不是这里");
            }
            //清洗
            else if ([model.fault_type isEqualToString:@"2"]) {
                ZYPersonCleanCKZDViewController *vc = [[ZYPersonCleanCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人清洗订单 查看账单 是不是这里");
            }
            //维修
            else if ([model.fault_type isEqualToString:@"3"]) {
                //判断订单是否是提前中止
                if ([model.orderType integerValue] == 1) {
                    ZYRefuseToRepairCKZDViewController *vc = [[ZYRefuseToRepairCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单 提前中止");
                }
                else
                {
                    ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单");
                }
            }
            //拆机
            else if ([model.fault_type isEqualToString:@"4"]) {
                //查看账单
                ZYPersonUnloadCKZDViewController *vc = [[ZYPersonUnloadCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人拆机订单 查看账单 未支付 是这里不是");
            }
        }
        //可以查看配件详情
        else if ([btnTitle isEqualToString:@"配件详情"])
        {
            ZOrderDetailPartDetailVC *vc= [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated: YES];
            KMyLog(@"个人维修订单 未支付 查看配件详情");
        }
    }
    //已支付
    else if ([model.rep_order_state integerValue] == 3)
    {
        if ([btnTitle isEqualToString:@"查看账单"])
        {
            //安装
            if ([model.fault_type isEqualToString:@"1"]) {
                ZYPersonInstallCKZDViewController *vc = [[ZYPersonInstallCKZDViewController alloc]init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人安装订单 查看账单 是不是这里");
            }
            //清洗
            else if ([model.fault_type isEqualToString:@"2"]) {
                ZYPersonCleanCKZDViewController *vc = [[ZYPersonCleanCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人清洗订单 查看账单 是不是这里");
            }
            //维修
            else if ([model.fault_type isEqualToString:@"3"]) {
                //判断订单是否是提前中止
                if ([model.orderType integerValue] == 1) {
                    ZYRefuseToRepairCKZDViewController *vc = [[ZYRefuseToRepairCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单 提前中止");
                }
                else
                {
                    ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单");
                }
            }
            //拆机
            else if ([model.fault_type isEqualToString:@"4"]) {
                //查看账单
                ZYPersonUnloadCKZDViewController *vc = [[ZYPersonUnloadCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人拆机订单 查看账单 未支付 是这里不是");
            }
        }
        //可以查看配件详情
        else if ([btnTitle isEqualToString:@"配件详情"])
        {
            ZOrderDetailPartDetailVC *vc= [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated: YES];
            KMyLog(@"个人维修订单 未支付 查看配件详情");
        }
    }
    //已评价
    else if ([model.rep_order_state integerValue] == 4)
    {
        if ([btnTitle isEqualToString:@"查看账单"])
        {
            //安装
            if ([model.fault_type isEqualToString:@"1"]) {
                ZYPersonInstallCKZDViewController *vc = [[ZYPersonInstallCKZDViewController alloc]init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人安装订单 查看账单 是不是这里");
            }
            //清洗
            else if ([model.fault_type isEqualToString:@"2"]) {
                ZYPersonCleanCKZDViewController *vc = [[ZYPersonCleanCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人清洗订单 查看账单 是不是这里");
            }
            //维修
            else if ([model.fault_type isEqualToString:@"3"]) {
                //判断订单是否是提前中止
                if ([model.orderType integerValue] == 1) {
                    ZYRefuseToRepairCKZDViewController *vc = [[ZYRefuseToRepairCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单 提前中止");
                }
                else
                {
                    ZYPersonServiceCKZDViewController *vc = [[ZYPersonServiceCKZDViewController alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                    KMyLog(@"个人维修订单 未支付 查看账单");
                }
            }
            //拆机
            else if ([model.fault_type isEqualToString:@"4"]) {
                //查看账单
                ZYPersonUnloadCKZDViewController *vc = [[ZYPersonUnloadCKZDViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
                KMyLog(@"个人拆机订单 查看账单 未支付 是这里不是");
            }
        }
        //可以查看配件详情
        else if ([btnTitle isEqualToString:@"配件详情"])
        {
            ZOrderDetailPartDetailVC *vc= [[ZOrderDetailPartDetailVC alloc] init];
            vc.orderModel = model;
            [self.navigationController pushViewController:vc animated: YES];
            KMyLog(@"个人维修订单 未支付 查看配件详情");
        }
        //查看评价
        else if ([btnTitle isEqualToString:@"查看评价"]){
            LooksevCimmitViewController *cimmit = [[LooksevCimmitViewController alloc]init];
            cimmit.orderid = model.orderId;
            [self.navigationController pushViewController:cimmit animated:YES];
        }
    }
    //已取消
    else if ([model.rep_order_state integerValue] == 5)
    {
        
    }
    //订单中止
    else  if ([model.rep_order_state integerValue] == 6)
    {
        if ([btnTitle isEqualToString:@"查看账单"])
        {
            //查看账单
            ZYRefuseToRepairCKZDViewController *vc = [[ZYRefuseToRepairCKZDViewController alloc] init];
            vc.model = model;
            [self.navigationController pushViewController:vc animated:YES];
            KMyLog(@"个人订单 维修 拒绝维修 查看账单");
        }
    }
}

#pragma mark------------刷新UI--------------
-(void)showui{
    [self.view addSubview:self.scrollView];
    [self.scrollView removeAllSubviews];
    
    CGFloat wei = 60;
    CGFloat left1 = 26;
    CGFloat left2 = 14;
    
    UIView *bbgvieww = [[UIView alloc]init];
    [self.scrollView addSubview:bbgvieww];
    
    if ([self.mymodel.rep_order_state integerValue] == 6) {
        UIImageView *redIV = [UIImageView new];
        redIV.frame = CGRectMake(0, 0, ScreenW-20, 73);
        redIV.image = [UIImage imageNamed:@"路径 1829"];
        [bbgvieww addSubview:redIV];
    }
    
    self.upOneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.upOneBtn.frame = CGRectMake(16, 10, 60, 26);
    [self.upOneBtn setTitleColor:[UIColor colorWithHexString:@"#F88B1F"] forState:(UIControlStateNormal)];
    self.upOneBtn.titleLabel.font = FontSize(12);
    [bbgvieww addSubview:self.upOneBtn];
    
    CGFloat butwir = 79;
    CGFloat fengxi = (KWIDTH -20 - (butwir*4))/5;
    
    NSArray *titleArr = @[@"检测报告",@"查看账单",@"查看评价",@"配件详情",];
    for (int i = 0; i<4; i++) {
        UIButton *mybut = [UIButton buttonWithType:UIButtonTypeCustom];
        mybut.frame = CGRectMake(fengxi+(butwir+fengxi)*i, 36, butwir, 26);
        [mybut setTitle:titleArr[i] forState:(UIControlStateNormal)];
        mybut.titleLabel.font = FontSize(12);
        mybut.tag = 9527+i;
        [mybut addTarget:self action:@selector(fourAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [bbgvieww addSubview:mybut];
        switch (i) {
            case 0:
            {
                self.firstBtn = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 1:
            {
                self.twobut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 2:
            {
                self.threebut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
                mybut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
                mybut.layer.borderWidth = 1;
            }
                break;
            case 3:
            {self.fourbut = mybut;
                [mybut setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:(UIControlStateNormal)];
                [mybut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
                mybut.layer.masksToBounds = YES;
                mybut.layer.cornerRadius = 13;
            }
                break;
            default:
                break;
        }
    }
    //刷新按钮 及其他数据
    [self refreshBtnWithModel:_mymodel];
    
    UILabel *lable16 = [[UILabel alloc]initWithFrame:CGRectMake(0, 73, KWIDTH-20, 2)];
    [bbgvieww addSubview:lable16];
    lable16.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    CGFloat hh = 20+lable16.bottom;
    
    UILabel *secvice = [[UILabel alloc]initWithFrame:CGRectMake(left1, hh, wei, 14)];
    secvice.font = FontSize(14);
    secvice.textColor = K999999;
    secvice.text = @"服务类型";
    [self.scrollView addSubview:secvice];
    
    UILabel *secvicelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, hh, KWIDTH-left2-left1-wei-left1-20, 14)];
    secvicelb.font = KFontPingFangSCMedium(14);
    secvicelb.textColor = K666666;
    secvicelb.text = _mymodel.service_name;
    [self.scrollView addSubview:secvicelb];
    
    UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, secvicelb.bottom+10, KWIDTH-left1-20, 1)];
    line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line2];
    
    UILabel *produrt = [[UILabel alloc]initWithFrame:CGRectMake(left1, line2.bottom+8, wei, 14)];
    produrt.font = FontSize(14);
    produrt.textColor = K999999;
    produrt.text = @"电器品类";
    [self.scrollView addSubview:produrt];
    
    CGFloat widd = [NSString widthWithFont:14 text:_mymodel.fault_name];
    UILabel *produrtlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line2.bottom+8,widd, 14)];
    produrtlb.font = KFontPingFangSCMedium(14);
    produrtlb.textColor = K666666;
    produrtlb.text = _mymodel.fault_name;
    [self.scrollView addSubview:produrtlb];
    
    UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(left1, produrt.bottom+10, KWIDTH-left1-20, 1)];
    line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.scrollView addSubview:line3];
    
    if ([_mymodel.user_type integerValue] == 1) {//家庭/企业报修
        
        UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
        number.font = FontSize(14);
        number.textColor = K999999;
        number.text = @"报修数量";
        [self.scrollView addSubview:number];
        UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
        numberlb.font = KFontPingFangSCMedium(14);
        numberlb.textColor = K666666;
        numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
        [self.scrollView addSubview:numberlb];
        
        UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
        line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line44];
        //维修增加故障原因
        if ([self.mymodel.fault_type isEqualToString:@"3"]) { //个人维修订单 服务单详情
            
            UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
            faultReasonL.font = KFontPingFangSCMedium(14);
            faultReasonL.textColor = K999999;
            faultReasonL.text = @"故障原因";
            [self.scrollView addSubview:faultReasonL];
            
            UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
            faultReason.font = KFontPingFangSCMedium(14);
            faultReason.textColor = K666666;
            faultReason.text = _mymodel.common_failures;
            [self.scrollView addSubview:faultReason];
            
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
            line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line];
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = KFontPingFangSCMedium(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = KFontPingFangSCMedium(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = KFontPingFangSCMedium(14);
            addresslb.textColor = K666666;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
            name.font = KFontPingFangSCMedium(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH -233, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            phoneA.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(KWIDTH-158, line11.bottom ,33,33);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, 105, 33)];
            phone.font = KFontPingFangSCMedium(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentRight;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = KFontPingFangSCMedium(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = KFontPingFangSCMedium(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
            
        } else {
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = KFontPingFangSCMedium(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = KFontPingFangSCMedium(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = KFontPingFangSCMedium(14);
            addresslb.textColor = K666666;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
            name.font = KFontPingFangSCMedium(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH -233, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            phoneA.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(KWIDTH-158, line11.bottom ,33,33);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, 105, 33)];
            phone.font = KFontPingFangSCMedium(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentRight;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = KFontPingFangSCMedium(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = KFontPingFangSCMedium(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        }
        
    } else {
        
        UILabel *number = [[UILabel alloc]initWithFrame:CGRectMake(left1, line3.bottom+8, 90, 14)];
        number.font = FontSize(14);
        number.textColor = K999999;
        number.text = @"报修数量";
        [self.scrollView addSubview:number];
        
        UILabel *numberlb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line3.bottom+8, 90, 14)];
        numberlb.font = KFontPingFangSCMedium(14);
        numberlb.textColor = K666666;
        numberlb.text = [NSString stringWithFormat:@"%.0f台",_mymodel.repair_number];
        [self.scrollView addSubview:numberlb];
        
        UILabel *line44 = [[UILabel alloc]initWithFrame:CGRectMake(left1, number.bottom+10, KWIDTH-left1-20, 1)];
        line44.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.scrollView addSubview:line44];
        
        //维修增加故障原因
        if ([self.mymodel.fault_type isEqualToString:@"3"]) {
            
            UILabel *faultReasonL = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, wei, 14)];
            faultReasonL.font = FontSize(14);
            faultReasonL.textColor = K999999;
            faultReasonL.text = @"故障原因";
            [self.scrollView addSubview:faultReasonL];
            
            UILabel *faultReason = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, 14)];
            faultReason.font = KFontPingFangSCMedium(14);
            faultReason.textColor = K666666;
            faultReason.text = _mymodel.common_failures;
            [self.scrollView addSubview:faultReason];
            
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(left1, faultReason.bottom+10, KWIDTH-left1-20, 1)];
            line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line];
            
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = KFontPingFangSCMedium(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = KFontPingFangSCMedium(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = KFontPingFangSCMedium(14);
            addresslb.textColor = K666666;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
            name.font = KFontPingFangSCMedium(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH -233, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            phoneA.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(KWIDTH-158, line11.bottom ,33,33);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, 105, 33)];
            phone.font = KFontPingFangSCMedium(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentRight;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = KFontPingFangSCMedium(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = KFontPingFangSCMedium(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        } else {
            UILabel *guzhang = [[UILabel alloc]initWithFrame:CGRectMake(left1, line44.bottom+8, 90, 14)];
            guzhang.font = FontSize(14);
            guzhang.textColor = K999999;
            guzhang.text = @"备注信息";
            [self.scrollView addSubview:guzhang];
            
            CGFloat heig = [NSString heightWithWidth:KWIDTH-left2-left1-wei-left1-20 font:14 text:_mymodel.fault_comment];
            UILabel *guzhanglb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line44.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            guzhanglb.font = FontSize(14);
            guzhanglb.textColor = K999999;
            guzhanglb.text = _mymodel.fault_comment;
            guzhanglb.numberOfLines = 0;
            [self.scrollView addSubview:guzhanglb];
            
            heig = 14;
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(left1, guzhanglb.bottom+10, KWIDTH-left1-20, 1)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            UILabel *yuyueTime = [[UILabel alloc]initWithFrame:CGRectMake(left1, line4.bottom+8, 90, 14)];
            yuyueTime.font = FontSize(14);
            yuyueTime.textColor = K999999;
            yuyueTime.text = @"上门日期";
            [self.scrollView addSubview:yuyueTime];
            
            UILabel *yuyueTimelb = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line4.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb.font = KFontPingFangSCMedium(14);
            yuyueTimelb.textColor = K666666;
            yuyueTimelb.text = _mymodel.order_date;
            
            NSMutableArray *muarrt =[NSMutableArray arrayWithArray:[_mymodel.order_date componentsSeparatedByString:@"-"]];
            if (muarrt.count>2) {
                NSString *yesr =[NSString stringWithFormat:@"%ld",[muarrt[0] integerValue]];
                NSString *mouth =[NSString stringWithFormat:@"%ld",[muarrt[1] integerValue]];
                NSString *day =[NSString stringWithFormat:@"%ld",[muarrt[2] integerValue]];
                yuyueTimelb.text = [NSString stringWithFormat:@"%@年%@月%@日",yesr,mouth,day];
            }
            [self.scrollView addSubview:yuyueTimelb];
            
            UILabel *line5 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb.bottom+10, KWIDTH-left1-20, 1)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            UILabel *yuyueTime2 = [[UILabel alloc]initWithFrame:CGRectMake(left1, line5.bottom+8, 90, 14)];
            yuyueTime2.font = FontSize(14);
            yuyueTime2.textColor = K999999;
            yuyueTime2.text = @"上门时间";
            [self.scrollView addSubview:yuyueTime2];
            
            UILabel *yuyueTimelb2 = [[UILabel alloc]initWithFrame:CGRectMake(left1+wei+left2, line5.bottom+8, KWIDTH-left2-left1-wei-left1-20, heig)];
            yuyueTimelb2.font = KFontPingFangSCMedium(14);
            yuyueTimelb2.textColor = K666666;
            NSInteger  indd = [[_mymodel.order_time substringToIndex:2] integerValue];
            NSString *ap = indd < 12 ? @"上午":@"下午";
            yuyueTimelb2.text = [NSString stringWithFormat:@"%@ %@", ap, _mymodel.order_time];
            [self.scrollView addSubview:yuyueTimelb2];
            
            UILabel *line6 = [[UILabel alloc]initWithFrame:CGRectMake(left1, yuyueTimelb2.bottom+10, KWIDTH-left1-20, 1)];
            line6.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line6];
            
            UILabel *address = [[UILabel alloc]initWithFrame:CGRectMake(left1, line6.bottom+8, 30, 14)];
            address.font = FontSize(14);
            address.textColor = K999999;
            address.text = @"地址";
            [self.scrollView addSubview:address];
            
            CGFloat heig2 = [NSString heightWithWidth:KWIDTH-80 fontNameString:@"PingFangSC-Medium" fontSize:14 text:_mymodel.user_address];
            
            UILabel *addresslb = [[UILabel alloc]initWithFrame:CGRectMake(67, line6.bottom+8, KWIDTH-80 , heig2)];
            addresslb.font = KFontPingFangSCMedium(14);
            addresslb.textColor = K666666;
            addresslb.text = _mymodel.user_address;
            addresslb.numberOfLines = 0;
            [self.scrollView addSubview:addresslb];
            
            UILabel *line11 = [[UILabel alloc]initWithFrame:CGRectMake(left1, addresslb.bottom+10, KWIDTH-left1-20, 1)];
            line11.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line11];
            
            UILabel *userLab = [[UILabel alloc]initWithFrame:CGRectMake(left1, line11.bottom+9, 30, 14)];
            userLab.font = FontSize(14);
            userLab.textColor = K999999;
            userLab.text = @"用户";
            [self.scrollView addSubview:userLab];
            
            UILabel *name = [[UILabel alloc]initWithFrame:CGRectMake(65, line11.bottom+9, 70, 14)];
            name.font = KFontPingFangSCMedium(14);
            name.textColor = K666666;
            name.text = _mymodel.user_name;
            [self.scrollView addSubview:name];
            
            UILabel *phoneA = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH -233, line11.bottom+9, 75, 14)];
            phoneA.font = FontSize(14);
            phoneA.textColor = K999999;
            phoneA.text = @"用户手机号";
            phoneA.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:phoneA];
            
            UIButton *phoneB = [UIButton new];
            phoneB.frame = CGRectMake(KWIDTH-158, line11.bottom ,33,33);
            [phoneB setBackgroundImage:[UIImage imageNamed:@"orginphone"] forState:(UIControlStateNormal)];
            [phoneB addTarget:self action:@selector(call) forControlEvents:(UIControlEventTouchUpInside)];
            [self.scrollView addSubview:phoneB];
            
            UILabel *phone = [[UILabel alloc]initWithFrame:CGRectMake(phoneB.right, line11.bottom, 105, 33)];
            phone.font = KFontPingFangSCMedium(14);
            phone.textColor = K666666;
            phone.textAlignment = NSTextAlignmentRight;
            phone.text = _mymodel.user_phone;
            [self.scrollView addSubview:phone];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(callaction)];
            phone.userInteractionEnabled = YES;
            [phone addGestureRecognizer:tap];
            
            UILabel *line7 = [[UILabel alloc]initWithFrame:CGRectMake(left1, userLab.bottom+9, KWIDTH-left1-20, 1)];
            line7.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line7];
            
            UILabel *dianti = [[UILabel alloc]initWithFrame:CGRectMake(left1, line7.bottom+7, 30, 14)];
            dianti.font = FontSize(14);
            dianti.textColor = K999999;
            dianti.text = @"电梯";
            [self.scrollView addSubview:dianti];
            UILabel *diantilb = [[UILabel alloc]initWithFrame:CGRectMake(67, line7.bottom+7, 30, 14)];
            diantilb.font = KFontPingFangSCMedium(14);
            diantilb.textColor = K666666;
            diantilb.text = @"无";
            if ([_mymodel.has_elevator integerValue] == 1) {
                diantilb.text = @"有";
            }
            [self.scrollView addSubview:diantilb];
            
            UILabel *louceng = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-105, line7.bottom+7, 30, 14)];
            louceng.font = FontSize(14);
            louceng.textColor = K999999;
            louceng.text = @"楼层";
            [self.scrollView addSubview:louceng];
            
            UILabel *loucenglb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-60, line7.bottom+7, 35, 14)];
            loucenglb.font = KFontPingFangSCMedium(14);
            loucenglb.textColor = K666666;
            loucenglb.text = [NSString stringWithFormat:@"%@楼",_mymodel.floorNum];
            [self.scrollView addSubview:loucenglb];
            
            UILabel *line8 = [[UILabel alloc]initWithFrame:CGRectMake(left1, loucenglb.bottom+10, KWIDTH-left1-20, 1)];
            line8.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line8];
            
            UILabel *ordernumLB = [[UILabel alloc]initWithFrame:CGRectMake(15, line8.bottom+5, KWIDTH-50, 17)];
            ordernumLB.font = FontSize(12);
            ordernumLB.textColor = K999999;
            ordernumLB.text = [NSString stringWithFormat:@"订单编号: %@",_mymodel.Id];
            [bbgvieww addSubview:ordernumLB];
            
            bbgvieww.frame = CGRectMake(10, 10, KWIDTH-20, ordernumLB.bottom+20);
            bbgvieww.backgroundColor = [UIColor whiteColor];
            [bbgvieww shadowshadowOpacity:0.2 borderWidth:1 borderColor:[UIColor whiteColor] erRadius:12 shadowColor:[UIColor colorWithHexString:@"#000000"] shadowRadius:3 shadowOffset:CGSizeMake(1, 1)];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, bbgvieww.bottom+20);
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

- (void)call {
    [HFTools callMobilePhone:_mymodel.user_phone];
}

-(void)callaction{
    [HFTools callMobilePhone:_mymodel.user_phone];
}

@end
