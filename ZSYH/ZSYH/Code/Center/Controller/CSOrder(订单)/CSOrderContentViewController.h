//
//  CSOrderContentViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSOrderContentViewController : BaseViewController
@property(nonatomic,copy)NSString *type;
@property(nonatomic,copy)NSString *from;
@end

NS_ASSUME_NONNULL_END
