//
//  CheckTestReportForFactoryVC.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/22.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CheckTestReportForFactoryVC.h"
#import "SYBigImage.h"

@interface CheckTestReportForFactoryVC ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *orderLB;
@property(nonatomic,strong)UILabel *guzhnagPruLB;
@property(nonatomic,strong)UILabel *guzhuangContentLB;
@property(nonatomic,strong)UILabel *yugufeiyongLB;
@property(nonatomic,strong)NSMutableDictionary *myDic;

@end

@implementation CheckTestReportForFactoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.title = @"检测报告";
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.myDic = [NSMutableDictionary dictionaryWithCapacity:1];
    [self request];
}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    param[@"orderId"] = NOTNIL(_model.test_order_id);
    [NetWorkTool POST:getTestReportByTestId param:param success:^(id dic) {
        KMyLog(@"查看结果%@",dic);
        NSDictionary *mudic = [dic objectForKey:@"data"];
        self.myDic = [mudic mutableCopy];
        
        [self showui];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showui{
    
    [self.view addSubview:self.scrollView];
    CGFloat lef = 46;
    
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(16, 5, KWIDTH, 17)];
    order.text =[NSString stringWithFormat:@"订单编号:%@",self.model.test_order_id];
    order.font = FontSize(12);
    order.textColor = [UIColor colorWithHexString:@"#979797"];
    [self.scrollView addSubview:order];
    
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 27, KWIDTH-20, KHEIGHT-27-kNaviHeight)];
    bgimage.image = imgname(@"jiancebaogaoBgView");
    [self.scrollView addSubview:bgimage];
    
    UIImageView *bgimageup = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-170)/2, order.bottom+61, 170, 146)];
    bgimageup.image = imgname(@"jiancebaogaoupview");
    [self.scrollView addSubview:bgimageup];
    
    UILabel *guzhangh = [[UILabel alloc]initWithFrame:CGRectMake(lef, bgimageup.bottom+29, 67, 14)];
    guzhangh.text =@"产品品类:";
    guzhangh.font = FontSize(12);
    guzhangh.textColor = [UIColor colorWithHexString:@"#333333"];
    [self.scrollView addSubview:guzhangh];
    
    UILabel *guzhanghlb = [[UILabel alloc]initWithFrame:CGRectMake(guzhangh.right, bgimageup.bottom+29, 67+100, 14)];
    guzhanghlb.text =[_myDic objectForKey:@"fault_name"];
    guzhanghlb.font = FontSize(12);
    guzhanghlb.textColor = [UIColor colorWithHexString:@"#444444"];
    [self.scrollView addSubview:guzhanghlb];
    
    UILabel *guzhangms = [[UILabel alloc]initWithFrame:CGRectMake(lef, guzhangh.bottom+10, 67, 14)];
//    guzhangms.text =@"故障描述:";
    guzhangms.text =@"故障说明:";
    guzhangms.font = FontSize(12);
    guzhangms.textColor = [UIColor colorWithHexString:@"#333333"];
//    guzhangms.hidden = YES;
    [self.scrollView addSubview:guzhangms];
    
    CGFloat fff = [NSString heightWithWidth:KWIDTH-115-46 font:12 text:[_myDic objectForKey:@"fault_comment"]];
    UILabel *guzhangmslb = [[UILabel alloc]initWithFrame:CGRectMake(guzhangh.right, guzhanghlb.bottom+10, KWIDTH-115-46, fff)];
    guzhangmslb.text =[_myDic objectForKey:@"fault_comment"];
    guzhangmslb.font = FontSize(12);
    guzhangmslb.numberOfLines = 0;
    guzhangmslb.textColor = [UIColor colorWithHexString:@"#444444"];
//    guzhangmslb.hidden = YES;
    [self.scrollView addSubview:guzhangmslb];
    
    UILabel *guzhangL = [[UILabel alloc]initWithFrame:CGRectMake(lef, guzhangms.bottom+10, 67, 14)];
    guzhangL.text =@"故障配件:";
    guzhangL.font = FontSize(12);
    guzhangL.textColor = [UIColor colorWithHexString:@"#333333"];
    
    if ([_model.product_source isEqualToString:@"1"]) {//厂家提供配件
        [self.scrollView addSubview:guzhangL];
    } else if ([_model.product_source isEqualToString:@"2"]) {//师傅自带配件
        
    }
    
    CGFloat h = guzhangL.bottom;
    NSArray *pruArr = [_myDic objectForKey:@"products"];
    CGFloat imageViewWidth = 65;
    CGFloat imageViewTopMargin = 15;
    CGFloat imageViewBottomMargin = 15;
    CGFloat imageViewSpace = 5;
    if (pruArr.count >0) {
        for (int i = 0; i<pruArr.count; i++) {
            UIView *leftV = [UIView new];
            leftV.frame = CGRectMake(lef, h + 10 + (12 + imageViewTopMargin + imageViewWidth + imageViewBottomMargin)*i, 12, 12);
            leftV.backgroundColor = [UIColor brownColor];
            [self.scrollView addSubview:leftV];
            
            NSDictionary *mydic = pruArr[i];
            UILabel *rightL = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100-50, leftV.top, 144, 12)];
            rightL.textColor = K999999;
            rightL.font = FontSize(12);
            rightL.textAlignment = NSTextAlignmentRight;
            rightL.text =[NSString stringWithFormat:@"%@ *%@",[mydic objectForKey:@"product_name"],[mydic objectForKey:@"count"]];
            [self.scrollView addSubview:rightL];
            
            //配件图片
            NSDictionary *products = pruArr[i];
            NSArray *imgs = products[@"imgs"];
            if (imgs.count > 0) {
                for (int j = 0; j < imgs.count; j++) {
                    UIImageView *myimage = [[UIImageView alloc]init];
                    [self.scrollView addSubview:myimage];
                    [myimage sd_setImageWithURL:[NSURL URLWithString:[imgs[j] objectForKey:@"imageUrl"]] placeholderImage:defaultImg];
                    myimage.frame = CGRectMake(lef + (imageViewWidth + imageViewSpace) * j, leftV.bottom + imageViewTopMargin, imageViewWidth, imageViewWidth);
                    myimage.userInteractionEnabled = YES;
                    SYBigImage * bigI = [[SYBigImage alloc]init];
                    [myimage addGestureRecognizer:bigI];
                }
            }
            if (i == (pruArr.count-1)) {
                h = leftV.bottom + imageViewTopMargin + imageViewWidth + imageViewBottomMargin;
            }
        }
    }
    
    UILabel *bottomLine = [[UILabel alloc]initWithFrame:CGRectMake(10, h, KWIDTH-20, 1)];
//    bottomLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    bottomLine.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    [self.scrollView addSubview:bottomLine];
    self.scrollView.contentSize = CGSizeMake(KWIDTH, h + 20);
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end

