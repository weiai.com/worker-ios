//
//  ZYTestSCJCBGViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestSCJCBGViewController.h"
#import "CSmakeAccAddmodel.h"
#import "ZYNewCheckReportTableViewCell.h"

static NSString *ZYNewCheckReportIdentifier = @"zyCheckReportCell";

@interface ZYTestSCJCBGViewController ()<UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, ZYNewCheckReportDelegate>
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UILabel *orderNumLab; //订单编号
@property (nonatomic, strong) UILabel *cpplConLab;  //产品品类
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UIButton *tuiusongBut;
@property (nonatomic, strong) UITextView *gzyyTv;
@property (nonatomic, strong) UILabel *placeLab;
@property (nonatomic, strong) UITextView *jjfaTv;
@property (nonatomic, strong) UILabel *placeLab1;
@property (nonatomic, strong) UILabel *stirngLenghLabel;
@property (nonatomic, strong) UIButton *addAccBtn;

@end

@implementation ZYTestSCJCBGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成检测报告";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [self jiancebaogaoUI];
    [self shwoBgviewsec];
    
}

- (void)jiancebaogaoUI {
    [self.view addSubview:self.scrollView];
    
    //最上部 背景View
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor whiteColor];
    topView.frame = CGRectMake(0, kNaviHeight, KWIDTH, 40);
    [self.scrollView addSubview:topView];
    //订单编号
    UILabel *orderNumLab = [[UILabel alloc] init];
    orderNumLab.frame = CGRectMake(16, 9, KWIDTH-32, 22);
    orderNumLab.text = [NSString stringWithFormat:@"%@%@", @"订单编号:  ", self.model.Id];
    orderNumLab.font = FontSize(16);
    orderNumLab.textColor = K666666;
    [topView addSubview:orderNumLab];
    //中部 背景
    UIView *centerView1 = [[UIView alloc] init];
    centerView1.backgroundColor = [UIColor whiteColor];
    centerView1.frame = CGRectMake(0, topView.bottom +12, KWIDTH, 48);
    [self.scrollView addSubview:centerView1];
    //产品品类 标题
    UILabel *cpplTitLab = [[UILabel alloc] init];
    cpplTitLab.frame = CGRectMake(16, 13, 70, 22);
    cpplTitLab.text = @"产品品类";
    cpplTitLab.textColor = K333333;
    cpplTitLab.font = FontSize(16);
    [centerView1 addSubview:cpplTitLab];
    //产品品类 内容
    UILabel *cpplConLab = [[UILabel alloc] init];
    cpplConLab.frame = CGRectMake(cpplTitLab.right +8, 13, KWIDTH-32-79-8, 22);
    cpplConLab.text = [NSString stringWithFormat:@"%@", _model.fault_name];
    cpplConLab.textColor = K333333;
    cpplConLab.font = FontSize(16);
    [centerView1 addSubview:cpplConLab];
    self.cpplConLab = cpplConLab;
    //中部 背景
    UIView *centerView2 = [[UIView alloc] init];
    centerView2.backgroundColor = [UIColor whiteColor];
    centerView2.frame = CGRectMake(0, centerView1.bottom +2, KWIDTH, 290);
    [self.scrollView addSubview:centerView2];
    //故障原因 标题
    UILabel *gzyyTitLab = [[UILabel alloc] init];
    gzyyTitLab.frame = CGRectMake(16, 13, 70, 22);
    gzyyTitLab.text = @"故障原因:";
    gzyyTitLab.textColor = K333333;
    gzyyTitLab.font = FontSize(16);
    [centerView2 addSubview:gzyyTitLab];
    //故障原因 内容
    UITextView *gzyyTv = [[UITextView alloc] init];
    gzyyTv.frame = CGRectMake(gzyyTitLab.right +6, 17, KWIDTH-32-70-6, 120);
    gzyyTv.layer.cornerRadius = 8;
    gzyyTv.layer.masksToBounds = YES;
    gzyyTv.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    gzyyTv.layer.borderWidth = 1;
    gzyyTv.font = FontSize(14);
    gzyyTv.delegate = self;
    [centerView2 addSubview:gzyyTv];
    self.gzyyTv = gzyyTv;
    //占位 Label
    UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
    placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    placeLab.text = @"请输入故障说明";
    placeLab.font = FontSize(14);
    self.placeLab = placeLab;
    [self.gzyyTv addSubview:placeLab];
    //分割线
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, gzyyTv.bottom +9, KWIDTH, 2)];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [centerView2 addSubview:line];
    //故障原因 标题
    UILabel *jjfaTitLab = [[UILabel alloc] init];
    jjfaTitLab.frame = CGRectMake(16, line.bottom +13, 70, 22);
    jjfaTitLab.text = @"解决方案:";
    jjfaTitLab.textColor = K333333;
    jjfaTitLab.font = FontSize(16);
    [centerView2 addSubview:jjfaTitLab];
    //故障原因 内容
    UITextView *jjfaTv = [[UITextView alloc] init];
    jjfaTv.frame = CGRectMake(jjfaTitLab.right +6, line.bottom +17, KWIDTH-33-70-6, 120);
    jjfaTv.layer.cornerRadius = 8;
    jjfaTv.layer.masksToBounds = YES;
    jjfaTv.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    jjfaTv.layer.borderWidth = 1;
    jjfaTv.font = FontSize(14);
    jjfaTv.delegate = self;
    [centerView2 addSubview:jjfaTv];
    self.jjfaTv = jjfaTv;
    //占位 Label
    UILabel *placeLab1 = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
    placeLab1.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    placeLab1.text = @"请输入相关的解决方案";
    placeLab1.font = FontSize(14);
    self.placeLab1 = placeLab1;
    [self.jjfaTv addSubview:placeLab1];
    //中部 背景
    UIView *centerView3 = [[UIView alloc] init];
    centerView3.backgroundColor = [UIColor whiteColor];
    centerView3.frame = CGRectMake(0, centerView2.bottom +2, KWIDTH, 48);
    [self.scrollView addSubview:centerView3];
    //故障配件
    UILabel *gzpjTitLab = [[UILabel alloc] init];
    gzpjTitLab.frame = CGRectMake(16, 13, 70, 22);
    gzpjTitLab.text = @"故障配件:";
    gzpjTitLab.textColor = K333333;
    gzpjTitLab.font = FontSize(16);
    [centerView3 addSubview:gzpjTitLab];
    //添加配件 按钮
    UIButton *addAccBtn = [[UIButton alloc] init];
    addAccBtn.frame = CGRectMake(gzpjTitLab.right +12, 0, KWIDTH-32-70-12, 48);
    [addAccBtn setImage:imgname(@"组 3181") forState:UIControlStateNormal];
    [addAccBtn addTarget:self action:@selector(addAccessBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [centerView3 addSubview:addAccBtn];
    self.addAccBtn = addAccBtn;
    
    _myTableView = [[UITableView alloc] init];
    _myTableView.frame = CGRectMake(0, centerView3.bottom +10, KWIDTH, KHEIGHT-kNaviHeight-KTabBarHeight-40-12-44-2-48-10);
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.estimatedRowHeight = 90;
    _myTableView.rowHeight = UITableViewAutomaticDimension;
    _myTableView.tableFooterView = [UIView new];
    [_myTableView registerClass:[ZYNewCheckReportTableViewCell class] forCellReuseIdentifier:@"ZYNewCheckReportTableViewCell"];
    _myTableView.backgroundColor = [UIColor clearColor];
    adjustInset(_myTableView);
    [_myTableView reloadData];
    [self.scrollView addSubview:self.myTableView];
    
    [self showtabFootView];
}

- (void)showtabFootView {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 109)];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"生成检测报告" forState:(UIControlStateNormal)];
    tuiusongBut.frame = CGRectMake(28, 41, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    self.tuiusongBut = tuiusongBut;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _myTableView.tableFooterView = self.tabFootView;
}

#pragma mark - 推送账单
-(void)tuisongAction:(UIButton *)but{
    //推送账单
    // 订单状态(0生成订单（已接单）//生成检测报告
    // 1已检测 //生成账单
    // 2生成账单（未支付）//去支付
    // 3已支付（已完成） //去评价
    // 4已评价
    // 5已取消) //先不管
    
    NSMutableArray *productsArray = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *imageArray = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *imageKeyArray = [NSMutableArray arrayWithCapacity:1];
    
    if ([self.gzyyTv.text isEqualToString:@""]) {
        ShowToastWithText(@"请输入故障原因");
        return;
    }
    if (_mydateSource.count == 0) {
        
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"rep_order_id"] = _model.Id;
        mudic[@"products"] = @"";
        mudic[@"fault_comment"] = self.gzyyTv.text;
        mudic[@"solution"] = self.jjfaTv.text;
        [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"testCost"];
        [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"cost"];
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
        param[@"data"] = [HFTools toJSONString:mudic];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [NetWorkTool POST:createTestOrderReport param:param success:^(id dic) {
            KMyLog(@"______检测单生成检测报告______%@",dic);
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];//展示新弹框
        } other:^(id dic) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        } fail:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        } needUser:YES];
        
    } else {
        
        if (_mydateSource.count == 0) {
            ShowToastWithText(@"请添加配件");
            return;
        }
        
        for (int i = 0; i < _mydateSource.count; i++) {
            @autoreleasepool {
                CSmakeAccAddmodel *model = _mydateSource[i];
                NSMutableDictionary *productDic = [NSMutableDictionary dictionaryWithCapacity:1];
                NSString *cellNum = model.count;
                NSString *cellName = model.name;
                NSString *product_model = @"1";//此处默认为1
                [productDic setObject:[NSString stringWithFormat:@"%@", cellNum] forKey:@"count"];
                [productDic setObject:[NSString stringWithFormat:@"%@", cellName] forKey:@"name"];
                [productDic setObject:[NSString stringWithFormat:@"%@", product_model] forKey:@"product_model"];
                NSLog(@"cell上的内容 %@ %@, %@, %@",cellNum, cellName, product_model, model.imageArray);
                if (model.name.length == 0) {//未填写名字
                    ShowToastWithText(@"请输入配件名称");
                    return;
                }
                if (model.imageArray.count <= 1) {//未选择图片
                    ShowToastWithText(@"请添加两张配件图片");
                    return;
                }
                if (model.imageArray.count >=2 && model.imageArray.count != 3) {//未选择图片
                    ShowToastWithText(@"请添加两张配件图片");
                    return;
                }
                for (int k = 0; k < model.imageArray.count - 1; k++) {//最后一张为占位图
                    UIImageView *imageView = model.imageArray[k];
                    [imageArray addObject:imageView.image];
                    [imageKeyArray addObject:[NSString stringWithFormat:@"%d-%d", i + 1, k]];
                }
                [productsArray addObject:productDic];
            }
        }
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"rep_order_id"] = _model.Id;
        mudic[@"products"] = productsArray;
        mudic[@"fault_comment"] = self.gzyyTv.text;
        mudic[@"solution"] = self.jjfaTv.text;
        [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"testCost"];
        [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"cost"];
        NSLog(@"转换前的内容 %@ 图片key %@ 图片 %@", productsArray, imageKeyArray, imageArray);
        NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
        param[@"data"] = [HFTools toJSONString:mudic];
        //kWeakSelf;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NetWorkTool UploadPicWithUrl:createTestOrderReport param:param keys:imageKeyArray images:imageArray withSuccess:^(id dic) {
            KMyLog(@"______检测单生成检测报告______%@",dic);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([dic[@"errorCode"] intValue] == 0) {
                [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];//展示新弹框
               
            } else {
                [NetWorkTool showOnleText:dic[@"msg"] delay:3];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }
}

/** 添加配件按钮点击事件
 @param button 添加配件
 */
- (void)addAccessBtnAction:(UIButton *)button {
    
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        if (model.imageArray.count != 3) {
            ShowToastWithText(@"请添加两张配件图片");
            return;
        }
    }
    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.isSaoyisao = @"1";
    model.count = @"1";
    model.typeProduct = @"1";//此处默认是1
    [self.mydateSource addObject:model];

    for (int i=0; i<self.mydateSource.count; i++) {
        if (i == self.mydateSource.count-2) {
            CSmakeAccAddmodel *model2 = self.mydateSource[i];
            model2.deleButHidden = @"1";
        }
    }
    [self.myTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 301;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    //通过唯一标识创建Cell实例
    ZYNewCheckReportTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYNewCheckReportTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    
    mycell.indexPath = indexPath;
    self.myTableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;
    mycell.delegate = self;
    
    mycell.mainVC = self;
    mycell.layer.cornerRadius = 10;
    mycell.layer.masksToBounds = YES;
    
    mycell.nameTF.text = model.name;
    mycell.rowLab.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    mycell.numLab.text = model.count;
    
    if (model.imageArray.count > 0) {//有图片才开始展示数据
        
    }
    mycell.makeAccAddmodel = model;
    [mycell loadContent];//加载数据
    
    kWeakSelf;
    mycell.imageArrayChangeBlock = ^(NSArray * _Nonnull imageArray) {
        model.imageArray = [imageArray mutableCopy];
    };
    __weak typeof(mycell)weakCell = mycell;
    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 0: {
                //输入名字
                model.name = str;
                NSLog(@"%@", str);
            }
                break;
            case 1: {
                //减号
                KMyLog(@"减一");
                //NSInteger ind;
                NSInteger ind = [weakCell.numLab.text integerValue];
                ind-=1;
                if (ind == 0) {
                    ind = 1;
                }
                weakCell.numLab.text = [NSString stringWithFormat:@"%ld",ind];
                model.count = [NSString stringWithFormat:@"%ld",ind];
            }
                break;
            case 2: {
                //加号
                KMyLog(@"加一");
                NSInteger ind = [weakCell.numLab.text integerValue];
                ind+=1;
                weakCell.numLab.text = [NSString stringWithFormat:@"%ld", (long)ind];
                model.count = [NSString stringWithFormat:@"%ld",(long)ind];
            }
                break;
            case 3: {
                //删除
                [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                [weakSelf.myTableView reloadData];
                
                CGFloat ffftotle = 0;
                for (CSmakeAccAddmodel *model in self.mydateSource) {
                    CGFloat motltlt = [model.total_price floatValue];
                    ffftotle = ffftotle +motltlt;
                }
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell暂时不能点击哟 %ld", (long)indexPath.row);
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    CGFloat ffftotle = 0;
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        CGFloat motltlt = [model.total_price floatValue];
        ffftotle = ffftotle +motltlt;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if (self.gzyyTv == textView) {
        self.placeLab.hidden = YES;
    } else if (self.jjfaTv == textView) {
        self.placeLab1.hidden = YES;
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if (self.gzyyTv == textView) {
        if (textView.text.length < 1) {
            self.placeLab.hidden = NO;
        }
    }
    
    if (self.jjfaTv == textView) {
        if (textView.text.length < 1) {
            self.placeLab1.hidden = NO;
        }
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    KMyLog(@"%@", textView.text);
    
    if (self.gzyyTv == textView) {
        self.placeLab.hidden = YES;
        //实时显示字数
        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
        //字数限制操作
        if (textView.text.length >= 60) {
            textView.text = [textView.text substringToIndex:60];
            self.stirngLenghLabel.text = @"60/60";
        }
        //取消按钮点击权限，并显示提示文字
        if (textView.text.length == 0) {
            self.placeLab.hidden = NO;
        }
    }
    
    if (self.jjfaTv == textView) {
        self.placeLab1.hidden = YES;
        //实时显示字数
        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
        //字数限制操作
        if (textView.text.length >= 60) {
            textView.text = [textView.text substringToIndex:60];
            self.stirngLenghLabel.text = @"60/60";
        }
        //取消按钮点击权限，并显示提示文字
        if (textView.text.length == 0) {
            self.placeLab1.hidden = NO;
        }
    }
}

/**
 弹出框的背景图 检测报告推送成功
 */
-(void)shwoBgviewsec {
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(331);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH-22*2, 96)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 6;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 3201"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 36, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"检测报告推送成功";
    
    UILabel *CenLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom+10, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:CenLable];
    CenLable.font = FontSize(16);
    CenLable.numberOfLines = 0;
    CenLable.textColor = [UIColor colorWithHexString:@"#333333"];
    CenLable.textAlignment = NSTextAlignmentCenter;
    CenLable.text = @"请在检测单详情中拨打";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, CenLable.bottom+10, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"厂商电话进行联系";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

- (void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    [singlTool shareSingTool].needReasfh = YES;
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT +245);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

@end
