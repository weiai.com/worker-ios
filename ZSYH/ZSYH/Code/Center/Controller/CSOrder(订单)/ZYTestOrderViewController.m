//
//  ZYTestOrderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestOrderViewController.h"
#import "SGPagingView.h"
#import "CSOrderContentViewController.h" //测试跳转用
#import "CSMaintenanceViewController.h"  //测试跳转用

#import "ZYTestOrderContentViewController.h"

@interface ZYTestOrderViewController ()<SGPageTitleViewDelegate, SGPageContentScrollViewDelegate>
@property (nonatomic, strong) SGPageTitleView *pageTitleView;
@property (nonatomic, strong) SGPageContentScrollView *pageContentScrollView;
@property (nonatomic, strong) NSMutableArray *titArr;
@end

@implementation ZYTestOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //顶部标题
    self.navigationItem.title = @"检测单";
    
    if ([self.from isEqualToString:@"tabbar"]) {
        
    } else {
        [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    }
    
    [self.rightbutton setImage:imgname(@"组 1474") forState:(UIControlStateNormal)];
    [self.rightbutton addTarget:self action:@selector(rightClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.titArr = [NSMutableArray arrayWithCapacity:1];
    
    [self showNav];
}

- (void)rightClick{
    CSMaintenanceViewController *VC = [CSMaintenanceViewController new];
    [self.navigationController pushViewController:VC animated:YES];
}
-(void)showNav{
    
    SGPageTitleViewConfigure *configure = [SGPageTitleViewConfigure pageTitleViewConfigure];
    configure.indicatorAdditionalWidth = 10; // 说明：指示器额外增加的宽度，不设置，指示器宽度为标题文字宽度；若设置无限大，则指示器宽度为按钮宽度
    configure.titleGradientEffect = YES;
    
    configure.titleSelectedColor= [UIColor colorWithHexString:@"#70BE68"];
    configure.titleColor= [UIColor colorWithHexString:@"#999999"];
    configure.showIndicator = YES;
    configure.indicatorColor = zhutiColor;
    configure.bottomSeparatorColor= [UIColor whiteColor];
    
    NSMutableArray *titleArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *childVCS = [NSMutableArray arrayWithCapacity:1];
    [titleArr addObject:@"全部"];
    [titleArr addObject:@"已接单"];
    [titleArr addObject:@"已检测"];

    [titleArr addObject:@"已完成"];
    
    for (int i = 0; i<titleArr.count; i++) {
        ZYTestOrderContentViewController *oneVC = [[ZYTestOrderContentViewController alloc] init];
        oneVC.from = self.from;
        oneVC.type = [NSString stringWithFormat:@"%d",i];
        [childVCS addObject:oneVC];
    }
    
    self.pageTitleView = [SGPageTitleView pageTitleViewWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, 44) delegate:self titleNames:titleArr configure:configure];
   
    self.pageContentScrollView = [[SGPageContentScrollView alloc] initWithFrame:CGRectMake(0, kNaviHeight+44, KWIDTH, KHEIGHT-kNaviHeight-44) parentVC:self childVCs:childVCS];
    
    _pageContentScrollView.delegatePageContentScrollView = self;
    [self.view addSubview:self.pageTitleView ];
    
    [self.view addSubview:_pageContentScrollView];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [super textFieldShouldReturn:textField];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [super textFieldShouldReturn:textField];
    //    SearchResultController *vc = [SearchResultController new];
    //    vc.keyword = textField.text;
    //    vc.Ctype = @"0";
    //    [self.navigationController pushViewController:vc animated:YES];
    return YES;
}

- (void)pageTitleView:(SGPageTitleView *)pageTitleView selectedIndex:(NSInteger)selectedIndex {
    [self.pageContentScrollView setPageContentScrollViewCurrentIndex:selectedIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView progress:(CGFloat)progress originalIndex:(NSInteger)originalIndex targetIndex:(NSInteger)targetIndex {
    [self.pageTitleView setPageTitleViewWithProgress:progress originalIndex:originalIndex targetIndex:targetIndex];
}

- (void)pageContentScrollView:(SGPageContentScrollView *)pageContentScrollView index:(NSInteger)index {
    if (index == 1 || index == 5) {
        [_pageTitleView removeBadgeForIndex:index];
    }
}

@end
