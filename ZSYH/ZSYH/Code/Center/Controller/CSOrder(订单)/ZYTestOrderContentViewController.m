//
//  ZYTestOrderContentViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYTestOrderContentViewController.h"
#import "ZYTestOrderContentTableViewCell.h"
#import "ZYTestOrderDetailViewController.h" //检测单详情
#import "ZYTestSCJCBGViewController.h"      //生成检测报告
#import "FillDateViewController.h"          //填写预约信息

@interface ZYTestOrderContentViewController ()<UITableViewDelegate, UITableViewDataSource> {
    BOOL _isDelete;
}
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;

@end

@implementation ZYTestOrderContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //检测单
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:WODEDINGDAN_REQUEST_JIANCEDAN_LISTDATA object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestData) name:@"applicationWillEnterForeground" object:nil];
    
    [self.view addSubview:self.myTableView];
    [self requestData];

}

#pragma mark ***页面刷新
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].isLjiBuy = 2;
    if ([singlTool shareSingTool].needReasfh) {
        [self.myTableView.mj_header beginRefreshing];
        [singlTool shareSingTool].needReasfh = NO;
    }
}

-(void)requestData {
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSInteger ind = [_type integerValue];
    
    ind-=1;
    NSString *stt  = [NSString stringWithFormat:@"%ld",ind];
    if (ind == -1) {
        stt = @"";
    } else if (ind == 0) {
        stt = @"1";
    } else if (ind == 1) {
        stt = @"2";
    } else if (ind == 2) {
        stt = @"3";
    }
    
    // 订单状态
    //1.已接单
    //2.已检测
    //3.已支付
    //4.已取消
    param[@"state"] = stt;
    [weakSelf.mydateSource removeAllObjects];
    [NetWorkTool POST:getTestOrders param:param success:^(id dic) {
        NSLog(@"打印一下 检测单 列表所有数据 %@", dic);
        
        self.mydateSource = [CSmaintenanceListModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } other:^(id dic) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } fail:^(NSError *error) {
        [weakSelf.mydateSource removeAllObjects];
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        
    } needUser:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 179;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",(long)indexPath.section, (long)indexPath.row];
    //通过唯一标识创建Cell实例
    ZYTestOrderContentTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYTestOrderContentTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态
    mycell.backgroundColor = [UIColor clearColor];
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    [mycell refasf:model];
    
    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 0: {
                //生成检测报告按钮点击事件
                ZYTestSCJCBGViewController *vc = [[ZYTestSCJCBGViewController alloc] init];
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case 1: {
                FillDateViewController *VC = [[FillDateViewController alloc]init];
                VC.mymodel = model;
                VC.myblock = ^(NSString * _Nonnull str) {
                    [self requestData];
                };
                [self.navigationController pushViewController:VC animated:YES];
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CSmaintenanceListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];

    ZYTestOrderDetailViewController *vc = [[ZYTestOrderDetailViewController alloc] init];
    NSString *strPush = @"2";
    vc.strPush = strPush;
    vc.mymodel = model;
    vc.idStr = model.Id;
    [self.navigationController pushViewController:vc animated:YES];
    //KMyLog(@"您点击了第 %ld 个分区第 %ld行", indexPath.section, indexPath.row);
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        
        if ([self.from isEqualToString:@"tabbar"]) {
            _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49-kTabbarHeight) style:(UITableViewStyleGrouped)];
        }else {
            _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kNaviHeight-49-kTabbarHeight) style:(UITableViewStyleGrouped)];
        }
        
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _myTableView.tableFooterView = [UIView new];
        adjustInset(_myTableView);
        
        [_myTableView registerClass:[ZYTestOrderContentTableViewCell class] forCellReuseIdentifier:@"ZYTestOrderContentTableViewCell"];
        
        kWeakSelf;
        weakSelf.myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [weakSelf.myTableView.mj_header beginRefreshing];
        }];
        
        self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self requestData];//请求数据
            
        }];
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
