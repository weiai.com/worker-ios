
//
//  NewSelenameViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "NewSelenameViewController.h"
#import "CSServiceFWModel.h"
#import "NewSelenameCollectionViewCell.h"

@interface NewSelenameViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,strong) CSServiceFWModel *seleModel;

@end

@implementation NewSelenameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"配件品类";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self.rightbutton setTitle:@"完成" forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    self.rightbutton.frame = CGRectMake(0, 0, 60, 30);
  
    self.modelArr = [NSMutableArray arrayWithCapacity:1];
    [self.view addSubview:self.collectionView];
    [self.collectionView reloadData];
    [self requestGoods];
    
}

- (void)rightClick{
    
    if (self.myblock) {
        self.myblock(_seleModel.type_name);
    }
    
    if (self.selblock) {
        self.selblock(_seleModel.type_name, _seleModel.Id);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestGoods {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"typeId"] = NOTNIL(_type);
    [self.modelArr removeAllObjects];
    
    [NetWorkTool POST:erJIProductList param:param success:^(id dic) {
        [self.modelArr removeAllObjects];
        
        self.modelArr = [CSServiceFWModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        if (self.modelArr.count>0) {
            CSServiceFWModel *selemodel  =self.modelArr[0];
            self.seleModel = selemodel;
            selemodel.issele = YES;
        }
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } other:^(id dic) {
        [self->_collectionView.mj_footer endRefreshingWithNoMoreData];
        [self->_collectionView.mj_header endRefreshing];
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } fail:^(NSError *error) {
        [self->_collectionView.mj_footer endRefreshing];
        
        [self->_collectionView.mj_header endRefreshing];
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:NO];
}

#pragma mark - CollectionView -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NewSelenameCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewSelenameCollectionViewCell" forIndexPath:indexPath];
    CSServiceFWModel *selemodel  =[self.modelArr safeObjectAtIndex:indexPath.row];
    cell.myLb.text = selemodel.type_name;
    if (selemodel.issele) {
        cell.myLb.backgroundColor = [UIColor colorWithHexString:@"#D4EBD1"];
    } else {
        cell.myLb.backgroundColor = [UIColor colorWithHexString:@"##F2F2F2"];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    CSServiceFWModel *selemodel  =[self.modelArr safeObjectAtIndex:indexPath.row];
    self.seleModel = selemodel;
    for (int i = 0; i < self.modelArr.count; i++) {
        CSServiceFWModel *selemodel  =self.modelArr[i];
        selemodel.issele = i == indexPath.row?YES:NO;
    }
    [_collectionView reloadData];
   
}

-(UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake((KWIDTH-76)/3 , 36);
        //        layout.headerReferenceSize = CGSizeMake(KWIDTH, 204);
        layout.sectionInset = UIEdgeInsetsMake(16, 16, 16, 22);
        layout.minimumLineSpacing = 16;
        layout.minimumInteritemSpacing = 16;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) collectionViewLayout:layout];
        adjustInset(_collectionView);
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        //        _collectionView.backgroundColor =RGBA(237, 237, 237, 1);
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"NewSelenameCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"NewSelenameCollectionViewCell"];
        [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"head"];
        [_collectionView reloadData];
        //        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //            [self requestGoods];
        //        }];
        //
        //        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        //            [self requestGoods];
        //        }];
    }
    return _collectionView;
}


@end
