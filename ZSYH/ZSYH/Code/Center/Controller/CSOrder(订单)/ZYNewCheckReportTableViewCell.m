//
//  ZYNewCheckReportTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYNewCheckReportTableViewCell.h"

@interface ZYNewCheckReportTableViewCell ()<UITextFieldDelegate>
@end

@implementation ZYNewCheckReportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameTF.delegate = self;
    
    // Initialization code
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == self.nameTF) {
        
        if (self.myblock) {
            self.myblock(0,textField.text);//上边的输入名称
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.nameTF) {
        if (self.myblock) {
            self.myblock(0,textField.text);//上边的输入名称
        }
    }
    return YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self=[super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        UIView *bottomV = [[UIView alloc] init];
        bottomV.frame = CGRectMake(0, 0, KWIDTH - 20, 281);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        [self.contentView addSubview:bottomV];
        
        self.imageArr  = [NSMutableArray arrayWithCapacity:1];
        self.myimage = [[UIImageView alloc]init];
        _myimage.image = imgname(@"addpicimage");
        [self.imageArr addObject:_myimage];

        UIButton *closeBtn = [[UIButton alloc] init];
        closeBtn.frame = CGRectMake(0, 0, 34, 38);
        [closeBtn setImage:imgname(@"cellCha") forState:(UIControlStateNormal)];
        [closeBtn addTarget:self action:@selector(leftClostBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:closeBtn];
        self.closeBtn = closeBtn;
        //设置圆角
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:closeBtn.bounds byRoundingCorners:UIRectCornerTopLeft cornerRadii:CGSizeMake(8, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = closeBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        closeBtn.layer.mask = maskLayer;

        UILabel *rowLab = [[UILabel alloc] init];
        rowLab.frame = CGRectMake(closeBtn.right + 98, 19, 18, 18);
        rowLab.textAlignment = NSTextAlignmentCenter;
        rowLab.text = @"1";
        rowLab.font = FontSize(16);
        [bottomV addSubview:rowLab];
        rowLab.layer.borderColor = [UIColor colorWithHexString:@"#333333"].CGColor;
        rowLab.layer.borderWidth = 1;
        rowLab.layer.masksToBounds = YES;
        rowLab.layer.cornerRadius = 9;
        self.rowLab = rowLab;

        UILabel *titleLab = [[UILabel alloc] init];
        titleLab.frame = CGRectMake(rowLab.right + 8, 19, 70, 18);
        titleLab.text = @"配件详情";
        titleLab.font = FontSize(16);
        [bottomV addSubview:titleLab];
        self.titleLab = titleLab;

        UILabel *nameTLab = [[UILabel alloc] init];
        nameTLab.frame = CGRectMake(10, _titleLab.bottom + 26, 70, 16);
        nameTLab.text = @"配件名称:";
        nameTLab.font = FontSize(16);
        [bottomV addSubview:nameTLab];
        self.nameTLab = nameTLab;

        UITextField *nameTF = [[UITextField alloc] init];
        nameTF.frame = CGRectMake(nameTLab.right + 10, _titleLab.bottom + 19, KWIDTH-20-10- nameTLab.bounds.size.width-10-10, 30);
        nameTF.placeholder = @"请输入配件名称";
        nameTF.font = FontSize(16);
        nameTF.delegate = self;
        [bottomV addSubview:nameTF];
        self.nameTF = nameTF;

        UILabel *line = [[UILabel alloc] init];
        line.frame = CGRectMake(0, nameTF.bottom + 10, KWIDTH - 20, 1);
        line.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line];

        UILabel *numberTLab = [[UILabel alloc] init];
        numberTLab.frame = CGRectMake(10, line.bottom + 16, 70, 16);
        numberTLab.text = @"配件数量:";
        numberTLab.font = FontSize(16);
        [bottomV addSubview:numberTLab];
        self.numberTLab = numberTLab;
        
        UIButton *lessBtn = [[UIButton alloc] init];
        lessBtn.frame = CGRectMake(numberTLab.right +10, line.bottom + 11.5, 36, 25);
        [lessBtn setImage:imgname(@"jian_select") forState:(UIControlStateNormal)];
        [lessBtn addTarget:self action:@selector(lessBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        lessBtn.layer.borderWidth = 1;
        lessBtn.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        //这里设置的是左上和左下角
        UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:lessBtn.bounds   byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:CGSizeMake(4, 0)];
        CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
        maskLayer1.frame = lessBtn.bounds;
        maskLayer1.path = maskPath1.CGPath;
        lessBtn.layer.mask = maskLayer1;
        [bottomV addSubview:lessBtn];
        
        UILabel *numLab = [[UILabel alloc] init];
        numLab.frame = CGRectMake(lessBtn.right, line.bottom + 11.5, 40, 25);
        numLab.textAlignment = NSTextAlignmentCenter;
        numLab.text = @"1";
        numLab.font = FontSize(16);
        numLab.layer.borderWidth = 1;
        numLab.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        [bottomV addSubview:numLab];
        self.numLab = numLab;
        
        UIButton *addBtn = [[UIButton alloc] init];
        addBtn.frame = CGRectMake(numLab.right, line.bottom + 11.5, 35, 25);
        [addBtn setImage:imgname(@"jia_select") forState:(UIControlStateNormal)];
        [addBtn addTarget:self action:@selector(addBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        addBtn.layer.borderWidth = 1;
        addBtn.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        //这里设置的是右上和右下角
        UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:addBtn.bounds   byRoundingCorners:UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii:CGSizeMake(4, 0)];
        CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
        maskLayer2.frame = addBtn.bounds;
        maskLayer2.path = maskPath2.CGPath;
        addBtn.layer.mask = maskLayer2;
        [bottomV addSubview:addBtn];

        UILabel *line1 = [[UILabel alloc] init];
        line1.frame = CGRectMake(0, numberTLab.bottom + 16, KWIDTH - 20, 1);
        line1.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
        [bottomV addSubview:line1];

        UILabel *imageTLab = [[UILabel alloc] init];
        imageTLab.frame = CGRectMake(10, line1.bottom + 16, 70, 16);
        imageTLab.text = @"配件图片:";
        imageTLab.font = FontSize(16);
        [bottomV addSubview:imageTLab];
        self.imageTLab = imageTLab;

        self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, imageTLab.bottom, KWIDTH - 20, 100)];
        [bottomV addSubview: self.addView];
        self.bottomf = imageTLab.bottom;
        [self showinageWirhArr:_imageArr];

        CGRect ffff = self.addView.frame;
        ffff.size.height = self.addView.height;
        self.addView.frame = ffff;
    }
    return self;
}

//加载数据
- (void)loadContent {
    NSLog(@"row %ld 图片个数:%ld model图片个数:%ld", self.indexPath.row, _imageArr.count, self.makeAccAddmodel.imageArray.count);
    if (self.makeAccAddmodel.imageArray.count > 0) {
        _imageArr = self.makeAccAddmodel.imageArray;
    } else {
        UIImageView *addImageView = [[UIImageView alloc] initWithImage:imgname(@"addpicimage")];
        [self.makeAccAddmodel.imageArray addObject:addImageView];
        _imageArr = self.makeAccAddmodel.imageArray;
    }
    [self showinageWirhArr:_imageArr];
    
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind] removeFromSuperview];
    NSLog(@"删除%ld图片", ind);
    [_imageArr removeObjectAtIndex:ind];
    [self showinageWirhArr:_imageArr];
    if (self.imageArrayChangeBlock) {
        self.imageArrayChangeBlock(_imageArr);
    }
}

-(void)addimageAction {
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
//        ZYNewCheckReportViewController *mainVC = [[ZYNewCheckReportViewController alloc] initWithNibName:@"VC" bundle:nil];
        [self.mainVC presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
//            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            //[self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        //页面跳转
//        ZYNewCheckReportViewController *mainVC = [[ZYNewCheckReportViewController alloc] initWithNibName:@"VC" bundle:nil];
        [self.mainVC presentViewController:PickerImage animated:YES completion:nil];
        
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self.mainVC presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    NSLog(@"添加了一个图片");
    if (self.imageArrayChangeBlock) {
        self.imageArrayChangeBlock(_imageArr);
    }
    //页面跳转
    [self.mainVC dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    //self.bottomf = 0;
    //CGFloat www = (KWIDTH - 62)/4;
    CGFloat www = (KWIDTH - 20 - 10 * 2 - 10*2)/4;
    for (int i = 0; i < _imageArr.count; i++) {
        UIImageView *image = _imageArr[i];
        //[image removeFromSuperview];
        [image removeAllSubviews];
        //y:self.bottomf+10
        image.frame = CGRectMake(10+(www+10)*i, 10, www, www);
        [self.addView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        if ([self.makeAccAddmodel.deleButHidden isEqualToString:@"1"]) {
            deleBut.hidden = YES;
        }else
        {
            deleBut.hidden = NO;
        }
        if (i == (_imageArr.count - 1) ) {
            if (i == 2) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            }else{
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        }else{
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

//左上角 删除按钮点击事件
- (void)leftClostBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(3, @"");//删除
    }
}

//配件数量 减号按钮点击事件
- (void)lessBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(1,@"");//减号
    }
}

//配件数量 加好按钮点击事件
- (void)addBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(2,@"");//加号
    }
    
}

@end
