//
//  ZYNewCheckReportViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYNewCheckReportViewController.h"
#import "CSmakeAccAddmodel.h"
#import "ZYNewCheckReportTableViewCell.h"
#import "ZYAddHoubaoAccessoriesViewController.h"

@interface ZYNewCheckReportViewController () <UITableViewDelegate, UITableViewDataSource, ZYNewCheckReportDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UILabel *orderNumLab;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLab;
@property (weak, nonatomic) IBOutlet UIButton *addProductBtn;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) UIView *bgViewsec;
@property (nonatomic, strong) UIButton *tuiusongBut;

@end

static NSString *ZYNewCheckReportIdentifier = @"zyCheckReportCell";

@implementation ZYNewCheckReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成检测报告";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    //[self.rightbutton setImage:imgname(@"Csaoyosao") forState:(UIControlStateNormal)];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.estimatedRowHeight = 90;
    _myTableView.rowHeight = UITableViewAutomaticDimension;
    _myTableView.tableFooterView = [UIView new];
    [_myTableView registerClass:[ZYNewCheckReportTableViewCell class] forCellReuseIdentifier:@"ZYNewCheckReportTableViewCell"];
    _myTableView.backgroundColor = [UIColor clearColor];
    adjustInset(_myTableView);
    [_myTableView reloadData];
    
    self.orderNumLab.text = _mymodel.Id;
    self.productTypeLab.text = _mymodel.fault_name;
    
    [self showtabFootView];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view from its nib.
}

-(void)showtabFootView{
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"下一步" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 48, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    self.tuiusongBut = tuiusongBut;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _myTableView.tableFooterView = self.tabFootView;
}

#pragma mark - 推送账单
-(void)tuisongAction:(UIButton *)but{
    //推送账单
    // 订单状态(0生成订单（已接单）//生成检测报告
    // 1已检测，//生成账单
    // 2生成账单（未支付），//去支付
    // 3已支付（已完成），//去评价
    // 4已评价，
    // 5已取消)',//先不管
    
    NSMutableArray *productsArray = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *imageArray = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *imageKeyArray = [NSMutableArray arrayWithCapacity:1];
    if (_mydateSource.count == 0) {
        ShowToastWithText(@"请添加配件");
        return;
    }
    for (int i = 0; i < _mydateSource.count; i++) {
        @autoreleasepool {
            CSmakeAccAddmodel *model = _mydateSource[i];
            NSMutableDictionary *productDic = [NSMutableDictionary dictionaryWithCapacity:1];
            NSString *cellNum = model.count;
            NSString *cellName = model.name;
            NSString *product_model = @"1";//此处默认为1
            [productDic setObject:[NSString stringWithFormat:@"%@", cellNum] forKey:@"count"];
            [productDic setObject:[NSString stringWithFormat:@"%@", cellName] forKey:@"name"];
            [productDic setObject:[NSString stringWithFormat:@"%@", product_model] forKey:@"product_model"];
            
            NSLog(@"cell上的内容 %@ %@, %@, %@",cellNum, cellName, product_model, model.imageArray);
            
            if (model.name.length == 0) {//未填写名字
                ShowToastWithText(@"请输入配件名称");
                return;
            }
            if (model.imageArray.count <= 1) {//未选择图片
                ShowToastWithText(@"请添加两张配件图片");
                return;
            }
            if (model.imageArray.count != 3) {//未选择图片
                NSString *tips = [NSString stringWithFormat:@"请添加配件图片"];
                ShowToastWithText(tips);
                return;
            }
            for (int k = 0; k < model.imageArray.count - 1; k++) {//最后一张为占位图
                UIImageView *imageView = model.imageArray[k];
                [imageArray addObject:imageView.image];
                [imageKeyArray addObject:[NSString stringWithFormat:@"%d-%d", i + 1, k]];
            }
            [productsArray addObject:productDic];
        }
    }
    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    mudic[@"rep_order_id"] = _mymodel.orderId;
    mudic[@"products"] = productsArray;
    [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"testCost"];
    [mudic setObject:[NSString stringWithFormat:@"0"] forKey:@"cost"];
    NSLog(@"  转换前的内容 %@ 图片key %@ 图片 %@", productsArray, imageKeyArray, imageArray);
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = [HFTools toJSONString:mudic];
    NSLog(@"生成检测报告的参数:%@", param);
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool UploadPicWithUrl:sehncgehngList param:param keys:imageKeyArray images:imageArray withSuccess:^(id dic) {
        KMyLog(@"____________%@",dic);
        [MBProgressHUD  hideHUDForView:self.view animated:YES];
        if ([dic[@"errorCode"] intValue]==0) {
            [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsec];//展示新弹框,舍弃旧弹框
            if (weakSelf.myblock) {
                weakSelf.myblock(@"");
            }
        }else {
            [NetWorkTool showOnleText:dic[@"msg"] delay:3];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD  hideHUDForView:self.view animated:YES];
    }];
}

/**
 更换配件 按钮点击事件
 @param sender 更换配件
 */
- (IBAction)replaceBtnAction:(id)sender {
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
    
    mymodel.row = @"";               mymodel.type_name = @"";
    mymodel.type_id = @"";           mymodel.name = @"";
    mymodel.price = @"";             mymodel.count = @"1";
    mymodel.total_price = @"";       mymodel.qrCode = @"";
    mymodel.isSaoyisao = @"2";       mymodel.typeProduct = @"2";
    mymodel.old_type_name = @"";     mymodel.old_type_id = @"";
    mymodel.old_name = @"";          mymodel.old_price = @"1";
    mymodel.old_count = @"1";        mymodel.old_total_price = @"";
    mymodel.old_isSaoyisao = @"2";   mymodel.ne_type_name = @"";
    mymodel.ne_type_id = @"";        mymodel.ne_name = @"";
    mymodel.ne_price = @"2";         mymodel.ne_count = @"";
    mymodel.ne_total_price = @"";    mymodel.ne_qrCode = @"";
    mymodel.ne_isSaoyisao = @"2";    model.typeProduct = @"2";
    model.ne_count = @"1";           model.old_count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.myTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

//新增配件按钮点击事件
/**
 @param sender 添加配件
 */
- (IBAction)newAddBtnClick:(id)sender {
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    model.isSaoyisao = @"1";
    model.count = @"1";
    model.typeProduct = @"1";//此处默认是1
    [self.mydateSource addObject:model];
    
    [self.myTableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.myTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 301;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];

    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    //通过唯一标识创建Cell实例
    ZYNewCheckReportTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYNewCheckReportTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    mycell.indexPath = indexPath;
    self.myTableView.separatorStyle = UITableViewCellEditingStyleNone;//不显示分割线
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    mycell.mainVC = self;
    mycell.layer.cornerRadius = 10;
    mycell.layer.masksToBounds = YES;
    
    mycell.delegate = self;
    
    mycell.nameTF.text = model.name;
    mycell.rowLab.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    mycell.numLab.text = model.count;

    if (model.imageArray.count > 0) {//有图片才开始展示数据
    }
    mycell.makeAccAddmodel = model;
    [mycell loadContent];//加载数据
    
    kWeakSelf;
    mycell.imageArrayChangeBlock = ^(NSArray * _Nonnull imageArray) {
        model.imageArray = [imageArray mutableCopy];
    };
    mycell.myblock = ^(NSUInteger ind, NSString * _Nonnull str) {
        switch (ind) {
            case 0: {
                //输入名字
                model.name = str;
                NSLog(@"%@", str);
            }
                break;
            case 1: {
                //减号
                KMyLog(@"减一");
                //NSInteger ind;
                NSInteger ind = [mycell.numLab.text integerValue];
                ind-=1;
                if (ind == 0) {
                    ind = 1;
                }
                mycell.numLab.text = [NSString stringWithFormat:@"%ld",ind];
                model.count = [NSString stringWithFormat:@"%ld",ind];
            }
                break;
            case 2: {
                //加号
                KMyLog(@"加一");
                NSInteger ind = [mycell.numLab.text integerValue];
                ind+=1;
                mycell.numLab.text = [NSString stringWithFormat:@"%ld", ind];
                model.count = [NSString stringWithFormat:@"%ld",ind];
            }
                break;
            case 3: {
                //删除
                [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                [weakSelf.myTableView reloadData];
                
                CGFloat ffftotle = 0;
                for (CSmakeAccAddmodel *model in self.mydateSource) {
                    CGFloat motltlt = [model.total_price floatValue];
                    ffftotle = ffftotle +motltlt;
                }
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cell暂时不能点击哟 %ld", (long)indexPath.row);
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    CGFloat ffftotle = 0;
    for (CSmakeAccAddmodel *model in self.mydateSource) {
        CGFloat motltlt = [model.total_price floatValue];
        ffftotle = ffftotle +motltlt;
    }
    return YES;
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        //make.height.offset(240);
        make.height.offset(331);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH-22*2, 96)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 6;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 3201"];

    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 36, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"检测报告推送成功";
    
    UILabel *CenLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom+10, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:CenLable];
    CenLable.font = FontSize(16);
    CenLable.numberOfLines = 0;
    CenLable.textColor = [UIColor colorWithHexString:@"#333333"];
    CenLable.textAlignment = NSTextAlignmentCenter;
    CenLable.text = @"请告知客户配件到货时";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, CenLable.bottom+10, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"及时通知维修师傅上门";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    //    if (self.mybleoc) {
    //        self.mybleoc(@"");
    //    }
    //[self.navigationController popViewControllerAnimated:YES];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}



@end
