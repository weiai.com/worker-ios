//
//  CSmyGrandTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/11/11.
//  Copyright © 2019 张正阳. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSmyGrandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderLb;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;

@property (weak, nonatomic) IBOutlet UILabel *moneyLb;
@property (weak, nonatomic) IBOutlet UIImageView *jituruImage;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;

@end

NS_ASSUME_NONNULL_END
