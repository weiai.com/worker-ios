//
//  CSmyImcomeLeftViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmyImcomeLeftViewController.h"
#import "CSmyincomeTableViewCell.h"
#import "CSMyincomModel.h"
#import "PGDatePickManager.h"
#import "JSHostoryTableViewCell.h"

@interface CSmyImcomeLeftViewController ()<UITableViewDelegate,UITableViewDataSource,PGDatePickerDelegate>
@property (nonatomic,strong) NSMutableArray *mydateSource;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UILabel *upTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *dowTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *bgfLB;
@property (weak, nonatomic) IBOutlet UILabel *totleincomLB;
@property (weak, nonatomic) IBOutlet UIButton *seleButtn;
@property (nonatomic, strong) UIView *bgview;
@property (nonatomic, strong) UIButton *lefttimeLB;
@property (nonatomic, strong) UIButton *righttimeLB;
@property (nonatomic, strong) UILabel *leftLB;
@property (nonatomic, strong) UILabel *rightLB;
@property (nonatomic, assign) BOOL isleft;

@end

@implementation CSmyImcomeLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isleft = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.bgfLB.layer.cornerRadius = 18;
    self.bgfLB.layer.masksToBounds = YES;
    self.bgfLB.layer.borderWidth = 1;
    self.bgfLB.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;
    
    _myTableView.backgroundColor = [UIColor clearColor];
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.estimatedRowHeight = 90;
    _myTableView.rowHeight = UITableViewAutomaticDimension;
    _myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSmyincomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSmyincomeTableViewCell"];
    [_myTableView registerNib:[UINib nibWithNibName:@"JSHostoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"JSHostoryTableViewCell"];
    [_myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; //取消cell的下划线
//    _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:self.view.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
//        [self->_myTableView.mj_header beginRefreshing];
//    }];
    _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(0, 0, _myTableView.width ,_myTableView.height) image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self->_myTableView.mj_header beginRefreshing];
    }];
    KMyLog(@"dsfd3%@",_titleStr);
    
    adjustInset(_myTableView);
    if (strIsEmpty(_titleStr)) {
        _titleStr = [singlTool shareSingTool].titlsr;
    }
    [self showbgview];
    [self request];
    
    KMyLog(@"dsfd2%@",_titleStr);
    // Do any additional setup after loading the view from its nib.
}

//- (void)setTitleStr:(NSString *)titleStr{
//    [self request];
//}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"begTime"] = _upTimeLb.text;
    param[@"endTime"] = _dowTimeLb.text;
    [_mydateSource removeAllObjects];
    [self.myTableView reloadData];
    self.totleincomLB.text = [NSString stringWithFormat:@"合计收益:"];
    self.totleincomLB.font = KFontPingFangSCMedium(12);
    kWeakSelf;
    NSString *str = lookMyInco;
    KMyLog(@"dsfd1%@",_titleStr);
    
    if ([self.titleStr isEqualToString:@"结算历史"]) {
        str = jieSuanHos;
    }
    if ([self.titleStr isEqualToString:@"结算历史"]) {
        self.totleincomLB.text =[NSString stringWithFormat:@"合计结算:￥0"];
        self.totleincomLB.font = KFontPingFangSCMedium(12);

    }else{
        self.totleincomLB.text =[NSString stringWithFormat:@"合计收益:￥0"];
        self.totleincomLB.font = KFontPingFangSCMedium(12);

    }
    
    [NetWorkTool POST:str param:param success:^(id dic) {
        CGFloat ffffff = 0;
        if ([self.titleStr isEqualToString:@"结算历史"]) {
            for (NSMutableDictionary *mudci in [dic objectForKey:@"data"] ) {
                CSMyincomModel *model = [[CSMyincomModel alloc]init];
                [model setValuesForKeysWithDictionary:mudci];
                ffffff = ffffff + model.app_set_count;
                [weakSelf.mydateSource addObject:model];
                
            }
            self.totleincomLB.text =[NSString stringWithFormat:@"合计结算:￥%.2f",ffffff];
            self.totleincomLB.font = KFontPingFangSCMedium(12);

        }else{
            self.totleincomLB.text =[NSString stringWithFormat:@"合计收益:￥%@",[[dic objectForKey:@"data"] objectForKey:@"allMoney"]];
            self.totleincomLB.font = KFontPingFangSCMedium(12);
            
            for (NSMutableDictionary *mudci in [[dic objectForKey:@"data"] objectForKey:@"orderList"]) {
                CSMyincomModel *model = [[CSMyincomModel alloc]init];
                [model setValuesForKeysWithDictionary:mudci];
                [weakSelf.mydateSource addObject:model];
            }
        }
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.titleStr isEqualToString:@"结算历史"]) {
        return 80;
    }
    return 76;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSMyincomModel *mode = [_mydateSource safeObjectAtIndex:indexPath.row];
    if ([self.titleStr isEqualToString:@"结算历史"]) {
        JSHostoryTableViewCell *cellJS = [tableView dequeueReusableCellWithIdentifier:@"JSHostoryTableViewCell" forIndexPath:indexPath];
        
        cellJS.selectionStyle = UITableViewCellSelectionStyleNone; //取消cell点击状态
        
        if ([mode.type integerValue] == 2) {
            cellJS.titleLB.text = [NSString stringWithFormat:@"结算-支付宝"];
            cellJS.shenqingLB.text =  [NSString stringWithFormat:@"%@",[mode.create_time substringToIndex:16]];
        }else{
            cellJS.titleLB.text = [NSString stringWithFormat:@"结算-微信"];
            cellJS.shenqingLB.text =  [NSString stringWithFormat:@"%@",[mode.create_time substringToIndex:16]];
        }
        if (strIsEmpty(mode.pay_time)) {
            cellJS.stateLB.text = @"审核中";
            cellJS.stateLB.textColor = K999999;
            cellJS.daozhang.hidden = YES;cellJS.daozhnagLB.hidden = YES;
        }else{
            cellJS.stateLB.text = @"已到账";
            cellJS.stateLB.textColor = [UIColor colorWithHexString:@"#CA9F61"];
            cellJS.daozhnagLB.text =[NSString stringWithFormat:@"%@",[mode.pay_time substringToIndex:16]];
        }
        cellJS.moneyLB.text = [NSString stringWithFormat:@"-%.2f",mode.app_set_count];
        return cellJS;
        
    } else {
        CSmyincomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmyincomeTableViewCell" forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone; //取消cell点击状态
        
        cell.orderLb.text = [NSString stringWithFormat:@"订单编号: %@",mode.Id];
        cell.moneyLb.text = [NSString stringWithFormat:@"+%@",mode.earning];
        
        // NSInteger ime =  [self timeSwitchTimestamp:mode.pay_time andFormatter:@"YYYY-MM-dd hh:mm:ss"];
        //    NSString *time = [NSString stringWithFormat:@"%ld",ime];
        //    cell.timeLB.text = [NSString timeAgoWithDate:time];
        cell.timeLB.text = [NSString stringWithFormat:@"%@",[mode.pay_time substringToIndex:16]];
        
        cell.timeLB.textColor = K333333;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //取消选中效果
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (IBAction)seleTimeAction:(UIButton *)sender {
    //[[UIApplication sharedApplication].keyWindow addSubview:_bgview];
    [self.view addSubview:_bgview];
}

-(void)showbgview{
    
    //self.bgview = [[UIView alloc]initWithFrame:CGRectMake(_myTableView.frame.origin.x, _myTableView.frame.origin.y, KWIDTH, KHEIGHT-_myTableView.frame.origin.y)];
    self.bgview = [[UIView alloc]initWithFrame:CGRectMake(_myTableView.frame.origin.x, kScaleNum(64), KWIDTH, KHEIGHT-_myTableView.frame.origin.y)];
    self.bgview.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whithview = [[UIView alloc]initWithFrame:CGRectMake(0, -20, KWIDTH, 255)];
    whithview.backgroundColor = [UIColor whiteColor];
    [self.bgview  addSubview:whithview];
    
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    datePickManager.isShadeBackground = true;
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    //    datePicker.datePickerType = PGDatePickerTypeVertical;
    datePicker.isHiddenMiddleText = false;
    datePicker.maximumDate = [NSDate date];
    datePicker.autoSelected = true;
    datePicker.datePickerMode = PGDatePickerModeDate;
    [whithview addSubview:datePickManager.view];
    //    datePickManager.view.frame = CGRectMake(0, 50, KWIDTH, KHEIGHT);
    datePickManager.view.frame = CGRectMake(0, 50, KWIDTH, 150);
    [self addChildViewController:datePickManager];
    
    UIButton *fifishBUt = [UIButton  buttonWithType:(UIButtonTypeCustom)];
    [fifishBUt setTitle:@"完成" forState:(UIControlStateNormal)];
    [whithview addSubview:fifishBUt];
    [fifishBUt setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    [fifishBUt addTarget:self action:@selector(finishAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [fifishBUt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.right.offset(-16);
        make.width.offset(80);
        make.height.offset(42);
    }];
    
    self.lefttimeLB = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.lefttimeLB.frame =CGRectMake(0, 42, KWIDTH/2, 34 );
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    [self.lefttimeLB setTitle: [[self getCurrentTimes] substringToIndex:10] forState:(UIControlStateNormal)];
    [whithview  addSubview:self.lefttimeLB];
    [self.lefttimeLB addTarget:self action:@selector(leftActionsss) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.righttimeLB = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.righttimeLB.frame =CGRectMake(KWIDTH/2, 42, KWIDTH/2, 34 );
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
    [self.righttimeLB setTitle: [[self getCurrentTimes] substringToIndex:10] forState:(UIControlStateNormal)];
    [whithview  addSubview:self.righttimeLB];
    [self.righttimeLB addTarget:self action:@selector(righttimeLBActionsss) forControlEvents:(UIControlEventTouchUpInside)];
    
    UILabel *zhi = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH/2-8, 42, 16, 34)];
    zhi.textAlignment = NSTextAlignmentCenter;
    [whithview  addSubview:zhi];
    zhi.text = @"至";
    zhi.font = FontSize(14);
    
    kWeakSelf;
    self.leftLB = [[UILabel alloc]init];
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    [whithview addSubview:self.leftLB];
    [_leftLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.lefttimeLB.mas_bottom).offset(8);
        make.width.offset(130);
        make.height.offset(1);
        
        make.centerX.mas_equalTo(weakSelf.lefttimeLB.mas_centerX).offset(0);
    }];
    self.rightLB = [[UILabel alloc]init];
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [whithview addSubview:self.rightLB];
    [_rightLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.righttimeLB.mas_bottom).offset(8);
        make.width.offset(130);
        make.height.offset(1);
        make.centerX.mas_equalTo(weakSelf.righttimeLB.mas_centerX).offset(0);
    }];
}


-(void)leftActionsss{
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
    self.isleft = YES;
}

-(void)righttimeLBActionsss{
    self.isleft = NO;
    
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
}

-(NSString*)getCurrentTimes{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];
    //----------将nsdate按formatter格式转成nsstring
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}

-(void)finishAction:(UIButton *)buf{
    [self.bgview removeFromSuperview];
    
    [self.seleButtn setTitle:@"" forState:(UIControlStateNormal)];
    
    self.upTimeLb.text = self.lefttimeLB.titleLabel.text;
    self.dowTimeLb.text = self.righttimeLB.titleLabel.text;
    [self request];
}

#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    NSLog(@"dateComponents = %@", dateComponents);
    NSString *time = [NSString stringWithFormat:@"%ld-%ld-%ld",dateComponents.year,dateComponents.month,dateComponents.day];
    
    if (!strIsEmpty(time)) {
        
        NSString *ster = [[time substringFromIndex:5] substringToIndex:2];
        NSInteger ind = [ster integerValue];
        if (ind < 10) {
            NSMutableString* str1=[[NSMutableString alloc]initWithString:time];//存在堆区，可变字符串
            [str1 insertString:@"0"atIndex:5];//把一个字符串插入另一个字符串中的某一个位置
            KMyLog(@"dsfdsfds%@",str1);
            time = str1;
        }
        
        if (time.length == 9) {
            NSMutableString* ind2=[[NSMutableString alloc]initWithString:time];//存在堆区，可变字符串
            [ind2 insertString:@"0"atIndex:8];//把一个字符串插入另一个字符串中的某一个位置
            KMyLog(@"dsfdsfds%@",ind2);
            time = ind2;
        }
    }
    
    if (self.isleft) {
        [self.lefttimeLB setTitle:time forState:(UIControlStateNormal)];
    }else{
        [self.righttimeLB setTitle:time forState:(UIControlStateNormal)];
    }
}

#pragma mark - 将某个时间转化成 时间戳
- (NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format]; //(@"YYYY-MM-dd hh:mm:ss") ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate* date = [formatter dateFromString:formatTime]; //------------将字符串按formatter转成nsdate
    
    //时间转时间戳的方法:
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    NSLog(@"将某个时间转化成 时间戳&&&&&&&timeSp:%ld",(long)timeSp); //时间戳的值
    return timeSp;
}

@end
