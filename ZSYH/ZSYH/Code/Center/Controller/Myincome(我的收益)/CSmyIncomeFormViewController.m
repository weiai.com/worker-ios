//
//  CSmyIncomeFormViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmyIncomeFormViewController.h"
#import "CSmyIncomeFormRightViewController.h"
#import "CSmyImcomeLeftViewController.h"

@interface CSmyIncomeFormViewController ()<UIScrollViewDelegate>{
    UILabel *mylab;
}
@property(nonatomic,weak)UIScrollView *contentScrollView;
@property(nonatomic,strong)UIButton *oldButton;
@property(nonatomic,strong)UILabel *seletLabel;
@property(nonatomic,strong)NSMutableArray *titleArray;

@end

@implementation CSmyIncomeFormViewController
-(NSMutableArray *)titleArray{
    
    if (_titleArray == nil) {
        _titleArray = [NSMutableArray array];
        
    }
    return _titleArray;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%f, %f", self.view.frame.size.width, self.view.frame.size.height);
    self.title = self.titleStr;
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self setScrollViewTopandBottom];
    [self setupViewControllers];
    [self setTitlesButton];
    // Do any additional setup after loading the view.
}


//设置页面scrollView
-(void)setScrollViewTopandBottom{
    
    
    UIScrollView *contentScroll = [[UIScrollView alloc]init];
    contentScroll.frame = CGRectMake(0, kNaviHeight, KWIDTH, self.view.frame.size.height  -kNaviHeight);
    contentScroll.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentScroll];
    self.contentScrollView = contentScroll;
    self.contentScrollView.userInteractionEnabled = YES;
    self.contentScrollView.scrollEnabled = YES;;
    self.contentScrollView.pagingEnabled = YES;
    //显示水平方向的滚动条(默认YES)
    self.contentScrollView.showsHorizontalScrollIndicator = NO;
    //显示垂直方向的滚动条(默认YES)
    self.contentScrollView.showsVerticalScrollIndicator = NO;
    self.contentScrollView.bounces = NO;
    
    self.contentScrollView.contentSize = CGSizeMake(KWIDTH*2, KHEIGHT-kNaviHeight-kNaviHeight);
    
    self.contentScrollView.delegate = self;
}



#pragma mark -- UISCrollview代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSInteger index = scrollView.contentOffset.x/scrollView.bounds.size.width;
    
    //  CGFloat offsetX = index * ScreenW;
    
    UIButton *but = (UIButton *)[self.view viewWithTag:100+index];
    [self.oldButton setTitleColor:[self colorWithHexString:@"#666666"] forState:UIControlStateNormal];
    
    [but setTitleColor:[self colorWithHexString:@"#CA9F61"] forState:UIControlStateNormal];
    self.oldButton = but;
    CGPoint CEN =  mylab.center;
    CEN.x = but.center.x;
    mylab.center = CEN;
}

-(void)setTitlesButton{
    UIView *myview = [[UIView alloc]initWithFrame:CGRectMake(0, KHEIGHT-50, KWIDTH, 50)];
    myview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:myview];
    
    NSArray *arr = @[@"明细",@"统计"];
    CGFloat leftRight = 0;
    CGFloat wwww = KWIDTH/arr.count;
    CGFloat hhhhh = 50;
//    CGFloat yyyy = KHEIGHT - 50;
    CGFloat yyyy = 0;

    for (int i = 0; i<arr.count; i++) {
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(leftRight+wwww*i, yyyy, wwww, hhhhh);
        [myview addSubview:myButton];
        myButton.tag = 100+i;
        [myButton setTitle:arr[i] forState:UIControlStateNormal];
        [myButton addTarget:self action:@selector(titleClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [myButton setTitleColor:[ self colorWithHexString:@"#666666"] forState:UIControlStateNormal];
        myButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        if (i == 0) {
            self.oldButton = myButton;
            [self titleClick:myButton];
            mylab = [[UILabel alloc]init];
            [myview addSubview:mylab];
            mylab.frame = CGRectMake(10, yyyy, 50, 2);
            CGPoint CEN =  mylab.center;
            CEN.x = myButton.center.x;
            mylab.center = CEN;
            mylab.backgroundColor = [UIColor colorWithHexString:@"#D9B87E"];
        }
    }
//    myview.layer.shadowColor = [UIColor blackColor].CGColor;
//    myview.layer.shadowOpacity = 0.5f;
//    myview.layer.shadowOffset = CGSizeMake(0,2);
    [myview shadowshadowOpacity:0.3 borderWidth:0.5 borderColor:[UIColor whiteColor] erRadius:0 shadowColor:K999999 shadowRadius:2 shadowOffset:CGSizeMake(0, -1)];
}

-(void)titleClick:(UIButton *)but{
    //0取出label
    NSInteger index = but.tag-100;
    //2.1计算滚动位置
    CGFloat offsetX = index * KWIDTH;
    [self.contentScrollView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
}

//显示页面
-(void)showVC:(NSInteger)index{
    CGFloat offsetX = index * KWIDTH;
    UIViewController *vc = self.childViewControllers[index];
    //判断当前控制器的View 有没有加载过 如果已经加载过 就不需要加载
    if (vc.isViewLoaded) return;
    vc.view.frame = CGRectMake(offsetX, 0, ScreenW, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:vc.view];
}

- (void)setupViewControllers {
    
    //明细
    CSmyImcomeLeftViewController *seveVC = [[CSmyImcomeLeftViewController alloc]init];
    seveVC.view.frame = CGRectMake(0, 0, KWIDTH, self.contentScrollView.frame.size.height);
    seveVC.titleStr = self.titleStr;
    [self.contentScrollView addSubview:seveVC.view];
    [self addChildViewController:seveVC];
    
    //统计
    CSmyIncomeFormRightViewController *evaluate = [[CSmyIncomeFormRightViewController alloc]init];
    evaluate.titleStr = self.titleStr;
    evaluate.view.frame = CGRectMake(KWIDTH, 0, KWIDTH, self.contentScrollView.frame.size.height);
    [self.contentScrollView addSubview:evaluate.view];
    [self addChildViewController:evaluate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
