//
//  ZYGrandAccDetailViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYGrandAccDetailViewController.h"
#import "ZGrantStateCell.h"

@interface ZYGrandAccDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UILabel *orderNumC; //订单编号
@property (nonatomic, strong) UILabel *anzhuDateC; //安装时间
@property (nonatomic, strong) UILabel *sunhuDateC; //损坏时间
@property (nonatomic, strong) UILabel *pjplTitC; //配件品类
@property (nonatomic, strong) UILabel *pjmcTitC; //配件名称
@property (nonatomic, strong) UILabel *yxqTitC; //有效期
@property (nonatomic, strong) UILabel *bzqTitC; //保质期

@end

@implementation ZYGrandAccDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self showPJXQDetailUI];
    [self setupViewAction];
    [self.tableView reloadData];
}

#pragma mark - 界面布局
- (void)setupViewAction{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight, kScreen_Width, kScreen_Height - kNaviHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = kColorWithHex(0xf2f2f2);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [_tableView registerClass:[ZGrantStateCell class] forCellReuseIdentifier:NSStringFromClass([ZGrantStateCell class])];
    _tableView.tableFooterView = self.footerView;
}

#pragma mark - UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return self.footerView.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        ZGrantStateCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZGrantStateCell class]) forIndexPath:indexPath];
        cell.reason = self.reason;
        cell.state = self.state;
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        NSInteger stateInteger = [self.state integerValue];
        if (stateInteger == 2 || stateInteger == 4) {
            return [ZGrantStateCell cellHeightRefuse];
        }else{
            return [ZGrantStateCell cellHeightNormal];
        }
    }
    return 0;
}

- (void)showPJXQDetailUI {
    
    _footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, kScaleNum(314+55))];
    _footerView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UILabel *topBgLab = [[UILabel alloc] init];
    topBgLab.backgroundColor = [UIColor whiteColor];
    topBgLab.frame = CGRectMake(0, 9, KWIDTH, kScaleNum(55));
    [self.footerView addSubview:topBgLab];
    
    UILabel *orderNumT = [[UILabel alloc] init];
    orderNumT.frame = CGRectMake(16, 22, 70, 14);
    orderNumT.text = @"订单编号:";
    orderNumT.font = FontSize(14);
    orderNumT.textColor = K666666;
    [topBgLab addSubview:orderNumT];
    
    UILabel *orderNumC = [[UILabel alloc] init];
    orderNumC.frame = CGRectMake(orderNumT.right, 22, KWIDTH-32-70, 14);
    orderNumC.text = self.orderIdStr;
    orderNumC.font = FontSize(14);
    orderNumC.textColor = K666666;
    [topBgLab addSubview:orderNumC];
    self.orderNumC = orderNumC;
    
    UIView *bottomV = [[UIView alloc]init];
    bottomV.backgroundColor = [UIColor whiteColor];
    bottomV.frame = CGRectMake(0, topBgLab.bottom+10, KWIDTH, kScaleNum(314));
    [self.footerView addSubview:bottomV];
    
    UILabel *anzhuDateT = [[UILabel alloc] init];
    anzhuDateT.frame = CGRectMake(26, 22, 70, 14);
    anzhuDateT.text = @"安装时间";
    anzhuDateT.font = FontSize(14);
    anzhuDateT.textColor = K999999;
    [bottomV addSubview:anzhuDateT];
    
    UILabel *anzhuDateC = [[UILabel alloc] init];
    anzhuDateC.frame = CGRectMake(orderNumT.right+10, 22, KWIDTH-52-70-10, 14);
    anzhuDateC.text = self.anzhuTime;
    anzhuDateC.font = FontSize(14);
    anzhuDateC.textColor = K666666;
    [bottomV addSubview:anzhuDateC];
    self.anzhuDateC = anzhuDateC;
    
    UILabel *line = [[UILabel alloc] init];
    line.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    line.frame = CGRectMake(10, anzhuDateT.bottom +10, KWIDTH-20, 1);
    [bottomV addSubview:line];
    
    UILabel *sunhuDateT = [[UILabel alloc] init];
    sunhuDateT.frame = CGRectMake(26, line.bottom +8, 70, 14);
    sunhuDateT.text = @"损坏时间";
    sunhuDateT.font = FontSize(14);
    sunhuDateT.textColor = K666666;
    [bottomV addSubview:sunhuDateT];
    
    UILabel *sunhuDateC = [[UILabel alloc] init];
    sunhuDateC.frame = CGRectMake(orderNumT.right+10, line.bottom +8, KWIDTH-52-70-10, 14);
    sunhuDateC.text = self.sunhuTime;
    sunhuDateC.font = FontSize(14);
    sunhuDateC.textColor = K666666;
    [bottomV addSubview:sunhuDateC];
    self.sunhuDateC = sunhuDateC;
    
    UILabel *kuang = [[UILabel alloc] init];
    kuang.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    kuang.frame = CGRectMake(10, sunhuDateT.bottom +24, KWIDTH- 20, 199);
    kuang.layer.borderWidth = 1;
    kuang.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    kuang.layer.cornerRadius = 12;
    kuang.layer.masksToBounds = YES;
    [bottomV addSubview:kuang];
    
    UILabel *pjplTitL = [[UILabel alloc] init];
    pjplTitL.frame = CGRectMake(16, 22, 70, 14);
    pjplTitL.text = @"配件品类";
    pjplTitL.font = FontSize(14);
    pjplTitL.textColor = K666666;
    [kuang addSubview:pjplTitL];
    
    UILabel *pjplTitC = [[UILabel alloc] init];
    pjplTitC.frame = CGRectMake(orderNumT.right+10, 22, KWIDTH-52-70-10, 14);
    pjplTitC.text = self.pjpinlei;
    pjplTitC.font = FontSize(14);
    pjplTitC.textColor = K333333;
    [kuang addSubview:pjplTitC];
    self.pjplTitC = pjplTitC;
    
    UILabel *line1 = [[UILabel alloc] init];
    line1.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    line1.frame = CGRectMake(10, pjplTitL.bottom +10, KWIDTH-40, 1);
    [kuang addSubview:line1];
    
    UILabel *pjmcTitL = [[UILabel alloc] init];
    pjmcTitL.frame = CGRectMake(16, line1.bottom +8, 70, 14);
    pjmcTitL.text = @"配件名称";
    pjmcTitL.font = FontSize(14);
    pjmcTitL.textColor = K666666;
    [kuang addSubview:pjmcTitL];
    
    UILabel *pjmcTitC = [[UILabel alloc] init];
    pjmcTitC.frame = CGRectMake(orderNumT.right+10, line1.bottom +8, KWIDTH-52-70-10, 14);
    pjmcTitC.text = self.pjname;
    pjmcTitC.font = FontSize(14);
    pjmcTitC.textColor = K333333;
    [kuang addSubview:pjmcTitC];
    self.pjmcTitC = pjmcTitC;
    
    UILabel *line2 = [[UILabel alloc] init];
    line2.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    line2.frame = CGRectMake(10, pjmcTitL.bottom +10, KWIDTH-40, 1);
    [kuang addSubview:line2];
    
    UILabel *yxqTitL = [[UILabel alloc] init];
    yxqTitL.frame = CGRectMake(16, line2.bottom +8, 70, 14);
    yxqTitL.text = @"有效期";
    yxqTitL.font = FontSize(14);
    yxqTitL.textColor = K666666;
    [kuang addSubview:yxqTitL];
    
    UILabel *yxqTitC = [[UILabel alloc] init];
    yxqTitC.frame = CGRectMake(orderNumT.right+10, line2.bottom +8, KWIDTH-52-70-10, 14);
    yxqTitC.text = self.youxiaoqi;
    yxqTitC.font = FontSize(14);
    yxqTitC.textColor = K333333;
    [kuang addSubview:yxqTitC];
    self.yxqTitC = yxqTitC;
    
    UILabel *line3 = [[UILabel alloc] init];
    line3.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    line3.frame = CGRectMake(10, yxqTitL.bottom +10, KWIDTH-40, 1);
    [kuang addSubview:line3];
    
    UILabel *bzqTitL = [[UILabel alloc] init];
    bzqTitL.frame = CGRectMake(16, line3.bottom +8, 70, 14);
    bzqTitL.text = @"保质期";
    bzqTitL.font = FontSize(14);
    bzqTitL.textColor = K666666;
    [kuang addSubview:bzqTitL];
    
    UILabel *bzqTitC = [[UILabel alloc] init];
    bzqTitC.frame = CGRectMake(orderNumT.right+10, line3.bottom +8, KWIDTH-52-70-10, 14);
    bzqTitC.text = self.baozhiqi;
    bzqTitC.font = FontSize(14);
    bzqTitC.textColor = K333333;
    [kuang addSubview:bzqTitC];
    self.bzqTitC = bzqTitC;
    
    UILabel *line4 = [[UILabel alloc] init];
    line4.backgroundColor = [UIColor colorWithHexString:@"F2F2F2"];
    line4.frame = CGRectMake(10, bzqTitL.bottom +10, KWIDTH-40, 1);
    [kuang addSubview:line4];
}

@end
