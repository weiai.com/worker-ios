//
//  CSmyIncomeFormRightViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmyIncomeFormRightViewController.h"
#import "SCChart.h"
#import "CSMyincomModel.h"

@interface CSmyIncomeFormRightViewController ()<SCChartDataSource>
{
    NSIndexPath *path;
    SCChart *chartView;
    int ind ;
    
}
@property (weak, nonatomic) IBOutlet UILabel *shuomingLb;
@property (weak, nonatomic) IBOutlet UILabel *bgLable;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;
@property (weak, nonatomic) IBOutlet UILabel *chartsBGview;
@property (nonatomic,strong) NSString *type;
@property (weak, nonatomic) IBOutlet UIButton *yearbut;
@property (nonatomic,strong) UIButton *oldBut;
@property (nonatomic,strong) NSMutableArray *dateSource;
@property (weak, nonatomic) IBOutlet UIButton *rightBut;

@end

@implementation CSmyIncomeFormRightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ind = 24;
    _type = @"yea";
    
    self.bgLable.layer.masksToBounds = YES;
    self.bgLable.layer.cornerRadius = 4;
    self.bgLable.layer.borderWidth = 1;
    self.bgLable.layer.borderColor = [UIColor colorWithHexString:@"#CA9F61"].CGColor;
    // Do any additional setup after loading the view from its nib.
    self.dateSource = [NSMutableArray arrayWithCapacity:1];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, _yearbut.frame.size.width, _yearbut.frame.size.height) byRoundingCorners:UIRectCornerBottomRight|UIRectCornerTopRight cornerRadii:CGSizeMake(4, 4)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _yearbut.bounds;
    maskLayer.path = maskPath.CGPath;
    _yearbut.layer.mask = maskLayer;
    self.oldBut = _yearbut;
    NSString *timestr = [self getCurrentTimes];
    self.titleLB.text = [timestr substringToIndex:4];
    if (strIsEmpty(_titleStr)) {
        _titleStr = [singlTool shareSingTool].titlsr;
    }
    [self requestDetaile];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
}

-(void)requestDetaile{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] =  _type;
    param[@"time"] =  _titleLB.text;
    if ([_type isEqualToString:@"wek"]) {
        NSString *myate =_titleLB.text;
        NSString *str = [myate substringToIndex:10];
        param[@"time"] =  str;
    }
    
    kWeakSelf;
    NSString *str = @"";
    if ([self.titleStr isEqualToString:@"结算历史"]) {
        str = jieSuantongji;
        [NetWorkTool POST:str param:param success:^(id dic) {
            KMyLog(@"ww订单收益%@",dic);
            [weakSelf.dateSource removeAllObjects];
            
            //            NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
            //            mudic = [[dic objectForKey:@"data"]  objectForKey:@"data"];
            //
            //            for (int i = 1; i <[mudic allKeys].count +1; i++) {
            //                NSString *key  = @"";
            //                if (i < 10) {
            //                    key = [NSString stringWithFormat:@"0%d",i];
            //                }else{
            //                    key = [NSString stringWithFormat:@"%d",i];
            //                }
            //                NSString *vale = [mudic objectForKey:key];
            //                [weakSelf.dateSource addObject:vale];
            //            }
            
            NSArray *myArr = dic[@"data"][@"data"];
            
            for (int i = 0; i <myArr.count; i++) {
                NSString *vale = myArr[i];
                [weakSelf.dateSource addObject:vale];
            }
            
            self->ind = weakSelf.dateSource.count;
            [self showUUUUUUU];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
    } else {
        str = shouyiTongji;
        [NetWorkTool POST:str param:param success:^(id dic) {
            KMyLog(@"ww订单收益%@",dic);
            [weakSelf.dateSource removeAllObjects];
            //        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
            //        mudic = [[dic objectForKey:@"data"]  objectForKey:@"data"];
            //
            //        for (int i = 1; i <[mudic allKeys].count +1; i++) {
            //            NSString *key  = @"";
            //            if (i < 10) {
            //                key = [NSString stringWithFormat:@"0%d",i];
            //            }else{
            //                key = [NSString stringWithFormat:@"%d",i];
            //            }
            //            NSString *vale = [mudic objectForKey:key];
            //            [weakSelf.dateSource addObject:vale];
            //        }
            
            //        NSArray *myArr = [dic objectForKey:@"data"];
            //
            //        for (int i = 1; i <myArr.count +1; i++) {
            //            NSString *key  = @"";
            //            if (i < 10) {
            //                key = [NSString stringWithFormat:@"0%d",i];
            //            }else{
            //                key = [NSString stringWithFormat:@"%d",i];
            //            }
            //            NSString *vale = [myArr[i] objectForKey:key];
            //            [weakSelf.dateSource addObject:vale];
            //        }
            NSArray *myArr = dic[@"data"][@"data"];
            
            for (int i = 0; i <myArr.count; i++) {
                NSString *vale = myArr[i];
                [weakSelf.dateSource addObject:vale];
            }
            
            self->ind = weakSelf.dateSource.count;
            [self showUUUUUUU];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }
}

- (void)btnPressed:(id)sender {
    [chartView strokeChart];
}

- (NSArray *)getXTitles:(int)num {
    NSMutableArray *xTitles = [NSMutableArray array];
    for (int i=0; i<num; i++) {
        NSString * str = [NSString stringWithFormat:@"%d",i+1];
        [xTitles addObject:str];
    }
    return xTitles;
}

#pragma mark - @required
//横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart {
    return [self getXTitles:ind];
}

//数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart {
    NSMutableArray *ary = [NSMutableArray array];
    ary =self.dateSource;
    return @[ary];
}

#pragma mark - @optional
//颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart {
    return @[SCGreen,SCRed,[UIColor redColor]];
}

- (IBAction)dayAction:(UIButton *)sender {
    _type = @"day";
    
    NSString *str  = [self getCurrentTimes];
    NSString *mouStr = [str substringToIndex:10];
    self.titleLB.text = mouStr;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:sender.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft cornerRadii:CGSizeMake(4, 4)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = sender.bounds;
    maskLayer.path = maskPath.CGPath;
    sender.layer.mask = maskLayer;
    
    [self.oldBut setBackgroundColor:[UIColor clearColor]];
    [self.oldBut setTitleColor:[UIColor colorWithHexString:@"#E29501"] forState:(UIControlStateNormal)];
    [sender setBackgroundColor:[UIColor colorWithHexString:@"#E29501"]];
    [sender setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.oldBut = sender;
    
    [self requestDetaile];
}

- (IBAction)zhouAction:(UIButton *)sender {
    _type = @"wek";
    self.titleLB.text = [NSString stringWithFormat:@"%@",[self getWeekTimewith:[NSDate date]]];
    
    [self.oldBut setBackgroundColor:[UIColor clearColor]];
    [self.oldBut setTitleColor:[UIColor colorWithHexString:@"#E29501"] forState:(UIControlStateNormal)];
    [sender setBackgroundColor:[UIColor colorWithHexString:@"#E29501"]];
    [sender setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.oldBut = sender;
    
    [self requestDetaile];
}

- (IBAction)mouthAction:(UIButton *)sender {
    _type = @"mon";
    NSString *str  = [self getCurrentTimes];
    NSString *mouStr = [str substringToIndex:7];
    self.titleLB.text = mouStr;
    [self.oldBut setBackgroundColor:[UIColor clearColor]];
    
    [self.oldBut setTitleColor:[UIColor colorWithHexString:@"#E29501"] forState:(UIControlStateNormal)];
    [sender setBackgroundColor:[UIColor colorWithHexString:@"#E29501"]];
    [sender setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.oldBut = sender;
    
    [self requestDetaile];
}

- (IBAction)yearAction:(UIButton *)sender {
    _type = @"yea";
    NSString *str  = [self getCurrentTimes];
    NSString *mouStr = [str substringToIndex:4];
    self.titleLB.text = mouStr;
    
    [self.oldBut setBackgroundColor:[UIColor clearColor]];
    [self.oldBut setTitleColor:[UIColor colorWithHexString:@"#E29501"] forState:(UIControlStateNormal)];
    [sender setBackgroundColor:[UIColor colorWithHexString:@"#E29501"]];
    [sender setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.oldBut = sender;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:sender.bounds byRoundingCorners:UIRectCornerBottomRight|UIRectCornerTopRight cornerRadii:CGSizeMake(4, 4)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = sender.bounds;
    maskLayer.path = maskPath.CGPath;
    sender.layer.mask = maskLayer;
    [self requestDetaile];
}

- (IBAction)leftAction:(UIButton *)sender {
    
    if ([_type isEqualToString:@"yea"]) {
        //        if ([[self.titleLB.text substringToIndex: 4] intValue] < [[[self getCurrentTimes] substringToIndex:4] intValue]) {
        //            [self.rightBut setImage:imgname(@"AccleftBut") forState:(UIControlStateNormal)];
        //        }else{
        //            [self.rightBut setImage:imgname(@"AccrightBut") forState:(UIControlStateNormal)];
        //        }
        NSInteger idd = [self.titleLB.text integerValue];
        idd -=1;
        self.titleLB.text = [NSString stringWithFormat:@"%ld",idd];
        
    }else if ([_type isEqualToString:@"mon"]) {
        if ([[self.titleLB.text substringFromIndex:5] integerValue] == 01) {
            //[sender setImage:imgname(@"AccrightBut") forState:(UIControlStateNormal)];
            
        } else {
            //[sender setImage:imgname(@"AccleftBut") forState:(UIControlStateNormal)];
            NSInteger idd = [[[self.titleLB.text substringFromIndex:5] substringToIndex:2] integerValue];
            idd -=1;
            NSString *iddStr =  [NSString stringWithFormat:@"%ld",idd];
            if (idd <10) {
                iddStr = [NSString stringWithFormat:@"0%ld",idd];
            }
            
            self.titleLB.text = [NSString stringWithFormat:@"%@%@",[self.titleLB.text substringToIndex:5],iddStr];
        }
        //        if ([[self.titleLB.text substringFromIndex:5] integerValue] == 12) {
        //            [self.rightBut setImage:imgname(@"AccrightBut") forState:(UIControlStateNormal)];
        //        }else{
        //            [self.rightBut setImage:imgname(@"AccleftBut") forState:(UIControlStateNormal)];
        //        }
    }else if ([_type isEqualToString:@"wek"]) {
        
        NSInteger day = [[[self.titleLB.text substringFromIndex:8] substringToIndex:2] integerValue];
        if (day > 7) {
            day -=1;
            if (day == -1) {
                day = 1;
            }
            NSString *yew = [[self.titleLB.text substringFromIndex:0] substringToIndex:8];
            
            NSString *ste = [NSString stringWithFormat:@"%@%ld 00:00:00.000",yew,day];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
            NSDate *birthdayDate = [dateFormatter dateFromString:ste];
            
            self.titleLB.text =    [self getWeekTimewith:birthdayDate];
        }else{
            //只显示本月
        }
        //        NSInteger idd = [[[self.titleLB.text substringFromIndex:8] substringToIndex:2] integerValue];
        //        if (idd == 01) {
        //            NSInteger mou = [[[self.titleLB.text substringFromIndex:5] substringToIndex:2] integerValue];
        //            mou -=1;
        //            if (mou == -1) {
        //                mou = 1;
        //            }
        //            NSInteger yew = [[[self.titleLB.text substringFromIndex:0] substringToIndex:4] integerValue];
        //            NSInteger intt = [self howManyDaysInThisMonth:mou yera:yew];
        //            NSString *ste = [NSString stringWithFormat:@"%ld-%ld-%ld 00:00:00.000",yew,mou,intt];
        //            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        //            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
        //            NSDate *birthdayDate = [dateFormatter dateFromString:ste];
        //
        //            self.titleLB.text =    [self getWeekTimewith:birthdayDate];
        //        }else{
        //
        //            NSInteger day = [[[self.titleLB.text substringFromIndex:8] substringToIndex:2] integerValue];
        //            day -=1;
        //            if (day == -1) {
        //                day = 1;
        //            }
        //            NSString *yew = [[self.titleLB.text substringFromIndex:0] substringToIndex:8];
        //
        //            NSString *ste = [NSString stringWithFormat:@"%@%ld 00:00:00.000",yew,day];
        //
        //            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
        //            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
        //            NSDate *birthdayDate = [dateFormatter dateFromString:ste];
        //
        //            self.titleLB.text =    [self getWeekTimewith:birthdayDate];
        //        }
        //
    }else if ([_type isEqualToString:@"day"]) {
        
        if ([[self.titleLB.text substringFromIndex:8] integerValue] == 01) {
            //[sender setImage:imgname(@"AccrightBut") forState:(UIControlStateNormal)];
        } else {
            //[sender setImage:imgname(@"AccleftBut") forState:(UIControlStateNormal)];
            NSInteger idd = [[[self.titleLB.text substringFromIndex:8] substringToIndex:2] integerValue];
            idd -=1;
            NSString *iddStr =  [NSString stringWithFormat:@"%ld",idd];
            if (idd <10) {
                iddStr = [NSString stringWithFormat:@"0%ld",idd];
            }
            self.titleLB.text = [NSString stringWithFormat:@"%@%@",[self.titleLB.text substringToIndex:8],iddStr];
        }
    }
    [self requestDetaile];
}

- (IBAction)rightAction:(UIButton *)sender {
    
    if ([_type isEqualToString:@"yea"]) {
        
        NSInteger idd = [self.titleLB.text integerValue];
        idd +=1;
        self.titleLB.text = [NSString stringWithFormat:@"%ld",idd];
        
    } else if ([_type isEqualToString:@"mon"]) {
        if ([[self.titleLB.text substringFromIndex:5] integerValue] == 12) {
            //[sender setImage:imgname(@"AccrightBut") forState:(UIControlStateNormal)];
            
        }else{
            //[sender setImage:imgname(@"AccleftBut") forState:(UIControlStateNormal)];
            NSInteger idd = [[[self.titleLB.text substringFromIndex:5] substringToIndex:2] integerValue];
            idd +=1;
            NSString *iddStr =  [NSString stringWithFormat:@"%ld",idd];
            if (idd <10) {
                iddStr = [NSString stringWithFormat:@"0%ld",idd];
            }
            
            self.titleLB.text = [NSString stringWithFormat:@"%@%@",[self.titleLB.text substringToIndex:5],iddStr];
        }
    }else if ([_type isEqualToString:@"wek"]) {
        NSInteger idd = [[[self.titleLB.text substringFromIndex:19] substringToIndex:2] integerValue];
        NSInteger year = [[[self.titleLB.text substringFromIndex:0] substringToIndex:4] integerValue];
        NSInteger motttt = [[[self.titleLB.text substringFromIndex:16] substringToIndex:2] integerValue];
        NSInteger iii = [self howManyDaysInThisMonth:motttt yera:year];
        
        if (idd == iii) {
            //NSInteger mou = [[[self.titleLB.text substringFromIndex:5] substringToIndex:2] integerValue];
            //            mou +=1;
            //            if (mou == 13) {
            //                mou = 12;
            //            }
            //            NSInteger yew = [[[self.titleLB.text substringFromIndex:0] substringToIndex:4] integerValue];
            //            NSString *ste = [NSString stringWithFormat:@"%ld-%ld-01 00:00:00.000",yew,mou];
            //            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            //            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
            //            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
            //            NSDate *birthdayDate = [dateFormatter dateFromString:ste];
            //            self.titleLB.text =    [self getWeekTimewith:birthdayDate];
        } else {
            
            NSInteger day = [[[self.titleLB.text substringFromIndex:19] substringToIndex:2] integerValue];
            
            day +=1;
            if (day == -1) {
                day = 1;
            }
            NSString *yew = [[self.titleLB.text substringFromIndex:0] substringToIndex:8];
            
            NSString *ste = [NSString stringWithFormat:@"%@%ld 00:00:00.000",yew,day];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
            NSDate *birthdayDate = [dateFormatter dateFromString:ste];
            
            self.titleLB.text =    [self getWeekTimewith:birthdayDate];
        }
        
    } else if ([_type isEqualToString:@"day"]) {
        
        NSInteger year = [[[self.titleLB.text substringFromIndex:0] substringToIndex:4] integerValue];
        NSInteger motttt = [[[self.titleLB.text substringFromIndex:5] substringToIndex:2] integerValue];
        NSInteger iii = [self howManyDaysInThisMonth:motttt yera:year];
        
        if ([[self.titleLB.text substringFromIndex:8] integerValue] == iii) {
            //如果是最后一天
            
        }else{
            NSInteger idd = [[[self.titleLB.text substringFromIndex:8] substringToIndex:2] integerValue];
            idd +=1;
            NSString *iddStr =  [NSString stringWithFormat:@"%ld",idd];
            if (idd <10) {
                iddStr = [NSString stringWithFormat:@"0%ld",idd];
            }
            self.titleLB.text = [NSString stringWithFormat:@"%@%@",[self.titleLB.text substringToIndex:8],iddStr];
        }
    }
    [self requestDetaile];
}

-(NSString*)getCurrentTimes{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    //现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];

    //----------将nsdate按formatter格式转成nsstring
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}

-(void)arrwithArr:(NSMutableArray *)date {
    
    if ([_type isEqualToString:@"yea"]) {
        
        ind = 12;
        NSMutableArray *yearMUarr = [NSMutableArray arrayWithCapacity:1];
        for (int i = 0; i <12; i++) {
            CSMyincomModel *moldel = [[CSMyincomModel alloc]init];
            moldel.all_cost = @"0";
            [yearMUarr addObject:moldel];
        }
        
        for (CSMyincomModel *mymodel in _dateSource) {
            NSInteger ind = [[[mymodel.createTime substringFromIndex:5]   substringToIndex:2] integerValue];
            ind -= 1;
            [yearMUarr removeObjectAtIndex:ind];
            [yearMUarr insertObject:mymodel atIndex:ind];
        }
        self.dateSource = yearMUarr;
    } else if([_type isEqualToString:@"mon"]){
        NSString *yeaSte =  [self.titleLB.text substringToIndex:4];
        NSInteger yer = [yeaSte integerValue];
        NSInteger mou = [[self.titleLB.text substringFromIndex:6] integerValue];
        ind =  (int)[self howManyDaysInThisMonth:mou yera:yer];
        
        NSMutableArray *yearMUarr = [NSMutableArray arrayWithCapacity:1];
        for (int i = 0; i <ind; i++) {
            CSMyincomModel *moldel = [[CSMyincomModel alloc]init];
            moldel.all_cost = @"0";
            [yearMUarr addObject:moldel];
        }
        
        for (CSMyincomModel *mymodel in _dateSource) {
            NSInteger ind = [[[mymodel.createTime substringFromIndex:8]   substringToIndex:2] integerValue];
            ind-=1;
            [yearMUarr removeObjectAtIndex:ind];
            [yearMUarr insertObject:mymodel atIndex:ind];
        }
        self.dateSource = yearMUarr;
    } else if([_type isEqualToString:@"wek"]){
        
        ind =  7;
        
        NSMutableArray *yearMUarr = [NSMutableArray arrayWithCapacity:1];
        for (int i = 0; i <ind; i++) {
            CSMyincomModel *moldel = [[CSMyincomModel alloc]init];
            moldel.all_cost = @"0";
            [yearMUarr addObject:moldel];
        }
        
        for (CSMyincomModel *mymodel in _dateSource) {
            NSInteger ind = [[[mymodel.createTime substringFromIndex:8]   substringToIndex:2] integerValue];
            int sss = [[[self.titleLB.text substringToIndex:10] substringFromIndex:8] intValue];
            ind = ind -sss;
            //ind-=1;
            if (ind == -1) {
                ind = 0;
            }
            [yearMUarr removeObjectAtIndex:ind];
            [yearMUarr insertObject:mymodel atIndex:ind];
        }
        self.dateSource = yearMUarr;
        
    } else if([_type isEqualToString:@"day"]){
        
        ind =  24;
        
        NSMutableArray *yearMUarr = [NSMutableArray arrayWithCapacity:1];
        for (int i = 0; i <ind; i++) {
            CSMyincomModel *moldel = [[CSMyincomModel alloc]init];
            moldel.all_cost = @"0";
            [yearMUarr addObject:moldel];
        }
        
        for (CSMyincomModel *mymodel in _dateSource) {
            NSInteger ind = [[[mymodel.createTime substringFromIndex:11]   substringToIndex:2] integerValue];
            ind-=1;
            
            [yearMUarr removeObjectAtIndex:ind];
            [yearMUarr insertObject:mymodel atIndex:ind];
        }
        self.dateSource = yearMUarr;
    }
    [self showUUUUUUU];
}

-(void)showUUUUUUU{
    if (chartView) {
        [chartView removeFromSuperview];
        chartView = nil;
    }
    //    path = indexPath;
    chartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(0, 30, [UIScreen mainScreen].bounds.size.width - 32, 190)
                                               withSource:self
                                                withStyle:SCChartBarStyle];
    [chartView showInView:self.chartsBGview];
}

// 获取今年指定月的天数
- (NSInteger)howManyDaysInThisMonth :(NSInteger)imonth  yera:(NSUInteger )year{
    
    if((imonth == 1)||(imonth == 3)||(imonth == 5)||(imonth == 7)||(imonth == 8)||(imonth == 10)||(imonth == 12))
        return 31 ;
    
    if((imonth == 4)||(imonth == 6)||(imonth == 9)||(imonth == 11))
        return 30;
    
    if((year%4 == 1)||(year%4 == 2)||(year%4 == 3)) {
        return 28;
    }
    
    if(year%400 == 0)
        
        return 29;
    if(year%100 == 0)
        return 28;
    return 29;
}

- (NSString *)getWeekTimewith:(NSDate* )nowDate {
    if (nowDate == nil) {
        nowDate = [NSDate date];
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSDayCalendarUnit fromDate:nowDate];
    // 获取今天是周几
    NSInteger weekDay = [comp weekday];
    // 获取几天是几号
    NSInteger day = [comp day];
    NSLog(@"%d----%d",weekDay,day);
    
    // 计算当前日期和本周的星期一和星期天相差天数
    long firstDiff,lastDiff;
    //    weekDay = 1;
    if (weekDay == 1) {
        firstDiff = -6;
        lastDiff = 0;
    } else {
        firstDiff = [calendar firstWeekday] - weekDay + 1;
        lastDiff = 8 - weekDay;
    }
    NSLog(@"firstDiff: %ld   lastDiff: %ld",firstDiff,lastDiff);
    
    // 在当前日期(去掉时分秒)基础上加上差的天数
    NSDateComponents *firstDayComp = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit  fromDate:nowDate];
    [firstDayComp setDay:day + firstDiff];
    NSDate *firstDayOfWeek = [calendar dateFromComponents:firstDayComp];
    
    NSDateComponents *lastDayComp = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit   fromDate:nowDate];
    [lastDayComp setDay:day + lastDiff];
    NSDate *lastDayOfWeek = [calendar dateFromComponents:lastDayComp];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *firstDay = [formatter stringFromDate:firstDayOfWeek];
    NSString *lastDay = [formatter stringFromDate:lastDayOfWeek];
    NSLog(@"%@=======%@",firstDay,lastDay);
    
    NSString *dateStr = [NSString stringWithFormat:@"%@-%@",firstDay,lastDay];
    
    return dateStr;
}

-(void)dateWithString:(NSString *)birthdayStr{
    
    //NSString *birthdayStr=@"1986-03-28 00:00:00.000";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];//解决8小时时间差问题
    NSDate *birthdayDate = [dateFormatter dateFromString:birthdayStr];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
