//
//  CSmyGrandListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSmyGrandListModel : NSObject

@property (nonatomic, copy) NSString *payTime;
@property (nonatomic, copy) NSString *azTime;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, copy) NSString *parts_name;
@property (nonatomic, copy) NSString *QRcode;
@property (nonatomic, copy) NSString *updateTime;

@property (nonatomic, copy) NSString *install;

@property (nonatomic, copy) NSString *isPay;
@property (nonatomic, copy) NSString *payUserId;
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *isAgentLaunch;
@property (nonatomic, copy) NSString *contractPrice;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *warranty;
@property (nonatomic, copy) NSString *agentId;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *state;

@property (nonatomic, copy) NSString *shTime;
@property (nonatomic, copy) NSString *soft;
@property (nonatomic, copy) NSString *quantity;
@property (nonatomic, copy) NSString *provinceAgentId;
@property (nonatomic, copy) NSString *reason;

@property (nonatomic, copy) NSString *salePrice;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *compensationPrice;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *partsId;

@property (nonatomic, copy) NSString *repUserId;

@property (nonatomic, copy) NSString *termValidity;
@property (nonatomic, copy) NSString *productionDate;
@property (nonatomic, copy) NSString *attribute;

//"payTime" : "2019-09-01 14:52:07",
//"azTime" : "2019-09-27 15:22:35",
//"code" : 5,
//"typeName" : "洗衣机滚筒洗衣机电脑板",
//"parts_name" : "测试配件一号",
//"QRcode" : "68231634377245081620190823692906",
//"updateTime" : "2019-09-27 17:24:56",
//"install" : "24",
//"isPay" : "1",
//"payUserId" : "",
//"qrcode" : "68231634377245081620190823692906",
//"isAgentLaunch" : "0",
//"attribute" : "123",
//"contractPrice" : 15,
//"name" : "属性名称1",
//"warranty" : "8",
//"agentId" : "674434775649357824",
//"id" : "695060969147076333",
//"state" : "6",
//"termValidity" : "2021-08-23",
//"shTime" : "2019-09-27 15:22:43",
//"soft" : 1,
//"productionDate" : "2019-08-23",
//"quantity" : 0,
//"provinceAgentId" : "687119632837906432",
//"reason" : "0.0.0",
//"salePrice" : 20,
//"createDate" : "2019-09-18 00:16:24",
//"compensationPrice" : 1,
//"createTime" : "2019-09-27 15:39:03",
//"partsId" : "1231313213131",
//"repUserId" : "676843363709751296"

@end

NS_ASSUME_NONNULL_END
