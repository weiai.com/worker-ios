//
//  CSmyImcomeLeftViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmyGrandViewController.h"
#import "CSmyGrandTableViewCell.h"
#import "CSMyincomModel.h"
#import "PGDatePickManager.h"
#import "ZYGrandAccDetailViewController.h"
#import "CSmyGrandListModel.h"

@interface CSmyGrandViewController ()<UITableViewDelegate,UITableViewDataSource,PGDatePickerDelegate>
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UILabel *upTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *dowTimeLb;
@property (weak, nonatomic) IBOutlet UILabel *bgfLB;
@property (weak, nonatomic) IBOutlet UILabel *totleincomLB;
@property (weak, nonatomic) IBOutlet UIButton *seleButtn;
@property (nonatomic, strong) UIView *bgview;
@property (nonatomic, strong) UIButton *lefttimeLB;
@property (nonatomic, strong) UIButton *righttimeLB;
@property (nonatomic, strong) UILabel *leftLB;
@property (nonatomic, strong) UILabel *rightLB;
@property (nonatomic, assign) BOOL isleft;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSMutableArray *numberArr;
@property (nonatomic,strong) NSString *dateStr;

@end

@implementation CSmyGrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isleft = YES;
    self.title = @"补助金";
    
    _type = @"day";
    
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    //self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    self.numberArr = [NSMutableArray arrayWithCapacity:1];
    
    self.bgfLB.layer.cornerRadius = 18;
    self.bgfLB.layer.masksToBounds = YES;
    self.bgfLB.layer.borderWidth = 1;
    self.bgfLB.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;
    
    _myTableView.backgroundColor = [UIColor clearColor];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    _myTableView.estimatedRowHeight = 90;
    _myTableView.rowHeight = UITableViewAutomaticDimension;
    _myTableView.tableFooterView = [UIView new];
    [_myTableView registerNib:[UINib nibWithNibName:@"CSmyGrandTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSmyGrandTableViewCell"];
    
    _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH ,KHEIGHT) image:imgname(@"tableviewPlaseholder") viewClick:^{
        [self->_myTableView.mj_header beginRefreshing];
    }];
    
    adjustInset(_myTableView);
    if (strIsEmpty(_titleStr)) {
        _titleStr = [singlTool shareSingTool].titlsr;
    }
    [self showbgview];
    [self request];
    KMyLog(@"带的过来的导航条标题是---- %@ ----",_titleStr);
    // Do any additional setup after loading the view from its nib.
}

-(void)request{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"type"] = _type; //mon 按月查询,day 按天查询
    //获取当前时间日期
    NSDate *date = [NSDate date];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr;
    dateStr = [format stringFromDate:date];
    self.dateStr = dateStr;
    
    if ([self.upTimeLb.text isEqualToString:@""] && [self.dowTimeLb.text isEqualToString:@""]) {
        param[@"begTime"] = self.dateStr;
        param[@"endTime"] = self.dateStr;
    } else {
        param[@"begTime"] = _upTimeLb.text;
        param[@"endTime"] = _dowTimeLb.text;
    }
    
    [_mydateSource removeAllObjects];
    [self.myTableView reloadData];
    self.totleincomLB.text = [NSString stringWithFormat:@"￥"];
    kWeakSelf;
    NSString *str = myGrantsList;
    KMyLog(@"dsfd1%@",_titleStr);
    
    if ([self.titleStr isEqualToString:@"补助金"]) {
        self.totleincomLB.text =[NSString stringWithFormat:@"￥0"];
    }
    
    [NetWorkTool POST:str param:param success:^(id dic) {
        KMyLog(@"补助金列表 %@ 补助金列表", dic);
        for (NSMutableDictionary *grandDic in [dic objectForKey:@"data"]) {
            CSmyGrandListModel *model = [[CSmyGrandListModel alloc] init];
            [model setValuesForKeysWithDictionary: grandDic];
            [weakSelf.mydateSource addObject:model];
            
            NSString *priceStr = model.compensationPrice;
            NSNumber *priceNum = @([priceStr integerValue]);
            [self.numberArr addObject:priceNum];
        }
        NSNumber *sum = [self.numberArr valueForKeyPath:@"@sum.self"];
        NSString *numberS = [NSString stringWithFormat:@"%@",sum];
        NSString *numberSt = [NSString stringWithFormat:@"%.2f", [numberS floatValue]];
        self.totleincomLB.text = [NSString stringWithFormat:@"%@%@",@"￥",numberSt];
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 76;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSmyGrandTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSmyGrandTableViewCell" forIndexPath:indexPath];
    [_myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; //取消cell的下划线
    cell.selectionStyle = UITableViewCellSelectionStyleNone; //取消cell点击状态
    cell.orderLb.textColor = [UIColor colorWithHexString:@"#70BE68"];
    
    CSmyGrandListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    cell.orderLb.text = [NSString stringWithFormat:@"订单编号: %@",model.Id];
    
    if ([model.state isEqualToString:@"1"]) {
        cell.stateLab.text = @"未审核";
    } else if ([model.state isEqualToString:@"2"]) {
        cell.stateLab.text = @"省级未通过";
    } else if ([model.state isEqualToString:@"3"]) {
        cell.stateLab.text = @"省级已通过";
    } else if ([model.state isEqualToString:@"4"]) {
        cell.stateLab.text = @"平台未通过";
    } else if ([model.state isEqualToString:@"5"]) {
        cell.stateLab.text = @"平台已通过";
    } else if ([model.state isEqualToString:@"6"]) {
        cell.stateLab.text = @"已到账";
    }
    
    NSString *moneyS = model.compensationPrice;
    NSString *moneySt = [NSString stringWithFormat:@"+%@", moneyS];
    NSString *moneyStr = [NSString stringWithFormat:@"%.2f", [moneySt floatValue]];
    cell.moneyLb.text = [NSString stringWithFormat:@"%@%@", @"+", moneyStr];
    
    //cell.timeLB.text = [NSString stringWithFormat:@"%@",[model.payTime substringToIndex:16]];
    cell.timeLB.text = [NSString stringWithFormat:@"%@",[model.createTime substringToIndex:16]];

    cell.timeLB.textColor = K333333;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //取消选中效果
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    CSmyGrandListModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    //补助金详情
    ZYGrandAccDetailViewController *detail = [[ZYGrandAccDetailViewController alloc] init];
    detail.orderIdStr = model.Id;       //订单编号
    detail.anzhuTime = model.azTime;    //安装时间
    detail.sunhuTime = model.shTime;    //损坏时间
    detail.pjpinlei = model.typeName;   //配件品类
    detail.pjname = model.name;         //配件名称
    detail.state = model.state;//审核结果
    detail.reason = model.reason;//原因
    detail.youxiaoqi = [NSString stringWithFormat:@"%@%@%@", model.productionDate, @" 至 ", model.termValidity];
    detail.baozhiqi = [NSString stringWithFormat:@"%@%@", model.install, @"个月"]; //保质期
    [self.navigationController pushViewController:detail animated:YES];
}

- (IBAction)seleTimeAction:(UIButton *)sender {
    
    //[[UIApplication sharedApplication].keyWindow addSubview:_bgview];
    [self.view addSubview:_bgview];
}

-(void)showbgview{
    
    //self.bgview = [[UIView alloc]initWithFrame:CGRectMake(_myTableView.frame.origin.x, _myTableView.frame.origin.y, KWIDTH, KHEIGHT-_myTableView.frame.origin.y)];
    self.bgview = [[UIView alloc]initWithFrame:CGRectMake(_myTableView.frame.origin.x, kNaviHeight +80, KWIDTH, KHEIGHT-_myTableView.frame.origin.y)];
    self.bgview.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whithview = [[UIView alloc]initWithFrame:CGRectMake(0, -20, KWIDTH, 255)];
    whithview.backgroundColor = [UIColor whiteColor];
    [self.bgview  addSubview:whithview];
    
    PGDatePickManager *datePickManager = [[PGDatePickManager alloc]init];
    datePickManager.isShadeBackground = true;
    PGDatePicker *datePicker = datePickManager.datePicker;
    datePicker.delegate = self;
    //    datePicker.datePickerType = PGDatePickerTypeVertical;
    datePicker.isHiddenMiddleText = false;
    datePicker.maximumDate = [NSDate date];
    datePicker.autoSelected = true;
    datePicker.datePickerMode = PGDatePickerModeDate;
    [whithview addSubview:datePickManager.view];
    //    datePickManager.view.frame = CGRectMake(0, 50, KWIDTH, KHEIGHT);
    datePickManager.view.frame = CGRectMake(0, 50, KWIDTH, 150);
    [self addChildViewController:datePickManager];
    
    UIButton *fifishBUt = [UIButton  buttonWithType:(UIButtonTypeCustom)];
    [fifishBUt setTitle:@"完成" forState:(UIControlStateNormal)];
    [whithview addSubview:fifishBUt];
    [fifishBUt setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    [fifishBUt addTarget:self action:@selector(finishAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [fifishBUt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.right.offset(-16);
        make.width.offset(80);
        make.height.offset(42);
    }];
    
    self.lefttimeLB = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.lefttimeLB.frame =CGRectMake(0, 42, KWIDTH/2, 34 );
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    [self.lefttimeLB setTitle: [[self getCurrentTimes] substringToIndex:10] forState:(UIControlStateNormal)];
    [whithview  addSubview:self.lefttimeLB];
    [self.lefttimeLB addTarget:self action:@selector(leftActionsss) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.righttimeLB = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.righttimeLB.frame =CGRectMake(KWIDTH/2, 42, KWIDTH/2, 34 );
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
    [self.righttimeLB setTitle: [[self getCurrentTimes] substringToIndex:10] forState:(UIControlStateNormal)];
    [whithview  addSubview:self.righttimeLB];
    [self.righttimeLB addTarget:self action:@selector(righttimeLBActionsss) forControlEvents:(UIControlEventTouchUpInside)];
    
    UILabel *zhi = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH/2-8, 42, 16, 34)];
    zhi.textAlignment = NSTextAlignmentCenter;
    [whithview  addSubview:zhi];
    zhi.text = @"至";
    zhi.font = FontSize(14);
    
    kWeakSelf;
    self.leftLB = [[UILabel alloc]init];
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    [whithview addSubview:self.leftLB];
    [_leftLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.lefttimeLB.mas_bottom).offset(8);
        make.width.offset(130);
        make.height.offset(1);
        
        make.centerX.mas_equalTo(weakSelf.lefttimeLB.mas_centerX).offset(0);
    }];
    self.rightLB = [[UILabel alloc]init];
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [whithview addSubview:self.rightLB];
    [_rightLB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weakSelf.righttimeLB.mas_bottom).offset(8);
        make.width.offset(130);
        make.height.offset(1);
        make.centerX.mas_equalTo(weakSelf.righttimeLB.mas_centerX).offset(0);
    }];
}

-(void)leftActionsss{
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
    self.isleft = YES;
}

-(void)righttimeLBActionsss{
    self.isleft = NO;
    
    self.rightLB.backgroundColor = [UIColor colorWithHexString:@"#FFCA9F61"];
    
    [self.righttimeLB setTitleColor:[UIColor colorWithHexString:@"#FFCA9F61"] forState:(UIControlStateNormal)];
    
    self.leftLB.backgroundColor = [UIColor colorWithHexString:@"#FF999999"];
    [self.lefttimeLB setTitleColor:[UIColor colorWithHexString:@"#FF666666"] forState:(UIControlStateNormal)];
}

-(NSString*)getCurrentTimes{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];
    //----------将nsdate按formatter格式转成nsstring
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    NSLog(@"currentTimeString =  %@",currentTimeString);
    return currentTimeString;
}

-(void)finishAction:(UIButton *)buf{
    [self.bgview removeFromSuperview];
    
    [self.seleButtn setTitle:@"" forState:(UIControlStateNormal)];
    
    self.upTimeLb.text = self.lefttimeLB.titleLabel.text;
    self.dowTimeLb.text = self.righttimeLB.titleLabel.text;
    [self request];
}

#pragma PGDatePickerDelegate
- (void)datePicker:(PGDatePicker *)datePicker didSelectDate:(NSDateComponents *)dateComponents {
    NSLog(@"dateComponents = %@", dateComponents);
    //拼接 时分秒
    //NSString *time = [NSString stringWithFormat:@"%ld-%ld-%ld %ld:%ld:%ld",dateComponents.year,dateComponents.month,dateComponents.day,dateComponents.hour,dateComponents.minute,dateComponents.second];
    
    //不拼接 时分秒
    NSString *time = [NSString stringWithFormat:@"%ld-%ld-%ld",dateComponents.year,dateComponents.month,dateComponents.day];
    
    if (!strIsEmpty(time)) {
        
        NSString *ster = [[time substringFromIndex:5] substringToIndex:2];
        NSInteger ind = [ster integerValue];
        if (ind < 10) {
            NSMutableString* str1=[[NSMutableString alloc]initWithString:time];//存在堆区，可变字符串
            [str1 insertString:@"0"atIndex:5];//把一个字符串插入另一个字符串中的某一个位置
            KMyLog(@"输出一下这个数据 *** %@ ***",str1);
            time = str1;
        }
        
        if (time.length == 9) {
            NSMutableString* ind2=[[NSMutableString alloc]initWithString:time];//存在堆区，可变字符串
            [ind2 insertString:@"0"atIndex:8];//把一个字符串插入另一个字符串中的某一个位置
            KMyLog(@"打印一下这个数据 !!! %@ !!!",ind2);
            time = ind2;
        }
    }
    
    if (self.isleft) {
        [self.lefttimeLB setTitle:time forState:(UIControlStateNormal)];
    }else{
        [self.righttimeLB setTitle:time forState:(UIControlStateNormal)];
    }
}

#pragma mark - 将某个时间转化成 时间戳
- (NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format]; //(@"YYYY-MM-dd hh:mm:ss") ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate* date = [formatter dateFromString:formatTime]; //------------将字符串按formatter转成nsdate
    
    //时间转时间戳的方法:
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    NSLog(@"将某个时间转化成 时间戳&&&&&&&timeSp:%ld",(long)timeSp); //时间戳的值
    return timeSp;
}

@end
