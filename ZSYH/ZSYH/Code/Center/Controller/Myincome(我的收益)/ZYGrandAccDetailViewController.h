//
//  ZYGrandAccDetailViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZYGrandAccDetailViewController : BaseViewController
@property (nonatomic, strong) NSString *orderIdStr; //订单编号
@property (nonatomic, strong) NSString *anzhuTime;  //安装时间
@property (nonatomic, strong) NSString *sunhuTime;  //损坏时间
@property (nonatomic, strong) NSString *pjpinlei;   //配件品类
@property (nonatomic, strong) NSString *pjname;     //配件名称
@property (nonatomic, strong) NSString *youxiaoqi;  //有效期
@property (nonatomic, strong) NSString *baozhiqi;   //保质期
@property (nonatomic, strong) NSString *state;   //审核结果
@property (nonatomic, strong) NSString *reason;   //原因
@end

NS_ASSUME_NONNULL_END
