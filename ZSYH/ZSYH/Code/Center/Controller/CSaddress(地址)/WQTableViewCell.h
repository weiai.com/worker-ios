//
//  WQTableViewCell.h
//  APPStart
//
//  Created by 魏堰青 on 2019/1/21.
//  Copyright © 2019 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WQTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *mytf;
@property(nonatomic,copy)void (^myblcok)(NSString *str);
@end

NS_ASSUME_NONNULL_END
