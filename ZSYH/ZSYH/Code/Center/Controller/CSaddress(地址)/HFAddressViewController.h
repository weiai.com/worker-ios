//
//  HFAddressViewController.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "BaseViewController.h"
@class HFAddressModel;
NS_ASSUME_NONNULL_BEGIN

@interface HFAddressViewController : BaseViewController

@property (nonatomic, copy) void(^chooseAddressBlock)(HFAddressModel *model);

@end

NS_ASSUME_NONNULL_END
