//
//  TYJTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "TYJTableViewCell.h"

@implementation TYJTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.editBut.layer.borderWidth = 1;
    self.editBut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    self.editBut.layer.masksToBounds = YES;
    self.editBut.layer.cornerRadius = 4;
    self.editBut.backgroundColor = [UIColor whiteColor];
    self.bglable.layer.masksToBounds = YES;
    self.bglable.layer.cornerRadius = 4;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)bianjiaAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(@"");
    }
}

@end
