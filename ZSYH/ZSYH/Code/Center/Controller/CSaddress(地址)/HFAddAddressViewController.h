//
//  HFAddAddressViewController.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "BaseViewController.h"
#import "HFAddressModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface HFAddAddressViewController : BaseViewController

@property (nonatomic, strong) HFAddressModel *model;

@end

NS_ASSUME_NONNULL_END
