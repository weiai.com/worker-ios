//
//  HFAddAddressViewController.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFAddAddressViewController.h"
#import "HFTiXianTFCell.h"
#import "HFDefaultAddressCell.h"
//#import "WqGaoDeLocationViewController.h"
#import "BaseNavViewController.h" //
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h" //
#import "WQTableViewCell.h"
#import "MOFSPickerManager.h" //
#import "NetLoadAreaPickerViewController.h"
#import "CSquyumodel.h"

@interface HFAddAddressViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSString *_name;
    NSString *_mobile;
    NSString *_area;
    NSString *_address;
    NSInteger _isDefault;
    NSString *_WQaddress;
    NSString *shengshiqu;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *placeholderArray;
@property (nonatomic, strong) NSArray *contentArray;
@property (nonatomic, assign) BOOL  iscansele;
@property (nonatomic, strong) NSMutableArray *muarr;

@end

@implementation HFAddAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    _titleArray = @[@"姓名:", @"联系方式:", @"所在地区:", @"详细地址:"];
    //_placeholderArray = @[@"输入姓名", @"输入电话", @"输入所在地区(如河南省郑州市金水区)", @"输入详细地址"];
    _placeholderArray = @[@"请输入姓名", @"请输入电话", @"请选择所在地区", @"请输入详细地址"];
    
    self.mydateSource  = [NSMutableArray arrayWithCapacity:1];
    //self.delearr = [NSMutableArray arrayWithCapacity:1];
    
    if (_muarr.count >0) {
        self.mydateSource =_muarr;
        [_tableView reloadData];
    }
    
    [self configNavi];
    [self.view addSubview:self.tableView];
    [self configSubmitBtn];
}

- (void)configNavi {
    if (self.model) {
        self.title = @"编辑";
        _name = _model.user_name;
        _mobile = _model.phone;
        _area = _model.addressName;
        _address = _model.address;
        _contentArray = @[_name, _mobile, _area, _address];
        shengshiqu = [NSString stringWithFormat:@"%@-%@-%@",_model.province,_model.city,_model.area];
    } else {
        self.title = @"添加地址";
        _contentArray = @[@"", @"", @"", @""];
    }
}

- (void)configSubmitBtn {
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(30, ScreenH-150, ScreenW-60, 48);
    [btn setTitle:@"确认" forState:(UIControlStateNormal)];
    [btn setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    btn.titleLabel.textColor = [UIColor colorWithHexString:@"ffffff"];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:btn];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 4;
    btn.timeInterval = 2;
    btn.isIgnore = YES;
    [btn addTarget:self action:@selector(submitBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
}

- (void)requestAddAddressData {
    
    //    if (strIsEmpty(_name) || strIsEmpty(_mobile) || strIsEmpty(shengshiqu) || strIsEmpty(_address)) {
    //        ShowToastWithText(@"请填写全部信息");
    //        return;
    //    }
    
    if (strIsEmpty(_name)) {
        ShowToastWithText(@"请填写姓名");
        return;
    }
    
    if (strIsEmpty(_mobile)) {
        ShowToastWithText(@"请填写联系方式");
        return;
    }
    
    if (strIsEmpty(shengshiqu)) {
        ShowToastWithText(@"请选择所在地区");
        return;
    }
    
    if (strIsEmpty(_address)) {
        ShowToastWithText(@"请填写详细地址");
        return;
    }
    
    if(![_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
        ShowToastWithText(@"名字不能全是空格");
        return;
    }
    
    if(![_address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
        ShowToastWithText(@"详细地址不能全是空格");
        return;
    }
    
    //去除字符串中空格
    _mobile = [_mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_mobile]) {
        ShowToastWithText(@"请输入正确手机号");
        return;
    }
   //http://192.168.2.114:8096/repAddress/Addaddress?address={"area":"110101","address":"address","province":"110000","city":"110100","is_default":"1"}
    
    //    NSDictionary *dic = @{
    //                          @"consignee":safe(_name),
    //                          @"mobile":safe(_mobile),
    //                          @"region_name":safe(_area),
    //                          @"building_name":safe(_address),
    //                          @"is_default":@(_isDefault)
    //                          };
    NSArray  *array = [shengshiqu componentsSeparatedByString:@"-"];
    
    NSDictionary *dic = @{
                          @"area":array[2],
                          @"address":safe(_address),
                          @"province":array[0],
                          @"city":array[1],
                          @"is_default":@(_isDefault),
                          @"user_name":_name,
                          @"phone":_mobile
                          };
    kWeakSelf;
    NSString *json = [HFTools toJSONString:dic];
    NSDictionary *mudci = @{@"address":json};
    
    [NetWorkTool POST:CmyAddaddress param:mudci success:^(id dic) {
        NSLog(@"%@", dic);
        [weakSelf leftClick];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)requestEditAddressData {
    //if (strIsEmpty(_name) || strIsEmpty(_mobile) || strIsEmpty(shengshiqu) || strIsEmpty(_address)) {
        //ShowToastWithText(@"请填写全部信息");
        //return;
    //}
    
    if (strIsEmpty(_name)) {
        ShowToastWithText(@"请填写收货人姓名");
        return;
    }
    
    if (strIsEmpty(_mobile)) {
        ShowToastWithText(@"请填写收货人电话");
        return;
    }
    
    if (strIsEmpty(shengshiqu)) {
        ShowToastWithText(@"请选择收货人省市区");
        return;
    }
    
    if (strIsEmpty(_address)) {
        ShowToastWithText(@"请填写收货人详细地址");
        return;
    }
    
    if(![_name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
        ShowToastWithText(@"名字不能全是空格");
        return;
    }
    
    if(![_address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length) {
        ShowToastWithText(@"详细地址不能全是空格");
        return;
    }
    
    //去除字符串中空格
    _mobile = [_mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_mobile]) {
        ShowToastWithText(@"请输入正确手机号");
        return;
    }
    
    NSArray  *array = [shengshiqu componentsSeparatedByString:@"-"];
    NSDictionary *dic = @{
                          @"area":array[2],
                          @"address":safe(_address),
                          @"province":array[0],
                          @"city":array[1],
                          @"is_default":@(_isDefault),
                          @"user_name":NOTNIL(_name),
                          @"phone":NOTNIL(_mobile),
                          @"id":NOTNIL(_model.id)
                          };
    
    NSString *json = [HFTools toJSONString:dic];
    NSDictionary *dicccc = @{@"address":json};
    kWeakSelf;
    [NetWorkTool POST:cmyeditaddress param:[dicccc mutableCopy] success:^(id dic) {
        NSLog(@"____%@", dic);
        [weakSelf leftClick];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark -- click点击事件
- (void)submitBtnClick:(UIButton *)sender {
    NSLog(@"提交");
    if (self.model) {
        [self requestEditAddressData];
    } else {
        [self requestAddAddressData];
    }
}

#pragma mark -- tableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HFTiXianTFCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HFTiXianTFCell" forIndexPath:indexPath];
    cell.jinruimage.hidden = YES;
    if (indexPath.row ==2 ) {
        cell.contentTF.tag = 777;
        cell.contentTF.userInteractionEnabled = NO;
        cell.selebut.hidden = NO;
        cell.jinruimage.hidden = NO;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell configDataWithTitle:_titleArray[indexPath.row] placeHolder:_placeholderArray[indexPath.row] content:_contentArray[indexPath.row]];
    
    if (indexPath.row == 0) {
        cell.contentTF.delegate = self;
        [cell.contentTF addTarget:self action:@selector(nametextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
    if (indexPath.row == 1) {
        cell.contentTF.delegate = self;
        cell.contentTF.keyboardType = UIKeyboardTypeNumberPad;
        [cell.contentTF addTarget:self action:@selector(phonetextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
    if (indexPath.row == 2 ) {
        if (!strIsEmpty(_WQaddress)) {
            cell.contentTF.text = _WQaddress;
            self->_area = _WQaddress;
        }
    }
    
    if (indexPath.row == 3) {
        cell.contentTF.delegate = self;
        [cell.contentTF addTarget:self action:@selector(addresstextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
    cell.inputContentBlock = ^(NSString * _Nonnull content) {
        switch (indexPath.row) {
            case 0:
                self->_name = content;
                break;
            case 1:
                self->_mobile = content;
                break;
            case 2:
                [self locationAction];
                break;
            case 3:
                self->_address = content;
                break;
            default:
                break;
        }
    };
    return cell;
}

//姓名
- (void)nametextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 5;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

//联系方式
- (void)phonetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 11;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

//联系方式
- (void)addresstextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 20;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

#pragma mark -- 地区选择
-(void)locationAction{
    NetLoadAreaPickerViewController * loadAreaVC = [[NetLoadAreaPickerViewController alloc]init];
    kWeakSelf;
    loadAreaVC.selectedBlock = ^(NetLoadAreaModel *areaModel) {
        KMyLog(@"选中的地址%@",areaModel);
        CSquyumodel *model = [[CSquyumodel alloc]init];
        model.area = [NSString stringWithFormat:@"%@%@%@", areaModel.provinceName, areaModel.cityName, areaModel.districtName];
        model.idstr = areaModel.districtCode;
        model.isdelde = @"0";
        self->_WQaddress = model.area;
        self->shengshiqu = [NSString stringWithFormat:@"%@-%@-%@",areaModel.provinceCode,areaModel.cityCode,areaModel.districtCode];
        [weakSelf.tableView reloadData];
    };
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        loadAreaVC.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    } else {
        loadAreaVC.modalPresentationStyle=UIModalPresentationCurrentContext;
    }
    [self presentViewController:loadAreaVC animated:NO completion:^{
        
    }];
    return;
}

-(void)requestperson{
    self.iscansele = NO;
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        [self.mydateSource removeAllObjects];
        
        NSArray *atttt = [[dic objectForKey:@"data"] objectForKey:@"area"];
        for (NSMutableDictionary *mu in atttt) {
            CSquyumodel *mdoe = [[CSquyumodel alloc]init];
            [mdoe setValuesForKeysWithDictionary:mu];
            mdoe.idstr = [mu objectForKey:@"id"];
            mdoe.isdelde = @"0";
            [self.mydateSource addObject:mdoe];
        }
        
        NSMutableArray* temp = [[NSMutableArray alloc] init];
        NSMutableArray* tempstr = [[NSMutableArray alloc] init];
        
        for ( CSquyumodel *mdoe in self.mydateSource) {
            if (![tempstr containsObject:mdoe.idstr]){
                [tempstr addObject:mdoe.idstr];
                [temp addObject:mdoe];
            }
        }
        self.mydateSource = temp;
        [self.tableView reloadData];
        
        if (weakSelf.mydateSource.count == 5) {
            weakSelf.tableView.tableFooterView = [UIView new];
        }else{
            //self.tableView.tableFooterView = self.tabfootview;
        }
        self.iscansele = YES;
        //self.tabheaderbut.userInteractionEnabled = YES;
    } other:^(id dic) {
        self.iscansele = YES;
        
    } fail:^(NSError *error) {
        self.iscansele = YES;
        
    } needUser:YES];
}

//是否开启定位
- (BOOL)isLocationServiceOpen {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        return NO;
    } else
        return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _titleArray.count) {
        HFDefaultAddressCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.selectBtn.selected = !cell.selectBtn.selected;
        _isDefault = cell.selectBtn.selected?1:0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _titleArray.count) {
        return 54;
    }
    return 49;
}

#pragma mark -- 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight, ScreenW, ScreenH-kNaviHeight) style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
        if (iOS11) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 48;
        [_tableView registerNib:[UINib nibWithNibName:@"HFTiXianTFCell" bundle:nil] forCellReuseIdentifier:@"HFTiXianTFCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"HFDefaultAddressCell" bundle:nil] forCellReuseIdentifier:@"HFDefaultAddressCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"WQTableViewCell" bundle:nil] forCellReuseIdentifier:@"WQTableViewCell"];
    }
    return _tableView;
}

- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

@end
