//
//  TYJTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TYJTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *isdeforeLB;
@property (weak, nonatomic) IBOutlet UILabel *contentAddressLB;
@property (weak, nonatomic) IBOutlet UIButton *editBut;
@property(nonatomic,copy)void (^myblock)(NSString *str);
@property (weak, nonatomic) IBOutlet UILabel *bglable;

@end

NS_ASSUME_NONNULL_END
