//
//  HFAddressViewController.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFAddressViewController.h"
#import "HFAddressTableViewCell.h"
#import "HFAddAddressViewController.h"
#import "HFAddressModel.h"
#import "TYJTableViewCell.h"

@interface HFAddressViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UIView *showbgView;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end

@implementation HFAddressViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self.view addSubview:self.tableView];
    [self configNavi];
    
    [self showloadingview];
}

//我的地址列表 数据请求
- (void)requestData {
    
    [self.dataSource removeAllObjects];
    kWeakSelf;
    [NetWorkTool POST:CmyaddressList param:nil success:^(id dic) {
        NSLog(@"%@", dic);
        
        self.dataSource = [HFAddressModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [weakSelf.tableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)configNavi {
    [self.leftbutton setImage:imgname(@"mblack") forState:(UIControlStateNormal)];
    self.title = @"我的地址";
    //[self.rightbutton setTitle:@"添加" forState:UIControlStateNormal];
    [self.rightbutton setImage:imgname(@"C添加") forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
}

#pragma mark -- click点击事件
- (void)rightClick {
    HFAddAddressViewController *addAddress = [[HFAddAddressViewController alloc] init];
    [self.navigationController pushViewController:addAddress animated:YES];
}

#pragma mark -- tableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HFAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HFAddressTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    HFAddressModel *model = [self.dataSource objectAtIndex:indexPath.row];
    
    [cell setModel:model];
    
    kWeakSelf;
    cell.editOrDelBlock = ^(NSInteger index) {
        if (index == 0) { // 编辑
            KMyLog(@"您点击了我的地址 编辑 按钮");
            HFAddAddressViewController *edit = [[HFAddAddressViewController alloc] init];
            edit.model = model;
            [weakSelf.navigationController pushViewController:edit animated:YES];
        } else if (index == 1) { // 删除
            KMyLog(@"您点击了我的地址 删除 按钮 %@", model.id);
            //[weakSelf requestData];
            [weakSelf deleWithModel:model];
        }else if (index == 2) { // 默认地址
            KMyLog(@"您点击了我的地址 设置默认 按钮");
            [weakSelf requestData];
        }
    };
    return cell;
}

- (void)deleWithModel:(HFAddressModel *)model {
    kWeakSelf;
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"确定删除?" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        KMyLog(@"取消");
    }]];
    
    [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        KMyLog(@"确定");
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        dic[@"addressId"] = model.id;
        [NetWorkTool POST:Cmyadeleaddress param:dic success:^(id dic) {
            
            //ShowToastWithText(@"删除成功");
            [weakSelf requestData];
            [weakSelf.tableView reloadData];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }]];
    [self presentViewController:alertCon animated:YES completion:nil];
    
    //[[UIApplication sharedApplication].keyWindow addSubview:self.showbgView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.chooseAddressBlock) {
        HFAddressModel *model = [self.dataSource objectAtIndex:indexPath.row];
        self.chooseAddressBlock(model);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)showloadingview{
    
    self.showbgView = [[UIView alloc]init];
    self.showbgView.frame = self.view.bounds;
    self.showbgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.showbgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(239);
        make.width.offset(KWIDTH - 44);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-30, 34, 60, 60)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 30;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 1461"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"";
    
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, KWIDTH-20, 34)];
    [whiteBGView addSubview:topLable];
    topLable.font = FontSize(16);
    topLable.numberOfLines = 0;
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.textAlignment =  NSTextAlignmentCenter;
    topLable.text = @"确认删除?";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"";
    
    UIButton *endButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [endButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [endButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [endButton setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    endButton.layer.masksToBounds = YES;
    endButton.layer.cornerRadius = 4;
    [endButton addTarget:self action:@selector(quxiaoBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:endButton];
    [endButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.offset(28);
        make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.bottom.offset(-19);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"确认" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[[UIColor colorWithHexString:@"#F2F2F2"] colorWithAlphaComponent:0.5]];
    iKnowBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    iKnowBut.layer.borderWidth = 1;
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(yesBtnActionWithModel:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)quxiaoBtnAction:(UIButton *)but{
    KMyLog(@"取消");
    [self.showbgView removeFromSuperview];
}

-(void)yesBtnActionWithModel:(HFAddressModel *)model {
    KMyLog(@"确定");
    
    //    HFAddressTableViewCell *quYuCell = [[HFAddressTableViewCell alloc] init];
    //    NSIndexPath *indexpath = quYuCell.indexPath;
    //    HFAddressModel *model = self.mydateSource[indexpath.row];
    
    //[self deleteBtnActionWithModel:model];
    //删除数据
    [self deleteAddressData:model];
    //    if (self.mydateSource.count == 1) {
    //        ShowToastWithText(@"不能删除最后一个接单区域");
    //        return;
    //    }
    //
    //    CSquyuTableViewCell *quYuCell = [[CSquyuTableViewCell alloc] init];
    //    NSIndexPath *indexpath = quYuCell.indexPath;
    //    CSquyumodel *model = self.mydateSource[indexpath.row];
    //
    //    //[self deleteBtnActionWithModel:model];
    //
    //    //删除数据
    //    [self deleteDateWithModel:model];
    //
    //    [_showbgView removeFromSuperview];
    //
    //    /*//    [self.navigationController popViewControllerAnimated:YES];
    //     if ([self.mymodel.isprivate_work integerValue]  == 1) {
    //     NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    //     mudic[@"appId"] = self.mymodel.Id;
    //     [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
    //     [self.navigationController popViewControllerAnimated:YES];
    //
    //     } other:^(id dic) {
    //
    //     } fail:^(NSError *error) {
    //
    //     } needUser:YES];
    //
    //     KMyLog(@"rightbuttobuttoaction");
    //     } else {
    //     [self.navigationController popViewControllerAnimated:YES];
    //     }
    //
    //     //    NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
    //     //    mudic[@"appId"] = _mymodel.Id;
    //     //    [NetWorkTool POST:Listjujue param:mudic success:^(id dic) {
    //     //        [self.navigationController popViewControllerAnimated:YES];
    //     //
    //     //    } other:^(id dic) {
    //     //
    //     //    } fail:^(NSError *error) {
    //     //
    //     //    } needUser:YES];
    //     //
    //     //    KMyLog(@"rightbuttobuttoaction");*/
}

//我的地址 删除地址 数据请求(原先的)
- (void)deleteAddressData:(HFAddressModel *)model {
    kWeakSelf;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"addressId"] = model.id;
    [NetWorkTool POST:Cmyadeleaddress param:dic success:^(id dic) {
        
        //ShowToastWithText(@"删除成功");
        [weakSelf requestData];
        [weakSelf.tableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark -- 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight+5, ScreenW, ScreenH-kNaviHeight-5) style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
        if (iOS11) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _tableView.tableFooterView = [UIView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 120;
        [_tableView registerNib:[UINib nibWithNibName:@"HFAddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"HFAddressTableViewCell"];
        _tableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_tableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [self->_tableView.mj_header beginRefreshing];
        }];
    }
    return _tableView;
}

- (NSMutableArray *)dataSource {
    if (_dataSource == nil) {
        _dataSource = [NSMutableArray arrayWithCapacity:1];
    }
    return _dataSource;
}

@end
