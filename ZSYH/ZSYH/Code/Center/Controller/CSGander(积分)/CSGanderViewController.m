//
//  CSGanderViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSGanderViewController.h"
#import "CSGanderdrHomeTableViewCell.h"

@interface CSGanderViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *mytableView;
@property(nonatomic,strong)NSMutableArray *dataSource;
@property (weak, nonatomic) IBOutlet UILabel *bgLb;

@end

@implementation CSGanderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"积分";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.mytableView.dataSource = self;
    self.mytableView.delegate = self;
    [self.mytableView registerNib:[UINib nibWithNibName:@"CSGanderdrHomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSGanderdrHomeTableViewCell"];
    self.mytableView.tableFooterView = [UIView new];
    self.dataSource = [NSMutableArray arrayWithCapacity:1];
    [self.dataSource addObject:@"积分规则"];
    [self.dataSource addObject:@"历史积分情况"];

    
    self.bgLb.layer.borderWidth = 2;
    self.bgLb.layer.borderColor = [UIColor whiteColor].CGColor;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSGanderdrHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSGanderdrHomeTableViewCell" forIndexPath:indexPath];
    cell.leftLB.text = [NSString stringWithFormat:@"%@",[_dataSource safeObjectAtIndex:indexPath.row]];
    return cell;
    
}

@end

