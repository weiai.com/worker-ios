//
//  CSlookMailCimmitViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSlookMailCimmitViewController.h"

@interface CSlookMailCimmitViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagev;
@property (weak, nonatomic) IBOutlet UILabel *cimLb;
@property (weak, nonatomic) IBOutlet UILabel *cimConLb;

@end

@implementation CSlookMailCimmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.title = @"查看评价";
    [self request];
}
-(void)request{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(_orderid);
    [NetWorkTool POST:lookcimmMail param:param success:^(id dic) {
        KMyLog(@"dddddd%@",dic);
        NSDictionary *mydic = [dic objectForKey:@"data"];
        if ([[mydic objectForKey:@"evaluate_score"] integerValue]  == 0) {
            //好评
            self.imagev.image = imgname(@"haopingpj");
            self.cimLb.text = @"好评";
        }else if ([[mydic objectForKey:@"evaluate_score"] integerValue]  == 1) {
            //中评
            self.imagev.image = imgname(@"zhongpingpj");
            self.cimLb.text = @"中评";
        }else if ([[mydic objectForKey:@"evaluate_score"] integerValue]  == 2) {
            //中评
            self.imagev.image = imgname(@"chapingpj");
            self.cimLb.text = @"差评";
        }
        
        self.cimConLb.text = [mydic objectForKey:@"evaluate_content"];
                             
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
    
    
}

@end
