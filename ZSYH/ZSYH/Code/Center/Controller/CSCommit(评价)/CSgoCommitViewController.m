//
//  CSgoCommitViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSgoCommitViewController.h"
#import "SYBigImage.h"
#import "XWScanImage.h"
#import <AVFoundation/AVFoundation.h>

@interface CSgoCommitViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)UIImageView *myimage;
@property(nonatomic,strong)NSMutableArray *imageArr;//
@property (weak, nonatomic) IBOutlet UIImageView *proImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLB;
@property (weak, nonatomic) IBOutlet UILabel *plaseLB;
@property (weak, nonatomic) IBOutlet UITextView *mytv;
@property (weak, nonatomic) IBOutlet UILabel *bgImageLB;
@property(nonatomic,strong)NSString *type;//
@property (weak, nonatomic) IBOutlet UIButton *haopingbut;
@property (weak, nonatomic) IBOutlet UIButton *zhongpingbut;
@property (weak, nonatomic) IBOutlet UIButton *chapingbut;

@end

@implementation CSgoCommitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mytv.delegate = self;
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"addpicimage");
    //[self.imageArr addObject:_myimage];
    //[self showinageWirhArr:_imageArr];
    self.type = @"0";
    
    self.bgImageLB.height = 0.01;
    
  
}

- (IBAction)haoping:(UIButton *)sender {
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [sender setImage:imgname(@"haopingpj") forState:(UIControlStateNormal)];
    self.type = @"0";
    [self.zhongpingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.zhongpingbut setImage:imgname(@"zhongpingpj1") forState:(UIControlStateNormal)];
    [self.chapingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.chapingbut setImage:imgname(@"chapingpj1") forState:(UIControlStateNormal)];
}

- (IBAction)zhongping:(UIButton *)sender {
    self.type = @"1";
    
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [sender setImage:imgname(@"zhongpingpj") forState:(UIControlStateNormal)];
    
    [self.haopingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.haopingbut setImage:imgname(@"haopingpj1") forState:(UIControlStateNormal)];
    [self.chapingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.chapingbut setImage:imgname(@"chapingpj1") forState:(UIControlStateNormal)];
}

- (IBAction)cahping:(UIButton *)sender {
    self.type = @"2";
    [sender setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    [sender setImage:imgname(@"chapingpj") forState:(UIControlStateNormal)];
    
    [self.haopingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.haopingbut setImage:imgname(@"haopingpj1") forState:(UIControlStateNormal)];
    [self.zhongpingbut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [self.zhongpingbut setImage:imgname(@"zhongpingpj1") forState:(UIControlStateNormal)];
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
        
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.bgImageLB removeAllSubviews];
    //self.bottomf = 0;
    CGFloat   www = (KWIDTH -(8*7))/4;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(8+(www+8)*i, 0, www, www);
        [self.bgImageLB addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 4) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        } else {
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind]  removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

- (IBAction)tijaoping:(UIButton *)sender {
    
    if (self.mytv.text.length < 5) {
        ShowToastWithText(@"请输入至少5个字符的评价");
        return;
    }
    NSMutableDictionary *mudci = [NSMutableDictionary dictionaryWithCapacity:1];
    mudci[@"orderNum"] = NOTNIL(_headerModel.orderNumber);
    mudci[@"orderId"] = NOTNIL(_headerModel.Id);
    mudci[@"score"] =  NOTNIL(_type);
    mudci[@"content"] = NOTNIL(_mytv.text);
    
    [NetWorkTool POST:upcommit param:mudci success:^(id dic) {
        ShowToastWithText(@"评价成功");
        if (self.myblock) {
            self.myblock(@"");
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.plaseLB.hidden = YES;
    return YES;
}

@end
