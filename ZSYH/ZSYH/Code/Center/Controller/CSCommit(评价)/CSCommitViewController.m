//
//  CSCommitViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSCommitViewController.h"
#import "CSCommitListTableViewCell.h"
#import "CSCimmentModel.h"
@interface CSCommitViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSArray *titArr;

@end

@implementation CSCommitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    self.title = @"评价";
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
  
    [self request];
}
-(void)request{
    
    [NetWorkTool POST:CmyCommitList param:nil success:^(id dic) {
        for (NSMutableDictionary *mudic in [dic objectForKey:@"date"]) {
            CSCimmentModel *model = [[CSCimmentModel alloc]init];
            NSMutableDictionary *mu = [mudic objectForKey:@"evals"];
            [model setValuesForKeysWithDictionary: mu];
            [self.mydateSource addObject:model];
        }
        [self.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSCommitListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSCommitListTableViewCell" forIndexPath:indexPath];
    CSCimmentModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell refash:model];
    return cell;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-150) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSCommitListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSCommitListTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}
-(NSArray *)titArr{
    if (!_titArr) {
        _titArr = @[@"昵称",@"头像",@"电话",@"地址",@"银行卡信息"];
        
    }
    return _titArr;
    
}
@end
