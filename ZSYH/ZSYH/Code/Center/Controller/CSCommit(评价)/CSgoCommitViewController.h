//
//  CSgoCommitViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"
#import "CSorderHeaderModel.h"
#import "CSMallOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSgoCommitViewController : BaseViewController
@property(nonatomic,strong)CSorderHeaderModel *headerModel;
@property(nonatomic,strong)CSMallOrderModel *proModel;


@property(nonatomic,strong)NSString * orderid;

@property(nonatomic,copy)void (^myblock)(NSString *str);
@end

NS_ASSUME_NONNULL_END
