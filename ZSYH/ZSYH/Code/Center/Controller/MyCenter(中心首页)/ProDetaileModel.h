//
//  ProDetaileModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProDetaileModel : BaseModel
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *qrcode;
@property (nonatomic, copy) NSString *operation;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *node_time;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *node_code;
@property (nonatomic, copy) NSString *operation_user_id;
@end

NS_ASSUME_NONNULL_END
