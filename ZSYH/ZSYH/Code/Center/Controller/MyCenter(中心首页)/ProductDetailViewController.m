//
//  ProductDetailViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/30.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "ProdTableViewCell.h"
#import "ProdtwoTableViewCell.h"
#import "ProDetaileModel.h"

@interface ProductDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UIImageView *headerimage;
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *leftArray;
@property(nonatomic,strong)NSMutableArray *rightArray;

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view. [super viewDidLoad];
    self.title = @"产品信息";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    //self.headerImage = [[UIImage alloc]init];
    self.leftArray = [NSMutableArray arrayWithCapacity:1];
    self.rightArray = [NSMutableArray arrayWithCapacity:1];
    
    //NSArray *lefta = @[@"产品分类:",@"产品名称:",@"产品型号:",@"产品数量:",@"产品价格:",@"产品操作:",@"操作时间:"];
    NSArray *lefta = @[@"产品分类:",@"产品名称:",@"产品型号:",@"产品属性:",@"赔付金额",@"有效期:",@"质保期:"];

    self.leftArray = [lefta mutableCopy];
    NSMutableArray *oneArr = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *twoArr = [NSMutableArray arrayWithCapacity:1];
    
    [oneArr addObject:[NSString stringWithFormat:@"%@",[_mydic objectForKey:@"type_name"]]];
    [oneArr addObject:[NSString stringWithFormat:@"%@",[_mydic objectForKey:@"parts_name"]]];
    [oneArr addObject:[NSString stringWithFormat:@"%@",[_mydic objectForKey:@"parts_model"]]];
    [oneArr addObject:[NSString stringWithFormat:@"%@",[_mydic objectForKey:@"name"]]];
    
    [oneArr addObject:[NSString stringWithFormat:@"%@%@",[_mydic objectForKey:@"compensationPrice"],@"元"]];
    [oneArr addObject:[NSString stringWithFormat:@"%@%@%@",[_mydic objectForKey:@"productionDate"],@"  至  ",[_mydic objectForKey:@"termValidity"]]];
    [oneArr addObject:[NSString stringWithFormat:@"%@%@",[_mydic objectForKey:@"warranty"],@"个月"]];
        
    for (NSDictionary *dic in [_mydic objectForKey:@"hisList"]) {
        [DYModelMaker DY_makeModelWithDictionary:dic modelKeyword:@"" modelName:@""];
        ProDetaileModel *model = [[ProDetaileModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [twoArr addObject:model];
    }
    [_rightArray addObject:oneArr];
    
    if (twoArr.count>0) {
        [_rightArray addObject:twoArr];
    }
    
    [self showheaderview];
    [_myTableView reloadData];
    // Do any additional setup after loading the view.
}

-(void)request{
    
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {

    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showheaderview{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 300)];
    header.backgroundColor = [UIColor whiteColor];
    self.headerimage = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-245)/2, 18, 245, 204)];
    [header addSubview:self.headerimage];
    [self.headerimage sd_setImageWithURL:[NSURL URLWithString:[_mydic objectForKey:@"image_url"]] placeholderImage:defaultImg];
    UILabel *beLb = [[UILabel alloc]initWithFrame:CGRectMake(0, 240, KWIDTH, 5)];
    beLb.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [header addSubview:beLb];
    
    UILabel *xinagqing = [[UILabel alloc]initWithFrame:CGRectMake(16, 262, KWIDTH, 16)];
    xinagqing.textColor = K333333;xinagqing.font = FontSize(16);
    xinagqing.text = @"产品详情";
    [header addSubview:xinagqing];
    
    self.myTableView.tableHeaderView = header;
    self.myTableView.tableHeaderView.userInteractionEnabled = YES;
    [self.myTableView reloadData];
    
    [self request];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.01;
    }else{
        return 40;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return [UIView new];
    }else{
        UIView *heid = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 40)];
        heid.backgroundColor = [UIColor whiteColor];
        UILabel *mhylabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, KWIDTH, 20)];
        mhylabel.textColor = K333333;mhylabel.font = FontSize(16);
        [heid addSubview:mhylabel];
        mhylabel.text = @"操作历史";
        return heid;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _rightArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //[_rightArray[section] count]
    NSMutableArray *arr = _rightArray[section];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 38;
    }else{
        return 76;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        ProdTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"ProdTableViewCell" forIndexPath:indexPath];
        cell1.leftLb.text = _leftArray[indexPath.row];
        cell1.rightLB.text = _rightArray[indexPath.section][indexPath.row];
        return cell1;
    } else {
        ProdtwoTableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:@"ProdtwoTableViewCell" forIndexPath:indexPath];
        ProDetaileModel *model = _rightArray[indexPath.section][indexPath.row];
        cell2.upLb.text = model.operation;
        cell2.dowlb.text = model.node_time;
        return cell2;
    }
}

-(UITableView *)myTableView {
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"ProdTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProdTableViewCell"];
        [_myTableView registerNib:[UINib nibWithNibName:@"ProdtwoTableViewCell" bundle:nil] forCellReuseIdentifier:@"ProdtwoTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end


