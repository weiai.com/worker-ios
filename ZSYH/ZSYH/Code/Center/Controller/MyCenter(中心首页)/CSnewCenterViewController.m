//
//  CSnewCenterViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/2.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewCenterViewController.h"
#import "MyheaderView.h"
#import "WXCollectionView.h"
#import "HFShopingCarViewController.h"
#import "HFAddressViewController.h"
#import "CSMyInfoViewController.h"
#import "CSMyorderViewController.h"
#import "CSCommitViewController.h"
#import "CSGanderViewController.h"
#import "CSBuyOrderViewController.h"
#import "CSMyAccConViewController.h"
#import "CSMyCustomerViewController.h"
#import "PingzhengceTableViewCell.h"
#import "CSPersonModel.h"
#import "CSpolicyViewController.h"
#import "CSNewACcontViewController.h"
#import "CSMaintenanceViewController.h"
#import "CSMyTouSuViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "HWScanViewController.h"
#import "CSmakeAccAddmodel.h"

#import "ProductDetailViewController.h"
#import "CSIdAuthViewController.h"
#import "SYBigImage.h"
#import "SkillsCerViewController.h"
#import "PerfectingInfoViewController.h"
#import "PGDatePickManager.h"
#import "OperationGuideViewController.h"
#import "ZYMyQRCodeViewController.h"
#import "ZYBusinessLicenseViewController.h"
#import "ZYunderReviewViewController.h"
#import "ZYNotApprovedViewController.h"
#import "ZYMyMessageViewController.h"
#import "ZYHouBaoOrderFormViewController.h" //候保订货单

@interface CSnewCenterViewController ()<UITableViewDelegate,UITableViewDataSource, PGDatePickerDelegate>
@property (nonatomic, strong) MyheaderView *headerupView;
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) CSPersonModel *model;
@property (nonatomic, strong) NSMutableArray *titarr;
@property (nonatomic, strong) NSMutableArray *imageArr;

@property (nonatomic, strong) UIImageView *headerImage;//头部 头像
@property (nonatomic, strong) UILabel *headername;//头部 昵称
@property (nonatomic, strong) UIButton *headerStuteBut;//头部 听单状态按钮
@property (nonatomic, strong) UILabel *headerlovelb; //爱心值

@property (nonatomic, strong) UIImageView *tablebgImnage;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *navBgview;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *bgViewPhone;
@property (nonatomic, strong) UIImageView *goldMedalImg;

@end

@implementation CSnewCenterViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].index = 2;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self request];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (instancetype)init{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tongzhiRefish:) name:@"tongzhiRefish" object:nil];
    }
    return self;
}

- (void)tongzhiRefish:(NSNotification *)text{
    //  if ([text.userInfo[@"textOne"] isEqualToString:@"chuanshiji"]) {
    
    //如果此页面已经展示 则用此通知在刷新一次
    [self.navigationController pushViewController:[CSBuyOrderViewController new] animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];

//    NSArray *tit = @[@"专属二维码",@"商城订单",@"购物车",@"个人信息",@"我的账户",@"我的客户",@"我的留言",@"我的预订",@"平台政策",@"投诉",@"联系我们",@"操作指南"];
    NSArray *tit = @[@"专属二维码",@"个人信息",@"我的账户",@"我的客户",@"我的留言",@"我的预订",@"平台政策",@"投诉",@"联系我们",@"操作指南"];
    self.titarr = [tit mutableCopy];

//    NSArray *image = @[@"QRCode",@"dingdan",@"gouwuche",@"gerenxinxi",@"zhanghu",@"kehu",@"路径 2584",@"wodedinggou",@"pingtzhengc .png",@"ctousu.png",@"callUs.png",@"caozuozhinan"];
    NSArray *image = @[@"QRCode",@"gerenxinxi",@"zhanghu",@"kehu",@"路径 2584",@"wodedinggou",@"pingtzhengc .png",@"ctousu.png",@"callUs.png",@"caozuozhinan"];
    
    self.imageArr = [image mutableCopy];
    
    [self showui];
    [self shonavview];
    [self shwoBgviewPhone];
    
    UIButton *retBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [retBut setTitle:@"退出登录" forState:(UIControlStateNormal)];
    retBut.frame = CGRectMake(0, 0, KWIDTH-20, 53);
    [retBut setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
    UIView *footview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 53)];
    [footview addSubview:retBut];
    self.myTableView.tableFooterView = footview;
    [retBut addTarget:self action:@selector(tuichu) forControlEvents:(UIControlEventTouchUpInside)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tuisonhg:) name:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil];

    // Do any additional setup after loading the view.
}

- (void)tuisonhg:(NSNotification *)no{
    [self request];
}

-(void)shonavview {
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:CGRectMake(0, -kDHeight, KWIDTH, kNaviHeight+kDHeight)];
    self.navBgview = bgimage;
    bgimage.userInteractionEnabled = YES;
    bgimage.backgroundColor = [UIColor colorWithHexString:@"#70BE68"];
    bgimage.alpha = 0;
    //bgimage.image = imgname(@"ccenterheaImage");
    [self.view addSubview:bgimage];
    //UILabel *title  = [[UILabel alloc]initWithFrame:CGRectMake(0, kNaviHeight-35+kDHeight, KWIDTH, 25)];
    //title.font = FontSize(18);
    //title.text = @"个人中心";
    //title.textAlignment = NSTextAlignmentCenter;
    //[bgimage addSubview:title];
    
    UIButton *tuibut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    tuibut.frame = CGRectMake(KWIDTH-75, kNaviHeight-35+kDHeight, 60, 25);
    //[tuibut setTitle:@"退出" forState:(UIControlStateNormal)];
    //[tuibut setImage:imgname(@"erweimaLv") forState:(UIControlStateNormal)];
    [tuibut setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
    [bgimage addSubview:tuibut];
    [tuibut addTarget:self action:@selector(aeweima) forControlEvents:(UIControlEventTouchUpInside)];
    
    UIButton *testB = [UIButton new];
    testB.frame = CGRectMake(100, 100, 100, 100);
    [testB setBackgroundColor:[UIColor redColor]];
    [testB addTarget:self action:@selector(test) forControlEvents:(UIControlEventTouchUpInside)];
    //[self.view addSubview:testB];
}

- (void)test {
    //    ZSTipsView *view = [ZSTipsView new];
    //    view.upLable.text = @"信息提交成功";
    //    view.btnBlock = ^{
    //
    //    };
    //    [view show];
    
    //    ZSTipsView *view = [ZSTipsView new];
    //    view.upImage.image = [UIImage imageNamed:@"accyes"];
    //    view.dowLable.text = @"结算申请已提交，等待平台处理";
    //    [view.btn setTitle:@"完成" forState:(UIControlStateNormal)];
    //    [view.btn setBackgroundColor:[UIColor colorWithHexString:@"#CA9F61"]];
    //    view.btnBlock = ^{
    //
    //    };
    //    [view show];
    //
    //    ZSYHAlertView *view = [ZSYHAlertView new];
    //    view.upImage.image = [UIImage imageNamed:@"组 1461"];
    //    view.upLable.text = @"您选择的服务范围";
    //    view.dowLable.text = @"需要上传您本人的技能证件！";
    //    [view.btn1 setTitle:@"取消" forState:(UIControlStateNormal)];
    //    [view.btn2 setTitle:@"去上传" forState:(UIControlStateNormal)];
    //    view.btn1Block = ^{
    //
    //    };
    //    view.btn2Block = ^{
    //
    //    };
    //    [view show];
    //
    //    ZSAlertView *view = [ZSAlertView new];
    //    view.upImage.image = [UIImage imageNamed:@"组 1461"];
    //    view.upLable.text = @"您选择的服务范围";
    //    view.dowLable.text = @"需要上传您本人的技能证件！";
    //    [view.btn1 setTitle:@"取消" forState:(UIControlStateNormal)];
    //    [view.btn2 setTitle:@"去上传" forState:(UIControlStateNormal)];
    //    view.btn1Block = ^{
    //
    //    };
    //    view.btn2Block = ^{
    //
    //    };
    //    [view show];
    //
    //    [self shwoBgview];
    
    [self showBgviewWithMes:@"{\"subscribe_date\":\"2019-07-25\",\"user_address\":\"北京北京市东城区 sfsd4849032840982408324982409283409248980\",\"user_phone\":\"13949667279\",\"id\":\"671191432421183488\",\"subscribe_time\":\"11 : 00-12 : 00\"}"];
}

/**
 弹出框的背景图
 */
-(void)showBgviewWithMes:(NSString *)mes {
    
    NSDictionary *dict = [HFTools dictWithJsonString:mes];
    NSLog(@"%@",dict);
    
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.centerY.offset(0);
        make.height.offset(300);
        make.width.offset(KWIDTH-40);
    }];
    
    ////    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((246-105)/2, 0, 105, 105)];
    ////    upImage.layer.masksToBounds = YES;
    ////    [whiteBGView addSubview:upImage];
    ////    upImage.image = [UIImage imageNamed:@"组 2861"];
    //
    //    /*UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, KWIDTH-40, 21)];
    //    [whiteBGView addSubview:UpLable];
    //    UpLable.font = [UIFont boldSystemFontOfSize:18];
    //    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    //    UpLable.textAlignment =  NSTextAlignmentCenter;
    //    UpLable.text = @"配件已到货";*/
    //
    //    UILabel *DowLable1 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31, KWIDTH-40-40, 21)];
    //    [whiteBGView addSubview:DowLable1];
    //    DowLable1.font = FontSize(14);
    //    DowLable1.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable1.textAlignment =  NSTextAlignmentLeft;
    //    NSArray  *myarray = [dict[@"subscribe_date"] componentsSeparatedByString:@"-"];
    //    DowLable1.text = [NSString stringWithFormat:@"新预约日期: %@年%@月%@日", myarray[0],myarray[1],myarray[2]];
    //
    //    UILabel *DowLable2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31*2, KWIDTH-40-40, 21)];
    //    [whiteBGView addSubview:DowLable2];
    //    DowLable2.font = FontSize(14);
    //    DowLable2.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable2.textAlignment =  NSTextAlignmentLeft;
    //    NSInteger indd = [[dict[@"subscribe_time"] substringToIndex:2] integerValue];
    //    NSString *ap = indd < 12 ? @"上午":@"下午";
    //    DowLable2.text = [NSString stringWithFormat:@"新预约时间: %@ %@", ap, dict[@"subscribe_time"]];
    //
    //    UILabel *DowLable3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31*3, KWIDTH-40-40, 21)];
    //    [whiteBGView addSubview:DowLable3];
    //    DowLable3.font = FontSize(14);
    //    DowLable3.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable3.textAlignment =  NSTextAlignmentLeft;
    //    DowLable3.text = [NSString stringWithFormat:@"客户电话:     %@", dict[@"user_phone"]];
    //
    //    UILabel *DowLable4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31*4, 80, 21)];
    //    [whiteBGView addSubview:DowLable4];
    //    DowLable4.font = FontSize(14);
    //    DowLable4.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable4.textAlignment =  NSTextAlignmentLeft;
    //    DowLable4.text = [NSString stringWithFormat:@"客户地址:     "];
    //    DowLable4.numberOfLines = 2;
    //
    //    UILabel *DowLable5 = [[UILabel alloc]initWithFrame:CGRectMake(DowLable4.right, 50+31*4, KWIDTH-40-DowLable4.right, 40)];
    //    [whiteBGView addSubview:DowLable5];
    //    DowLable5.font = FontSize(14);
    //    DowLable5.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable5.textAlignment =  NSTextAlignmentLeft;
    //    DowLable5.text = [NSString stringWithFormat:@"%@", dict[@"user_address"]];
    //    DowLable5.numberOfLines = 2;
    //
    //    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    //    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    //    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    //    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    //    iKnowBut.layer.masksToBounds = YES;
    //    iKnowBut.layer.cornerRadius = 22;
    //    [iKnowBut.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    //    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //    [whiteBGView addSubview:iKnowBut];
    //    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.offset(44);
    //        make.left.offset(36);
    //        make.right.offset(-36);
    //        make.bottom.offset(-20);
    //    }];
    //
    //    [[UIApplication sharedApplication].keyWindow addSubview: self.bgView];
    
    //    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((246-105)/2, 0, 105, 105)];
    //    upImage.layer.masksToBounds = YES;
    //    [whiteBGView addSubview:upImage];
    //    upImage.image = [UIImage imageNamed:@"组 2861"];
    
    /*UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, KWIDTH-40, 21)];
     [whiteBGView addSubview:UpLable];
     UpLable.font = [UIFont boldSystemFontOfSize:18];
     UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
     UpLable.textAlignment =  NSTextAlignmentCenter;
     UpLable.text = @"配件已到货";*/
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bgView.width, 96)];
    upImage.layer.masksToBounds = YES;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 2111"];
    
    UILabel *DowLable3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable3];
    DowLable3.font = FontSize(14);
    DowLable3.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable3.textAlignment =  NSTextAlignmentLeft;
    DowLable3.text = [NSString stringWithFormat:@"客户电话:     %@", dict[@"user_phone"]];
    
    UILabel *DowLable4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*2, 80, 21)];
    [whiteBGView addSubview:DowLable4];
    DowLable4.font = FontSize(14);
    DowLable4.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable4.textAlignment =  NSTextAlignmentLeft;
    DowLable4.text = [NSString stringWithFormat:@"客户地址:     "];
    DowLable4.numberOfLines = 2;
    
    UILabel *DowLable5 = [[UILabel alloc]initWithFrame:CGRectMake(DowLable4.right, 96+31*2, KWIDTH-40-DowLable4.right, 20)];
    [whiteBGView addSubview:DowLable5];
    DowLable5.font = FontSize(14);
    DowLable5.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable5.textAlignment =  NSTextAlignmentLeft;
    DowLable5.text = [NSString stringWithFormat:@"%@", dict[@"user_address"]];
    DowLable5.numberOfLines = 2;
    
    UILabel *DowLable1 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*3, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable1];
    DowLable1.font = FontSize(14);
    DowLable1.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable1.textAlignment =  NSTextAlignmentLeft;
    NSArray  *myarray = [dict[@"subscribe_date"] componentsSeparatedByString:@"-"];
    DowLable1.text = [NSString stringWithFormat:@"新预约日期: %@年%@月%@日", myarray[0],myarray[1],myarray[2]];
    
    UILabel *DowLable2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*4, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable2];
    DowLable2.font = FontSize(14);
    DowLable2.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable2.textAlignment =  NSTextAlignmentLeft;
    NSInteger indd = [[dict[@"subscribe_time"] substringToIndex:2] integerValue];
    NSString *ap = indd < 12 ? @"上午":@"下午";
    DowLable2.text = [NSString stringWithFormat:@"新预约时间: %@ %@", ap, dict[@"subscribe_time"]];
    
    //    UILabel *DowLable3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31*3, KWIDTH-40-40, 21)];
    //    [whiteBGView addSubview:DowLable3];
    //    DowLable3.font = FontSize(14);
    //    DowLable3.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable3.textAlignment =  NSTextAlignmentLeft;
    //    DowLable3.text = [NSString stringWithFormat:@"客户电话:     %@", dict[@"user_phone"]];
    
    //    UILabel *DowLable4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 50+31*4, 80, 21)];
    //    [whiteBGView addSubview:DowLable4];
    //    DowLable4.font = FontSize(14);
    //    DowLable4.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable4.textAlignment =  NSTextAlignmentLeft;
    //    DowLable4.text = [NSString stringWithFormat:@"客户地址:     "];
    //    DowLable4.numberOfLines = 2;
    
    //    UILabel *DowLable5 = [[UILabel alloc]initWithFrame:CGRectMake(DowLable4.right, 50+31*4, KWIDTH-40-DowLable4.right, 40)];
    //    [whiteBGView addSubview:DowLable5];
    //    DowLable5.font = FontSize(14);
    //    DowLable5.textColor = [UIColor colorWithHexString:@"#333333"];
    //    DowLable5.textAlignment =  NSTextAlignmentLeft;
    //    DowLable5.text = [NSString stringWithFormat:@"%@", dict[@"user_address"]];
    //    DowLable5.numberOfLines = 2;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 22;
    [iKnowBut.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(44);
        make.left.offset(36);
        make.right.offset(-36);
        make.bottom.offset(-20);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.bgView];
}

/**
 弹出框的背景图
 */
-(void)shwoBgview{
    self.bgView = [[UIView alloc]init];
    self.bgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.bgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"订单支付完成";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"请准时上门处理！";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"结束订单" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenowAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.bgView];
}

-(void)ikenowAction:(UIButton *)but{
    [self.bgView removeFromSuperview];
}

-(void)aeweima{
    KMyLog(@"二维码");
}

//点击事件
-(void)selectHeaderImage {
    //具体的实现
}

-(void)showui{
    CGFloat fff = 220 ;
    [self.view addSubview:self.scrollView];
    if (IPHONE_X) {
        fff = 260+24;
    }
    
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:CGRectMake(0, -kDHeight, KWIDTH, fff)];
    self.tablebgImnage = bgimage;
    bgimage.userInteractionEnabled = YES;
    bgimage.image = imgname(@"ccenterheaImage");
    [self.scrollView addSubview:bgimage];
    [self.myTableView reloadData];
    
    UILabel *title  = [[UILabel alloc]initWithFrame:CGRectMake(0, kNaviHeight-35+kDHeight, KWIDTH, 25)];
    title.font = FontSize(18);
    title.text = @"个人中心";
    title.textAlignment = NSTextAlignmentCenter;
    [bgimage addSubview:title];
    
    UIButton *tuibut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    tuibut.frame = CGRectMake(KWIDTH-75, kNaviHeight-35+kDHeight, 60, 25);
    [tuibut setImage:imgname(@"heideerweimadf") forState:(UIControlStateNormal)];
    [tuibut setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
    [bgimage addSubview:tuibut];
    [tuibut addTarget:self action:@selector(saoyisao) forControlEvents:(UIControlEventTouchUpInside)];
    
    CGFloat ff = IPHONE_X?50 :0;
    
    self.headerImage = [[UIImageView alloc]initWithFrame:CGRectMake(25, 95+ff, 66, 66)];
    self.headerImage.layer.masksToBounds = YES;
    self.headerImage.layer.cornerRadius = 33;
    [bgimage addSubview:self.headerImage];
    self.headerImage.image = imgname(@"userImage");
    self.headerImage.userInteractionEnabled = YES;
    SYBigImage * bigI = [[SYBigImage alloc]init];
    [self.headerImage addGestureRecognizer:bigI];
    
    self.goldMedalImg = [[UIImageView alloc] initWithFrame:CGRectMake(28, 139+ff, 62, 26)];
    [bgimage addSubview:self.goldMedalImg];
    
    ////打开用户交互
    //self.headerImage.userInteractionEnabled = YES;
    ////给图片添加点击手势（也可以添加其他手势）
    //UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectHeaderImage)];
    //[self.headerImage addGestureRecognizer:tap];
    
    self.headername = [[UILabel alloc]init];
    _headername.font = FontSize(16);
    _headername.text = @"昵称";
    _headername.textColor = [UIColor whiteColor];
    _headername.textAlignment = NSTextAlignmentCenter;
    [bgimage addSubview:_headername];
    kWeakSelf;
    [_headername mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.headerImage.mas_right).offset(16);
        make.top.mas_equalTo(weakSelf.headerImage.mas_top).offset(16);
        make.height.offset(16);
    }];
    
    self.headerStuteBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.headerStuteBut.hidden = YES;
    [bgimage addSubview:_headerStuteBut];
    [_headerStuteBut setTitle:@"状态" forState:(UIControlStateNormal)];
    [_headerStuteBut setBackgroundColor:[UIColor colorWithHexString:@"#FFFFFF"]];
    [_headerStuteBut setTitleColor:[UIColor colorWithHexString:@"#FFA800"] forState:(UIControlStateNormal)];
    _headerStuteBut.layer.masksToBounds = YES;
    _headerStuteBut.layer.cornerRadius = 12;
    [_headerStuteBut addTarget:self action:@selector(headerlisingSute:) forControlEvents:(UIControlEventTouchUpInside)];
    _headerStuteBut.titleLabel.font = FontSize(12);
    [_headerStuteBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakSelf.headerImage.mas_right).offset(16);
        make.top.mas_equalTo(weakSelf.headername.mas_bottom).offset(11);
        make.height.offset(24);
        make.width.offset(58);
    }];
    
    UIButton *codeBtn = [[UIButton alloc]init];
    [bgimage addSubview:codeBtn];
    [codeBtn setImage:imgname(@"组 3215") forState:UIControlStateNormal];
    [codeBtn addTarget:self action:@selector(myCodeBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    [codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        //make.centerY.mas_equalTo(weakSelf.headerStuteBut.mas_centerY).offset(2.5);
        make.top.mas_equalTo(weakSelf.headerImage.mas_top).offset(16);
        make.right.offset(-55);
        make.height.offset(24);
        make.width.offset(24);
    }];
    
    //    UIImageView *codeImage = [[UIImageView alloc]init];
    //    [bgimage addSubview:codeImage];
    //    codeImage.image = imgname(@"组 3215");
    //    [codeImage mas_makeConstraints:^(MASConstraintMaker *make) {
    ////        make.centerY.mas_equalTo(weakSelf.headerStuteBut.mas_centerY).offset(2.5);
    //        make.top.mas_equalTo(weakSelf.headerImage.mas_top).offset(16);
    //        make.right.offset(-55);
    //        make.height.offset(24);
    //        make.width.offset(24);
    //    }];
    
    //    UIImageView *loveImage = [[UIImageView alloc]init];
    //    [bgimage addSubview:loveImage];
    //    loveImage.image = imgname(@"CLove");
    //    [loveImage mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.centerY.mas_equalTo(weakSelf.headerStuteBut.mas_centerY).offset(2.5);
    //        make.right.offset(-55);
    //        make.height.offset(23);
    //        make.width.offset(26);
    //    }];
    
    //    self.headerlovelb = [[UILabel alloc]init];
    //    [bgimage addSubview:_headerlovelb];
    //    _headerlovelb.font = FontSize(9);
    //    _headerlovelb.text = @"爱心值";
    //    _headerlovelb.textColor = [UIColor colorWithHexString:@"#020202"];
    //
    //    [_headerlovelb mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.left.mas_equalTo(loveImage.mas_right).offset(2);
    //        make.height.offset(9);
    //        make.centerY.mas_equalTo(weakSelf.headerStuteBut.mas_centerY).offset(2.5);
    //    }];
}

-(void)saoyisao{
    KMyLog(@"扫一扫");
    
    HWScanViewController *vc = [[HWScanViewController alloc]init];
    vc.myblock = ^(NSString *str) {
        [self requesrtWith:str ];
    };
    UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:na animated:YES completion:nil];
}

- (void)myCodeBtnAction {
    KMyLog(@"专属二维码");
//    if ([self.model.licenseState isEqualToString:@"-1"]) { //拒绝
//        //审核未通过
//        ZYNotApprovedViewController *nvc = [[ZYNotApprovedViewController alloc] init];
//        nvc.model = _model;
//        [self.navigationController pushViewController:nvc animated:YES];
//    } else if ([self.model.licenseState isEqualToString:@"0"]) { //未上传
//        //上传相关证件
//        ZYBusinessLicenseViewController *bvc = [[ZYBusinessLicenseViewController alloc] init];
//        bvc.model = _model;
//        [self.navigationController pushViewController:bvc animated:YES];
//    } else if ([self.model.licenseState isEqualToString:@"1"]) { //审核中
//        //审核中
//        ZYUnderReviewViewController *uvc = [[ZYUnderReviewViewController alloc] init];
//        [self.navigationController pushViewController:uvc animated:YES];
//    } else if ([self.model.licenseState isEqualToString:@"2"]) { //通过
        //专属二维码
        ZYMyQRCodeViewController *codeVC = [[ZYMyQRCodeViewController alloc] init];
        codeVC.model = _model;
        [self.navigationController pushViewController:codeVC animated:YES];
//    }
}

- (void)requesrtWith:(NSString *)strr{
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        
        ProductDetailViewController *bao = [[ProductDetailViewController alloc]init];
        bao.mydic = [dic objectForKey:@"data"];
        [self.navigationController pushViewController:bao animated:YES];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)headerlisingSute:(UIButton *)but{
    //ShowToastWithText(@"听单中");
}

-(void)request{
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        KMyLog(@"----------个人信息---------- %@", dic);
        self.model = [[CSPersonModel alloc]init];
        [DYModelMaker DY_makeModelWithDictionary:[dic objectForKey:@"data"] modelKeyword:@"" modelName:@""];
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        
        self.headerupView.userName.text = weakSelf.model.nickname;
        self.headerupView.userPhone.text = weakSelf.model.phone;
        
        if ([weakSelf.model.workstate isEqualToString:@"1"]) {
            [self.headerStuteBut setTitle:@"听单中" forState:(UIControlStateNormal)];
        } else {
            [self.headerStuteBut setTitle:@"休息" forState:(UIControlStateNormal)];
        }
        
        weakSelf.headername.text = weakSelf.model.nickname;
        weakSelf.headerlovelb.text = [NSString stringWithFormat:@"爱心值:%@",weakSelf.model.invitationCode];
        
        if (weakSelf.model.image_url.length <= 0) {
            weakSelf.headerImage.image = [UIImage imageNamed:@"userImage"];
        } else {
            if ([weakSelf.model.image_url hasPrefix:@"http"]) {
                [weakSelf.headerImage sd_setImageWithURL:[NSURL URLWithString:_model.image_url] placeholderImage:[UIImage imageNamed:@"userImage"]];
            } else {
                sdimg(weakSelf.headerImage, weakSelf.model.image_url);
            }
        }
        
        //金牌师傅 图片
//        if ([self.model.licenseState isEqualToString:@"2"]) {
//            self.goldMedalImg.image = imgname(@"组 3330");
//        } else {
//
//        }
        
        NSString *phone = [NSString stringWithFormat:@"%@",weakSelf.model.phone];
        [USER_DEFAULT setObject:phone forKey:@"phone"];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (void)tuichu{
    
    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"确定退出登录?" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        KMyLog(@"取消");
    }]];
    [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        KMyLog(@"确定");
        ///退出
        //CheckLogin
        [NetWorkTool POST:CUserZhuXiao param:nil success:^(id dic) {
            [USER_DEFAULT removeObjectForKey:@"Token"];
            //设置Alias
            [JPUSHService setAlias:nil completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                
            } seq:0];
            
            CheckLogin
            //[JPUSHService setTags:nil alias:nil fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
            //
            //}];
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }]];
    [self presentViewController:alertCon animated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    //NSLog(@"滑动开始%f",scrollView.contentOffset.y);
    CGFloat yyy = scrollView.contentOffset.y;
    
    CGFloat reOffset = scrollView.contentOffset.y + (KHEIGHT - kNaviHeight) * 0.2;
    CGFloat alpha = reOffset / ((KHEIGHT - kNaviHeight) * 0.15);
    if (alpha <= 1) { //下拉永不显示导航栏
        alpha = 0;
    } else { //上划前一个导航栏的长度是渐变的
        alpha -= 1;
    }
    if (yyy<=0) {
        alpha = 0;
    }
    self.navBgview.alpha = alpha;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.imageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PingzhengceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PingzhengceTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.image.image = imgname([_imageArr safeObjectAtIndex:indexPath.row]);
    cell.conlb.text = _titarr[indexPath.row];
    cell.conlb.font = KFontPingFangSCRegular(14);
    cell.conlb.textColor = K333333;
    if ([cell.conlb.text  isEqualToString:@"联系我们"]) {
        cell.rightBut.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 46;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *str = [NSString stringWithFormat:@"%@",[_titarr safeObjectAtIndex:indexPath.row]];
    KMyLog(@"dsafaf%@",str);
    if ([str isEqualToString:@"专属二维码"]) {
//        if ([self.model.licenseState isEqualToString:@"-1"]) { //拒绝
//            ZYNotApprovedViewController *nvc = [[ZYNotApprovedViewController alloc] init];
//            nvc.model = _model;
//            [self.navigationController pushViewController:nvc animated:YES];
//        } else if ([self.model.licenseState isEqualToString:@"0"]) { //未上传
//            //上传相关证件
//            ZYBusinessLicenseViewController *bvc = [[ZYBusinessLicenseViewController alloc] init];
//            bvc.model = _model;
//            [self.navigationController pushViewController:bvc animated:YES];
//        } else if ([self.model.licenseState isEqualToString:@"1"]) { //审核中
//            //审核中
//            ZYUnderReviewViewController *uvc = [[ZYUnderReviewViewController alloc] init];
//            [self.navigationController pushViewController:uvc animated:YES];
//        } else if ([self.model.licenseState isEqualToString:@"2"]) { //通过
            //我的二维码
            ZYMyQRCodeViewController *codeVC = [[ZYMyQRCodeViewController alloc] init];
            codeVC.model = _model;
            [self.navigationController pushViewController:codeVC animated:YES];
//        }
        
    }
    //else if ([str isEqualToString:@"购物车"]) {
        //[self.navigationController pushViewController:[HFShopingCarViewController new] animated:YES];
    //}
    else if ([str isEqualToString:@"个人信息"]){
        
        CSMyInfoViewController *per = [[CSMyInfoViewController alloc]init];
        per.model = _model;
        [self.navigationController pushViewController:per animated:YES];
        
    } else if ([str isEqualToString:@"服务单"]){
        
        [self.navigationController pushViewController:[CSMyorderViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"评价"]){
        
        [self.navigationController pushViewController:[CSCommitViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"积分"]){
        
        [self.navigationController pushViewController:[CSGanderViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"商城订单"]){
        
        [self.navigationController pushViewController:[CSBuyOrderViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"我的账户"]){
        
        //[self.navigationController pushViewController:[CSNewACcontViewController new] animated:YES];
        
        CSNewACcontViewController *vc = [[CSNewACcontViewController alloc] init];
        vc.model = _model;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if ([str isEqualToString:@"我的客户"]){
        
        [self.navigationController pushViewController:[CSMyCustomerViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"我的留言"]){
        
        ZYMyMessageViewController *message = [[ZYMyMessageViewController alloc] init];
        message.mymodel = _model;
        [self.navigationController pushViewController:message animated:YES];
        
    } else if ([str isEqualToString:@"我的预订"]){
        
        ZYHouBaoOrderFormViewController *vc = [[ZYHouBaoOrderFormViewController alloc] init];
        
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if ([str isEqualToString:@"平台政策"]){
        
        [self.navigationController pushViewController:[CSpolicyViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"售后维修"]){
        
        [self.navigationController pushViewController:[CSMaintenanceViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"联系我们"]){
        
        //[HFTools callMobilePhone:@"400-961-5811"];
        
        [[UIApplication sharedApplication].keyWindow addSubview:self->_bgViewPhone];
        
    } else if ([str isEqualToString:@"投诉"]){
        [self.navigationController pushViewController:[CSMyTouSuViewController new] animated:YES];
        
    } else if ([str isEqualToString:@"操作指南"]) {
        
        [self.navigationController pushViewController:[OperationGuideViewController new]  animated:YES];
    }
}

/**
 弹出框的背景图 确定向平台拨打电话?
 */
-(void)shwoBgviewPhone {
    self.bgViewPhone = [[UIView alloc]init];
    self.bgViewPhone.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewPhone.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewPhone addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(154);
        make.left.offset(70);
        make.right.offset(-70);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    //upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 37, KWIDTH-140, 42)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.numberOfLines = 0;
    UpLable.text = @"确定向平台拨打电话?";
    //self.bgUPLable = UpLable;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    //DowLable.text = @"请到结算日申请结算";
    //self.bgdowLable = DowLable;
    
    UIButton *cancleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancleBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancleBut setTitleColor:[UIColor colorWithHexString:@"#999999"] forState:(UIControlStateNormal)];
    [cancleBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    cancleBut.layer.masksToBounds = YES;
    cancleBut.layer.cornerRadius = 4;
    [cancleBut addTarget:self action:@selector(cancleBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:cancleBut];
    [cancleBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.left.offset(35);
        make.width.offset((KWIDTH-70*2-35*2-37)/2);
        make.bottom.offset(-32);
    }];
    
    UIButton *sureBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [sureBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [sureBut setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:(UIControlStateNormal)];
    [sureBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    sureBut.layer.masksToBounds = YES;
    sureBut.layer.cornerRadius = 4;
    [sureBut addTarget:self action:@selector(sureBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:sureBut];
    [sureBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(32);
        make.right.offset(-35);
        make.width.offset((KWIDTH-70*2-35*2-37)/2);
        make.bottom.offset(-32);
    }];
}

-(void)cancleBtnAction:(UIButton *)but {
    [_bgViewPhone removeFromSuperview];
    //if (self.myblock) {
    //self.myblock(@"");
    //}
    //[self.navigationController popViewControllerAnimated:YES];
}

- (void)sureBtnAction:(UIButton *)but {
    [_bgViewPhone removeFromSuperview];
    [HFTools callMobilePhone:@"400-961-5811"];
}


-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 13, KWIDTH-20, KHEIGHT-50) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.scrollEnabled = NO;
        _myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"PingzhengceTableViewCell" bundle:nil] forCellReuseIdentifier:@"PingzhengceTableViewCell"];
        adjustInset(_myTableView);
        CGFloat ff = IPHONE_X?35 :0;
        CGFloat fffhei = _titarr.count * 46+60;
        
        UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(10, 159+ff, KWIDTH-20, fffhei)];
        bgview.layer.masksToBounds = YES;
        bgview.layer.cornerRadius = 8;
        bgview.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:bgview];
        [bgview addSubview:_myTableView];
        _scrollView.contentSize = CGSizeMake(KWIDTH, bgview.bottom + 80);
        
    }
    return _myTableView;
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT-kTabbarHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 100);
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
