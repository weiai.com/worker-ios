//
//  ProdtwoTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProdtwoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *upLb;
@property (weak, nonatomic) IBOutlet UILabel *dowlb;

@end

NS_ASSUME_NONNULL_END
