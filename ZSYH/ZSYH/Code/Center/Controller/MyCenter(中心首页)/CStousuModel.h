//
//  CStousuModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CStousuModel : BaseModel
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *state;
@end

NS_ASSUME_NONNULL_END
