//
//  MyCenterViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "MyCenterViewController.h"
#import "MyheaderView.h"
#import "WXCollectionView.h"
#import "HFShopingCarViewController.h"
#import "HFAddressViewController.h"
#import "CSMyInfoViewController.h"
#import "CSMyorderViewController.h"
#import "CSCommitViewController.h"
#import "CSGanderViewController.h"
#import "CSBuyOrderViewController.h"
#import "CSMyAccConViewController.h"
#import "CSMyCustomerViewController.h"
#import "PingzhengceTableViewCell.h"
#import "CSPersonModel.h"
#import "CSpolicyViewController.h"

@interface MyCenterViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)MyheaderView *headerupView;
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)CSPersonModel *model;

@end

@implementation MyCenterViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [singlTool shareSingTool].index = 2;
    
    [self request];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人中心";
    [self.rightbutton setTitle:@"退出" forState:(UIControlStateNormal)];
    [self showheaderview];
    [self request];
}

- (void)rightClick{

    [NetWorkTool POST:CUserZhuXiao param:nil success:^(id dic) {
       [USER_DEFAULT removeObjectForKey:@"Token"];
        CheckLogin
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)request{
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        
        self.model = [[CSPersonModel alloc]init];
        [self.model setValuesForKeysWithDictionary:[dic objectForKey:@"data"]];
        self.headerupView.userName.text =weakSelf.model.nickname;
        self.headerupView.userPhone.text = weakSelf.model.phone;
        if ([weakSelf.model.workstate integerValue] == 1) {
            [self.headerupView.stateButton setTitle:@"忙时" forState:(UIControlStateNormal)];
        }else{
        [self.headerupView.stateButton setTitle:@"休息" forState:(UIControlStateNormal)];
        }
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showheaderview{
    UIView *header = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 200)];
    [self.view addSubview:header];
    header.backgroundColor = [UIColor whiteColor];
    header.userInteractionEnabled = YES;
    self.headerupView = [[MyheaderView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 200)];
    NSArray *tit = @[@"订单",@"购物车",@"个人信息",@"维修单",@"评价",@"积分",@"我的账户",@"我的客户"];
    NSArray *image = @[@"dingdan",@"gouwuche",@"gerenxinxi",@"weixiudan",@"pingjia",@"jifen",@"kehu",@"zhanghu"]; 

    WXCollectionView *wx = [[WXCollectionView alloc]initWithFrame:CGRectMake(0, _headerupView.bottom+10, KWIDTH, 160) img:image title:tit block:^(NSInteger index) {
        KMyLog(@"点击了地%ld个",(long)index);
        
        NSString *str = [NSString stringWithFormat:@"%@",tit[index]];
        if ([str isEqualToString:@"购物车"]) {
            [self.navigationController pushViewController:[HFShopingCarViewController new] animated:YES];
        }else if ([str isEqualToString:@"个人信息"]){
            [self.navigationController pushViewController:[CSMyInfoViewController new] animated:YES];

        }else if ([str isEqualToString:@"服务单"]){
            [self.navigationController pushViewController:[CSMyorderViewController new] animated:YES];

        }else if ([str isEqualToString:@"评价"]){
            
            [self.navigationController pushViewController:[CSCommitViewController new] animated:YES];

        }else if ([str isEqualToString:@"积分"]){
            
            [self.navigationController pushViewController:[CSGanderViewController new] animated:YES];
            
        }else if ([str isEqualToString:@"订单"]){
            
            [self.navigationController pushViewController:[CSBuyOrderViewController new] animated:YES];
            
        }else if ([str isEqualToString:@"我的账户"]){
            
            [self.navigationController pushViewController:[CSMyAccConViewController new] animated:YES];
            
        }else if ([str isEqualToString:@"我的客户"]){
            
            [self.navigationController pushViewController:[CSMyCustomerViewController new] animated:YES];
            
        }
    }];
    header.frame = CGRectMake(0, 0, KWIDTH, 370);
    [header addSubview:_headerupView];
    [header addSubview:wx];

    self.myTableView.tableHeaderView = header;
    self.myTableView.tableHeaderView.userInteractionEnabled = YES;
    [self.myTableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PingzhengceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PingzhengceTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 52;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.navigationController pushViewController:[CSpolicyViewController new] animated:YES];
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"PingzhengceTableViewCell" bundle:nil] forCellReuseIdentifier:@"PingzhengceTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

@end
