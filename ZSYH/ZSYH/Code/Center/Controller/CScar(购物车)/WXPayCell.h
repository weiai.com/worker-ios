//
//  WXPayCell.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXPayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *kuang;
@property (weak, nonatomic) IBOutlet UILabel *addLB;

@end
