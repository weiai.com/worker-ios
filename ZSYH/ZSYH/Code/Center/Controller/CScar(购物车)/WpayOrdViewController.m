//
//  WpayOrdViewController.m
//  APPStart
//
//  Created by 魏堰青 on 2018/12/5.
//  Copyright © 2018年 WQ. All rights reserved.
//

#import "WpayOrdViewController.h"
#import "HFAddressViewController.h"
//#import "HFOrderViewController.h"
#import "CommitOrderController.h"
//#import "PayOrdersController.h"
#import "CommitOrderFoot.h"
#import "CommitOrderCell.h"
#import "WXPayView.h"
#import "DefaultAreaView.h"
#import "WXApi.h"
#import <AlipaySDK/AlipaySDK.h>
#import "TabBarViewController.h"
#import "CSGoodListModel.h"

@interface WpayOrdViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSString *_pay;
    NSString *_total_fee;
    NSString *_areaId;
    NSString *_couponId;
    UILabel *_priceLabel;
    UILabel *_numLab;
}
@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSString *payway;
@end

static NSString *const cellId = @"CommitOrderCell";
static NSString *const footId = @"CommitOrderFoot";

@implementation WpayOrdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zhifuAction:) name:@"zhifuPay" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tongzhiRefiddsh:) name:@"weixinPaystatusSuccess" object:nil];

    _modelArr = [NSMutableArray array];
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, ScreenH-59) style:UITableViewStylePlain];
    self.tableView.backgroundColor = RGBA(243, 243, 243,1);
    self.tableView.rowHeight = 115;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    TableRegisterNib(self.tableView, cellId);
    [self.tableView registerClass:[CommitOrderFoot class] forHeaderFooterViewReuseIdentifier:footId];
    [self.view addSubview:self.tableView];
    
    [self setBottomView];
    
    NSDictionary *retval = _response[@"retval"];
    
    //[_modelArr addObjectsFromArray:[GoodsCartModel mj_objectArrayWithKeyValuesArray:data]];
    //赋值
    
    [self.tableView reloadData];
    NSDictionary *statc = retval[@"statistics"];
    _total_fee = statc[@"amount"];
    NSString *string = [NSString stringWithFormat:@"总计￥%@元", _total_fee];
    NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
    [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2, string.length-3)];
    _priceLabel.attributedText = totalss;
    
    _numLab.text = [NSString stringWithFormat:@"共%@件产品",statc[@"quantity"]];
    if (!strIsEmpty(_goods_num)) {
        _numLab.text = [NSString stringWithFormat:@"共%@件产品",_goods_num];
        NSString *string = [NSString stringWithFormat:@"总计￥%.0f元", [_goods_total floatValue]];
        NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
        [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2, string.length-3)];
        _priceLabel.attributedText = totalss;
        
    }
    
    UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenW, 200)];
    footView.userInteractionEnabled = YES;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 49.5)];
    label.backgroundColor = [UIColor whiteColor];
    label.textColor = KTEXTCOLOR;
    label.text = @"    选择付款方式";
    [footView addSubview:label];
    
    CGRect frame = CGRectMake(0, label.bottom, KWIDTH, 150);
    NSArray *arr = @[@"支付宝",@"微信",@"余额"];
    NSArray *img = @[@"alipay",@"wechat",@"yue"];
    
    WXPayView *payV = [[WXPayView alloc]initWithFrame:frame array:arr image:img block:^(NSInteger index) {
        self->_payway = @[@"alipay",@"wechat",@"balance"][index];
        NSLog(@"选择了第%ld",index);
        int num = index == 3?10:1;
        NSString *string = [NSString stringWithFormat:@"总计￥%.2f%@", self->_goods_total.floatValue*num, index==3?@"":@"元"];
        NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
        [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2, string.length-2)];
        self->_priceLabel.attributedText = totalss;
        
    }];
    _payway = @"alipay";//设置默认选项
    [footView addSubview:payV];
    self.tableView.tableFooterView = footView;
    
    [self.leftbutton setImage:imgname(@"mblack") forState:UIControlStateNormal];
}

-(void)zhifuAction:(NSNotification *)no{
    NSDictionary *dic = no.userInfo;
    
    KMyLog(@"通知参数%@",dic);
    if ([[dic objectForKey:@"resultStatus"] integerValue]==9000 ) {
        //支付成功 未处理
        [self zhifuchengg];
    }
}

-(void)hzifuSuccess{
    //    HFOrderViewController *order = [[HFOrderViewController alloc] init];
    //    [self.navigationController popToViewController:order animated:YES ];
    TabBarViewController *tabbar = [[TabBarViewController alloc]init];
    [UIApplication sharedApplication].delegate.window.rootViewController = tabbar;
}

- (void)tongzhiRefiddsh:(NSNotification *)text{
    if ([text.userInfo[@"status"] isEqualToString:@"success"]) {
        [self showOnleText:@"支付成功" delay:1.5];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //支付成功 未处理
            [self zhifuchengg];
            
        });
    }else if([text.userInfo[@"status"] isEqualToString:@"cancle"]){
        [self showOnleText:@"取消支付" delay:1.5];
        
        
    }else if([text.userInfo[@"status"] isEqualToString:@"shibai"]){
        [self showOnleText:@"支付失败" delay:1.5];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Table View -

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return _modelArr.count;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommitOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    CSGoodListModel *sub = [_modelArr safeObjectAtIndex:indexPath.row];
    [cell refresh:sub];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @" ";
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    //    if (section == _modelArr.count-1) {
    //        CommitOrderFoot *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:footId];
    //        //footer.yunfei.text = [NSString stringWithFormat:@"¥:%@",_goods_fee];
    //        footer.price.text = [NSString stringWithFormat:@"¥:%@",_total_fee];
    //        return footer;
    //    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == _modelArr.count-1) {
        return 0.1;
    }
    return 0.1;
}

#pragma mark - 设置大区头、大区尾 -
- (void)setTableHeaderView {
    DefaultAreaView *address = [[NSBundle mainBundle]loadNibNamed:@"DefaultAreaView" owner:nil options:nil].firstObject;
    address.frame = CGRectMake(0, 0, KWIDTH, 70);
    address.backgroundColor = WHITECOLOR;
    address.name.text = @"请先设置默认收货地址";
    address.address.text = @"地址详情";
    [address whenTapped:^{
        // @property (nonatomic, copy) void(^chooseAddressBlock)(HFAddressModel *model);
        
        HFAddressViewController *vc = [[HFAddressViewController alloc]init];
        vc.chooseAddressBlock = ^(HFAddressModel *model) {
            self->_areaId = model.address_id;
            address.name.text = [NSString stringWithFormat:@"%@  %@",model.consignee,model.mobile];
            // address.address.text = [NSString stringWithFormat:@"%@%@",model.area_name,model.detail];
            address.address.text = [NSString stringWithFormat:@"%@",model.building_name];
            
        };
        [self.navigationController pushViewController:vc animated:YES];
    }];
    self.tableView.tableHeaderView = address;
}

- (void)setBottomView {
    UIView *bottomV = [[UIView alloc]initWithFrame:CGRectMake(0, self.tableView.frame.size.height+1, KWIDTH, 59)];
    bottomV.backgroundColor = [UIColor whiteColor];
    
    _numLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
    _numLab.font = [UIFont systemFontOfSize:14];
    _numLab.textColor = [UIColor darkGrayColor];
    [bottomV addSubview:_numLab];
    
    UIButton *commit = [UIButton buttonWithType:UIButtonTypeCustom];
    commit.backgroundColor = RGBA(240, 76, 83,1);
    commit.frame = CGRectMake(KWIDTH-100, 0, 110, 59);
    commit.titleLabel.font = [UIFont systemFontOfSize:16];
    [commit setTitle:@"提交订单" forState:UIControlStateNormal];
    [commit addTarget:self action:@selector(commitACTION) forControlEvents:UIControlEventTouchUpInside];
    //    [[commit rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
    //        if (_areaId == nil) {
    //            ShowToastWithText(@"请选择收货地址");
    //            return;
    //        }
    
    //        NSMutableDictionary *param = [NSMutableDictionary dictionary];
    //        param[@"data"] = _spec_str;
    //        param[@"type"] = _isOrder?@"confirm":@"cart";
    //        param[@"address_id"] = _areaId;
    //        [NewNetWorkManager POST:@"api/order/confirm.html" param:param success:^(id dic) {
    //            NSString *order_sn = dic[@"retval"];
    //
    //            PayOrdersController *vc = [PayOrdersController new];
    //            vc.order_sn = order_sn;
    //            vc.goods_num = _numLab.text;
    //            vc.goods_total = _total_fee;
    //            vc.isFirst = YES;
    //            [self.navigationController pushViewController:vc animated:YES];
    //            ShowToastWithText(dic[@"msg"]);
    //        } other:^(id dic) {
    //
    //        } fail:^(NSError *error) {
    //
    //        } needUser:YES];
    //    }];
    [bottomV addSubview:commit];
    
    _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 10, KWIDTH-220, 40)];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.font = [UIFont systemFontOfSize:16];
    [bottomV addSubview:_priceLabel];
    
    [self.view addSubview:bottomV];
}

-(void)commitACTION{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    param[@"order_sn"] = _order_sn;
    param[@"type"] = _payway;
    
    [NetWorkTool POST:@"repPay/orderPay" param:param success:^(id dic) {
//        NSDictionary *data = dic[@"retval"];
        KMyLog(@"再有一个其那题%@",dic);
        
        if ([[dic objectForKey:@"code"] integerValue] == 1000) {
            
            
            if ([self.payway isEqualToString:@"alipay"]) {
                //默认支付宝
                NSString *appScheme = @"chenghuitong105";
                
                // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
                // NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",
                //                           orderInfoEncoded, signedString];
                NSString *orderString =[NSString stringWithFormat:@"%@",[dic objectForKey:@"data"]];
                // NOTE: 调用支付结果开始支付
                [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
                    
                    NSLog(@"支付宝reslut = %@",resultDic);
                    if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
                        [self showOnleText:[dic objectForKey:@"memo"] delay:1.5];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            //支付成功
                        });
                    }else{
                        [self showOnleText:[dic objectForKey:@"memo"] delay:1.5];
                    }
                }];
            }else   if ([self.payway isEqualToString:@"wechat"]){
                NSMutableDictionary *diccc = [NSMutableDictionary dictionaryWithDictionary:[dic objectForKey:@"data"]];
                
                //微信
                //需要创建这个支付对象
                PayReq *req   = [[PayReq alloc] init];
                //由用户微信号和AppID组成的唯一标识，用于校验微信用户
                req.openID = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"appid"]];
                // 商家id，在注册的时候给的
                req.partnerId = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"partnerid"]];;
                // 预支付订单这个是后台跟微信服务器交互后，微信服务器传给你们服务器的，你们服务器再传给你
                req.prepayId  = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"prepayid"]];;
                // 根据财付通文档填写的数据和签名
                req.package  = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"package"]];;
                // 随机编码，为了防止重复的，在后台生成
                req.nonceStr  = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"noncestr"]];;
                // 这个是时间戳，也是在后台生成的，为了验证支付的
                NSString * stamp = [NSString stringWithFormat:@"%@",[diccc objectForKey:@"timestamp"]];
                req.timeStamp = stamp.intValue;
                // 这个签名也是后台做的
                req.sign = [diccc objectForKey:@"sign"];
                //发送请求到微信，等待微信返回onResp
                [WXApi sendReq:req];
            }else{
                ShowToastWithText(@"付款成功");
                //跳转还没做
                [self zhifuchengg];
            }
        }
        //NSString *goods  = data[@"goods"];
        //        NSString *quantity = data[@"quantity"];
        //        NSString *shipping = data[@"shipping"];
        //        NSString *amount   = data[@"amount"];
        //  _detailLab.text = [NSString stringWithFormat:@"共%@件产品，邮费%@元，共%@元",quantity,shipping,amount];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)zhifuchengg{
    //ShowToastWithText(@"支付成功");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        TabBarViewController *tabbar = [[TabBarViewController alloc]init];
        tabbar.selectedIndex = 0;
        [UIApplication sharedApplication].delegate.window.rootViewController = tabbar;
    });
}

- (void)dealloc {
    NSLog(@"提交订单页面dealloc");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
