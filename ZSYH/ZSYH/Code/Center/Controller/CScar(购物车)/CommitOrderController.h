//
//  CommitOrderController.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "BaseTableController.h"


@interface CommitOrderController : BaseTableController

@property (nonatomic,strong) NSDictionary *response;
@property (nonatomic,  copy) NSString *spec_str;
@property (nonatomic,  copy) NSString *totle;
@property (nonatomic,  copy) NSString *numberstr;

@property (nonatomic,assign) BOOL isOrder;//立即下单
@end

