//
//  CommitOrderCell.m
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "CommitOrderCell.h"

@implementation CommitOrderCell


- (void)refresh:(CSGoodListModel *)model {
//    self.goodName.text = model.goods_name;
//    self.numLab.text = [NSString stringWithFormat:@"x%@",model.quantity];
//    self.price.text = [NSString stringWithFormat:@"￥%@元", model.price];
//    sdimg(self.goodPic,model.goods_images);
//    self.goodAttr.text = model.spec_value;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.goodPic.layer.masksToBounds = YES;
    self.price.adjustsFontSizeToFitWidth = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
