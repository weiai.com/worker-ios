//
//  DefaultAreaView.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFAddressModel.h"

@interface DefaultAreaView : UIView

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIImageView *myImage;

//@property(nonatomic,strong) MyAddressModel *model;
- (void)refresh:(HFAddressModel *)model;

@end
