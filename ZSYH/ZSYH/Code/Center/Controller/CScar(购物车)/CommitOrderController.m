//
//  CommitOrderController.m
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//


//#import "MineReceivingController.h"
#import "HFAddressViewController.h"
#import "CommitOrderController.h"
//#import "PayOrdersController.h"
#import "CommitOrderFoot.h"
#import "CommitOrderCell.h"
#import "WpayOrdViewController.h"
#import "DefaultAreaView.h"
//#import "MyAddressModel.h"
#import "HFAddressModel.h"

@interface CommitOrderController () {
    NSString *_pay;
    NSString *_total_fee;
    NSString *_areaId;
    NSString *_couponId;
    UILabel *_priceLabel;
    UILabel *_numLab;
}
//@property (nonatomic,strong) MyAddressModel *model;
@property (nonatomic,strong) NSMutableArray *modelArr;
@property(nonatomic,strong)HFAddressModel *addressModel;

@end

static NSString *const cellId = @"CommitOrderCell";
static NSString *const footId = @"CommitOrderFoot";
@implementation CommitOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单详情";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    _modelArr = [NSMutableArray array];
    
    self.tableView.backgroundColor = RGBA(243, 243, 243,1);
    self.tableView.rowHeight = 115;
    TableRegisterNib(self.tableView, cellId);
    [self.tableView registerClass:[CommitOrderFoot class] forHeaderFooterViewReuseIdentifier:footId];
    [self.view addSubview:self.tableView];
    
    [self requeslocation];
    [self setBottomView];
        
    
    [self.tableView reloadData];
    _total_fee = _totle;
    NSString *string = [NSString stringWithFormat:@"共计%@件，实付:￥%@",_numberstr, _total_fee];
    NSInteger indd =_numberstr.length+7;
    NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
    [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(indd, _total_fee.length+2)];
    _priceLabel.attributedText = totalss;
    //_numLab.text = [NSString stringWithFormat:@"共%@件产品",_numberstr];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Table View -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _modelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommitOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    CSGoodListModel *sub = [_modelArr safeObjectAtIndex:indexPath.row];
    [cell refresh:sub];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @" ";
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == _modelArr.count-1) {
        CommitOrderFoot *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:footId];
        //footer.yunfei.text = [NSString stringWithFormat:@"¥:%@",_goods_fee];
        footer.price.text = [NSString stringWithFormat:@"¥:%@",_total_fee];
        return footer;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == _modelArr.count-1) {
        return 80;
    }
    return 0.1;
}

#pragma mark - 设置大区头、大区尾 -
- (void)setTableHeaderView {
    DefaultAreaView *address = [[NSBundle mainBundle]loadNibNamed:@"DefaultAreaView" owner:nil options:nil].firstObject;
    address.frame = CGRectMake(0, 0, KWIDTH, 70);
    address.backgroundColor = WHITECOLOR;
    address.name.text = @"请先设置默认收货地址";
    address.address.text = @"地址详情";
    [address whenTapped:^{
        // @property (nonatomic, copy) void(^chooseAddressBlock)(HFAddressModel *model);
        
        HFAddressViewController *vc = [[HFAddressViewController alloc]init];
        vc.chooseAddressBlock = ^(HFAddressModel *model) {
            self->_areaId = model.address_id;
            address.name.text = [NSString stringWithFormat:@"%@  %@",model.consignee,model.mobile];
            // address.address.text = [NSString stringWithFormat:@"%@%@",model.area_name,model.detail];
            address.address.text = [NSString stringWithFormat:@"%@",model.building_name];
            
        };
        [self.navigationController pushViewController:vc animated:YES];
    }];
    self.tableView.tableHeaderView = address;
    if (!strIsEmpty(_addressModel.consignee)) {
        _areaId = _addressModel.address_id;
        address.name.text = [NSString stringWithFormat:@"%@  %@",_addressModel.consignee,_addressModel.mobile];
        // address.address.text = [NSString stringWithFormat:@"%@%@",model.area_name,model.detail];
        address.address.text = [NSString stringWithFormat:@"%@",_addressModel.building_name];
    }
}

- (void)requeslocation{
    [NetWorkTool POST:CmyaddressList param:nil success:^(id dic) {
        NSLog(@"%@", dic);
        NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
        self.addressModel = [[HFAddressModel alloc]init];
        
        for (NSDictionary *dict in dic[@"retval"]) {
            HFAddressModel *model = [[HFAddressModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            [muarr addObject:model];
        }
        for (HFAddressModel *mode in muarr) {
            if (mode.is_default == 1) {
                self.addressModel = mode;
            }
        }
        [self setTableHeaderView];
    } other:^(id dic) {
        [self setTableHeaderView];
    } fail:^(NSError *error) {
        [self setTableHeaderView];
    } needUser:YES];
}

- (void)setBottomView {
    UIView *bottomV = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenH-60, KWIDTH, 59)];
    bottomV.backgroundColor = [UIColor whiteColor];
    
    _numLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
    _numLab.font = [UIFont systemFontOfSize:14];
    _numLab.textColor = [UIColor darkGrayColor];
    [bottomV addSubview:_numLab];
    
    UIButton *commit = [UIButton buttonWithType:UIButtonTypeCustom];
    commit.backgroundColor = RGBA(240, 76, 83,1);
    commit.frame = CGRectMake(KWIDTH-100, 0, 110, 59);
    commit.titleLabel.font =[UIFont systemFontOfSize:16];
    [commit setTitle:@"提交订单" forState:UIControlStateNormal];
    [commit addTarget:self action:@selector(commitACTION) forControlEvents:UIControlEventTouchUpInside];
    //    [[commit rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
    //        if (_areaId == nil) {
    //            ShowToastWithText(@"请选择收货地址");
    //            return;
    //        }
    
    //        NSMutableDictionary *param = [NSMutableDictionary dictionary];
    //        param[@"data"] = _spec_str;
    //        param[@"type"] = _isOrder?@"confirm":@"cart";
    //        param[@"address_id"] = _areaId;
    //        [NewNetWorkManager POST:@"api/order/confirm.html" param:param success:^(id dic) {
    //            NSString *order_sn = dic[@"retval"];
    //
    //            PayOrdersController *vc = [PayOrdersController new];
    //            vc.order_sn = order_sn;
    //            vc.goods_num = _numLab.text;
    //            vc.goods_total = _total_fee;
    //            vc.isFirst = YES;
    //            [self.navigationController pushViewController:vc animated:YES];
    //            ShowToastWithText(dic[@"msg"]);
    //        } other:^(id dic) {
    //
    //        } fail:^(NSError *error) {
    //
    //        } needUser:YES];
    //    }];
    [bottomV addSubview:commit];
    
    _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 10, KWIDTH-220, 40)];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.font = [UIFont systemFontOfSize:16];
    [bottomV addSubview:_priceLabel];
    
    [self.view addSubview:bottomV];
}

-(void)commitACTION{
    WpayOrdViewController *vc = [WpayOrdViewController new];
    vc.order_sn = _spec_str;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)dealloc {
    NSLog(@"提交订单页面dealloc");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
