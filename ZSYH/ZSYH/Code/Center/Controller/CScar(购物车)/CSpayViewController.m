//
//  CSpayViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/27.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSpayViewController.h"
#import "WXPayView.h"
#import "CSgoCommitViewController.h"
#import "CSgoodsViewController.h"
#import "HFShopingCarViewController.h"
#import "CSOrderContentViewController.h"
#import "CSBuyOrderViewController.h"
#import "TabBarViewController.h"
#import "CSBuyDetaileViewController.h"

@interface CSpayViewController (){
    NSString* _payway;
}
@property(nonatomic,strong)UIButton *shwouButton;

@property(nonatomic,strong)UIView *pasuccessView;
@property(nonatomic,copy)NSString *payType;
@property(nonatomic,strong)UIView *bgView;

@end

@implementation CSpayViewController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"支付订单";
    
    [self showDetaile];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];

    [Center addObserver:self selector:@selector(showOrders) name:@"PaySuccess" object:nil];
    [Center addObserver:self selector:@selector(showOrdersno) name:@"PayFail" object:nil];
}

- (void)myshowOnleText:(NSString *)text delay:(NSTimeInterval)delay {
    MBProgressHUD *ghud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    
    UIImage *img = [UIImage imageNamed:@"successapy"];
    ghud.customView = [[UIImageView alloc] initWithImage:img];
    // 再设置模式
    ghud.mode = MBProgressHUDModeCustomView;
    // Set the text mode to show only text.
    ghud.mode = MBProgressHUDModeText;
    ghud.detailsLabel.text = text;
    ghud.detailsLabel.numberOfLines = 0;
    ghud.bezelView.backgroundColor = [UIColor whiteColor];
    ghud.backgroundView.backgroundColor = RGBA(0, 0, 0, 0.2);
    
    ghud.detailsLabel.textColor = K333333;
    // Move to bottm center.
    ghud.center = [UIApplication sharedApplication].keyWindow.center;//屏幕正中心
    ghud.square = NO;

    [ghud hideAnimated:YES afterDelay:delay];
}

- (void)showOrders {
//    @property(nonatomic, assign)NSInteger isLjiBuy;//0  商城来的  1  购物车来的  2 商城订单来的
    if (self.myblockj) {
        self.myblockj(@"dddd");
    }
    
    NSString *sstr = @"返回购物车";
    kWeakSelf;
    
    ZSAlertView *zsAlert = [ZSAlertView new];
    zsAlert.upLable.text = @"支付成功";
    [zsAlert.btn1 setTitle:@"查看订单" forState:(UIControlStateNormal)];
    [zsAlert.btn2 setTitle:@"返回商城" forState:(UIControlStateNormal)];
    zsAlert.btn1Block = ^{
        CSBuyDetaileViewController *buyDetailVC = [CSBuyDetaileViewController new];
        buyDetailVC.orderstr = self.orderSon;
        buyDetailVC.from = @"paySuc";
        buyDetailVC.payType = self.payType;
        [self.navigationController pushViewController:buyDetailVC animated:YES];
    };
    zsAlert.btn2Block = ^{
        TabBarViewController *tbc = [TabBarViewController new];
        tbc.selectedIndex = 2;
        [self presentViewController:tbc animated:YES completion:nil];
    };
    [zsAlert show];
    
//    UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"支付成功" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//    
//    [alertCon addAction:[UIAlertAction actionWithTitle:@"查看订单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        CSBuyDetaileViewController *buyDetailVC = [CSBuyDetaileViewController new];
//        buyDetailVC.orderstr = self.orderSon;
//        buyDetailVC.from = @"paySuc";
//        buyDetailVC.payType = self.payType;
//        [self.navigationController pushViewController:buyDetailVC animated:YES];
//    }]];
//    
//    [alertCon addAction:[UIAlertAction actionWithTitle:@"返回商城" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//        TabBarViewController *tbc = [TabBarViewController new];
//        tbc.selectedIndex = 2;
//        [self presentViewController:tbc animated:YES completion:nil];
//  
//    }]];
//    
//    [self presentViewController:alertCon animated:YES completion:nil];
    
    return;

    if ([singlTool shareSingTool].isLjiBuy == 0) {
        UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"支付成功" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        [alertCon addAction:[UIAlertAction actionWithTitle:@"查看订单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //跳到订单页
            NSDictionary *dict =[[NSDictionary alloc] initWithObjectsAndKeys:@"two",@"one", nil];
            NSNotification *notification =[NSNotification notificationWithName:@"tongzhiRefish" object:nil userInfo:dict];
            //通过通知中心发送通知
            [[NSNotificationCenter defaultCenter] postNotification:notification];
            weakSelf.tabBarController.selectedIndex = 2;
            
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[CSgoodsViewController class]]) {
                    CSgoodsViewController *vc =(CSgoodsViewController *)controller;
                    [weakSelf.navigationController popToViewController:vc animated:YES];
                    return;
                }
            }
        }]];
        [alertCon addAction:[UIAlertAction actionWithTitle:sstr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"确定");
            
//            if ([singlTool shareSingTool].isLjiBuy == 0) {
                for (UIViewController *controller in self.navigationController.viewControllers) {
                    if ([controller isKindOfClass:[CSgoodsViewController class]]) {
                        CSgoodsViewController *vc =(CSgoodsViewController *)controller;
                        [weakSelf.navigationController popToViewController:vc animated:YES];
                        return;
                    }

                }
//            }else if ([singlTool shareSingTool].isLjiBuy == 1) {
//                for (UIViewController *controller in self.navigationController.viewControllers) {
//                    if ([controller isKindOfClass:[HFShopingCarViewController class]]) {
//                        HFShopingCarViewController *vc =(HFShopingCarViewController *)controller;
//                        [singlTool shareSingTool].needReasfh = YES;
//                        [weakSelf.navigationController popToViewController:vc animated:YES];
//                    }
//                }
//            }else if ([singlTool shareSingTool].isLjiBuy == 2) {
//                //            sstr = @"返回订单";
//            }
        }]];
        
        [self presentViewController:alertCon animated:YES completion:nil];
        
    }else if ([singlTool shareSingTool].isLjiBuy == 1) {
        sstr = @"返回购物车";
            ShowToastWithText(@"支付成功");
        [self myshowOnleText:@"支付成功" delay:1.5];
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[HFShopingCarViewController class]]) {
                HFShopingCarViewController *vc =(HFShopingCarViewController *)controller;
                [weakSelf.navigationController popToViewController:vc animated:YES];
                return;

            }

        }
        
    }else if ([singlTool shareSingTool].isLjiBuy == 2) {
        sstr = @"返回订单";
        [self myshowOnleText:@"支付成功" delay:1.5];

        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[CSBuyOrderViewController class]]) {
                CSBuyOrderViewController *vc =(CSBuyOrderViewController *)controller;
                [weakSelf.navigationController popToViewController:vc animated:YES];
                return;
            }
        }
    }

//调到订单页
//    NSDictionary *dict =[[NSDictionary alloc] initWithObjectsAndKeys:@"two",@"one", nil];
//    NSNotification *notification =[NSNotification notificationWithName:@"tongzhiRefish" object:nil userInfo:dict];
//    //通过通知中心发送通知
//    [[NSNotificationCenter defaultCenter] postNotification:notification];
//     self.tabBarController.selectedIndex = 2;
    
//    //创建串行队列  (DISPATCH_QUEUE_SERIAL:串行) (DISPATCH_QUEUE_CONCURRENT:并发)
//    dispatch_queue_t  queue = dispatch_queue_create("com.fw.queue", DISPATCH_QUEUE_SERIAL);
//    dispatch_async(queue, ^{
////        sleep(1);
////        for (UIViewController *controller in self.navigationController.viewControllers) {
////            if ([controller isKindOfClass:[CSgoodsViewController class]]) {
////                CSgoodsViewController *vc =(CSgoodsViewController *)controller;
////                [self.navigationController popToViewController:vc animated:YES];
////            }
////
////        }
//        NSLog(@"任务A  thread:%@",[NSThread currentThread]);
//    });
//
//    dispatch_async(queue, ^{
////        sleep(1);
//        NSLog(@"任务B  thread:%@",[NSThread currentThread]);
////        self.tabBarController.selectedIndex = 2;
//
//    });
//
//    dispatch_async(queue, ^{
////        sleep(1);
//        NSLog(@"任务C  thread:%@",[NSThread currentThread]);
//    });
//
//    dispatch_async(queue, ^{
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            NSLog(@"任务执行完毕，回到主线程 thread:%@",[NSThread currentThread]);
//        });
//
//    });
//    NSLog(@"结束");
//
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"支付成功" message:@"" preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"去评价" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        CSgoCommitViewController *commit = [[CSgoCommitViewController alloc]init];
//        commit.orderid = self.orderid;
//        [self.navigationController pushViewController:commit animated:YES];
//
//    }];
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//
//        if (self.myblockj) {
//            self.myblockj(@"dddd");
//        }
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
//    [alertController addAction:sureAction];
//    [alertController addAction:cancelAction];
//
//    [self presentViewController:alertController animated:YES completion:nil];
//

}

-(void)showOrdersno{
    ShowToastWithText(@"支付失败");
}

-(void)showDetaile{
    
    UIButton *show = [UIButton buttonWithType:(UIButtonTypeCustom)];
    show.frame = CGRectMake(0, kNaviHeight+22, KWIDTH, 24);
    [show setImage:imgname(@"shopCar_select") forState:(UIControlStateNormal)];
    [show setTitle:@"订单已提交" forState:(UIControlStateNormal)];
    [show setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
    self.shwouButton = show;
    [self.view addSubview:show];
    UILabel *shoLb = [[UILabel alloc]initWithFrame:CGRectMake(0, show.bottom+5, KWIDTH, 20)];
    shoLb.text = @"（请在5分钟内完成支付）";
    shoLb.textAlignment = NSTextAlignmentCenter;
    shoLb.textColor = [UIColor colorWithHexString:@"#999999"];
    shoLb.font = FontSize(16);
    [self.view addSubview:shoLb];

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, shoLb.bottom+25, KWIDTH, 48)];
    label.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    label.textColor = KTEXTCOLOR;
    label.text = @"    选择支付方式";
    [self.view addSubview:label];
    
    NSArray *arr = @[@"支付宝",@"微信"];
    NSArray *img = @[@"zhifubao",@"weixin"];
    _payway = @"alipay";
    WXPayView *payV = [[WXPayView alloc]initWithFrame:CGRectMake(0, label.bottom, KWIDTH, 112) array:arr image:img block:^(NSInteger index) {
        
        self->_payway = @[@"alipay",@"wechat",@"balance"][index];
//        NSLog(@"选择了第%ld",index);
//        int num = index == 3?10:1;
//        NSString *string = [NSString stringWithFormat:@"总计￥%.2f%@", _goods_total.floatValue*num, index==3?@"":@"元"];
//        NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
//        [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(2, string.length-2)];
//        _priceLabel.attributedText = totalss;
        
    }];
    [self.view addSubview:payV];
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0, payV.bottom-1, KWIDTH, KHEIGHT)];
    bgview.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:bgview];
    
    UIButton *buildingButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    buildingButton.frame = CGRectMake(30, KHEIGHT-140-48, KWIDTH-60, 48);
    [buildingButton setTitle:@"确认支付" forState:(UIControlStateNormal)];
    [self.view addSubview:buildingButton];
    [buildingButton addTarget:self action:@selector(buildingButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    buildingButton.layer.masksToBounds = YES;
    buildingButton.layer.cornerRadius = 4;
    [buildingButton setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [buildingButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.view addSubview:buildingButton];
}

-(void)buildingButtonAction:(UIButton *)but{
    
    if ([_payway isEqualToString:@"alipay"]) {

        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"orderId"] = NOTNIL(_orderSon);
        [NetWorkTool POST:alipay param:mudic success:^(id dic) {
            ShowToastWithText(@"支付");
            KMyLog(@"ZHIFUBAO%@",dic);
            [PayOrderTool aliPay:[dic objectForKey:@"data"]];
            self.payType = @"支付宝支付";
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        //支付宝支付
    }else if ([_payway isEqualToString:@"wechat"]){
        //微信支付
        
        NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
        mudic[@"orderId"] = NOTNIL(_orderSon);
        [NetWorkTool POST:wechat param:mudic success:^(id dic) {
            ShowToastWithText(@"支付");
            KMyLog(@"ZHIFUBAO%@",dic);
            
            [PayOrderTool wxPay:[dic objectForKey:@"data"]];
            self.payType = @"微信支付";
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
    }
    KMyLog(@"支付");
}

-(NSInteger)getNowTimestamp{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    //设置时区,这个对于时间的处理有时很重要
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    
    NSDate *datenow = [NSDate date];//现在时间
    NSLog(@"设备当前的时间:%@",[formatter stringFromDate:datenow]);
    
    //时间转时间戳的方法:
    NSInteger timeSp = [[NSNumber numberWithDouble:[datenow timeIntervalSince1970]] integerValue];
    NSLog(@"设备当前的时间戳:%ld",(long)timeSp); //时间戳的值
    return timeSp;
}

@end
