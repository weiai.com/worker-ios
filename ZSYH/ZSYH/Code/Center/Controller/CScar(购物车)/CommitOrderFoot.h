//
//  CommitOrderFoot.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommitOrderFoot : UITableViewHeaderFooterView

@property(nonatomic,strong)UILabel *yunfei;

@property(nonatomic,strong)UILabel *price;

@property(nonatomic,strong)UILabel *orderNum;

@property(nonatomic,strong)UILabel *areaName;

@end
