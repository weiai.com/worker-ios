//
//  WpayOrdViewController.h
//  APPStart
//
//  Created by 魏堰青 on 2018/12/5.
//  Copyright © 2018年 WQ. All rights reserved.
//

#import "BaseViewController.h"

@interface WpayOrdViewController : BaseViewController
@property (nonatomic,strong) NSDictionary *response;
@property (nonatomic,  copy) NSString *spec_str;

@property (nonatomic,assign) BOOL isOrder;//立即下单


@property (nonatomic,assign) BOOL isFirst;//是否从提交订单页面跳转来的

//@property (nonatomic,copy) NSString *areaId;
//@property (nonatomic,copy) NSString *order_id;

@property (nonatomic,copy) NSString *order_sn;
@property (nonatomic,copy) NSString *goods_num;
@property (nonatomic,copy) NSString *goods_total;
@property (nonatomic,copy) NSString *zongJia;

@end
