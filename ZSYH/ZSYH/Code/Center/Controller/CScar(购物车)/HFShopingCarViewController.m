//
//  HFShopingCarViewController.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopingCarViewController.h"
#import "HFShopingCarCell.h"
#import "HFShopCarHeaderView.h"
#import "HFShopCarBottomView.h"
#import "HFShopCarGoodsModel.h"
#import "HFShopCarStoreModel.h"
#import "WpayOrdViewController.h"
#import "CommitOrderController.h"
#import "CSorderListViewController.h"
#import "CSShopViewController.h"
#import "CSgoodsViewController.h"

@interface HFShopingCarViewController () <UITableViewDelegate, UITableViewDataSource> {
    BOOL _isDelete;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSouceArr;
@property (nonatomic, assign) CGFloat orderTotalPrice;
@property (nonatomic, strong) NSMutableArray *orderTotalGoods;
@property (nonatomic, strong) NSMutableArray *headerViews;//0
@property (nonatomic, strong) NSMutableArray *stores;//1
@property (nonatomic, strong) HFShopCarBottomView *bottomView;
@property (nonatomic, strong) NSMutableArray *chunzhiArr;
@property (nonatomic, strong) NSMutableDictionary *dataSouceDic;
@property (nonatomic, strong) NSMutableDictionary *curDic;//3
@property (nonatomic, strong) NSString *nemberstr;//4
@property (nonatomic, strong) NSString *curid;//

@property (nonatomic, copy) void(^selectAllBlock)(BOOL select);

@end

@implementation HFShopingCarViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    if (_tableView) {
        [self requestListData];
    }
    [singlTool shareSingTool].isLjiBuy = 1;
    [self.bottomView changeBottomViewTotalPrice:@"0.00" goodsCount:@"0"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _isDelete = NO;
    [self.view addSubview:self.tableView];
    
    [self configNavi];
    [self configBottomBtn];
    [self requestListData];
}

- (void)configNavi {
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.title = @"购物车";
    [self.rightbutton setTitle:@"管理" forState:UIControlStateNormal];
    [self.rightbutton setTitleColor:K333333 forState:UIControlStateNormal];
}

- (void)configBottomBtn {
    HFShopCarBottomView *bottomView = [[HFShopCarBottomView alloc] initWithFrame:CGRectMake(0, ScreenH-kTabbarHeight, ScreenW, 49)];
    [self.view addSubview:bottomView];
    self.bottomView = bottomView;
    kWeakSelf;
    bottomView.selectAllBlock = ^(BOOL select) {
        //先移除所有
        [weakSelf.orderTotalGoods removeAllObjects];
        
        NSLog(@"%@", weakSelf.stores);
        
        for (HFShopCarStoreModel *storeModel in weakSelf.stores) {
            storeModel.isSelect = select;
            NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
            NSArray *goodsArray = [self.dataSouceDic objectForKey:key];
            
            NSLog(@"%@", self.dataSouceDic);
            for (HFShopCarGoodsModel *goodsModel in goodsArray) {
                goodsModel.isSelect = select;
                if (select) {
                    [weakSelf.orderTotalGoods addObject:goodsModel];
                }
            }
        }
        
        if (!select) {
            [weakSelf.orderTotalGoods removeAllObjects];
        }
        
        [weakSelf calculationOrderTotalPrice];
        [weakSelf.tableView reloadData];
    };

    bottomView.deleteOrSettleBtnClock = ^(NSInteger index) {
        if (index == 0) { // 结算
            
            [weakSelf.bottomView changeBottomViewTotalPrice:@"0.00" goodsCount:@"0"];
            
            NSString *str = @"";
            for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
                NSMutableDictionary *temp = [NSMutableDictionary dictionary];
                temp[@"id"]  = model.product_id;
                temp[@"count"]  = model.count;
                temp[@"price"]  = model.product_price;
                str = [NSString stringWithFormat:@"%@,%@",str,model.product_id];
            }
            if (strIsEmpty(str)) {
                ShowToastWithText(@"请至少选择一个产品");
                return;
            }
            str = [str substringFromIndex:1];
            NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
            param[@"id"] = str;
            param[@"carId"] =  [self.curDic objectForKey:@"id"];

            [NetWorkTool POST:getseetShopCar param:param success:^(id dic) {

                CSorderListViewController *vc = [[CSorderListViewController alloc]init];
                
                vc.mydateSource = self.orderTotalGoods;
                vc.dibuCZArr = self.chunzhiArr;
                vc.shopDic =[[dic objectForKey:@"data"] firstObject];
                vc.carid = weakSelf.curid;

                [self.navigationController pushViewController:vc animated:YES];
                
            } other:^(id dic) {
                
            } fail:^(NSError *error) {
                
            } needUser:YES];
            
         
        } else { // 删除
            
            [weakSelf requestDeleteOneCartData];
        }
    };
}

#pragma mark -- 请求接口
/*! 产品列表 */
- (void)requestListData {
    kWeakSelf;
    [NetWorkTool POST:CmycurList param:nil success:^(id dic) {
        
        [self.orderTotalGoods removeAllObjects];
        self.orderTotalPrice = 0;
        [self.headerViews removeAllObjects];
        [self.dataSouceDic removeAllObjects];
        [self.stores removeAllObjects];
        
        NSLog(@"%@", dic);
        KMyLog(@"chsahfiahf%@",[HFTools toJSONString:dic]);
        
        self.curDic = [[dic objectForKey:@"data"] objectForKey:@"shopCar"];
        weakSelf.curid = [[[dic objectForKey:@"data"] objectForKey:@"shopCar"] objectForKey:@"id"];
        /*! 获取购物车店铺个数 */
        NSArray *storeArray = [dic[@"data"] objectForKey:@"products"];
        if ([storeArray isKindOfClass:[NSArray class]]) {
            if (storeArray.count>0) {
            
        for (NSDictionary *dataDic in storeArray) {
            
            // 先创建固定个数的headerView
            HFShopCarHeaderView *header = [[HFShopCarHeaderView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 38)];
            [weakSelf.headerViews addObject:header];
            
            /*! 获取单个店铺的产品个数 */
            NSArray *goodsList = dataDic[@"pros"];
            NSMutableArray *goodsArray = [NSMutableArray arrayWithCapacity:1];
            for (NSDictionary *goodsDic in goodsList) {
                HFShopCarGoodsModel *goodsModel = [[HFShopCarGoodsModel alloc] init];
                [goodsModel setValuesForKeysWithDictionary:goodsDic];
                CGFloat price = [goodsModel.count integerValue] * [goodsModel.product_price floatValue];
                goodsModel.totalPrice = price;
                goodsModel.isSelect = NO;
                [goodsArray addObject:goodsModel];
            }
            
            /*! 获取店铺的信息 */
            HFShopCarStoreModel *storeModel = [[HFShopCarStoreModel alloc] init];
            [storeModel setValuesForKeysWithDictionary:dataDic];
            storeModel.isSelect = NO;
            
            [weakSelf.stores addObject:storeModel];
            
            /*! 将数据保存成字典 */
            NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
            [weakSelf.dataSouceDic setObject:goodsArray forKey:key];
        }
            }}
        [weakSelf.tableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

/*! 删除单个产品 */
- (void)requestDeleteOneCartData {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//        for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
            for (int i = 0; i < self.orderTotalGoods.count ; i++) {
                HFShopCarGoodsModel *model = self.orderTotalGoods[i];
                
            NSMutableDictionary *mudic = [NSMutableDictionary dictionaryWithCapacity:1];
            mudic[@"carId"] =model.car_id;
            mudic[@"count"] =model.count;
            mudic[@"payState"] = @"3";
            mudic[@"id"] =model.id;
            NSDictionary *mydic = @{@"products":mudic};
            NSString *jsonString = [HFTools toJSONString:mydic];
    
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
            dic[@"data"] = jsonString;
            kWeakSelf;
            [NetWorkTool POST:genggaiCur param:dic success:^(id dic) {
              
                NSLog(@"+++++++++=%d", i);
              
                [weakSelf.bottomView changeBottomViewTotalPrice:@"0.00" goodsCount:@"0"];
                if (i == (self.orderTotalGoods.count-1) ) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [weakSelf requestListData];
                    [weakSelf rightClick];
                }
            } other:^(id dic) {
    
            } fail:^(NSError *error) {
    
            } needUser:YES];
        }
}

/*! 批量删除产品 */
- (void)requestDeleteMoreCartData {
    NSMutableArray *idArray = [NSMutableArray arrayWithCapacity:1];
    for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
        [idArray addObject:[NSString stringWithFormat:@"%@", model.car_id]];
    }
    NSString *jsonString = [HFTools toJSONString:idArray];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
    dic[@"cart_ids"] = jsonString;
    kWeakSelf;
    [NetWorkTool POST:genggaiCur param:dic success:^(id dic) {
        NSLog(@"%@", dic);
        [weakSelf requestListData];
        [weakSelf rightClick];
        [weakSelf.bottomView changeBottomViewTotalPrice:@"0.00" goodsCount:@"0"];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark -- click点击事件
- (void)rightClick {
    if (!_isDelete) {
        [self.rightbutton setTitle:@"完成" forState:UIControlStateNormal];
        _isDelete = YES;
    } else {
        [self.rightbutton setTitle:@"管理" forState:UIControlStateNormal];
        _isDelete = NO;
    }
    
    [_bottomView changeDeleteStatus:_isDelete];
}

/* 计算订单总价格及总数量 */
- (void)calculationOrderTotalPrice {
    
    NSLog(@"%@", self.orderTotalGoods);
    
    CGFloat totalPrice = 0;
    for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
        totalPrice += model.totalPrice;
    }
    _orderTotalPrice = totalPrice;
    
    NSString *totalPriceStr = [NSString stringWithFormat:@"¥ %.2f", [[HFTools formatFloat:_orderTotalPrice] floatValue]];
    NSString *totalCountStr = [NSString stringWithFormat:@"%ld", self.orderTotalGoods.count];
    [_bottomView changeBottomViewTotalPrice:totalPriceStr goodsCount:totalCountStr];
}

// 更新底部全选按钮的状态
- (void)updateBottomViewSelectStatus {
    [self.bottomView changeSelectBtnStatus:YES];
    
    for (int i = 0; i < self.stores.count; i++) {
        HFShopCarStoreModel *storeModel = [self.stores objectAtIndex:i];
        if (!storeModel.isSelect) {
            [self.bottomView changeSelectBtnStatus:NO];
            return;
        }
        NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
        NSArray *values = [self.dataSouceDic objectForKey:key];
        for (HFShopCarGoodsModel *goodsModel in values) {
            if (!goodsModel.isSelect) {
                [self.bottomView changeSelectBtnStatus:NO];
                return;
            }
        }
    }
}

#pragma mark -- tableView代理
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_stores count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:section];
    NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
    NSArray *values = [self.dataSouceDic objectForKey:key];
    return values.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HFShopingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HFShopingCarCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // 获取对象模型
    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:indexPath.section];
    NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
    NSArray *values = [self.dataSouceDic objectForKey:key];
    HFShopCarGoodsModel *goods = [values objectAtIndex:indexPath.row];
    [cell configDataWithModel:goods];
    
    kWeakSelf;
    cell.selectBlock = ^(HFShopCarGoodsModel * _Nonnull model) {
        
        if (model.isSelect) {
            if (![weakSelf.orderTotalGoods containsObject:model]) {
                [weakSelf.orderTotalGoods addObject:model];
            }
            
            // 修改店铺的选中状态
            storeModel.isSelect = YES;
            for (HFShopCarGoodsModel *goodsModel in values) {
                if (!goodsModel.isSelect) {
                    storeModel.isSelect = NO;
                }
            }
        } else {
            if ([weakSelf.orderTotalGoods containsObject:model]) {
                [weakSelf.orderTotalGoods removeObject:model];
            }
            // 更改店铺选中状态为NO
            storeModel.isSelect = NO;
        }
        
        [weakSelf updateBottomViewSelectStatus];
        [weakSelf calculationOrderTotalPrice];
        [weakSelf.tableView reloadData];
    };
    
    cell.setCountBlock = ^(HFShopCarGoodsModel * _Nonnull model) {
        [weakSelf calculationOrderTotalPrice];
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    CSMallOrderModel *mdeol = _myHeaderdate[indexPath.section];
    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:indexPath.section];
    NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
    NSArray *values = [self.dataSouceDic objectForKey:key];
    HFShopCarGoodsModel *goods = [values objectAtIndex:indexPath.row];
    
    CSgoodsViewController *vc = [CSgoodsViewController new];
    vc.goodsID = goods.product_id;
    vc.iscur = @"cur";
    //    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController pushViewController:vc animated:YES];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 38;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:section];
    NSString *key = [NSString stringWithFormat:@"%@", storeModel.Id];
    NSArray *values = [self.dataSouceDic objectForKey:key];
    
    HFShopCarHeaderView *header = [self.headerViews objectAtIndex:section];
    [header configDataWithModel:storeModel];
    header.myblock = ^(NSInteger ind) {
        CSShopViewController *jin = [[CSShopViewController alloc]init];
        jin.shopid = storeModel.Id;
        [self.navigationController pushViewController:jin animated:YES];
    };
    kWeakSelf;
    header.selectSectionAllBlock = ^(HFShopCarStoreModel * _Nonnull model) {
        if (model.isSelect) {
            for (HFShopCarGoodsModel *goods in values) {
                goods.isSelect = YES;
                if (![weakSelf.orderTotalGoods containsObject:goods]) {
                    [weakSelf.orderTotalGoods addObject:goods];
                }
            }
            model.isSelect = YES;
        } else {
            for (HFShopCarGoodsModel *goods in values) {
                goods.isSelect = NO;
                if ([weakSelf.orderTotalGoods containsObject:goods]) {
                    [weakSelf.orderTotalGoods removeObject:goods];
                }
            }
            model.isSelect = NO;
        }
        [weakSelf updateBottomViewSelectStatus];
        [weakSelf calculationOrderTotalPrice];
        [weakSelf.tableView reloadData];
    };
    
    return header;
}

#pragma mark -- 懒加载
- (UITableView *)tableView {
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight, ScreenW, ScreenH-kNaviHeight-kTabbarHeight) style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        if (iOS11) {
            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        _tableView.tableFooterView = [UIView new];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.1)];
        _tableView.sectionFooterHeight = 0;
        _tableView.sectionHeaderHeight = 0;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 120;
        _tableView.tableFooterView = [UIView new];
        
        _tableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_tableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [self->_tableView.mj_header beginRefreshing];
        }];
        
        [_tableView registerNib:[UINib nibWithNibName:@"HFShopingCarCell" bundle:nil] forCellReuseIdentifier:@"HFShopingCarCell"];
    }
    return _tableView;
}


- (NSMutableDictionary *)dataSouceDic {
    if (_dataSouceDic == nil) {
        _dataSouceDic = [NSMutableDictionary dictionaryWithCapacity:1];
    }
    return _dataSouceDic;
}


- (NSMutableArray *)orderTotalGoods {
    if (_orderTotalGoods == nil) {
        _orderTotalGoods = [NSMutableArray arrayWithCapacity:1];
    }
    return _orderTotalGoods;
}

- (NSMutableArray *)headerViews {
    if (_headerViews == nil) {
        _headerViews = [NSMutableArray arrayWithCapacity:1];
    }
    return _headerViews;
}

- (NSMutableArray *)stores {
    if (_stores == nil) {
        _stores = [NSMutableArray arrayWithCapacity:1];
    }
    return _stores;
}

@end

