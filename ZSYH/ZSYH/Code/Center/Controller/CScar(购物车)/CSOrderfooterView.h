//
//  CSOrderfooterView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSOrderfooterView : UIView
@property (weak, nonatomic) IBOutlet UIButton *zitiBut;
@property (weak, nonatomic) IBOutlet UIButton *wuliuBUt;
@property (weak, nonatomic) IBOutlet UIButton *didipeisongBut;
@property(nonatomic,copy)void(^myblok)(NSString *type);
@property (weak, nonatomic) IBOutlet UILabel *gongjiLB;
@property (weak, nonatomic) IBOutlet UILabel *hejiLb;


@end

NS_ASSUME_NONNULL_END
