//
//  CSheaderSeationView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSheaderSeationView.h"

@implementation CSheaderSeationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"CSheaderSeationView" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;        
        
    }
    return self;
}

- (IBAction)callaction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(@"");
    }
}


@end
