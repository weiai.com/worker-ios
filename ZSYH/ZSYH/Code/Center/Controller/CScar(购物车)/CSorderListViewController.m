//
//  CSorderListViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSorderListViewController.h"
#import "CSorderListTableViewCell.h"
#import "HFShopCarGoodsModel.h"
#import "DefaultAreaView.h"
#import "HFAddressViewController.h"
#import "CSpayViewController.h"
#import "CSorderHeadermyView.h"
#import "CSOrderfooterView.h"
#import "CSheaderSeationView.h"

@interface CSorderListViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSString *_areaId;
    UILabel *_priceLabel;
}
@property(nonatomic,strong)UITableView *myTableView;

@property(nonatomic,strong)UIImage *headerImage;
@property(nonatomic,strong)HFAddressModel *addressModel;


@property(nonatomic,strong)NSString *totelStr;
@property(nonatomic,strong) CSorderHeadermyView *address;

@property(nonatomic,strong)NSString *send_type;

@property(nonatomic,strong) UIButton  *oldBut;
@end
@implementation CSorderListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#DDDBD7"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.headerImage = [[UIImage alloc]init];
    self.title = @"确认订单";
    [self.myTableView reloadData];
    [self setTableHeaderView];
    
    CGFloat index = 0;
    for (HFShopCarGoodsModel *model in self.mydateSource) {
    
        CGFloat flonum =[ model.count floatValue];
        CGFloat floprice =[ model.product_price floatValue];
        CGFloat fffff = flonum*floprice;
        
        index = index + fffff;
        
    }
    NSString *ibuCZArr = [NSString stringWithFormat:@"%.2f",index];
    self.totelStr =  ibuCZArr;
    NSString *sssss = @"  ";
    
    HFShopCarGoodsModel *model = _mydateSource[0];
    
    _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, KHEIGHT-50, KWIDTH-160, 50)];
    NSString *string = [NSString stringWithFormat:@"共计%@件，实付:¥ %@%@%@",model.count,ibuCZArr,sssss,sssss];
    NSString *infff = [NSString stringWithFormat:@"%ld",_mydateSource.count];
    NSInteger ind =infff.length +7;
    _priceLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
    [totalss addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#EC4900"] range:NSMakeRange(ind, ibuCZArr.length+2)];
    _priceLabel.attributedText = totalss;
    _priceLabel.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_priceLabel];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    if (iPhone5) {
        _priceLabel.font = FontSize(10);
    }
    UILabel *white =[[UILabel alloc]initWithFrame:CGRectMake(_priceLabel.right, KHEIGHT-50, 10, 50)];
    white.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:white];

    UIButton *upBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    upBut.frame = CGRectMake(KWIDTH-150, KHEIGHT-50, 150, 50);
    [self.view addSubview:upBut];
    [upBut setTitle:@"提交订单" forState:(UIControlStateNormal)];
    [upBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [upBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [upBut addTarget:self action:@selector(upAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self requestDataaddress];
    [self shwofootView];
    self.send_type = @"0";
  
}

-(void)shwofootView{
    CSOrderfooterView *footview = [[CSOrderfooterView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 100)];
    footview.backgroundColor = [UIColor whiteColor];
    kWeakSelf;
    footview.myblok = ^(NSString * _Nonnull type) {
        weakSelf.send_type =type;
    };
    HFShopCarGoodsModel *model = _mydateSource[0];

    footview.gongjiLB.text = [NSString stringWithFormat:@"共计%@件",model.count];
    footview.hejiLb.text = [NSString stringWithFormat:@"合计: ¥%@", self.totelStr];
    self.myTableView.tableFooterView = footview;
    
    
}
-(void)upAction:(UIButton *)upBut{
    
    KMyLog(@"提交订单");
    if (!_addressModel) {
        ShowToastWithText(@"请选择地址");
        return;
    }
    NSMutableArray *idarray = [NSMutableArray arrayWithCapacity:1];
    
    for (HFShopCarGoodsModel *model in self.mydateSource) {
        NSMutableDictionary *temp = [NSMutableDictionary dictionary];
        temp[@"id"]  = model.product_id;
        temp[@"count"]  = model.count;
        temp[@"price"]  = model.product_price;

        [idarray addObject: temp];

        
    }
    

    NSDictionary *userdicddd = @{@"userId":@"1",@"addressId":_addressModel.id};
    NSDictionary *orderdic = @{@"privace":self.totelStr ,@"send_type":self.send_type};

    if (!strIsEmpty(_carid)) {
   orderdic = @{@"carId":_carid,@"privace":self.totelStr ,@"send_type":self.send_type};

    }
    
    NSDictionary *myydic = @{@"user":userdicddd,@"products":idarray,@"order":orderdic};
    
    NSData *json = [NSJSONSerialization dataWithJSONObject:myydic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *str = [[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    param[@"data"] = str;
    
    [NetWorkTool POST:MmakeOrder param:param success:^(id dic) {
        KMyLog(@"wode__%@",dic);
        CSpayViewController *vc = [[CSpayViewController alloc]init];
                        NSString *strOrder = [NSString stringWithFormat:@"%@",[dic objectForKey:@"data"]];
        vc.laiyuan = @"cur";
        vc.orderSon =strOrder;
        [self.navigationController pushViewController:vc animated:YES];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSorderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSorderListTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    HFShopCarGoodsModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    if ([model.product_pic hasPrefix:@"http"]) {
        [cell.myImage sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    }else{
        sdimg(cell.myImage, model.product_pic);
        
    }
    cell.nameLB.text = [NSString stringWithFormat:@"%@",model.product_name];
    cell.priceLB.text = [NSString stringWithFormat:@"¥%@",model.product_price];
    cell.numberLB.text = [NSString stringWithFormat:@"*%@",model.count];
    
    cell.wuliuType.text = [NSString stringWithFormat:@"到店自取"];
    cell.wuliuType.hidden = YES;
    
    return cell;
}
- (void)setTableHeaderView {
    
   self.address = [[NSBundle mainBundle]loadNibNamed:@"CSorderHeadermyView" owner:nil options:nil].firstObject;
    _address.frame = CGRectMake(0, 0, KWIDTH, 180);
//  _address.nameLB.text = [NSString stringWithFormat:@"%@",_addressModel.user_name];
//   _address.phoneLB.text = [NSString stringWithFormat:@"%@",_addressModel.phone];
//   _address.addressLB.text = [NSString stringWithFormat:@"%@%@",_addressModel.addressName,_addressModel.address];
//    _areaId = _addressModel.address_id;

    kWeakSelf;
    _address.myblock = ^(NSString * _Nonnull ste) {
        
        HFAddressViewController *vc = [[HFAddressViewController alloc]init];
        vc.chooseAddressBlock = ^(HFAddressModel *model) {
           weakSelf.addressModel = model;
            weakSelf.address.nameLB.text = [NSString stringWithFormat:@"%@",model.user_name];
            weakSelf.address.phoneLB.text = [NSString stringWithFormat:@"%@",model.phone];
            weakSelf.address.addressLB.text = [NSString stringWithFormat:@"%@%@",model.addressName,model.address];

        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    
    
   
    self.myTableView.tableHeaderView = _address;
  
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    CSheaderSeationView *foot = [[CSheaderSeationView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 40)];
    [foot.shopImage sd_setImageWithURL:[NSURL URLWithString:[_shopDic objectForKey:@"shop_image"]] placeholderImage:defaultImg];
    foot.shopname.text =[_shopDic objectForKey:@"shop_name"];
    
    kWeakSelf;
    
    foot.myblock = ^(NSString * _Nonnull str) {
        [HFTools callMobilePhone:[weakSelf.shopDic objectForKey:@"shop_phone"]];
    };
    
    
    return foot;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}
-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight-50) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSorderListTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSorderListTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

- (void)requestDataaddress{
    

    kWeakSelf;
    [NetWorkTool POST:CmyaddressList param:nil success:^(id dic) {
        NSLog(@"%@", dic);
        
        for (NSDictionary *dict in dic[@"data"]) {
            HFAddressModel *model = [[HFAddressModel alloc] init];
            [model setValuesForKeysWithDictionary:dict];
            if (model.is_default == 1) {
                self.addressModel = model;
                self->_areaId = model.address_id;
                weakSelf.address.nameLB.text = [NSString stringWithFormat:@"%@",model.user_name];
                 weakSelf.address.phoneLB.text = [NSString stringWithFormat:@"%@",model.phone];
                weakSelf.address.addressLB.text = [NSString stringWithFormat:@"%@%@",model.addressName,model.address];
            }
        }
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

- (CGFloat)getWidthByText:(NSString *)text font:(UIFont *)font {
    
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MAXFLOAT, 0)];
    label.text = text;
    label.font = font;
    [label sizeToFit];
    CGFloat width = label.frame.size.width;
    return width;
}

@end

