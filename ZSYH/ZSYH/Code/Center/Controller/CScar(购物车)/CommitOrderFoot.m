//
//  CommitOrderFoot.m
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "CommitOrderFoot.h"

@implementation CommitOrderFoot

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.contentView.backgroundColor = RGBA(233, 233, 233,1);
        
        
        NSArray *titles;
        if ([reuseIdentifier isEqualToString:@"CommitOrderFoot"]) {
            titles = @[@"物流运费",@"消费合计"];
        }else {
            titles = @[@"收货地址",@"物流运费",@"订单编号",@"消费合计"];
        }
        
        for (int i = 0; i < titles.count; i++) {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 40*i+0.5, KWIDTH, 39.5)];
            label.backgroundColor = [UIColor whiteColor];
            label.textColor = [UIColor colorWithHexString:@"#666666"];
            label.font = [UIFont systemFontOfSize:14];

            label.text = [NSString stringWithFormat:@"    %@",titles[i]];
            [self.contentView addSubview:label];
        }
        
        for (int i = 0; i < titles.count; i++) {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(100, 40*i+10.5, KWIDTH-110, 20)];
            label.font = [UIFont systemFontOfSize:14];
            label.text = @"￥0";
            label.textColor = [UIColor lightGrayColor];
            label.textAlignment = NSTextAlignmentRight;
            [self.contentView addSubview:label];
            
            if ([reuseIdentifier isEqualToString:@"CommitOrderFoot"]) {
                if (i == 0) {
                    self.yunfei = label;
                }else if (i == 1) {
                    label.textColor = [UIColor redColor];
                    self.price = label;
                }
            }else {
                if (i == 0) {
                    self.areaName = label;
                }else if (i == 1) {
                    self.price = label;
                }else if (i == 2) {
                    self.orderNum = label;
                }else if (i == 3) {
                    label.textColor = [UIColor redColor];
                    self.price = label;
                }
            }
            
        }
    }
    return self;
}

@end
