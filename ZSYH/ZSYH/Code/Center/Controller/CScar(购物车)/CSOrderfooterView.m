//
//  CSOrderfooterView.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/3.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSOrderfooterView.h"

@implementation CSOrderfooterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    self = [[[NSBundle mainBundle] loadNibNamed:@"CSOrderfooterView" owner:self options:nil] lastObject];
    if (self) {
        self.frame = frame;
        
        [self showui];
        
        
    }
    return self;
}
-(void)showui{
    
    self.zitiBut.layer.masksToBounds = YES;
    self.zitiBut.layer.cornerRadius = 4;
    self.zitiBut.layer.borderWidth = 1;
    self.zitiBut.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;

    self.wuliuBUt.layer.masksToBounds = YES;
    self.wuliuBUt.layer.cornerRadius = 4;
    self.wuliuBUt.layer.borderWidth = 1;
    self.wuliuBUt.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;
    
    self.didipeisongBut.layer.masksToBounds = YES;
    self.didipeisongBut.layer.cornerRadius = 4;
    self.didipeisongBut.layer.borderWidth = 1;
    self.didipeisongBut.layer.borderColor = [UIColor colorWithHexString:@"#777777"].CGColor;

    
    [self.zitiBut setBackgroundColor:[UIColor colorWithHexString:@"#777777"]];
    [self.wuliuBUt setBackgroundColor:[UIColor whiteColor]];
    [self.didipeisongBut setBackgroundColor:[UIColor whiteColor]];
    
    [self.zitiBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.wuliuBUt setTitleColor:K666666 forState:(UIControlStateNormal)];
    [self.didipeisongBut setTitleColor:K666666 forState:(UIControlStateNormal)];

    
}

- (IBAction)zitiavtion:(UIButton *)sender {
    [self.zitiBut setBackgroundColor:[UIColor colorWithHexString:@"#777777"]];
    [self.wuliuBUt setBackgroundColor:[UIColor whiteColor]];
    [self.didipeisongBut setBackgroundColor:[UIColor whiteColor]];
    
    [self.zitiBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.wuliuBUt setTitleColor:K666666 forState:(UIControlStateNormal)];
    [self.didipeisongBut setTitleColor:K666666 forState:(UIControlStateNormal)];
    
    if (self.myblok) {
        self.myblok(@"0");
    }
}

- (IBAction)wuliuAction:(UIButton *)sender {
    [self.wuliuBUt setBackgroundColor:[UIColor colorWithHexString:@"#777777"]];
    [self.zitiBut setBackgroundColor:[UIColor whiteColor]];
    [self.didipeisongBut setBackgroundColor:[UIColor whiteColor]];
    
    [self.wuliuBUt setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.zitiBut setTitleColor:K666666 forState:(UIControlStateNormal)];
    [self.didipeisongBut setTitleColor:K666666 forState:(UIControlStateNormal)];
    if (self.myblok) {
        self.myblok(@"1");
    }
}
- (IBAction)didipeiosngAction:(UIButton *)sender {
    
    [self.didipeisongBut setBackgroundColor:[UIColor colorWithHexString:@"#777777"]];
    [self.wuliuBUt setBackgroundColor:[UIColor whiteColor]];
    [self.zitiBut setBackgroundColor:[UIColor whiteColor]];
    
    [self.didipeisongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.wuliuBUt setTitleColor:K666666 forState:(UIControlStateNormal)];
    [self.zitiBut setTitleColor:K666666 forState:(UIControlStateNormal)];
    if (self.myblok) {
        self.myblok(@"2");
    }
}

@end
