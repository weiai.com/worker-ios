//
//  WXPayView.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectBlock)(NSInteger index);

@interface WXPayView : UIView

- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)titles image:(NSArray *)imgs block:(SelectBlock)select;

@end
