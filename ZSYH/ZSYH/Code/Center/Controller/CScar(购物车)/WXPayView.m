//
//  WXPayView.m
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "WXPayView.h"
#import "WXPayCell.h"

@interface WXPayView()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *table;
@property(nonatomic,strong)NSArray *titles;
@property(nonatomic,strong)NSArray *images;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,  copy)SelectBlock block;
@end

static NSString *const cellId = @"WXPayCell";

@implementation WXPayView

- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)titles image:(NSArray *)imgs block:(SelectBlock)select {
    self = [super initWithFrame:frame];
    if (self) {
        _block = select;
        _titles = titles;
        _images = imgs;
        _index = 0;//默认选中0
        [self addSubview:self.table];
    }
    return self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titles.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WXPayCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    
    if (_images && _images.count == _titles.count) {
        cell.icon.image = [UIImage imageNamed:_images[indexPath.row]];
    }
    cell.name.text = _titles[indexPath.row];
    
    if (indexPath.row == _index) {
        cell.kuang.image = [UIImage imageNamed:@"xuanze"];
    }else {
        cell.kuang.image = [UIImage imageNamed:@"shopCar_normal"];
    }
    if ([_titles containsObject:@"支付宝"]) {
        if (indexPath.row == 0) {
            cell.addLB.text = @"支持支付宝余额和银行充值";
        }else  if (indexPath.row == 1){
            cell.addLB.text = @"亿万用户的选择，更快更安全";
            
        }
    }else{
        cell.addLB.hidden = YES;
    }
 
    
    
//    if ([singlTool shareSingTool].isSeleJL) {
//        if (indexPath.row == _index) {
//            cell.kuang.image = [UIImage imageNamed:@"selecter"];
//        }else {
//            cell.kuang.image = [UIImage imageNamed:@"unselecter"];
//        }
//    }else{
//        if (indexPath.row == _index) {
//            cell.kuang.image = [UIImage imageNamed:@"selecter"];
//        }else {
//            cell.kuang.image = [UIImage imageNamed:@"unselecter"];
//        }
//    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _index = indexPath.row;
    [_table reloadData];
    _block(indexPath.row);
}


- (UITableView *)table {
    if (!_table) {
        _table = [[UITableView alloc]initWithFrame:self.bounds];
        _table.backgroundColor = [UIColor whiteColor];
        _table.delegate = self;
        _table.dataSource = self;
        _table.rowHeight = self.height/_titles.count;
        _table.bounces = NO;
//        _table.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_table registerNib:[UINib nibWithNibName:cellId bundle:nil] forCellReuseIdentifier:cellId];
    }
    return _table;
}

@end
