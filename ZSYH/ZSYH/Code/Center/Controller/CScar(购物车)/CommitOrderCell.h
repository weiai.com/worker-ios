//
//  CommitOrderCell.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/29.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSGoodListModel.h"
@interface CommitOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *goodPic;
@property (weak, nonatomic) IBOutlet UILabel *goodName;
@property (weak, nonatomic) IBOutlet UILabel *goodAttr;
@property (weak, nonatomic) IBOutlet UILabel *price;
//@property (weak, nonatomic) IBOutlet UIButton *add;
//@property (weak, nonatomic) IBOutlet UITextField *numTF;
//@property (weak, nonatomic) IBOutlet UIButton *reduce;
@property (weak, nonatomic) IBOutlet UILabel *numLab;

- (void)refresh:(CSGoodListModel *)model;

@end
