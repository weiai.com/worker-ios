//
//  CSorderListViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSorderListViewController : BaseViewController
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSMutableArray *dibuCZArr;
@property(nonatomic,strong)NSDictionary *shopDic;

@property(nonatomic,strong)NSString *orderSon;
@property(nonatomic,strong)NSString *carid;

@end

NS_ASSUME_NONNULL_END
