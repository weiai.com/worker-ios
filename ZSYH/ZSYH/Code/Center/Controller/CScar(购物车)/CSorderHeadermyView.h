//
//  CSorderHeadermyView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSorderHeadermyView : UIView
@property (weak, nonatomic) IBOutlet UILabel *shoujianren;
@property (weak, nonatomic) IBOutlet UILabel *nameLB;
@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *addressLB;
@property(nonatomic,copy)void (^myblock)(NSString *ste);
@property (weak, nonatomic) IBOutlet UILabel *shouhuodizhi;

@end

NS_ASSUME_NONNULL_END
