//
//  CSMyCustomerViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyCustomerViewController.h"
#import "CSMyCustTableViewCell.h"
#import "csMyKehuModel.h"

@interface CSMyCustomerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *myTableView;
@property(nonatomic,strong)NSMutableArray *mydateSource;
@property(nonatomic,strong)NSArray *titArr;

@end

@implementation CSMyCustomerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.title = @"我的客户";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    [self.myTableView reloadData];
    [self requesrt];
   
}

-(void)requesrt{
    kWeakSelf;
    [NetWorkTool POST:Cmyperson param:nil success:^(id dic) {

        self.mydateSource = [csMyKehuModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        
        [weakSelf.myTableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _mydateSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSMyCustTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSMyCustTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    csMyKehuModel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    [cell refashWithmodel:model];
    return cell;
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        _myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
            [self->_myTableView.mj_header beginRefreshing];
        }];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSMyCustTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSMyCustTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

-(NSArray *)titArr{
    if (!_titArr) {
        _titArr = @[@"昵称",@"头像",@"电话",@"地址",@"银行卡信息"];
    }
    return _titArr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
