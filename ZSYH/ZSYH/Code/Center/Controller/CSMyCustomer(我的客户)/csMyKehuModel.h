//
//  csMyKehuModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface csMyKehuModel : BaseModel
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *token_lose_time;
@property (nonatomic, copy) NSString *headimg;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *open_id;
@property (nonatomic, copy) NSString *access_token;
@property (nonatomic, assign) double balance;
@property (nonatomic, copy) NSString *user_name;
@end

NS_ASSUME_NONNULL_END
