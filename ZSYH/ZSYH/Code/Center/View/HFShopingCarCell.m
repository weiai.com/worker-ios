//
//  HFShopingCarCell.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopingCarCell.h"

@interface HFShopingCarCell ()
{
    NSInteger _count;
    NSInteger _maxCount;
}
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextField *contTF;
@property (weak, nonatomic) IBOutlet UIButton *jianBtn;
@property (weak, nonatomic) IBOutlet UIButton *jiaBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (nonatomic, strong) HFShopCarGoodsModel *model;
@property (weak, nonatomic) IBOutlet UILabel *bgLbale;

@end

@implementation HFShopingCarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bgLbale.layer.masksToBounds = YES;
    self.bgLbale.layer.cornerRadius = 2;
    self.bgLbale.layer.borderWidth =1;
    self.bgLbale.layer.borderColor =[UIColor colorWithHexString:@"#979797"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

/*! 增加减少产品个数 */
- (void)requestCartCountData {
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
    [dic setObject:[NSString stringWithFormat:@"%@", self.model.id] forKey:@"spec"];
    [dic setObject:[NSString stringWithFormat:@"%ld", _count] forKey:@"quantity"];
    
    kWeakSelf;

        // 修改购物车个数
        weakSelf.contTF.text = [NSString stringWithFormat:@"%ld", self->_count];
        // 修改模型中个数
        weakSelf.model.count = [NSString stringWithFormat:@"%ld", self->_count];
        // 计算该产品的总价
        CGFloat price = [weakSelf.model.count integerValue] * [weakSelf.model.product_price floatValue];
        weakSelf.model.totalPrice = price;
        // 调用回传block
        if (weakSelf.setCountBlock) {
            weakSelf.setCountBlock(weakSelf.model);
        }
    
}

- (IBAction)selectBtnClick:(UIButton *)sender {
    if (self.selectBlock) {
        sender.selected = !sender.selected;
        self.model.isSelect = sender.selected;
        self.selectBlock(self.model);
    }
}

- (IBAction)jianCountClick:(UIButton *)sender {
    _count --;
    if (_count<1) {
        _count = 1;
    }
    [self requestCartCountData];
}

- (IBAction)jiaCountClick:(UIButton *)sender {
    _count ++;
    [self requestCartCountData];
}

- (void)configDataWithModel:(HFShopCarGoodsModel *)model {
    
    self.model = model;
    _count = [model.count integerValue];

    if ([model.product_pic hasPrefix:@"http"]) {
        [_iconImgView sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    } else {
        sdimg(_iconImgView, model.product_pic);
    }

    _titleLabel.text = [NSString stringWithFormat:@"%@", model.product_name];
    _colorLabel.text = [NSString stringWithFormat:@"%@", model.product_info];
    _priceLabel.text = [NSString stringWithFormat:@"¥%.2f", [model.product_price floatValue]];
    _contTF.text = [NSString stringWithFormat:@"%@", model.count];
    _selectBtn.selected = model.isSelect;
}

@end
