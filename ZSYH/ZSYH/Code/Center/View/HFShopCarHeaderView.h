//
//  HFShopCarHeaderView.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFShopCarStoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HFShopCarHeaderView : UIView

@property (nonatomic, copy) void(^selectSectionAllBlock)(HFShopCarStoreModel *model);
@property (nonatomic, copy) void(^myblock)(NSInteger ind);

- (void)configDataWithModel:(HFShopCarStoreModel *)model;

- (void)changeSelctBtnStatus:(BOOL)select;

@end

NS_ASSUME_NONNULL_END
