//
//  ZYDQCBWCKZDViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/11/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYDQCBWCKZDViewController.h"
#import "SYBigImage.h"

@interface ZYDQCBWCKZDViewController ()
@property(nonatomic,strong) UIScrollView *scrollView;

@property(nonatomic,strong) UIView *bgView;
@property(nonatomic,strong) UIView *whiteBackView;

@property(nonatomic,strong) UILabel *thisFeeTit;     //本单费用-标题
@property(nonatomic,strong) UILabel *thisFeeCon;     //本单费用-内容
@property(nonatomic,strong) UILabel *thisFeeLine;

@property(nonatomic,strong) UILabel *acceNameTit;    //配件名称 标题
@property(nonatomic,strong) UITextView *acceNameCon; //配件名称 内容

@property(nonatomic, strong) UILabel *hourWageTit;   //工时费 标题
@property(nonatomic, strong) UILabel *hourWageCon;   //工时费 内容
@property(nonatomic, strong) UILabel *hourWageLine;  //工时费 分割线

@property(nonatomic, strong) UILabel *warrantyTit;   //质保期 标题
@property(nonatomic, strong) UILabel *warrantyCon;   //质保期 内容
@property(nonatomic, strong) UILabel *warrantyLine;  //质保期 分割线

@property(nonatomic, strong) UILabel *totalFeeTit;   //费用合计 标题
@property(nonatomic, strong) UILabel *totalFeeCon;   //费用合计 内容
@property(nonatomic, strong) UILabel *totalFeeLine;   //费用合计 分割线

@property(nonatomic,strong) UILabel *todaysFirstSingleTit;  //本日首单-标题
@property(nonatomic,strong) UILabel *todaysFirstSingleCon;  //本日首单-内容
@property(nonatomic,strong) UILabel *todaysFirstSingleLine; //本日首单-分割线

@property(nonatomic,strong) UILabel *compPhotTit;  //完工图片 标题
@property(nonatomic,strong) UIView *imageBackView; //放图片的背景
@property(nonatomic,strong) UILabel *imageViewLine;

@property(nonatomic,strong) UILabel *infoServFeeTit; //信息服务费 标题
@property(nonatomic,strong) UILabel *infoServFeeCon; //信息服务费 内容
@property(nonatomic,strong) UILabel *infoServLine;   //信息服务费 分割线

@property(nonatomic,strong) UILabel *techSuppFeeTit;  //技术支持费 标题
@property(nonatomic,strong) UILabel *techSuppFeeCon;  //技术支持费 标题
@property(nonatomic,strong) UILabel *techSuppFeeLine; //技术支持费 标题

@property(nonatomic,strong) UILabel *incomeFromThisOrderTit;  //本单收益-标题
@property(nonatomic,strong) UILabel *incomeFromThisOrderCon;  //本单收益-内容
@property(nonatomic,strong) UILabel *incomeFromThisOrderLine; //本单收益 分割线

@property(nonatomic,strong) UILabel *payTime; //支付时间

@property(nonatomic,strong) NSDictionary *mydic;

@end

@implementation ZYDQCBWCKZDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
    // Do any additional setup after loading the view.
}

-(void)request{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] =NOTNIL(_model.orderId);
    kWeakSelf;
    [NetWorkTool POST:lookListacc param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
        KMyLog(@"电器厂保外订单 查看账单 %@", dic);
        [weakSelf.model setValuesForKeysWithDictionary:[weakSelf.mydic objectForKey:@"order"]];
        [self showDetaileUi];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

-(void)showDetaileUi{
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, KHEIGHT-20)];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview: self.bgView];
    self.bgView.backgroundColor = zhutiColor;
    
    [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];
    
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
    order.textColor = [UIColor whiteColor];
    order.font = FontSize(16);
    order.text = @"订单编号";
    [self.bgView addSubview:order];
    
    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
    orderlb.textColor = [UIColor whiteColor];
    orderlb.font = FontSize(16);
    orderlb.text = [[_mydic objectForKey:@"order"] objectForKey:@"app_order_id"];
    [self.bgView addSubview:orderlb];
    
    UILabel *thisFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, 13, 100, 20)];
    thisFeeTit.textColor = K666666;
    thisFeeTit.font = KFontPingFangSCMedium(14);
    thisFeeTit.text = @"配件总费";
    self.thisFeeTit = thisFeeTit;
    [self.whiteBackView addSubview:thisFeeTit];
    
    UILabel *thisFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, thisFeeTit.top, 100, 20)];
    thisFeeCon.textColor = K666666;
    thisFeeCon.textAlignment = NSTextAlignmentRight;
    thisFeeCon.font = KFontPingFangSCMedium(14);
    thisFeeCon.text = [NSString stringWithFormat:@"¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"proCost"] floatValue]];
    self.thisFeeCon = thisFeeCon;
    [self.whiteBackView addSubview:thisFeeCon];
    
    UILabel *thisFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, thisFeeCon.bottom +13, KWIDTH-20, 1)];
    thisFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.whiteBackView addSubview:thisFeeLine];
    self.thisFeeLine = thisFeeLine;
    
    //配件名称
    UILabel *acceNameTit = [[UILabel alloc]initWithFrame:CGRectMake(24, thisFeeLine.bottom+15, 100, 20)];
    acceNameTit.textColor = K666666;
    acceNameTit.font = KFontPingFangSCMedium(14);
    acceNameTit.text = @"配件名称";
    [self.whiteBackView addSubview:acceNameTit];
    self.acceNameTit = acceNameTit;
    
    UITextView *acceNameCon = [[UITextView alloc] init];
    acceNameCon.frame = CGRectMake(24, acceNameTit.bottom + 11, KWIDTH-20-48, 76);
    acceNameCon.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    acceNameCon.contentSize = CGSizeMake(KWIDTH-20-48, 176);
    acceNameCon.layer.borderWidth = 1;
    acceNameCon.layer.cornerRadius = 8;
    NSString *prodcutComment = [NSString stringWithFormat:@"%@",[[_mydic objectForKey:@"order"] objectForKey:@"prodcutComment"]];
    if ([prodcutComment isEqualToString:@"(null)"]) {
        acceNameCon.text = @"";
    } else {
        acceNameCon.text = prodcutComment;
    }
    acceNameCon.textColor = K999999;
    acceNameCon.font = KFontPingFangSCMedium(14);
    acceNameCon.editable = NO;
    [self.whiteBackView addSubview:acceNameCon];
    self.acceNameCon = acceNameCon;
    
#pragma mark ***查看收益
    if ([_model.order_state integerValue] < 3) {//未支付
        
        self.compPhotTit.frame = CGRectMake(24, acceNameCon.bottom + 16, 100, 14);
        [self showImageView];
        [self imageViewLine];
        [self hourWageTit];
        [self hourWageCon];
        [self hourWageLine];
        
        [self warrantyTit];
        [self warrantyCon];
        [self warrantyLine];
        
        [self totalFeeTit];
        [self totalFeeCon];
        //[self totalFeeLine];
        
        //self.whiteBackView.height = self.compPhotTit.bottom +10 + self.imageBackView.height +10;
        self.whiteBackView.height = self.totalFeeTit.bottom +10;
        
        //未支付 状态
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.whiteBackView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(4, 4)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.whiteBackView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.whiteBackView.layer.mask = maskLayer;
        self.bgView.height = self.whiteBackView.bottom;
    } else {//已支付
        NSString *insurance_premium = [[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"];
        //insurance_premium = @"1";
        if ([insurance_premium floatValue] > 0) {//如果是本日首单
            
            self.compPhotTit.frame = CGRectMake(24, acceNameCon.bottom + 16, 100, 14);
            [self showImageView];
            [self imageViewLine];
            [self hourWageTit];
            [self hourWageCon];
            [self hourWageLine];
            
            [self warrantyTit];
            [self warrantyCon];
            [self warrantyLine];
            
            [self totalFeeTit];
            [self totalFeeCon];
            [self totalFeeLine];
            self.todaysFirstSingleTit.frame = CGRectMake(24, self.totalFeeLine.bottom +13, (KWIDTH-48-20)/2, 20);
            [self todaysFirstSingleCon];//加载出来
            [self todaysFirstSingleLine];
            self.infoServFeeTit.frame = CGRectMake(24, self.todaysFirstSingleCon.bottom + 13 +10, (KWIDTH-48-20)/2,20);
            self.infoServFeeCon.frame = CGRectMake(24 + (KWIDTH-48-20)/2, self.todaysFirstSingleCon.bottom + 13 +10, (KWIDTH-48-20)/2, 20);
            [self infoServLine];
            self.techSuppFeeTit.frame = CGRectMake(24, self.infoServLine.bottom +13, (KWIDTH-48-20)/2, 20);
            self.techSuppFeeCon.frame = CGRectMake(24 + (KWIDTH-48-20)/2, self.infoServLine.bottom + 13, (KWIDTH-48-20)/2, 20);
            [self techSuppFeeLine];
            self.incomeFromThisOrderTit.frame = CGRectMake(24, self.techSuppFeeLine.bottom + 13, 100, 20);
            [self incomeFromThisOrderCon];
            self.whiteBackView.height = self.incomeFromThisOrderCon.bottom + 13;
            self.bgView.height = self.whiteBackView.bottom;
            [self payTime];
            self.whiteBackView.height = self.incomeFromThisOrderTit.bottom + 13;
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.whiteBackView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(4, 4)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.whiteBackView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.whiteBackView.layer.mask = maskLayer;
            self.bgView.height = self.whiteBackView.bottom;
            self.scrollView.contentSize = CGSizeMake(KWIDTH, self.bgView.height + 45);
            
        } else {//不是本日首单
            self.compPhotTit.frame = CGRectMake(24, acceNameCon.bottom + 16, 100, 14);
            [self showImageView];
            [self imageViewLine];
            [self hourWageTit];
            [self hourWageCon];
            [self hourWageLine];
            
            [self warrantyTit];
            [self warrantyCon];
            [self warrantyLine];
            
            [self totalFeeTit];
            [self totalFeeCon];
            [self totalFeeLine];
            self.infoServFeeTit.frame = CGRectMake(24, self.totalFeeLine.bottom + 13, (KWIDTH-48-20)/2,20);
            self.infoServFeeCon.frame = CGRectMake(24 + (KWIDTH-48-20)/2, self.totalFeeLine.bottom + 13, (KWIDTH-48-20)/2, 20);
            [self infoServLine];
            self.techSuppFeeTit.frame = CGRectMake(24, self.infoServLine.bottom +13, (KWIDTH-48-20)/2, 20);
            self.techSuppFeeCon.frame = CGRectMake(24 + (KWIDTH-48-20)/2, self.infoServLine.bottom + 13, (KWIDTH-48-20)/2, 20);
            [self techSuppFeeLine];
            self.incomeFromThisOrderTit.frame = CGRectMake(24, self.techSuppFeeLine.bottom + 13, 100, 14);
            [self incomeFromThisOrderCon];
            //[self myShouYiLine];
            self.whiteBackView.height = self.incomeFromThisOrderCon.bottom +20;
            self.bgView.height = self.whiteBackView.bottom;
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.whiteBackView.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(4, 4)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.whiteBackView.bounds;
            maskLayer.path = maskPath.CGPath;
            self.whiteBackView.layer.mask = maskLayer;
            self.bgView.height = self.whiteBackView.bottom;
            [self payTime];
            self.scrollView.contentSize = CGSizeMake(KWIDTH, self.bgView.height + 45);
        }
    }
}

#pragma mark - 白色背景
- (UIView *)whiteBackView {
    if (!_whiteBackView) {
        UIView *upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 100)];
        upLable.backgroundColor = [UIColor whiteColor];
        upLable.userInteractionEnabled = YES;
        _whiteBackView = upLable;
        [self.bgView addSubview:upLable];
    }
    return _whiteBackView;
}

#pragma mark - 完工照片
- (UILabel *)compPhotTit {
    if (!_compPhotTit) {
        UILabel *compPhotTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.acceNameCon.bottom + 13, 100, 14)];
        compPhotTit.textColor = K666666;
        compPhotTit.font = KFontPingFangSCMedium(14);
        compPhotTit.text = @"完工照片";
        [self.whiteBackView addSubview:compPhotTit];
        _compPhotTit = compPhotTit;
    }
    return _compPhotTit;
}

#pragma mark - 信息服务费 标题
- (UILabel *)infoServFeeTit {
    if (!_infoServFeeTit) {
        UILabel *infoServFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.imageViewLine.bottom + 13, (KWIDTH-48-20)/2, 20)];
        infoServFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        infoServFeeTit.font = KFontPingFangSCMedium(14);
        infoServFeeTit.text = @"信息服务费（10%）";
        [self.whiteBackView addSubview:infoServFeeTit];
        _infoServFeeTit = infoServFeeTit;
    }
    return _infoServFeeTit;
}

#pragma mark - 信息服务费 内容
- (UILabel *)infoServFeeCon {
    if (!_infoServFeeCon) {
        UILabel *infoServFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 +(KWIDTH-48-20)/2, self.imageViewLine.bottom + 16, (KWIDTH-48-20)/2, 20)];
        infoServFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        infoServFeeCon.textAlignment = NSTextAlignmentRight;
        infoServFeeCon.font = KFontPingFangSCMedium(14);
        
        CGFloat earPro = 0;
        CGFloat earCit = 0;
        CGFloat earPla = 0;
        CGFloat sum = 0;
        
        NSString *earProStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earProvince"] floatValue]];
        earPro = [earProStr floatValue];
        NSString *earCitStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earCity"] floatValue]];
        earCit = [earCitStr floatValue];
        NSString *earPlaStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earPlatform"] floatValue]];
        earPla = [earPlaStr floatValue];
        sum = earPro + earCit + earPla;
        infoServFeeCon.text = [NSString stringWithFormat:@"-￥ %.2f", sum];
        [self.whiteBackView addSubview:infoServFeeCon];
        _infoServFeeCon = infoServFeeCon;
    }
    return _infoServFeeCon;
}

#pragma mark - 信息服务费 分割线
- (UILabel *)infoServLine {
    if (!_infoServLine) {
        UILabel *infoServLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.infoServFeeTit.bottom+13, KWIDTH-20, 1)];
        infoServLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _infoServLine = infoServLine;
        [self.whiteBackView addSubview:infoServLine];
    }
    return _infoServLine;
}

#pragma mark - 技术支持费 标题
- (UILabel *)techSuppFeeTit {
    if (!_techSuppFeeTit) {
        UILabel *techSuppFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.infoServLine.bottom + 13, (KWIDTH-48-20)/2, 20)];
        techSuppFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        techSuppFeeTit.font = KFontPingFangSCMedium(14);
        techSuppFeeTit.text = @"技术支持费（6%）";
        [self.whiteBackView addSubview:techSuppFeeTit];
        _techSuppFeeTit = techSuppFeeTit;
    }
    return _techSuppFeeTit;
}

#pragma mark - 技术支持费 内容
- (UILabel *)techSuppFeeCon {
    if (!_techSuppFeeCon) {
        UILabel *techSuppFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 +(KWIDTH-48-20)/2, self.infoServLine.bottom + 13, (KWIDTH-48-20)/2, 20)];
        techSuppFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        techSuppFeeCon.textAlignment = NSTextAlignmentRight;
        techSuppFeeCon.font = KFontPingFangSCMedium(14);
        techSuppFeeCon.text = [NSString stringWithFormat:@"-¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"tax"]];
        [self.whiteBackView addSubview:techSuppFeeCon];
        _techSuppFeeCon = techSuppFeeCon;
    }
    return _techSuppFeeCon;
}

#pragma mark - 技术支持费 分割线
- (UILabel *)techSuppFeeLine {
    if (!_techSuppFeeLine) {
        UILabel *techSuppFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.techSuppFeeTit.bottom+13, KWIDTH-20, 1)];
        techSuppFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _techSuppFeeLine = techSuppFeeLine;
        [self.whiteBackView addSubview:techSuppFeeLine];
    }
    return _techSuppFeeLine;
}

#pragma mark - 订单收益-标题
- (UILabel *)incomeFromThisOrderTit {
    if (!_incomeFromThisOrderTit) {
        UILabel *incomeFromThisOrderTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.imageViewLine.bottom + 16, 100, 20)];
        incomeFromThisOrderTit.textColor = zhutiColor;
        incomeFromThisOrderTit.font = KFontPingFangSCMedium(14);
        incomeFromThisOrderTit.text = @"本单收益";
        _incomeFromThisOrderTit = incomeFromThisOrderTit;
        [self.whiteBackView addSubview:incomeFromThisOrderTit];
    }
    return _incomeFromThisOrderTit;
}

#pragma mark - 订单收益-内容
- (UILabel *)incomeFromThisOrderCon {
    if (!_incomeFromThisOrderCon) {
        UILabel *incomeFromThisOrderCon = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.incomeFromThisOrderTit.top, 100, 20)];
        incomeFromThisOrderCon.textColor = zhutiColor;
        incomeFromThisOrderCon.textAlignment = NSTextAlignmentRight;
        incomeFromThisOrderCon.font = KFontPingFangSCMedium(14);
        incomeFromThisOrderCon.text = [NSString stringWithFormat:@"¥ %.2f",[[_mydic  objectForKey:@"earning"] floatValue]];
        [self.whiteBackView addSubview:incomeFromThisOrderCon];
        self.whiteBackView.height = incomeFromThisOrderCon.bottom+100;
        _incomeFromThisOrderCon = incomeFromThisOrderCon;
    }
    return _incomeFromThisOrderCon;
}

#pragma mark - 订单收益-分割线
- (UILabel *)incomeFromThisOrderLine {
    if (!_incomeFromThisOrderLine) {
        UILabel *incomeFromThisOrderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.incomeFromThisOrderTit.bottom+10, KWIDTH-20, 1)];
        incomeFromThisOrderLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _incomeFromThisOrderLine = incomeFromThisOrderLine;
        [self.whiteBackView addSubview:incomeFromThisOrderLine];
    }
    return _incomeFromThisOrderLine;
}

#pragma mark - 本日首单-标题
- (UILabel *)todaysFirstSingleTit {
    if (!_todaysFirstSingleTit) {
        UILabel *todaysFirstSingleTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.imageViewLine.bottom +13, (KWIDTH-48-20)/2, 20)];
        todaysFirstSingleTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        todaysFirstSingleTit.font = KFontPingFangSCMedium(14);
        todaysFirstSingleTit.text = @"当日首单-为您投保";
        _todaysFirstSingleTit = todaysFirstSingleTit;
        [self.whiteBackView addSubview:todaysFirstSingleTit];
    }
    return _todaysFirstSingleTit;
}

#pragma mark - 本日首单-内容
- (UILabel *)todaysFirstSingleCon {
    if (!_todaysFirstSingleCon) {
        UILabel *todaysFirstSingleCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-48-20)/2, self.todaysFirstSingleTit.top, (KWIDTH-48-20)/2, 20)];
        todaysFirstSingleCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        todaysFirstSingleCon.textAlignment = NSTextAlignmentRight;
        todaysFirstSingleCon.font = KFontPingFangSCMedium(14);
        todaysFirstSingleCon.text = [NSString stringWithFormat:@"-¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"] floatValue] ];
        _todaysFirstSingleCon = todaysFirstSingleCon;
        [self.whiteBackView addSubview:todaysFirstSingleCon];
    }
    return _todaysFirstSingleCon;
}

#pragma mark - 本日首单-分割线
- (UILabel *)todaysFirstSingleLine {
    if (!_todaysFirstSingleLine) {
        UILabel *todaysFirstSingleLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.todaysFirstSingleTit.bottom + 13, KWIDTH-20, 1)];
        todaysFirstSingleLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.whiteBackView addSubview:todaysFirstSingleLine];
    }
    return _todaysFirstSingleLine;
}

#pragma mark - 完工图片 -背景View
- (UIView *)imageBackView {
    if (!_imageBackView) {
        UIView *imageBackView = [[UIView alloc] init];
        imageBackView.frame = CGRectMake(0, self.compPhotTit.bottom + 10, kScreen_Width, 60);
        imageBackView.backgroundColor = [UIColor clearColor];
        _imageBackView = imageBackView;
        [self.whiteBackView addSubview:imageBackView];
    }
    return _imageBackView;
}

#pragma mark - 图片-分割线
- (UILabel *)imageViewLine {
    if (!_imageViewLine) {
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.imageBackView.bottom +10, KWIDTH-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _imageViewLine = line1;
        [self.whiteBackView addSubview:line1];
    }
    return _imageViewLine;
}

#pragma mark - 工时费 标题
- (UILabel *)hourWageTit {
    if (!_hourWageTit) {
        UILabel *hourWageTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.imageViewLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        hourWageTit.textColor = K666666;
        hourWageTit.font = KFontPingFangSCMedium(14);
        hourWageTit.text = @"工时费";
        [self.whiteBackView addSubview:hourWageTit];
        self.hourWageTit = hourWageTit;
    }
    return _hourWageTit;
}

#pragma mark - 工时费 内容
- (UILabel *)hourWageCon {
    if (!_hourWageCon) {
        UILabel *hourWageCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-20-48)/2, self.imageViewLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        hourWageCon.textColor = K666666;
        hourWageCon.font = KFontPingFangSCMedium(14);
        hourWageCon.text = [NSString stringWithFormat:@"¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"peopleCost"] floatValue]];
        hourWageCon.textAlignment = NSTextAlignmentRight;
        [self.whiteBackView addSubview:hourWageCon];
        self.hourWageCon = hourWageCon;
    }
    return _hourWageCon;
}

#pragma mark - 工时费 分割线
- (UILabel *)hourWageLine {
    if (!_hourWageLine) {
        UILabel *hourWageLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.hourWageTit.bottom +8, KWIDTH-20, 1)];
        hourWageLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.whiteBackView addSubview:hourWageLine];
        self.hourWageLine = hourWageLine;
    }
    return _hourWageLine;
}

#pragma mark - 质保期 标题
- (UILabel *)warrantyTit {
    if (!_warrantyTit) {
        UILabel *warrantyTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.hourWageLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        warrantyTit.textColor = K666666;
        warrantyTit.font = KFontPingFangSCMedium(14);
        warrantyTit.text = @"质保期";
        [self.whiteBackView addSubview:warrantyTit];
        self.warrantyTit = warrantyTit;
    }
    return _warrantyTit;
}

#pragma mark - 质保期 内容
- (UILabel *)warrantyCon {
    if (!_warrantyCon) {
        UILabel *warrantyCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-20-48)/2, self.hourWageLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        warrantyCon.textColor = K666666;
        warrantyCon.font = KFontPingFangSCMedium(14);
        warrantyCon.text = [NSString stringWithFormat:@"%.f %@",[[[_mydic objectForKey:@"order"] objectForKey:@"warranty"] floatValue], @"个月"];
        warrantyCon.textAlignment = NSTextAlignmentRight;
        [self.whiteBackView addSubview:warrantyCon];
        self.warrantyCon = warrantyCon;
    }
    return _warrantyCon;
}

#pragma mark - 质保期 分割线
- (UILabel *)warrantyLine {
    if (!_warrantyLine) {
        UILabel *warrantyLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.warrantyTit.bottom +8, KWIDTH-20, 1)];
        warrantyLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.whiteBackView addSubview:warrantyLine];
        self.warrantyLine = warrantyLine;
    }
    return _warrantyLine;
}

#pragma mark - 费用合计 标题
- (UILabel *)totalFeeTit {
    if (!_totalFeeTit) {
        UILabel *totalFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.warrantyLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        totalFeeTit.textColor = K666666;
        totalFeeTit.font = KFontPingFangSCMedium(14);
        totalFeeTit.text = @"费用合计";
        [self.whiteBackView addSubview:totalFeeTit];
        self.totalFeeTit = totalFeeTit;
    }
    return _totalFeeTit;
}

#pragma mark - 费用合计 内容
- (UILabel *)totalFeeCon {
    if (!_totalFeeCon) {
        UILabel *totalFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-20-48)/2, self.warrantyLine.bottom+12, (KWIDTH-20-48)/2, 20)];
        totalFeeCon.textColor = K666666;
        totalFeeCon.font = KFontPingFangSCMedium(14);
        totalFeeCon.text = [NSString stringWithFormat:@"¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"all_cost"] floatValue]];
        totalFeeCon.textAlignment = NSTextAlignmentRight;
        [self.whiteBackView addSubview:totalFeeCon];
        self.totalFeeCon = totalFeeCon;
    }
    return _totalFeeCon;
}

#pragma mark - 费用合计 分割线
- (UILabel *)totalFeeLine {
    if (!_totalFeeLine) {
        UILabel *totalFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.totalFeeTit.bottom +8, KWIDTH-20, 1)];
        totalFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [self.whiteBackView addSubview:totalFeeLine];
        self.totalFeeLine = totalFeeLine;
    }
    return _totalFeeLine;
}

#pragma mark - 支付时间
- (UILabel *)payTime {
    if (!_payTime) {
        UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(22, self.bgView.bottom+6, KWIDTH-44, 13)];
        payTime.text = [NSString stringWithFormat:@"支付时间: %@",_model.pay_time];
        payTime.textColor = [UIColor colorWithHexString:@"#232620"];
        payTime.font = KFontPingFangSCMedium(14);
        payTime.textAlignment = NSTextAlignmentRight;
        _payTime = payTime;
        [self.scrollView addSubview:payTime];
    }
    return _payTime;
}

#pragma mark - 显示图片
- (void)showImageView {
    NSMutableArray *imagearr = [[_mydic objectForKey:@"imgs"] mutableCopy];
    CGFloat wei = (self.scrollView.frame.size.width - 25-46-46)/4;
    
    if (imagearr.count > 0) {
        [self.imageBackView removeAllSubviews];
        for (int i = 0; i < imagearr.count; i++) {
            UIImageView *myimage = [[UIImageView alloc]init];
            [self.imageBackView addSubview:myimage];
            [myimage sd_setImageWithURL:[NSURL URLWithString:[imagearr[i] objectForKey:@"img"]] placeholderImage:defaultImg];
            myimage.frame = CGRectMake(24+(wei+10)*(i % 4), (wei + 10) * (i / 4), wei, wei);
            myimage.userInteractionEnabled = YES;
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [myimage addGestureRecognizer:bigI];
            self.imageBackView.frame = CGRectMake(0, self.compPhotTit.bottom + 10, self.whiteBackView.width, (wei + 10) * (i / 4 + 1));
        }
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
