//
//  ZYDQCDQAZCKZDViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/11/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//  电器厂订单 电器安装 查看账单

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYDQCDQAZCKZDViewController : BaseViewController
@property (nonatomic, strong)CSmaintenanceListModel *model;

@end

NS_ASSUME_NONNULL_END
