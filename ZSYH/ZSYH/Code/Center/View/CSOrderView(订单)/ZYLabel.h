//
//  ZYLabel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/9/5.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@interface ZYLabel : UILabel {
    
VerticalAlignment _verticalAlignment;
    
}
@property (nonatomic) VerticalAlignment verticalAlignment;
@end

NS_ASSUME_NONNULL_END
