//
//  ZYCheckBillViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYCheckBillViewController.h"
#import "TanKuangNoseleview.h"
#import "CSOrderContentViewController.h"

@interface ZYCheckBillViewController ()
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLb; //订单编号
@property (weak, nonatomic) IBOutlet UITextField *inspectionFeesTF; //检测费输入框
@property (weak, nonatomic) IBOutlet UILabel *tatolLb; //合计费用
@property (weak, nonatomic) IBOutlet UIButton *pushBtn;
@property (nonatomic, strong) TanKuangNoseleview *tankiuiang;
@property (nonatomic, strong) UIView *bgViewsec; //个人维修
@property (nonatomic, strong) UIView *bgViewthr; //电器厂维修保外

@end

@implementation ZYCheckBillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成检测费账单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.inspectionFeesTF.keyboardType = UIKeyboardTypeNumberPad;
    self.inspectionFeesTF.placeholder = @"请输入检测费";
    self.inspectionFeesTF.font = [UIFont systemFontOfSize:14];
    self.inspectionFeesTF.delegate = self;
    [self.inspectionFeesTF addTarget:self action:@selector(textFieldChange:) forControlEvents:UIControlEventValueChanged];
    [self.inspectionFeesTF becomeFirstResponder];
    [self.inspectionFeesTF addRules];
    
    //self.orderNumber.text = _orderNumberStr;
    self.orderNumberLb.text = _mymodel.Id;
    self.tatolLb.text = [NSString stringWithFormat:@"合计:¥0.00"];
    
    [self shwoBgviewsec]; //个人维修 推送弹窗
    // Do any additional setup after loading the view from its nib.
}

//推送账单 点击事件
- (IBAction)pushBtnAction:(id)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (strIsEmpty(_inspectionFeesTF.text)) {
        ShowToastWithText(@"请输入检测费");
        return;
    }
    if (_inspectionFeesTF.text.floatValue < 30) {
        ShowToastWithText(@"当前总费用合计小于30元");
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    param[@"peopleCost"] = _inspectionFeesTF.text;
    param[@"orderId"] = _mymodel.orderId;
    param[@"type"] = @"1";
    param[@"productCost"] = @"";
    param[@"prodcutComment"] = @"";
    
    //NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    //param[@"products"] =muarr;
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] =json;
    kWeakSelf;
    
    [NetWorkTool POST:OrderCreateBill param:allDic success:^(id dic) {
        
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if ([dic[@"errorCode"] intValue] == 0) {
//            NSString *msg = dic[@"msg"];
//            ShowToastWithText(msg);
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [self.navigationController popToRootViewControllerAnimated:YES];
//            });
//        }
        //        NSMutableDictionary *muarr = [NSMutableDictionary dictionaryWithCapacity:1];
        //        muarr[@"repOrderId"] = weakSelf.orderNumberStr;
        //        [NetWorkTool POST:tuisongzhanbgd param:muarr success:^(id dic) {
        //            ShowToastWithText(@"添加成功");
        //            [self.navigationController popViewControllerAnimated:YES];
        //            if (self.myblockj) {
        //                self.myblockj(@"");
        //            }
        //            [MBProgressHUD hideHUDForView:self.view animated:YES];
        //        } other:^(id dic) {
        //        } fail:^(NSError *error) {
        //        } needUser:YES];
        
//        [self.tankiuiang removeFromSuperview];
//        self.navigationController.tabBarController.selectedIndex = 1;
//        [self.navigationController popToRootViewControllerAnimated:NO];
        
//        [self shwoBgviewsec];
//        [singlTool shareSingTool].needReasfh = YES;
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    self.tatolLb.text = [NSString stringWithFormat:@"合计:¥%.2f",[textField.text floatValue]];
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    if (textField == self.inspectionFeesTF) {
//        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
//        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
//            ShowToastWithText(@"只能输入数字");
//            return NO;
//        } else {
//            return YES;
//        }
//    } else {
//        return YES;
//    }
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == self.inspectionFeesTF) {
        if(string.length == 0)
            return YES;
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if(existedLength - selectedLength + replaceLength > 5) {
            return NO;
        }
    }
    return YES;
}

- (void)textFieldChange:(UITextField *)textField {
    self.tatolLb.text = [NSString stringWithFormat:@"合计:¥%.2f",[textField.text floatValue]];
}

/**
 弹出框的背景图 推送成功 个人维修/电器厂保外
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*为保障您的收益\n请您确认用户支付费用后离开" originalColor:K333333 originalFont:FontSize(16) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(16)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.numberOfLines = 0;
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.attributedText = attStr;
    DowLable.textAlignment =  NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikenow:(UIButton *)but{
    [self.bgViewsec removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
    [singlTool shareSingTool].needReasfh = YES;
//    for (UIViewController *controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[CSOrderContentViewController class]]) {
//            CSOrderContentViewController *vc = (CSOrderContentViewController *)controller;
//            [self.navigationController popToViewController:vc animated:YES];
//        }
//    }
//        if (self.myblock) {
//            self.myblock(@"");
//        }
//        [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
