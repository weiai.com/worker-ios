//
//  ZYPersonCleanCKZDViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYPersonCleanCKZDViewController : BaseViewController
@property (nonatomic, strong)CSmaintenanceListModel *model;

@end

NS_ASSUME_NONNULL_END
