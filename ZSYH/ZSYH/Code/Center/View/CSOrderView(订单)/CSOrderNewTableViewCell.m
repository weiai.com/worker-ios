//
//  CSOrderNewTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSOrderNewTableViewCell.h"

@implementation CSOrderNewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgImage.layer.masksToBounds = YES;
    self.bgImage.layer.cornerRadius = 8;
    self.bgImage.layer.borderWidth = 1;
    self.bgImage.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    
    self.userTypeLb.font = KFontPingFangSCMedium(12);
    self.timeLB.font = KFontPingFangSCMedium(10);
    self.orderStateLB.font = KFontPingFangSCMedium(13);
    self.serviceTypeLB.font = KFontPingFangSCMedium(13);
    self.productTypeLB.font = KFontPingFangSCMedium(13);
    
    self.contentLB.font = KFontPingFangSCMedium(13);
    self.servicetimeLB.font = KFontPingFangSCMedium(13);
    self.AddressLB.font = KFontPingFangSCMedium(13);
    
    self.serviceTypeTit.font = KFontPingFangSCMedium(13);
    self.elecCateTit.font = KFontPingFangSCMedium(13);
    self.serviceTimeTit.font = KFontPingFangSCMedium(13);
    self.serviceAddTit.font = KFontPingFangSCMedium(13);
}

-(void)refasf:(CSmaintenanceListModel *)model{
    
    self.orderStateLB.textColor = [UIColor colorWithHexString:@"#F88B1F"];
    self.bgImage.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    
    self.DQCImage.hidden = YES;
    
    if ([model.user_type integerValue] == 2) {
        self.DQCImage.hidden = NO;
    }
    self.timeLB.text = [NSString stringWithFormat:@"%@",model.create_time];
    self.orderStateLB.text = [NSString stringWithFormat:@"%@",model.order_state];
    self.serviceTypeLB.text = [NSString stringWithFormat:@"%@",model.service_name];
    self.productTypeLB.text = [NSString stringWithFormat:@"%@",model.fault_name];
    self.contentLB.text = [NSString stringWithFormat:@"%@",model.fault_comment];
    self.AddressLB.text = [NSString stringWithFormat:@"%@", model.user_address];
    //0:个人用户
    if ([model.user_type isEqualToString:@"0"]) {
        [self.userTypeImg setImage:imgname(@"gerenyonghu")];
        self.userTypeLb.text = @"个人用户";
    }
    //2:电器厂商
    else {
        [self.userTypeImg setImage:imgname(@"dianqichangjia")];
        self.userTypeLb.text = @"电器厂家";
    }
    
    if (model.order_time == nil && model.order_date == nil) {
        self.servicetimeLB.hidden = YES;
    }
    else
    {
        NSInteger  indd = [[model.order_time substringToIndex:2] integerValue];
        NSString *ap = indd < 12 ? @"上午":@"下午";
        NSArray  *myarray = [model.order_date
                             componentsSeparatedByString:@"-"];
        NSString *ordertiame =[NSString stringWithFormat:@"%@月%@日 %@%@",myarray[1],myarray[2],ap,model.order_time];
        self.servicetimeLB.text = ordertiame;
        self.servicetimeLB.hidden = NO;
    }
    
    // 订单状态(0生成订单（已接单）//生成检测报告
    // ，1已检测，//生成账单
    //2生成账单（未支付），//去支付
    // 3已支付（已完成），//去评价
    //4已评价，
    self.makeleftBut.hidden = NO;
    self.makecenterBut.hidden = NO;
    self.makeOrderBut.hidden = NO;
#pragma mark-------------维修类----------------------------
    if ([model.fault_type isEqualToString:@"3"])
    {
        //已接单
        if ([model.order_state integerValue] == 0)
        {
            //电器厂维修和安装，需要判断是否填写预约日期和时间
            if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                //无预约日期
                self.orderStateLB.text = @"待上门";
                //有物流信息，就可以查看物流信息
                if ([model.product_source isEqualToString:@"1"] && [model.is_protect isEqualToString:@"1"] && model.courierNumber.length > 0 && model.courierCompany.length > 0)
                {
                    self.makeleftBut.hidden = NO;
                    [self.makeleftBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
                }
                else
                {
                    self.makeleftBut.hidden = YES;
                }
                self.makecenterBut.hidden = NO;
                self.makeOrderBut.hidden = NO;
                [self.makecenterBut setImage:imgname(@"tianxieyuyuexinxi") forState:(UIControlStateNormal)];
                [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:UIControlStateNormal];
                
                self.servicetimeLB.text = @"待填写";
            }else {
                if ([model.product_source isEqualToString:@"1"]) {
                    self.orderStateLB.text = @"待上门";
                    
                    if ([model.product_source isEqualToString:@"1"] && [model.is_protect isEqualToString:@"1"] && model.courierNumber.length > 0 && model.courierCompany.length > 0)
                    {
                        self.makeleftBut.hidden = NO;
                        [self.makeleftBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
                    }
                    else
                    {
                        self.makeleftBut.hidden = YES;
                    }
                    //is_protect  0保外 1保内
                    //电器厂提供配件,保内且厂家提供  test_order_id为空，显示 生成检测报告
                    if ([model.is_protect isEqualToString:@"1"] && model.test_order_id.length==0) {
                        self.makecenterBut.hidden =NO;
                        [self.makecenterBut setImage:imgname(@"shengchengjiancebaogao") forState:(UIControlStateNormal)];
                    }
                    //电器厂提供配件,保内且厂家提供  test_order_id不为空，显示 生成账单按钮
                    else if ([model.is_protect isEqualToString:@"1"] && model.test_order_id.length>0)
                    {
                        self.makecenterBut.hidden =NO;
                        [self.makecenterBut setImage:imgname(@"shengchengzhangdan") forState:(UIControlStateNormal)];
                    }
                    else
                    {
                        self.makecenterBut.hidden =YES;
                    }
                    self.makeOrderBut.hidden =NO;
                    [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:UIControlStateNormal];
                } else {//师傅自带配件,生成账单
                    self.orderStateLB.text = @"待上门";
                    self.makeleftBut.hidden = YES;
                    self.makecenterBut.hidden = NO;
                    self.makeOrderBut.hidden = NO;
                    [self.makecenterBut setImage:imgname(@"shengchengzhangdan") forState:(UIControlStateNormal)];
                    [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:UIControlStateNormal];
                }
            }
        }
        //已检测,只有电器厂-维修-保内-厂家提供配件,才有已检测状态
        else if ([model.order_state integerValue] == 1)
        {
            self.orderStateLB.text = @"已检测";
            if ([model.product_source isEqualToString:@"1"] && [model.is_protect isEqualToString:@"1"] && model.courierNumber.length > 0 && model.courierCompany.length > 0) {
                //有物流信息,显示查看物流 按钮
                self.makeleftBut.hidden = NO;
                [self.makeleftBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
            }
            else
            {
                self.makeleftBut.hidden = YES;
            }
            self.makecenterBut.hidden = NO;
            self.makeOrderBut.hidden = NO;
            [self.makecenterBut setImage:imgname(@"chaKanWuLiu") forState:(UIControlStateNormal)];
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:UIControlStateNormal];
        }
        //待支付
        else if ([model.order_state integerValue] == 2)
        {
            self.orderStateLB.text = @"待支付";
            self.makeleftBut.hidden = YES;
            if (model.courierNumber.length > 0)
            {
                [self.makecenterBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
            }
            else
            {
                self.makecenterBut.hidden = YES;
            }
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        //已支付
        else if ([model.order_state integerValue] == 3)
        {
            self.orderStateLB.text = @"已支付";
            self.makeleftBut.hidden = YES;
            if (model.courierNumber.length > 0) {
                //有物流信息,显示查看物流 按钮
                [self.makecenterBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
            }
            else
            {
                self.makecenterBut.hidden = YES;
            }
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        //4已评价，
        else if ([model.order_state integerValue] == 4)
        {
            self.orderStateLB.text = @"已评价";
            self.makeleftBut.hidden = YES;
            if (model.courierNumber.length > 0) {
                //有物流信息,显示查看物流 按钮
                [self.makecenterBut setImage:imgname(@"chaKanWuLiu") forState:UIControlStateNormal];
            }
            else
            {
                self.makecenterBut.hidden = YES;
            }
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        // 5已取消
        else if ([model.order_state integerValue] == 5)
        {
            self.orderStateLB.text = @"已取消";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = YES;
            //(null)月(null)日 上午(null)
            if ([model.order_time isEqualToString:@""] && [model.order_date isEqualToString:@""]) {
                self.servicetimeLB.hidden = YES;
            }
            else
            {
                self.servicetimeLB.hidden = NO;
            }
            self.orderStateLB.textColor = [UIColor grayColor];
        }
        // 6订单中止已支付
        else if ([model.order_state integerValue] == 6)
        {
            self.orderStateLB.text = @"订单中止已支付";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chakanxiangqing_hong") forState:(UIControlStateNormal)];
            self.orderStateLB.textColor = [UIColor colorWithHexString:@"#FF000A"];
            self.bgImage.layer.borderColor = [UIColor colorWithHexString:@"#FF000A"].CGColor;
        }
    }
#pragma mark-------------非维修类----------------------------
    else {
        //已接单
        if ([model.order_state integerValue] == 0)
        {
            //电器厂安装，需要判断是否填写预约日期和时间
            if ([model.user_type isEqualToString:@"2"] && model.order_date.length == 0) {
                //无预约日期
                self.orderStateLB.text = @"待上门";
                self.makeleftBut.hidden = YES;
                self.makecenterBut.hidden = NO;
                self.makeOrderBut.hidden = NO;
                [self.makecenterBut setImage:imgname(@"tianxieyuyuexinxi") forState:(UIControlStateNormal)];
                [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:UIControlStateNormal];
                self.servicetimeLB.text = @"待填写";
            } else {
                self.orderStateLB.text = @"待上门";
                self.makeleftBut.hidden = YES;
                self.makecenterBut.hidden = NO;
                self.makeOrderBut.hidden = NO;
                [self.makecenterBut setImage:imgname(@"shengchengzhangdan") forState:(UIControlStateNormal)];
                [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
            }
        }
        //已检测
        else if ([model.order_state integerValue] == 1)
        {
            self.orderStateLB.text = @"已检测";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        //待支付
        else if ([model.order_state integerValue] == 2)
        {
            self.orderStateLB.text = @"待支付";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        //已支付
        else if ([model.order_state integerValue] == 3)
        {
            self.orderStateLB.text = @"已支付";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        //4已评价
        else if ([model.order_state integerValue] == 4)
        {
            self.orderStateLB.text = @"已评价";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = NO;
            [self.makeOrderBut setImage:imgname(@"chaKanXiangQing") forState:(UIControlStateNormal)];
        }
        // 5已取消
        else  if ([model.order_state integerValue] == 5)
        {
            self.orderStateLB.text = @"已取消";
            self.makeleftBut.hidden = YES;
            self.makecenterBut.hidden = YES;
            self.makeOrderBut.hidden = YES;
            
        }
    }
#pragma mark-------------服务单按钮逻辑结束------------------------
}

- (IBAction)makeOrderAction:(UIButton *)sender {
    if (self.orderBlock) {
        self.orderBlock(@"2");
    }
}

- (IBAction)lrfeAction:(UIButton *)sender {
    if (self.orderBlock) {
        self.orderBlock(@"0");
    }
}

#pragma mark - 中间的按钮
- (IBAction)centerAction:(UIButton *)sender {
    if (self.orderBlock) {
        self.orderBlock(@"1");
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

//is_protect  0保外 1保内

