//
//  CSBuyOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSBuyOrderTableViewCell.h"

@implementation CSBuyOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)refashWithModel:(CSMallOrderModel *)model{
    
    if ([model.product_pic hasPrefix:@"http"]) {
        [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    }else{
        sdimg(self.goodsImage, model.product_pic);
        
    }
    
    self.titleLB.text = [NSString stringWithFormat:@"%@",model.product_name];
    self.priceLB.text = [NSString stringWithFormat:@"¥%@",model.product_price];
    self.numberLB.text = [NSString stringWithFormat:@"*%@",model.pro_count];

    
    
}


@end
