//
//  ZYAddAccessoriesViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//  电器厂保外订单, 保内师傅自带订单, 个人维修订单点击去安装添加配件

#import "ZYAddAccessoriesViewController.h"
#import "ZYTJXPJScanCodeTableViewCell.h"
#import "ZYAddAccessoriesTableViewCell.h"
#import "CSmakeAccAddmodel.h"
#import "HWScanViewController.h"

@interface ZYAddAccessoriesViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *mytableView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UIView *tabFootView;
@property (nonatomic, strong) NSMutableSet <NSString *>*saoYiSaoStrSet;
@property (nonatomic, strong) UIView *bgViewsec;
@end

@implementation ZYAddAccessoriesViewController

- (NSMutableSet <NSString *>*)saoYiSaoStrDic {
    if (!_saoYiSaoStrSet) {
        _saoYiSaoStrSet = [NSMutableSet setWithCapacity:1];
    }
    return _saoYiSaoStrSet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加配件";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    //[self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    //跳转到添加配件页面 没有添加配件 返回按钮
    UIButton * leftBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    leftBtn.frame = CGRectMake(0, 0, 30,30);
    UIImage *img = [[UIImage imageNamed:@"left_icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [leftBtn setImage:img forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftBarBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];

    self.mydateSource = [NSMutableArray arrayWithCapacity:1];
    
    [self showdetaile];
    [self showtabFootView];
    [self shwoBgviewsec];
    // Do any additional setup after loading the view.
}

//接完单之后跳转到检测单详情页面 再返回 回到检测单列表页面
- (void)leftBarBtnClicked:(UIButton *)button {
    [singlTool shareSingTool].needReasfh = YES;

    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)showdetaile {
    UIView *bgview = [[UIView alloc]initWithFrame:CGRectMake(0,kNaviHeight+ 10, KWIDTH, 55)];
    [self.scrollView addSubview:bgview];
    [self.view addSubview:self.scrollView];
    bgview.backgroundColor = [UIColor whiteColor];
    
    UILabel *dingdan = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, 83, 55)];
    dingdan.font = FontSize(16);
    dingdan.textColor = K666666;
    dingdan.text = @"订单编号:";
    [bgview addSubview:dingdan];
    
    UILabel *dingdanlb = [[UILabel alloc]initWithFrame:CGRectMake(99, 0, KWIDTH/2, 55)];
    dingdanlb.font = FontSize(16);
    dingdanlb.textColor = K666666;
    dingdanlb.text = self.orderId;
    [bgview addSubview:dingdanlb];
    
    UIButton *rightBtn = [[UIButton alloc] init];
    rightBtn.frame = CGRectMake(dingdanlb.right, 0, 100, 55);
    [rightBtn setImage:imgname(@"newaddjinru") forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -40)];
    [bgview addSubview:rightBtn];
    
    UIView *btview = [[UIView alloc]initWithFrame:CGRectMake(0,bgview.bottom +2, KWIDTH, 55)];
    [self.scrollView addSubview:btview];
    [self.view addSubview:self.scrollView];
    btview.backgroundColor = [UIColor whiteColor];
    
    UIButton *addPjBtn = [[UIButton alloc] init];
    addPjBtn.frame = CGRectMake(56, 2, KWIDTH - 56 -55, 48);
    [addPjBtn setImage:imgname(@"组 3181") forState:(UIControlStateNormal)];
    [addPjBtn addTarget:self action:@selector(addNewPjBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btview addSubview:addPjBtn];
    
    UILabel *tipsLab = [[UILabel alloc]initWithFrame:CGRectMake(16, btview.bottom +10, KWIDTH -32, 15)];
    tipsLab.font = FontSize(14);
    tipsLab.textColor = K666666;
    tipsLab.text = @"请您扫描候保配件的二维码";
    tipsLab.textColor = [UIColor colorWithHexString:@"#D84B4A"];
    tipsLab.textAlignment = NSTextAlignmentLeft;
    [self.scrollView addSubview:tipsLab];
    [self.view addSubview:self.scrollView];
    tipsLab.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    _mytableView = [[UITableView alloc] init];
    _mytableView.frame = CGRectMake(0, tipsLab.bottom +10, KWIDTH, KHEIGHT - kNaviHeight -20-55-56);
    _mytableView.delegate = self;
    _mytableView.dataSource = self;
    _mytableView.estimatedRowHeight = 90;
    _mytableView.rowHeight = UITableViewAutomaticDimension;
    _mytableView.tableFooterView = [UIView new];
    //新添加配件 扫描
    //[_mytableView registerClass:[ZYTJXPJScanCodeTableViewCell class] forCellReuseIdentifier:@"ZYTJXPJScanCodeTableViewCell"];
    
    [_mytableView registerClass:[ZYAddAccessoriesTableViewCell class] forCellReuseIdentifier:@"ZYAddAccessoriesTableViewCell"];
    
    _mytableView.backgroundColor = [UIColor clearColor];
    adjustInset(_mytableView);
    [_mytableView reloadData];
    [_scrollView addSubview:_mytableView];
}

-(void)showtabFootView {
    
    self.tabFootView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 116)];
    
    UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [tuiusongBut setTitle:@"提交" forState:(UIControlStateNormal)];
    tuiusongBut.frame  = CGRectMake(28, 48, KWIDTH-28-28, 48);
    tuiusongBut.layer.masksToBounds = YES;
    tuiusongBut.layer.cornerRadius = 4;
    [self.tabFootView addSubview: tuiusongBut ];
    
    [tuiusongBut addTarget:self action:@selector(tuisongAction:) forControlEvents:(UIControlEventTouchUpInside)];
    _mytableView.tableFooterView = self.tabFootView;
}

#pragma mark - 提交
-(void)tuisongAction:(UIButton *)but{
    //推送账单
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = self.orderId;
    NSMutableArray *muarr = [NSMutableArray arrayWithCapacity:1];
    if (_mydateSource.count == 0) {
        ShowToastWithText(@"请先添加配件");
        return;
    }
    NSMutableArray *qrcodeArr = [NSMutableArray array];
    for (CSmakeAccAddmodel *model in _mydateSource) {
        NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithCapacity:1];
        
        if (strIsEmpty(model.name)) {
            ShowToastWithText(@"请扫描添加配件");
            return;
        }
        if (strIsEmpty(model.type_id)) {
            ShowToastWithText(@"请扫描添加配件");
            return;
        }
        muDic[@"price"] = @"0";
        muDic[@"type_id"] = model.type_id;
        muDic[@"count"] = model.count;
        muDic[@"name"] = model.name;
        muDic[@"productId"] = model.type_id;
        muDic[@"qrcode"] = model.qrCode;
        muDic[@"product_model"] = @"1";
        [muarr addObject:muDic];
        [qrcodeArr addObject:model.qrCode];
    }
    param[@"products"] = muarr;
    
    NSArray *compareArray = (NSArray *)qrcodeArr;
    NSArray *result = [compareArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        return [obj2 compare:obj1];//降序
        
    }];
    for (int i = 0; i < result.count; i ++) {
        for (int j = 0; j < result.count-i-1; j ++) {
            if ([result[j] isEqualToString: result[j + 1]]) {
                ShowToastWithText(@"不能重复添加同一个配件");
                return;
            }
        }
    }
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] = json;
    kWeakSelf;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [NetWorkTool POST:delRepProduct param:allDic success:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"请求成功 %@", dic);
        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        ShowToastWithText(dic[@"msg"]);
        
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } needUser:YES];
}

- (void)rightBtnAction:(UIButton *)button {
    NSLog(@"您点击了 右侧按钮");
}

- (void)addNewPjBtnAction:(UIButton *)button {
    NSLog(@"您点击了 添加新配件按钮");
    
    CSmakeAccAddmodel *model = [[CSmakeAccAddmodel alloc]init];
    
    CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
    
    mymodel.row = @"";              mymodel.type_name = @"";
    mymodel.type_id = @"";          mymodel.name = @"";
    mymodel.price = @"";            mymodel.count = @"1";
    mymodel.total_price = @"";      mymodel.qrCode = @"";
    mymodel.isSaoyisao = @"2";      mymodel.typeProduct = @"2";
    mymodel.old_type_name = @"";    mymodel.old_type_id = @"";
    mymodel.old_name = @"";         mymodel.old_price = @"1";
    mymodel.old_count = @"1";       mymodel.old_total_price = @"";
    
    mymodel.old_isSaoyisao = @"2";  mymodel.ne_type_name = @"";
    mymodel.ne_type_id = @"";       mymodel.ne_name = @"";
    mymodel.ne_price = @"2";        mymodel.ne_count = @"";
    mymodel.ne_total_price = @"";   mymodel.ne_qrCode = @"";
    mymodel.ne_isSaoyisao = @"2";
    
    model.typeProduct = @"2";
    model.ne_count = @"1";
    model.old_count = @"1";
    
    [self.mydateSource addObject:model];
    
    [self.mytableView reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(_mydateSource.count -1) inSection:0];
    [self.mytableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 159; //扫码
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //定义Cell标识 每一个Cell对应一个自己的标识
    NSString *CellIdentitier = [NSString stringWithFormat:@"cell%ld%ld",indexPath.section, indexPath.row];
    //通过唯一标识创建Cell实例
    ZYAddAccessoriesTableViewCell *mycell = [tableView dequeueReusableCellWithIdentifier:CellIdentitier];
    //判断为空进行初始化  --(当拉动页面显示超过主页面内容的时候就会重用之前的cell,w而不会再次初始化)
    if (!mycell) {
        mycell = [[ZYAddAccessoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentitier];
    }
    
    mycell.selectionStyle = UITableViewCellSelectionStyleNone;//取消cell的点击状态

    CSmakeAccAddmodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    
    mycell.pjplTF.text = model.type_name;
    mycell.pjmcTF.text = model.name;
    
    mycell.pjplTF.textColor = K666666;
    mycell.pjmcTF.textColor = K666666;
    
    kWeakSelf;
    mycell.myblock = ^(NSUInteger ind,NSString *str) {
        
        switch (ind) {
            case 0: {//上边的扫一扫
                //记录扫一扫的位置 如:1-0 第1个位置的第0位上的扫一扫
                HWScanViewController *vc = [[HWScanViewController alloc]init];
                vc.myblock = ^(NSString *str) {
                    if ([weakSelf.saoYiSaoStrSet containsObject:str]) {
                        ShowToastWithText(@"此配件已扫描");
                        return ;
                    }
                    model.old_type_name = str;
                    
                    [weakSelf.saoYiSaoStrSet addObject:str];
                    [self requesrtWith:str withind:indexPath.row]; //扫描新配件
                };
                UINavigationController *na = [[UINavigationController alloc]initWithRootViewController:vc];
                [self presentViewController:na animated:YES completion:nil];
            }
                break;
            case 1: {//删除
                [weakSelf.mydateSource removeObjectAtIndex:indexPath.row];
                [weakSelf.mytableView reloadData];
                
                CGFloat ffftotle = 0;
                for (CSmakeAccAddmodel *model in self.mydateSource) {
                    CGFloat motltlt = [model.total_price floatValue];
                    ffftotle = ffftotle +motltlt;
                }
            }
                break;
            default:
                break;
        }
    };
    return mycell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind isup:(NSInteger )index{
    
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"ggggggg%@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = weakSelf.mydateSource[ind];
        if (index == 1) {
            //旧配件
            mymodel.old_type_name =[mudic objectForKey:@"type_name"];
            mymodel.old_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.old_name =[mudic objectForKey:@"parts_name"];
            mymodel.old_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.old_isSaoyisao = @"1";
        } else {
            //新配件
            mymodel.ne_type_name =[mudic objectForKey:@"type_name"];
            mymodel.ne_type_id =[mudic objectForKey:@"parts_type"];
            mymodel.ne_name =[mudic objectForKey:@"parts_name"];
            mymodel.ne_qrCode =[mudic objectForKey:@"qrcode"];
            mymodel.ne_isSaoyisao = @"1";
        }
        [self.mytableView reloadData];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

//扫描新配件
- (void)requesrtWith:(NSString *)strr withind:(NSInteger )ind{
    kWeakSelf;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"data"] = strr;
    [NetWorkTool POST:Saoyisao param:param success:^(id dic) {
        KMyLog(@"扫描新配件 扫出来的数据是 %@",dic);
        NSMutableDictionary *mudic = [[dic objectForKey:@"data"] mutableCopy];
        CSmakeAccAddmodel *mymodel = [[CSmakeAccAddmodel alloc]init];
        mymodel.qrCode =[mudic objectForKey:@"qrcode"];
        mymodel.type_name =[mudic objectForKey:@"type_name"];
        mymodel.price =[mudic objectForKey:@"parts_price"];
        mymodel.price = @"0";
        mymodel.type_id =[mudic objectForKey:@"parts_type"];
        mymodel.Id =[mudic objectForKey:@"id"];
        
        //mymodel.count =[mudic objectForKey:@"count"];
        mymodel.count  = @"1";
        mymodel.name =[mudic objectForKey:@"parts_name"];
        mymodel.isSaoyisao = @"0";
        
        [weakSelf.mydateSource removeObjectAtIndex:ind];
        [weakSelf.mydateSource insertObject:mymodel atIndex:ind];
        [self.mytableView reloadData];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

/**
 弹出框的背景图
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"信息已提交";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.numberOfLines = 0;
    DowLable.text = @"可在我的订单该单的服务详情\n页面中查看配件详情";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
}

-(void)ikenow:(UIButton *)but{
    [_bgViewsec removeFromSuperview];
    
    [singlTool shareSingTool].needReasfh = YES;

    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 125);
        
        adjustInset(_scrollView);
    }
    return _scrollView;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

