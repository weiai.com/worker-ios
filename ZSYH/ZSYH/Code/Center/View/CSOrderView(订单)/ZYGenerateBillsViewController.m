//
//  ZYGenerateBillsViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYGenerateBillsViewController.h"
#import "ZYCheckBillViewController.h" //检测账单
#import "ZYGenerateBillingDetailsController.h" //生成账单详情

@interface ZYGenerateBillsViewController ()
@property (nonatomic, strong) UIView *bgViewsec; //电器厂维修个人/保外 正常订单流程
@property (nonatomic, strong) UIView *bgViewsecBw; //根据检测单下的保外订单 流程
@end

@implementation ZYGenerateBillsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.orderSuspension.adjustsImageWhenHighlighted = NO;
    self.endOrOrder.adjustsImageWhenHighlighted = NO;
    
    if (self.mymodel.test_order_id == NULL || self.mymodel.test_order_id == nil) {
        [self.endOrOrder setImage:imgname(@"kehujujueweixiu") forState:UIControlStateNormal];
    } else {
        [self.endOrOrder setImage:imgname(@"dqcBwSczdJjwxBtn") forState:UIControlStateNormal];
    }
    
    [self shwoBgviewsec]; //个人维修 推送弹窗
    [self shwoBgviewsecBaowai]; //根据检测单下的保外订单
    // Do any additional setup after loading the view from its nib.
}

//客户同意维修 正常走单 生成账单 点击事件
- (IBAction)orderSuspension:(id)sender {
    ZYGenerateBillingDetailsController *endVC = [[ZYGenerateBillingDetailsController alloc] init];
    endVC.mymodel = _mymodel;
    [self.navigationController pushViewController:endVC animated:YES];
}

//客户拒绝维修 提前中止 生成账单 点击事件
- (IBAction)endOfOrder:(id)sender {
    if (self.mymodel.test_order_id == NULL || self.mymodel.test_order_id == nil) {
        //正常下单 用户拒绝维修 流程
        ZYCheckBillViewController *checkVC = [[ZYCheckBillViewController alloc] init];
        checkVC.mymodel = _mymodel;
        [self.navigationController pushViewController:checkVC animated:YES];
    } else {
        //电器厂 根据检测单下的维修单 用户拒绝维修 流程
        [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecBw];
    }
}

/**
 弹出框的背景图 推送成功 个人维修/电器厂保外
 */
-(void)shwoBgviewsec{
    self.bgViewsec = [[UIView alloc]init];
    self.bgViewsec.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    self.bgViewsec.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsec addSubview:whiteBGView];
    
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*为保障您的收益\n请您确认用户支付费用后离开" originalColor:K333333 originalFont:FontSize(16) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(16)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.numberOfLines = 0;
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.attributedText = attStr;
    DowLable.textAlignment =  NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikenow:(UIButton *)but{
    [self.bgViewsec removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
    [singlTool shareSingTool].needReasfh = YES;
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/***
 根据检测单下的 保外订单 拒绝维修 弹窗
 */
-(void)shwoBgviewsecBaowai {
    self.bgViewsecBw = [[UIView alloc]init];
    self.bgViewsecBw.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsecBw.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecBw addSubview:whiteBGView];
    
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(239);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((KWIDTH-22*2)/2-30, 34, 60, 60)];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"tipImage"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, upImage.bottom + 19, KWIDTH-22*2, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"确定结束此订单?";
    
    UIButton *cancleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancleBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancleBut setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    [cancleBut setBackgroundColor:[UIColor whiteColor]];
    cancleBut.layer.masksToBounds = YES;
    cancleBut.layer.cornerRadius = 4;
    cancleBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    cancleBut.layer.borderWidth = 1;
    [cancleBut addTarget:self action:@selector(cancleButAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:cancleBut];
    
    [cancleBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(45);
        make.width.offset((KWIDTH-22*2-45*2-29)/2);
        make.bottom.offset(-22);
    }];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 4;
    [yesBut addTarget:self action:@selector(yesBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.right.offset(-45);
        make.width.offset((KWIDTH-22*2-45*2-29)/2);
        make.bottom.offset(-22);
    }];
}

- (void)cancleButAction:(UIButton *)button {
    [_bgViewsecBw removeFromSuperview];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)yesBtnAction:(UIButton *)button {
    [self requestData];
}

//根据检测单 下的维修保外订单的 走以下流程
- (void)requestData {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    param[@"peopleCost"] = @"0";
    param[@"orderId"] = NOTNIL(_mymodel.orderId);
    param[@"type"] = @"1";
    param[@"productCost"] = @"0";
    param[@"prodcutComment"] = @"";
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] = json;
    //kWeakSelf;
    [NetWorkTool POST:OrderCreateBill param:allDic success:^(id dic) {
        
        [self.bgViewsecBw removeFromSuperview];

        //[[UIApplication sharedApplication].keyWindow addSubview:weakSelf.bgViewsec];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
        [singlTool shareSingTool].needReasfh = YES;
        self.navigationController.tabBarController.selectedIndex = 1;
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } other:^(id dic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } fail:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } needUser:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
