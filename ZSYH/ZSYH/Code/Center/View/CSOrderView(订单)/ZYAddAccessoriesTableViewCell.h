//
//  ZYAddAccessoriesTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmakeAccAddmodel.h"
#import "ZYAccessoriesDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYAddAccessoriesTableViewCell : UITableViewCell
@property (nonatomic,copy) void (^myblock)(NSUInteger ind,NSString *str);
@property (strong, nonatomic) ZYAccessoriesDetailsModel *myModel;

@property (nonatomic, strong) UITextField *pjplTF;
@property (nonatomic, strong) UITextField *pjmcTF;
@end

NS_ASSUME_NONNULL_END

