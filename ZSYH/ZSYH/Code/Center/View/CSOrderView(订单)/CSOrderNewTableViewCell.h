//
//  CSOrderNewTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSOrderNewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLB;

@property (weak, nonatomic) IBOutlet UILabel *orderStateLB;
@property (weak, nonatomic) IBOutlet UILabel *serviceTypeLB;
@property (weak, nonatomic) IBOutlet UILabel *productTypeLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLB;
@property(nonatomic,copy)void (^orderBlock)(NSString *ind);

@property (weak, nonatomic) IBOutlet UIButton *makeOrderBut;

@property (weak, nonatomic) IBOutlet UIButton *makecenterBut;
@property (weak, nonatomic) IBOutlet UIButton *makeleftBut;
@property (weak, nonatomic) IBOutlet UIImageView *DQCImage;

@property (weak, nonatomic) IBOutlet UILabel *servicetimeLB;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (weak, nonatomic) IBOutlet UILabel *AddressLB;
@property (weak, nonatomic) IBOutlet UIImageView *userTypeImg;
@property (weak, nonatomic) IBOutlet UILabel *userTypeLb;
@property (weak, nonatomic) IBOutlet UILabel *serviceTypeTit;
@property (weak, nonatomic) IBOutlet UILabel *elecCateTit;
@property (weak, nonatomic) IBOutlet UILabel *serviceTimeTit;
@property (weak, nonatomic) IBOutlet UILabel *serviceAddTit;

-(void)refasf:(CSmaintenanceListModel *)model;

@end

NS_ASSUME_NONNULL_END
