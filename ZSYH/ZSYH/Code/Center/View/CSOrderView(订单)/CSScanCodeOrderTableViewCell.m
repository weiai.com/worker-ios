//
//  CSScanCodeOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSScanCodeOrderTableViewCell.h"

@implementation CSScanCodeOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    frame.origin.x += 10;
    //frame.origin.y += 10;
    frame.size.height -= 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        self.backgroundColor = [UIColor clearColor];
        
        UIView *bottomV = [[UIView alloc] init];
        bottomV.backgroundColor = [UIColor whiteColor];
        bottomV.frame = CGRectMake(0, 0, KWIDTH-20, 151);
        bottomV.layer.cornerRadius = 10;
        bottomV.layer.masksToBounds = YES;
        bottomV.layer.borderWidth = 1;
        bottomV.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
        [self.contentView addSubview:bottomV];
        
        //订单编号
        UILabel *orderNumLab = [[UILabel alloc] init];
        orderNumLab.frame = CGRectMake(15, 16, KWIDTH-50-80, 10);
        orderNumLab.text = @"订单编号:12345678900987654321";
        orderNumLab.font = FontSize(10);
        orderNumLab.textColor = K999999;
        [bottomV addSubview:orderNumLab];
        self.orderNumLab = orderNumLab;
        //订单状态
        UILabel *stateLab = [[UILabel alloc] init];
        stateLab.frame = CGRectMake(bottomV.right-70, 14, 45, 11);
        stateLab.text = @"已损坏";
        stateLab.textAlignment = NSTextAlignmentRight;
        stateLab.textColor = [UIColor colorWithHexString:@"#F88B1F"];
        stateLab.font = KFontPingFangSCLight(11);
        [bottomV addSubview:stateLab];
        self.stateLab = stateLab;
        //左侧 Logo 图片
        UIImageView *tipsImg = [[UIImageView alloc] init];
        tipsImg.frame = CGRectMake(19, orderNumLab.bottom +20, 44, 44);
        [tipsImg setImage:imgname(@"scanCodeTips")];
        [bottomV addSubview:tipsImg];
        self.tipsImg = tipsImg;
        //订单来源
        UILabel *scanOrderLab = [[UILabel alloc] init];
        scanOrderLab.frame = CGRectMake(5, tipsImg.bottom +7, 60, 11);
        scanOrderLab.text = @"扫码安装";
        scanOrderLab.textColor = K333333;
        scanOrderLab.font = KFontPingFangSCRegular(11);
        scanOrderLab.textAlignment = NSTextAlignmentCenter;
        [bottomV addSubview:scanOrderLab];
        //用户姓名 标题
        UILabel *nameTitLab = [[UILabel alloc] init];
        nameTitLab.frame = CGRectMake(tipsImg.right +12, orderNumLab.bottom +20, 70, 14);
        nameTitLab.font = FontSize(14);
        nameTitLab.text = @"用户姓名:";
        nameTitLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:nameTitLab];
        //用户姓名 内容
        UILabel *nameConLab = [[UILabel alloc] init];
        nameConLab.frame = CGRectMake(nameTitLab.right, orderNumLab.bottom +20, (KWIDTH-20-38-44-12-70)/2, 14);
        nameConLab.font = FontSize(14);
        nameConLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:nameConLab];
        self.nameConLab = nameConLab;
        //手机号码 内容
        UILabel *phoneLab = [[UILabel alloc] init];
        phoneLab.frame = CGRectMake(nameConLab.right, orderNumLab.bottom +20, (KWIDTH-20-38-44-12-70)/2, 14);
        phoneLab.textAlignment = NSTextAlignmentRight;
        phoneLab.font = FontSize(14);
        phoneLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:phoneLab];
        self.phoneLab = phoneLab;
        //故障原因 标题
        UILabel *errorTitLab = [[UILabel alloc] init];
        errorTitLab.frame = CGRectMake(tipsImg.right +12, nameConLab.bottom +8, 70, 14);
        errorTitLab.text = @"故障原因:";
        errorTitLab.font = FontSize(14);
        errorTitLab.textColor = [UIColor colorWithHexString:@"#272727"];
        [bottomV addSubview:errorTitLab];
        self.errorTitLab = errorTitLab;
        
        UILabel *errorConLab = [[UILabel alloc] init];
        errorConLab.frame = CGRectMake(errorTitLab.right, nameConLab.bottom +8, KWIDTH-20-38-44-12-70, 14);
        errorConLab.font = FontSize(14);
        errorConLab.textColor = [UIColor colorWithHexString:@"#CA9F61"];
        [bottomV addSubview:errorConLab];
        self.errorConLab = errorConLab;
        
        UIButton *detailBtn = [[UIButton alloc] init];
        detailBtn.frame = CGRectMake(bottomV.right - 84, errorConLab.bottom +30, 69, 26);
        [detailBtn setImage:[UIImage imageNamed:@"chaKanXiangQing"] forState:UIControlStateNormal];
        [detailBtn addTarget:self action:@selector(lookDetsilBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomV addSubview:detailBtn];
        self.detailBtn = detailBtn;
    }
    return self;
}

-(void)refasf:(CSmaintenanceListModel *)model{
    self.orderNumLab.text = [NSString stringWithFormat:@"%@%@", @"订单编号:",model.order_id]; //订单编号
    self.nameConLab.text = model.user_name;     //姓名
    self.phoneLab.text = model.user_phone;      //手机号
    //self.addrConLab.text = model.user_address;  //地址
    self.errorConLab.text = model.fault_common; //故障原因
    
    if ([model.state isEqualToString:@"1"]) {   //已安装
        self.stateLab.text = @"已安装";
    } else {                                    //已损坏
        self.stateLab.text = @"已损坏";
    }
}

//查看详情按钮 点击 跳转事件
- (void)lookDetsilBtnAction:(UIButton *)button {
    if (self.myblock) {
        self.myblock(0,@"");
    }
    NSLog(@"您点击了 服务单 扫码安装订单 查看详情按钮");
}

@end
