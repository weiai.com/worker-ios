//
//  ZYGenerateBillsViewController.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZYGenerateBillsViewController : BaseViewController
@property (nonatomic,strong)CSmaintenanceListModel *mymodel;
@property (weak, nonatomic) IBOutlet UIButton *orderSuspension; //订单中止按钮
@property (weak, nonatomic) IBOutlet UIButton *endOrOrder; //订单结束按钮
@property (nonatomic,copy)void (^myblock)(NSString *str);
@property (nonatomic, strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
