//
//  CSBuyOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSMallOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CSBuyOrderTableViewCell : UITableViewCell
-(void)refashWithModel:(CSMallOrderModel *)model;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLB;
@property (weak, nonatomic) IBOutlet UILabel *priceLB;
@property (weak, nonatomic) IBOutlet UILabel *numberLB;

@end

NS_ASSUME_NONNULL_END
