//
//  CSOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSOrderTableViewCell.h"

@implementation CSOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.makeOrderBut.layer.borderWidth = 1;
    self.makeOrderBut.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    self.makeOrderBut.layer.masksToBounds = YES;
    self.makeOrderBut.layer.cornerRadius = 4;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)refasf:(CSmaintenanceListModel *)model{
    
//    self.order_number.text = [NSString stringWithFormat:@"订单编号:%@",model.orderNumber];
    self.order_type.text = [NSString stringWithFormat:@"%@",model.order_date];
    self.nameLb.text = [NSString stringWithFormat:@"%@",model.userName];
    self.phoneLB.text = [NSString stringWithFormat:@"%@",model.userPhone];
    self.guzhangType.text = [NSString stringWithFormat:@"%@",model.fault_type];
    self.order_address.text = [NSString stringWithFormat:@"%@",model.address_content];
    self.order_state.text = [NSString stringWithFormat:@"%@",model.order_state];

    
}

- (IBAction)orderAction:(UIButton *)sender {
    if (self.orderBlock) {
        self.orderBlock(@"");
    }
}

@end
