//
//  ZYRefuseToRepairCKZDViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/31.
//  Copyright © 2019 魏堰青. All rights reserved.
//  个人订单/电器厂维修保外订单 拒绝维修 查看账单

#import "ZYRefuseToRepairCKZDViewController.h"
#import "SYBigImage.h"

@interface ZYRefuseToRepairCKZDViewController ()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) UIView *addView;
@property (nonatomic,strong) NSDictionary *mydic;
@property (nonatomic,strong) UIImageView *myimage;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UILabel *whiteBackView;//白色背景(upLabel)

@property (nonatomic,strong) UILabel *gsfTitLab;//工时费-标题
@property (nonatomic,strong) UILabel *gsfConLab;//工时费-内容

@property (nonatomic,strong) UILabel *heJiTitLab;//费用合计-标题
@property (nonatomic,strong) UILabel *heJiConLab;//费用合计-内容
@property (nonatomic,strong) UILabel *heJiLine;  //费用合计下划线

@property (nonatomic,strong) UILabel *theincoTitLab;//本单收益-标题
@property (nonatomic,strong) UILabel *theincoConLab;//本单收益-标题

@property (nonatomic,strong) UILabel *firstOrderLine; //当日首单 分割线

@end

@implementation ZYRefuseToRepairCKZDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
    // Do any additional setup after loading the view.
}

-(void)request{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] = NOTNIL(_model.orderId);
    kWeakSelf;
    [NetWorkTool POST:lookListacc param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
        [weakSelf.model setValuesForKeysWithDictionary:[weakSelf.mydic objectForKey:@"order"]];
        [self showDetaileUi];
        KMyLog(@"个人维修/电器厂保外 拒绝维修 查看账单%@",dic);
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

-(void)showDetaileUi {
    
    self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, 230)];
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview: self.bgView];
    self.bgView.backgroundColor = zhutiColor;
    [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.masksToBounds = YES;
    
    UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
    order.textColor = [UIColor whiteColor];
    order.font = FontSize(16);
    order.text = @"订单编号";
    [self.bgView addSubview:order];
    
    UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
    orderlb.textColor = [UIColor whiteColor];
    orderlb.font = FontSize(16);
    orderlb.text = [NSString stringWithFormat:@"%@",[[_mydic objectForKey:@"order"] objectForKey:@"app_order_id"]];;
    [self.bgView addSubview:orderlb];
    
    UILabel *whiteBackView = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 150)];
    whiteBackView.backgroundColor = [UIColor whiteColor];
    self.whiteBackView = whiteBackView;
    [self.bgView addSubview:whiteBackView];
    
    UILabel *gsfTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, 19, 100, 20)];
    gsfTitLab.textColor = K666666;
    gsfTitLab.font = KFontPingFangSCMedium(14);
    gsfTitLab.text = @"检测费:";
    [self.whiteBackView addSubview:gsfTitLab];
    
    UILabel *gsfConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, 19, 100, 20)];
    gsfConLab.textColor = K666666;
    gsfConLab.textAlignment = NSTextAlignmentRight;
    gsfConLab.font = KFontPingFangSCMedium(14);
    gsfConLab.text = [NSString stringWithFormat:@"¥%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"peopleCost"] floatValue]];
    [self.whiteBackView addSubview:gsfConLab];
    self.gsfConLab = gsfConLab;
    
    UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0, gsfTitLab.bottom+10, KWIDTH-20, 1)];
    line.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.whiteBackView addSubview:line];
    
    UILabel *heJiTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, line.bottom+16, 100, 20)];
    heJiTitLab.textColor = K666666;
    heJiTitLab.font = KFontPingFangSCMedium(14);
    heJiTitLab.text = @"费用合计:";
    self.heJiTitLab = heJiTitLab;
    [self.whiteBackView addSubview:heJiTitLab];
    
    UILabel *heJiConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line.bottom+16, 100, 20)];
    heJiConLab.textColor = K666666;
    heJiConLab.textAlignment = NSTextAlignmentRight;
    heJiConLab.font = KFontPingFangSCMedium(14);
    heJiConLab.text = [NSString stringWithFormat:@"¥%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"all_cost"] floatValue]];
    self.heJiConLab = heJiConLab;
    [self.whiteBackView addSubview:heJiConLab];
    
    UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, heJiTitLab.bottom+10, KWIDTH-20, 1)];
    line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.heJiLine = line1;
    [self.whiteBackView addSubview:line1];
    
    UILabel *theincoTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, line1.bottom+16, 100, 14)];
    theincoTitLab.textColor = zhutiColor;
    theincoTitLab.font = KFontPingFangSCMedium(14);
    //theincoTitLab.text = @"本单收益";
    self.theincoTitLab = theincoTitLab;
    [self.whiteBackView addSubview:theincoTitLab];
    
    UILabel *theincoConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line1.bottom+16, 100, 14)];
    theincoConLab.textColor = zhutiColor;
    theincoConLab.textAlignment = NSTextAlignmentRight;
    theincoConLab.font = KFontPingFangSCMedium(14);
    //theincoConLab.text = [NSString stringWithFormat:@"¥%.2f",[[_mydic objectForKey:@"earning"] floatValue]];
    self.theincoConLab = theincoConLab;
    [self.whiteBackView addSubview:theincoConLab];
    
    self.whiteBackView.height = theincoConLab.bottom+20;
    self.bgView.height = self.whiteBackView.bottom;
    
    if ([_model.order_state integerValue] < 3) {
        theincoTitLab.hidden = YES;
        theincoConLab.hidden = YES;
    }
    //如果订单未支付
    if ([_model.order_state integerValue] < 3) {
        self.theincoTitLab.hidden = YES;
        self.theincoConLab.hidden = YES;
        self.heJiLine.hidden = YES;
        self.whiteBackView.height = self.heJiConLab.bottom+20;
        self.bgView.height = self.whiteBackView.bottom;
    } else {
        
        NSString *insurance_premium = [[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"];
        //insurance_premium = @"1";
        if ([insurance_premium floatValue] > 0) {
            //当日首单 标题
            UILabel *firstOrderFeeL = [[UILabel alloc]initWithFrame:CGRectMake(24, self.heJiLine.bottom +13, 150, 14)];
            firstOrderFeeL.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            firstOrderFeeL.font = KFontPingFangSCMedium(14);
            firstOrderFeeL.text = @"当日首单-为您投保";
            [self.whiteBackView addSubview:firstOrderFeeL];
            
            //当日首单 内容
            UILabel *firstOrderFee = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, firstOrderFeeL.top, 100, 14)];
            firstOrderFee.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            firstOrderFee.textAlignment = NSTextAlignmentRight;
            firstOrderFee.font = KFontPingFangSCMedium(14);
            firstOrderFee.text = [NSString stringWithFormat:@"-¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"]];
            [self.whiteBackView addSubview:firstOrderFee];
            
            //当日首单 分割线
            UILabel *firstOrderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, firstOrderFeeL.bottom+11, KWIDTH-20, 1)];
            firstOrderLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:firstOrderLine];
            self.firstOrderLine = firstOrderLine;
            
            //本单收益 标题
            UILabel *theincoTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, self.firstOrderLine.bottom+16, 100, 14)];
            theincoTitLab.textColor = zhutiColor;
            theincoTitLab.font = KFontPingFangSCMedium(14);
            theincoTitLab.text = @"本单收益";
            self.theincoTitLab = theincoTitLab;
            [self.whiteBackView addSubview:theincoTitLab];
            
            //本单收益 内容
            UILabel *theincoConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.firstOrderLine.bottom+16, 100, 14)];
            theincoConLab.textColor = zhutiColor;
            theincoConLab.textAlignment = NSTextAlignmentRight;
            theincoConLab.font = KFontPingFangSCMedium(14);
            theincoConLab.text = [NSString stringWithFormat:@"¥%.2f",[[_mydic objectForKey:@"earning"] floatValue]];
            self.theincoConLab = theincoConLab;
            [self.whiteBackView addSubview:theincoConLab];
            
            self.whiteBackView.height = self.theincoTitLab.bottom+20;
            self.bgView.height = self.whiteBackView.bottom;
            
        } else {
            //不是首单
            self.heJiLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.heJiTitLab.bottom+7, KWIDTH-20, 1)];
            
            //本单收益 标题
            UILabel *theincoTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, self.heJiLine.bottom+16, 100, 14)];
            theincoTitLab.textColor = zhutiColor;
            theincoTitLab.font = KFontPingFangSCMedium(14);
            theincoTitLab.text = @"本单收益";
            self.theincoTitLab = theincoTitLab;
            [self.whiteBackView addSubview:theincoTitLab];
            
            //本单收益 内容
            UILabel *theincoConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, self.heJiLine.bottom+16, 100, 14)];
            theincoConLab.textColor = zhutiColor;
            theincoConLab.textAlignment = NSTextAlignmentRight;
            theincoConLab.font = KFontPingFangSCMedium(14);
            theincoConLab.text = [NSString stringWithFormat:@"¥%.2f",[[_mydic objectForKey:@"earning"] floatValue]];
            self.theincoConLab = theincoConLab;
            [self.whiteBackView addSubview:theincoConLab];
            
            self.whiteBackView.height = self.theincoTitLab.bottom+20;
            self.bgView.height = self.whiteBackView.bottom;
        }
        [self showPayTimeLabel];
    }
}

/*
 #pragma mark - 质保期 标题
 - (UILabel *)warrantyTit {
 if (!_warrantyTit) {
 UILabel *warrantyTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.hourWageLine.bottom+12, (KWIDTH-20-48)/2, 20)];
 warrantyTit.textColor = K666666;
 warrantyTit.font = FontSize(14);
 warrantyTit.text = @"质保期";
 [self.whiteBackView addSubview:warrantyTit];
 self.warrantyTit = warrantyTit;
 }
 return _warrantyTit;
 }*/

#pragma mark - 本单收益 标题
- (UILabel *)theincoTitLab {
    if (!_theincoTitLab) {
        UILabel *theincoTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, self.heJiLine.bottom+16, 100, 14)];
        theincoTitLab.textColor = zhutiColor;
        theincoTitLab.font = KFontPingFangSCMedium(14);
        theincoTitLab.text = @"本单收益";
        self.theincoTitLab = theincoTitLab;
        [self.whiteBackView addSubview:theincoTitLab];
    }
    return _theincoTitLab;
}
 
 /*#pragma mark - 质保期 内容
 - (UILabel *)warrantyCon {
 if (!_warrantyCon) {
 UILabel *warrantyCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-20-48)/2, self.hourWageLine.bottom+12, (KWIDTH-20-48)/2, 20)];
 warrantyCon.textColor = K666666;
 warrantyCon.font = FontSize(14);
 warrantyCon.text = [NSString stringWithFormat:@"%.f %@",[[[_mydic objectForKey:@"order"] objectForKey:@"warranty"] floatValue], @"个月"];
 warrantyCon.textAlignment = NSTextAlignmentRight;
 [self.whiteBackView addSubview:warrantyCon];
 self.warrantyCon = warrantyCon;
 }
 return _warrantyCon;
 }
 
 #pragma mark - 质保期 分割线
 - (UILabel *)warrantyLine {
 if (!_warrantyLine) {
 UILabel *warrantyLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.warrantyTit.bottom +8, KWIDTH-20, 1)];
 warrantyLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
 [self.whiteBackView addSubview:warrantyLine];
 self.warrantyLine = warrantyLine;
 }
 return _warrantyLine;
 }
 */

#pragma mark - 显示或隐藏支付时间
- (void)showPayTimeLabel {
    UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(22, self.bgView.bottom+6, KWIDTH-44, 13)];
    payTime.text = [NSString stringWithFormat:@"支付时间: %@",_model.pay_time];
    payTime.textColor = [UIColor colorWithHexString:@"#232620"];
    payTime.font = FontSize(12);
    payTime.textAlignment = NSTextAlignmentRight;
    [self.scrollView addSubview:payTime];
    
    self.scrollView.contentSize = CGSizeMake(KWIDTH, payTime.bottom);
    
    if ([_model.order_state integerValue] < 3) {
        payTime.hidden = YES;
    } else {
        payTime.hidden = NO;
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
