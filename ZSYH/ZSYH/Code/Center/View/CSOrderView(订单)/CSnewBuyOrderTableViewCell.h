//
//  CSnewBuyOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSMallOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSnewBuyOrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *orderImage;
@property (weak, nonatomic) IBOutlet UILabel *orderContentLB;

@property (weak, nonatomic) IBOutlet UILabel *orderPriceLB;

@property (weak, nonatomic) IBOutlet UILabel *pruNumber;

-(void)refashWithModel:(CSMallOrderModel *)model;

@end

NS_ASSUME_NONNULL_END
