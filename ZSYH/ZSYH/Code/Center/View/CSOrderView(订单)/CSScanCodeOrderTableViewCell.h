//
//  CSScanCodeOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSScanCodeOrderTableViewCell : UITableViewCell
@property(nonatomic,copy)void (^myblock)(NSUInteger ind,NSString *str);

@property (nonatomic, strong) UILabel *orderNumLab;   //订单编号
@property (nonatomic, strong) UILabel *stateLab;      //状态
@property (nonatomic, strong) UIImageView *tipsImg;   //左侧提示图片
@property (nonatomic, strong) UILabel *nameConLab;    //姓名 内容
@property (nonatomic, strong) UILabel *phoneLab;      //电话 内容
@property (nonatomic, strong) UILabel *addrConLab;    //地址 内容
@property (nonatomic, strong) UILabel *errorTitLab;   //故障原因 标题
@property (nonatomic, strong) UILabel *errorConLab;   //故障原因 内容
@property (nonatomic, strong) UIButton *detailBtn;    //查看详情 按钮

-(void)refasf:(CSmaintenanceListModel *)model;

@end

NS_ASSUME_NONNULL_END
