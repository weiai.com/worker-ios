//
//  CSnewBuyOrderTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/17.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSnewBuyOrderTableViewCell.h"

@implementation CSnewBuyOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)refashWithModel:(CSMallOrderModel *)model{
    if ([model.product_pic hasPrefix:@"http"]) {
        [self.orderImage sd_setImageWithURL:[NSURL URLWithString:model.product_pic] placeholderImage:defaultImg];
    }else{
        sdimg(self.orderImage, model.product_pic);
        
    }
    
    self.orderContentLB.text = [NSString stringWithFormat:@"%@",model.product_name];
    self.orderPriceLB.text = [NSString stringWithFormat:@"¥%@",model.product_price];
    self.pruNumber.text = [NSString stringWithFormat:@"*%@",model.pro_count];
    

    
    
    
    
    
}

@end
