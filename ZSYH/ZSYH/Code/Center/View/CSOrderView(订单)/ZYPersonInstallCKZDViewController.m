//
//  ZYPersonInstallCKZDViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/28.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYPersonInstallCKZDViewController.h"
#import "SYBigImage.h"

@interface ZYPersonInstallCKZDViewController ()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) NSDictionary *mydic;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UILabel *whiteBackView;//白色背景(upLabel)

@property (nonatomic,strong) UILabel *pjCostTtitleLabel;//材料总费-标题
@property (nonatomic,strong) UILabel *pjCostCLab;//材料总费-内容

@property (nonatomic,strong) UITextView *pjNameCTV;

@property (nonatomic,strong) UILabel *gsfTitLab;//工时费-标题
@property (nonatomic,strong) UILabel *gsfConLab;//工时费-内容

@property (nonatomic,strong) UIImageView *myimage;
@property (nonatomic,strong) UIView *addView;

@property (nonatomic,strong) UILabel *acceNameTit; //配件名称 标题
@property (nonatomic,strong) UILabel *compPhotTit; //完工照片 标题

@property (nonatomic,strong) UILabel *heJiTitleLabel;//费用合计-标题
@property (nonatomic,strong) UILabel *heJiContentLabel;//费用合计-内容
@property (nonatomic,strong) UILabel *heJiLine;//费用合计下划线

@property (nonatomic,strong) UILabel *shouYiTitleLabel;//本单收益-标题
@property (nonatomic,strong) UILabel *shouYicContentLabel;//本单收益-标题
@property (nonatomic,strong) UILabel *firstOrderLine; //当日首单 分割线

@property (nonatomic,strong) UILabel *infoServFeeTit; //信息服务费 标题
@property (nonatomic,strong) UILabel *infoServFeeCon; //信息服务费 内容
@property (nonatomic,strong) UILabel *infoServFeeLine;//信息服务费 分割线

@property (nonatomic,strong) UILabel *techSuppFeeLine;//技术支持费 分割线
@end

@implementation ZYPersonInstallCKZDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"账单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    [self request];
    // Do any additional setup after loading the view.
}

-(void)request{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"orderId"] =NOTNIL(_model.orderId);
    kWeakSelf;
    [NetWorkTool POST:lookListacc param:param success:^(id dic) {
        self.mydic = [dic objectForKey:@"data"];
       
        [weakSelf.model setValuesForKeysWithDictionary:[weakSelf.mydic objectForKey:@"order"]];
        [self showDetaileUi];
        KMyLog(@"个人安装单 查看账单%@",dic);
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
    
}

-(void)showDetaileUi {

    //执行array不为空时的操作
    NSMutableArray *imageArr = [[_mydic objectForKey:@"imgs"] mutableCopy];
    
    if (imageArr != nil && ![imageArr isKindOfClass:[NSNull class]] && imageArr.count != 0) {
        //有配件名称和配件图片的情况
        
        self.bgView = [[UIView alloc]initWithFrame:CGRectMake(10, 20, KWIDTH-20, KHEIGHT-20)];
        [self.view addSubview:self.scrollView];
        [self.scrollView addSubview: self.bgView];
        self.bgView.backgroundColor = zhutiColor;
        [self.bgView shadowshadowOpacity:0.2 borderWidth:0 borderColor:[UIColor colorWithHexString:@"#DDDBD7"] erRadius:4 shadowColor:[UIColor colorWithHexString:@"#333333"] shadowRadius:5 shadowOffset:CGSizeMake(1, 1)];
        self.bgView.clipsToBounds = YES;
        self.bgView.layer.masksToBounds = YES;
        
        UILabel *order = [[UILabel alloc]initWithFrame:CGRectMake(31, 19, 200, 20)];
        order.textColor = [UIColor whiteColor];
        order.font = FontSize(16);
        order.text = @"订单编号";
        [self.bgView addSubview:order];
        
        UILabel *orderlb = [[UILabel alloc]initWithFrame:CGRectMake(31, 50, 200, 20)];
        orderlb.textColor = [UIColor whiteColor];
        orderlb.font = FontSize(16);
        orderlb.text = [NSString stringWithFormat:@"%@",[[_mydic objectForKey:@"order"] objectForKey:@"app_order_id"]];;
        [self.bgView addSubview:orderlb];
        
        UILabel *upLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 80, KWIDTH-20, 110)];
        upLable.backgroundColor = [UIColor whiteColor];
        self.whiteBackView = upLable;
        [self.bgView addSubview:upLable];
        
        //材料总费
        UILabel *pjCostTLab = [[UILabel alloc]initWithFrame:CGRectMake(24, 19, 100, 20)];
        pjCostTLab.text = @"材料总费:";
        pjCostTLab.textColor = K666666;
        pjCostTLab.font = KFontPingFangSCMedium(14);
        self.pjCostTtitleLabel = pjCostTLab;
        [upLable addSubview:pjCostTLab];
        
        UILabel *pjCostCLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, 19, 100, 20)];
        pjCostCLab.text = [NSString stringWithFormat:@"¥%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"productCost"] floatValue]];
        pjCostCLab.textColor = K666666;
        pjCostCLab.textAlignment = NSTextAlignmentRight;
        pjCostCLab.font = KFontPingFangSCMedium(14);
        [upLable addSubview:pjCostCLab];
        self.pjCostCLab = pjCostCLab;
        
        UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, pjCostTLab.bottom+10, KWIDTH-20, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [upLable addSubview:line1];
        
        //配件名称
        UILabel *acceNameTit = [[UILabel alloc]initWithFrame:CGRectMake(24, line1.bottom+15, 100, 20)];
        acceNameTit.textColor = K666666;
        acceNameTit.font = KFontPingFangSCMedium(14);
        acceNameTit.text = @"材料名称:";
        [upLable addSubview:acceNameTit];
        self.acceNameTit = acceNameTit;
        
        UITextView *pjNameCTV = [[UITextView alloc] init];
        pjNameCTV.frame = CGRectMake(24, acceNameTit.bottom + 11, KWIDTH-20-48, 76);
        pjNameCTV.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
        pjNameCTV.contentSize = CGSizeMake(KWIDTH-20-48, 176);
        pjNameCTV.layer.borderWidth = 1;
        pjNameCTV.layer.cornerRadius = 8;
        pjNameCTV.text = [NSString stringWithFormat:@"%@",[[_mydic objectForKey:@"order"] objectForKey:@"prodcutComment"]];
        pjNameCTV.font = FontSize(14);
        pjNameCTV.editable = NO;
        [upLable addSubview:pjNameCTV];
        self.pjNameCTV = pjNameCTV;
        
        //配件图片
        UILabel *compPhotTit = [[UILabel alloc]initWithFrame:CGRectMake(24, pjNameCTV.bottom+15, 100, 20)];
        compPhotTit.textColor = K666666;
        compPhotTit.font = KFontPingFangSCMedium(14);
        compPhotTit.text = @"完工照片:";
        [upLable addSubview:compPhotTit];
        self.compPhotTit = compPhotTit;
        
        UIView *addView = [[UIView alloc] init];
        addView.frame = CGRectMake(10, compPhotTit.bottom + 10, KWIDTH-20-48, 65);
        [upLable addSubview:addView];
        self.addView = addView;
        
        NSMutableArray *imagearr = [[_mydic objectForKey:@"imgs"] mutableCopy];
        CGFloat wei = (KWIDTH-25-46-46)/4;
        if (imagearr.count > 0) {
            for (int i = 0; i < imagearr.count; i++) {
                _myimage = [[UIImageView alloc]init];
                [addView addSubview:_myimage];
                [_myimage sd_setImageWithURL:[NSURL URLWithString:[imagearr[i] objectForKey:@"img"]] placeholderImage:defaultImg];
                _myimage.frame = CGRectMake(14+(wei+10)*i, 0, wei, wei);
                _myimage.userInteractionEnabled = YES;
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [_myimage addGestureRecognizer:bigI];
            }
        }
        
        UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, self.addView.bottom+10, KWIDTH-20, 1)];
        line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [upLable addSubview:line2];
        
        //工时费-标题
        UILabel *gsfTitLab = [[UILabel alloc]initWithFrame:CGRectMake(24, line2.bottom+13, 100, 20)];
        gsfTitLab.textColor = K666666;
        gsfTitLab.font = KFontPingFangSCMedium(14);
        gsfTitLab.text = @"工时费:";
        [upLable addSubview:gsfTitLab];
        
        //工时费-内容
        UILabel *gsfConLab = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line2.bottom+13, 100, 20)];
        gsfConLab.textColor = K666666;
        gsfConLab.textAlignment = NSTextAlignmentRight;
        gsfConLab.font = KFontPingFangSCMedium(14);
        gsfConLab.text = [NSString stringWithFormat:@"¥%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"peopleCost"] floatValue]];
        [upLable addSubview:gsfConLab];
        self.gsfConLab = gsfConLab;
        
        UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, gsfTitLab.bottom+7, KWIDTH-20, 1)];
        line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        [upLable addSubview:line3];
        
        //费用合计 标题
        UILabel *hejiT = [[UILabel alloc]initWithFrame:CGRectMake(24, line3.bottom+13, 100, 20)];
        hejiT.textColor = K666666;
        hejiT.font = KFontPingFangSCMedium(14);
        hejiT.text = @"费用合计:";
        self.heJiTitleLabel = hejiT;
        [upLable addSubview:hejiT];
        
        //费用合计 内容
        UILabel *hejiL = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line3.bottom+13, 100, 20)];
        hejiL.textColor = K666666;
        hejiL.textAlignment = NSTextAlignmentRight;
        hejiL.font = KFontPingFangSCMedium(14);
        hejiL.text = [NSString stringWithFormat:@"¥%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"all_cost"] floatValue]];
        self.heJiContentLabel = hejiL;
        [upLable addSubview:hejiL];
        
        //费用合计 分割线
        UILabel *line33 = [[UILabel alloc]initWithFrame:CGRectMake(0, hejiL.bottom+7, KWIDTH-20, 1)];
        line33.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        self.heJiLine = line33;
        [upLable addSubview:line33];
        
        //本单收益 标题
        UILabel *myshouyi = [[UILabel alloc]initWithFrame:CGRectMake(24, line33.bottom+13, 100, 14)];
        myshouyi.textColor = zhutiColor;
        myshouyi.font = KFontPingFangSCMedium(14);
        myshouyi.text = @"本单收益";
        self.shouYiTitleLabel = myshouyi;
        [upLable addSubview:myshouyi];
        
        //本单收益 内容
        UILabel *myshouyilb = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, line33.bottom+13, 100, 14)];
        myshouyilb.textColor = zhutiColor;
        myshouyilb.textAlignment = NSTextAlignmentRight;
        myshouyilb.font = KFontPingFangSCMedium(14);
        myshouyilb.text = [NSString stringWithFormat:@"¥%.2f",[[_mydic objectForKey:@"earning"] floatValue]];
        [upLable addSubview:myshouyilb];
        self.shouYicContentLabel = myshouyilb;

        upLable.height = myshouyilb.bottom +20;
        self.bgView.height = upLable.bottom;
    }
    
    //如果订单未支付
    if ([_model.order_state integerValue] < 3) {
        
        self.shouYiTitleLabel.hidden = YES;
        self.shouYicContentLabel.hidden = YES;
        self.heJiLine.hidden = YES;
        self.whiteBackView.height = self.heJiTitleLabel.bottom+10;
        self.bgView.height = self.whiteBackView.bottom;
        
    } else {
        NSString *insurance_premium = [[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"];
        //insurance_premium = @"1";
        if ([insurance_premium floatValue] > 0) {
            //当日首单 标题
            UILabel *firstOrderFeeL = [[UILabel alloc]initWithFrame:CGRectMake(24, self.heJiLine.bottom +13, 150, 14)];
            firstOrderFeeL.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            firstOrderFeeL.font = KFontPingFangSCMedium(14);
            firstOrderFeeL.text = @"当日首单-为您投保";
            [self.whiteBackView addSubview:firstOrderFeeL];
            
            //当日首单 内容
            UILabel *firstOrderFee = [[UILabel alloc]initWithFrame:CGRectMake(KWIDTH-44-100, firstOrderFeeL.top, 100, 14)];
            firstOrderFee.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            firstOrderFee.textAlignment = NSTextAlignmentRight;
            firstOrderFee.font = KFontPingFangSCMedium(14);
            firstOrderFee.text = [NSString stringWithFormat:@"-¥ %@",[[_mydic objectForKey:@"order"] objectForKey:@"insurance_premium"]];
            [self.whiteBackView addSubview:firstOrderFee];
            
            //当日首单 分割线
            UILabel *firstOrderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, firstOrderFeeL.bottom+11, KWIDTH-20, 1)];
            firstOrderLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:firstOrderLine];
            self.firstOrderLine = firstOrderLine;
            
            //信息服务费 标题
            UILabel *infoServFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.firstOrderLine.bottom +10, (KWIDTH-48-20)/2, 20)];
            infoServFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            infoServFeeTit.font = KFontPingFangSCMedium(14);
            infoServFeeTit.text = @"信息服务费 (10%)";
            [self.whiteBackView addSubview:infoServFeeTit];
            
            //信息服务费 标题
            UILabel *infoServFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-48-20)/2, self.firstOrderLine.bottom +10, (KWIDTH-48-20)/2, 20)];
            infoServFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            infoServFeeCon.font = KFontPingFangSCMedium(14);
            infoServFeeCon.textAlignment = NSTextAlignmentRight;
            //infoServFeeCon.text = @"-￥3.50";
            CGFloat earPro = 0;
            CGFloat earCit = 0;
            CGFloat earPla = 0;
            CGFloat sum = 0;
            
            NSString *earProStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earProvince"] floatValue]];
            earPro = [earProStr floatValue];
            NSString *earCitStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earCity"] floatValue]];
            earCit = [earCitStr floatValue];
            NSString *earPlaStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earPlatform"] floatValue]];
            earPla = [earPlaStr floatValue];
            sum = earPro + earCit + earPla;
            infoServFeeCon.text = [NSString stringWithFormat:@"-¥ %.2f", sum];
            [self.whiteBackView addSubview:infoServFeeCon];
            
            //信息服务费 分割线
            UILabel *infoServFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, infoServFeeTit.bottom+10, KWIDTH-20, 1)];
            infoServFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:infoServFeeLine];
            self.infoServFeeLine = infoServFeeLine;
            
            //技术支持费 标题
            UILabel *techSuppFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.infoServFeeLine.bottom + 12, (KWIDTH-48-20)/2, 20)];
            techSuppFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            techSuppFeeTit.font = KFontPingFangSCMedium(14);
            techSuppFeeTit.text = @"技术支持费 (6%)";
            [self.whiteBackView addSubview:techSuppFeeTit];
            
            //技术支持费 标题
            UILabel *techSuppFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-48-20)/2, self.infoServFeeLine.bottom + 12, (KWIDTH-48-20)/2, 20)];
            techSuppFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            techSuppFeeCon.font = KFontPingFangSCMedium(14);
            techSuppFeeCon.textAlignment = NSTextAlignmentRight;
            techSuppFeeCon.text = [NSString stringWithFormat:@"-¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"tax"] floatValue]];
            [self.whiteBackView addSubview:techSuppFeeCon];
            
            //技术支持费 分割线
            UILabel *techSuppFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, techSuppFeeCon.bottom +12, KWIDTH-20, 1)];
            techSuppFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:techSuppFeeLine];
            self.techSuppFeeLine = techSuppFeeLine;
            
            self.shouYiTitleLabel.frame = CGRectMake(24, techSuppFeeLine.bottom + 16, 100, 14);
            self.shouYicContentLabel.frame = CGRectMake(KWIDTH-44-100, self.shouYiTitleLabel.top, 100, 14);

            self.whiteBackView.height = self.shouYiTitleLabel.bottom+20;
            self.bgView.height = self.whiteBackView.bottom;

        } else {
            //不是首单
            self.heJiLine = [[UILabel alloc]initWithFrame:CGRectMake(0, self.heJiTitleLabel.bottom+7, KWIDTH-20, 1)];
            
            //信息服务费 标题
            UILabel *infoServFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.heJiLine.bottom +12, (KWIDTH-48-20)/2, 20)];
            infoServFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            infoServFeeTit.font = KFontPingFangSCMedium(14);
            infoServFeeTit.text = @"信息服务费 (10%)";
            [self.whiteBackView addSubview:infoServFeeTit];
            
            //信息服务费 标题
            UILabel *infoServFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-48-20)/2, self.heJiLine.bottom +12, (KWIDTH-48-20)/2, 20)];
            infoServFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            infoServFeeCon.font = KFontPingFangSCMedium(14);
            infoServFeeCon.textAlignment = NSTextAlignmentRight;
            //infoServFeeCon.text = @"-￥3.50";
            CGFloat earPro = 0;
            CGFloat earCit = 0;
            CGFloat earPla = 0;
            CGFloat sum = 0;
            
            NSString *earProStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earProvince"] floatValue]];
            earPro = [earProStr floatValue];
            NSString *earCitStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earCity"] floatValue]];
            earCit = [earCitStr floatValue];
            NSString *earPlaStr = [NSString stringWithFormat:@"%.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"earPlatform"] floatValue]];
            earPla = [earPlaStr floatValue];
            sum = earPro + earCit + earPla;
            infoServFeeCon.text = [NSString stringWithFormat:@"-¥ %.2f", sum];
            [self.whiteBackView addSubview:infoServFeeCon];
            
            //信息服务费 分割线
            UILabel *infoServFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, infoServFeeTit.bottom+10, KWIDTH-20, 1)];
            infoServFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:infoServFeeLine];
            self.infoServFeeLine = infoServFeeLine;
            
            //技术支持费 标题
            UILabel *techSuppFeeTit = [[UILabel alloc]initWithFrame:CGRectMake(24, self.infoServFeeLine.bottom + 12, (KWIDTH-48-20)/2, 20)];
            techSuppFeeTit.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            techSuppFeeTit.font = KFontPingFangSCMedium(14);
            techSuppFeeTit.text = @"技术支持费 (6%)";
            [self.whiteBackView addSubview:techSuppFeeTit];
            
            //技术支持费 标题
            UILabel *techSuppFeeCon = [[UILabel alloc]initWithFrame:CGRectMake(24 + (KWIDTH-48-20)/2, self.infoServFeeLine.bottom + 12, (KWIDTH-48-20)/2, 20)];
            techSuppFeeCon.textColor = [UIColor colorWithHexString:@"#F88B1F"];
            techSuppFeeCon.font = KFontPingFangSCMedium(14);
            techSuppFeeCon.textAlignment = NSTextAlignmentRight;
            techSuppFeeCon.text = [NSString stringWithFormat:@"-¥ %.2f",[[[_mydic objectForKey:@"order"] objectForKey:@"tax"] floatValue]];
            [self.whiteBackView addSubview:techSuppFeeCon];
            
            //技术支持费 分割线
            UILabel *techSuppFeeLine = [[UILabel alloc]initWithFrame:CGRectMake(0, techSuppFeeCon.bottom +12, KWIDTH-20, 1)];
            techSuppFeeLine.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.whiteBackView addSubview:techSuppFeeLine];
            self.techSuppFeeLine = techSuppFeeLine;
            
            self.shouYiTitleLabel.frame = CGRectMake(24, techSuppFeeLine.bottom + 16, 100, 14);
            self.shouYicContentLabel.frame = CGRectMake(KWIDTH-44-100, self.shouYiTitleLabel.top, 100, 14);
            
            self.whiteBackView.height = self.shouYiTitleLabel.bottom+20;
            self.bgView.height = self.whiteBackView.bottom;
        }
    }

    [self showPayTimeLabel];
}

#pragma mark - 显示或隐藏支付时间
- (void)showPayTimeLabel {
    UILabel *payTime = [[UILabel alloc]initWithFrame:CGRectMake(22, self.bgView.bottom+6, KWIDTH-44, 13)];
    payTime.text = [NSString stringWithFormat:@"支付时间: %@",_model.pay_time];
    payTime.textColor = [UIColor colorWithHexString:@"#232620"];
    payTime.font = FontSize(12);
    payTime.textAlignment = NSTextAlignmentRight;
    [self.scrollView addSubview:payTime];
    
    self.scrollView.contentSize = CGSizeMake(KWIDTH, payTime.bottom +10);
    
    if ([_model.order_state integerValue] < 3) {
        payTime.hidden = YES;
    } else {
        payTime.hidden = NO;
    }
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT);
        _scrollView.delegate = self;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
