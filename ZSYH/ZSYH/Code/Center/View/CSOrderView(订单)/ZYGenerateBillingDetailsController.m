//
//  ZYGenerateBillingDetailsController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/8/16.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ZYGenerateBillingDetailsController.h"
#import "TanKuangNoseleview.h"
#import <AVFoundation/AVFoundation.h>
#import "XWScanImage.h"
#import "SYBigImage.h"
#import "ZYAddHoubaoAccessoriesViewController.h"
#import "ZYAddAccessoriesViewController.h" //添加配件

@interface ZYGenerateBillingDetailsController ()<UITextFieldDelegate, UITextViewDelegate>
@property (strong, nonatomic) UILabel *orderNumberLb; //订单编号
@property (strong, nonatomic) UITextField *hourlyWageTF; //工时费
@property (strong, nonatomic) UITextField *accessoFeeTF; //配件总费
@property (strong, nonatomic) UITextView *accessoryNameTV; //配件名称
@property (strong, nonatomic) UILabel *totalLab; //合计
@property (strong, nonatomic) UIButton *pushBtn; //推送账单
@property (strong, nonatomic) NSString *fuwufei;
@property (nonatomic, strong) TanKuangNoseleview *tankiuiang;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *imageArr;//
@property (nonatomic, strong) UIImageView *myimage;
@property (nonatomic, strong) UIView *addView;//
@property (nonatomic, assign) CGFloat bottomf;//
@property (nonatomic, strong) UIView *bgViewsecOne;
@property (nonatomic, strong) UIView *bgViewsecAnZhuang; //个人安装
@property (nonatomic, strong) UIView *bgViewsecjiedan;//弹框背景
@property (nonatomic, strong) UILabel *placeLab;
@property (nonatomic, strong) UILabel *stirngLenghLabel;
@property (nonatomic, strong) UILabel *zbqConLab;

@end

@implementation ZYGenerateBillingDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"生成账单";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    
    self.imageArr  = [NSMutableArray arrayWithCapacity:1];
    self.myimage = [[UIImageView alloc]init];
    _myimage.image = imgname(@"addpicimage");
    [self.imageArr addObject:_myimage];
    
    //self.orderNumber.text = _orderNumberStr;
    self.orderNumberLb.text = _neworderid;
    self.accessoryNameTV.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.totalLab.text = [NSString stringWithFormat:@"¥0.00"];
    
    kWeakSelf;
    self.tankiuiang = [[TanKuangNoseleview alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
    self.tankiuiang.titleLable.text = @"账单推送成功";
    self.tankiuiang.msgLable.hidden = YES;
    
    self.tankiuiang.myblcok = ^(NSInteger ind) {
        
        if (weakSelf.myblockj) {
            weakSelf.myblockj(@"");
        }
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self showBillUI];
    [self shwoBgviewsec];
    [self shwoBgviewsecAnzhuang]; //个人安装
    
    // Do any additional setup after loading the view from its nib.
}

- (void)showBillUI {
    [self.view addSubview:self.scrollView];
    CGFloat left1 = 16;
    
    if ([self.mymodel.user_type isEqualToString:@"0"]) { //个人
    //if ([self.mymodel.fault_type isEqualToString:@"3"]) { //维修-配件
        if ([self.mymodel.fault_type isEqualToString:@"1"]) { //个人安装 生成账单
            //订单编号标题
            UILabel *orderTlab = [[UILabel alloc] init];
            orderTlab.frame = CGRectMake(left1,0,70,55);
            orderTlab.numberOfLines = 0;
            orderTlab.text = @"订单编号:";
            orderTlab.font = FontSize(16);
            orderTlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderTlab];
            
            //订单编号内容
            UILabel *orderDlab = [[UILabel alloc] init];
            orderDlab.frame = CGRectMake(orderTlab.right + 13,0,KWIDTH,55);
            orderDlab.numberOfLines = 0;
            orderDlab.text = _mymodel.Id;
            orderDlab.font = FontSize(16);
            orderDlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderDlab];
            
            UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, orderTlab.bottom, KWIDTH, 10)];
            line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line0];
            
            //工时费标题
            UILabel *hourlyTlab = [[UILabel alloc] init];
            hourlyTlab.frame = CGRectMake(left1,line0.bottom,70,48);
            hourlyTlab.numberOfLines = 0;
            hourlyTlab.text = @"工时费:";
            hourlyTlab.font = FontSize(16);
            hourlyTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:hourlyTlab];
            
            //货币符号
            UILabel *fuhaoLab = [[UILabel alloc] init];
            fuhaoLab.frame = CGRectMake(hourlyTlab.right + 6, line0.bottom, 12, 48);
            fuhaoLab.text = @"¥";
            fuhaoLab.font = FontSize(16);
            fuhaoLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab];
            
            //工时费内容
            UITextField *hourlyWageTF = [[UITextField alloc] initWithFrame:CGRectMake(fuhaoLab.right + 6,line0.bottom,KWIDTH / 2,48)];
            hourlyWageTF.keyboardType = UIKeyboardTypeNumberPad;
            hourlyWageTF.placeholder = @"请输入工时费";
            hourlyWageTF.textColor = [UIColor blackColor];
            hourlyWageTF.font = FontSize(16);
            hourlyWageTF.textAlignment = NSTextAlignmentLeft;
            hourlyWageTF.delegate = self;
            [hourlyWageTF becomeFirstResponder];
            //[hourlyWageTF addRules];
            self.hourlyWageTF = hourlyWageTF;
            [self.scrollView addSubview:hourlyWageTF];
            
            UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, hourlyTlab.bottom, KWIDTH, 2)];
            line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line1];
            
            //配件总费标题
            UILabel *accesTlab = [[UILabel alloc] init];
            accesTlab.frame = CGRectMake(left1,line1.bottom,70,48);
            accesTlab.numberOfLines = 0;
            accesTlab.text = @"材料总费:";
            accesTlab.font = FontSize(16);
            accesTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:accesTlab];
            
            //货币符号
            UILabel *fuhaoLab1 = [[UILabel alloc] init];
            fuhaoLab1.frame = CGRectMake(accesTlab.right + 6, line1.bottom, 12, 48);
            fuhaoLab1.text = @"¥";
            fuhaoLab1.font = FontSize(16);
            fuhaoLab1.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab1];
            
            //配件总费内容
            UITextField *accessoFeeTF = [[UITextField alloc] init];
            accessoFeeTF.frame = CGRectMake(fuhaoLab1.right + 6,line1.bottom,KWIDTH / 2,48);
            accessoFeeTF.keyboardType = UIKeyboardTypeNumberPad;
            accessoFeeTF.placeholder = @"请输入材料总费";
            accessoFeeTF.font = FontSize(16);
            accessoFeeTF.textAlignment = NSTextAlignmentLeft;
            accessoFeeTF.delegate = self;
            //[accessoFeeTF becomeFirstResponder];
            self.accessoFeeTF = accessoFeeTF;
            [self.scrollView addSubview:accessoFeeTF];
            
            UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, accesTlab.bottom, KWIDTH, 2)];
            line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line2];
            
            //请输入配件名称标题
            UILabel *AccNamelab = [[UILabel alloc] init];
            AccNamelab.frame = CGRectMake(left1, line2.bottom, KWIDTH -left1*2, 48);
            AccNamelab.numberOfLines = 0;
            AccNamelab.text = @"请输入材料名称  (如果没有请填写无)";
            AccNamelab.font = FontSize(16);
            AccNamelab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccNamelab];
            
            //配件名称输入文本框
            UITextView *accessoryNameTV = [[UITextView alloc] init];
            accessoryNameTV.frame = CGRectMake(left1,AccNamelab.bottom,KWIDTH - 32,124);
            accessoryNameTV.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
            accessoryNameTV.layer.borderWidth = 1;
            accessoryNameTV.layer.cornerRadius = 8;
            accessoryNameTV.font = FontSize(14);
            [accessoryNameTV becomeFirstResponder];
            accessoryNameTV.delegate = self;
            [self.scrollView addSubview:accessoryNameTV];
            self.accessoryNameTV = accessoryNameTV;
            //占位 Label
            UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
            placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
            placeLab.text = @"请输入材料名称";
            placeLab.font = FontSize(14);
            self.placeLab = placeLab;
            [self.accessoryNameTV addSubview:placeLab];
            
            UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, accessoryNameTV.bottom+10, KWIDTH, 2)];
            line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line3];
            
            UILabel *starlab1 = [[UILabel alloc] init];
            starlab1.frame = CGRectMake(left1,line3.bottom,9,45);
            starlab1.numberOfLines = 0;
            starlab1.text = @"*";
            starlab1.font = FontSize(16);
            starlab1.textColor = [UIColor colorWithHexString:@"#FF000A"];
            [self.scrollView addSubview:starlab1];
            
            //请添加配件图片
            UILabel *AccImgLab = [[UILabel alloc] init];
            AccImgLab.frame = CGRectMake(starlab1.right + 1,line3.bottom,KWIDTH,45);
            AccImgLab.numberOfLines = 0;
            AccImgLab.text = @"请添加材料图片";
            AccImgLab.font = FontSize(16);
            AccImgLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccImgLab];
            
            self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, AccImgLab.bottom, KWIDTH, 100)];
            [self.scrollView addSubview: self.addView];
            CGFloat www = (KWIDTH - 62)/4;
            self.bottomf = AccImgLab.bottom;
            [self showinageWirhArr:_imageArr];
            
            CGRect ffff = self.addView.frame;
            ffff.size.height = www +10;
            self.addView.frame = ffff;
            
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom+10, KWIDTH, 2)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            //合计
            UILabel *totalLab = [[UILabel alloc] init];
            totalLab = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom + 27, KWIDTH, 18)];
            totalLab .font = FontSize(18);
            totalLab.textAlignment = NSTextAlignmentCenter;
            totalLab.text = [NSString stringWithFormat:@"合计: ¥0.00"];
            [self.scrollView addSubview: totalLab];
            self.totalLab = totalLab;
            
            UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
            [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
            [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
            tuiusongBut.frame  = CGRectMake(28, totalLab.bottom + 10, KWIDTH-28-28, 48);
            tuiusongBut.layer.masksToBounds = YES;
            tuiusongBut.layer.cornerRadius = 4;
            [self.scrollView addSubview: tuiusongBut ];
            
            [tuiusongBut addTarget:self action:@selector(pushBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        } else if ([self.mymodel.fault_type isEqualToString:@"3"]) { //个人维修 生成账单
            //订单编号标题
            UILabel *orderTlab = [[UILabel alloc] init];
            orderTlab.frame = CGRectMake(left1,0,70,55);
            orderTlab.numberOfLines = 0;
            orderTlab.text = @"订单编号:";
            orderTlab.font = FontSize(16);
            orderTlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderTlab];
            //订单编号内容
            UILabel *orderDlab = [[UILabel alloc] init];
            orderDlab.frame = CGRectMake(orderTlab.right + 13,0,KWIDTH,55);
            orderDlab.numberOfLines = 0;
            orderDlab.text = _mymodel.Id;
            orderDlab.font = FontSize(16);
            orderDlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderDlab];
            
            UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, orderTlab.bottom, KWIDTH, 10)];
            line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line0];
            
            //工时费标题
            UILabel *hourlyTlab = [[UILabel alloc] init];
            hourlyTlab.frame = CGRectMake(left1,line0.bottom,70,48);
            hourlyTlab.numberOfLines = 0;
            hourlyTlab.text = @"工时费:";
            hourlyTlab.font = FontSize(16);
            hourlyTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:hourlyTlab];
            
            //货币符号
            UILabel *fuhaoLab = [[UILabel alloc] init];
            fuhaoLab.frame = CGRectMake(hourlyTlab.right + 6, line0.bottom, 12, 48);
            fuhaoLab.text = @"¥";
            fuhaoLab.font = FontSize(16);
            fuhaoLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab];
            
            //工时费内容
            UITextField *hourlyWageTF = [[UITextField alloc] initWithFrame:CGRectMake(fuhaoLab.right + 6,line0.bottom,KWIDTH / 2,48)];
            hourlyWageTF.keyboardType = UIKeyboardTypeNumberPad;
            hourlyWageTF.placeholder = @"请输入工时费";
            hourlyWageTF.textColor = [UIColor blackColor];
            hourlyWageTF.font = FontSize(16);
            hourlyWageTF.textAlignment = NSTextAlignmentLeft;
            hourlyWageTF.delegate = self;
            [hourlyWageTF becomeFirstResponder];
            //[hourlyWageTF addRules];
            self.hourlyWageTF = hourlyWageTF;
            [self.scrollView addSubview:hourlyWageTF];
            
            UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, hourlyTlab.bottom, KWIDTH, 2)];
            line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line1];
            
            //配件总费标题
            UILabel *accesTlab = [[UILabel alloc] init];
            accesTlab.frame = CGRectMake(left1,line1.bottom,70,48);
            accesTlab.numberOfLines = 0;
            accesTlab.text = @"配件总费:";
            accesTlab.font = FontSize(16);
            accesTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:accesTlab];
            
            //货币符号
            UILabel *fuhaoLab1 = [[UILabel alloc] init];
            fuhaoLab1.frame = CGRectMake(accesTlab.right + 6, line1.bottom, 12, 48);
            fuhaoLab1.text = @"¥";
            fuhaoLab1.font = FontSize(16);
            fuhaoLab1.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab1];
            
            //配件总费内容
            UITextField *accessoFeeTF = [[UITextField alloc] init];
            accessoFeeTF.frame = CGRectMake(fuhaoLab1.right + 6,line1.bottom,KWIDTH / 2,48);
            accessoFeeTF.keyboardType = UIKeyboardTypeNumberPad;
            accessoFeeTF.placeholder = @"请输入配件总费";
            accessoFeeTF.font = FontSize(16);
            accessoFeeTF.textAlignment = NSTextAlignmentLeft;
            accessoFeeTF.delegate = self;
            //[accessoFeeTF becomeFirstResponder];
            self.accessoFeeTF = accessoFeeTF;
            [self.scrollView addSubview:accessoFeeTF];
            
            UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, accesTlab.bottom, KWIDTH, 2)];
            line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line2];
            
            //质保期 标题
            UILabel *zbqTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, line2.bottom, 70, 48)];
            zbqTitLab.text = @"质保期";
            zbqTitLab.font = FontSize(16);
            zbqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:zbqTitLab];

            //质保期 文本框
            UILabel *zbqConLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, line2.bottom +8, 60, 32)];
            zbqConLab.font = FontSize(16);
            zbqConLab.textColor = [UIColor colorWithHexString:@"#333333"];
            zbqConLab.textAlignment = NSTextAlignmentLeft;
            [self.scrollView addSubview:zbqConLab];
            self.zbqConLab = zbqConLab;
            //添加点击事件
            UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelClick)];
            [zbqConLab addGestureRecognizer:labelTapGestureRecognizer];
            zbqConLab.userInteractionEnabled = YES;
            
            //输入框 下划线
            UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, zbqConLab.bottom, 60, 1)];
            bottomLine.backgroundColor = [UIColor colorWithHexString:@"#BBBBBB"];
            [self.scrollView addSubview:bottomLine];

            //质保期 多少个月
            UILabel *monthLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqConLab.right, line2.bottom, 50, 48)];
            monthLab.text = @"个月";
            monthLab.font = FontSize(16);
            monthLab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:monthLab];

            UILabel *line5 = [[UILabel alloc] initWithFrame:CGRectMake(0, zbqTitLab.bottom, KWIDTH, 2)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            //请输入配件名称标题
            UILabel *AccNamelab = [[UILabel alloc] init];
            AccNamelab.frame = CGRectMake(left1,line5.bottom,KWIDTH- left1*2,48);
            AccNamelab.numberOfLines = 0;
            AccNamelab.text = @"请输入配件名称  (如果没有请填写无)";
            AccNamelab.font = FontSize(16);
            AccNamelab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccNamelab];

            //配件名称输入文本框
            UITextView *accessoryNameTV = [[UITextView alloc] init];
            accessoryNameTV.frame = CGRectMake(left1,AccNamelab.bottom,KWIDTH - 32,124);
            accessoryNameTV.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
            accessoryNameTV.layer.borderWidth = 1;
            accessoryNameTV.layer.cornerRadius = 8;
            accessoryNameTV.font = FontSize(14);
            [accessoryNameTV becomeFirstResponder];
            accessoryNameTV.delegate = self;
            [self.scrollView addSubview:accessoryNameTV];
            self.accessoryNameTV = accessoryNameTV;
            //占位 Label
            UILabel *placeLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 11, KWIDTH, 14)];
            placeLab.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
            placeLab.text = @"请输入配件名称";
            placeLab.font = FontSize(14);
            self.placeLab = placeLab;
            [self.accessoryNameTV addSubview:placeLab];

            UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, accessoryNameTV.bottom+10, KWIDTH, 2)];
            line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line3];

            UILabel *starlab1 = [[UILabel alloc] init];
            starlab1.frame = CGRectMake(left1,line3.bottom,9,45);
            starlab1.numberOfLines = 0;
            starlab1.text = @"*";
            starlab1.font = FontSize(16);
            starlab1.textColor = [UIColor colorWithHexString:@"#FF000A"];
            [self.scrollView addSubview:starlab1];

            //请添加配件图片
            UILabel *AccImgLab = [[UILabel alloc] init];
            AccImgLab.frame = CGRectMake(starlab1.right + 1,line3.bottom,KWIDTH,45);
            AccImgLab.numberOfLines = 0;
            AccImgLab.text = @"请添加完工照片";
            AccImgLab.font = FontSize(16);
            AccImgLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccImgLab];

            self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, AccImgLab.bottom, KWIDTH, 100)];
            [self.scrollView addSubview: self.addView];
            CGFloat www = (KWIDTH - 62)/4;
            self.bottomf = AccImgLab.bottom;
            [self showinageWirhArr:_imageArr];

            CGRect ffff = self.addView.frame;
            ffff.size.height = www +10;
            self.addView.frame = ffff;

            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom+10, KWIDTH, 2)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];

            //合计
            UILabel *totalLab = [[UILabel alloc] init];
            totalLab = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom + 27, KWIDTH, 18)];
            totalLab .font = FontSize(18);
            totalLab.textAlignment = NSTextAlignmentCenter;
            totalLab.text = [NSString stringWithFormat:@"合计: ¥0.00"];
            [self.scrollView addSubview: totalLab];
            self.totalLab = totalLab;

            UIButton *tuiusongBut  = [UIButton buttonWithType:(UIButtonTypeCustom)];
            [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
            [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
            tuiusongBut.frame  = CGRectMake(28, totalLab.bottom + 10, KWIDTH-28-28, 48);
            tuiusongBut.layer.masksToBounds = YES;
            tuiusongBut.layer.cornerRadius = 4;
            [self.scrollView addSubview: tuiusongBut ];

            [tuiusongBut addTarget:self action:@selector(pushBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        }
        
    } else { //电器厂订单 生成账单
        if ([self.mymodel.fault_type isEqualToString:@"1"]) { //厂家 安装-材料 生成账单
            //订单编号标题
            UILabel *orderTlab = [[UILabel alloc] init];
            orderTlab.frame = CGRectMake(left1,0,70,55);
            orderTlab.numberOfLines = 0;
            orderTlab.text = @"订单编号:";
            orderTlab.font = FontSize(16);
            orderTlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderTlab];
            //订单编号内容
            UILabel *orderDlab = [[UILabel alloc] init];
            orderDlab.frame = CGRectMake(orderTlab.right + 13,0,KWIDTH,55);
            orderDlab.numberOfLines = 0;
            orderDlab.text = _mymodel.Id;
            orderDlab.font = FontSize(16);
            orderDlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderDlab];
            
            UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, orderTlab.bottom, KWIDTH, 10)];
            line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line0];
            
            //工时费标题
            UILabel *hourlyTlab = [[UILabel alloc] init];
            hourlyTlab.frame = CGRectMake(left1,line0.bottom,70,48);
            hourlyTlab.numberOfLines = 0;
            hourlyTlab.text = @"工时费:";
            hourlyTlab.font = FontSize(16);
            hourlyTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:hourlyTlab];
            
            //货币符号
            UILabel *fuhaoLab = [[UILabel alloc] init];
            fuhaoLab.frame = CGRectMake(hourlyTlab.right + 6, line0.bottom, 12, 48);
            fuhaoLab.text = @"¥";
            fuhaoLab.font = FontSize(16);
            fuhaoLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab];
            
            //工时费内容
            UITextField *hourlyWageTF = [[UITextField alloc] initWithFrame:CGRectMake(fuhaoLab.right + 6,line0.bottom,KWIDTH / 2,48)];
            hourlyWageTF.keyboardType = UIKeyboardTypeNumberPad;
            hourlyWageTF.placeholder = @"请输入工时费";
            hourlyWageTF.textColor = [UIColor blackColor];
            hourlyWageTF.font = FontSize(16);
            hourlyWageTF.textAlignment = NSTextAlignmentLeft;
            hourlyWageTF.delegate = self;
            //[hourlyWageTF becomeFirstResponder];
            //[hourlyWageTF addRules];
            self.hourlyWageTF = hourlyWageTF;
            [self.scrollView addSubview:hourlyWageTF];
            
            UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, hourlyTlab.bottom, KWIDTH, 2)];
            line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line1];
            
            //配件总费标题
            UILabel *accesTlab = [[UILabel alloc] init];
            accesTlab.frame = CGRectMake(left1,line1.bottom,70,48);
            accesTlab.numberOfLines = 0;
            accesTlab.text = @"材料总费:";
            accesTlab.font = FontSize(16);
            accesTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:accesTlab];
            
            //货币符号
            UILabel *fuhaoLab1 = [[UILabel alloc] init];
            fuhaoLab1.frame = CGRectMake(accesTlab.right + 6, line1.bottom, 12, 48);
            fuhaoLab1.text = @"¥";
            fuhaoLab1.font = FontSize(16);
            fuhaoLab1.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab1];
            
            //配件总费内容
            UITextField *accessoFeeTF = [[UITextField alloc] init];
            accessoFeeTF.frame = CGRectMake(fuhaoLab1.right + 6,line1.bottom,KWIDTH / 2,48);
            accessoFeeTF.keyboardType = UIKeyboardTypeNumberPad;
            accessoFeeTF.placeholder = @"请输入材料总费";
            accessoFeeTF.font = FontSize(16);
            accessoFeeTF.textAlignment = NSTextAlignmentLeft;
            accessoFeeTF.delegate = self;
            //[accessoFeeTF becomeFirstResponder];
            self.accessoFeeTF = accessoFeeTF;
            [self.scrollView addSubview:accessoFeeTF];
            
            UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, accesTlab.bottom, KWIDTH, 2)];
            line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line2];
            
            //质保期 标题
            UILabel *zbqTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, line2.bottom, 70, 48)];
            zbqTitLab.text = @"质保期";
            zbqTitLab.font = FontSize(16);
            zbqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:zbqTitLab];
            
            //质保期 文本框
            UILabel *zbqConLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, line2.bottom +8, 60, 32)];
            zbqConLab.font = FontSize(16);
            zbqConLab.textColor = [UIColor colorWithHexString:@"#333333"];
            zbqConLab.textAlignment = NSTextAlignmentLeft;
            [self.scrollView addSubview:zbqConLab];
            self.zbqConLab = zbqConLab;
            //添加点击事件
            UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelClick)];
            [zbqConLab addGestureRecognizer:labelTapGestureRecognizer];
            zbqConLab.userInteractionEnabled = YES;
            
            //输入框 下划线
            UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, zbqConLab.bottom, 60, 1)];
            bottomLine.backgroundColor = [UIColor colorWithHexString:@"#BBBBBB"];
            [self.scrollView addSubview:bottomLine];
            
            //质保期
            UILabel *monthLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqConLab.right, line2.bottom, 50, 48)];
            monthLab.text = @"个月";
            monthLab.font = FontSize(16);
            monthLab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:monthLab];
            
            UILabel *line5 = [[UILabel alloc] initWithFrame:CGRectMake(0, zbqTitLab.bottom, KWIDTH, 2)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            //请输入配件名称标题
            UILabel *AccNamelab = [[UILabel alloc] init];
            AccNamelab.frame = CGRectMake(left1,line5.bottom,KWIDTH-left1*2,48);
            AccNamelab.numberOfLines = 0;
            AccNamelab.text = @"请输入配件名称 (如果没有请填写无)";
            AccNamelab.font = FontSize(16);
            AccNamelab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccNamelab];
            
            //配件名称输入文本框
            UITextView *accessoryNameTV = [[UITextView alloc] init];
            accessoryNameTV.frame = CGRectMake(left1,AccNamelab.bottom,KWIDTH - 32,124);
            accessoryNameTV.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
            accessoryNameTV.layer.borderWidth = 1;
            accessoryNameTV.layer.cornerRadius = 8;
            accessoryNameTV.font = FontSize(14);
            //[accessoryNameTV becomeFirstResponder];
            [self.scrollView addSubview:accessoryNameTV];
            self.accessoryNameTV = accessoryNameTV;
            
            UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, accessoryNameTV.bottom+10, KWIDTH, 2)];
            line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line3];
            
            UILabel *starlab1 = [[UILabel alloc] init];
            starlab1.frame = CGRectMake(left1,line3.bottom,9,45);
            starlab1.numberOfLines = 0;
            starlab1.text = @"*";
            starlab1.font = FontSize(16);
            starlab1.textColor = [UIColor colorWithHexString:@"#FF000A"];
            [self.scrollView addSubview:starlab1];
            
            //请添加配件图片
            UILabel *AccImgLab = [[UILabel alloc] init];
            AccImgLab.frame = CGRectMake(starlab1.right + 1,line3.bottom,KWIDTH,45);
            AccImgLab.numberOfLines = 0;
            AccImgLab.text = @"请添加配件图片";
            AccImgLab.font = FontSize(16);
            AccImgLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccImgLab];
            
            self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, AccImgLab.bottom, KWIDTH, 100)];
            [self.scrollView addSubview: self.addView];
            CGFloat www = (KWIDTH - 62)/4;
            self.bottomf = AccImgLab.bottom;
            [self showinageWirhArr:_imageArr];
            
            CGRect ffff = self.addView.frame;
            ffff.size.height = www +10;
            self.addView.frame = ffff;
            
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom+10, KWIDTH, 2)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            //合计
            UILabel *totalLab = [[UILabel alloc] init];
            totalLab = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom + 27, KWIDTH, 18)];
            totalLab .font = FontSize(18);
            totalLab.textAlignment = NSTextAlignmentCenter;
            totalLab.text = [NSString stringWithFormat:@"合计: ¥0.00"];
            [self.scrollView addSubview: totalLab];
            self.totalLab = totalLab;
            
            UIButton *tuiusongBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
            [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
            [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
            tuiusongBut.frame  = CGRectMake(28, totalLab.bottom + 10, KWIDTH-28-28, 48);
            tuiusongBut.layer.masksToBounds = YES;
            tuiusongBut.layer.cornerRadius = 4;
            [self.scrollView addSubview: tuiusongBut ];
            
            [tuiusongBut addTarget:self action:@selector(pushBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        } else { //电器厂订单 维修 保外 生成账单
            //订单编号标题
            UILabel *orderTlab = [[UILabel alloc] init];
            orderTlab.frame = CGRectMake(left1,0,70,55);
            orderTlab.numberOfLines = 0;
            orderTlab.text = @"订单编号:";
            orderTlab.font = FontSize(16);
            orderTlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderTlab];
            //订单编号内容
            UILabel *orderDlab = [[UILabel alloc] init];
            orderDlab.frame = CGRectMake(orderTlab.right + 13,0,KWIDTH,55);
            orderDlab.numberOfLines = 0;
            orderDlab.text = _mymodel.Id;
            orderDlab.font = FontSize(16);
            orderDlab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:orderDlab];
            
            UILabel *line0 = [[UILabel alloc]initWithFrame:CGRectMake(0, orderTlab.bottom, KWIDTH, 10)];
            line0.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line0];
            
            //工时费标题
            UILabel *hourlyTlab = [[UILabel alloc] init];
            hourlyTlab.frame = CGRectMake(left1,line0.bottom,70,48);
            hourlyTlab.numberOfLines = 0;
            hourlyTlab.text = @"工时费:";
            hourlyTlab.font = FontSize(16);
            hourlyTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:hourlyTlab];
            
            //货币符号
            UILabel *fuhaoLab = [[UILabel alloc] init];
            fuhaoLab.frame = CGRectMake(hourlyTlab.right + 6, line0.bottom, 12, 48);
            fuhaoLab.text = @"¥";
            fuhaoLab.font = FontSize(16);
            fuhaoLab.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab];
            
            //工时费内容
            UITextField *hourlyWageTF = [[UITextField alloc] initWithFrame:CGRectMake(fuhaoLab.right + 6,line0.bottom,KWIDTH / 2,48)];
            hourlyWageTF.keyboardType = UIKeyboardTypeNumberPad;
            hourlyWageTF.placeholder = @"请输入工时费";
            hourlyWageTF.textColor = [UIColor blackColor];
            hourlyWageTF.font = FontSize(16);
            hourlyWageTF.textAlignment = NSTextAlignmentLeft;
            hourlyWageTF.delegate = self;
            //[hourlyWageTF becomeFirstResponder];
            //[hourlyWageTF addRules];
            self.hourlyWageTF = hourlyWageTF;
            [self.scrollView addSubview:hourlyWageTF];
            
            UILabel *line1 = [[UILabel alloc]initWithFrame:CGRectMake(0, hourlyTlab.bottom, KWIDTH, 2)];
            line1.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line1];
            
            //配件总费标题
            UILabel *accesTlab = [[UILabel alloc] init];
            accesTlab.frame = CGRectMake(left1,line1.bottom,70,48);
            accesTlab.numberOfLines = 0;
            accesTlab.text = @"配件总费:";
            accesTlab.font = FontSize(16);
            accesTlab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:accesTlab];
            
            //货币符号
            UILabel *fuhaoLab1 = [[UILabel alloc] init];
            fuhaoLab1.frame = CGRectMake(accesTlab.right + 6, line1.bottom, 12, 48);
            fuhaoLab1.text = @"¥";
            fuhaoLab1.font = FontSize(16);
            fuhaoLab1.textAlignment = NSTextAlignmentCenter;
            [self.scrollView addSubview:fuhaoLab1];
            
            //配件总费内容
            UITextField *accessoFeeTF = [[UITextField alloc] init];
            accessoFeeTF.frame = CGRectMake(fuhaoLab1.right + 6,line1.bottom,KWIDTH / 2,48);
            accessoFeeTF.keyboardType = UIKeyboardTypeNumberPad;
            accessoFeeTF.placeholder = @"请输入配件总费";
            accessoFeeTF.font = FontSize(16);
            accessoFeeTF.textAlignment = NSTextAlignmentLeft;
            accessoFeeTF.delegate = self;
            //[accessoFeeTF becomeFirstResponder];
            self.accessoFeeTF = accessoFeeTF;
            [self.scrollView addSubview:accessoFeeTF];
            
            UILabel *line2 = [[UILabel alloc]initWithFrame:CGRectMake(0, accesTlab.bottom, KWIDTH, 2)];
            line2.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line2];
            
            //质保期 标题
            UILabel *zbqTitLab = [[UILabel alloc] initWithFrame:CGRectMake(left1, line2.bottom, 70, 48)];
            zbqTitLab.text = @"质保期";
            zbqTitLab.font = FontSize(16);
            zbqTitLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:zbqTitLab];
            
            //质保期 文本框
            UILabel *zbqConLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, line2.bottom +8, 60, 32)];
            zbqConLab.font = FontSize(16);
            zbqConLab.textColor = [UIColor colorWithHexString:@"#333333"];
            zbqConLab.textAlignment = NSTextAlignmentLeft;
            [self.scrollView addSubview:zbqConLab];
            self.zbqConLab = zbqConLab;
            //添加点击事件
            UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelClick)];
            [zbqConLab addGestureRecognizer:labelTapGestureRecognizer];
            zbqConLab.userInteractionEnabled = YES;
            
            //输入框 下划线
            UILabel *bottomLine = [[UILabel alloc] initWithFrame:CGRectMake(zbqTitLab.right +10, zbqConLab.bottom, 60, 1)];
            bottomLine.backgroundColor = [UIColor colorWithHexString:@"#BBBBBB"];
            [self.scrollView addSubview:bottomLine];
            
            //质保期
            UILabel *monthLab = [[UILabel alloc] initWithFrame:CGRectMake(zbqConLab.right, line2.bottom, 50, 48)];
            monthLab.text = @"个月";
            monthLab.font = FontSize(16);
            monthLab.textColor = [UIColor colorWithHexString:@"#666666"];
            [self.scrollView addSubview:monthLab];
            
            UILabel *line5 = [[UILabel alloc] initWithFrame:CGRectMake(0, zbqTitLab.bottom, KWIDTH, 2)];
            line5.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line5];
            
            //请输入配件名称标题
            UILabel *AccNamelab = [[UILabel alloc] init];
            AccNamelab.frame = CGRectMake(left1,line5.bottom,KWIDTH-left1*2,48);
            AccNamelab.numberOfLines = 0;
            AccNamelab.text = @"请输入配件名称 (如果没有请填写无)";
            AccNamelab.font = FontSize(16);
            AccNamelab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccNamelab];
            
            //配件名称输入文本框
            UITextView *accessoryNameTV = [[UITextView alloc] init];
            accessoryNameTV.frame = CGRectMake(left1,AccNamelab.bottom,KWIDTH - 32,124);
            accessoryNameTV.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
            accessoryNameTV.layer.borderWidth = 1;
            accessoryNameTV.layer.cornerRadius = 8;
            accessoryNameTV.font = FontSize(14);
            //[accessoryNameTV becomeFirstResponder];
            [self.scrollView addSubview:accessoryNameTV];
            self.accessoryNameTV = accessoryNameTV;
            
            UILabel *line3 = [[UILabel alloc]initWithFrame:CGRectMake(0, accessoryNameTV.bottom+10, KWIDTH, 2)];
            line3.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line3];
            
            UILabel *starlab1 = [[UILabel alloc] init];
            starlab1.frame = CGRectMake(left1,line3.bottom,9,45);
            starlab1.numberOfLines = 0;
            starlab1.text = @"*";
            starlab1.font = FontSize(16);
            starlab1.textColor = [UIColor colorWithHexString:@"#FF000A"];
            [self.scrollView addSubview:starlab1];
            
            //请添加配件图片
            UILabel *AccImgLab = [[UILabel alloc] init];
            AccImgLab.frame = CGRectMake(starlab1.right + 1,line3.bottom,KWIDTH,45);
            AccImgLab.numberOfLines = 0;
            AccImgLab.text = @"请添加完工照片";
            AccImgLab.font = FontSize(16);
            AccImgLab.textColor = [UIColor colorWithHexString:@"#333333"];
            [self.scrollView addSubview:AccImgLab];
            
            self.addView = [[UIView alloc]initWithFrame:CGRectMake(0, AccImgLab.bottom, KWIDTH, 100)];
            [self.scrollView addSubview: self.addView];
            CGFloat www = (KWIDTH - 62)/4;
            self.bottomf = AccImgLab.bottom;
            [self showinageWirhArr:_imageArr];
            
            CGRect ffff = self.addView.frame;
            ffff.size.height = www +10;
            self.addView.frame = ffff;
            
            UILabel *line4 = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom+10, KWIDTH, 2)];
            line4.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
            [self.scrollView addSubview:line4];
            
            //合计
            UILabel *totalLab = [[UILabel alloc] init];
            totalLab = [[UILabel alloc]initWithFrame:CGRectMake(0, _addView.bottom + 27, KWIDTH, 18)];
            totalLab .font = FontSize(18);
            totalLab.textAlignment = NSTextAlignmentCenter;
            totalLab.text = [NSString stringWithFormat:@"合计: ¥0.00"];
            [self.scrollView addSubview: totalLab];
            self.totalLab = totalLab;
            
            UIButton *tuiusongBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
            [tuiusongBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            [tuiusongBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
            [tuiusongBut setTitle:@"推送账单" forState:(UIControlStateNormal)];
            tuiusongBut.frame  = CGRectMake(28, totalLab.bottom + 10, KWIDTH-28-28, 48);
            tuiusongBut.layer.masksToBounds = YES;
            tuiusongBut.layer.cornerRadius = 4;
            [self.scrollView addSubview: tuiusongBut ];
            
            [tuiusongBut addTarget:self action:@selector(pushBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        }
    }
}

-(void)labelClick {
    [self creatActionSheet];
}

-(void)creatActionSheet {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"选择月份" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"3" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"3";
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"6" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"6";
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"12" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.zbqConLab.text = @"12";
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"取消");
    }];
    //把action添加到actionSheet里
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [actionSheet addAction:action4];
    //相当于之前的[actionSheet show];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    self.placeLab.hidden = YES;
    //self.placeLab1.hidden = YES;
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length < 1) {
        self.placeLab.hidden = NO;
    }
    
//    if (textView.text.length < 1) {
//        self.placeLab1.hidden = NO;
//    }
}

- (void)textViewDidChange:(UITextView *)textView {
    KMyLog(@"%@", textView.text);
    
    if (self.accessoryNameTV == textView) {
        self.placeLab.hidden = YES;
        //实时显示字数
        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
        //字数限制操作
        if (textView.text.length >= 80) {
            textView.text = [textView.text substringToIndex:80];
            self.stirngLenghLabel.text = @"80/80";
        }
        //取消按钮点击权限，并显示提示文字
        if (textView.text.length == 0) {
            self.placeLab.hidden = NO;
        }
    }
    
//    if (self.jjfaTv == textView) {
//        self.placeLab1.hidden = YES;
//        //实时显示字数
//        self.stirngLenghLabel.text = [NSString stringWithFormat:@"%lu/60", (unsigned long)textView.text.length];
//        //字数限制操作
//        if (textView.text.length >= 60) {
//            textView.text = [textView.text substringToIndex:60];
//            self.stirngLenghLabel.text = @"60/60";
//        }
//        //取消按钮点击权限，并显示提示文字
//        if (textView.text.length == 0) {
//            self.placeLab1.hidden = NO;
//        }
//    }
}

#pragma mark - 推送账单
- (void)pushBtnAction:(id)sender {
    UIImageView *myyy = [self.imageArr  lastObject];
    if (myyy == self.myimage) {
        [self.imageArr removeLastObject];;
    }

    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if ([self.mymodel.user_type isEqualToString:@"0"]) {//个人
        if ([self.mymodel.fault_type isEqualToString:@"1"]) { //安装
            if (strIsEmpty(_hourlyWageTF.text)) {
                ShowToastWithText(@"请输入工时费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入材料总费");
                return;
            } else if (strIsEmpty(_accessoryNameTV.text)) {
                ShowToastWithText(@"请输入材料名称");
                return;
            } else if (_imageArr.count == 0) {
                ShowToastWithText(@"请添加材料图片");
                return;
            }
        } else if ([self.mymodel.fault_type isEqualToString:@"3"]) { //维修
            if (strIsEmpty(_hourlyWageTF.text)) {
                ShowToastWithText(@"请输入工时费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入配件总费");
                return;
            } else if (strIsEmpty(_zbqConLab.text)) {
                ShowToastWithText(@"请选择质保期");
                return;
            } else if (strIsEmpty(_accessoryNameTV.text)) {
                ShowToastWithText(@"请输入配件名称");
                return;
            } else if (_imageArr.count == 0) {
                ShowToastWithText(@"请添加配件图片");
                return;
            }
        }
        param[@"warranty"] = self.zbqConLab.text;
        
    } else if ([self.mymodel.user_type isEqualToString:@"2"]) { //电器厂订单 安装 生成账单
        if ([self.mymodel.fault_type isEqualToString:@"1"]) { //电器安装 生成账单
            if (strIsEmpty(_hourlyWageTF.text)) {
                ShowToastWithText(@"请输入工时费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入配件总费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入配件总费");
                return;
            } else if (strIsEmpty(_zbqConLab.text)) {
                ShowToastWithText(@"请选择质保期");
                return;
            } else if (_imageArr.count == 0) {
                ShowToastWithText(@"请添加配件图片");
                return;
            }
            param[@"warranty"] = self.zbqConLab.text;
        } else { //保外 生成账单
            if (strIsEmpty(_hourlyWageTF.text)) {
                ShowToastWithText(@"请输入工时费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入配件总费");
                return;
            } else if (strIsEmpty(_accessoFeeTF.text)) {
                ShowToastWithText(@"请输入配件总费");
                return;
            } else if (strIsEmpty(_zbqConLab.text)) {
                ShowToastWithText(@"请选择质保期");
                return;
            } else if (_imageArr.count == 0) {
                ShowToastWithText(@"请添加完工照片");
                return;
            }
            param[@"warranty"] = self.zbqConLab.text;
        }
        
    }
    
    if (_hourlyWageTF.text.floatValue + _accessoFeeTF.text.floatValue < 30) {
        ShowToastWithText(@"当前总费用合计小于30元");
        return;
    }
    param[@"peopleCost"] = _hourlyWageTF.text; //工时费
    param[@"orderId"] = self.mymodel.orderId;
    param[@"type"] = @"2";
    param[@"productCost"] = _accessoFeeTF.text; //配件总费
    param[@"prodcutComment"] = _accessoryNameTV.text; //配件名称
    
    NSString *json = [HFTools toJSONString:param];
    NSMutableDictionary *allDic = [NSMutableDictionary dictionaryWithCapacity:1];
    
    allDic[@"data"] = json;
    if (_imageArr.count > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [NetWorkTool UploadPicWithUrl:OrderCreateBill param:allDic key:@"images" image:_imageArr withSuccess:^(id dic) {
            KMyLog(@"____________%@",dic);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if ([self.mymodel.user_type isEqualToString:@"0"]) { //个人
                if ([self.mymodel.fault_type isEqualToString:@"1"]) { //安装
                    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecAnZhuang]; //个人订单弹出去处理弹窗
                } else if ([self.mymodel.fault_type isEqualToString:@"3"]) { //维修
                    [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecOne]; //个人订单弹出去处理弹窗
                }
            } else {//电器厂商
                [[UIApplication sharedApplication].keyWindow addSubview:self.bgViewsecOne]; //保外 //厂家订单弹出去处理弹窗
            }
        } failure:^(NSError *error) {
            [MBProgressHUD  hideHUDForView:self.view animated:YES];
        }];
        NSLog(@"%@", param);
        NSLog(@"%@", allDic);
    } else {
        ShowToastWithText(@"请添加材料图片");
        [self.imageArr addObject:self.myimage];
    }
}

/**
 弹出框的背景图 账单推送成功
 */
-(void)shwoPushOrderSuccessAlert {
    self.bgViewsecjiedan = [[UIView alloc]init];
    self.bgViewsecjiedan.frame = self.view.bounds;
    self.bgViewsecjiedan.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecjiedan addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
//    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 40, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
//    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 92, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    UILabel *topLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 110, KWIDTH-22*2, 63)];
    //[whiteBGView addSubview:topLable];
    topLable.font = FontSize(14);
    topLable.numberOfLines = 0;
    topLable.textColor = [UIColor colorWithHexString:@"#333333"];
    topLable.textAlignment = NSTextAlignmentCenter;
    topLable.text = @"如果您此次更换的有补贴工时费的候保配\n件，请点击“去安装”，没有更换候保配件请\n点击“结束”。";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 132, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"";
    
    UIButton *endButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [endButton setTitle:@"结束" forState:(UIControlStateNormal)];
    [endButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [endButton setBackgroundColor:[[UIColor colorWithHexString:@"#70BE68"] colorWithAlphaComponent:1]];
    endButton.layer.masksToBounds = YES;
    endButton.layer.cornerRadius = 4;
    [endButton addTarget:self action:@selector(endButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:endButton];
    [endButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        //make.left.offset(28);
        //make.right.equalTo(whiteBGView.mas_centerX).offset(-13);
        make.left.offset(58);
        make.right.offset(-58);
        make.bottom.offset(-18);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"去安装" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(toHandleAction) forControlEvents:(UIControlEventTouchUpInside)];
    //[whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(35);
        make.left.equalTo(whiteBGView.mas_centerX).offset(13);
        make.right.offset(-28);
        make.bottom.offset(-18);
    }];
}

- (void)endButtonAction {
    [_bgViewsecjiedan removeFromSuperview];
    
    [singlTool shareSingTool].needReasfh = YES;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)toHandleAction {
    [_bgViewsecjiedan removeFromSuperview];
    //去安装 添加配件
    ZYAddHoubaoAccessoriesViewController *houBaoVC = [[ZYAddHoubaoAccessoriesViewController alloc] init];
    houBaoVC.mymodel = self.mymodel;
    [self.navigationController pushViewController:houBaoVC animated:YES];
    KMyLog(@"个人维修 生成账单之后 点击去安装 跳转到 添加配件页面");
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    CGFloat ffftotle = 0;
    CGFloat motltlt = 0;
    
    self.totalLab.text = [NSString stringWithFormat:@"合计:¥%.2f",[self.hourlyWageTF.text floatValue]];
    motltlt = [self.hourlyWageTF.text floatValue];
    ffftotle = ffftotle + motltlt;
    ffftotle = ffftotle +[self.accessoFeeTF.text floatValue];
    self.totalLab.text = [NSString stringWithFormat:@"合计: ¥%.2f",ffftotle];
    return YES;
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//
//    if (textField == self.hourlyWageTF) {
//        NSArray *attt1 = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
//        if (![attt1 containsObject:string]  && ![string  isEqualToString:@""]) {
//            ShowToastWithText(@"只能输入数字");
//            return NO;
//        } else {
//            return YES;
//        }
//    }
//
//    if (textField == self.accessoFeeTF) {
//        NSArray *attt2 = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"."];
//        if (![attt2 containsObject:string]  && ![string  isEqualToString:@""]) {
//            ShowToastWithText(@"只能输入数字");
//            return NO;
//        } else {
//            return YES;
//        }
//    } else {
//        return YES;
//    }
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == self.hourlyWageTF) {
        if(string.length == 0)
            return YES;
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if(existedLength - selectedLength + replaceLength > 5) {
            return NO;
        }
    }
    
    if(textField == self.accessoFeeTF) {
        if(string.length == 0)
            return YES;
        NSInteger existedLength = textField.text.length;
        NSInteger selectedLength = range.length;
        NSInteger replaceLength = string.length;
        if(existedLength - selectedLength + replaceLength > 5) {
            return NO;
        }
    }
    return YES;
}

-(void)deldeImage:(UIButton *)but{
    //删除照片
    NSInteger ind = but.tag-98;
    [_imageArr[ind]  removeFromSuperview];
    [_imageArr removeObjectAtIndex:ind];
    
    [self showinageWirhArr:_imageArr];
}

-(void)addimageAction{
    //添加照片
    KMyLog(@"上传图片");
    /**
     *  弹出提示框
     */
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    //按钮：从相册选择，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //初始化UIImagePickerController
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式1：通过相册（呈现全部相册），UIImagePickerControllerSourceTypePhotoLibrary
        //获取方式2，通过相机，UIImagePickerControllerSourceTypeCamera
        //获取方法3，通过相册（呈现全部图片），UIImagePickerControllerSourceTypeSavedPhotosAlbum
        PickerImage.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        //允许编辑，即放大裁剪
        PickerImage.allowsEditing = YES;
        //自代理
        PickerImage.delegate = self;
        //页面跳转
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：拍照，类型：UIAlertActionStyleDefault
    [alert addAction:[UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        //iOS 判断应用是否有使用相机的权限
        NSString *mediaType = AVMediaTypeVideo;//读取媒体类型
        AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];//读取设备授权状态
        if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
            NSString *errorStr = @"应用相机权限受限,请在设置中启用";
            [self showOnleText:errorStr delay:1.5];
            return;
        }
        /**
         其实和从相册选择一样，只是获取方式不同，前面是通过相册，而现在，我们要通过相机的方式
         */
        UIImagePickerController *PickerImage = [[UIImagePickerController alloc]init];
        //获取方式:通过相机
        PickerImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        PickerImage.allowsEditing = YES;
        PickerImage.delegate = self;
        [self presentViewController:PickerImage animated:YES completion:nil];
    }]];
    //按钮：取消，类型：UIAlertActionStyleCancel
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

//PickerImage完成后的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //定义一个newPhoto，用来存放我们选择的图片。
    UIImage *newPhoto = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    UIImageView *image = [[UIImageView alloc]init];
    image.image = newPhoto;
    [self.imageArr insertObject:image atIndex:0];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self showinageWirhArr:_imageArr];
}

-(void)showinageWirhArr:(NSMutableArray *)muarr{
    [self.addView removeAllSubviews];
    //self.bottomf = 0;
    CGFloat www = (KWIDTH - 62)/4;
    for (int i = 0; i < _imageArr.count; i++) {
        [_imageArr[i] removeFromSuperview];
        UIImageView *image = _imageArr[i];
        [image removeFromSuperview];
        [image removeAllSubviews];
        image.frame = CGRectMake(16+(www+10)*i, self.bottomf+5, www, www);
        [self.scrollView addSubview:image];
        image.userInteractionEnabled = YES;
        
        UIButton *deleBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [deleBut setImage:imgname(@"组 3179") forState:(UIControlStateNormal)];
        [image addSubview:deleBut];
        deleBut.tag = 98+i;
        [deleBut mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.offset(30);
            make.top.offset(-10);
            make.right.offset(10);
        }];
        [deleBut addTarget:self action:@selector(deldeImage:) forControlEvents:(UIControlEventTouchUpInside)];
        
        if (i == (_imageArr.count - 1) ) {
            if (i == 4) {
                [image removeFromSuperview];
                SYBigImage * bigI = [[SYBigImage alloc]init];
                [image addGestureRecognizer:bigI];
            } else {
                deleBut.hidden = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addimageAction)];
                [image addGestureRecognizer:tap];
            }
        }else{
            SYBigImage * bigI = [[SYBigImage alloc]init];
            [image addGestureRecognizer:bigI];
        }
    }
}

#pragma mark - 浏览大图点击事件
-(void)scanBigImageClick1:(UITapGestureRecognizer *)tap{
    NSLog(@"点击图片");
    UIImageView *clickedImageView = (UIImageView *)tap.view;
    [XWScanImage scanBigImageWithImageView:clickedImageView];
}

/**
 弹出框的背景图 厂家订单 推送成功
 */
-(void)shwoBgviewsec{
    self.bgViewsecOne = [[UIView alloc]init];
    self.bgViewsecOne.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewsecOne.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecOne addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
//    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 40, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
//    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 92, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, UpLable.bottom+10, KWIDTH-22*2, 63)];
    //[whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"如果您此次更换的有补贴工时费的候保配\n件，请点击“去安装”，没有更换候保配件请\n点击“结束”。";
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.numberOfLines = 0;
    
    UIButton *leftBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [leftBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [leftBut setTitle:@"结束" forState:(UIControlStateNormal)];
    leftBut.layer.masksToBounds = YES;
    leftBut.layer.cornerRadius = 4;
    [leftBut addTarget:self action:@selector(leftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftBut];
    [leftBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        //make.left.offset(36);
        //make.width.offset(116);
        make.left.offset(58);
        make.right.offset(-58);
        make.bottom.offset(-18);
    }];
    
    UIButton *rightBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [rightBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [rightBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [rightBut setTitle:@"去安装" forState:(UIControlStateNormal)];
    rightBut.layer.masksToBounds = YES;
    rightBut.layer.cornerRadius = 4;
    [rightBut addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    //[whiteBGView addSubview:rightBut];
    [rightBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(34);
        make.right.offset(-36);
        make.width.offset(116);
        make.bottom.offset(-18);
    }];
}

-(void)leftBtnAction:(UIButton *)but{
    [_bgViewsecOne removeFromSuperview];
    
    [singlTool shareSingTool].needReasfh = YES;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)rightBtnAction:(UIButton *)but{
    [_bgViewsecOne removeFromSuperview];
    //跳转到 添加配件 页面
    ZYAddAccessoriesViewController *vc = [[ZYAddAccessoriesViewController alloc] init];
    vc.mymodel = self.mymodel;
    vc.orderId = self.mymodel.orderId;
    [self.navigationController pushViewController:vc animated:YES];
    KMyLog(@"生成账单之后跳转到 添加配件页面");
}

/**
 弹出框的背景图 个人安装 推送成功
 */
-(void)shwoBgviewsecAnzhuang {
    self.bgViewsecAnZhuang = [[UIView alloc]init];
    self.bgViewsecAnZhuang.frame = CGRectMake(0, 0, kScreen_Width, kScreen_Height);
    self.bgViewsecAnZhuang.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewsecAnZhuang addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment = NSTextAlignmentCenter;
    UpLable.text = @"账单推送成功";
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*为保障您的收益\n请您确认用户支付费用后离开" originalColor:K333333 originalFont:FontSize(16) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(16)];
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 42)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.numberOfLines = 0;
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.attributedText = attStr;
    DowLable.textAlignment = NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(ikenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-19);
    }];
}

-(void)ikenow:(UIButton *)but{
    [self.bgViewsecAnZhuang removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
    [singlTool shareSingTool].needReasfh = YES;
    //for (UIViewController *controller in self.navigationController.viewControllers) {
    //    if ([controller isKindOfClass:[CSOrderContentViewController class]]) {
    //        CSOrderContentViewController *vc = (CSOrderContentViewController *)controller;
    //        [self.navigationController popToViewController:vc animated:YES];
    //    }
    //}
    
    //if (self.myblock) {
    //    self.myblock(@"");
    //}
    //[self.navigationController popViewControllerAnimated:YES];
    
    self.navigationController.tabBarController.selectedIndex = 1;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight)];
        _scrollView.contentSize = CGSizeMake(KWIDTH, KHEIGHT +100);
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
        _scrollView.scrollEnabled = YES;
        adjustInset(_scrollView);
    }
    return _scrollView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
