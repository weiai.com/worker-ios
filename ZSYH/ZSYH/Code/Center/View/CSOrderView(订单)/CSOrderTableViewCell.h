//
//  CSOrderTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmaintenanceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSOrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *order_number;
@property (weak, nonatomic) IBOutlet UILabel *order_type;
@property (weak, nonatomic) IBOutlet UILabel *order_address;
@property (weak, nonatomic) IBOutlet UILabel *order_state;
@property(nonatomic,copy)void (^orderBlock)(NSString *ind);
@property (weak, nonatomic) IBOutlet UILabel *nameLb;

@property (weak, nonatomic) IBOutlet UILabel *phoneLB;
@property (weak, nonatomic) IBOutlet UILabel *guzhangType;
@property (weak, nonatomic) IBOutlet UIButton *makeOrderBut;

-(void)refasf:(CSmaintenanceListModel *)model;
@end

NS_ASSUME_NONNULL_END
