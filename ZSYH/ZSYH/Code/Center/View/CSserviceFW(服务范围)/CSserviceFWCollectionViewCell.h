//
//  CSserviceFWCollectionViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSserviceFWCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mimage;
@property (weak, nonatomic) IBOutlet UILabel *mylable;

@end

NS_ASSUME_NONNULL_END
