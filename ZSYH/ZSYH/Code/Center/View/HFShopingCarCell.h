//
//  HFShopingCarCell.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFShopCarGoodsModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HFShopingCarCell : UITableViewCell

@property (nonatomic, copy) void(^selectBlock)(HFShopCarGoodsModel *model);

@property (nonatomic, copy) void(^setCountBlock)(HFShopCarGoodsModel *model);

- (void)configDataWithModel:(HFShopCarGoodsModel *)model;


@end

NS_ASSUME_NONNULL_END
