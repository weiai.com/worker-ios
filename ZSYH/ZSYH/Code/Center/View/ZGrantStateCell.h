//
//  ZGrantStateCell.h
//  DistributorAB
//
//  Created by LZY on 2019/10/15.
//  Copyright © 2019 主事丫环. All rights reserved.
//

// 补助金申请状态cell

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZGrantStateCell : UITableViewCell
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *reason;

+ (CGFloat)cellHeightNormal;//正常
+ (CGFloat)cellHeightRefuse;//拒绝
@end

NS_ASSUME_NONNULL_END
