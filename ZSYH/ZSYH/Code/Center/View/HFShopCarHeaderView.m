//
//  HFShopCarHeaderView.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopCarHeaderView.h"

@interface HFShopCarHeaderView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;
@property (nonatomic, strong) HFShopCarStoreModel *model;

@end

@implementation HFShopCarHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"HFShopCarHeaderView" owner:self options:nil] lastObject];
        self.frame = frame;
        
        UITapGestureRecognizer *tapges = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shopName)];
        self.userInteractionEnabled = YES;
        self.titleLabel.userInteractionEnabled = YES;
        [self.titleLabel addGestureRecognizer:tapges];
    }
    return self;
}

-(void)shopName{
    if (self.myblock) {
        self.myblock(1);
    }
}

- (IBAction)selectBtnClick:(UIButton *)sender {
    if (self.selectSectionAllBlock) {
        _selectBtn.selected = !_selectBtn.selected;
        self.model.isSelect = _selectBtn.selected;
        self.selectSectionAllBlock(self.model);
    }
}

- (void)configDataWithModel:(HFShopCarStoreModel *)model {
    self.model = model;
    self.titleLabel.text = [NSString stringWithFormat:@"%@ >", model.shop_name];
    _selectBtn.selected = model.isSelect;
}

- (void)changeSelctBtnStatus:(BOOL)select {
    _selectBtn.selected = select;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
