//
//  CSBankCDTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSBankCDTableViewCell.h"

@implementation CSBankCDTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)refash:(CSCbankCardListModel *)molel{
    
    if ([molel.logo_url hasPrefix:@"http"]) {
        [self.cardImage sd_setImageWithURL:[NSURL URLWithString:molel.logo_url] placeholderImage:defaultImg];
        
        
    }else{
        sdimg(self.cardImage, molel.logo_url);
        
    }
  
    self.cardNmme.text = [NSString stringWithFormat:@"%@",molel.type_name];
    if ([molel.band_card_type integerValue]  == 1) {
        self.cardtype.text = [NSString stringWithFormat:@"储蓄卡"];

    }else{
        self.cardtype.text = [NSString stringWithFormat:@"信用卡"];

    }
//    self.cardNumber.text = [NSString stringWithFormat:@"%@",molel.bank_card];
    self.cardNumber.text = [NSString stringWithFormat:@"****  ****  ****  %@",[molel.Id substringFromIndex:molel.Id.length-4]];
    
    

    
}

@end
