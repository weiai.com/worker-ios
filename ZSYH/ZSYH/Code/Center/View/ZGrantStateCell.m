//
//  ZGrantStateCell.m
//  DistributorAB
//
//  Created by LZY on 2019/10/15.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "ZGrantStateCell.h"
@interface ZGrantStateCell ()
@property (nonatomic, strong) UIView *viewLineOne;
@property (nonatomic, strong) UIView *viewLineTwo;
@property (nonatomic, strong) UIView *viewLineThree;
@property (nonatomic, strong) UIView *viewLineFour;
@property (nonatomic, strong) UIView *viewLineFive;

@property (nonatomic, strong) UIImageView *ivOne;;
@property (nonatomic, strong) UIImageView *ivTwo;
@property (nonatomic, strong) UIImageView *ivThree;
@property (nonatomic, strong) UIImageView *ivFour;

@property (nonatomic, strong) UILabel *lblOne;//申请已提交
@property (nonatomic, strong) UILabel *lblTwo;//省级审核 省级审核通过/省级审核未通过
@property (nonatomic, strong) UILabel *lblThree;//平台审核
@property (nonatomic, strong) UILabel *lblFour;//补助金到账

@property (nonatomic, strong) UIView *viewRefuse;//拒绝背景view
@property (nonatomic, strong) UITextView *tvRefuse;//拒绝原因
@property (nonatomic, strong) UIButton *btnTry;//重新申请
@end

@implementation ZGrantStateCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupViewAction];
    }
    return self;
}
#pragma mark - 界面布局
- (void)setupViewAction{
    
    CGFloat lineWidth = kScreen_Width / 5.0;
    
    _viewLineOne = [[UIView alloc] initWithFrame:CGRectMake(0, 55, lineWidth, 3)];
    _viewLineOne.backgroundColor = kColorWithHex(0x70be68);
    [self.contentView addSubview:_viewLineOne];
    
    _viewLineTwo = [[UIView alloc] initWithFrame:CGRectMake(lineWidth, 55, lineWidth, 3)];
    _viewLineTwo.backgroundColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_viewLineTwo];
    
    _viewLineThree = [[UIView alloc] initWithFrame:CGRectMake(2 * lineWidth, 55, lineWidth, 3)];
    _viewLineThree.backgroundColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_viewLineThree];
    
    _viewLineFour = [[UIView alloc] initWithFrame:CGRectMake(3 * lineWidth, 55, lineWidth, 3)];
    _viewLineFour.backgroundColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_viewLineFour];
    
    _viewLineFive = [[UIView alloc] initWithFrame:CGRectMake(4 * lineWidth, 55, lineWidth, 3)];
    _viewLineFive.backgroundColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_viewLineFive];
    
    _ivOne = [UIImageView new];
    _ivOne.image = imgname(@"grant_state_agree_icon");
    [self.contentView addSubview:_ivOne];
    [_ivOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewLineOne.mas_centerY);
        make.centerX.mas_equalTo(self.viewLineOne.mas_trailing);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    _ivTwo = [UIImageView new];
    _ivTwo.image = imgname(@"grant_state_normal_icon");
    [self.contentView addSubview:_ivTwo];
    [_ivTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewLineOne.mas_centerY);
        make.centerX.mas_equalTo(self.viewLineTwo.mas_trailing);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    _ivThree = [UIImageView new];
    _ivThree.image = imgname(@"grant_state_normal_icon");
    [self.contentView addSubview:_ivThree];
    [_ivThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewLineOne.mas_centerY);
        make.centerX.mas_equalTo(self.viewLineThree.mas_trailing);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    _ivFour = [UIImageView new];
    _ivFour.image = imgname(@"grant_state_normal_icon");
    [self.contentView addSubview:_ivFour];
    [_ivFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.viewLineOne.mas_centerY);
        make.centerX.mas_equalTo(self.viewLineFour.mas_trailing);
        make.size.mas_equalTo(CGSizeMake(22, 22));
    }];
    
    _lblOne = [UILabel new];
    _lblOne.text = @"申请已提交";
    _lblOne.font = [UIFont systemFontOfSize:12];
    _lblOne.textColor = kColorWithHex(0x70be68);
    [self.contentView addSubview:_lblOne];
    [_lblOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivOne.mas_centerX);
        make.bottom.equalTo(self.ivOne.mas_top).offset(-4);
    }];
    
    _lblTwo = [UILabel new];
    _lblTwo.text = @"省级审核";
    _lblTwo.font = [UIFont systemFontOfSize:12];
    _lblTwo.textColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_lblTwo];
    [_lblTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivTwo.mas_centerX);
        make.bottom.equalTo(self.ivTwo.mas_top).offset(-4);
    }];
    
    _lblThree = [UILabel new];
    _lblThree.text = @"平台审核";
    _lblThree.font = [UIFont systemFontOfSize:12];
    _lblThree.textColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_lblThree];
    [_lblThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivThree.mas_centerX);
        make.bottom.equalTo(self.ivThree.mas_top).offset(-4);
    }];
    
    _lblFour = [UILabel new];
    _lblFour.text = @"补助金到账";
    _lblFour.font = [UIFont systemFontOfSize:12];
    _lblFour.textColor = kColorWithHex(0xcfcccf);
    [self.contentView addSubview:_lblFour];
    [_lblFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.ivFour.mas_centerX);
        make.bottom.equalTo(self.ivFour.mas_top).offset(-4);
    }];
    
    _viewRefuse = [UIView new];
    _viewRefuse.layer.borderColor = kColorWithHex(0xdfdfdf).CGColor;
    _viewRefuse.layer.borderWidth = 1;
    _viewRefuse.layer.cornerRadius = 6;
    _viewRefuse.layer.masksToBounds = YES;
    _viewRefuse.hidden = YES;
    [self.contentView addSubview:_viewRefuse];
    [_viewRefuse mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.viewLineOne.mas_bottom).offset(25);
        make.leading.equalTo(self.contentView.mas_leading).offset(20);
        make.trailing.equalTo(self.contentView.mas_trailing).offset(-20);
        make.height.mas_equalTo(64);
    }];
    _tvRefuse = [UITextView new];
    _tvRefuse.editable = NO;
    _tvRefuse.font = [UIFont systemFontOfSize:14];
    _tvRefuse.textColor = kColorWithHex(0x666666);
    [_viewRefuse addSubview:_tvRefuse];
    [_tvRefuse mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.viewRefuse);
        make.leading.equalTo(self.viewRefuse.mas_leading).offset(20);
        make.trailing.equalTo(self.viewRefuse.mas_trailing).offset(-20);
    }];
        
    //审核未通过
    UIView *viewLine = [UIView new];
    viewLine.backgroundColor = kColorWithHex(0xf2f2f2);
    [self.contentView addSubview:viewLine];
    [viewLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_equalTo(2);
    }];
}
#pragma mark - 数据更新
-(void)setState:(NSString *)state
{
    if (state) {
        _state= state;
        NSInteger stateInteger = [_state integerValue];
        if (stateInteger == 1) {
            //未审核 已提交
            [_ivOne mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            [_lblOne mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivOne.mas_top).offset(-7);
            }];
            _lblOne.font = [UIFont systemFontOfSize:14];
        }else if (stateInteger == 2){
            //省级未通过
            _viewLineTwo.backgroundColor = kColorWithHex(0xd84b4a);
            
            [_ivTwo mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            _ivTwo.image = imgname(@"grant_state_refuse_icon");
            
            [_lblTwo mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivTwo.mas_top).offset(-7);
            }];
            _lblTwo.font = [UIFont systemFontOfSize:14];
            _lblTwo.text = @"省级审核未通过";
            _lblTwo.textColor = kColorWithHex(0xd84b4a);
            
            [self refuseView];
            
        }else if (stateInteger == 3){
            //省级通过
            _viewLineTwo.backgroundColor = kColorWithHex(0x70be68);
            
            [_ivTwo mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            _ivTwo.image = imgname(@"grant_state_agree_icon");
            
            [_lblTwo mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivTwo.mas_top).offset(-7);
            }];
            _lblTwo.font = [UIFont systemFontOfSize:14];
            _lblTwo.text = @"省级审核通过";
            _lblTwo.textColor = kColorWithHex(0x70be68);
        }else if (stateInteger == 4){
            //平台未通过
            _viewLineTwo.backgroundColor = kColorWithHex(0x70be68);
            _lblTwo.text = @"省级审核通过";
            _lblTwo.textColor = kColorWithHex(0x70be68);
            _ivTwo.image = imgname(@"grant_state_agree_icon");
            
            _viewLineThree.backgroundColor = kColorWithHex(0xd84b4a);
            
            [_ivThree mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            _ivThree.image = imgname(@"grant_state_refuse_icon");
            
            [_lblThree mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivTwo.mas_top).offset(-7);
            }];
            _lblThree.font = [UIFont systemFontOfSize:14];
            _lblThree.text = @"平台审核未通过";
            _lblThree.textColor = kColorWithHex(0xd84b4a);
            
            [self refuseView];
            
        }else if (stateInteger == 5){
            //平台通过
            _viewLineTwo.backgroundColor = kColorWithHex(0x70be68);
            _lblTwo.text = @"省级审核通过";
            _lblTwo.textColor = kColorWithHex(0x70be68);
            _ivTwo.image = imgname(@"grant_state_agree_icon");
            
            _viewLineThree.backgroundColor = kColorWithHex(0x70be68);
            
            [_ivThree mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            _ivThree.image = imgname(@"grant_state_agree_icon");
            
            [_lblThree mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivTwo.mas_top).offset(-7);
            }];
            _lblThree.font = [UIFont systemFontOfSize:14];
            _lblThree.text = @"平台审核通过";
            _lblThree.textColor = kColorWithHex(0x70be68);
        }else if (stateInteger == 6){
            //补助金到账
            _viewLineTwo.backgroundColor = kColorWithHex(0x70be68);
            _lblTwo.text = @"省级审核通过";
            _lblTwo.textColor = kColorWithHex(0x70be68);
            _ivTwo.image = imgname(@"grant_state_agree_icon");
            
            _viewLineThree.backgroundColor = kColorWithHex(0x70be68);
            _lblThree.text = @"平台审核通过";
            _lblThree.textColor = kColorWithHex(0x70be68);
            _ivThree.image = imgname(@"grant_state_agree_icon");
            
            _viewLineFour.backgroundColor = kColorWithHex(0x70be68);
            [_ivFour mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(30, 30));
            }];
            _ivFour.image = imgname(@"grant_state_agree_icon");
            
            [_lblFour mas_updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.ivTwo.mas_top).offset(-7);
            }];
            _lblFour.font = [UIFont systemFontOfSize:14];
            _lblFour.text = @"补助金到账";
            _lblFour.textColor = kColorWithHex(0x70be68);
            
            _viewLineFive.backgroundColor = kColorWithHex(0x70be68);
        }
    }
}
-(void)setReason:(NSString *)reason
{
    if (reason) {
        _reason = reason;
    }
}
//审核不通过处理
- (void)refuseView{
    _viewRefuse.hidden = NO;
    [_viewRefuse mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(92);
    }];
    _tvRefuse.text = self.reason.length > 0 ? self.reason : @"审核未通过";
}
+ (CGFloat)cellHeightNormal{
    return 116.0;
}
+ (CGFloat)cellHeightRefuse{
    return 190.0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
