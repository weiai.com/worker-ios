//
//  JSHostoryTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JSHostoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLB;

@property (weak, nonatomic) IBOutlet UILabel *stateLB;
@property (weak, nonatomic) IBOutlet UILabel *moneyLB;

@property (weak, nonatomic) IBOutlet UILabel *shenqing;
@property (weak, nonatomic) IBOutlet UILabel *shenqingLB;

@property (weak, nonatomic) IBOutlet UILabel *daozhang;
@property (weak, nonatomic) IBOutlet UILabel *daozhnagLB;
@end

NS_ASSUME_NONNULL_END
