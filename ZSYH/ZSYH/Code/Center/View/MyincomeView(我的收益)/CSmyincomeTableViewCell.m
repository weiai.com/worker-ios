//
//  CSmyincomeTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSmyincomeTableViewCell.h"

@implementation CSmyincomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.orderLb.font = KFontPingFangSCMedium(14);
    self.orderLb.textColor = K666666;
    
    self.timeLB.font = KFontPingFangSCMedium(11);
    self.timeLB.textColor = K999999;
    
    self.moneyLb.font = KFontPingFangSCMedium(20);

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
