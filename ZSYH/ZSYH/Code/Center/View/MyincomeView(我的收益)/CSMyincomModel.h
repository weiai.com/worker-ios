//
//  CSMyincomModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMyincomModel : BaseModel
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *invoice_status;
@property (nonatomic, copy) NSString *charge;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *app_order_id;
@property (nonatomic, copy) NSString *pay_type;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *order_state;
@property (nonatomic, copy) NSString *all_cost;
@property (nonatomic, copy) NSString *peopleCost;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *allMoney;
@property (nonatomic, copy) NSString *earning;
@property (nonatomic, assign) CGFloat app_set_count;

@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *set_policy_id;

@property (nonatomic, copy) NSString *type;



@end

NS_ASSUME_NONNULL_END
