
//
//  JSHostoryTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "JSHostoryTableViewCell.h"

@implementation JSHostoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLB.font = KFontPingFangSCMedium(14);
    self.stateLB.font = KFontPingFangSCMedium(12);

    self.shenqing.font = KFontPingFangSCMedium(11);
    self.shenqingLB.font = KFontPingFangSCMedium(11);
    self.daozhang.font = KFontPingFangSCMedium(11);
    self.daozhnagLB.font = KFontPingFangSCMedium(11);

    self.moneyLB.font = KFontPingFangSCMedium(20);

    /*
     @property (weak, nonatomic) IBOutlet UILabel *titleLB;
     @property (weak, nonatomic) IBOutlet UILabel *daozhang;
     @property (weak, nonatomic) IBOutlet UILabel *shenqingLB;
     @property (weak, nonatomic) IBOutlet UILabel *daozhnagLB;
     @property (weak, nonatomic) IBOutlet UILabel *stateLB;
     @property (weak, nonatomic) IBOutlet UILabel *moneyLB;
     */
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
