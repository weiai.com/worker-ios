//
//  CSGanderdrHomeTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSGanderdrHomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLB;

@end

NS_ASSUME_NONNULL_END
