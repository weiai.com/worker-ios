//
//  HFShopCarBottomView.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopCarBottomView.h"

@interface HFShopCarBottomView ()
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *settleBtn;
@property (weak, nonatomic) IBOutlet UIButton *selectAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *buhanyunfeiLB;

@end

@implementation HFShopCarBottomView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"HFShopCarBottomView" owner:self options:nil] lastObject];
        self.frame = frame;
        _deleteBtn.hidden = YES;
    }
    return self;
}

- (IBAction)selectBtnClick:(UIButton *)sender {
    NSLog(@"全选");
    if (self.selectAllBlock) {
        sender.selected = !sender.selected;
        self.selectAllBlock(sender.selected);
    }
}

- (IBAction)settleBtnClick:(UIButton *)sender {
    NSLog(@"结算");
    if (self.deleteOrSettleBtnClock) {
        self.deleteOrSettleBtnClock(0);
    }
}

- (IBAction)deleteBtnClick:(UIButton *)sender {
    NSLog(@"删除");
    if (self.deleteOrSettleBtnClock) {
        self.deleteOrSettleBtnClock(1);
    }
}

- (void)changeBottomViewTotalPrice:(NSString *)totalPrice goodsCount:(NSString *)goodsCount {
    _priceLabel.text = [NSString stringWithFormat:@"%@", totalPrice];
    NSString *title = [NSString stringWithFormat:@"结算 (%@)", goodsCount];//s需要显示数量的时候打开
    [_settleBtn setTitle:title forState:(UIControlStateNormal)];//s需要显示数量的时候打开
}

- (void)changeSelectBtnStatus:(BOOL)select {
    _selectAllBtn.selected = select;
}

- (void)changeDeleteStatus:(BOOL)isDelete {
    if (isDelete) {
        _deleteBtn.hidden = NO;
        _textLabel.hidden = YES;
        _buhanyunfeiLB.hidden = YES;

        _priceLabel.hidden = YES;
    } else {
        _deleteBtn.hidden = YES;
        _textLabel.hidden = NO;
        _buhanyunfeiLB.hidden = NO;

        _priceLabel.hidden = NO;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
