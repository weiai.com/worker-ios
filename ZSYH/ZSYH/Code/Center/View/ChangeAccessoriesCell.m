
//
//  ChangeAccessoriesCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/12.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "ChangeAccessoriesCell.h"
@interface ChangeAccessoriesCell ()<UITextFieldDelegate>
@end
@implementation ChangeAccessoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.oldBut setTitleColor:K333333 forState:(UIControlStateNormal)];
    [self.nebut setTitleColor:K333333 forState:(UIControlStateNormal)];
    
    self.old_nameTF.delegate =self;
    self.ne_nameTF.delegate =self;
    self.PeiJianFeiTF.delegate =self;
    [self.PeiJianFeiTF addRules];
    // Initialization code
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == _PeiJianFeiTF) {
        self.myblock(10,textField.text);//配件费

    }else if (textField == self.old_nameTF) {
        
        if (self.myblock) {
            self.myblock(2,textField.text);//上边的输入名称
        }
    }else{
        
        if (self.myblock) {
            self.myblock(5,textField.text);//下边的输入名称
        }
    }
    
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == _PeiJianFeiTF) {
        self.myblock(10,textField.text);//配件费
        
    }else if (textField == self.old_nameTF) {
        
        if (self.myblock) {
            self.myblock(2,textField.text);//上边的输入名称
        }
    }else{
        
        if (self.myblock) {
            self.myblock(5,textField.text);//下边的输入名称
        }
    }
    return YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)upSaoyisao:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(0,@"");//上边的扫一扫
    }
}

- (IBAction)upSeleProducttype:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(1,@"");//上边的选择品类
    }
}

- (IBAction)dowSaoyisao:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(3,@"");//下边的扫一扫
    }
}

- (IBAction)dowProduct:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(4,@"");//下边的选择品类
    }
}

- (IBAction)deleAction:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(6,@"");//删除
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.PeiJianFeiTF) {
        
        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
            ShowToastWithText(@"只能输入数字");
            return NO;
            
        }else{
            return YES;
            
        }
    }else{
        return YES;
    }
}

@end
