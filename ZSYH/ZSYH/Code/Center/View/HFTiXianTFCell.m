//
//  HFTiXianTFCell.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFTiXianTFCell.h"
//#import "WqGaoDeLocationViewController.h"

#import "BaseNavViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
@interface HFTiXianTFCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation HFTiXianTFCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _contentTF.delegate = self;
    [_contentTF addTarget:self action:@selector(inputContentClick:) forControlEvents:(UIControlEventEditingDidEnd)];
    
    [_contentTF addTarget:self action:@selector(inputContentClending:) forControlEvents:(UIControlEventEditingDidBegin)];
    
    UIButton *bug = [UIButton buttonWithType:(UIButtonTypeCustom)];
    self.selebut = bug;
    bug.frame = self.contentTF.frame;
    bug.width = KWIDTH;
    [self.contentView addSubview:bug];
    [bug addTarget:self action:@selector(dsffdsfdsfsd) forControlEvents:(UIControlEventTouchUpInside)];
    self.selebut.hidden = YES;
}

-(void)dsffdsfdsfsd{
    if (self.inputContentBlock) {
        self.inputContentBlock(@"");
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isFirstResponder]) {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage]) {
            return NO;
        }
//        if ([self hasEmoji:string] || [self stringContainsEmoji:string]){
//            return NO;
//        }
    }
    return YES;
}

#pragma mark - 判断字符串中是否有表情
- (BOOL)stringContainsEmoji:(NSString *)string {
    
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    return returnValue;
}

#pragma mark - 判断是否是九宫格键盘
-(BOOL)isNineKeyBoard:(NSString *)string{
    NSString *other = @"➋➌➍➎➏➐➑➒";
    int len = (int)string.length;
    for(int i=0;i<len;i++)
    {
        if(!([other rangeOfString:string].location != NSNotFound))
            return NO;
    }
    return YES;
}

#pragma mark - 判断是否含有表情(第三方键盘)
- (BOOL)hasEmoji:(NSString*)string{
    NSString *pattern = @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:string];
    return isMatch;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)inputContentClending:(UITextField *)sender {
    if( sender.tag ==777 ){
        if (self.inputContentBlock) {
            self.inputContentBlock(@"");
        }
    }
}

- (void)inputContentClick:(UITextField *)sender {
    if (self.inputContentBlock) {
        self.inputContentBlock(sender.text);
    }
    [sender resignFirstResponder];
}

- (void)configDataWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder content:(NSString *)content {
    _titleLabel.text = title;
    _contentTF.placeholder = placeHolder;
  
    if (!strIsEmpty(content)) {
        _contentTF.text = content;
    }
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField:(UITextField *)textField{
//    NSLog(@"dsfsf%@",_titleLabel.text);
//    if( [_titleLabel.text isEqualToString:@"所在地区"]){
//        if (self.inputContentBlock) {
//            self.inputContentBlock(@"");
//        }
//        return NO;
//        }
//    return YES;
//}

#pragma mark -- 定位服务
-(void)locationAction{
    /*
    KMyLog(@"定位");
    kWeakSelf;
    if ([self isLocationServiceOpen]) {
        WqGaoDeLocationViewController *locaVC = [[WqGaoDeLocationViewController alloc]init];
        BaseNavViewController * navigation = [[BaseNavViewController alloc]initWithRootViewController:locaVC];
        [[UINavigationController new] presentViewController:navigation animated:YES completion:nil];
        locaVC.locaBlock = ^(NSMutableDictionary *locadic) {
            NSLog(@"%@", locadic);
            NSString *info = [NSString stringWithFormat:@"{\"text\":\"%@\",\"location\":\"%@,%@\"}", locadic[@"building_name"], locadic[@"longitude"], locadic[@"latitude"]];
            NSString *callBack = [NSString stringWithFormat:@"getLocationCallBack('%@')", info];
            //[weakSelf.webView evaluateJavaScript:callBack completionHandler:nil];
        };
    }else{
        //未开启定位权限
        UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"需要您开启定位权限,是否允许" preferredStyle:UIAlertControllerStyleAlert];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"取消");
        }]];
        [alertCon addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            KMyLog(@"确定");
            if ([[[UIDevice currentDevice] systemVersion] integerValue] >9.9) {
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            } else {
                
            }
        }]];
    }
    */
}

//是否开启定位
- (BOOL)isLocationServiceOpen {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        return NO;
    } else
        return YES;
}

@end
