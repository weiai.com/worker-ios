//
//  HFShopCarBottomView.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFShopCarBottomView : UIView

- (void)changeBottomViewTotalPrice:(NSString *)totalPrice goodsCount:(NSString *)goodsCount;

- (void)changeSelectBtnStatus:(BOOL)select;

@property (nonatomic, copy) void(^selectAllBlock)(BOOL select);

- (void)changeDeleteStatus:(BOOL)isDelete;

@property (nonatomic, copy) void(^deleteOrSettleBtnClock)(NSInteger index);

@end

NS_ASSUME_NONNULL_END
