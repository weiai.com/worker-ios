//
//  HFTiXianTFCell.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFTiXianTFCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *contentTF;
@property (nonatomic, strong) void(^inputContentBlock)(NSString *);
@property(nonatomic,strong)UIButton *selebut;
- (void)configDataWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder content:(NSString *)content;
@property (weak, nonatomic) IBOutlet UIImageView *jinruimage;

@end

NS_ASSUME_NONNULL_END
