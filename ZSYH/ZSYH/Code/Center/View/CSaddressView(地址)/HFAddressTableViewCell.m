//
//  HFAddressTableViewCell.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFAddressTableViewCell.h"

@interface HFAddressTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
@property (weak, nonatomic) IBOutlet UIButton *editbut;
@property (weak, nonatomic) IBOutlet UIButton *delebut;

@end

@implementation HFAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.editbut.layer.borderWidth = 1;
    self.editbut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    self.editbut.layer.masksToBounds = YES;
    self.editbut.layer.cornerRadius = 4;
    
    self.delebut.layer.borderWidth = 1;
    self.delebut.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    self.delebut.layer.masksToBounds = YES;
    self.delebut.layer.cornerRadius = 4;
    
    _addressLabel.text = @"河南省金水区东风路街道正弘蓝堡湾世玺中心1712河南省金水区东风路街道正弘蓝堡湾世玺中心1712";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (IBAction)setAddressClick:(UIButton *)sender {
    if (_model.is_default) {
        return;
    }
    kWeakSelf;
    NSDictionary *dic = @{
                          @"area":NOTNIL(_model.area) ,
                          @"address":NOTNIL(_model.address),
                          @"province":NOTNIL(_model.province),
                          @"city":NOTNIL(_model.city),
                          @"is_default":@"1",
                          @"user_name":NOTNIL(_model.user_name),
                          @"phone":NOTNIL(_model.phone),
                          @"id":NOTNIL(_model.id)
                          
                          };
    NSString *json = [HFTools toJSONString:dic];
    
    NSDictionary *dicccc = @{@"address":json};
    [NetWorkTool POST:cmyeditaddress param:[dicccc mutableCopy] success:^(id dic) {
        sender.selected = YES;
        if (weakSelf.editOrDelBlock) {
            weakSelf.editOrDelBlock(2);
        }
        ShowToastWithText(@"已设为默认地址");
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}


- (IBAction)editAddressClick:(UIButton *)sender {
    NSLog(@"编辑");
    if (self.editOrDelBlock) {
        self.editOrDelBlock(0);
    }
}

- (IBAction)deleteAddressClick:(UIButton *)sender {
    NSLog(@"删除");
    
    kWeakSelf;
    if (weakSelf.editOrDelBlock) {
        weakSelf.editOrDelBlock(1);
    }
}

- (void)setModel:(HFAddressModel *)model {
    _model = model;
    _addressLabel.text = [NSString stringWithFormat:@"%@%@", model.addressName, model.address];
    _nameLabel.text = [NSString stringWithFormat:@"%@", model.user_name];
    _mobileLabel.text = [NSString stringWithFormat:@"%@", model.phone];
    if (model.is_default  == 1) {
        _defaultBtn.selected = YES;
    } else {
        _defaultBtn.selected = NO;
    }
}

@end
