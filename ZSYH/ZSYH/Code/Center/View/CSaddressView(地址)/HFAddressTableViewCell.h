//
//  HFAddressTableViewCell.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/10.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFAddressModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface HFAddressTableViewCell : UITableViewCell

@property (nonatomic, strong) HFAddressModel *model;

@property (nonatomic, strong) void(^editOrDelBlock)(NSInteger index);


@end

NS_ASSUME_NONNULL_END
