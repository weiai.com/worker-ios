//
//  CSCommitListTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StarEvaluator.h"
#import "CSCimmentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSCommitListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *user_pic;
@property (weak, nonatomic) IBOutlet UILabel *user_name;
@property (weak, nonatomic) IBOutlet UILabel *bgLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *contentLB;
@property (nonatomic,strong)StarEvaluator *star;
-(void)refash:(CSCimmentModel *)model;

@end

NS_ASSUME_NONNULL_END
