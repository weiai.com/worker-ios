//
//  CSCommitListTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSCommitListTableViewCell.h"

@implementation CSCommitListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    self.bgLable.backgroundColor = [UIColor whiteColor];
    // Initialization code
}
-(StarEvaluator *)star{
    if (!_star) {
        self.star = [[StarEvaluator alloc] init];
        [self addSubview:self.star];
        self.star.frame = self.bgLable.frame;
        self.star.isShow = YES;
        self.star.BGViewWidth = 90;
        self.star.widthDistence = 5;
    }
    return _star;
    
}
-(void)refash:(CSCimmentModel *)model{
    self.star.gardernumber = [model.score integerValue];
    [self.star loadSubviews];
    
//    *user_pic;
//    @property (weak, nonatomic) IBOutlet UILabel *user_name;
//    @property (weak, nonatomic) IBOutlet UILabel *bgLable;
//    @property (weak, nonatomic) IBOutlet UILabel *timeLB;
//    @property (weak, nonatomic) IBOutlet UILabel *contentLB;
    
    
//    if ([model.productPic hasPrefix:@"http"]) {
//        [self.goodsImage sd_setImageWithURL:[NSURL URLWithString:model.productPic] placeholderImage:defaultImg];
//    }else{
//        sdimg(self.goodsImage, model.productPic);
//
//    }
    
    self.user_name.text = model.content;
    self.timeLB.text = model.content;
    self.contentLB.text = model.content;


    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
