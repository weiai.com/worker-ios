//
//  CSmakeAccontListcell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSmakeAccontListcell : UITableViewCell

@property (strong, nonatomic) CSmakeAccAddmodel *myModel;

@property (weak, nonatomic) IBOutlet UILabel *butBGlable;

@property (weak, nonatomic) IBOutlet UILabel *rowLB;

@property (weak, nonatomic) IBOutlet UILabel *product_category;
@property (weak, nonatomic) IBOutlet UITextField *product_name;

@property (weak, nonatomic) IBOutlet UITextField *product_price;
@property (weak, nonatomic) IBOutlet UILabel *totalPrice;

@property (weak, nonatomic) IBOutlet UILabel *bigLable;//用来遮盖数量
@property (weak, nonatomic) IBOutlet UILabel *product_count;
@property (weak, nonatomic) IBOutlet UIButton *seleBut;

@property(nonatomic,copy)void (^myblock)(NSInteger stute,NSString *coun);
-(void)refashWithmodel:(CSmakeAccAddmodel *)model;

@end

NS_ASSUME_NONNULL_END
