//
//  CSChangeTwoTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSmakeAccAddmodel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSChangeTwoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *indexLb;

@property (weak, nonatomic) IBOutlet UITextField *PeiJianFeiTF;

@property (weak, nonatomic) IBOutlet UILabel *odlCategoryLB;
@property (weak, nonatomic) IBOutlet UILabel *oldPruNameLB;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLB;

@property (weak, nonatomic) IBOutlet UILabel *neCategoryLB;
@property (weak, nonatomic) IBOutlet UILabel *neNameLB;
@property (weak, nonatomic) IBOutlet UILabel *nePriceLB;


@property (weak, nonatomic) IBOutlet UILabel *oneBG;
@property (weak, nonatomic) IBOutlet UILabel *twoBg;

@property (weak, nonatomic) IBOutlet UILabel *threebg;
@property (weak, nonatomic) IBOutlet UILabel *fourBG;

@property(nonatomic,copy)void (^myblock)(NSString *strl);

-(void)reasfWith:(CSmakeAccAddmodel *)molde;




@end

NS_ASSUME_NONNULL_END
