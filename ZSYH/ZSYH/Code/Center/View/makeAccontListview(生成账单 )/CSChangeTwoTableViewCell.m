//
//  CSChangeTwoTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSChangeTwoTableViewCell.h"
@interface CSChangeTwoTableViewCell ()<UITextFieldDelegate>
@end
@implementation CSChangeTwoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.PeiJianFeiTF.delegate = self;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.oneBG.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.oneBG.bounds;
    maskLayer.path = maskPath.CGPath;
    self.oneBG.layer.mask = maskLayer;
    self.oneBG.layer.borderWidth = 1;
    self.oneBG.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.twoBg.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.twoBg.bounds;
    maskLayer1.path = maskPath1.CGPath;
    self.twoBg.layer.mask = maskLayer1;
    self.twoBg.layer.borderWidth = 1;
    self.twoBg.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    UIBezierPath *maskPath3 = [UIBezierPath bezierPathWithRoundedRect:self.threebg.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer3 = [[CAShapeLayer alloc] init];
    maskLayer3.frame = self.threebg.bounds;
    maskLayer3.path = maskPath3.CGPath;
    self.threebg.layer.mask = maskLayer3;
    self.threebg.layer.borderWidth = 1;
    self.threebg.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    
    UIBezierPath *maskPath4 = [UIBezierPath bezierPathWithRoundedRect:self.fourBG.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(8, 8)];
    CAShapeLayer *maskLayer4 = [[CAShapeLayer alloc] init];
    maskLayer4.frame = self.fourBG.bounds;
    maskLayer4.path = maskPath4.CGPath;
    self.fourBG.layer.mask = maskLayer4;
    self.fourBG.layer.borderWidth = 1;
    self.fourBG.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    
    self.indexLb.layer.borderWidth = 1;
    self.indexLb.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    self.indexLb.layer.masksToBounds = YES;
    self.indexLb.layer.cornerRadius = self.indexLb.width/2;
    
    
   // 订单状态(0生成订单（已接单）//生成检测报告
   // ，1已检测，//生成账单
    //2生成账单（未支付），//去支付
   // 3已支付（已完成），//去评价
    //4已评价， // 更换配件 检测报告  查看账单
  //  5已取消)',//先不管
    
    
  
//http://192.168.2.114:8094/cs_order/addRepProduct?data={"orderId":"001","products":[{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321"},{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321"},{"price":"253","type_id":"12313","count":"2","name":"波片","productId":"1321","peopleCost":"50"}]}

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)reasfWith:(CSmakeAccAddmodel *)molde{
    
    
    self.odlCategoryLB.text = [NSString stringWithFormat:@"%@",molde.old_type_name];
    self.oldPruNameLB.text = [NSString stringWithFormat:@"%@",molde.old_name];
    self.oldPriceLB.text = [NSString stringWithFormat:@"%@",molde.old_price];

    self.neCategoryLB.text = [NSString stringWithFormat:@"%@",molde.ne_type_name];
    self.neNameLB.text = [NSString stringWithFormat:@"%@",molde.ne_name];
    self.nePriceLB.text = [NSString stringWithFormat:@"%@",molde.ne_price];
    
    
    self.indexLb.text = molde.row;
self.PeiJianFeiTF.text = [NSString stringWithFormat:@"%@",molde.total_price];
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (self.myblock) {
        self.myblock(textField.text);
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (self.myblock) {
        self.myblock(textField.text);
    }
    return YES;
}
- (IBAction)dele:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(@"haodexinlizhuangtai");
    }
}


@end

