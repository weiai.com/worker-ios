//
//  CSChangeProZeroTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CSChangeProZeroTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *iindexLB;
@property(nonatomic,copy)void (^myblock)(NSString *ind);
@property (weak, nonatomic) IBOutlet UILabel *upLB;
@property (weak, nonatomic) IBOutlet UILabel *dowLB;

@end

NS_ASSUME_NONNULL_END
