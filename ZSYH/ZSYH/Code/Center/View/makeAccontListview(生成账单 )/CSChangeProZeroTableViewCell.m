//
//  CSChangeProZeroTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSChangeProZeroTableViewCell.h"

@implementation CSChangeProZeroTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.upLB.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.upLB.bounds;
    maskLayer.path = maskPath.CGPath;
    self.upLB.layer.mask = maskLayer;
    self.upLB.layer.borderWidth = 1;
    self.upLB.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:self.upLB.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.upLB.bounds;
    maskLayer2.path = maskPath2.CGPath;
    self.upLB.layer.mask = maskLayer2;
    self.upLB.layer.borderWidth = 1;
    self.upLB.layer.borderColor = [UIColor colorWithHexString:@"#DBDBDB"].CGColor;
    
    
    
    self.iindexLB.layer.borderWidth = 1;
    self.iindexLB.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    self.iindexLB.layer.masksToBounds = YES;
    self.iindexLB.layer.cornerRadius = 9;
    
    
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)oldPruSayiSao:(UIButton *)sender {
    if (self.myblock) {
        self.myblock(@"0");
    }
}

- (IBAction)newPrusaoyisao:(UIButton *)sender {
    
    ShowToastWithText(@"请先选择旧配件");
    
    
}


@end
