//
//  AddingAccessoriesCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "AddingAccessoriesCell.h"

@interface AddingAccessoriesCell ()<UITextFieldDelegate>

@property(nonatomic,strong)UIView *bgViewsec;

@end

@implementation AddingAccessoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.butBGlable.layer.borderWidth = 1;
    self.butBGlable.layer.masksToBounds = YES;
    self.butBGlable.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    self.butBGlable.layer.cornerRadius = 2;
    self.bigLable.backgroundColor = [UIColor whiteColor];
    self.bigLable.hidden = YES;
    
    self.product_price.delegate = self;
    self.product_name.delegate = self;
    [self.product_price addTarget:self action:@selector(inputContentClick:) forControlEvents:(UIControlEventEditingDidEnd)];
    [self.product_price addTarget:self action:@selector(inputContentClending:) forControlEvents:(UIControlEventEditingDidBegin)];
    [self.product_name addTarget:self action:@selector(inputContentClick:) forControlEvents:(UIControlEventEditingDidEnd)];
    [self.product_name addTarget:self action:@selector(inputContentClending:) forControlEvents:(UIControlEventEditingDidBegin)];
    [self.product_price addRules];

    [self.product_price addRules];
    self.rowLB.layer.borderWidth = 1;
    self.rowLB.layer.borderColor = [UIColor colorWithHexString:@"#70BE68"].CGColor;
    self.rowLB.layer.masksToBounds = YES;
    self.rowLB.layer.cornerRadius = self.rowLB.width/2;
    
    
    // Initialization code
}
- (void)inputContentClending:(UITextField *)sender {
    
    if ([self.myModel.isSaoyisao integerValue] == 1) {
        self.bigLable.hidden = YES;

    }else{
        self.bigLable.hidden = NO;
        
    }
    
}
- (void)inputContentClick:(UITextField *)sender {
    

    if ([self.myModel.isSaoyisao integerValue] == 1) {
        self.bigLable.hidden = YES;
        
    }else{
        self.bigLable.hidden = NO;

    }
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.product_price) {
        
        self.product_price.text = @"";
        if (self.myblock) {
            self.myblock(0, @"");
        }
    }else if(textField == self.product_name){
        
        if (self.myblock) {
            self.myblock(1, @"");
        }
    }
    return YES;
}
- (BOOL)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.product_price) {
        if ([self.product_price.text hasPrefix:@"0"]) {
            ShowToastWithText(@"价格不能是0开头");
            textField.text = @"";
        }
        if (self.myblock) {
            self.myblock(0, textField.text);
        }
    }else if(textField == self.product_name){
        
        if (self.myblock) {
            self.myblock(1, textField.text);
        }
    }
    
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.product_price) {
        
        NSArray *attt = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
        if (![attt containsObject:string]  && ![string  isEqualToString:@""]) {
            ShowToastWithText(@"只能输入数字");
            return NO;
            
        }else{
            return YES;
            
        }
    }else{
        return YES;
    }
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
//    @property(nonatomic,strong)NSString *row;
//    @property(nonatomic,strong)NSString *product_category;
//    @property(nonatomic,strong)NSString *product_categoryid;
//    @property(nonatomic,strong)NSString *product_name;
//    @property(nonatomic,strong)NSString *product_price;
//    @property(nonatomic,strong)NSString *product_number;
//    @property(nonatomic,strong)NSString *total_price;
//
//    @property(nonatomic,strong)NSString *isSaoyisao;//0是扫一扫的 不展示加减 // 1是正常手输的 // 2是更换配件
    
    // Configure the view for the selected state
}
-(void)refashWithmodel:(CSmakeAccAddmodel *)model{
//    if (!model.product_name) {
//        return;
//    }
    self.myModel = model;
    
//    self.bigLable.backgroundColor = zhutiColor;
    self.bigLable.hidden = NO;
    self.bigLable.userInteractionEnabled = YES;

    if ([model.isSaoyisao integerValue] == 1) {
        //手输
        self.bigLable.hidden = YES;
        self.bigLable.userInteractionEnabled = NO;

    }
    
    self.product_name.userInteractionEnabled = YES;
    self.seleBut.userInteractionEnabled = YES;
    if ([model.isSaoyisao integerValue] == 0) {
        self.product_name.userInteractionEnabled = NO;
        self.seleBut.userInteractionEnabled = NO;
    }
    self.product_count.text = [NSString stringWithFormat:@"%@",model.count];
    if (model.type_name) {
        self.product_category.text = model.type_name;
        self.product_category.textColor = [UIColor blackColor];
    } else {
        self.product_category.text = @"请选择配件品类";
        self.product_category.textColor = [UIColor colorWithHexString:@"#B7B7B7"];
    }
    //self.product_category.text = model.type_name;
    self.product_name.text = model.name;
    self.product_price.text = model.price;
    if (strIsEmpty(model.total_price)) {
        model.total_price = @"0";
    }
    self.totalPrice.text = [NSString stringWithFormat:@"%@",model.total_price];
    self.product_count.text = model.count;
    self.rowLB.text = model.row;
}

- (IBAction)jinashaoBUt:(UIButton *)sender {
    KMyLog(@"ddd");
    [self.product_price resignFirstResponder];
    [self.product_name resignFirstResponder];
    
    NSInteger ind = [self.product_count.text integerValue];
    ind -=1;
    if (ind == 0) {
        ind = 1;
    }
    if (ind == -1) {
        ind = 1;
    }
    self.product_count.text = [NSString stringWithFormat:@"%ld",ind];
    
    if ([self.product_price.text floatValue] >= 0) {
        CGFloat fff = [self.product_price.text floatValue] * ind;
        self.totalPrice.text = [NSString stringWithFormat:@"%.2f",fff];
        if (self.myblock) {
            self.myblock(3, self.product_count.text);
        }
    }
  
    
    
}

- (IBAction)addbut:(UIButton *)sender {
    [self.product_price resignFirstResponder];
    [self.product_name resignFirstResponder];
    NSInteger ind = [self.product_count.text integerValue];
    ind +=1;
    if (ind == 0) {
        ind = 1;
    }
    self.product_count.text = [NSString stringWithFormat:@"%ld",ind];
    if ([self.product_price.text floatValue] >= 0) {
        CGFloat fff = [self.product_price.text floatValue] * ind;
        self.totalPrice.text = [NSString stringWithFormat:@"%.2f",fff];
       
    }
    if (self.myblock) {
        self.myblock(3, self.product_count.text);
    }
    
   
}
- (IBAction)saoyisao:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2 3配件数量 4扫一扫 5 6 7

    if (self.myblock) {
        self.myblock(4, @"");
    }
    
}
- (IBAction)seletorProductGAtegory:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2 3配件数量 4扫一扫 5选择配件种类 6 7
    
    if (self.myblock) {
        self.myblock(5, @"");
    }
    
    
}
- (IBAction)deldeAction:(UIButton *)sender {
    //self.myblock(0, @""); 0配件单价 1 配件名称2删除 3配件数量 4扫一扫 5选择配件种类 6 7
    
    if (self.myblock) {
        self.myblock(2, @"");
    }
}


@end
