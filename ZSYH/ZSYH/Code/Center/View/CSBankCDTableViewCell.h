//
//  CSBankCDTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/21.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSCbankCardListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSBankCDTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cardImage;
@property (weak, nonatomic) IBOutlet UILabel *cardNmme;

@property (weak, nonatomic) IBOutlet UILabel *cardtype;

@property (weak, nonatomic) IBOutlet UILabel *cardNumber;

-(void)refash:(CSCbankCardListModel *)molel;
@end

NS_ASSUME_NONNULL_END
