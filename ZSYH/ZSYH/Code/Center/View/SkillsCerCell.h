//
//  SkillsCerCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/13.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SkillsCerCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *imageB;
@property (weak, nonatomic) IBOutlet UIButton *delB;

@end

NS_ASSUME_NONNULL_END
