//
//  PingzhengceTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/20.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PingzhengceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *conlb;
@property (weak, nonatomic) IBOutlet UIButton *rightBut;

@end

NS_ASSUME_NONNULL_END
