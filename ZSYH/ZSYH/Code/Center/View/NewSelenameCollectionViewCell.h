//
//  NewSelenameCollectionViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/6/25.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewSelenameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *myLb;

@end

NS_ASSUME_NONNULL_END
