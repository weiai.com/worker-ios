//
//  CSMyCustTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "csMyKehuModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMyCustTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *timeLB;
@property (weak, nonatomic) IBOutlet UILabel *phonelb;
@property (weak, nonatomic) IBOutlet UILabel *addressLb;


-(void)refashWithmodel:(csMyKehuModel *)model;
@end

NS_ASSUME_NONNULL_END
