//
//  CSMyCustTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSMyCustTableViewCell.h"

@implementation CSMyCustTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.timeLB.font = KFontPingFangSCMedium(14);
    self.phonelb.font = KFontPingFangSCMedium(14);
    self.addressLb.font = KFontPingFangSCMedium(14);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)refashWithmodel:(csMyKehuModel *)model{

    if ([model.headimg hasPrefix:@"http"]) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:model.headimg] placeholderImage:defaultImg];
    } else {
        sdimg(self.userImage, model.headimg);
    }
    self.timeLB.text = [NSString stringWithFormat:@"%@",model.user_name];
    self.phonelb.text = [NSString stringWithFormat:@"%@",model.phone];
    self.addressLb.text = [NSString stringWithFormat:@"%@",model.update_time];
}

@end
