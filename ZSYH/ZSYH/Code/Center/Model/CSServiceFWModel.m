//
//  CSServiceFWModel.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSServiceFWModel.h"

@implementation CSServiceFWModel
- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@", self.Id, self.fault_name, self.parent_id, self.parent_code, self.type_code, self.type_name, self.type_level];
}
@end
