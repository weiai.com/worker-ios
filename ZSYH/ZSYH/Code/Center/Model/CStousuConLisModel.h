//
//  CStousuConLisModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CStousuConLisModel : BaseModel
@property (nonatomic, copy) NSString *del_content;
@property (nonatomic, copy) NSString *complaint_id;
@property (nonatomic, copy) NSString *del_time;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *delwith_user_id;
@end

NS_ASSUME_NONNULL_END
