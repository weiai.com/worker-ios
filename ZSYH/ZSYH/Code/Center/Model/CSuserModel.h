//
//  CSuserModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSuserModel : BaseModel
@property (nonatomic, copy) NSString *user_type_id;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *user_state;
@property (nonatomic, copy) NSString *user_gender;
@property (nonatomic, copy) NSString *user_phone;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *user_account;
@property (nonatomic, copy) NSString *user_password;


@end

NS_ASSUME_NONNULL_END
