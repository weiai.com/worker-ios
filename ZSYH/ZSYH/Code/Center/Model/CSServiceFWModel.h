//
//  CSServiceFWModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/4.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSServiceFWModel : BaseModel
@property(nonatomic,strong)NSString *Id;
@property(nonatomic,strong)NSString *fault_name;
@property(nonatomic,assign)BOOL issele;

@property (nonatomic, copy) NSString *parent_id;
@property (nonatomic, copy) NSString *parent_code;
@property (nonatomic, copy) NSString *type_level;
@property (nonatomic, copy) NSString *type_code;
@property (nonatomic, copy) NSString *type_name;

@end

NS_ASSUME_NONNULL_END
