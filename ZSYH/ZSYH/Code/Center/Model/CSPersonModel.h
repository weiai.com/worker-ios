//
//  CSPersonModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSPersonModel : BaseModel

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *workstate;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *invitationCode;

@property (nonatomic, copy) NSString *area_name;
@property (nonatomic, copy) NSString *faultType;

@property (nonatomic, copy) NSString *personName;
@property (nonatomic, copy) NSString *alipay_name;
@property (nonatomic, copy) NSString *alipay_image_url;

@property (nonatomic, copy) NSString *wx_image_url;

@property (nonatomic, copy) NSString *wx_name;

@property (nonatomic, strong) NSArray *area;
@property (nonatomic, strong) id id_card_no;
@property (nonatomic, strong) NSString *licenseState; //相关证件审核状态
@property (nonatomic, strong) NSString *refusalReasons; //未通过备注

@property (nonatomic, copy) NSString *star_evaluate;
@property (nonatomic, copy) NSString *businessLicense;
@property (nonatomic, copy) NSString *invitation_code;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *orderNumber;

@property (nonatomic, copy) NSString *licenseTime;
@property (nonatomic, copy) NSString *order_number;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *fault_type_id;
@property (nonatomic, copy) NSString *starEvaluate;

@property (nonatomic, copy) NSString *valid_state;
@property (nonatomic, copy) NSString *alipay_userid;
@property (nonatomic, copy) NSString *handHeldIdUrl;
//@property (nonatomic, copy) NSString *
//@property (nonatomic, copy) NSString *
//
//@property (nonatomic, copy) NSString *
//@property (nonatomic, copy) NSString *
//@property (nonatomic, copy) NSString *
//@property (nonatomic, copy) NSString *
//@property (nonatomic, copy) NSString *
/*
 {
 "area" : [
 {
 "id" : "710109",
 "area" : "台湾台北市南港区"
 },
 {
 "id" : "710104",
 "area" : "台湾台北市中山区"
 },
 {
 "id" : "710101",
 "area" : "台湾台北市松山区"
 }
 ],
 "id_card_no" : "410423199309221001"
 }*/
@end

NS_ASSUME_NONNULL_END
