//
//  HFShopCarModel.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFShopCarGoodsModel : NSObject
@property (nonatomic, copy) NSString *pay_state;
@property (nonatomic, copy) NSString *product_pic;
@property (nonatomic, copy) NSString *count;
@property (nonatomic, copy) NSString *product_info;
@property (nonatomic, copy) NSString *product_type_id;
@property (nonatomic, copy) NSString *product_price;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *shop_id;
@property (nonatomic, copy) NSString *product_bel_type;
@property (nonatomic, copy) NSString *product_id;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *product_state;
@property (nonatomic, copy) NSString *car_id;
@property (nonatomic, copy) NSString *Stock;

/**+++++++++++++++++++**/
@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, assign) CGFloat totalPrice;

//店铺信息
@property (nonatomic, copy) NSString *shopname;
@property (nonatomic, copy) NSString *shopid;

@end

NS_ASSUME_NONNULL_END
