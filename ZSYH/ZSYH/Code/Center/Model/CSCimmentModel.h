//
//  CSCimmentModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSCimmentModel : BaseModel
@property(nonatomic,strong)NSString * rep_user_id;
@property(nonatomic,strong)NSString * score;
@property(nonatomic,strong)NSString * update_time;
@property(nonatomic,strong)NSString * create_time;
@property(nonatomic,strong)NSString * order_number;
@property(nonatomic,strong)NSString * id;
@property(nonatomic,strong)NSString * order_id;
@property(nonatomic,strong)NSString * content;

@end

NS_ASSUME_NONNULL_END
