//
//  SkillsCerModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/7/13.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SkillsCerModel : NSObject

@property (nonatomic, copy) NSString *certificate_type;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *update_time;
//自定义字段 判断是否是 接口返回的原始图片数据
@property (nonatomic, copy) NSString *isOriginalData;//1是原始数据 


//"certificate_type" : "1",
//"id" : "667460495954939904",
//"image_url" : "https:\/\/www.zzzsyh.com\/image\/user\/repairUserCertificate\/667460495615201280\/201907131144343.jpg",
//"rep_user_id" : "667460495615201280",
//"create_time" : "2019-07-13 11;44:37",
//"update_time" : "2019-07-13 11;44:37"

@end

NS_ASSUME_NONNULL_END
