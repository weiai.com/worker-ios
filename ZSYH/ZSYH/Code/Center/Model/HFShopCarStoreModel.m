//
//  HFShopCarStoreModel.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/20.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopCarStoreModel.h"

@implementation HFShopCarStoreModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
}



- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    HFShopCarStoreModel *model = [[[self class] allocWithZone:zone] init];
    model.level = self.level;
    model.logo  = self.logo;
    model.store_id = self.store_id;
    model.store_name = self.store_name;
    return model;
}

@end
