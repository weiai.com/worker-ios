//
//  HFAddressModel.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/13.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFAddressModel : NSObject

/**
 地址id*/
@property (nonatomic, copy) NSString *address_id;

/**
 建筑物名称*/
@property (nonatomic, copy) NSString *building_name;

/**
 姓名*/
@property (nonatomic, copy) NSString *consignee;

/**
 房间号*/
@property (nonatomic, copy) NSString *house_number;

/**
 是否默认*/
@property (nonatomic, assign) NSInteger is_default;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;

/**
 手机号*/
@property (nonatomic, copy) NSString *mobile;

/**
 地区*/
@property (nonatomic, copy) NSString *region_name;

@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *addressName;

@end

NS_ASSUME_NONNULL_END
