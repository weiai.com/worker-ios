//
//  CSCbankCardListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/27.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSCbankCardListModel : BaseModel
@property (nonatomic, copy) NSString *bank_card_state;
@property (nonatomic, copy) NSString *card_pic_url;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *type_name;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *logo_url;
@property (nonatomic, copy) NSString *band_card_type;
@property (nonatomic, copy) NSString *bank_card_belong;
@property (nonatomic, copy) NSString *bank_card;



@end

NS_ASSUME_NONNULL_END
