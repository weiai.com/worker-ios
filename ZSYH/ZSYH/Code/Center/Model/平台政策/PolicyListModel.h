//
//  PolicyListModel.h
//  ZSYH
//
//  Created by 李 on 2019/11/13.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PolicyListModel : NSObject

@property (nonatomic, copy) NSString *update_time;//2019-11-11 17:54:56",
@property (nonatomic, copy) NSString *type_code;//1",
@property (nonatomic, copy) NSString *id;//"1",
@property (nonatomic, copy) NSString *type_name;// "类型1",
@property (nonatomic, copy) NSString *pol_comment;// "1.每周二可以提起申请，结算上周一至周日的所有实际收益。2.每天的首单中会扣除2.8元作为保险的费用。3.每单抽取10%的信息服务费，6%的技术支持费。",
@property (nonatomic, copy) NSString *pol_cycle;// "7",
@property (nonatomic, copy) NSString *pol_name;//"侯保师傅结算",
@property (nonatomic, copy) NSString *create_time;// "2019-05-17 10:59:15",
@property (nonatomic, copy) NSString *pol_date;// "1",
@property (nonatomic, copy) NSString *state;// "1",
@property (nonatomic, copy) NSString *pol_type;// "1"
@end

NS_ASSUME_NONNULL_END
