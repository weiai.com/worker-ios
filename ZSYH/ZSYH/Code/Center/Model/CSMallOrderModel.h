//
//  CSMallOrderModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/26.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSMallOrderModel : BaseModel
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *orderNumber;
@property (nonatomic, copy) NSString *repUserId;
@property (nonatomic, copy) NSString *countMoney;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *sendTypeId;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, copy) NSString *carId;
@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *product_bel_type;
@property (nonatomic, copy) NSString *product_pic;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *product_price;
@property (nonatomic, copy) NSString *product_info;
@property (nonatomic, copy) NSString *shop_order_id;
@property (nonatomic, copy) NSString *product_state;
@property (nonatomic, copy) NSString *product_type_id;
@property (nonatomic, copy) NSString *Stock;
@property (nonatomic, copy) NSString *cell_product_id;
@property (nonatomic, copy) NSString *pro_count;

@property (nonatomic, copy) NSString *shop_name;
@property (nonatomic, copy) NSString *shop_id;

@end

NS_ASSUME_NONNULL_END
