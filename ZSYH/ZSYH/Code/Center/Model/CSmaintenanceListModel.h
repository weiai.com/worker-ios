//
//  CSmaintenanceListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/4/1.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"
@class ZOrderInfoPartInfoModel;

NS_ASSUME_NONNULL_BEGIN

@interface CSmaintenanceListModel : BaseModel
///有值，显示厂商联系电话，否则不展示
@property (nonatomic, copy) NSString *test_order_id;

@property (nonatomic, copy) NSString *common_failures;
@property (nonatomic, copy) NSString *order_time;
@property (nonatomic, copy) NSString *order_date;
@property (nonatomic, copy) NSString *user_address;
@property (nonatomic, copy) NSString *user_phone;

@property (nonatomic, copy) NSString *has_elevator;
@property (nonatomic, copy) NSString *userPhone;
@property (nonatomic, copy) NSString *address_content;
@property (nonatomic, assign) double repair_number;
@property (nonatomic, copy) NSString *create_time;

@property (nonatomic, copy) NSString *service_name;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *company_name;
@property (nonatomic, copy) NSString *floorNum;
@property (nonatomic, copy) NSString *fault_category;

@property (nonatomic, copy) NSString *order_state;
@property (nonatomic, copy) NSString *order_type_id;
@property (nonatomic, copy) NSString *cancel_remark;
@property (nonatomic, copy) NSString *fault_comment;
@property (nonatomic, copy) NSString *orderId;

@property (nonatomic, copy) NSString *user_address_id;
@property (nonatomic, copy) NSString *fault_name;
@property (nonatomic, copy) NSString *fault_type; //故障类型
@property (nonatomic, copy) NSString *user_type;
@property (nonatomic, copy) NSString *cancel_reason;

@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, copy) NSString *isprivate_work;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, copy) NSString *parName;

@property (nonatomic, copy) NSString *courierCompany;//快递公司编号
@property (nonatomic, copy) NSString *courierNumber;//快递号 当courierCompany 和 courierNumber 同时有内容,才能查看物流
@property (nonatomic, copy) NSString *peopleCost;
@property (nonatomic, copy) NSString *rep_order_id;
@property (nonatomic, copy) NSString *rep_order_state;

@property (nonatomic, copy) NSString *is_protect;
@property (nonatomic, copy) NSString *pay_time;
@property (nonatomic, copy) NSString *fixed_price;
@property (nonatomic, copy) NSString *product_source;
@property (nonatomic, copy) NSString *confirmation_product;

@property (nonatomic, copy) NSString *subscribe_date;
@property (nonatomic, copy) NSString *subscribe_time;
@property (nonatomic, copy) NSString *repid;
@property (nonatomic, assign) double delstate;
@property (nonatomic, copy) NSString *number;
//扫码下单
@property (nonatomic, copy) NSString *state;  //扫码订单 1显示 2不显示button
@property (nonatomic, copy) NSString *parts_id; //配件id
@property (nonatomic, copy) NSString *cost;
@property (nonatomic, copy) NSString *fault_common; //故障原因
@property (nonatomic, copy) NSString *type_name;

@property (nonatomic, copy) NSString *product_name;
@property (nonatomic, copy) NSString *validity;
@property (nonatomic, copy) NSString *quality;
@property (nonatomic, copy) NSString *is_again_order; //10.10添加
@property (nonatomic, copy) NSString *wxsend_overtime;

@property (nonatomic, copy) NSString *areaId;
@property (nonatomic, copy) NSString *order_model;
@property (nonatomic, copy) NSString *createTime;//订单完成的时间
@property (nonatomic, copy) NSString *order_id;
@property (nonatomic, copy) NSString *is_send_bill;

@property (nonatomic, copy) NSString *isOrder;
@property (nonatomic, copy) NSString *company_phone;
@property (nonatomic, copy) NSString *rep_user_id;
@property (nonatomic, copy) NSString *is_appset;
@property (nonatomic, copy) NSString *earning;

@property (nonatomic, copy) NSString *credit_score;
@property (nonatomic, copy) NSString *pay_type;
@property (nonatomic, copy) NSString *insurance_premium;
@property (nonatomic, copy) NSString *install;
@property (nonatomic, copy) NSString *azNumber;//10.30添加

@property (nonatomic, copy) NSString *orderType;//10.31添加
@property (nonatomic, copy) NSString *is_appsetA;
@property (nonatomic, copy) NSString *all_cost;
@property (nonatomic, copy) NSString *is_appsetB;
@property (nonatomic, copy) NSString *invoice_status;

@property (nonatomic, copy) NSString *app_order_id;
@property (nonatomic, copy) NSString *remake;

//订单详情 -> 查看配件详情
@property (nonatomic, copy) NSString *prodcutComment;//配件名称字段(顶部展示)
@property (nonatomic, strong) NSArray <ZOrderInfoPartInfoModel *> *pros;//该订单使用的配件列表

@property (nonatomic, copy) NSString *product_type_id;//

@end

#pragma mark - 订单详情 -> 配件详情model
@interface ZOrderInfoPartInfoModel : BaseModel
@property (nonatomic, copy) NSString *type_name;//配件品类
@property (nonatomic, copy) NSString *product_name;//配件名称
@property (nonatomic, copy) NSString *productionDate;//有效期开始
@property (nonatomic, copy) NSString *termValidity;//有效期结束
@property (nonatomic, copy) NSString *install;//没有有效期开始和结束 使用该字段
@property (nonatomic, copy) NSString *warranty;//质保期
@property (nonatomic, copy) NSString *quantity;//
@property (nonatomic, copy) NSString *salePrice;//
@property (nonatomic, copy) NSString *qrcode;//
@property (nonatomic, copy) NSString *partsId;//
@property (nonatomic, copy) NSString *count;//
@property (nonatomic, copy) NSString *product_type_id;//
@property (nonatomic, copy) NSString *type;//
@property (nonatomic, copy) NSString *compensationPrice;//
@property (nonatomic, copy) NSString *contractPrice;//
@property (nonatomic, copy) NSString *isaddnew;//
@property (nonatomic, copy) NSString *name;//
@property (nonatomic, copy) NSString *state;//
@property (nonatomic, copy) NSString *order_id;//
@property (nonatomic, copy) NSString *product_model;//
@property (nonatomic, copy) NSString *createDate;//
@property (nonatomic, copy) NSString *soft;//
@property (nonatomic, copy) NSString *create_time;

@end

NS_ASSUME_NONNULL_END
