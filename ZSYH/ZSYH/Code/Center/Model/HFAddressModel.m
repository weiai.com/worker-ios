//
//  HFAddressModel.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/13.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFAddressModel.h"

@implementation HFAddressModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    if ([key isEqualToString:@"id"]) {
        self.Id = value;
    }
}

@end










/*
//
//  HFShopingCarViewController.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/12.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFShopingCarViewController.h"
#import "HFShopingCarCell.h"
#import "HFShopCarHeaderView.h"
#import "HFShopCarBottomView.h"
#import "HFShopCarGoodsModel.h"
#import "HFShopCarStoreModel.h"
#import "WpayOrdViewController.h"
#import "CommitOrderController.h"
#import "CSorderListViewController.h"

@interface HFShopingCarViewController () <UITableViewDelegate, UITableViewDataSource>
{
    BOOL _isDelete;
}
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableDictionary *dataSouceDic;

@property (nonatomic, assign) CGFloat orderTotalPrice;

@property (nonatomic, strong) NSMutableArray *orderTotalGoods;

@property (nonatomic, strong) NSMutableArray *headerViews;

@property (nonatomic, strong) NSMutableArray *stores;

@property (nonatomic, strong) HFShopCarBottomView *bottomView;
@property (nonatomic, strong) NSMutableArray *chunzhiArr;

@end

@implementation HFShopingCarViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isDelete = NO;
    
    [self.view addSubview:self.tableView];
    
    [self configNavi];
    
    [self configBottomBtn];
    
    [self requestListData];
}


- (void)configNavi {
    [self.leftbutton setImage:imgname(@"mblack") forState:(UIControlStateNormal)];
    
    self.title = @"购物车";
    
    [self.rightbutton setTitle:@"编辑" forState:UIControlStateNormal];
    
    [self.rightbutton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
}

- (void)configBottomBtn {
    HFShopCarBottomView *bottomView = [[HFShopCarBottomView alloc] initWithFrame:CGRectMake(0, ScreenH-kTabbarHeight, ScreenW, 49)];
    [self.view addSubview:bottomView];
    self.bottomView = bottomView;
    kWeakSelf;
    bottomView.selectAllBlock = ^(BOOL select) {
        
        for (HFShopCarStoreModel *storeModel in weakSelf.stores) {
            storeModel.isSelect = select;
            NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
            NSArray *goodsArray = [self.dataSouceDic objectForKey:key];
            for (HFShopCarGoodsModel *goodsModel in goodsArray) {
                goodsModel.isSelect = select;
                if (select) {
                    [weakSelf.orderTotalGoods addObject:goodsModel];
                }
            }
        }
        
        if (!select) {
            [weakSelf.orderTotalGoods removeAllObjects];
        }
        
        [weakSelf calculationOrderTotalPrice];
        [weakSelf.tableView reloadData];
    };
    
    
    bottomView.deleteOrSettleBtnClock = ^(NSInteger index) {
        if (index == 0) { // 结算
            
            NSLog(@"%@", [self.orderTotalGoods firstObject]);
            NSMutableArray *array = [NSMutableArray arrayWithCapacity:1];
            for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
                NSMutableDictionary *temp = [NSMutableDictionary dictionary];
                //                temp[@"goods_id"]  = model.goods_id;
                //                temp[@"quantity"] = model.quantity;
                //                temp[@"spec"]   = model.spec_key;
                //                temp[@"cart_id"]   = model.cart_id;
                //
                //                [array addObject: temp];
            }
            
            
            NSData *json = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
            NSString *str = [[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding];
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            param[@"data"] = str;
            //    if (strIsEmpty(str)) {
            //        param[@"type"] = @"confirm";
            //
            //    }else{
            //        param[@"type"] = @"cart";
            //
            //    }
            param[@"type"] = @"cart";
            
            [NetWorkTool POST:@"" param:param success:^(id dic) {
                CommitOrderController *vc = [[CommitOrderController alloc]init];
                vc.response = dic;
                vc.spec_str = str;
                vc.isOrder = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
                //                ShowToastWithText(dic[@"msg"]);
            } other:^(id dic) {
                
            } fail:^(NSError *error) {
                
            } needUser:YES];
            
            
            
            
        } else { // 删除
            [weakSelf requestDeleteMoreCartData];
        }
    };
    
    
}


#pragma mark -- 请求接口
/*! 产品列表 */

//- (void)requestListData {
//
//    [self.orderTotalGoods removeAllObjects];
//    self.orderTotalPrice = 0;
//    [self.headerViews removeAllObjects];
//    [self.dataSouceDic removeAllObjects];
//    [self.stores removeAllObjects];
//
//    kWeakSelf;
//    [NetWorkTool POST:CmycurList param:nil success:^(id dic) {
//        NSLog(@"++++++++++++++++%@", [HFTools toJSONString:dic]);
//
//        /*! 获取购物车店铺个数 */
//        NSArray *storeArray = dic[@"retval"];
//        for (NSDictionary *dataDic in storeArray) {
//
//            // 先创建固定个数的headerView
//            HFShopCarHeaderView *header = [[HFShopCarHeaderView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 38)];
//            [weakSelf.headerViews addObject:header];
//
//            /*! 获取单个店铺的产品个数 */
//            NSArray *goodsList = dataDic[@"goods_list"];
//            NSMutableArray *goodsArray = [NSMutableArray arrayWithCapacity:1];
//            for (NSDictionary *goodsDic in goodsList) {
//                HFShopCarGoodsModel *goodsModel = [[HFShopCarGoodsModel alloc] init];
//                [goodsModel setValuesForKeysWithDictionary:goodsDic];
//                CGFloat price = [goodsModel.count integerValue] * [goodsModel.product_price floatValue];
//
//                goodsModel.totalPrice = price;
//                goodsModel.isSelect = NO;
//                [goodsArray addObject:goodsModel];
//            }
//
//            /*! 获取店铺的信息 */
//            NSDictionary *storeDic = dataDic[@"store_info"];
//            HFShopCarStoreModel *storeModel = [[HFShopCarStoreModel alloc] init];
//            [storeModel setValuesForKeysWithDictionary:storeDic];
//            storeModel.isSelect = NO;
//
//            [weakSelf.stores addObject:storeModel];
//
//            /*! 将数据保存成字典 */
//            NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
//            [weakSelf.dataSouceDic setObject:goodsArray forKey:key];
//        }
//        [weakSelf.tableView reloadData];
//    } other:^(id dic) {
//
//    } fail:^(NSError *error) {
//
//    } needUser:YES];
//
//}
//
///*! 删除单个产品 */
//- (void)requestDeleteOneCartData {
//
//    [NetWorkTool GET:@"" param:nil success:^(id dic) {
//        NSLog(@"%@", dic);
//    } other:^(id dic) {
//
//    } fail:^(NSError *error) {
//
//    } needUser:YES];
//
//}
//
///*! 批量删除产品 */
//- (void)requestDeleteMoreCartData {
//    NSMutableArray *idArray = [NSMutableArray arrayWithCapacity:1];
//    for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
//        [idArray addObject:[NSString stringWithFormat:@"%@", model.id]];
//    }
//    NSString *jsonString = [HFTools toJSONString:idArray];
//
//    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:1];
//    dic[@"cart_ids"] = jsonString;
//    kWeakSelf;
//    [NetWorkTool POST:@"" param:dic success:^(id dic) {
//        NSLog(@"%@", dic);
//        [weakSelf requestListData];
//        [weakSelf rightClick];
//        [weakSelf.bottomView changeBottomViewTotalPrice:@"0" goodsCount:@"0"];
//    } other:^(id dic) {
//
//    } fail:^(NSError *error) {
//
//    } needUser:YES];
//
//}
//
//
///*
//
//#pragma mark -- click点击事件
//- (void)rightClick {
//    if (!_isDelete) {
//        [self.rightbutton setTitle:@"完成" forState:UIControlStateNormal];
//        _isDelete = YES;
//    } else {
//        [self.rightbutton setTitle:@"编辑" forState:UIControlStateNormal];
//        _isDelete = NO;
//    }
//
//    [_bottomView changeDeleteStatus:_isDelete];
//}
//
//
///* 计算订单总价格及总数量 */
//- (void)calculationOrderTotalPrice {
//
//    CGFloat totalPrice = 0;
//    for (HFShopCarGoodsModel *model in self.orderTotalGoods) {
//        totalPrice += model.totalPrice;
//    }
//    _orderTotalPrice = totalPrice;
//
//    NSString *totalPriceStr = [NSString stringWithFormat:@"¥ %@", [HFTools formatFloat:_orderTotalPrice]];
//    NSString *totalCountStr = [NSString stringWithFormat:@"%ld", self.orderTotalGoods.count];
//    [_bottomView changeBottomViewTotalPrice:totalPriceStr goodsCount:totalCountStr];
//}
//
//// 更新底部全选按钮的状态
//- (void)updateBottomViewSelectStatus {
//    [self.bottomView changeSelectBtnStatus:YES];
//
//    for (int i = 0; i < self.stores.count; i++) {
//        HFShopCarStoreModel *storeModel = [self.stores objectAtIndex:i];
//        if (!storeModel.isSelect) {
//            [self.bottomView changeSelectBtnStatus:NO];
//            return;
//        }
//        NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
//        NSArray *values = [self.dataSouceDic objectForKey:key];
//        for (HFShopCarGoodsModel *goodsModel in values) {
//            if (!goodsModel.isSelect) {
//                [self.bottomView changeSelectBtnStatus:NO];
//                return;
//            }
//        }
//    }
//
//}
//
//
//
//#pragma mark -- tableView代理
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return [_stores count];
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:section];
//    NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
//    NSArray *values = [self.dataSouceDic objectForKey:key];
//    return values.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    HFShopingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HFShopingCarCell" forIndexPath:indexPath];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    // 获取对象模型
//    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:indexPath.section];
//    NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
//    NSArray *values = [self.dataSouceDic objectForKey:key];
//    HFShopCarGoodsModel *goods = [values objectAtIndex:indexPath.row];
//    [cell configDataWithModel:goods];
//
//    kWeakSelf;
//    cell.selectBlock = ^(HFShopCarGoodsModel * _Nonnull model) {
//
//        if (model.isSelect) {
//            if (![weakSelf.orderTotalGoods containsObject:model]) {
//                [weakSelf.orderTotalGoods addObject:model];
//            }
//
//            // 修改店铺的选中状态
//            storeModel.isSelect = YES;
//            for (HFShopCarGoodsModel *goodsModel in values) {
//                if (!goodsModel.isSelect) {
//                    storeModel.isSelect = NO;
//                }
//            }
//
//
//        } else {
//            if ([weakSelf.orderTotalGoods containsObject:model]) {
//                [weakSelf.orderTotalGoods removeObject:model];
//            }
//            // 更改店铺选中状态为NO
//            storeModel.isSelect = NO;
//        }
//
//        [weakSelf updateBottomViewSelectStatus];
//        [weakSelf calculationOrderTotalPrice];
//        [weakSelf.tableView reloadData];
//    };
//
//    cell.setCountBlock = ^(HFShopCarGoodsModel * _Nonnull model) {
//
//        [weakSelf calculationOrderTotalPrice];
//    };
//
//    return cell;
//}
//
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 38;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//
//    HFShopCarStoreModel *storeModel = [_stores objectAtIndex:section];
//    NSString *key = [NSString stringWithFormat:@"%@", storeModel.store_id];
//    NSArray *values = [self.dataSouceDic objectForKey:key];
//
//    HFShopCarHeaderView *header = [self.headerViews objectAtIndex:section];
//    [header configDataWithModel:storeModel];
//
//    kWeakSelf;
//    header.selectSectionAllBlock = ^(HFShopCarStoreModel * _Nonnull model) {
//        if (model.isSelect) {
//            for (HFShopCarGoodsModel *goods in values) {
//                goods.isSelect = YES;
//                if (![weakSelf.orderTotalGoods containsObject:goods]) {
//                    [weakSelf.orderTotalGoods addObject:goods];
//                }
//            }
//            model.isSelect = YES;
//        } else {
//            for (HFShopCarGoodsModel *goods in values) {
//                goods.isSelect = NO;
//                if ([weakSelf.orderTotalGoods containsObject:goods]) {
//                    [weakSelf.orderTotalGoods removeObject:goods];
//                }
//            }
//            model.isSelect = NO;
//        }
//        [weakSelf updateBottomViewSelectStatus];
//        [weakSelf calculationOrderTotalPrice];
//        [weakSelf.tableView reloadData];
//    };
//
//    return header;
//}
//
//#pragma mark -- 懒加载
//- (UITableView *)tableView {
//    if (_tableView == nil) {
//        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kNaviHeight, ScreenW, ScreenH-kNaviHeight-kTabbarHeight) style:(UITableViewStyleGrouped)];
//        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        if (iOS11) {
//            self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//        }
//        _tableView.tableFooterView = [UIView new];
//        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 0.1)];
//        _tableView.sectionFooterHeight = 0;
//        _tableView.sectionHeaderHeight = 0;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.rowHeight = 120;
//        _tableView.tableFooterView = [UIView new];
//
//        [_tableView registerNib:[UINib nibWithNibName:@"HFShopingCarCell" bundle:nil] forCellReuseIdentifier:@"HFShopingCarCell"];
//    }
//    return _tableView;
//}
//
//
//- (NSMutableDictionary *)dataSouceDic {
//    if (_dataSouceDic == nil) {
//        _dataSouceDic = [NSMutableDictionary dictionaryWithCapacity:1];
//    }
//    return _dataSouceDic;
//}
//
//
//- (NSMutableArray *)orderTotalGoods {
//    if (_orderTotalGoods == nil) {
//        _orderTotalGoods = [NSMutableArray arrayWithCapacity:1];
//    }
//    return _orderTotalGoods;
//}
//
//- (NSMutableArray *)headerViews {
//    if (_headerViews == nil) {
//        _headerViews = [NSMutableArray arrayWithCapacity:1];
//    }
//    return _headerViews;
//}
//
//- (NSMutableArray *)stores {
//    if (_stores == nil) {
//        _stores = [NSMutableArray arrayWithCapacity:1];
//    }
//    return _stores;
//}
//
//@end

