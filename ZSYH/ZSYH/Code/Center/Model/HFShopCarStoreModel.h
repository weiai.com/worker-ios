//
//  HFShopCarStoreModel.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/20.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFShopCarStoreModel : NSObject <NSCopying>

@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *logo;

@property (nonatomic, copy) NSString *store_id;

@property (nonatomic, copy) NSString *store_name;

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *shop_name;


/**+++++++++++++++++++**/
@property (nonatomic, assign) BOOL isSelect;


@end

NS_ASSUME_NONNULL_END
