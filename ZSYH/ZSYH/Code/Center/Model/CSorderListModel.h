//
//  CSorderListModel.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/14.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSorderListModel : BaseModel
@property(nonatomic,strong)NSString * id;
@property(nonatomic,strong)NSString * orderNumber;
@property(nonatomic,strong)NSString * repUserId;
@property(nonatomic,strong)NSString * countMoney;
@property(nonatomic,strong)NSString * state;
@property(nonatomic,strong)NSString * address;
@property(nonatomic,strong)NSString * sendTypeId;
@property(nonatomic,strong)NSString * createTime;
@property(nonatomic,strong)NSString * updateTime;
@property(nonatomic,strong)NSString * carId;



@property(nonatomic,strong)NSString * has_elevator;
@property(nonatomic,strong)NSString * rep_user_id;
@property(nonatomic,strong)NSString * repair_user_id;
@property(nonatomic,strong)NSString * order_type_id;
@property(nonatomic,strong)NSString * order_detail_add;
@property(nonatomic,strong)NSString * order_time;
@property(nonatomic,strong)NSString * all_cost;
@property(nonatomic,strong)NSString * has_charges;
@property(nonatomic,strong)NSString * order_date;
@property(nonatomic,strong)NSString * user_id;
@property(nonatomic,strong)NSString * order_address;
@property(nonatomic,strong)NSString * app_order_id;
@property(nonatomic,strong)NSString * order_state;
@property(nonatomic,strong)NSString * fault_type_id;
@property(nonatomic,strong)NSString * fault_comment;









@end

NS_ASSUME_NONNULL_END
