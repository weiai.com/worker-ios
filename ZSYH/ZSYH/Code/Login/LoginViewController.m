//
//  LoginViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/11.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "LoginViewController.h"
#import "MOFSPickerManager.h"
#import "CSaddPerCardViewController.h"
#import "CSIdAuthViewController.h"
#import "CSLoginViewController.h"
#import "CSquyuViewController.h"
#import "DPSendCodeButton.h"
#import "PerfectingInfoViewController.h"
#import "UserAgreementViewController.h"
#import "LMCountDownButton.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *accontBUtton;
@property (weak, nonatomic) IBOutlet YZMButton *yzmButton;

@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *yzmTF;
@property (weak, nonatomic) IBOutlet UITextField *YQMTF;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *agreeUplayout;
@property (weak, nonatomic) IBOutlet UIView *accontBGview;

@property (weak, nonatomic) IBOutlet UIView *acc_nameTF;

@property (weak, nonatomic) IBOutlet UIView *acc_passwordTF;
@property (weak, nonatomic) IBOutlet UIView *acc_passwordSecTF;
@property (weak, nonatomic) IBOutlet UIView *acc_yzmTF;

@property (weak, nonatomic) IBOutlet UITextField *accTF_tf;
@property (weak, nonatomic) IBOutlet UITextField *accapss_tf;

@property (weak, nonatomic) IBOutlet UITextField *accpass_sectf;
@property (weak, nonatomic) IBOutlet UITextField *accinv_tf;

@property (weak, nonatomic) IBOutlet UILabel *phoneAddressLb;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *catego;
@property(nonatomic,strong)NSString *addressCode;

@property(nonatomic,strong)NSString *yzmAcode;
@property (weak, nonatomic) IBOutlet UIButton *agreebut;

@property(nonatomic,strong)NSMutableArray  *mydata;//记录选择的区域 仅做传值用
@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];;
}

- (IBAction)phoneSeleAction:(UIButton *)sender {
    //[[MOFSPickerManager shareManger] showMOFSAddressPickerWithDefaultZipcode:@"410000-410100-410184" title:@"选择地址" cancelTitle:@"取消" commitTitle:@"确定" commitBlock:^(NSString * _Nullable address, NSString * _Nullable zipcode) {
    //    self.phoneAddressLb.text = [NSString stringWithFormat:@"%@",address];
    //    self.addressCode = zipcode;
    //} cancelBlock:^{
    //}];
    CSquyuViewController *selss = [[CSquyuViewController alloc]init];
    selss.myblcok = ^(NSString * _Nonnull idstr, NSMutableArray * _Nonnull muarr) {
        self.phoneAddressLb.text = @"";
        self.addressCode = idstr;
        self.mydata =muarr;
    };
    
    if (self.mydata.count >0) {
        selss.muarr = _mydata;
    }
    selss.islogin = @"login";
    
    [self.navigationController pushViewController:selss animated:YES];
}

-(void)click:(LMCountDownButton *)sender {
    //去除字符串中空格
    _phoneTF.text = [_phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_phoneTF.text]) {
        ShowToastWithText(@"请输入正确电话号码");
        return;
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =_phoneTF.text;
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        [sender startCountDown];
        self.yzmAcode = [dic objectForKey:@"data"];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"注册";
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    self.label1.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;//颜色
    self.label1.layer.borderWidth = 1.0f;//设置边框粗细
    self.label1.layer.masksToBounds = YES;
    self.label2.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;//颜色
    self.label2.layer.borderWidth = 1.0f;//设置边框粗细
    self.label2.layer.masksToBounds = YES;
    self.label3.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;//颜色
    self.label3.layer.borderWidth = 1.0f;//设置边框粗细
    self.label3.layer.masksToBounds = YES;
    
    self.accontBGview.hidden = YES;
    self.mydata = [NSMutableArray arrayWithCapacity:1];
    
    LMCountDownButton *sendCodeButton = [[LMCountDownButton alloc] initWithFrame:_yzmButton.bounds];
    [sendCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [sendCodeButton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    sendCodeButton.titleLabel.font = FontSize(13);
    [sendCodeButton addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [self.yzmButton addSubview:sendCodeButton];
    
    self.phoneTF.delegate = self;
    [self.phoneTF addTarget:self action:@selector(phonetextFieldDidChange:)  forControlEvents:UIControlEventEditingChanged]; //手机号限制11位
    
    self.yzmTF.delegate = self;
    [self.yzmTF addTarget:self action:@selector(vericodetextFieldDidChange:) forControlEvents:UIControlEventEditingChanged]; //验证码限制6位
    
}

- (IBAction)AccontAction:(UIButton *)sender {
    self.accontBGview.hidden = NO;
    self.agreeUplayout.constant = 62;
    self.catego .constant = 66;
    
    [self.accontBUtton setTitleColor:[UIColor colorWithHexString:@"#70BE68"] forState:(UIControlStateNormal)];
    
    if (self.accontBGview.hidden ) {
        KMyLog(@"////////////////\\\\\\\\\\\\\\");
    }
}

- (IBAction)registButton:(UIButton *)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    //去除字符串中空格
    _phoneTF.text = [_phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_phoneTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    
    if (strIsEmpty(_yzmTF.text)) {
        ShowToastWithText(@"请输入验证码");
        return;
    }
    
    if ( self.accontBGview.hidden) {
        //if (![self.yzmTF.text isEqualToString:_yzmAcode]) {
        //    ShowToastWithText(@"验证码错误");
        //    return;
        //}
        //电话注册
        //param[@"phone"] = _phoneTF.text;
        //param[@"account"] = _accTF_tf.text;
        //param[@"name"] = _phoneTF.text;
        //param[@"yzCode"] = _yzmTF.text;
        //param[@"invitationCode"] = _YQMTF.text;
        //param[@"area"] = _addressCode ;
    } else {
        //param[@"phone"] = _accTF_tf.text;
        //param[@"account"] = _accTF_tf.text;
        //param[@"name"] = _accapss_tf.text;
        //param[@"password"] = _accapss_tf.text;
        //param[@"invitationCode"] = _accinv_tf.text;
        //param[@"area"] = [_addressCode substringFromIndex:14];
    }
    
    if (!self.agreebut.selected) {
        ShowToastWithText(@"请阅读并同意用户协议");
        return;
    }
    
    param[@"phone"] = _phoneTF.text;
    param[@"yzCode"] = _yzmTF.text;
    param[@"invitationCode"] = _YQMTF.text;
    
    [NetWorkTool POST:checkPhone param:param success:^(id dic) {
        //KMyLog(@"注册成功____%@",dic);
        //ShowToastWithText(@"注册成功");
        //NSString *toStr = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"data"] objectForKey:@"token"]];
        //[USER_DEFAULT setObject:toStr forKey:@"Token"];
        
        PerfectingInfoViewController *VC = [PerfectingInfoViewController new];
        VC.stepOneDict = param;
        [self.navigationController pushViewController:VC animated:YES];
        //[self.navigationController pushViewController:[CSIdAuthViewController new] animated:YES];
        //[self.navigationController popViewControllerAnimated:YES];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

//手机号码 限制输入11位
- (void)phonetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 11;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

//验证码 限制输入6位
- (void)vericodetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 6;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

//邀请码 不做限制了

/**
 隐私政策
 */
- (IBAction)CSYinSiButton:(UIButton *)sender {
    sender.selected = ! sender.selected ;
}

- (IBAction)agreeright:(UIButton *)sender { //用户协议
    //_agreebut.selected = !_agreebut.selected;
    UserAgreementViewController *VC = [[UserAgreementViewController alloc] init];
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)goLogin:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 sender 发送验证码
 */
- (IBAction)sendYZm:(YZMButton *)sender {
    return;
    //去除字符串中空格
    _phoneTF.text = [_phoneTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_phoneTF.text]) {
        ShowToastWithText(@"请输入正确电话号码");
        return;
    }
    [_yzmButton timeFailBeginFrom:60];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    param[@"phone"] =_phoneTF.text;
    
    [NetWorkTool POST:sendMsg param:param success:^(id dic) {
        KMyLog(@"fasonbgyanzhnegamn%@",dic);
        self.yzmAcode = [dic objectForKey:@"data"];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

//判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

@end
