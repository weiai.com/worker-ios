//
//  CSquyuTableViewCell.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSquyuTableViewCell.h"

@implementation CSquyuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.myimage.userInteractionEnabled = YES;
    [self.editBtn setImage:imgname(@"路径 2314") forState:(UIControlStateNormal)];
    [self.deleteBtn setImage:imgname(@"组 3146") forState:(UIControlStateNormal)];
     
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)mybut:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.myblock) {
        self.myblock(sender.selected);
    }
}

//编辑按钮点击事件处理
- (IBAction)editBtnAction:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(editDataWithCell:)]) {
        [_delegate editDataWithCell:self];
    }
    
}

//删除按钮点击事件处理
- (IBAction)deleteBtnAction:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(deleteDataWithCell:)]) {
        [_delegate deleteDataWithCell:self];
    }
}

@end
