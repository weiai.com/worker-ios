//
//  CSLoginViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/3/15.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSLoginViewController.h"
#import "JPUSHService.h"
#import "DPSendCodeButton.h"
#import "LMCountDownButton.h"
#import "PerfectingInfoViewController.h"

@interface CSLoginViewController ()<JPUSHRegisterDelegate,JPUSHGeofenceDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet YZMButton *yzmButton;
@property (weak, nonatomic) IBOutlet UILabel *upLable;
@property (weak, nonatomic) IBOutlet UITextField *upTF;
@property (weak, nonatomic) IBOutlet UILabel *dowLable;
@property (weak, nonatomic) IBOutlet UITextField *dowTF;

@end

@implementation CSLoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];;
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =@"登录";
    
    //self.upLable.text = [NSString stringWithFormat:@"  账户名"];
    //self.dowLable.text = [NSString stringWithFormat:@"  密码"];
    //self.yzmButton.hidden = YES;
    //
    //self.upLable.text = [NSString stringWithFormat:@"  手机号"];
    //self.dowLable.text = [NSString stringWithFormat:@"  验证码"];
    //self.upTF.placeholder = @"";
    //self.dowTF.placeholder = @"";
    
    self.upLable.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;//颜色
    self.upLable.layer.borderWidth = 1.0f;//设置边框粗细
    self.upLable.layer.masksToBounds = YES;
    
    self.dowLable.layer.borderColor = [UIColor colorWithHexString:@"#DDDBD7"].CGColor;//颜色
    self.dowLable.layer.borderWidth = 1.0f;//设置边框粗细
    self.dowLable.layer.masksToBounds = YES;
    
    self.yzmButton.hidden = NO;
    
    //self.upTF.text = @"18607115683";
    //self.upTF.text = @"15232587888";
    
    LMCountDownButton *sendCodeButton = [[LMCountDownButton alloc] init];
    [sendCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [sendCodeButton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    sendCodeButton.titleLabel.font = FontSize(13);
    sendCodeButton.frame = _yzmButton.bounds;
    [sendCodeButton addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [self.yzmButton addSubview:sendCodeButton];
    
    self.upTF.delegate = self;
    self.upTF.keyboardType = UIKeyboardTypeNumberPad;
    [self.upTF addTarget:self action:@selector(phonetextFieldDidChange:)  forControlEvents:UIControlEventEditingChanged]; //手机号限制11位
    
    self.dowTF.delegate = self;
    self.dowTF.keyboardType = UIKeyboardTypeNumberPad;
    //[self.dowTF addTarget:self action:@selector(vericodetextFieldDidChange:) forControlEvents:UIControlEventEditingChanged]; //验证码限制6位
    
    // Do any additional setup after loading the view from its nib.
}

-(void)click:(LMCountDownButton *)sender {
    //去除字符串中空格
    _upTF.text = [_upTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_upTF.text]) {
        ShowToastWithText(@"请输入正确电话号码");
        return;
    }
    NSDictionary *dic = @{@"phone":_upTF.text};
    
    [NetWorkTool POST:sendMsg param:[dic mutableCopy] success:^(id dic) {
        [sender startCountDown];
        ShowToastWithText(@"验证码发送成功");
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

//手机号码 限制输入11位
- (void)phonetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 11;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

//验证码 限制输入6位
- (void)vericodetextFieldDidChange:(UITextField *)textField {
    NSString *toBeString = textField.text;
    
    //获取高亮部分
    UITextRange *selectedRange = [textField markedTextRange];
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    CGFloat maxLength = 6;
    
    // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
    if (!position){
        if (toBeString.length > maxLength) {
            NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
            if (rangeIndex.length == 1){
                textField.text = [toBeString substringToIndex:maxLength];
            } else {
                NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                textField.text = [toBeString substringWithRange:rangeRange];
            }
        }
    }
}

- (IBAction)registAction:(UIButton *)sender {
    [self.navigationController pushViewController:[LoginViewController new] animated:YES];
}

- (IBAction)forgetAction:(UIButton *)sender {
    [self.navigationController pushViewController:[CSForgetPSViewController new] animated:YES];
}

- (IBAction)sendYZm:(YZMButton *)sender {
    return;
    
    //去除字符串中空格
    _upTF.text = [_upTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_upTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    [_yzmButton timeFailBeginFrom:60];
    
    NSDictionary *dic = @{@"phone":_upTF.text};
    
    [NetWorkTool POST:sendMsg param:[dic mutableCopy] success:^(id dic) {
        
        ShowToastWithText(@"验证码发送成功");
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:NO];
}

- (IBAction)loginBut:(UIButton *)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (_yzmButton.hidden) {
        param[@"account"] = _upTF.text;
        param[@"password"] = _dowTF.text;
        //账号
    } else {
        param[@"phone"] = _upTF.text;
        param[@"yzCode"] = _dowTF.text;
    }
    //去除字符串中空格
    _upTF.text = [_upTF.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (![self isMobileNumberOnly:_upTF.text]) {
        ShowToastWithText(@"请输入正确的手机号");
        return;
    }
    
    if ([_dowTF.text isEqualToString:@""]) {
        ShowToastWithText(@"验证码不能为空");
        return;
    }
    
    [NetWorkTool POST:Login param:param success:^(id dic) {
        KMyLog(@"登录成功%@",dic);
        NSString *toStr = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"data"] objectForKey:@"token"]];
        [USER_DEFAULT setObject:toStr forKey:@"Token"];
        
        [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
            NSString *userID = [[dic objectForKey:@"data"] objectForKey:@"id"];
            
            [USER_DEFAULT setObject:userID forKey:@"user_id"];
            
            NSLog(@"用户id:%@", KUID);
            [JPUSHService setAlias:KUID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                NSLog(@"极光推送设置别名:%ld %@ %ld", (long)iResCode, iAlias, (long)seq);
            } seq:1];
            
            //未完善信息
            if (!dic[@"data"][@"id_card_no"]) {
                PerfectingInfoViewController *VC = [PerfectingInfoViewController new];
                VC.phone = param[@"phone"];
                [self.navigationController pushViewController:VC animated:YES];
            }else {
                [[NSNotificationCenter defaultCenter] postNotificationName:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            
        } other:^(id dic) {
            
        } fail:^(NSError *error) {
            
        } needUser:YES];
        
        //[self dismissViewControllerAnimated:YES completion:nil];
        
    } other:^(id dic) {
        ShowToastWithText(@"登录失败");
    } fail:^(NSError *error) {
        ShowToastWithText(@"登录失败");
    } needUser:NO];
}

///判断是否为电话号码
- (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

@end
