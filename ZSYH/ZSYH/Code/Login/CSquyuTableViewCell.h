//
//  CSquyuTableViewCell.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CSquyuTableViewCellDelegate <NSObject>

-(void)deleteDataWithCell:(UITableViewCell *_Nullable)cell;
-(void)editDataWithCell:(UITableViewCell *_Nonnull)cell;

@end

@interface CSquyuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mycontentLB;
@property (weak, nonatomic) IBOutlet UIImageView *myimage;
@property (weak, nonatomic) IBOutlet UIButton *mybut;
@property(nonatomic,copy)void (^myblock)(BOOL boo);
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (nonatomic, strong) NSIndexPath *indexPath;

@property(assign, nonatomic)id <CSquyuTableViewCellDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
