//
//  CSquyuViewController.m
//  ZSYH
//
//  Created by 主事丫环 on 2019/5/24.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "CSquyuViewController.h"
#import "CSquyuTableViewCell.h"
#import "CSquyumodel.h"
#import "MOFSPickerManager.h"
#import "CSPersonModel.h"
#import "NetLoadAreaPickerViewController.h"

@interface CSquyuViewController ()<UITableViewDelegate,UITableViewDataSource, CSquyuTableViewCellDelegate>
@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *mydateSource;
@property (nonatomic, strong) UILabel *numberLB;
@property (nonatomic, strong) UIView *tabheaderview;
@property (nonatomic, strong) CSPersonModel *model;
@property (nonatomic, strong) NSMutableArray *delearr;
@property (nonatomic, strong) UIView *tabfootview;
@property (nonatomic, assign) BOOL  iscansele;
@property (nonatomic, strong) UIButton *tabheaderbut;
@property (nonatomic, strong) UIView *showbgView;//提示是否取消
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong)CSquyumodel *deleteModel;
@end

@implementation CSquyuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"接单区域";
    self.iscansele = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.leftbutton setImage:imgname(@"left_icon") forState:(UIControlStateNormal)];
    [self.rightbutton setTitleColor:zhutiColor forState:(UIControlStateNormal)];
    if (!strIsEmpty(_islogin)) {
        [self.rightbutton setTitle:@"提交" forState:(UIControlStateNormal)];
    }
    self.mydateSource  = [NSMutableArray arrayWithCapacity:1];
    self.delearr = [NSMutableArray arrayWithCapacity:1];
    
    if (_muarr.count >0) {
        self.mydateSource =_muarr;
        [_myTableView reloadData];
    }
    [self showheader];
    [self showloadingview];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
}
-(void)requestperson{
    self.iscansele = NO;
    self.tabheaderbut.userInteractionEnabled = NO;
    kWeakSelf;
    [NetWorkTool POST:CcentenInfo param:nil success:^(id dic) {
        [self.mydateSource removeAllObjects];
        
        NSArray *atttt = [[dic objectForKey:@"data"] objectForKey:@"area"];
        for (NSMutableDictionary *mu in atttt) {
            CSquyumodel *mdoe = [[CSquyumodel alloc]init];
            [mdoe setValuesForKeysWithDictionary:mu];
            mdoe.idstr = [mu objectForKey:@"id"];
            mdoe.isdelde = @"0";
            [self.mydateSource addObject:mdoe];
        }
        
        NSMutableArray* temp = [[NSMutableArray alloc] init];
        NSMutableArray* tempstr = [[NSMutableArray alloc] init];
        
        for ( CSquyumodel *mdoe in self.mydateSource){
            if (![tempstr containsObject:mdoe.idstr]){
                [tempstr addObject:mdoe.idstr];
                [temp addObject:mdoe];
            }
        }
        self.mydateSource = temp;
        [self.myTableView reloadData];
        
        if (weakSelf.mydateSource.count == 5) {
            weakSelf.myTableView.tableFooterView = [UIView new];
        } else {
            self.myTableView.tableFooterView =  self.tabfootview;
        }
        self.iscansele = YES;
        self.tabheaderbut.userInteractionEnabled = YES;
    } other:^(id dic) {
        self.iscansele = YES;
        
    } fail:^(NSError *error) {
        self.iscansele = YES;
        
    } needUser:YES];
}

#pragma mark - 提交按钮(注册时使用)
- (void)rightClick {
    
    if (_mydateSource.count > 0) {
        NSString *idstr = @"";
        for (CSquyumodel *model in _mydateSource) {
            idstr =[NSString stringWithFormat:@"%@,%@",model.idstr,idstr];
        }
        idstr = [idstr substringToIndex:idstr.length-1];
        
        NSString *str = [NSString stringWithFormat:@"%lu/5", (unsigned long)self.mydateSource.count];
        if (self.commitBlock) {
            self.commitBlock(str, idstr, _mydateSource);
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        ShowToastWithText(@"请至少选择一个接单区域");
    }
}

-(void)showheader{
    UIView *head = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 48)];
    UILabel *contertLb = [[UILabel alloc]initWithFrame:CGRectMake(28, 10, 112, 25)];
    contertLb.textAlignment = NSTextAlignmentCenter;
    contertLb.textColor = [UIColor colorWithHexString:@"#333333"];
    contertLb.font = FontSize(18);
    contertLb.text = [NSString stringWithFormat:@"接单区域数量"];
    [head addSubview:contertLb];
    
    self.numberLB =[[UILabel alloc]initWithFrame:CGRectMake(142, 10, 20, 25)];
    _numberLB.textAlignment = NSTextAlignmentRight;
    _numberLB.textColor = zhutiColor;
    _numberLB.font = FontSize(20);
    _numberLB.text = [NSString stringWithFormat:@"0"];
    [head addSubview:_numberLB];
    
    UILabel *numberLbcon = [[UILabel alloc]initWithFrame:CGRectMake(162, 10, 30, 25)];
    numberLbcon.textColor = [UIColor colorWithHexString:@"#333333"];
    numberLbcon.font = FontSize(20);
    numberLbcon.text = [NSString stringWithFormat:@"/5"];
    [head addSubview:numberLbcon];
    
    self.tabheaderview = head;
    self.myTableView.tableHeaderView =  self.tabheaderview;
    
    UIView *foot = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH + 10, 48)];
    UIImageView *footimage = [[UIImageView alloc]initWithFrame:CGRectMake(22, 0, KWIDTH-44, 48)];
    footimage.userInteractionEnabled = YES;
    footimage.image = imgname(@"selequyufoot");
    UITapGestureRecognizer *get = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addactkion:)];
    [ footimage  addGestureRecognizer:get];
    [foot addSubview:footimage];
    self.tabfootview = foot;
    
    if (self.mydateSource.count == 5) {
        self.myTableView.tableFooterView = [UIView new];
    }else{
        self.myTableView.tableFooterView =  self.tabfootview;
    }
}

#pragma mark - 添加接单区域
//type:1 添加 2 编辑 3 删除
-(void)changeAreaWithType:(NSInteger)type {
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *idstr = @"";
    
    for (CSquyumodel *model in _mydateSource) {
        if ([model.isdelde integerValue] == 0) {
            
            idstr =[NSString stringWithFormat:@"%@,%@",idstr,model.idstr];
        } 
    }
    
    idstr = [idstr substringFromIndex:1];
    NSLog(@"字符串的结果是 %@", idstr);
    NSArray  *array = [idstr componentsSeparatedByString:@","];
    NSLog(@"字符串转数组 %@", array);
    
    param[@"area"] = idstr;
    if (idstr.length < 3) {//删除全部区域时,设置为null
        param[@"area"] = @"null";
    } else {
        param[@"area"] = idstr;
    }
    [NetWorkTool POST:setUserInfo param:param success:^(id dic) {
        KMyLog(@"");
        if (type == 1) {
            ShowToastWithText(@"添加成功");
        } else if (type == 2) {
            ShowToastWithText(@"编辑成功");
        }
        [self requestperson];
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}

#pragma mark - 添加按钮
-(void)addactkion:(UITapGestureRecognizer *)but{
    if (_mydateSource.count == 5) {
        self.myTableView.tableFooterView = [UIView new];
        return;
    }
    NetLoadAreaPickerViewController * loadAreaVC = [[NetLoadAreaPickerViewController alloc]init];
    kWeakSelf;
    loadAreaVC.selectedBlock = ^(NetLoadAreaModel *areaModel) {
        
        CSquyumodel *model = [[CSquyumodel alloc]init];
        model.area = [NSString stringWithFormat:@"%@%@%@", areaModel.provinceName, areaModel.cityName, areaModel.districtName];
        model.idstr = areaModel.districtCode;
        model.isdelde = @"0";
        
        for (CSquyumodel *model4 in weakSelf.mydateSource) {
            if ([model4.idstr isEqualToString: model.idstr]) {
                ShowToastWithText(@"重复添加");
                return ;
            }
        }
        
        [weakSelf.mydateSource insertObject:model atIndex:0];
        
        if (!strIsEmpty(weakSelf.islogin)) {//如果是注册界面过来的
            [weakSelf.myTableView reloadData];
            if (weakSelf.mydateSource.count == 5) {
                weakSelf.myTableView.tableFooterView = [UIView new];
            }
            return ;
        }
        //个人信息界面过来的,添加接单区域
        [self changeAreaWithType:1];
    };
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        loadAreaVC.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    }else{
        loadAreaVC.modalPresentationStyle=UIModalPresentationCurrentContext;
    }
    [self presentViewController:loadAreaVC animated:NO completion:^{
    }];
    return;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mydateSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CSquyuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CSquyuTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CSquyumodel *model = [_mydateSource safeObjectAtIndex:indexPath.row];
    cell.mycontentLB.text = model.area;
    cell.indexPath = indexPath;
    cell.delegate = self;
    
    self.numberLB.text = [NSString stringWithFormat:@"%lu",(unsigned long)_mydateSource.count];
  
    return cell;
}

#pragma mark ===CSquyuTableViewCellDelegate====
#pragma mark - 删除接单区域
-(void)deleteDataWithCell:(UITableViewCell *_Nullable)cell{
    
    if (self.mydateSource.count == 1) {
        ShowToastWithText(@"至少保留一个接单区域");
        return;
    }
    CSquyuTableViewCell *quYuCell = (CSquyuTableViewCell *)cell;
    NSIndexPath *indexpath = quYuCell.indexPath;
    _deleteModel = [self.mydateSource safeObjectAtIndex:indexpath.row];
    [[UIApplication sharedApplication].keyWindow addSubview:_showbgView];
}
-(void)yescaozuoActionWithModel:(UIButton *)but {
    KMyLog(@"确定");
    if (self.mydateSource.count == 1) {
        ShowToastWithText(@"不能删除最后一个接单区域");
        return;
    }

    //删除数据
    [self deleteDateWithModel:_deleteModel];
    [_showbgView removeFromSuperview];
    
    
}
#pragma mark - 编辑接单区域
-(void)editDataWithCell:(UITableViewCell *_Nonnull)cell {
    
    NSIndexPath *indexPath = [self.myTableView indexPathForCell:cell];
    NetLoadAreaPickerViewController * loadAreaVC = [[NetLoadAreaPickerViewController alloc]init];
    kWeakSelf;
    loadAreaVC.selectedBlock = ^(NetLoadAreaModel *areaModel) {
        
        CSquyumodel *model = [[CSquyumodel alloc]init];
        model.area = [NSString stringWithFormat:@"%@%@%@", areaModel.provinceName, areaModel.cityName, areaModel.districtName];
        model.idstr = areaModel.districtCode;
        model.isdelde = @"0";
        
        for (CSquyumodel *model4 in self.mydateSource) {
            if ([model4.idstr isEqualToString: model.idstr]) {
                ShowToastWithText(@"重复添加");
                return ;
            }
        }
        
        [self.mydateSource removeObjectAtIndex:indexPath.row];
        [weakSelf.mydateSource insertObject:model atIndex:indexPath.row];
        if (!strIsEmpty(weakSelf.islogin)) {//如果是注册界面过来的
            [weakSelf.myTableView reloadData];
            return ;
        }
        //2 编辑
        [self changeAreaWithType:2];
    };
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0) {
        loadAreaVC.modalPresentationStyle=UIModalPresentationOverCurrentContext;
    }else{
        loadAreaVC.modalPresentationStyle=UIModalPresentationCurrentContext;
    }
    [self presentViewController:loadAreaVC animated:NO completion:^{
    }];
}
-(void)quxiaocaozuoAction:(UIButton *)but{
    KMyLog(@"取消");
    [self.showbgView removeFromSuperview];
}


#pragma mark - 删除数据
-(void)deleteDateWithModel:(CSquyumodel *)model{
    //请求删除接口
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    NSString *idstr = @"";
    int deleteIndex = -1;//删除的下标
    for (int i = 0; i < self.mydateSource.count; i++) {
        CSquyumodel *orignalModel = self.mydateSource[i];
        if ([orignalModel.area isEqualToString:model.area]) {
            deleteIndex = i;
        }
    }
    if (deleteIndex == -1) {//没有查到删除区域
        return;
    }
    [self.mydateSource removeObjectAtIndex:deleteIndex];
    if (!strIsEmpty(self.islogin)) {//从注册界面过来的
        [self.myTableView reloadData];
        return ;
    }
    //从个人信息界面过来
    for (CSquyumodel *selectedModel in self.mydateSource) {
        if ([selectedModel.idstr isEqualToString:model.idstr]) {
            
        } else {
            idstr = [NSString stringWithFormat:@"%@,%@", idstr,selectedModel.idstr];
        }
    }
    
    if (idstr.length > 0) {//去掉逗号
        idstr = [idstr substringFromIndex:1];
    }
    NSLog(@"字符串的结果是 %@", idstr);
    if (idstr.length < 3) {
        param[@"area"] = @"null";
    } else {
        param[@"area"] = idstr;
    }
    [NetWorkTool POST:setUserInfo param:param success:^(id dic) {
        ShowToastWithText(@"删除成功");
        
        [self requestperson];
        
    } other:^(id dic) {
        
    } fail:^(NSError *error) {
        
    } needUser:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"您点击了第 %ld 个分区第 %ld 行",indexPath.section, indexPath.row);
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_muarr) {
        return NO;
    }
    return YES;
}

// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSInteger ind =indexPath.row;
        [_mydateSource removeObjectAtIndex:ind];
        [self.myTableView reloadData];
        self.myTableView.tableFooterView = self.tabfootview;
    }
}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

-(UITableView *)myTableView{
    if (!_myTableView) {
        _myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNaviHeight, KWIDTH, KHEIGHT-kNaviHeight) style:(UITableViewStylePlain)];
        _myTableView.backgroundColor = [UIColor clearColor];
        _myTableView.delegate = self;
        _myTableView.dataSource = self;
        _myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        _myTableView.estimatedRowHeight = 90;
        _myTableView.rowHeight = UITableViewAutomaticDimension;
        _myTableView.tableFooterView = [UIView new];
        //_myTableView.placeHolderView = [[KKTableViewNoDataView alloc]initWithFrame:_myTableView.bounds image:imgname(@"tableviewPlaseholder") viewClick:^{
        //[_myTableView.mj_header beginRefreshing];
        //}];
        [_myTableView registerNib:[UINib nibWithNibName:@"CSquyuTableViewCell" bundle:nil] forCellReuseIdentifier:@"CSquyuTableViewCell"];
        adjustInset(_myTableView);
        [self.view addSubview:_myTableView];
    }
    return _myTableView;
}

-(void)showloadingview{
    
    self.showbgView = [[UIView alloc]init];
    self.showbgView.frame = self.view.bounds;
    self.showbgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.showbgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.centerX.offset(0);
        make.height.offset(239);
        make.width.offset(KWIDTH - 44);
    }];
    
    UILabel *titile = [[UILabel alloc]init];
    titile.font = FontSize(16);
    titile.textColor = [UIColor colorWithHexString:@"#333333"];
    titile.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:titile];
    
    UILabel *msglb = [[UILabel alloc]init];
    msglb.font = FontSize(16);
    msglb.text = @"确定删除该接单区域？";
    msglb.textColor = [UIColor colorWithHexString:@"#333333"];
    msglb.textAlignment = NSTextAlignmentCenter;
    [whiteBGView addSubview:msglb];
    
    [titile mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(17);
        make.top.offset(20);
    }];
    
    [msglb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.height.offset(20);
        make.top.offset(87);
    }];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"取消" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 2;
    [iKnowBut addTarget:self action:@selector(quxiaocaozuoAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    
    UIButton *yesBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [yesBut setTitle:@"确定" forState:(UIControlStateNormal)];
    [yesBut setTitleColor:K999999 forState:(UIControlStateNormal)];
    [yesBut setBackgroundColor:[UIColor colorWithHexString:@"#F2F2F2"]];
    yesBut.layer.masksToBounds = YES;
    yesBut.layer.cornerRadius = 2;
    [yesBut addTarget:self action:@selector(yescaozuoActionWithModel:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:yesBut];
    
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.offset(45);
        make.width.offset(113);
        make.bottom.offset(-52);
    }];
    
    [yesBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.right.offset(-45);
        make.width.offset(113);
        make.bottom.offset(-52);
    }];
}



@end
