//
//  ZLableTag.m
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "ZLableTag.h"

@implementation ZLableTag

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.isSelected = NO;
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)setupWithText:(NSString*)text {
    
    self.text = text;
    self.font = [UIFont systemFontOfSize:12];
    UIFont* font = self.font;
    
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: font}];
    
    CGRect frame = self.frame;
    frame.size = CGSizeMake(size.width + 20, size.height + 10);
    
    self.frame = frame;
    
    self.layer.cornerRadius = 2;
    self.layer.borderColor = self.textColor.CGColor;
    self.layer.borderWidth = 1.0;
    
}
- (void)setupWithYellowText:(NSString *)text{
    self.text = text;
    self.font = [UIFont boldSystemFontOfSize:14];
    self.textColor = kColorWithHex(0xff7950);
    self.backgroundColor = [kColorWithHex(0xfff378) colorWithAlphaComponent:0.4];
    self.layer.cornerRadius = 2;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = kColorWithHex(0xca9f61).CGColor;
    self.layer.borderWidth = 1;
    
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    CGRect frame = self.frame;
    frame.size = CGSizeMake(size.width + 20, size.height + 10);
    self.frame = frame;
}
- (void)setIsSelected:(BOOL)isSelected{
    if (isSelected) {
        self.layer.borderColor = kColorWithHex(0x70be68).CGColor;
        self.textColor = kColorWithHex(0x70be68);
    }else{
        self.layer.borderColor = kColorWithHex(0xc1c1c1).CGColor;
        self.textColor = kColorWithHex(0xc1c1c1);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
