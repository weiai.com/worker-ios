//
//  ZTagListView.m
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "ZTagListView.h"
#import "ZLableTag.h"

@interface ZTagListView ()
@property (nonatomic, strong) NSMutableArray *tagArr;//存储lblTag数组
@property (nonatomic, strong) ZLableTag *lblSelected;//被选中的标签
@end

@implementation ZTagListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.bounces = NO;
        self.pagingEnabled = YES;
    }
    return self;
}
- (void)setupSubviewWithTitles:(NSArray *)titleArr{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self.tagArr removeAllObjects];
    
    for (NSInteger i = 0; i < titleArr.count; i++) {
        ZLableTag *lblTag = [[ZLableTag alloc] initWithFrame:CGRectZero];
        lblTag.textColor = kColorWithHex(0xc1c1c1);
        [lblTag setupWithText:[titleArr objectAtIndexSafe:i]];
        [self addSubview:lblTag];
        [self.tagArr addObject:lblTag];
        lblTag.userInteractionEnabled = YES;
        lblTag.tag = i + 1000;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblTagSelectedActin:)];
        [lblTag addGestureRecognizer:tap];
        //防止下标为0的时候直接被选中
        if (self.selectedIndex - 1 == i && self.selectedIndex > 0) {
            lblTag.isSelected = YES;
            self.lblSelected = lblTag;
        }
    }
    [self setupAllSubviews];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    [self setupAllSubviews];
}
#pragma mark - 交互实现
- (void)lblTagSelectedActin:(UITapGestureRecognizer *)tap{
    ZLableTag *lblTag = (ZLableTag *)tap.view;
    if (self.tag_delegate && [self.tag_delegate respondsToSelector:@selector(tagListView:didSelectedTagAtIndex:selectedContent:)]) {
        [self.tag_delegate tagListView:self didSelectedTagAtIndex:lblTag.tag - 1000 selectedContent:lblTag.text];
        self.lblSelected.isSelected = NO;
        lblTag.isSelected = YES;
        self.lblSelected = lblTag;
    }
}
#pragma mark - 布局实现
- (void)setupAllSubviews{
    CGFloat marginX = 10;
    CGFloat marginY = 10;
    
    __block CGFloat x = 0;
    __block CGFloat y = 10;
    
    [self.tagArr enumerateObjectsUsingBlock:^(ZLableTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        
        CGFloat height = CGRectGetHeight(obj.frame);
        
        if (idx == 0) {
            x = marginX;
        }else {
            x = CGRectGetMaxX([self.tagArr[idx - 1] frame]) + marginX;
            if ( x + CGRectGetWidth(obj.frame) + marginX > CGRectGetWidth(self.frame) ) {
                x = marginX;
                y += height;
                y += marginY;
            }
        }
        CGRect frame = obj.frame;
        frame.origin = CGPointMake(x, y);
        obj.frame = frame;
        
    }];
    
    // 如果只有一行，居中显示
    if (y == 10) {
        
        [self.tagArr enumerateObjectsUsingBlock:^(ZLableTag *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            CGFloat height = CGRectGetHeight(obj.frame);
            y = CGRectGetHeight(self.frame) / 2 - height / 2.0;
            
            if (idx == 0) {
                x = marginX;
            }else {
                x = CGRectGetMaxX([self.tagArr[idx - 1] frame]) + marginX;
            }
            CGRect frame = obj.frame;
            frame.origin = CGPointMake(x, y);
            obj.frame = frame;
            
        }];
        
    }
    
    CGFloat contentHeight = CGRectGetMaxY([self.tagArr.lastObject frame]) + 1;
    if (contentHeight < CGRectGetHeight(self.frame)) {
        contentHeight = 0;
    }
    
    self.contentSize = CGSizeMake(0, contentHeight);
}
#pragma mark - 懒加载
- (NSMutableArray *)tagArr{
    if (!_tagArr) {
        _tagArr = [NSMutableArray array];
    }
    return _tagArr;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
