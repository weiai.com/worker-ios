//
//  ZLableTag.h
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLableTag : UILabel
@property (nonatomic,assign)BOOL isSelected;
@property (nonatomic, strong) UIColor *selectedColor;

- (void)setupWithText:(NSString*)text;

//创建黄色文字标签
- (void)setupWithYellowText:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
