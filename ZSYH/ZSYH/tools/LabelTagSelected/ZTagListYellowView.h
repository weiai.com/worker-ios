//
//  ZTagListYellowView.h
//  DistributorAB
//
//  Created by LZY on 2019/10/18.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZTagListYellowView : UIScrollView
- (void)setupSubviewWithTitles:(NSArray *)titleArr;
@property (nonatomic, assign) CGFloat viewHeight;
@end

NS_ASSUME_NONNULL_END
