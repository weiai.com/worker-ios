//
//  ZTagListView.h
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZTagListView;

@protocol ZTagListViewDelegate <NSObject>
//选中标签代理
- (void)tagListView:(ZTagListView *_Nullable)tagListView didSelectedTagAtIndex:(NSInteger)index selectedContent:(NSString *_Nonnull)content;
@end
NS_ASSUME_NONNULL_BEGIN

@interface ZTagListView : UIScrollView
@property (nonatomic, assign) NSInteger selectedIndex;//选中的下标(赋值的时候+1一下)
@property (nonatomic, weak) id <ZTagListViewDelegate> tag_delegate;
- (void)setupSubviewWithTitles:(NSArray *)titleArr;
@end

NS_ASSUME_NONNULL_END
