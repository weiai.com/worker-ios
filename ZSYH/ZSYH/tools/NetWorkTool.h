//
//  NetWorkTool.h
//  AiBao
//
//  Created by Mac on 2018/1/10.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NetWorkTool : NSObject

+ (void)GET:(NSString *)url param:(id)param success:(void (^)(id dic))success other:(void (^)(id dic))reason fail:(void (^)(NSError *error))fail needUser:(BOOL)need;


+ (void)POST:(NSString *)url param:(id)param success:(void (^)(id dic))success other:(void (^)(id dic))reason fail:(void (^)(NSError *error))fail needUser:(BOOL)need;


+ (void)POST:(NSString *)url parameters:(id)parameters progress:(void (^)(id progress))downloadProgress complation:(void (^)(id dic))complaction failure:(void (^)(NSError *error))failure;

//多张图片上传 接口
+ (void)UploadPicWithUrl:(NSString *)url param:(NSDictionary *)params key:(NSString *)key image:(id)image withSuccess:(void (^)(id dic))success failure:(void (^)(NSError *error))failure;
/**
 上传多张 指定名字 图片的接口
 */
+ (void)UploadPicWithUrl:(NSString *)url param:(NSDictionary *)params keys:(NSArray <NSString *>*)keys images:(NSArray <UIImage *>*)images withSuccess:(void (^)(id dic))success failure:(void (^)(NSError *error))failure;
/**
 注册上传图片接口
 */
+ (void)registerUploadPicWithUrl:(NSString *)url param:(NSDictionary *)params key:(NSString *)key image:(id)image withSuccess:(void (^)(id dic))success failure:(void (^)(NSError *error))failure;

+ (void)clickLogin;

+ (void)showOnleText:(NSString *)text delay:(NSTimeInterval)delay;


@end





