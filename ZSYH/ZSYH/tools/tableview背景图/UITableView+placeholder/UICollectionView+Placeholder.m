//
//  UICollectionView+Placeholder.m
//  ZengBei
//
//  Created by 魏堰青 on 2019/1/26.
//  Copyright © 2019 WYX. All rights reserved.
//

#import "UICollectionView+Placeholder.h"
#import <objc/runtime.h>




@implementation NSObject (swizzle)
+ (void)swizzleInstanceSelector:(SEL)originalSel
           WithSwizzledSelector:(SEL)swizzledSel {
    
    Method originMethod = class_getInstanceMethod(self, originalSel);
    Method swizzedMehtod = class_getInstanceMethod(self, swizzledSel);
    BOOL methodAdded = class_addMethod(self, originalSel, method_getImplementation(swizzedMehtod), method_getTypeEncoding(swizzedMehtod));
    
    if (methodAdded) {
        class_replaceMethod(self, swizzledSel, method_getImplementation(originMethod), method_getTypeEncoding(originMethod));
    }else{
        method_exchangeImplementations(originMethod, swizzedMehtod);
    }
}

@end
@implementation UICollectionView (Placeholder)
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzleInstanceSelector:@selector(reloadData) WithSwizzledSelector:@selector(kk_reloadData)];
    });
}

- (void)setPlaceHolderView:(UIView *)placeHolderView {
    objc_setAssociatedObject(self, @selector(placeHolderView), placeHolderView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)placeHolderView {
    return objc_getAssociatedObject(self, @selector(placeHolderView));
}


- (void)kk_reloadData {
    [self kk_checkEmpty];
    [self kk_reloadData];
}


- (void)kk_checkEmpty {
    BOOL isEmpty = YES;
    id<UICollectionViewDataSource> src = self.dataSource;
    NSInteger sections = 0;
    if ([src respondsToSelector:@selector(numberOfSectionsInTableView:)]) {
        sections = [src numberOfSectionsInCollectionView:self];
    }
    for (int i = 0; i < sections; i++) {
        NSInteger rows = [src collectionView:self numberOfItemsInSection:i];
        if (rows) {
            isEmpty = NO;
        }
    }
    if (isEmpty) {
        [self.placeHolderView removeFromSuperview];
        [self addSubview:self.placeHolderView];
    }else{
        [self.placeHolderView removeFromSuperview];
    }
    
}


@end
