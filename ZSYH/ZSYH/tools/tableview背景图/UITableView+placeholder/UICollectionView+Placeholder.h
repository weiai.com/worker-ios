//
//  UICollectionView+Placeholder.h
//  ZengBei
//
//  Created by 魏堰青 on 2019/1/26.
//  Copyright © 2019 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionView (Placeholder)
@property (nonatomic, strong) UIView *placeHolderView;

@end

NS_ASSUME_NONNULL_END
