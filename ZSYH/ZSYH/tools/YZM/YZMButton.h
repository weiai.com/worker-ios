//
//  YZMButton.h
//  ZhuanZhuan
//
//  Created by apple on 2017/3/27.
//  Copyright © 2017年 server. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YZMButton : UIButton
// 由于有些时间需求不同，特意露出方法，倒计时时间次数

- (void)timeFailBeginFrom:(NSInteger)timeCount;

@end
