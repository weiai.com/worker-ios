//
//  WXCollectionView.m
//  NanYangParty
//
//  Created by wanshangwl on 2018/5/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import "WXCollectionView.h"
#import "WXCollectionCell.h"

@interface WXCollectionView ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSArray *titles;
@property (nonatomic,strong) NSArray *models;


@property (nonatomic,  copy) void(^select)(NSInteger index);
@end

static NSString *const cellId = @"WXCollectionCell";

@implementation WXCollectionView

- (instancetype)initWithFrame:(CGRect)frame img:(NSArray *)imgs title:(NSArray *)titles block:(void (^)(NSInteger))select {
    self = [super initWithFrame:frame];
    if (self) {
        
        _imgs = imgs;
        _titles = titles;
        _select = select;
        
        [self collectionArr:titles];
    }
    return self;
}




- (void)collectionArr:(NSArray *)array {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    
//    CGFloat w = array.count > 5?(self.width/(array.count%2==0?(array.count/2):((array.count/2)+1))):(self.width/array.count);
    
    

    NSInteger ind = array.count /4 + 0 ;

    CGFloat h = array.count > 4?(self.height/ind):self.height;
    if (array.count <= 4) {
        layout.itemSize = CGSizeMake(self.width/_number, self.height);
        
    }else {
    CGFloat w = self.width/4;

        layout.itemSize = CGSizeMake(w, self.height/ind);
    }
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    _collectionView = [[UICollectionView alloc]initWithFrame:self.bounds collectionViewLayout:layout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerNib:[UINib nibWithNibName:cellId bundle:nil] forCellWithReuseIdentifier:cellId];
    [self addSubview:_collectionView];
    [_collectionView reloadData];
}


#pragma mark - 列表代理 -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (_models) {
        return _models.count;
    }
    return _titles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WXCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    cell.title.textAlignment = NSTextAlignmentCenter;

        cell.title.text = _titles[indexPath.row];
        NSString *imgs = _imgs[indexPath.row];
        cell.pic.image = imgname(imgs);
    
    
    if (_labColor) {
        cell.title.textColor = _labColor;
    }
    if (_labFont) {
        cell.title.font = [UIFont systemFontOfSize:_labFont];
    }
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:( NSIndexPath *)indexPath {
    if (self.select) {
        self.select(indexPath.row);
    }
    
}


- (void)setLabColor:(UIColor *)labColor {
    _labColor = labColor;
    [_collectionView reloadData];
}


- (void)setLabFont:(CGFloat)labFont {
    _labFont = labFont;
    [_collectionView reloadData];
}


- (void)setImgs:(NSArray *)imgs {
    _imgs = imgs;
    [_collectionView reloadData];
}
@end
