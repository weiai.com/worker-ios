//
//  WXCollectionCell.h
//  NanYangParty
//
//  Created by wanshangwl on 2018/5/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WXCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *pic;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
