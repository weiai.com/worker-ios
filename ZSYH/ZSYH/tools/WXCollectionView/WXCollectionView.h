//
//  WXCollectionView.h
//  NanYangParty
//
//  Created by wanshangwl on 2018/5/24.
//  Copyright © 2018年 WYX. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ColumnListModel.h"

@interface WXCollectionView : UIView

- (instancetype)initWithFrame:(CGRect)frame img:(NSArray *)imgs title:(NSArray *)titles block:(void(^)(NSInteger index))select;



@property (nonatomic,strong) UIColor *labColor;
@property (nonatomic,strong) NSArray *imgs;
@property (nonatomic,assign) CGFloat labFont;
@property (nonatomic,assign) NSInteger number;

@end
