//
//  PayOrderTool.h
//  AiBao
//
//  Created by wanshangwl on 2018/3/31.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayOrderTool : NSObject

//支付宝支付
+ (void)aliPay:(NSString *)orderstr;
//微信支付
+ (void)wxPay:(NSDictionary *)dict;

@end
