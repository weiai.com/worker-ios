//
//  singlTool.m
//  APPStart
//
//  Created by 魏堰青 on 2018/4/12.
//  Copyright © 2018年 WQ. All rights reserved.
//

#import "singlTool.h"

@implementation singlTool
static id _instace = nil;


+ (id)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{   //onceToken是GCD用来记录是否执行过 ，如果已经执行过就不再执行(保证执行一次）
        _instace = [super allocWithZone:zone];
    });
    return _instace;
}
+ (instancetype)shareSingTool
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instace = [[self alloc] init];
    });
    return _instace;
}

- (id)copyWithZone:(NSZone *)zone
{
    return _instace;
}
-(NSString *)StringAndData:(NSString *) str{
    
    long long time = [str longLongValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    //1:创建日期转换器  (实例化一个NSDateFormatter对象)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //2:设置日期转换的时区
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    //3:设置日期转换的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *temDateStr = [dateFormatter stringFromDate:date];
    return temDateStr;
    
    
}
//获取当前时间戳

- (NSString *)currentTimeStr{
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

//+ (instancetype)shareSingTool{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _instace = [[self alloc]init];
//    });
//    return _instace;
//}
- (void)propertyCodeWithDictionary:(NSDictionary *)dict
{
    
    NSMutableString *strM = [NSMutableString string];
    
    [dict enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSString *str;
        
        NSLog(@"%@",[obj class]);
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFString")] || [obj isKindOfClass:NSClassFromString(@"NSTaggedPointerString")] || [obj isKindOfClass:NSClassFromString(@"__NSCFConstantString")]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, copy) NSString *%@;",key];
        }
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFNumber")]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, assign) NSInteger %@;",key];
        }
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFArray")]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, copy) NSArray *%@;",key];
        }
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFDictionary")]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, copy) NSDictionary *%@;",key];
        }
        if ([obj isKindOfClass:NSClassFromString(@"__NSCFBoolean")]) {
            str = [NSString stringWithFormat:@"@property (nonatomic, assign) BOOL %@;",key];
        }
        
        [strM appendFormat:@"\n%@\n",str];
    }];
    
    NSLog(@"%@",strM);
}


@end
