//
//  ZYBuyNowShopDetailImageCycleView.h
//  ZSYH
//
//  Created by 主事丫环 on 2019/10/10.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ZYBuyNowShopDetailImageCycleView;

@protocol ZYBuyNowShopDetailImageCycleViewDataSource <NSObject>
//轮播图个数
- (NSInteger)numberOfImagesForView:(ZYBuyNowShopDetailImageCycleView *)cycleView;
//轮播图填充
- (UIView *)cycleView:(ZYBuyNowShopDetailImageCycleView *)cycleView imageAtIndex:(NSInteger)index;
@end

@protocol ZYBuyNowShopDetailImageCycleViewDelegate <NSObject>
//轮播图点击事件
- (void)cycleView:(ZYBuyNowShopDetailImageCycleView *)cycleView didSelectedAtIndex:(NSInteger)index;
@end

@interface ZYBuyNowShopDetailImageCycleView : UIView
@property (nonatomic, weak) id <ZYBuyNowShopDetailImageCycleViewDataSource> dataSource;
@property (nonatomic, weak) id <ZYBuyNowShopDetailImageCycleViewDelegate> delegate;
- (instancetype)initWithFrame:(CGRect)frame withTimerValue:(CGFloat)timerValue;
- (void)reloadData;

@end

NS_ASSUME_NONNULL_END
