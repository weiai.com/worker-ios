//
//  StarEvaluator.h
//  StarEvaluator
//
//  Created by Mac on 16/4/27.
//  Copyright © 2016年 jyb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StarEvaluator;
@protocol StarEvaluatorDelegate <NSObject>

@optional
- (void)starEvaluator:(StarEvaluator *)evaluator currentValue:(float)value;

@end

@interface StarEvaluator : UIControl

@property (assign, nonatomic) BOOL animate;
@property (assign, nonatomic) float gardernumber;

@property (assign, nonatomic) BOOL isShow;//是展示星星 还是评价星星
@property (assign, nonatomic) BOOL iszheng;//是否显示小数

@property (assign, nonatomic) float BGViewWidth;//试图的宽
@property (assign, nonatomic) float widthDistence;//星星之间的距离;

- (void)loadSubviews;


@property (assign, nonatomic) id<StarEvaluatorDelegate>delegate;

@end
