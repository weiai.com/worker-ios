//
//  CancelOrderView.m
//  AiBao
//
//  Created by wanshangwl on 2018/4/2.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "CancelOrderView.h"
#import "WXPayView.h"
//@interface CancelOrderView()
//@property(nonatomic,strong)NSArray *titles;
//@property(nonatomic,strong)NSArray *images;
//@property(nonatomic,assign)NSInteger index;
//@end


@implementation CancelOrderView

- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)titles block:(void(^)(NSString *type))select {
    self = [super initWithFrame:frame];
    if (self) {
        UIView *backV = [[UIView alloc]initWithFrame:self.bounds];
        backV.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
        [backV whenTapped:^{
            [self removeFromSuperview];
        }];
        [self addSubview:backV];
        _myBlock = select;
        CGFloat height = titles.count*40+100;
        UIView *content = [[UIView alloc]initWithFrame:CGRectMake(self.width/2-130, self.height/2-height/2, 260, height)];
        content.backgroundColor = zhutiColor;
        content.layer.cornerRadius = 4;
        content.layer.masksToBounds = YES;
        [self addSubview:content];
        
        _type = titles[0];
        
        UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, content.width, 49)];
        msg.font = FontSize(16);
        msg.backgroundColor = [UIColor whiteColor];
        msg.textAlignment = NSTextAlignmentCenter;
        msg.textColor = [UIColor colorWithHexString:@"#333333"];
        msg.text = @"请选择取消订单的理由";
        [content addSubview:msg];
        kWeakSelf;
        
        WXPayView *pay = [[WXPayView alloc]initWithFrame:CGRectMake(-20, 50, content.width+20, titles.count*40) array:titles image:@[] block:^(NSInteger index) {
            weakSelf.type = titles[index];
        }];
        [content addSubview:pay];
        
        for (int i = 0; i < 2; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = [UIFont systemFontOfSize:15];
            btn.frame = CGRectMake(i*content.width/2, pay.bottom, content.width/2, 50);
            [btn setTitleColor:i==0?[UIColor darkGrayColor]:zhutiColor forState:UIControlStateNormal];
            [btn setTitle:i==0?@"取消":@"确定" forState:UIControlStateNormal];
            btn.tag = i+786;
            [btn addTarget:self action:@selector(butdddd:) forControlEvents:(UIControlEventTouchUpInside)];
            
//            [[btn rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
//                if (i==0) {
//                    [self removeFromSuperview];
//                }else {
//                    select(_type);
//                }
//            }];
            btn.backgroundColor = [UIColor whiteColor];
            [content addSubview:btn];
        }
    }
    return self;
}
-(void)butdddd:(UIButton *)but{
    
    NSInteger ind = but.tag - 786;
    if (ind==0) {
        [self removeFromSuperview];
    }else {
        [self removeFromSuperview];

    if (self.myBlock) {
        self.myBlock(_type);
        }
}
    
}
@end
