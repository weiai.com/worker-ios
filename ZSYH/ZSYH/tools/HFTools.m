//
//  HFTools.m
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/15.
//  Copyright © 2018 WQ. All rights reserved.
//

#import "HFTools.h"

@implementation HFTools

#pragma mark +++获取图片高度
+ (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth {
    
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = defineWidth;
    CGFloat targetHeight = height / (width / targetWidth);
    CGSize size = CGSizeMake(targetWidth, targetHeight);
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        } else {
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - 字符串替换成*
+ (NSString *)replaceStringWithString:(NSString *)str Asterisk:(NSInteger)startLocation length:(NSInteger)length {
    NSString *replaceStr = str;
    for (NSInteger i = 0; i < length; i++) {
        NSRange range = NSMakeRange(startLocation, 1);
        replaceStr = [replaceStr stringByReplacingCharactersInRange:range withString:@"*"];
        startLocation ++;
    }
    return replaceStr;
}

#pragma mark json字符串转字典
+ (NSDictionary *)dictWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

#pragma mark 接口添加公共参数 加密
+ (NSDictionary*)addCommonParams:(NSDictionary*)dic {
    NSMutableDictionary *dic_append = [NSMutableDictionary dictionaryWithDictionary:dic];
//    [dic_append setObject:KUID forKey:@"user_id"];
//    [dic_append setObject:KTKID forKey:@"token"];
//    [dic_append setObject:KTKID forKey:@"timestamp"];
//    [dic_append setObject:[[KUID stringByAppendingString:KTKID] MD5] forKey:@"sign"];
    
    dic_append[@"user_id"] = KUID;
    dic_append[@"token"] = KTKID;
    NSLog(@"______KUID_______%@", KUID);
    NSString *str =  [NSString stringWithFormat:@"%@",[[singlTool shareSingTool] currentTimeStr]] ;
    str = [str substringToIndex:10];
    dic_append[@"timestamp"] = str;
    dic_append[@"sign"] = [[NSString stringWithFormat:@"%@%@%@",KUID,KTKID,str]MD5];
    return dic_append;
}

#pragma mark 可传参数 字符串字典 使用&拼接 转 字符串
+ (NSString*)securityKey:(NSDictionary*)aDic exclude:(NSString *)key {
    
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:aDic];
    if (key) {
        [muDic removeObjectForKey:key];
    }
    NSArray *array = muDic.allKeys;
    NSArray *sortedArray = [array sortedArrayUsingSelector:@selector(compare:)];
    NSMutableString *str_append = [NSMutableString stringWithFormat:@""];
    for (int i=0; i<sortedArray.count; i++) {
        [str_append appendString:[NSString stringWithFormat:@"%@",[sortedArray objectAtIndex:i]]];
        [str_append appendString:[NSString stringWithFormat:@"=%@",[muDic objectForKey:[sortedArray objectAtIndex:i]]]];
        [str_append appendString:@"&"];
    }
    NSString *secStr = [self removeLastOneChar:str_append];
    NSLog(@"_________________%@",secStr);
    return secStr;
}

#pragma mark 字符串字典 使用&拼接 转 字符串
+ (NSString*)returnStringWithDic:(NSDictionary*)aDic {
    
    NSMutableDictionary *muDic = [NSMutableDictionary dictionaryWithDictionary:aDic];
    NSArray *array = muDic.allKeys;
    NSMutableString *str_append = [NSMutableString stringWithFormat:@""];
    for (int i=0; i<array.count; i++) {
        [str_append appendString:[NSString stringWithFormat:@"%@",[array objectAtIndex:i]]];
        [str_append appendString:[NSString stringWithFormat:@"=%@",[muDic objectForKey:[array objectAtIndex:i]]]];
        [str_append appendString:@"&"];
    }
    NSString *secStr = [self removeLastOneChar:str_append];
    return secStr;
}

#pragma mark 移除字符串最后一个字符
+ (NSString *)removeLastOneChar:(NSString*)origin {
    NSString* cutted;
    if([origin length] > 0){
        cutted = [origin substringToIndex:([origin length]-1)];// 去掉最后一个"&"
    } else {
        cutted = origin;
    }
    return cutted;
}

#pragma mark 时间戳 转 NSDate
+ (NSDate *)dateFromTimeInterva:(NSString *)str {
    long long time = [str longLongValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    //1:创建日期转换器  (实例化一个NSDateFormatter对象)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    //2:设置日期转换的时区
    [dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
    //3:设置日期转换的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return date;
}

#pragma mark 获取APP版本号
+ (NSString *)getAppVersion {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    return app_Version;
}

#pragma mark 拨打电话
+ (void)callMobilePhone:(NSString *)mobile {
    
    if (strIsEmpty(mobile)) {
        ShowToastWithText(@"暂无手机号码");
        return;
    }
    
    NSString *callMobile = [NSString stringWithFormat:@"tel://%@", mobile];
    NSURL *url = [NSURL URLWithString:callMobile];
    
    // 解决iOS10及其以上系统弹出拨号框延迟的问题
    if (@available(iOS 10.0, *)) {
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    } else {
        // Fallback on earlier versions
        [[UIApplication sharedApplication] openURL:url];
    }
}

+ (NSString *)toJSONString:(id)obj {
    NSData *data = [NSJSONSerialization dataWithJSONObject:obj
                                                   options:NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments
                                                     error:nil];
    if (data == nil) {
        return nil;
    }
    
    NSString *string = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    return string;
    ShowToastWithText(@"");
}

+ (NSString *)formatFloat:(float)f {
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}

///判断是否为电话号码
+ (BOOL)isMobileNumberOnly:(NSString *)mobileNum {
    
    NSString * MOBILE = @"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    
    if ([regextestmobile evaluateWithObject:mobileNum] == YES) {
        return YES;
    } else {
        return NO;
    }
}

+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format]; //(@"YYYY-MM-dd hh:mm:ss") ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate* date = [formatter dateFromString:formatTime]; //------------将字符串按formatter转成nsdate
    //时间转时间戳的方法:
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    return timeSp;
}

#pragma mark - 图片赋值处理
+ (void)imageViewUpdateWithUrl:(NSString *)url withImageView:(UIImageView *)imageView withPlaceholderImage:(nonnull NSString *)imageName{
    if ([url hasPrefix:@"http"]) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:imageName]];
    }else{
        sdimg(imageView,url);
    }
}

#pragma mark - 获取当前屏幕显示的UIViewController
+ (UIViewController *)getCurrentVC{
    UIViewController *resultVC;
    resultVC = [self getTopViewController:[[UIApplication sharedApplication].keyWindow rootViewController] vcArr:nil];
    while (resultVC.presentedViewController) {
        resultVC = [self getTopViewController:resultVC.presentedViewController vcArr:nil];
    }
    return resultVC;
}
+ (UIViewController *)getTopViewController:(UIViewController *)vc vcArr:(NSMutableArray *)vcArr {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        UIViewController *tempVC = [self getTopViewController:[(UINavigationController *)vc topViewController] vcArr:vcArr];
        if (vcArr && ![vcArr containsObject:tempVC]) {
            [vcArr addObject:tempVC];
        }
        return tempVC;
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        UIViewController *tempVC = [self getTopViewController:[(UITabBarController *)vc selectedViewController] vcArr:vcArr];
        if (vcArr && ![vcArr containsObject:tempVC]) {
            [vcArr addObject:tempVC];
        }
        return tempVC;
    } else {
        if (vcArr && ![vcArr containsObject:vc]) {
            [vcArr addObject:vc];
        }
        return vc;
    }
    return nil;
}

@end
