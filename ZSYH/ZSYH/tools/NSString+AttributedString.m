//
//  NSString+AttributedString.m
//  DaXueZhang
//
//  Created by 李英杰 on 2018/7/20.
//  Copyright © 2018年 qiaoxuekui. All rights reserved.
//

#import "NSString+AttributedString.h"

@implementation NSString (AttributedString)

//创建一段不同颜色的富文本
+ (NSMutableAttributedString *)getAttributedStringWithOriginalString:(NSString *)originalString originalColor:(UIColor *)origianlColor originalFont:(UIFont *)originalFont originalLineSpacing:(NSInteger)originalLineSpacing changeString:(NSString *)changeString changeColor:(UIColor *)changeColor changeFont:(UIFont *)changeFont {
    
    NSMutableParagraphStyle *muStyle = [[NSMutableParagraphStyle alloc]init];
    muStyle.alignment = NSTextAlignmentLeft;//对齐方式
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:originalString attributes:@{NSParagraphStyleAttributeName:muStyle, NSForegroundColorAttributeName:origianlColor, NSFontAttributeName:originalFont}];
    
    NSMutableParagraphStyle *changeStyle = [[NSMutableParagraphStyle alloc]init];
    muStyle.lineSpacing = originalLineSpacing;//设置行间距离
    NSRange range = [originalString rangeOfString:changeString];
    [attrString addAttribute:NSParagraphStyleAttributeName value:changeStyle range:range];
    [attrString addAttribute:NSForegroundColorAttributeName value:changeColor range:range];
    [attrString addAttribute:NSFontAttributeName value:changeFont range:range];
    return attrString;
    
    //    paragraphStyle.lineHeightMultiple = 1.5;// 行高倍数（1.5倍行高）
    //    paragraphStyle.firstLineHeadIndent = 30.0f;//首行缩进
    //    paragraphStyle.minimumLineHeight = 10;//最低行高
    //    paragraphStyle.maximumLineHeight = 20;//最大行高(会影响字体)
    //    paragraphStyle.defaultTabInterval = 144;// 默认Tab 宽度
    //    paragraphStyle.headIndent = 20;// 起始 x位置
    //    paragraphStyle.tailIndent = 320;// 结束 x位置（不是右边间距，与inset 不一样）
    //
    //    paragraphStyle.paragraphSpacing = 44.;// 段落间距
    //    paragraphStyle.paragraphSpacingBefore = 44.;// 段落头部空白(实测与上边的没差啊？)
    
    
    //paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;// 分割模式
    /*
     NSLineBreakByWordWrapping = 0,      // Wrap at word boundaries, default
     NSLineBreakByCharWrapping,  // Wrap at character boundaries
     NSLineBreakByClipping,  // Simply clip
     NSLineBreakByTruncatingHead, // Truncate at head of line: "...wxyz"
     NSLineBreakByTruncatingTail, // Truncate at tail of line: "abcd..."
     NSLineBreakByTruncatingMiddle // Truncate middle of line:  "ab...yz"
     */
    
    
    //paragraphStyle.baseWritingDirection = NSWritingDirectionRightToLeft;// 段落方向
    /*
     NSWritingDirectionNatural       = -1,    // Determines direction using the Unicode Bidi Algorithm rules P2 and P3
     NSWritingDirectionLeftToRight   =  0,    // Left to right writing direction
     NSWritingDirectionRightToLeft   =  1
     */
    
}

//将图片转化为富文本
+ (NSAttributedString *)getAttributedImageWithImage:(UIImage *)image imageBounds:(CGRect)bounds {
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = image;
    textAttachment.bounds = bounds;
    NSAttributedString *attachmentAttributedString = [NSAttributedString attributedStringWithAttachment:textAttachment];
    return attachmentAttributedString;
}

- (NSAttributedString *)getAttributedImageWithImage:(UIImage *)image imageBounds:(CGRect)bounds {
    NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
    textAttachment.image = image;
    textAttachment.bounds = bounds;
    NSAttributedString *attachmentAttributedString = [NSAttributedString attributedStringWithAttachment:textAttachment];
    return attachmentAttributedString;
}

//快速创建富文本
+ (NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string Color:(UIColor *)color font:(UIFont *)font lineSpacing:(NSInteger)lineSpacing {
    return [[self class] getAttributedStringWithOriginalString:string originalColor:color originalFont:font originalLineSpacing:lineSpacing changeString:@"" changeColor:color changeFont:font];
}

- (NSMutableAttributedString *)getAttributedStringWithOriginalColor:(UIColor *)origianlColor originalFont:(UIFont *)originalFont originalLineSpacing:(NSInteger)originalLineSpacing  changeString:(NSString *)changeString changeColor:(UIColor *)changeColor changeFont:(UIFont *)changeFont {
    return [[self class] getAttributedStringWithOriginalString:self originalColor:origianlColor originalFont:originalFont originalLineSpacing:originalLineSpacing changeString:changeString changeColor:changeColor changeFont:changeFont];
}

//创建中间划线富文本
- (NSMutableAttributedString *)getLineThroughAttributeStringWithOriginalAttributeString:(NSMutableAttributedString *)originalAttributeString lineThroughColor:(UIColor *)lineColor {
    return [[self class] getLineThroughAttributeStringWithOriginalAttributeString:originalAttributeString lineThroughColor:lineColor];
}

+ (NSMutableAttributedString *)getLineThroughAttributeStringWithOriginalAttributeString:(NSMutableAttributedString *)originalAttributeString lineThroughColor:(UIColor *)lineColor {
    [originalAttributeString addAttribute:NSStrikethroughStyleAttributeName value:@(1) range:NSMakeRange(0, originalAttributeString.length)];
    [originalAttributeString addAttribute:NSStrikethroughColorAttributeName value:lineColor range:NSMakeRange(0, originalAttributeString.length)];
    [originalAttributeString addAttribute:NSBaselineOffsetAttributeName value:@(0) range:NSMakeRange(0, originalAttributeString.length)];
    return originalAttributeString;
}

@end
