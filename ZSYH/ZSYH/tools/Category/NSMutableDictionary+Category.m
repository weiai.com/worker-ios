//
//  NSMutableDictionary+Category.m
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "NSMutableDictionary+Category.h"

@implementation NSMutableDictionary (Category)
- (void)setObjectIfNotNil:(id)object forKey:(id<NSCopying>)key{
    if (object) {
        [self setObject:object forKey:key];
    }
}

- (void)setObjectsFromDictionary:(NSDictionary *)dictionary{
    //不存在返回
    if (dictionary == nil) return;
    
    NSArray *allKeys = dictionary.allKeys;
    for (id key in allKeys) {
        id value = [dictionary objectForKey:key];
        [self setObject:value forKey:key];
    }
}
@end
