//
//  NSMutableArray+Category.h
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

// 可变数组 不可变数组 category

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
// 不可变数组
@interface NSArray (Category)
// 下标安全取值
- (id)objectAtIndexSafe:(NSUInteger)index;
@end
// 可变数组
@interface NSMutableArray (Category)
// 添加不为空的元素
- (void)addObjectNotNil:(id)object;
- (void)insertObjectNotNil:(id)object atIndex:(NSInteger)index;
// 获得指定范围内的数组
- (NSArray *)objectsTop:(NSUInteger)topNumber;
// 数组重新排序(倒叙)
- (NSArray *)reverse;
@end

NS_ASSUME_NONNULL_END
