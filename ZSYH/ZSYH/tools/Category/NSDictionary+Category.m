//
//  NSDictionary+Category.m
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "NSDictionary+Category.h"

@implementation NSDictionary (Category)
- (id)objectForKeyNotNil:(id)key{
    if ([[self allKeys] containsObject:key]) {
        if ([[self objectForKey:key] isKindOfClass:[NSNull class]]) {
            return nil;
        }else{
            return [self objectForKey:key];
        }
    }
    return nil;
}
@end
