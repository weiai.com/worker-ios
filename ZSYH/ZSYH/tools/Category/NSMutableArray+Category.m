//
//  NSMutableArray+Category.m
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

#import "NSMutableArray+Category.h"

@implementation NSArray (Category)
- (id)objectAtIndexSafe:(NSUInteger)index{
    
    if (index < self.count) {
        return self[index];
    }
    return nil;
}

@end

@implementation NSMutableArray (Category)
- (void)addObjectNotNil:(id)object{
    if (object) {
        [self addObject:object];
    }
}

- (void)insertObjectNotNil:(id)object atIndex:(NSInteger)index{
    if (object) {
        [self insertObject:object atIndex:index];
    }
}

- (NSArray *)objectsTop:(NSUInteger)topNumber{
    
    NSUInteger number = MIN(topNumber, self.count);
    
    if (number > 0) {
        NSMutableArray *arr = [NSMutableArray arrayWithCapacity:number];
        for (int i = 0; i < number; i++) {
            [arr addObject:self[i]];
        }
        return arr;
    }
    return nil;
}

- (NSArray *)reverse{
    if ([self count] == 0) return self;
    
    NSUInteger i = 0;
    NSUInteger j = [self count] - 1;
    while (i < j) {
        [self exchangeObjectAtIndex:i withObjectAtIndex:j];
        i ++;
        j --;
    }
    return self;
}
@end
