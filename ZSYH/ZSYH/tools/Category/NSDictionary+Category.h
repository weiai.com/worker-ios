//
//  NSDictionary+Category.h
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

// 不可变字典的category

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Category)
//字典取值key为空的处理
- (id)objectForKeyNotNil:(id)key;

@end

NS_ASSUME_NONNULL_END
