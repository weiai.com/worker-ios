//
//  UISegmentedControl+ChangeColor.h
//  ZSYH
//
//  Created by 李 on 2019/12/6.
//  Copyright © 2019 魏堰青. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UISegmentedControl (ChangeColor)
- (void)ensureiOS12Style;
@end

NS_ASSUME_NONNULL_END
