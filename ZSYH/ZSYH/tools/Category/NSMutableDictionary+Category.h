//
//  NSMutableDictionary+Category.h
//  DistributorAB
//
//  Created by LZY on 2019/10/9.
//  Copyright © 2019 主事丫环. All rights reserved.
//

// 可变字典category

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableDictionary (Category)
//添加不为空的key-value处理
- (void)setObjectIfNotNil:(id)object forKey:(id<NSCopying>)key;

- (void)setObjectsFromDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
