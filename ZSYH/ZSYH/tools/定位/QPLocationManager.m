//
//  QPLocationManager.m
//  TaoHuo
//
//  Created by wanshangwl on 2018/4/10.
//  Copyright © 2018年 wyx. All rights reserved.
//

#import "QPLocationManager.h"

#import "QPLocationManager.h"
#import <CoreLocation/CoreLocation.h>

@interface QPLocationManager ()<CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager * locManager;

@property (nonatomic, copy) LocationBlock block;

//@property (nonatomic, copy) LatitudeUpdate latitude;

@end

@implementation QPLocationManager

+ (instancetype)sharedManager {
    
    static QPLocationManager * _manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[QPLocationManager alloc] init];
    });
    
    return _manager;
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _locManager = [[CLLocationManager alloc] init];
        _locManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locManager.distanceFilter = 100;
        _locManager.delegate = self;
        
        if (![CLLocationManager locationServicesEnabled]) {
            ShowToastWithText(@"请开启定位服务");
        } else {
            CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
            if (status == kCLAuthorizationStatusNotDetermined) {
                [_locManager requestWhenInUseAuthorization];
            }
        }
    }
    return self;
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([error code] == kCLErrorDenied) {
        ShowToastWithText(@"访问被拒绝");
    }
    if ([error code] == kCLErrorLocationUnknown) {
        //ShowToastWithText(@"无法获取位置信息");
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *newLocation = locations[0];

    NSString *lat = [NSString stringWithFormat:@"%.6f",newLocation.coordinate.latitude];
    NSString *lon = [NSString stringWithFormat:@"%.6f",newLocation.coordinate.longitude];
    [USER_DEFAULT setObject:lat forKey:@"latitude"];
    [USER_DEFAULT setObject:lon forKey:@"longitude"];
    [USER_DEFAULT synchronize];

    
    if (self.block) {
        CLGeocoder *geocoder = [[CLGeocoder alloc]init];
        [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray<CLPlacemark *> *_Nullable array, NSError * _Nullable error) {
            if (array.count > 0){
                CLPlacemark *placemark = [array objectAtIndex:0];
                //获取城市
                NSString *city = placemark.locality;
                if (!city) {
                    city = placemark.administrativeArea;
                }
                self.block(placemark.administrativeArea,city,placemark.subLocality,placemark.name);
            }else if (error == nil && [array count] == 0) {
                NSLog(@"No results were returned.");
            }else if (error != nil) {
                NSLog(@"An error occurred = %@", error);
            }
        }];
    }
    [manager stopUpdatingLocation];
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    
//    CLLocationCoordinate2D coor = newLocation.coordinate;
//    
//    NSString * lat = [NSString stringWithFormat:@"%@",@(coor.longitude)];
//    NSString * lon = [NSString stringWithFormat:@"%@",@(coor.latitude)];
//    
//    [QPLocationManager sharedManager].lon = lon;
//    [QPLocationManager sharedManager].lat = lat;
//    
//    self.block(lat,lon,@"",@"");
//    
//    [self.locManager stopUpdatingLocation];
//    
//}

- (void)getGps:(LocationBlock)block {
    
    self.block = block;
    [self.locManager startUpdatingLocation];
    
}


//- (void)getLatitude:(LatitudeUpdate)block {
//    self.latitude = block;
//    [self.locManager startUpdatingLocation];
//}

@end

