//
//  QPLocationManager.h
//  TaoHuo
//
//  Created by wanshangwl on 2018/4/10.
//  Copyright © 2018年 wyx. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^LocationBlock)(NSString * province, NSString * city, NSString * area, NSString * street);

//typedef void(^LatitudeUpdate)(NSString *latitude, NSString *longitude);

@interface QPLocationManager : NSObject

+ (instancetype)sharedManager;

- (void)getGps:(LocationBlock)block;

//- (void)getLatitude:(LatitudeUpdate)block;

//@property (nonatomic, strong) NSString * lon;
//@property (nonatomic, strong) NSString * lat;



@end

