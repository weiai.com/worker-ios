//
//  NetLoadAreaModel.m
//  ZSYH
//
//  Created by 李英杰 on 2019/9/4.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import "NetLoadAreaModel.h"

@implementation NetLoadAreaModel

+ (NetLoadAreaModel *)initWithProvinceCode:(NSString *)provinceCode provinceName:(NSString *)provinceName cityCode:(NSString *)cityCode cityName:(NSString *)cityName districtCode:(NSString *)districtCode districtName:(NSString *)districtName {
    NetLoadAreaModel *model = [[NetLoadAreaModel alloc] init];
    model.provinceCode = provinceCode;
    model.provinceName = provinceName;
    model.cityCode = cityCode;
    model.cityName = cityName;
    model.districtCode = districtCode;
    model.districtName = districtName;
    return model;
}

@end
