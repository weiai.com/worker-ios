//
//  NetLoadAreaModel.h
//  ZSYH
//
//  Created by 李英杰 on 2019/9/4.
//  Copyright © 2019年 魏堰青. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetLoadAreaModel : NSObject

@property (nonatomic, copy) NSString * provinceName;//省名字
@property (nonatomic, copy) NSString * provinceCode;//省编号
@property (nonatomic, copy) NSString * cityName;//市名字
@property (nonatomic, copy) NSString * cityCode;//市编号
@property (nonatomic, copy) NSString * districtName;//区名字
@property (nonatomic, copy) NSString * districtCode;//区编号

+ (NetLoadAreaModel *)initWithProvinceCode:(NSString *)provinceCode provinceName:(NSString *)provinceName cityCode:(NSString *)cityCode cityName:(NSString *)cityName districtCode:(NSString *)districtCode districtName:(NSString *)districtName;
@end

NS_ASSUME_NONNULL_END
