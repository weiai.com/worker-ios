//
//  NetLoadAreaPickerViewController.h
//  KangChiLianMeng
//
//  Created by apple on 2018/6/8.
//  Copyright © 2018年 com.ruanmeng.login. All rights reserved.
//

#import "BaseViewController.h"
#import "NetLoadAreaModel.h"

@protocol NetLoadAddressPickerDelegate <NSObject>

- (void)getAddressStringWithProvinceName:(NSString *)procinvename cityName:(NSString *)cityName districtName:(NSString *)districtName provinceCode:(NSString *)provincecode cityCode:(NSString *)citycode districtCode:(NSString *)districtcode;


@end

@interface NetLoadAreaPickerViewController : BaseViewController
@property (nonatomic,strong)UIPickerView * pickerView;
@property (nonatomic,retain)UIButton * canCleBtn;
@property (nonatomic,strong)UILabel * titleLab;
@property (nonatomic,retain)UIButton * finishBtn;
@property (nonatomic, copy) void(^selectedBlock)(NetLoadAreaModel *areaModel);


@property (nonatomic,assign)id <NetLoadAddressPickerDelegate>delegate;

@end
