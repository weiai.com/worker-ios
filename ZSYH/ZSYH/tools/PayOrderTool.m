//
//  PayOrderTool.m
//  AiBao
//
//  Created by wanshangwl on 2018/3/31.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <AlipaySDK/AlipaySDK.h>
#import "PayOrderTool.h"

#import "WXApi.h"

@implementation PayOrderTool

//+ (instancetype)sharPayOrder {
//    static PayOrder *share=nil;
//    static dispatch_once_t once;
//    dispatch_once(&once,^{
//        share = [[PayOrder alloc]init];
//    });
//    return share;
//}


#pragma mark - aliPay
+ (void)aliPay:(NSString *)orderstr {
    
    //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
    NSString *appScheme = @"zhushiyahuan";
    
    // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
    //NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",orderInfoEncoded, signedString];
    // NOTE: 调用支付结果开始支付
    [[AlipaySDK defaultService] payOrder:orderstr fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"resultDic----%@",resultDic);
        
        if ([[resultDic objectForKey:@"resultStatus"]isEqualToString:@"9000"]) {
            //支付成功了
            [Center postNotificationName:@"PaySuccess" object:nil];
        }else {
            [Center postNotificationName:@"PayFail" object:nil];
        }
    }];
}


+ (void)wxPay:(NSDictionary *)dict {
    PayReq* req             = [[PayReq alloc] init];
    req.partnerId           = [dict objectForKey:@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    
    UInt32 timeStamp        = [[dict objectForKey:@"timestamp"] intValue];
    req.timeStamp           = timeStamp;
    req.package             = [dict objectForKey:@"package"];
    req.sign                = [dict objectForKey:@"sign"];
        NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",[dict objectForKey:@"appid"],req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
    [WXApi sendReq:req];
}


+ (NSString *)createTradeNO {
    static int kNumber = 15;
    
    NSString *sourceStr = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    srand((unsigned)time(0));
    for (int i = 0; i < kNumber; i++) {
        unsigned index = rand() % [sourceStr length];
        NSString *oneStr = [sourceStr substringWithRange:NSMakeRange(index, 1)];
        [resultStr appendString:oneStr];
    }
    return resultStr;
}

@end

