//
//  NSString+AttributedString.h
//  DaXueZhang
//
//  Created by 李英杰 on 2018/7/20.
//  Copyright © 2018年 qiaoxuekui. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AttributedString)

//创建一段不同颜色的富文本
+ (NSMutableAttributedString *)getAttributedStringWithOriginalString:(NSString *)originalString originalColor:(UIColor *)origianlColor originalFont:(UIFont *)originalFont originalLineSpacing:(NSInteger)originalLineSpacing changeString:(NSString *)changeString changeColor:(UIColor *)changeColor changeFont:(UIFont *)changeFont;

//将图片转化为富文本
+ (NSAttributedString *)getAttributedImageWithImage:(UIImage *)image imageBounds:(CGRect)bounds;

- (NSAttributedString *)getAttributedImageWithImage:(UIImage *)image imageBounds:(CGRect)bounds;

//快速创建富文本
+ (NSMutableAttributedString *)getAttributedStringWithString:(NSString *)string Color:(UIColor *)color font:(UIFont *)font lineSpacing:(NSInteger)lineSpacing;

- (NSMutableAttributedString *)getAttributedStringWithOriginalColor:(UIColor *)origianlColor originalFont:(UIFont *)originalFont originalLineSpacing:(NSInteger)originalLineSpacing  changeString:(NSString *)changeString changeColor:(UIColor *)changeColor changeFont:(UIFont *)changeFont;


/**
 创建中间划线富文本

 @param originalAttributeString 原始可变富文本
 @param lineColor 划线的颜色
 @return 加上划线的富文本
 */
- (NSMutableAttributedString *)getLineThroughAttributeStringWithOriginalAttributeString:(NSMutableAttributedString *)originalAttributeString lineThroughColor:(UIColor *)lineColor;

+ (NSMutableAttributedString *)getLineThroughAttributeStringWithOriginalAttributeString:(NSMutableAttributedString *)originalAttributeString lineThroughColor:(UIColor *)lineColor;
@end
