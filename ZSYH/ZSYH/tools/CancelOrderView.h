//
//  CancelOrderView.h
//  AiBao
//
//  Created by wanshangwl on 2018/4/2.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^Select)(NSString *index);

@interface CancelOrderView : UIView

@property(nonatomic,copy)NSString *type;//取消原因
@property(nonatomic,copy)void (^myBlock)(NSString *type);

//- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)titles block:(Select)select;
- (instancetype)initWithFrame:(CGRect)frame array:(NSArray *)titles block:(void(^)(NSString *type))select;

@end
