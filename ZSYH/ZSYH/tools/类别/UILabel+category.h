//
//  UILabel+category.h
//  BestFresh
//
//  Created by wanshangwl on 2018/4/13.
//  Copyright © 2018年 wyx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (category)

//设置字符串不同颜色
- (void)setAttrText:(NSString *)string range:(NSRange)range color:(UIColor *)color font:(CGFloat)font;

//加斜线
- (void)obliqueLineText:(NSString *)text color:(UIColor *)color;

- (CGFloat)getLabelHeightByWidth:(CGFloat)width Title:(NSString *)title font:(UIFont *)font; 
@end
