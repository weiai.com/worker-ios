//
//  UITextField+category.m
//  OrganizationDepartment
//
//  Created by xiaoshunliang on 16/10/12.
//  Copyright © 2016年 bodaokeji. All rights reserved.
//

#import "UITextField+category.h"
#import <objc/runtime.h>


@implementation UITextField (category)
static NSString *kLimitTextLengthKey = @"kLimitTextLengthKey";

- (void)limitTextLength:(NSInteger)length {
    objc_setAssociatedObject(self, (__bridge const void *)(kLimitTextLengthKey), [NSNumber numberWithInteger:length], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addTarget:self action:@selector(textFieldTextLengthLimit:) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldTextLengthLimit:(id)sender {
    NSNumber *lengthNumber = objc_getAssociatedObject(self, (__bridge const void *)(kLimitTextLengthKey));
    NSUInteger length = [lengthNumber intValue];
    
    if (self.text.length > length) {
        self.text = [self.text substringToIndex:length];
    }
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    if ([self respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = placeholderColor;
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: color}];
        // self.textColor = [[UIColor blackColor] colorWithAlphaComponent:0.75];
    } else {
        // earlier than iOS 6.0
    }
    
}


//设置底部线条

- (void)setLine {
    [self setBottomLine:WHITECOLOR];
}

- (void)setBottomLine:(UIColor *)color {
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, self.bounds.size.height-0.5, KWIDTH-60, 0.5)];
    
    line.backgroundColor = color;
    self.layer.masksToBounds = YES;
    [self addSubview:line];
}


- (void)leftImage:(NSString *)img {
    //[self setPlaceholderColor:WHITECOLOR];
    
    UIButton *imgv = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgv setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    self.leftView = imgv;
    self.leftViewMode = UITextFieldViewModeAlways;
    [self setLine];
}


- (void)createLeftViewImage:(NSString *)img {
    self.frame = CGRectMake(0, 0, KWIDTH-100, 30);
    self.backgroundColor = WHITECOLOR;
    self.font = [UIFont systemFontOfSize:15];
    self.textColor = KTEXTCOLOR;
    
    self.placeholder = @"请输入产品名称";
    self.tintColor = zhutiColor;
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
    
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.returnKeyType = UIReturnKeySearch;
    
    UIButton *imgv = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [imgv setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    self.leftView = imgv;
    self.leftViewMode = UITextFieldViewModeAlways;
}
- (void)PlasecreateLeftViewImage:(NSString *)img {
    self.frame = CGRectMake(0, 0, KWIDTH-100-20, 30);
    self.backgroundColor = WHITECOLOR;
    self.font = [UIFont systemFontOfSize:15];
    self.textColor = KTEXTCOLOR;
    
    self.placeholder = @"请输入产品名称";
    self.tintColor = zhutiColor;
    self.layer.cornerRadius = 15;
    self.layer.masksToBounds = YES;
    
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.returnKeyType = UIReturnKeySearch;
    
    UIButton *imgv = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    [imgv setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
    self.leftView = imgv;
    self.leftViewMode = UITextFieldViewModeAlways;
}



- (void)createLeftViewText:(NSString *)text {
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.height+30, self.height)];
    lab.textColor = KTEXTCOLOR;
    lab.textAlignment = NSTextAlignmentCenter;
    lab.font = self.font;
    lab.text = text;
    
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(lab.width-5.5, 5, 0.5, lab.height-10)];
    line.backgroundColor = KTEXTCOLOR;
    [lab addSubview:line];
    
    self.leftView = lab;
    self.leftViewMode = UITextFieldViewModeAlways;
}



//输入金额控制
- (void)addRules {
    [self addTarget:self action:@selector(checkInput)forControlEvents:UIControlEventEditingChanged];
    self.keyboardType = UIKeyboardTypeDecimalPad;
}

- (void)checkInput {
    if([self.text hasPrefix:@"00"]) { //不能已00开头
        self.text= [self.text substringToIndex:1];
    }
    if([self.text hasPrefix:@"0"]) { //不能已00开头
        self.text= [self.text substringToIndex:0];
    }
    if([self.text hasPrefix:@"."]) { //不能以小数点开头
        self.text= [self.text substringToIndex:0];
    }
    
    NSRange range = [self.text rangeOfString:@"."];
    if(range.location!=NSNotFound) {
        //不能输入多个小数点
        if([[self.text substringFromIndex:range.location+1]rangeOfString:@"."].location!=NSNotFound) {
            self.text= [self.text substringToIndex:self.text.length-1];
        }
        //最多输入两位小数
        if(self.text.length>= range.location+ range.length+3) {
            self.text= [self.text substringToIndex:range.location+3];
        }
    }else if(self.text.length>=9) { //金额小数点前不能超过九位
        self.text= [self.text substringToIndex:9];
    }
}

@end
