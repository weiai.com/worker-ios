//
//  UIAlertController+category.m
//  IntelligenceStudents
//
//  Created by xiaoshunliang on 2016/12/14.
//  Copyright © 2016年 GaoJuan. All rights reserved.
//

#import "UIAlertController+category.h"
@implementation UIAlertController (category)

+ (void)showPhotoAlerControllerPicker:(UIImagePickerController *)picker controller:(UIViewController *)controller sourceView:(UIView *)view {
    UIAlertController *alerSheet = [UIAlertController alertControllerWithTitle:nil message:@"选择照片" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *album = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [controller.navigationController presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *pictures = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [controller.navigationController presentViewController:picker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];

    [alerSheet addAction:album];
    [alerSheet addAction:pictures];
    [alerSheet addAction:cancel];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        UIPopoverPresentationController *popPresenter = alerSheet.popoverPresentationController;
        popPresenter.sourceView = view;
        popPresenter.sourceRect = view.bounds;
        [controller presentViewController:alerSheet animated:YES completion:nil];
    }else {
        [controller presentViewController:alerSheet animated:YES completion:nil];
        
    }
}

+ (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message controller:(UIViewController *)controller block:(void (^)(NSInteger buttonIndex))inblock cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (cancelButtonTitle) {
        UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"点击了alert的取消按钮");
        }];
        [alertController addAction:cancleAction];
    }
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:otherButtonTitles style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        inblock(1);//给确定按钮传个buttonIndex 调用block
    }];
    //[okAction setValue:KBColor forKey:@"titleTextColor"];

    [alertController addAction:okAction];
    [controller presentViewController:alertController animated:YES completion:nil];
}


+ (void)showVideoAlerControllerPicker:(UIImagePickerController *)picker controller:(UIViewController *)controller sourceView:(UIView *)view {
    UIAlertController *alerSheet = [UIAlertController alertControllerWithTitle:nil message:@"选择视频来源" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *album = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [controller.navigationController presentViewController:picker animated:YES completion:nil];
    }];
    UIAlertAction *pictures = [UIAlertAction actionWithTitle:@"拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [controller.navigationController presentViewController:picker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    [alerSheet addAction:album];
    [alerSheet addAction:pictures];
    [alerSheet addAction:cancel];
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        UIPopoverPresentationController *popPresenter = alerSheet.popoverPresentationController;
        popPresenter.sourceView = view;
        popPresenter.sourceRect = view.bounds;
        [controller presentViewController:alerSheet animated:YES completion:nil];
    }else {
        [controller presentViewController:alerSheet animated:YES completion:nil];
        
    }
}


@end
