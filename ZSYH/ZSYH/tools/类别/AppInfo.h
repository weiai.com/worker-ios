//
//  AppInfo.h
//  OrganizationDepartment
//
//  Created by xiaoshunliang on 16/9/16.
//  Copyright © 2016年 bodaokeji. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppInfo : NSObject

#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr, "[%s:%d行] %s\n", [[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#define JiGuangIsProduction  YES
#else
#define NSLog(FORMAT, ...) nil
#define JiGuangIsProduction  false
#endif



//项目中简写
#define USER_DEFAULT [NSUserDefaults standardUserDefaults]
#define Center       [NSNotificationCenter defaultCenter]

#define  TableRegisterNib(table,cell) [table registerNib:[UINib nibWithNibName:cell bundle:nil] forCellReuseIdentifier:cell]

#define  CollectionRegisterNib(collection,cell) [collection registerNib:[UINib nibWithNibName:cell bundle:nil] forCellWithReuseIdentifier:cell]

//#define KLock   @"is_lock"


#define KUID [USER_DEFAULT objectForKey:@"user_id"]
#define KTKID [USER_DEFAULT objectForKey:@"token"]
#define KRTOKEN [USER_DEFAULT objectForKey:@"rongToken"]

//#define KLatID [USER_DEFAULT objectForKey:@"latitude"]
//#define KLongID [USER_DEFAULT objectForKey:@"longitude"]


#define KVIP 1



@end
