//
//  FL_Button.m
//
//  Created by xiaoshunliang on 15/12/10.
//  Copyright © 2015年 czebd. All rights reserved.
//

#import "FL_Button.h"
/**
 *  定义宏：按钮中文本和图片的间隔
 */
#define SPACE 7


// 获得按钮的大小
#define btnW self.bounds.size.width
#define btnH self.bounds.size.height
// 获得按钮中UILabel文本的大小
#define labW self.titleLabel.bounds.size.width
#define labH self.titleLabel.bounds.size.height
// 获得按钮中image图标的大小
#define imgW self.imageView.bounds.size.width
#define imgH self.imageView.bounds.size.height

@implementation FL_Button

+ (instancetype)fl_shareButton{
    return [[self alloc] init];
}

- (instancetype)initWithAlignmentStatus:(FLAlignmentStatus)status{
    FL_Button *fl_button = [[FL_Button alloc] init];
    fl_button.status = status;
    return fl_button;
}

- (void)setStatus:(FLAlignmentStatus)status{
    _status = status;
}
#pragma mark - 左对齐
- (void)alignmentLeft{
    //    获得按钮的文本的frame
    CGRect titleFrame = self.titleLabel.frame;
    //    设置按钮的文本的x坐标为0-－－左对齐
    titleFrame.origin.x = 0;
    //    获得按钮的图片的frame
    CGRect imageFrame = self.imageView.frame;
    //    设置按钮的图片的x坐标紧跟文本的后面
    imageFrame.origin.x = CGRectGetWidth(titleFrame);
    //    重写赋值frame
    self.titleLabel.frame = titleFrame;
    self.imageView.frame = imageFrame;
}
#pragma mark - 右对齐
- (void)alignmentRight{
    // 计算文本的的宽度
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    dictM[NSFontAttributeName] = self.titleLabel.font;
    CGRect frame = [self.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dictM context:nil];
    
    CGRect imageFrame = self.imageView.frame;
    imageFrame.origin.x = self.bounds.size.width - imgW;
    CGRect titleFrame = self.titleLabel.frame;
    titleFrame.origin.x = imageFrame.origin.x - frame.size.width ;
    //    重写赋值frame
    self.titleLabel.frame = titleFrame;
    self.imageView.frame = imageFrame;
}

#pragma mark - 居中对齐
- (void)alignmentCenter {
    //    设置文本的坐标
    CGFloat labelX = (btnW - labW -imgW - SPACE) * 0.5;
    CGFloat labelY = (btnH - labH) * 0.5;
    
    //    设置label的frame
    self.titleLabel.frame = CGRectMake(labelX, labelY, labW, labH);
    
    //    设置图片的坐标
    CGFloat imageX = CGRectGetMaxX(self.titleLabel.frame) + SPACE;
    CGFloat imageY = (btnH - imgH) * 0.5;
    //    设置图片的frame
    self.imageView.frame = CGRectMake(imageX, imageY, imgW, imgH);
}

#pragma mark - 图标在上，文本在下(居中)
- (void)alignmentTop {
    // 计算文本的的宽度
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    dictM[NSFontAttributeName] = self.titleLabel.font;
    CGRect frame = [self.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dictM context:nil];
    
    CGFloat imageX = (btnW - imgW) * 0.5;
    self.imageView.frame = CGRectMake(imageX, btnH * 0.5 - imgH * imgRadio, imgW, imgH);
    self.titleLabel.frame = CGRectMake((self.center.x - frame.size.width) * 0.5, btnH * 0.5 + labH * 0.5, labW, labH);
    CGPoint labelCenter = self.titleLabel.center;
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -20, 0, -20)];
    labelCenter.x = self.imageView.center.x;
    [self setTitleColor:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1] forState:UIControlStateNormal];
    self.titleLabel.center = labelCenter;
}

#pragma mark - 图标在下，文本在上(居中)
- (void)alignmentBottom{
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    // 计算文本的的宽度
    NSMutableDictionary *dictM = [NSMutableDictionary dictionary];
    dictM[NSFontAttributeName] = self.titleLabel.font;
    CGRect frame = [self.titleLabel.text boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dictM context:nil];
    
    CGFloat imageX = (btnW - imgW) * 0.5;
    self.titleLabel.frame = CGRectMake((self.center.x - frame.size.width) * 0.5, btnH * 0.5 - labH * (1 + textRadio), labW, labH);
    self.imageView.frame = CGRectMake(imageX, btnH * 0.5 , imgW, imgH);
    CGPoint labelCenter = self.titleLabel.center;
    labelCenter.x = self.imageView.center.x;
    self.titleLabel.center = labelCenter;
}

/**
 *  布局子控件
 */
- (void)layoutSubviews{
    [super layoutSubviews];
    // 判断
    if (_status == FLAlignmentStatusNormal) {
        
    }
    else if (_status == FLAlignmentStatusLeft){
        [self alignmentLeft];
    }
    else if (_status == FLAlignmentStatusCenter){
        [self alignmentCenter];
    }
    else if (_status == FLAlignmentStatusRight){
        [self alignmentRight];
    }
    else if (_status == FLAlignmentStatusTop){
        [self alignmentTop];
    }
    else if (_status == FLAlignmentStatusBottom){
        [self alignmentBottom];
    }
}



@end
