//
//  UILabel+category.m
//  BestFresh
//
//  Created by wanshangwl on 2018/4/13.
//  Copyright © 2018年 wyx. All rights reserved.
//

#import "UILabel+category.h"

@implementation UILabel (category)


- (void)setAttrText:(NSString *)string range:(NSRange)range color:(UIColor *)color font:(CGFloat)font {
    NSMutableAttributedString *totalss = [[NSMutableAttributedString alloc]initWithString:string];
    [totalss addAttribute:NSForegroundColorAttributeName value:color range:range];
    [totalss addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:font] range:range];
    self.attributedText = totalss;
}


- (void)obliqueLineText:(NSString *)text color:(UIColor *)color {
    CGFloat width = [NSString sizeWithFontSize:14 text:text].width;
    
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] initWithLayer:self.layer];
    shapeLayer.lineWidth = 1.0;
    shapeLayer.strokeColor = color.CGColor;
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0, 3)];
    [path addLineToPoint:CGPointMake(width+3, self.frame.size.height-5)];
    
    shapeLayer.path = path.CGPath;
    [self.layer addSublayer:shapeLayer];
}

//- (CGRect)widthOfTextFieldWithStr:(NSString *)str {
//    CGRect size = [str boundingRectWithSize:CGSizeMake(kScreenW, heightEx(60)) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]} context:nil];
//    return size;
//}

- (CGFloat)getLabelHeightByWidth:(CGFloat)width Title:(NSString *)title font:(UIFont *)font {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    label.text = title;
    label.font = font;
    label.numberOfLines = 0;
    [label sizeToFit];
    CGFloat height = label.frame.size.height;
    return height;
}
@end
