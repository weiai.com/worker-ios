//
//  UITextField+category.h
//  OrganizationDepartment
//
//  Created by xiaoshunliang on 16/10/12.
//  Copyright © 2016年 bodaokeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (category)
/**
 *  Set limit length
 *
 *  @param length max length
 */
- (void)limitTextLength:(NSInteger)length;


/**
 *  Set limit length
 *
 *  设置placerHolder的颜色
 */
- (void)setPlaceholderColor:(UIColor *)placeholderColor;


//底部线
- (void)setLine;

- (void)setBottomLine:(UIColor *)color;

//登陆输入框
- (void)leftImage:(NSString *)img;


//搜索输入框
- (void)createLeftViewImage:(NSString *)img;

//添加金额输入规则
- (void)addRules;

//添加占位符
- (void)PlasecreateLeftViewImage:(NSString *)img;

@end
