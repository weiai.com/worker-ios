//
//  NSString+category.h
//  MiYun360
//
//  Created by xiaoshunliang on 16/5/12.
//  Copyright © 2016年 AUNotBad. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ExamDataAnswerModel;
@class ChallengeExamDataModel;

@interface NSString (category)
//提交数据是空数据用户提示 结构形式  @[@{@"提示语":要不断对象}]
+ (NSString *)showTip:(NSArray *)messageArr;
//判读是不是空字符串
+ (BOOL)isBlankString:(id)string;
+ (NSString *)splicingHttpStr:(NSString *)str;
//时间转化为年月日
+ (NSString *)fromTimeStringToTime:(NSString *)timeString;
//月日
+ (NSString *)fromTimeStringToDayAndMonth:(NSString *)timeString;

/**
 *  指定字体宽度返回高度
 */
+ (CGFloat)heightWithWidth:(CGFloat)width font:(CGFloat)font text:(NSString *)text;
/**
*  指定字体宽度文字样式返回高度
*/
+ (CGFloat)heightWithWidth:(CGFloat)width fontNameString:(NSString *)fontName fontSize:(CGFloat)fontSize text:(NSString *)text;
/**
 *  返回字体宽度
 */
+ (CGFloat)widthWithFont:(CGFloat)font text:(NSString *)text;
/**
*  指定文字样式返回字体宽度
*/
+ (CGFloat)widthWithFont:(CGFloat)font fontNameString:(NSString *)fontName text:(NSString *)text;

/**
 *  指定字体的大小返回size
 */
+ (CGSize)sizeWithFontSize:(CGFloat)fontSize text:(NSString *)text;
/**
 *  指定字体的大小返回frame
 */
+ (CGRect)boundsWithFontSize:(CGFloat)fontSize text:(NSString *)text needWidth:(CGFloat)needWidth;
/**
 *  指定行数的大小返回frame
 */
+ (CGFloat)boundsWithFontSize:(CGFloat)fontSize text:(NSString *)text needWidth:(CGFloat)needWidth lineNumber:(NSInteger)lineNumber;
/**
 *  将字符串转化为json
 */
+ (NSDictionary *)parseJSONStringToNSDictionary:(NSString *)JSONString;
/**
 *  替换掉null的字符串
 */

+ (NSString *)nullString:(NSString *)nullStr;
/**
 *  判读是不是手机号
 */
- (BOOL)isValidMobileNumber;

/**
 *  判读是不是身份证
 */
+ (BOOL)identityIsValid:(NSString *)str;

/**
 *  用特定的格式显示当前时间
 */
+ (NSString *)setTheCurrentTimeWithFormat:(NSDate *)date fromat:(NSString *)format;
/**
 *  刚刚，5秒前，1分钟前，1小时前等等
 *
 */
+ (NSString*)timeAgoWithDate:(NSString*)date;

/**
 *  用当前时间 - 结束时间 来比较看是否答题已经结束
 */
+ (BOOL)isCurrentTimeCutEndTimeWithDate:(NSDate *)date endTime:(NSString*)endTimeStr;

/**
 *  是不是中文
 */
- (BOOL)isChinese;

/**
 *  Create a MD5 string from self
 *
 *  @return Return the MD5 NSString from self
 */
- (NSString *)MD5;
+ (NSString *)MD5;
+ (NSString *)encodeUrlStr:(NSString *)urlStr;

/**
 打印请求的URL
 */
+ (void)printAppendHttpURL:(NSDictionary *)params urlStr:(NSString *)urlStr;

/**
 判断密码输入的是否符合要求
 @return YES 表示符合要求  反之不符合要求
 */
+ (BOOL)judgePassword:(NSString *)text;
/**
 判断金额输入的是否符合要求
 @return YES 表示符合要求  反之不符合要求
 */
+ (BOOL)judgeCash:(NSString *)text;
@end

