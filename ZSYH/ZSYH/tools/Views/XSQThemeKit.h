//
//  XSQThemeKit.h
//  PandaHelp
//
//  Created by xiaoshunliang on 15/11/5.
//  Copyright (c) 2015年 xiaoshunliang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XSQThemeKit : NSObject



#pragma mark - Master Theme

+ (void)setupThemeWithPrimaryColor:(UIColor *)primaryColor secondaryColor:(UIColor *)secondaryColor fontName:(NSString *)fontName lightStatusBar:(BOOL)lightStatusBar;


#pragma mark - UINavigationBar

+ (void)customizeNavigationBarColor:(UIColor *)barColor textColor:(UIColor *)textColor buttonColor:(UIColor *)buttonColor;
+ (void)customizeNavigationBarColor:(UIColor *)barColor textColor:(UIColor *)textColor fontName:(NSString *)fontName fontSize:(CGFloat)fontSize buttonColor:(UIColor *)buttonColor;


#pragma mark - UIBarButtonItem

+ (void)customizeNavigationBarButtonColor:(UIColor *)buttonColor;


#pragma mark - UITabBar

+ (void)customizeTabBarColor:(UIColor *)barColor textColor:(UIColor *)textColor;
+ (void)customizeTabBarColor:(UIColor *)barColor textColor:(UIColor *)textColor fontName:(NSString *)fontName fontSize:(CGFloat)fontSize;


#pragma mark - UIButton

+ (void)customizeButtonColor:(UIColor *)buttonColor;


#pragma mark - UISwitch

+ (void)customizeSwitchOnColor:(UIColor *)switchOnColor;


#pragma mark - UISearchBar

+ (void)customizeSearchBarColor:(UIColor *)barColor buttonTintColor:(UIColor *)buttonTintColor;


#pragma mark - UIActivityIndicator

+ (void)customizeActivityIndicatorColor:(UIColor *)color;


#pragma mark - UISegmentedControl

+ (void)customizeSegmentedControlWithMainColor:(UIColor *)mainColor secondaryColor:(UIColor *)secondaryColor;


#pragma mark - UISlider

+ (void)customizeSliderColor:(UIColor *)sliderColor;


#pragma mark - UIToolbar

+ (void)customizeToolbarTintColor:(UIColor *)tintColor;


#pragma mark - UIPageControl

+ (void)customizePageControlCurrentPageColor:(UIColor *)mainColor;


@end
