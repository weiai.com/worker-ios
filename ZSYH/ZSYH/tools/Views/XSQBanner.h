//
//  XSQBbanner.h
//  kongge
//
//  Created by xiaoshunliang on 16/2/1.
//  Copyright © 2016年 nil. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageView+WebCache.h"

typedef NS_ENUM(NSInteger,PageControlLocation) {
    PageControlLocationLeft=0,
    PageControlLocationCenter,
    PageControlLocationRight,
};
typedef void(^imageClickBlock)(NSInteger index);

@interface XSQBanner : UIView
//轮播的页码
@property(strong,nonatomic) UIPageControl *pageVC;
//轮播滚动时间间隔
@property(assign,nonatomic) CGFloat time;
//pageControl 的位置
@property (nonatomic, assign) PageControlLocation pageLocation;
/**
 *  更新图片数组
 */
- (void)updateImageArr:(NSArray *)imageNameArray AndImageClickBlock:(imageClickBlock)clickBlock;

#pragma mark 开始定时器
-(void)beginTimer;

#pragma mark 销毁定时器
-(void)stopTimer;

#pragma mark 自定义UIPageControl样式（图片大小要合适）
-(void)customPageVcWithSelectedImg:(UIImage *)selectedImg NormalImg:(UIImage *)normalImg;

@end
