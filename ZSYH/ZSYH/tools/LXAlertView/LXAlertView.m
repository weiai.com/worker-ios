//
//  LXAlertView.m
//  LXAlertViewDemo
//
//  Created by 刘鑫 on 16/4/15.
//  Copyright © 2016年 liuxin. All rights reserved.
//

//弹框的相关参数

#define AlertTitleTop           kScaleNum(15)
#define AlertTitleH             kScaleNum(15)
#define AlertButtonTop          kScaleNum(15)
#define AlertButtonH            kScaleNum(48)
#define AlertLineH              0.5
#define AlertTopBarH            kScaleNum(4)
#define AlertContentLRPadding   kScaleNum(28)
#define AlertContentTBPadding   40


#define AlertLRPadding      kScaleNum(20)
#define AlertW              (kScreen_Width - 2 * AlertLRPadding)
#define AlertContentW       (AlertW - 2 * AlertContentLRPadding)
#define AlertContentTop     kScaleNum(28)
#define AlertContentBottom  kScaleNum(24)

//弹窗所处的背景色
#define kAlertBG [[UIColor blackColor] colorWithAlphaComponent:0.6]
#define MainScreenRect [UIScreen mainScreen].bounds
#define AlertView_W     kScaleNum(270.0f)
#define AlertView_MinH     kScaleNum(300 / 2)
#define MessageMin_H    kScaleNum(60.0f)      //messagelab的最小高度
#define MessageMAX_H    kScaleNum(120.0f)      //messagelab的最大高度，当超过时，文本会以...结尾

#define SFQBlueColor        [UIColor colorWithRed:9/255.0 green:170/255.0 blue:238/255.0 alpha:1]
#define SFQRedColor         [UIColor colorWithRed:64/255.0 green:185/255.0 blue:213/255.0 alpha:1]
#define SFQLightGrayColor   [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1]

#define LXADTitleFont       [UIFont boldSystemFontOfSize:17];
#define LXADMessageFont     [UIFont systemFontOfSize:15];
#define LXADBtnTitleFont    [UIFont systemFontOfSize:15];

//#import "Const.h"
#import "LXAlertView.h"
#import "UILabel+LXAdd.h"
#import "BlockButton.h"
#import "NSString+AttributedString.h"

//#import "BlockButton.h"
//#import <UIKit/UIKit.h>

@interface LXAlertView()<UIWebViewDelegate, UITextViewDelegate> {
    NSString *_aliPayUrl;
    NSString *_weiXinPayUrl;
}
@property (nonatomic, strong) UIWindow *alertWindow;
@property (nonatomic, strong) UIView *alertView;

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIImageView *placeholderImage;
@property (nonatomic, strong) UILabel *placeLabel;
@property (nonatomic, strong) UILabel *messageLab;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UIButton *otherBtn;
@end

@implementation LXAlertView

/**
 初始化不带标题的alert方法（上半部满铺图片、内容、一个按钮）
 */
+ (void)initWithAlertTitleImage:(UIImage *)titleImage
                             Message:(NSString *)message
                           sureTitle:(NSString *)sureTitle
                         sureClicked:(void(^)(LXAlertView *alertView))sureClicked {
    Class class = NSClassFromString(@"LXAlertView");
    id alertView = [[class alloc] init];
    [alertView initWithAlertTitleImage:titleImage Message:message sureTitle:sureTitle sureClicked:sureClicked];
}
/**
 *  初始化不带标题的alert方法（上半部满铺图片、内容、一个按钮）
 */
- (void)initWithAlertTitleImage:(UIImage *)titleImage
                                    Message:(NSString *)message
                                  sureTitle:(NSString *)sureTitle
                         sureClicked:(void(^)(LXAlertView *alertView))sureClicked {
    if ([self init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(6);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        
        //UIImage *titleImage = [UIImage imageNamed:@"successapy"];
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeScaleAspectFill;
        titleImageView.backgroundColor = [UIColor clearColor];
        titleImageView.image = titleImage;
        [alertV addSubview:titleImageView];
        CGFloat titleImageViewHeight = titleImage.size.height / titleImage.size.width * AlertW;
        
        //-----------没有title
        
        //-----------message
        UILabel *contentLabel = [[UILabel alloc]init];
        contentLabel.lineSpace = 6;
        contentLabel.text = message;
        contentLabel.numberOfLines = 0;
        contentLabel.textColor = kMainTextColor33;
        contentLabel.font = kFont(16);
        [alertV addSubview:contentLabel];
        
        //-----------line(竖线)
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = [UIColor colorWithRed:240/256.0 green:240/256.0 blue:240/256.0 alpha:1];
        [alertV addSubview:bottomLine];
        bottomLine.hidden = YES;
        
        //-----------没有cancelBtn
        
        //-----------sureBtn
        BlockButton *sureBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        [sureBtn setTitle:sureTitle forState:UIControlStateNormal];
        sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        sureBtn.backgroundColor = kMain_Color;
        sureBtn.titleLabel.font = kFont(16);
        [sureBtn rounded:kScaleNum(4)];
        [sureBtn addClickBlock:^{
            sureClicked(self);
            [self dismissAlertView];
        }];
        [alertV addSubview:sureBtn];
        
        CGFloat contentHeight;
        //-----------宽度计算
        CGSize size = [contentLabel.text sizeWithFont:contentLabel.font];
        if (size.width < AlertContentW) {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
            contentHeight = contentHeight*2;
        }else
        {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
        }
        
        CGFloat alertViewH = AlertContentTop + titleImageViewHeight + AlertTitleTop + AlertTitleH + AlertTitleTop + contentHeight + AlertButtonTop + AlertButtonH + AlertContentBottom;
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center = self.center;
        [self addSubview:alertV];
        
        [titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV).mas_offset(0);
            make.centerX.equalTo(alertV);
            make.width.mas_equalTo(AlertW);
            make.height.mas_equalTo(titleImageViewHeight);
        }];
        
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleImageView.mas_bottom).offset(kScaleNum(42));
            make.left.mas_equalTo(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(ceil(contentHeight));
        }];
        
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).offset(kScaleNum(25));
            make.left.width.equalTo(alertV);
            make.height.mas_equalTo(AlertLineH);
        }];
        
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).mas_offset(kScaleNum(24));
            make.left.equalTo(alertV).mas_offset(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(AlertButtonH);
        }];
    }
    [self showLXAlertView];
}

/**
 初始化带标题的alert方法（内容、两个按钮）
 */
- (instancetype)initWithTitleAlertTitle:(NSString *)title
                                Message:(NSString *)message
                                  sureTitle:(NSString *)sureTitle
                                cancleTitle:(NSString *)cancleTitle
                                sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                              cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked {
    
    if (self = [super init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIControl *backControl = [[UIControl alloc]initWithFrame:self.frame];
        backControl.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.00];
        [backControl addTarget:self action:@selector(dismissAlertView) forControlEvents:UIControlEventTouchUpInside];
        backControl.alpha = 1.0;
        [self addSubview:backControl];
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(10);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        
        //-----------title
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.numberOfLines = 0;
        titleLabel.font = kFont(18);
        [alertV addSubview:titleLabel];
        
        //-----------message
        UILabel *contentLabel = [[UILabel alloc]init];
        contentLabel.attributedText = [NSString getAttributedStringWithString:message Color:[UIColor blackColor] font:kFont(14) lineSpacing:3];
        contentLabel.numberOfLines = 0;
        [alertV addSubview:contentLabel];
        
        //-----------line(横线、竖线)
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = [UIColor lightGrayColor];
        [alertV addSubview:bottomLine];
        
        UIView *verLine = [[UIView alloc]init];
        verLine.backgroundColor = [UIColor lightGrayColor];
        [alertV addSubview:verLine];
        
        //-----------cancelBtn
        kWeakSelf;
        BlockButton *cancelBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.backgroundColor = [UIColor whiteColor];
        [cancelBtn setTitle:cancleTitle forState:UIControlStateNormal];
        [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [cancelBtn addClickBlock:^{
            cancleClicked(weakSelf);
            [weakSelf dismissAlertView];
        }];
        [alertV addSubview:cancelBtn];
        
        //-----------sureBtn
        BlockButton *sureBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        [sureBtn setTitle:sureTitle forState:UIControlStateNormal];
        sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sureBtn addClickBlock:^{
            sureClicked(weakSelf);
            [weakSelf dismissAlertView];
        }];
        [alertV addSubview:sureBtn];
        
        CGFloat contentHeight;
        //-----------高度计算
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(AlertContentW, CGFLOAT_MAX)];
        if (size.width < AlertContentW) {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            contentHeight = size.height;
        }else
        {
            contentLabel.textAlignment = NSTextAlignmentLeft;
            contentHeight = size.height;
        }
        
        
        CGFloat alertViewH = AlertTitleTop + AlertTitleH + AlertContentTop + contentHeight + AlertContentBottom + AlertLineH + AlertButtonH;
        if (alertViewH < AlertView_MinH) {
            //alertViewH = AlertView_MinH;
        }
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center=self.center;
        [self addSubview:alertV];
        
        //约束
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV.mas_top).mas_offset(AlertTitleTop);
            make.left.equalTo(alertV.mas_left).mas_offset(AlertContentLRPadding);
            make.right.equalTo(alertV.mas_right).mas_offset(-AlertContentLRPadding);
            make.height.mas_equalTo(AlertTitleH);
        }];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_bottom).mas_offset(AlertContentTop);
            make.left.mas_equalTo(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(contentHeight);
        }];
        
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(alertV.mas_bottom).offset(-AlertButtonH);
            make.left.width.equalTo(alertV);
            make.height.mas_equalTo(AlertLineH);
        }];
        
        [verLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bottomLine.mas_bottom);
            make.centerX.equalTo(alertV);
            make.width.mas_equalTo(AlertLineH);
            make.height.mas_equalTo(AlertButtonH);
        }];
        
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(alertV);
            make.right.mas_equalTo(verLine.mas_left);
            make.top.height.equalTo(verLine);
        }];
        
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(alertV).mas_offset(kScaleNum(1));
            make.left.mas_equalTo(verLine.mas_right);
            make.top.height.equalTo(verLine);
        }];
    }
    [self showLXAlertView];
    return self;
}

/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
+ (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked {
    Class class = NSClassFromString(@"LXAlertView");
    id alertView = [[class alloc] init];
    [alertView initWithTitleAlertTitle:title titleImage:titleImage Message:message sureTitle:sureTitle sureClicked:sureClicked];
}

/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
- (void)initWithTitleAlertTitle:(NSString *)title
                             titleImage:(UIImage *)titleImage
                                Message:(NSString *)message
                              sureTitle:(NSString *)sureTitle
                            sureClicked:(void(^)(LXAlertView *alertView))sureClicked {
    if ([self init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(6);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        
        //UIImage *titleImage = [UIImage imageNamed:@"successapy"];
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeScaleAspectFill;
        titleImageView.backgroundColor = [UIColor clearColor];
        titleImageView.image = titleImage;
        [alertV addSubview:titleImageView];
        
        //-----------title
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = kMainTextColor33;
        titleLabel.numberOfLines = 0;
        titleLabel.font = kFont(16);
        [alertV addSubview:titleLabel];
        
        //-----------message
        UILabel *contentLabel = [[UILabel alloc]init];
        contentLabel.lineSpace = 6;
        contentLabel.text = message;
        contentLabel.numberOfLines = 0;
        contentLabel.textColor = kMainTextColor33;
        contentLabel.font = kFont(16);
        [alertV addSubview:contentLabel];
        
        //-----------line(竖线)
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = [UIColor colorWithRed:240/256.0 green:240/256.0 blue:240/256.0 alpha:1];
        [alertV addSubview:bottomLine];
        bottomLine.hidden = YES;
        
        //-----------没有cancelBtn
        
        
        //-----------sureBtn
        BlockButton *sureBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        [sureBtn setTitle:sureTitle forState:UIControlStateNormal];
        sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        sureBtn.backgroundColor = kMain_Color;
        sureBtn.titleLabel.font = kFont(16);
        [sureBtn rounded:kScaleNum(4)];
        [sureBtn addClickBlock:^{
            sureClicked(self);
            [self dismissAlertView];
        }];
        [alertV addSubview:sureBtn];
        
        CGFloat contentHeight;
        //-----------宽度计算
        CGSize size = [contentLabel.text sizeWithFont:contentLabel.font];
        if (size.width < AlertContentW) {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
            contentHeight = contentHeight*2;
        }else
        {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
        }
        
        CGFloat alertViewH = AlertContentTop + titleImage.size.height + AlertTitleTop + AlertTitleH + AlertTitleTop + contentHeight + AlertButtonTop + AlertButtonH + AlertContentBottom;
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center = self.center;
        [self addSubview:alertV];
        
        [titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV).mas_offset(AlertContentTop);
            make.centerX.equalTo(alertV);
            make.width.mas_equalTo(titleImage.size.width);
            make.height.mas_equalTo(titleImage.size.height);
        }];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleImageView.mas_bottom).mas_offset(AlertTitleTop);
            make.left.equalTo(alertV).mas_offset(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(AlertTitleH);
        }];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabel.mas_bottom).offset(AlertTitleTop);
            make.left.mas_equalTo(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(ceil(contentHeight));
        }];
        
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).offset(kScaleNum(25));
            make.left.width.equalTo(alertV);
            make.height.mas_equalTo(AlertLineH);
        }];
        
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).mas_offset(AlertButtonTop);
            make.left.equalTo(alertV).mas_offset(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(AlertButtonH);
        }];
    }
    [self showLXAlertView];
}

/**
 初始化带标题的alert方法（状态图片、标题、内容、两个按钮）
 */
+ (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    cancleTitle:(NSString *)cancleTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                  cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked {
    Class class = NSClassFromString(@"LXAlertView");
    id alertView = [[class alloc] init];
    [alertView initWithTitleAlertTitle:title titleImage:titleImage Message:message sureTitle:sureTitle cancleTitle:cancleTitle sureClicked:sureClicked cancleClicked:cancleClicked];
}
/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
- (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    cancleTitle:(NSString *)cancleTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                  cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked {
    if ([self init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(6);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        
        UIImageView *titleImageView = [[UIImageView alloc] init];
        titleImageView.contentMode = UIViewContentModeScaleAspectFill;
        titleImageView.backgroundColor = [UIColor clearColor];
        titleImageView.image = titleImage;
        [alertV addSubview:titleImageView];
        
        //-----------title
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = kMainTextColor33;
        titleLabel.numberOfLines = 0;
        titleLabel.font = kFont(16);
        [alertV addSubview:titleLabel];
        
        //-----------message
        UILabel *contentLabel = [[UILabel alloc]init];
        contentLabel.lineSpace = 6;
        contentLabel.text = message;
        contentLabel.numberOfLines = 0;
        contentLabel.textColor = kMainTextColor33;
        contentLabel.font = kFont(16);
        [alertV addSubview:contentLabel];
        
        //-----------line(竖线)
        UIView *bottomLine = [[UIView alloc]init];
        bottomLine.backgroundColor = [UIColor colorWithRed:240/256.0 green:240/256.0 blue:240/256.0 alpha:1];
        [alertV addSubview:bottomLine];
        bottomLine.hidden = YES;
        
        //-----------cancelBtn
        BlockButton *cancleButton = [BlockButton buttonWithType:UIButtonTypeCustom];
        [cancleButton setTitle:cancleTitle forState:UIControlStateNormal];
        cancleButton.titleLabel.font = [UIFont systemFontOfSize:16];
        cancleButton.backgroundColor = kColor(237, 237, 237);
        [cancleButton setTitleColor:kColor(150, 150, 150) forState:UIControlStateNormal];
        cancleButton.titleLabel.font = kFont(16);
        [cancleButton rounded:kScaleNum(4)];
        [cancleButton addClickBlock:^{
            cancleClicked(self);
            [self dismissAlertView];
        }];
        [alertV addSubview:cancleButton];
        
        //-----------sureBtn
        BlockButton *sureBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        [sureBtn setTitle:sureTitle forState:UIControlStateNormal];
        sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        sureBtn.backgroundColor = kMain_Color;
        sureBtn.titleLabel.font = kFont(16);
        [sureBtn rounded:kScaleNum(4)];
        [sureBtn addClickBlock:^{
            sureClicked(self);
            [self dismissAlertView];
        }];
        [alertV addSubview:sureBtn];
        
        CGFloat contentHeight;
        //-----------宽度计算
        CGSize size = [contentLabel.text sizeWithFont:contentLabel.font];
        if (size.width < AlertContentW) {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
            contentHeight = contentHeight*2;
        }else
        {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            contentHeight = [contentLabel getLableRectWithMaxWidth:AlertContentW].height;
        }
        
        CGFloat alertViewH = AlertContentTop + titleImage.size.height + AlertTitleTop + AlertTitleH + AlertTitleTop + contentHeight + AlertButtonTop + AlertButtonH + AlertContentBottom;
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center = self.center;
        [self addSubview:alertV];
        
        [titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV).mas_offset(AlertContentTop);
            make.centerX.equalTo(alertV);
            make.width.mas_equalTo(titleImage.size.width);
            make.height.mas_equalTo(titleImage.size.height);
        }];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleImageView.mas_bottom).mas_offset(AlertTitleTop);
            make.left.equalTo(alertV).mas_offset(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(AlertTitleH);
        }];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(titleLabel.mas_bottom).offset(AlertTitleTop);
            make.left.mas_equalTo(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(ceil(contentHeight));
        }];
        
        [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).offset(kScaleNum(25));
            make.left.width.equalTo(alertV);
            make.height.mas_equalTo(AlertLineH);
        }];
        
        [cancleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).mas_offset(AlertButtonTop);
            make.right.equalTo(alertV.mas_centerX).mas_offset(kScaleNum(-15));
            make.width.mas_equalTo(kScaleNum(113));
            make.height.mas_equalTo(kScaleNum(36));
        }];
        
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(contentLabel.mas_bottom).mas_offset(AlertButtonTop);
            make.left.equalTo(alertV.mas_centerX).mas_offset(kScaleNum(15));
            make.width.mas_equalTo(kScaleNum(113));
            make.height.mas_equalTo(kScaleNum(36));
        }];
    }
    [self showLXAlertView];
}

/**
 初始化带标题的alert方法（内容、两个图片、无按钮）
 */
- (instancetype)initWithTitleAlertTitle:(NSString *)title
                                Message:(NSString *)message
                          firstImageUrl:(NSString *)firstImageUrl
                         secondImageUrl:(NSString *)secondImageUrl
                          cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked {
    if (self = [super init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIControl *backControl = [[UIControl alloc]initWithFrame:self.frame];
        backControl.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.00];
        [backControl addTarget:self action:@selector(dismissAlertView) forControlEvents:UIControlEventTouchUpInside];
        backControl.alpha = 1.0;
        [self addSubview:backControl];
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(4);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        [self addSubview:alertV];
        
        //-----------title
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.numberOfLines = 0;
        titleLabel.font = kFont(17);
        [alertV addSubview:titleLabel];
        
        UIImageView *tipsImageView = [[UIImageView alloc] init];
        tipsImageView.contentMode = UIViewContentModeCenter;
        tipsImageView.backgroundColor = [UIColor clearColor];
        tipsImageView.image = imgname(@"state3");
        [alertV addSubview:tipsImageView];
        
        //-----------message
        UILabel *contentLabel = [[UILabel alloc]init];
        contentLabel.text = message;
        contentLabel.textColor = K666666;
        contentLabel.numberOfLines = 0;
        contentLabel.font = kFont(14);
        //[contentLabel setLineSpace:5];//和标签的getLableRectWithMaxWidth结合使用
        [alertV addSubview:contentLabel];
        
        //第一个图片
        UIImageView *firstImageView = [[UIImageView alloc] init];
        firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        firstImageView.backgroundColor = [UIColor clearColor];
        firstImageView.image = imgname(firstImageUrl);
        firstImageView.layer.masksToBounds = YES;
        [alertV addSubview:firstImageView];
        
        //第二个图片
        UIImageView *secondImageView = [[UIImageView alloc] init];
        secondImageView.contentMode = UIViewContentModeScaleAspectFill;
        secondImageView.backgroundColor = [UIColor clearColor];
        secondImageView.image = imgname(secondImageUrl);
        secondImageView.layer.masksToBounds = YES;
        [alertV addSubview:secondImageView];
        
        //-----------cancelBtn
        kWeakSelf;
        BlockButton *cancelBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.backgroundColor = [UIColor whiteColor];
        [cancelBtn setImage:imgname(@"closeAlert") forState:UIControlStateNormal];
        [cancelBtn addClickBlock:^{
            cancleClicked(weakSelf);
            [weakSelf dismissAlertView];
        }];
        [alertV addSubview:cancelBtn];
        
        CGFloat contentHeight;
        //-----------高度计算
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(AlertContentW, CGFLOAT_MAX)];
        if (size.width < AlertContentW) {
            contentLabel.textAlignment = NSTextAlignmentCenter;
            contentHeight = size.height;
        }else
        {
            contentLabel.textAlignment = NSTextAlignmentLeft;
            contentHeight = size.height;
        }
        
        
        CGFloat alertViewH = kScaleNum(15) + AlertTitleH + kScaleNum(15) + contentHeight + kScaleNum(15) + kScaleNum(90) + kScaleNum(10) + kScaleNum(90) + kScaleNum(10);
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center=self.center;
        [self addSubview:alertV];
        
        //约束
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV.mas_top).mas_offset(kScaleNum(15));
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(AlertTitleH);
        }];
        
        [tipsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleLabel);
            make.right.equalTo(titleLabel.mas_left).mas_offset(kScaleNum(-5));
            
        }];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(titleLabel);
            make.right.equalTo(alertV).mas_offset(kScaleNum(-10));
        }];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(titleLabel.mas_bottom).mas_offset(kScaleNum(15));
            make.left.mas_equalTo(AlertContentLRPadding);
            make.centerX.equalTo(alertV);
            make.height.mas_equalTo(contentHeight);
        }];
        
        [firstImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(contentLabel.mas_bottom).mas_offset(kScaleNum(15));
            make.left.equalTo(alertV).mas_offset(kScaleNum(10));
            make.right.equalTo(alertV).mas_offset(kScaleNum(-10));
            make.height.mas_equalTo(kScaleNum(90));
        }];
        
        [secondImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(firstImageView.mas_bottom).mas_offset(kScaleNum(10));
            make.left.equalTo(alertV).mas_offset(kScaleNum(10));
            make.right.equalTo(alertV).mas_offset(kScaleNum(-10));
            make.height.mas_equalTo(kScaleNum(90));
        }];
    }
    [self showLXAlertView];
    return self;
}

/**
 遍历构造初始化不带标题/一个按钮的alert方法（内容、一个按钮）
 */
+ (void)initAlertWithQRCodeImage:(UIImage *)qrCodeImage headImageUrlString:(NSString *)headImageUrlString title:(NSString *)title sureTitle:(NSString *)sureTitle sureClicked:(void (^)(LXAlertView *))sureClicked cancleClicked:(void (^)(LXAlertView *))cancleClicked {
    Class class = NSClassFromString(@"LXAlertView");
    id alertView = [[class alloc] init];
    [alertView initAlertWithQRCodeImage:qrCodeImage headImageUrlString:headImageUrlString title:title sureTitle:sureTitle sureClicked:sureClicked cancleClicked:cancleClicked];
}
/**
 初始化带二维码推广弹框 (头像,手机号,二维码,保存按钮)
 */
- (void)initAlertWithQRCodeImage:(UIImage *)qrCodeImage
                               headImageUrlString:(NSString *)headImageUrlString
                                            title:(NSString *)title
                                        sureTitle:(NSString *)sureTitle
                                      sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                           cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked {
    if ([self init]) {
        self.frame = MainScreenRect;
        self.backgroundColor = kAlertBG;
        UIControl *backControl = [[UIControl alloc]initWithFrame:self.frame];
        backControl.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.00];
        [backControl addTarget:self action:@selector(dismissAlertView) forControlEvents:UIControlEventTouchUpInside];
        backControl.alpha = 1.0;
        [self addSubview:backControl];
        UIView *alertV = [[UIView alloc] init];
        alertV.layer.cornerRadius = kScaleNum(10);
        alertV.layer.masksToBounds = YES;
        alertV.backgroundColor = [UIColor whiteColor];
        
        UIImageView *headImageView = [[UIImageView alloc] init];
        headImageView.contentMode = UIViewContentModeScaleAspectFill;
        headImageView.backgroundColor = [UIColor clearColor];
        headImageView.layer.cornerRadius = kScaleNum(20);
        headImageView.layer.masksToBounds = YES;
        [headImageView sd_setImageWithURL:[NSURL URLWithString:headImageUrlString] placeholderImage:imgname(@"")];
        [alertV addSubview:headImageView];
        
        //-----------title
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.numberOfLines = 0;
        titleLabel.font = kFont(14);
        [alertV addSubview:titleLabel];
        
        UIView *QRCodeBackView = [[UIView alloc] init];
        QRCodeBackView.backgroundColor = [UIColor clearColor];
        QRCodeBackView.layer.cornerRadius = kScaleNum(5);
        [alertV addSubview:QRCodeBackView];
        
        UIImageView *QRCodeImageView = [[UIImageView alloc] init];
        QRCodeImageView.contentMode = UIViewContentModeScaleAspectFill;
        QRCodeImageView.backgroundColor = [UIColor lightGrayColor];
        QRCodeImageView.image = qrCodeImage;
        [QRCodeBackView addSubview:QRCodeImageView];
        
        //-----------cancelBtn
        kWeakSelf;
        BlockButton *cancelBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.backgroundColor = [UIColor whiteColor];
        [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cancelBtn setImage:imgname(@"close") forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [cancelBtn addClickBlock:^{
            cancleClicked(weakSelf);
            [weakSelf dismissAlertView];
        }];
        [alertV addSubview:cancelBtn];
        
        //-----------sureBtn
        BlockButton *sureBtn = [BlockButton buttonWithType:UIButtonTypeCustom];
        sureBtn.backgroundColor = [UIColor colorWithHex:0xff9e24 alpha:1];
        sureBtn.titleLabel.font = kFont(16);
        [sureBtn setTitle:sureTitle forState:UIControlStateNormal];
        [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sureBtn addClickBlock:^{
            sureClicked(weakSelf);
            [weakSelf dismissAlertView];
        }];
        [alertV addSubview:sureBtn];
        
        
        CGFloat alertViewH = kScaleNum(20) + kScaleNum(40) + kScaleNum(20) + AlertW - kScaleNum(40) + kScaleNum(20) + kScaleNum(35) + kScaleNum(20);
        if (alertViewH < AlertView_MinH) {
            //alertViewH = AlertView_MinH;
        }
        alertV.frame = CGRectMake(0, 0, AlertW, alertViewH);
        alertV.center=self.center;
        [self addSubview:alertV];
        
        //约束
        [headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV).mas_offset(kScaleNum(20));
            make.left.equalTo(alertV).mas_offset(kScaleNum(20));
            make.width.height.height.mas_equalTo(kScaleNum(40));
        }];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(headImageView);
            make.left.equalTo(headImageView.mas_right).mas_offset(kScaleNum(10));
            make.right.equalTo(alertV.mas_right).mas_offset(kScaleNum(-60));
        }];
        
        [QRCodeBackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(headImageView.mas_bottom).mas_offset(kScaleNum(20));
            make.left.equalTo(alertV).mas_offset(kScaleNum(20));
            make.width.height.mas_equalTo(AlertW - kScaleNum(40));
        }];
        [QRCodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(QRCodeBackView);
        }];
        
        [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(alertV).mas_offset(kScaleNum(20));
            make.right.equalTo(alertV).mas_offset(kScaleNum(-20));
            make.width.height.mas_equalTo(kScaleNum(40));
        }];
        
        [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(alertV).mas_offset(kScaleNum(-20));
            make.left.equalTo(alertV).mas_offset(kScaleNum(20));
            make.top.equalTo(QRCodeImageView.mas_bottom).mas_offset(kScaleNum(20));
        }];
    }
    [self showLXAlertView];
    
}



-(void)btnClick:(UIButton *)btn{
    
    if (self.clickBlock) {
        self.clickBlock(btn.tag);
    }
    
    if (!_dontDissmiss) {
        [self dismissAlertView];
    }
    
}

-(void)setDontDissmiss:(BOOL)dontDissmiss{
    _dontDissmiss=dontDissmiss;
}
-(void)showLXAlertView{
    
    
//    _alertWindow=[[UIWindow alloc] initWithFrame:MainScreenRect];
//    _alertWindow.windowLevel=UIWindowLevelAlert;
//    
//    [_alertWindow becomeKeyWindow];
//    [_alertWindow makeKeyAndVisible];
    
//    [_alertWindow addSubview:self];
    
//    UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
//    
//    [window addSubview:self];
    [[[UIApplication sharedApplication].delegate window] addSubview:self];
    
//    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setShowAnimation];
    
}
- (void)showLXAlertViewToSender:(UIView *)sender {
    [sender addSubview:self];
    [self setShowAnimation];
}
-(void)dismissAlertView{
    [self removeFromSuperview];
//    [_alertWindow resignKeyWindow];
    
}

-(void)setShowAnimation{
    
    switch (_animationStyle) {
            
        case LXASAnimationDefault:
        {
            [UIView animateWithDuration:0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                [_alertView.layer setValue:@(0) forKeyPath:@"transform.scale"];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.23 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [_alertView.layer setValue:@(1.2) forKeyPath:@"transform.scale"];
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.09 delay:0.02 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                        [_alertView.layer setValue:@(.9) forKeyPath:@"transform.scale"];
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.05 delay:0.02 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                            [_alertView.layer setValue:@(1.0) forKeyPath:@"transform.scale"];
                        } completion:^(BOOL finished) {
                            
                        }];
                    }];
                }];
            }];
        }
            break;
            
        case LXASAnimationLeftShake:{
    
            CGPoint startPoint = CGPointMake(-AlertView_W, self.center.y);
            _alertView.layer.position=startPoint;
            
            //damping:阻尼，范围0-1，阻尼越接近于0，弹性效果越明显
            //velocity:弹性复位的速度
            [UIView animateWithDuration:.8 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
                _alertView.layer.position=self.center;
                
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
            
        case LXASAnimationTopShake:{
            
            CGPoint startPoint = CGPointMake(self.center.x, -_alertView.frame.size.height);
            _alertView.layer.position=startPoint;
            
            //damping:阻尼，范围0-1，阻尼越接近于0，弹性效果越明显
            //velocity:弹性复位的速度
            [UIView animateWithDuration:.8 delay:0 usingSpringWithDamping:.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
                _alertView.layer.position=self.center;
                
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
            
        case LXASAnimationNO:{
            
        }
            
            break;
            
        default:
            break;
    }
    
}


-(void)setAnimationStyle:(LXAShowAnimationStyle)animationStyle{
    _animationStyle=animationStyle;
}

- (void)dealloc {
    NSLog(@"LXAlertView释放");
}
@end

