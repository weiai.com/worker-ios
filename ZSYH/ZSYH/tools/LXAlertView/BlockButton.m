//
//  BlockButton.m
//  边框标签
//
//  Created by simpleplan004 on 17/3/15.
//  Copyright © 2017年 Teplot_03. All rights reserved.
//

#import "BlockButton.h"

@implementation BlockButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addTarget:self action:@selector(clickAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)clickAction {
    self.selected = YES;
    if (self.block) {
        self.block();
    }
}
- (void)addClickBlock:(ClickBlock)block {
    self.block = block;
}
@end
