//
//  LXAlertView.h
//  LXAlertViewDemo
//
//  Created by 刘鑫 on 16/4/15.
//  Copyright © 2016年 liuxin. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger , LXAShowAnimationStyle) {
    LXASAnimationDefault    = 0,
    LXASAnimationLeftShake  ,
    LXASAnimationTopShake   ,
    LXASAnimationNO         ,
};

typedef void(^LXAlertClickIndexBlock)(NSInteger clickIndex);


@interface LXAlertView : UIView

@property (nonatomic,copy)LXAlertClickIndexBlock clickBlock;

@property (nonatomic,assign)LXAShowAnimationStyle animationStyle;

/**
 初始化不带标题的alert方法（上半部满铺图片、内容、一个按钮）
 */
+ (void)initWithAlertTitleImage:(UIImage *)titleImage
                             Message:(NSString *)message
                           sureTitle:(NSString *)sureTitle
                         sureClicked:(void(^)(LXAlertView *alertView))sureClicked;
/**
 *  初始化不带标题的alert方法（上半部满铺图片、内容、一个按钮）
 */
- (void)initWithAlertTitleImage:(UIImage *)titleImage
                                    Message:(NSString *)message
                                  sureTitle:(NSString *)sureTitle
                                sureClicked:(void(^)(LXAlertView *alertView))sureClicked;

/**
 初始化带标题的alert方法（内容、两个按钮）
 */
- (instancetype)initWithTitleAlertTitle:(NSString *)title
                                Message:(NSString *)message
                              sureTitle:(NSString *)sureTitle
                            cancleTitle:(NSString *)cancleTitle
                            sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                          cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;

/**
 初始化带标题的alert方法（内容、两个图片、无按钮）
 */
- (instancetype)initWithTitleAlertTitle:(NSString *)title
                                Message:(NSString *)message
                              firstImageUrl:(NSString *)firstImageUrl
                            secondImageUrl:(NSString *)secondImageUrl
                          cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;
/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
+ (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked;
/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
- (void)initWithTitleAlertTitle:(NSString *)title
                             titleImage:(UIImage *)titleImage
                                Message:(NSString *)message
                              sureTitle:(NSString *)sureTitle
                            sureClicked:(void(^)(LXAlertView *alertView))sureClicked;
/**
 初始化带标题的alert方法（状态图片、标题、内容、两个按钮）
 */
+ (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    cancleTitle:(NSString *)cancleTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                  cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;
/**
 初始化带标题的alert方法（状态图片、标题、内容、一个按钮）
 */
- (void)initWithTitleAlertTitle:(NSString *)title
                     titleImage:(UIImage *)titleImage
                        Message:(NSString *)message
                      sureTitle:(NSString *)sureTitle
                    cancleTitle:(NSString *)cancleTitle
                    sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                  cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;
/**
 遍历构造初始化不带标题/一个按钮的alert方法（内容、一个按钮）
 */
+ (void)initAlertWithQRCodeImage:(UIImage *)qrCodeImage
                       headImageUrlString:(NSString *)headImageUrlString
                                    title:(NSString *)title
                                sureTitle:(NSString *)sureTitle
                              sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                            cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;
/**
 初始化带二维码推广弹框 (头像,手机号,二维码,保存按钮)
 */
- (void)initAlertWithQRCodeImage:(UIImage *)qrCodeImage
                               headImageUrlString:(NSString *)headImageUrlString
                                            title:(NSString *)title
                                        sureTitle:(NSString *)sureTitle
                                      sureClicked:(void(^)(LXAlertView *alertView))sureClicked
                                    cancleClicked:(void(^)(LXAlertView *alertView))cancleClicked;

/**
 *  showLXAlertView
 */

-(void)showLXAlertView;

- (void)showLXAlertViewToSender:(UIView *)sender;
/**
 *  dismissAlertView
 */
-(void)dismissAlertView;
/**
 *  不隐藏，默认为NO。设置为YES时点击按钮alertView不会消失（适合在强制升级时使用）
 */
@property (nonatomic,assign)BOOL dontDissmiss;
@end

