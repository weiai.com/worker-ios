//
//  BlockButton.h
//  边框标签
//
//  Created by simpleplan004 on 17/3/15.
//  Copyright © 2017年 Teplot_03. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ClickBlock) ();

@interface BlockButton : UIButton

@property (nonatomic, copy) ClickBlock block;
- (void)addClickBlock:(ClickBlock)block;
@end
