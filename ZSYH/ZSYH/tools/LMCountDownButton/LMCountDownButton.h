//
//  LMCountDownButton.h
//  BusinessBlockChain
//
//  Created by shien_ios1 on 2018/7/18.
//  Copyright © 2018年 lairui. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMCountDownButton : UIButton

/// 开始倒计时
- (void)startCountDown;

@end
