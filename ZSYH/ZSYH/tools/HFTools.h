//
//  HFTools.h
//  APPStart
//
//  Created by 慧鼎科技 on 2018/11/15.
//  Copyright © 2018 WQ. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HFTools : NSObject

#pragma mark - 指定图片的宽度，获取新图片
+ (UIImage *)imageCompressForWidth:(UIImage *)sourceImage targetWidth:(CGFloat)defineWidth;

#pragma mark - 字符串替换成*
+ (NSString *)replaceStringWithString:(NSString *)str Asterisk:(NSInteger)startLocation length:(NSInteger)length;

#pragma mark - json字符串转字典
+ (NSDictionary *)dictWithJsonString:(NSString *)jsonString;

/**
 接口添加公共参数

 @param dic 不含公共参数的参数字典
 @return 添加公共参数的参数字段
 */
+ (NSDictionary*)addCommonParams:(NSDictionary*)dic;

/**
 含参数 &拼接字典中的字符串

 @param aDic 字符串字典
 @param key 不需要拼接的字符串key
 @return &拼接后的字符串
 */
+ (NSString*)securityKey:(NSDictionary*)aDic exclude:(NSString *)key;

/**
 字典字符串 &转 字符串

 @param aDic 字符串字典
 @return &拼接后的字符串
 */
+ (NSString*)returnStringWithDic:(NSDictionary*)aDic;

/**
 时间戳 转 NSDate

 @param str 时间戳字符串
 @return NSDate时间
 */
+ (NSDate *)dateFromTimeInterva:(NSString *)str;

/**
 获取APP版本号

 @return app版本号
 */
+ (NSString *)getAppVersion;

/**
 拨打电话

 @param mobile 电话号码
 */
+ (void)callMobilePhone:(NSString *)mobile;

/**
 obj转json字符串

 @param obj 对象
 @return 字符串
 */
+ (NSString *)toJSONString:(id)obj;

/**
 格式化小数点

 @param f 数字
 @return 格式化后的字符串
 */
+ (NSString *)formatFloat:(float)f;

/**
 正则电话号码
 
 @param mobileNum 电话号码
 @return 结果
 */
+ (BOOL)isMobileNumberOnly:(NSString *)mobileNum;


/**
 将某个时间转化成 时间戳
 
 @param formatTime 字符串
 @param format YYYY-MM-dd hh:mm:ss
 @return 时间戳
 */
+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;

//图片赋值
+ (void)imageViewUpdateWithUrl:(NSString *)url withImageView:(UIImageView *)imageView withPlaceholderImage:(NSString *)imageName;

+ (UIViewController *)getCurrentVC;

@end

NS_ASSUME_NONNULL_END
