//
//  ZNotificationHeader.h
//  DistributorAB
//
//  Created by LZY on 2019/10/11.
//  Copyright © 2019 主事丫环. All rights reserved.
//

// 发送通知名称汇总

#ifndef ZNotificationHeader_h
#define ZNotificationHeader_h


//AppDelegate发送通知 请求数据刷新界面数据通知
#define APPDELEGATE_REQUEST_REFRESHLISTDATA @"AppDelegate_request_refreshListData"
//我的订单的 二次保修 数据刷新通知
#define WODEDINGDAN_REQUEST_ERCIBAOXIU_LISTDATA @"woDeDingDan_request_erCiBaoXiu_ListData"
//我的订单的 检测单 数据刷新通知
#define WODEDINGDAN_REQUEST_JIANCEDAN_LISTDATA @"woDeDingDan_request_jianCeDan_ListData"


#endif /* ZNotificationHeader_h */
