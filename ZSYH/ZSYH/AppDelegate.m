//
//  AppDelegate.m
//  ZSYH
//
//  Created by 魏堰青 on 2019/2/18.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarViewController.h"
#import "WQAPPStarViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AVFoundation/AVFoundation.h>
#import <UserNotifications/UserNotifications.h>
#import "JSHAREService.h"
#import <AdSupport/AdSupport.h>
#import "JPUSHService.h"
#import "OrderViewController.h"
#import "CSMyorderViewController.h"
#import "ZYGoodObjectPreViewController.h" //配件
#import "ZYNewProductViewController.h" //整机

static NSString *APIKey = @"e5dfc95f027726695c26a9c403cd4ecf";
static NSString *const jpushAppKey = @"ed7048ae9f80a0fd8c577038";
static NSString *const channel = @"07943182d88c60a9996a1685";
static BOOL isProduction = TRUE;

@interface AppDelegate ()<JPUSHRegisterDelegate,JPUSHGeofenceDelegate,AVAudioPlayerDelegate,WXApiDelegate>

@property (nonatomic, retain) AVAudioPlayer *player;
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *firstOrderBgView; //首单 弹窗
@property (nonatomic, strong) UIView *orderTimebgView; //预约时间 弹窗
@property (nonatomic, strong) UIView *orderFinishedBgView; //订单完成 弹窗
@property (nonatomic, strong) UIView *buyAgainBgView; //用户重新下单 弹窗
@property (nonatomic, strong) UIView *bzjView; //补助金已到账 弹窗
@property (nonatomic, strong) UIView *ysjView; //新配件已上架 弹窗
@property (nonatomic, strong) UIView *xitongView; //系统已自动为您接取
@property (nonatomic, strong) NSDictionary *userDic;
@property (nonatomic, strong) UIView *bgViewOne;//有一条新的回复提醒

@end

@implementation AppDelegate

//5cb580e1
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    
    [AMapServices sharedServices].apiKey = APIKey;
    [WXApi registerApp:@"wx0b43bea25a4614a6"];
    
    // 3.0.0及以后版本注册
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    if (@available(iOS 12.0, *)) {
        entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound|JPAuthorizationOptionProvidesAppNotificationSettings;
    } else {
        entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    }
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        //    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        //      NSSet<UNNotificationCategory *> *categories;
        //      entity.categories = categories;
        //    } else {
        //      NSSet<UIUserNotificationCategory *> *categories;
        //      entity.categories = categories;
        //    }
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    [JPUSHService registerLbsGeofenceDelegate:self withLaunchOptions:launchOptions];
    //如不需要使用IDFA，advertisingIdentifier 可为nil
    [JPUSHService setupWithOption:launchOptions appKey:jpushAppKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    
    //2.1.9版本新增获取registration id block接口。
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"registrationID获取成功：%@",registrationID);
        } else {
            NSLog(@"registrationID获取失败，code：%d",resCode);
        }
    }];
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
        //跳转到启动页
        [self firstStart];
        
    } else {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        TabBarViewController *tabbar = [[TabBarViewController alloc]init];
        self.window.rootViewController = tabbar;
    }
    NSString *udid =[USER_DEFAULT objectForKey:@"udid"];
    KMyLog(@"sdfsf%@",udid);
    
    if (strIsEmpty(udid)) {
        udid = [self uuid];
        [USER_DEFAULT setObject:udid forKey:@"udid"];
    }
    
    if([[UIDevice currentDevice].systemVersion doubleValue]>=8.0){//8.0以后使用这种方法来注册推送通知
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    } else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    }
    
    if (@available(iOS 10.0, *)) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        center.delegate = self;
    }
    
    JSHARELaunchConfig *config = [[JSHARELaunchConfig alloc] init];
    config.appKey = @"ed7048ae9f80a0fd8c577038";
    config.SinaWeiboAppKey = @"374535501";
    config.SinaWeiboAppSecret = @"baccd12c166f1df96736b51ffbf600a2";
    config.SinaRedirectUri = @"https://www.jiguang.cn";
    config.QQAppId = @"1105864531";
    config.QQAppKey = @"glFYjkHQGSOCJHMC";
    config.WeChatAppId = @"wx0b43bea25a4614a6";
    config.WeChatAppSecret = @"b4e9479d5ece8a516b8f7991f50c50e6";
    config.FacebookAppID = @"1847959632183996";
    config.FacebookDisplayName = @"JShareDemo";
    config.TwitterConsumerKey = @"4hCeIip1cpTk9oPYeCbYKhVWi";
    config.TwitterConsumerSecret = @"DuIontT8KPSmO2Y1oAvby7tpbWHJimuakpbiAUHEKncbffekmC";
    config.JChatProAuth = @"a7e2ce002d1a071a6ca9f37d";
    
    [JSHAREService setupWithConfig:config];
    
    [JSHAREService setDebug:YES];
    
    return YES;
}

#pragma mark - 收到本地推送
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    NSLog(@"appdfdf收到本地推送(didReceiveLocalNotification:):%@", notification.userInfo);
    
    if (@available(iOS 10.0, *)) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        content.title = [NSString localizedUserNotificationStringForKey:@"Hello77777777" arguments:nil];
        content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"Agent-%d",arc4random()%100] arguments:nil];
        content.sound = [UNNotificationSound defaultSound];
        UNNotificationRequest *requester = [UNNotificationRequest requestWithIdentifier:@"OXNotification" content:content trigger:nil];
        [center addNotificationRequest:requester withCompletionHandler:^(NSError *_Nullable error) {
            NSLog(@"成功添加推送");
        }];
    }
    
    [self tuisonhg:@"tuisonhg"];
}

-(void)tuisonhg:(NSString *)textstr{
    //加载本地音乐
    NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"支付宝" withExtension:@"m4a"];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:fileUrl error:nil];
    if (self.player) {
        [self.player prepareToPlay];
    }
    self.player.volume = 0.5;
    self.player.pan = -1;
    self.player.numberOfLoops = -1;
    self.player.rate = 0.5;
    [self.player play];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler API_AVAILABLE(ios(10.0)){
    NSLog(@"============收到本地推送:%@", response);
}

- (void)networkDidReceiveMessage:(NSNotification *)notification {
    NSDictionary * userInfo = [notification userInfo];
    NSString *content = [userInfo valueForKey:@"content"];
    NSString *messageID = [userInfo valueForKey:@"_j_msgid"];
    NSDictionary *extras = [userInfo valueForKey:@"extras"];
    NSString *customizeField1 = [extras valueForKey:@"customizeField1"]; //服务端传递的 Extras 附加字段，key 是自己定义的
    NSLog(@"极光iOS7及以上系统，收到通知:%@", [self logDic:userInfo]);//没收到
}

#pragma mark - 注册远程推送
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    ///Required - 注册 DeviceToken
    //NSLog(@"推送deviceToken -- >> %@",deviceToken);
    //[Manager sharedManager].tuiSongString = [NSString stringWithFormat:@"%@",deviceToken];
    NSLog(@"deviceToken:%@", deviceToken);
    [JPUSHService registerDeviceToken:deviceToken];
    KMyLog(@"%@ 注册远程推送",KUID);
    if (![KUID isEqualToString:@""]) {//不为空,就注册
            [JPUSHService setAlias:KUID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
                NSLog(@"极光推送设置别名:%ld %@ %ld", iResCode, iAlias, seq);
            } seq:1];
    }
    
    //JPush 监听登陆成功
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkDidLogin:)
                                                 name:kJPFNetworkDidLoginNotification
                                               object:nil];
}

#pragma mark - 注册远程推送失败
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"注册远程推送失败: %@", error);
}

- (void)networkDidLogin:(NSNotification *)notification {
    //[JPUSHService setAlias:KUID completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
    //
    //} seq:1];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kJPFNetworkDidLoginNotification
                                                  object:nil];
    NSString *str  = [NSString stringWithFormat:@"%@",[JPUSHService registrationID]];
    [JPUSHService getAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"推送别名:%ld %@ %ld", (long)iResCode, iAlias, seq);
    } seq:123];
    
    KMyLog(@"registrationID:%@",str);
}

#pragma mark - 收到远程推送
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [JPUSHService handleRemoteNotification:userInfo];
    
    self.userDic = userInfo;
    NSLog(@"iOS7及以上系统，收到远程通知:%@", [self logDic:userInfo]);
    
    NSString *stere = @"";
    NSString *sterename = @"";
    if ([[userInfo allKeys] containsObject:@"subscribeId"]) { //1
        //您有一条新的预约单
        stere = @"您有一条新的预约单";
        sterename = @"yuyuedan";
        [singlTool shareSingTool].needReasfh = YES; ///
        
    } else if ([[userInfo allKeys] containsObject:@"afterOrderToRepairUser"]) { //1
        //您有新的售后订单,请查看
        stere = @"您有新的售后订单";
        sterename = @"shouhouweiixui";
        [singlTool shareSingTool].needReasfh = YES; ///
        
    } else if ([[userInfo allKeys] containsObject:@"userPayToRepairUser"]) { //1
        //用户支付账单
        [self showBgview];
        stere = @"用户支付订单成功";
        sterename = @"dingdanyizhifu";
        [singlTool shareSingTool].needReasfh = YES;///

    } else if ([[userInfo allKeys] containsObject:@"repairUserInsurancePremium"]) { //1
        //首单支付 (当日首单扣除保险费)
        [self showBgviewWithCost:userInfo[@"repairUserInsurancePremium"]]; //James修改
        stere = @"用户支付订单成功";
        sterename = @"dingdanyizhifu";
        [singlTool shareSingTool].needReasfh = YES; ///
        
    } else if ([[userInfo allKeys] containsObject:@"userPayTestReportToRepairUser"]) {
        //用户支付检测费
        [self showBgview];
        stere = @"用户支付检测费成功";
        sterename = @"dingdanyizhifu";
        [singlTool shareSingTool].needReasfh = YES;
        
    } else if ([[userInfo allKeys] containsObject:@"testReportToRepairUserIP"]) { //1
        //当日首单扣除保险费
        [self showBgviewWithCost:userInfo[@"testReportToRepairUserIP"]];
        stere = @"用户支付检测费成功";
        sterename = @"dingdanyizhifu";
        [singlTool shareSingTool].needReasfh = YES;
        
    } else if ([[userInfo allKeys] containsObject:@"accessoriesArrived"]) { //1
        //配件已到货
        [self showBgviewWithMes:userInfo[@"accessoriesArrived"]];
        stere = @"配件已到货";
        [singlTool shareSingTool].needReasfh = YES;
        sterename = @"peijianyidaohuo";
        
    } else if ([[userInfo allKeys] containsObject:@"againSaveAppOrder"]) { //1
        //用户重新下单
        [self showUserMakeOrderAgainBgview];
        [singlTool shareSingTool].needReasfh = YES;
        
    } else if ([[userInfo allKeys] containsObject:@"repairGrants"]) { //1
        //补助金已到账(不是语音,是弹窗)
        [self shwoBzjBgview:userInfo[@"repairGrants"]];
        [singlTool shareSingTool].needReasfh = YES;

    } else if ([[userInfo allKeys] containsObject:@"newParts"]) { //1
        //候保配件上架(不是语音,是弹窗)
        [self shwoPjysjBgview:userInfo[@"newParts"]];
        stere = @"有新的配件上架";
        sterename = @"youxindepeijianshangjia";
        [singlTool shareSingTool].needReasfh = YES;

    } else if ([[userInfo allKeys] containsObject:@"subscribeTestId"]) {
        //您有一条新的检测单 语音
        stere = @"您有一条新的检测单";
        sterename = @"newtestorder";
        [singlTool shareSingTool].needReasfh = YES;

    } else if ([[userInfo allKeys] containsObject:@"designationRepairUser"]) { //1
        //厂家指定维修单 弹窗
        [self shwoCJWXDBgview:userInfo[@"designationRepairUser"]];
        [singlTool shareSingTool].needReasfh = YES;
        //您有一条新的服务单 语音
        stere = @"您有一条新的服务单";
        sterename = @"newserviceorder";

    } else if ([[userInfo allKeys] containsObject:@"userPayTestOrderFirst"]) { //1
        //检测支付 首单 当日首单扣除保险费
        [self showBgviewWithCost:userInfo[@"userPayTestOrderFirst"]];
        stere = @"您的检测费已支付";
        sterename = @"nindejiancefeiyizhifu";
        
    } else if ([[userInfo allKeys] containsObject:@"userPayTestOrder"]) { //1
        //检测支付 不是首单 用户支付账单
        [self showBgview];
        stere = @"用户支付订单成功";
        sterename = @"nindejiancefeiyizhifu";
        
    } else if ([[userInfo allKeys] containsObject:@"testReportToRepairUserIP"]) { //
        //检测支付 用户终止订单 并支付 用户支付账单
        [self showBgview];
        stere = @"用户支付订单成功";
        sterename = @"nindejiancefeiyizhifu";
        
    } else if ([[userInfo allKeys] containsObject:@"userPayTestReportToRepairUser"]) { //1
        //检测支付 正常流程支付 用户支付账单
        [self showBgview];
        stere = @"用户支付订单成功";
        sterename = @"nindejiancefeiyizhifu";
        
    } else if ([[userInfo allKeys] containsObject:@"sendAuditGrants"]) { //1
        //您好，您有一条新的回复提醒，请注意查看
        [self shwoBgviewOne];
        stere = @"您好，您有一条新的回复提醒，请注意查看";
        
    } else if ([[userInfo allKeys] containsObject:@"notice"]) { //1
        //有新的通知，请及时查看
        stere = @"有新的通知,请及时查看";
        sterename = @"youxindetongzhi";
    }
    
    if (sterename.length > 5) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:sterename ofType:@"m4a"];
        NSURL *fileUrl = [NSURL URLWithString:filePath];
        SystemSoundID soundID = 0;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)(fileUrl), &soundID);
        AudioServicesAddSystemSoundCompletion(soundID,NULL,NULL,soundCompleteCallBack,NULL);
        AudioServicesPlaySystemSound(soundID);
    }
    
    NSDictionary *diccc = @{@"textstr":stere};
    completionHandler(UIBackgroundFetchResultNewData);
    [[NSNotificationCenter defaultCenter] postNotificationName:APPDELEGATE_REQUEST_REFRESHLISTDATA object:nil userInfo:diccc];
}

void soundCompleteCallBack(SystemSoundID soundID, void *clientData) {
    NSLog(@"声音播放完成");
}

/**
 弹出框的背景图 您有一条新的回复提醒
 */
-(void)shwoBgviewOne{
    self.bgViewOne = [[UIView alloc]init];
    self.bgViewOne.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bgViewOne.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bgViewOne addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(198);
        make.left.offset(38);
        make.right.offset(-38);
    }];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(15, 38.5, KWIDTH-76-30, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(17);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentLeft;
    UpLable.text = @"温馨提示";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(15, UpLable.bottom+31.5, KWIDTH-76-30, 20)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(13);
    DowLable.textColor = [UIColor colorWithHexString:@"#666666"];
    DowLable.textAlignment = NSTextAlignmentLeft;
    DowLable.text = @"您好，您有一条新的回复提醒，请注意查看";
    DowLable.numberOfLines = 0;
    
    UIButton *leftBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    leftBut.backgroundColor = [UIColor orangeColor];
    [leftBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [leftBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    [leftBut setTitle:@"立即查看" forState:(UIControlStateNormal)];
    leftBut.titleLabel.font = FontSize(14);
    leftBut.layer.masksToBounds = YES;
    leftBut.layer.cornerRadius = 18;
    [leftBut addTarget:self action:@selector(topLeftBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:leftBut];
    [leftBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(36);
        make.left.offset(100);
        make.right.offset(-100);
        make.bottom.offset(-25);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.bgViewOne];
}

-(void)topLeftBtnAction:(UIButton *)but{
    [_bgViewOne removeFromSuperview];
    //[self.navigationController popToRootViewControllerAnimated:YES];
}


/**
 新预约时间 弹出框的背景图
 */
-(void)showBgviewWithMes:(NSString *)mes {
    
    if (self.orderTimebgView) {
       [self.orderTimebgView removeFromSuperview];
    }
    NSDictionary *dict = [HFTools dictWithJsonString:mes];
    NSLog(@"%@",dict);
    
    self.orderTimebgView = [[UIView alloc]init];
    self.orderTimebgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.orderTimebgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.orderTimebgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.centerY.offset(0);
        make.height.offset(334);
        make.width.offset(KWIDTH-40);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.orderTimebgView.width, 96)];
    upImage.layer.masksToBounds = YES;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 2111"];

    UILabel *DowLable3 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable3];
    DowLable3.font = FontSize(14);
    DowLable3.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable3.textAlignment =  NSTextAlignmentLeft;
    DowLable3.text = [NSString stringWithFormat:@"客户电话:     %@", dict[@"user_phone"]];
    
    UILabel *DowLable4 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*2, 80, 21)];
    [whiteBGView addSubview:DowLable4];
    DowLable4.font = FontSize(14);
    DowLable4.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable4.textAlignment =  NSTextAlignmentLeft;
    DowLable4.text = [NSString stringWithFormat:@"客户地址:     "];
    DowLable4.numberOfLines = 2;
    
    UILabel *DowLable5 = [[UILabel alloc]initWithFrame:CGRectMake(DowLable4.right, 96+31*2, KWIDTH-40-DowLable4.right, 20)];
    [whiteBGView addSubview:DowLable5];
    DowLable5.font = FontSize(14);
    DowLable5.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable5.textAlignment =  NSTextAlignmentLeft;
    DowLable5.text = [NSString stringWithFormat:@"%@", dict[@"user_address"]];
    DowLable5.numberOfLines = 2;
    
    UILabel *DowLable1 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*3, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable1];
    DowLable1.font = FontSize(14);
    DowLable1.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable1.textAlignment =  NSTextAlignmentLeft;
    NSArray  *myarray = [dict[@"subscribe_date"] componentsSeparatedByString:@"-"];
    DowLable1.text = [NSString stringWithFormat:@"新预约日期: %@年%@月%@日", myarray[0],myarray[1],myarray[2]];
    
    UILabel *DowLable2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 96+31*4, KWIDTH-40-40, 21)];
    [whiteBGView addSubview:DowLable2];
    DowLable2.font = FontSize(14);
    DowLable2.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable2.textAlignment =  NSTextAlignmentLeft;
    NSInteger indd = [[dict[@"subscribe_time"] substringToIndex:2] integerValue];
    NSString *ap = indd < 12 ? @"上午":@"下午";
    DowLable2.text = [NSString stringWithFormat:@"新预约时间: %@ %@", ap, dict[@"subscribe_time"]];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 22;
    [iKnowBut.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [iKnowBut addTarget:self action:@selector(orderTimebgViewAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(44);
        make.left.offset(36);
        make.right.offset(-36);
        make.bottom.offset(-20);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.orderTimebgView];
}
-(void)orderTimebgViewAction:(UIButton *)but{
    [self.orderTimebgView removeFromSuperview];
}
/**
 首单提醒 弹出框的背景图
 */
-(void)showBgviewWithCost:(NSString *)cost {
    if (self.firstOrderBgView) {
        [self.firstOrderBgView removeFromSuperview];
    }
    self.firstOrderBgView = [[UIView alloc]init];
    self.firstOrderBgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.firstOrderBgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.firstOrderBgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.centerY.offset(0);
        make.height.offset(261);
        make.width.offset(246);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake((246-105)/2, 0, 105, 105)];
    upImage.layer.masksToBounds = YES;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组 2861"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 105, 246, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = [UIFont boldSystemFontOfSize:18];
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"账单支付完成";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 105+21, 246, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"恭喜您完成今日首单";
    
    UILabel *DowLable2 = [[UILabel alloc]initWithFrame:CGRectMake(20, 105+21+21+5, 246-40, 40)];
    [whiteBGView addSubview:DowLable2];
    DowLable2.font = FontSize(14);
    DowLable2.textColor = [UIColor colorWithHexString:@"#F88B1F"];
    DowLable2.textAlignment = NSTextAlignmentLeft;
    DowLable2.numberOfLines = 0;
    DowLable2.text = [NSString stringWithFormat:@"温馨提示:系统将从每日首单中扣除%@元费用为您投保。", cost];
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"好的" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 15;
    [iKnowBut.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [iKnowBut addTarget:self action:@selector(firstOrderBgViewAction) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(30);
        make.left.offset(36);
        make.right.offset(-36);
        make.bottom.offset(-20);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.firstOrderBgView];
}
-(void)firstOrderBgViewAction
{
    [self.firstOrderBgView removeFromSuperview];
}
/**
 订单支付完成 弹出框的背景图
 */
-(void)showBgview{
    if (self.orderFinishedBgView) {
        [self.orderFinishedBgView removeFromSuperview];
    }
    self.orderFinishedBgView = [[UIView alloc]init];
    self.orderFinishedBgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.orderFinishedBgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.orderFinishedBgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 30, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment = NSTextAlignmentCenter;
    DowLable.text = @"订单支付完成";

    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"结束订单" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(orderFinishedBgViewAction) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.orderFinishedBgView];
}
-(void)orderFinishedBgViewAction{
    
    [self.orderFinishedBgView removeFromSuperview];
}
/**
 用户重新下单 弹出框的背景图
 */
-(void)showUserMakeOrderAgainBgview{
    if ( self.buyAgainBgView) {
        [_buyAgainBgView removeFromSuperview];
    }
    self.buyAgainBgView = [[UIView alloc]init];
    self.buyAgainBgView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    
    self.buyAgainBgView.backgroundColor = RGBA(1, 1, 1, 0.5);
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.buyAgainBgView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(240);
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]initWithFrame:CGRectMake(KWIDTH/2-22-20, 20, 40, 40)];
    upImage.layer.masksToBounds = YES;
    upImage.layer.cornerRadius = 20;
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"组732"];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 21)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"用户已重新下单,请前往已接单列表查看";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(buyAgainBgViewAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-39);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.buyAgainBgView];
}
-(void)buyAgainBgViewAction:(UIButton *)but{
    
    [self.buyAgainBgView removeFromSuperview];
}

//补助金已到账
-(void)shwoBzjBgview:(NSString *)cost{
    if ( self.bzjView) {
         [_bzjView removeFromSuperview];
    }
    self.bzjView = [[UIView alloc] init];
    self.bzjView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.bzjView.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.bzjView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(283);
        make.left.offset(49.5);
        make.right.offset(-49.5);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]init];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"buzhujinyidaozhang"];
    [upImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(283);
        make.left.offset(0);
        make.right.offset(0);
    }];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    //[whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"请到我的账户";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"补助金中查看详细信息";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setImage:imgname(@"buzhujinIknow") forState:UIControlStateNormal];
    [iKnowBut addTarget:self action:@selector(bzjViewAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(60);
        make.left.offset(7);
        make.right.offset(-7);
        make.bottom.offset(-2);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.bzjView];

}

//补助金已到账 我知道了 点击按钮
-(void)bzjViewAction:(UIButton *)but{
    [_bzjView removeFromSuperview];
}

//新配件已上架
-(void)shwoPjysjBgview:(NSString *)cost {
    self.ysjView = [[UIView alloc]init];
    self.ysjView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.ysjView.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor clearColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.ysjView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(283);
        make.left.offset(26);
        make.right.offset(-26);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]init];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"peijianyishangjia"];
    [upImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(283);
        make.left.offset(0);
        make.right.offset(0);
    }];
    
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 72, KWIDTH-44, 45)];
    //[whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.textAlignment =  NSTextAlignmentCenter;
    UpLable.text = @"新配件已上架";
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 103, KWIDTH-44, 21)];
    //[whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(14);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"请到配件商城中查看";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setImage:imgname(@"ysjordernow") forState:UIControlStateNormal];
    [iKnowBut addTarget:self action:@selector(ysjViewAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(43);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-35);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.ysjView];

}

//新配件已上架 我知道了 点击按钮
-(void)ysjViewAction:(UIButton *)but {
    
    if ([self.userDic[@"newParts"] isEqualToString:@"1"]) { //跳转到 配件页面
        UITabBarController *tab = (UITabBarController *)_window.rootViewController;
        UINavigationController *nav = tab.viewControllers[tab.selectedIndex];
        ZYGoodObjectPreViewController *vc = [[ZYGoodObjectPreViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [nav pushViewController:vc animated:YES];
        
        [_ysjView removeFromSuperview];

    } else if ([self.userDic[@"newParts"] isEqualToString:@"2"]) { //跳转到 整机页面
        
        UITabBarController *tab = (UITabBarController *)_window.rootViewController;
        UINavigationController *nav = tab.viewControllers[tab.selectedIndex];
        ZYNewProductViewController *vc = [[ZYNewProductViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [nav pushViewController:vc animated:YES];
        
        [_ysjView removeFromSuperview];
    }
}

//系统已自动为您接取厂家所下维修单
-(void)shwoCJWXDBgview:(NSString *)cost {
    
    if (self.xitongView) {
        [self.xitongView removeFromSuperview];
    }
    self.xitongView = [[UIView alloc]init];
    self.xitongView.frame = CGRectMake(0, 0, KWIDTH, KHEIGHT);
    self.xitongView.backgroundColor = RGBA(1, 1, 1, 0.5);
    
    UIView *whiteBGView = [[UIView alloc]init];
    whiteBGView.backgroundColor = [UIColor whiteColor];
    whiteBGView.layer.masksToBounds = YES;
    whiteBGView.layer.cornerRadius = 6;
    [self.xitongView addSubview:whiteBGView];
    [whiteBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.height.offset(kScaleNum(239));
        make.left.offset(22);
        make.right.offset(-22);
    }];
    
    UIImageView *upImage = [[UIImageView alloc]init];
    [whiteBGView addSubview:upImage];
    upImage.image = [UIImage imageNamed:@"xitongimg"];
    [upImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(21);
        make.height.offset(62.05);
        make.left.offset(122);
        make.right.offset(-122);
    }];
    
    NSMutableAttributedString *attStr = [NSString getAttributedStringWithOriginalString:@"*系统已自动为您接取厂家所下维修单" originalColor:K333333 originalFont:FontSize(16) originalLineSpacing:0 changeString:@"*" changeColor:[UIColor colorWithHex:0xFF0000] changeFont:FontSize(16)];
    UILabel *UpLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 104, KWIDTH-44, 22)];
    [whiteBGView addSubview:UpLable];
    UpLable.font = FontSize(16);
    UpLable.numberOfLines = 0;
    UpLable.textColor = [UIColor colorWithHexString:@"#333333"];
    UpLable.attributedText = attStr;
    UpLable.textAlignment = NSTextAlignmentCenter;
    
    UILabel *DowLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 138, KWIDTH-44, 22)];
    [whiteBGView addSubview:DowLable];
    DowLable.font = FontSize(16);
    DowLable.textColor = [UIColor colorWithHexString:@"#333333"];
    DowLable.textAlignment =  NSTextAlignmentCenter;
    DowLable.text = @"请到我的订单服务单中查看该订单";
    
    UIButton *iKnowBut = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [iKnowBut setTitle:@"我知道了" forState:(UIControlStateNormal)];
    [iKnowBut setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [iKnowBut setBackgroundColor:[UIColor colorWithHexString:@"#70BE68"]];
    iKnowBut.layer.masksToBounds = YES;
    iKnowBut.layer.cornerRadius = 4;
    [iKnowBut addTarget:self action:@selector(CJWXDkenow:) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteBGView addSubview:iKnowBut];
    [iKnowBut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.offset(48);
        make.left.offset(28);
        make.right.offset(-28);
        make.bottom.offset(-15);
    }];
    
    [[UIApplication sharedApplication].keyWindow addSubview: self.xitongView];
    
}

//新配件已上架 我知道了 点击按钮
-(void)CJWXDkenow:(UIButton *)but{
    
    [self.xitongView removeFromSuperview];
    
}

- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (NSString*)uuid {
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString(nil, puuid);
    NSString *result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    return result;
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    [JSHAREService handleOpenUrl:url];
    // 其他如支付等SDK的回调
    if ([url.host isEqualToString:@"safepay"]) {
        
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            //code：拿到授权信息，完成业务逻辑
            KMyLog(@"resultDic%@",resultDic);
            if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
                
                [Center postNotificationName:@"shouquanSuccess" object:nil userInfo:resultDic];
            }
        }];
        
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            
            NSString *statues = resultDic[@"resultStatus"];
            if ([statues isEqualToString:@"9000"]) {
                //支付成功后通知后台
                [Center postNotificationName:@"PaySuccess" object:nil];
                
            }else {
                [Center postNotificationName:@"PayFail" object:nil];
            }
        }];
    } else  {
        return [WXApi handleOpenURL:url delegate:self];
    }
    //    else if ([url.host isEqualToString:@"pay"]) {
    //        return [WXApi handleOpenURL:url delegate:self];
    //    }
    return YES;
}

//微信支付
-(void)onResp:(BaseResp*)resp {
    if ([resp isKindOfClass:[SendMessageToWXResp class]]) {
        // strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
    }
    
    if ([resp isKindOfClass:[SendAuthResp class]]) {   //授权登录的类。
        SendAuthResp *aresp = (SendAuthResp *)resp;
        NSDictionary *diccc = @{@"code":aresp.code};
        [Center postNotificationName:@"wechatshouquanSuccess" object:nil userInfo:diccc];
    }
    
    if ([resp isKindOfClass:[PayResp class]]) {
        // strTitle = [NSString stringWithFormat:@"支付结果"];
        // NSLog(@"%@",PayResp.returnKey);
        switch (resp.errCode) {
            case WXSuccess: {
                NSLog(@"支付结果: 成功!");
                [Center postNotificationName:@"PaySuccess" object:nil];
            }
                break;
            case WXErrCodeCommon: {
                //签名错误、未注册APPID、项目设置APPID不正确、注册的APPID与设置的不匹配、其他异常等
                NSLog(@"支付结果: 失败!");
            }
                break;
            case WXErrCodeUserCancel: {
                //用户点击取消并返回
                NSLog(@"用户点击取消并返回");
                [Center postNotificationName:@"PayFail" object:nil];
            }
                break;
            case WXErrCodeSentFail: {
                //发送失败
                NSLog(@"发送失败");
            }
                break;
            case WXErrCodeUnsupport: {
                //微信不支持
                NSLog(@"微信不支持");
            }
                break;
            case WXErrCodeAuthDeny: {
                //授权失败
                NSLog(@"授权失败");
            }
                break;
            default:
                break;
        }
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationWillEnterForeground" object:nil userInfo:nil];
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    [JSHAREService handleOpenUrl:url];
    return YES;
}


- (void)firstStart{
    // 如果是第一次安装打开App --- 显示引导页面
    WQAPPStarViewController *wQController = [[WQAPPStarViewController alloc] initWithPagesCount:3 setupCellHandler:^(WQAPPStaetCollectionViewCell *cell, NSIndexPath *indexPath) {
        // 设置图片
        NSString *imageName = [NSString stringWithFormat:@"引导图0%ld.PNG",indexPath.row];
        UIImageView *myimage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, KHEIGHT)];
        myimage.backgroundColor = [UIColor whiteColor];
        cell.imageView.image = [UIImage imageNamed:imageName];
        // 设置按钮属性
        [cell.finishBtn setImage:imgname(@"loijitiy") forState:(UIControlStateNormal)];
        
    } finishHandler:^(UIButton *finishBtn) {
        //正常显示
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        TabBarViewController *tabbar = [[TabBarViewController alloc]init];
        self.window.rootViewController = tabbar;
    }];
    
    // 自定义属性
    wQController.pageControl.pageIndicatorTintColor = [UIColor yellowColor];
    wQController.pageControl.currentPageIndicatorTintColor = [UIColor purpleColor];
    [wQController.pageControl removeFromSuperview];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = wQController;
}

//全局禁止横屏
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskPortrait;
}

@end
