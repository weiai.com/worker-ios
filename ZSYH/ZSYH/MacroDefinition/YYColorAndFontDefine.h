//
//  YYColorDefine.h
//  testDemo
//
//  Created by apple on 2018/1/15.
//  Copyright © 2018年 com.ruanmeng. All rights reserved.
//

#ifndef YYColorDefine_h
#define YYColorDefine_h

/**传入16进制值，默认透明度1.0*/
#define kColorWithHex(RGB) [UIColor colorWithRed:((float)((RGB & 0xFF0000) >> 16))/255.0 green:((float)((RGB & 0xFF00) >> 8))/255.0 blue:((float)(RGB & 0xFF))/255.0 alpha:1.0]
/**传入16进制值，自定义透明度*/
#define kColorWithHexA(RGB,a) [UIColor colorWithRed:((float)((RGB & 0xFF0000) >> 16))/255.0 green:((float)((RGB & 0xFF00) >> 8))/255.0 blue:((float)(RGB & 0xFF))/255.0 alpha:a]
//RGB颜色
#define kColor(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
// 随机颜色
#define kColor_Random [UIColor colorWithRed:arc4random_uniform(256) / 255.0 green:arc4random_uniform(256) / 255.0 blue:arc4random_uniform(256) / 255.0 alpha:1]

#pragma mark -------------------------------- 背景颜色
/** 透明背景*/
#define ClearColor              [UIColor clearColor]
/** 白色背景*/
#define WhiteBGColor            [UIColor whiteColor]

/** 默认主颜色 */
#define kMain_Color             kColorWithHex(0x70BE68)
/** 默认辅助颜色 */
#define kMainAssisted_Color     kColorWithHex(0xFFCC00)
/** 默认主黄颜色 */
#define kMainYellowColor         kColorWithHex(0xFB9206)
/** 默认主紫颜色 */
#define kMainPurpleColor         kColorWithHex(0x866A99)
/** 默认主红颜色 */
#define kMainRedColor            kColorWithHex(0xFB4846)
/** 默认背景颜色*/
#define kMainBackGroundColor    kColorWithHex(0xF2F2F2)

#pragma mark -------------------------------- 字体颜色
/**黑色字体颜色*/
#define kMainTextColor00        kColorWithHex(0x000000)
/**黑色字体颜色*/
#define kMainTextColor33        kColorWithHex(0x333333)
/**灰色字体颜色*/
#define kMainTextColor66        kColorWithHex(0x666666)
#define GrayTextColor           kColorWithHex(0x666666)
/**灰色字体颜色*/
#define kMainTextColor99        kColorWithHex(0x999999)
#define LightGrayTextColor      kColorWithHex(0x999999)
/**灰色字体颜色*/
#define kMainTextColor1A        kColorWithHex(0x1A1A1A)
/**提示字体颜色*/
#define kMainPlaceHolderTextColor kColorWithHex(0xC7C7CD)
/** 白色字体*/
#define kMainWhiteTextColor     [UIColor whiteColor]

#pragma mark -------------------------------- 线条颜色
/**灰色线条颜色*/
#define kMainLineColor      kColorWithHex(0xE0E0E0)
#define GrayLineColor       kMainLineColor

#pragma mark -------------------------------- 边框颜色
/** 默认边框颜色*/
#define kMainBoardColor      kColorWithHex(0xE0E0E0)
#define GrayBoardColor      kMainBoardColor

/*
 * 普通字体
 */
#pragma mark -------------------------------- 普通字体
//字体大小
//#define KFontScale ([[UIScreen mainScreen] bounds].size.width / 375.0)
#define KFontScale 1
#define kFont(x)        [UIFont systemFontOfSize:x*KFontScale]
#define regular18Font   [UIFont systemFontOfSize:18*KFontScale]
#define regular17Font   [UIFont systemFontOfSize:17*KFontScale]
#define regular16Font   [UIFont systemFontOfSize:16*KFontScale]
#define regular15Font   [UIFont systemFontOfSize:15*KFontScale]
#define regular14Font   [UIFont systemFontOfSize:14*KFontScale]
#define regular13Font   [UIFont systemFontOfSize:13*KFontScale]
#define regular12Font   [UIFont systemFontOfSize:12*KFontScale]
#define regular11Font   [UIFont systemFontOfSize:11*KFontScale]
#define regular10Font   [UIFont systemFontOfSize:10*KFontScale]

/*
 * 加粗字体
 */
#pragma mark -------------------------------- 加粗字体
#define kBoldFont(x)    [UIFont boldSystemFontOfSize:x*KFontScale]
#define Bold18Font      [UIFont fontWithName:@"Helvetica-Bold" size:18*KFontScale]
#define Bold17Font      [UIFont fontWithName:@"Helvetica-Bold" size:17*KFontScale]
#define Bold16Font      [UIFont fontWithName:@"Helvetica-Bold" size:16*KFontScale]
#define Bold15Font      [UIFont fontWithName:@"Helvetica-Bold" size:15*KFontScale]
#define Bold14Font      [UIFont fontWithName:@"Helvetica-Bold" size:14*KFontScale]
#define Bold13Font      [UIFont fontWithName:@"Helvetica-Bold" size:13*KFontScale]
#define Bold12Font      [UIFont fontWithName:@"Helvetica-Bold" size:12*KFontScale]
#define Bold11Font      [UIFont fontWithName:@"Helvetica-Bold" size:11*KFontScale]
#define Bold10Font      [UIFont fontWithName:@"Helvetica-Bold" size:10*KFontScale]

/*
 * 其他字体
 */
#define KFontName(fontName, x) [UIFont fontWithName:fontName size:x*KFontScale]
#define KFontPingFangSCLight(x) [UIFont fontWithName:@"PingFangSC-Light" size:x*KFontScale]
#define KFontPingFangSCMedium(x) [UIFont fontWithName:@"PingFangSC-Medium" size:x*KFontScale]
#define KFontPingFangSCRegular(x) [UIFont fontWithName:@"PingFangSC-Regular" size:x*KFontScale]
#endif /* YYColorDefine_h */
