//
//  NotificationViewController.m
//  myjspathEstension
//
//  Created by 主事丫环 on 2019/5/29.
//  Copyright © 2019 魏堰青. All rights reserved.
//

#import "NotificationViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

@interface NotificationViewController () <UNNotificationContentExtension>

@property IBOutlet UILabel *label;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any required interface initialization here.
}

- (void)didReceiveNotification:(UNNotification *)notification {
    self.label.text = notification.request.content.body;
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"Hello" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"Agent-%d",arc4random()%100] arguments:nil];
    content.sound = [UNNotificationSound defaultSound];
    UNNotificationRequest *requester = [UNNotificationRequest requestWithIdentifier:@"OXNotification" content:content trigger:nil];
    [center addNotificationRequest:requester withCompletionHandler:^(NSError *_Nullable error) {
        NSLog(@"成功添加推送");
    }];

    
    
}

@end
